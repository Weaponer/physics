#ifndef PE_RESULT_RECEIVER_SYSTEM_H
#define PE_RESULT_RECEIVER_SYSTEM_H

#include "peSystem.h"
#include "peGpuStaticReaderSystem.h"

#include "common/peGpuData.h"

namespace phys
{
    class PeResultReceiverSystem : public PeSystem
    {
    private:
        PeGpuData *gpu;
        PeGpuStaticReaderSystem *staticReader;

    public:
        PeResultReceiverSystem();

        virtual ~PeResultReceiverSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void receiveData();

        void sendData();
    };
}

#endif