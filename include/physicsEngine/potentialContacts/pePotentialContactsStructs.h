#ifndef PE_POTENTIAL_CONTACTS_STRUCTS_H
#define PE_POTENTIAL_CONTACTS_STRUCTS_H

#include <MTH/matrix.h>

namespace phys
{
    struct PeGpuDataOutAverage
    {
        int32_t averageX;
        int32_t averageY;
        int32_t averageZ;
    };

    struct PeGpuDataOutDisperstion
    {
        int32_t dispersionX;
        int32_t dispersionY;
        int32_t dispersionZ;
    };

    struct PeGpuDataMainPlanes
    {
        mth::vec4 outPosition;
        mth::vec4 outRight;
        mth::vec4 outUp;
        mth::vec4 outForward;
    };

    struct PeGpuDataCountContacts
    {
        uint32_t xPlaneContacts;
        uint32_t yPlaneContacts;
        uint32_t zPlaneContacts;
    };

    struct PeGpuDataGlobalBound
    {
        mth::mat4 outGlobalMatrix;
    };

    struct PeGpuDataIndexKeyOBB
    {
        uint32_t index;
        uint32_t key;
    };

    struct PeGpuDataNodeOBB
    {
        uint32_t index;
        int32_t next;       //may -1
    };

    struct PeGpuDataSegmentOBBs
    {
        uint32_t indexStart;
        uint32_t count;
    };
}

#endif