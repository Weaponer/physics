#ifndef PE_LIST_CHECK_CONTACTS_BUILDER_SYSTEM_H
#define PE_LIST_CHECK_CONTACTS_BUILDER_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"

#include "physicsEngine/potentialContacts/peUpdateGeometryOBBsSystem.h"
#include "physicsEngine/potentialContacts/peGlobalBoundOBBsBuilderSystem.h"
#include "physicsEngine/potentialContacts/peKdTreeBuilderSystem.h"

namespace phys
{
    class PeListCheckContactsBuilderSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;

        PeKdTreeBuilderSystem *kdTreeBuilder;

        struct
        {
            PeGpuImage *headPointsCounterImage;
            PeGpuImage *segmentsCounterImage;
            PeGpuImage *indexesCounterImage;

            PeGpuImage *headPointsImage;

            PeGpuBuffer *outputNodeOBBsIndexesBuffer;
            PeGpuBuffer *outputSegmentsOBBsBuffer;
            PeGpuBuffer *outputIndexesBuffer;

            uint32_t maxCountIndexes;
            mth::Vector2Int sizeNodesImage;
        } buffers;

        struct
        {
            vk::ComputePipeline linkedListBuilderP;
            VkPipelineLayout linkedListBuilderL;
            vk::ShaderAutoParameters *linkedListBuilderS;

            vk::ComputePipeline segmentsBuilderP;
            VkPipelineLayout segmentsBuilderL;
            vk::ShaderAutoParameters *segmentsBuilderS;
        } shaders;

        PeGpuReadImageMemory *readCountSegments;
        PeGpuReadMemory *readSegments;
        PeGpuReadMemory *readIndexes;

    public:
        PeListCheckContactsBuilderSystem();

        virtual ~PeListCheckContactsBuilderSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        uint32_t getMaxCountIndexes() const { return buffers.maxCountIndexes; }

        uint32_t getMaxCountSegments() const { return buffers.maxCountIndexes; }

        PeGpuBuffer *getSegmentsBuffer() const { return buffers.outputSegmentsOBBsBuffer; }

        PeGpuBuffer *getIndexesBuffer() const { return buffers.outputIndexesBuffer; }

        PeGpuImage *getSegmentsCounterImage() const { return buffers.segmentsCounterImage; }

        PeGpuImage *getIndexesCounterImage() const { return buffers.indexesCounterImage; }

    private:
        void ensureMemory(PeSceneSimulationData *_simulationData);

        void ensureImages(mth::Vector2Int _sizeStep);

        void ensureBuffers(uint32_t _countInputIndexes, mth::Vector2Int _sizeImage);
    };
}

#endif