#ifndef PE_GLOBAL_BOUND_OBBS_BUILDER_SYSTEM_H
#define PE_GLOBAL_BOUND_OBBS_BUILDER_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"

#include "physicsEngine/potentialContacts/peUpdateGeometryOBBsSystem.h"

namespace phys
{
    class PeGlobalBoundOBBsBuilderSystem : public PeSystem
    {
    private:
        PeUpdateGeometryOBBsSystem *updateGeometrySystem;
        PeGpuData *gpuData;
        PeGpuStaticReaderSystem *staticReader;
        PeShapeDescriptionSystem *shapeDescriptionSystem;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;


        struct // i - shader input, o - shader output
        {
            PeGpuBuffer *oAveragePos;
            PeGpuBuffer *oDispersionSize;
            PeGpuBuffer *oMainPlanes;
            PeGpuBuffer *iPlaneCountContact;
            PeGpuBuffer *iGlobalBound;
        } buffers;

        struct
        {
            vk::ComputePipeline calculateAverageP;
            VkPipelineLayout calculateAverageL;
            vk::ShaderAutoParameters *calculateAverageS;

            vk::ComputePipeline calculateDispersionBoundP;
            VkPipelineLayout calculateDispersionBoundL;
            vk::ShaderAutoParameters *calculateDispersionBoundS;

            vk::ComputePipeline buildBoundP;
            VkPipelineLayout buildBoundL;
            vk::ShaderAutoParameters *buildBoundS;

            vk::ComputePipeline checkContactsBoundP;
            VkPipelineLayout checkContactsBoundL;
            vk::ShaderAutoParameters *checkContactsBoundS;

            vk::ComputePipeline rebalanceGlobalBoundP;
            VkPipelineLayout rebalanceGlobalBoundL;
            vk::ShaderAutoParameters *rebalanceGlobalBoundS;
        } shaders;

        PeGpuReadMemory *readMemory;

    public:
        PeGlobalBoundOBBsBuilderSystem();

        virtual ~PeGlobalBoundOBBsBuilderSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        PeGpuBuffer *getOutputGlobalBoundBuffer() const { return buffers.iGlobalBound; }

    private:
        void buildGlobalBound(PeSceneSimulationData *_simulationData);
    };
}

#endif