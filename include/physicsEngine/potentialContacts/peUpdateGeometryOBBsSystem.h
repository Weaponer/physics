#ifndef PE_UPDATE_GEOMETRY_OBBS_SYSTEM_H
#define PE_UPDATE_GEOMETRY_OBBS_SYSTEM_H

#include "foundation/phEasyStack.h"

#include "physics/phOutputDebugData.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peReallocateDatasSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"

namespace phys
{
    class PeUpdateGeometryOBBsSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeReallocateDatasSystem *reallocateDatasSystem;
        PeMemoryBarrierControllerSystem *memoryBarrierController;

        struct
        {
            PeGpuBuffer *indexesToUpdateBuffer;
            PeGpuBuffer *indexesToTestBuffer;
        } buffers;

        struct SupportIndexOBB
        {
            PhOutputDebugData::OBBofActor data;
            uint32_t indexOBB;
        };

        uint32_t maxIndexOBB;
        int32_t indexPlaneGeomtery;
        PhEasyStack<uint32_t> bufferOBBsToUpdate;
        PhEasyStack<uint32_t> bufferOBBsToTestContacts;
        PhEasyStack<uint32_t> bufferOBBsToTestOnlySizableContacts;
        PhEasyStack<SupportIndexOBB> obbOfActorsData;
        PhEasyStack<PhOutputDebugData::OBBofActor> filledOfActorsData;

    public:
        PeUpdateGeometryOBBsSystem();

        virtual ~PeUpdateGeometryOBBsSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareListUpdateGeometries(PeSceneSimulationData *_simulationData);

    private:
        bool isSizable(uint32_t _flags);

    public:
        PeGpuBuffer *getIndexesToUpdateBuffer() const { return buffers.indexesToUpdateBuffer; }

        PeGpuBuffer *getIndexesToTestBuffer() const { return buffers.indexesToTestBuffer; }

        uint32_t getMaxIndexOBB() const { return maxIndexOBB; }

        int32_t getIndexOfPlaneGeometry() const { return indexPlaneGeomtery; }

        uint32_t getSizeDataOBBsToUpdate() const { return bufferOBBsToUpdate.getPos(); }

        const uint32_t *getDataOBBsToUpdate() const { return bufferOBBsToUpdate.getMemory(0); }

        uint32_t getSizeDataOBBsToTestContacts() const { return bufferOBBsToTestContacts.getPos(); }

        const uint32_t *getDataOBBsToTestContacts() const { return bufferOBBsToTestContacts.getMemory(0); }

        uint32_t getSizeDataOBBsToTestOnlySizableContacts() const { return bufferOBBsToTestOnlySizableContacts.getPos(); }

        const uint32_t *getDataOBBsToTestOnlySizableContacts() const { return bufferOBBsToTestOnlySizableContacts.getMemory(0); }

        const PhOutputDebugData::OBBofActor *getFilledOBBsActorsData() const { return filledOfActorsData.getMemory(0); }
    };
}

#endif