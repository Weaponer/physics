#ifndef PE_POTENTIAL_CONTACTS_SYSTEM_H
#define PE_POTENTIAL_CONTACTS_SYSTEM_H

#include "foundation/phContext.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/common/peGpuData.h"

#include "peUpdateGeometryOBBsSystem.h"
#include "peUpdateOrientationGeometryOBBsSystem.h"
#include "peGlobalBoundOBBsBuilderSystem.h"
#include "peKdTreeBuilderSystem.h"
#include "peListCheckContactsBuilderSystem.h"

namespace phys
{
    class PePotentialContactsSystem : public PeSystem
    {
    public:
        struct ResultStage
        {
            uint32_t maxIndexOBB;
            uint32_t countTestOBBs;
            int32_t indexPlaneGeomtery;
            uint32_t countIndexesToUpdate;
            uint32_t countIndexesToTest;
            PeGpuBuffer *indexesToUpdateBuffer; 
            PeGpuBuffer *indexesToTestBuffer;

            uint32_t maxCountSegments;
            uint32_t maxCountIndexes;

            PeGpuBuffer *segmentsBuffer;
            PeGpuBuffer *indexesBuffer;

            PeGpuImage *segmentsCounter;
            PeGpuImage *indexesCounter;
        };

    private:
        PhContext potentialContactsContext;

        PeUpdateGeometryOBBsSystem updateGeometryOBBs;
        PeUpdateOrientationGeometryOBBsSystem updateOrientationSystem;
        PeGlobalBoundOBBsBuilderSystem globalBoundOBBsBuilderSystem;
        PeKdTreeBuilderSystem kdTreeBuilderSystem;
        PeListCheckContactsBuilderSystem listCheckContactsBuilderSystem;

        PeGpuData *gpu;

    public:
        PePotentialContactsSystem();

        virtual ~PePotentialContactsSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        ResultStage getResultStage();
    };
}

#endif