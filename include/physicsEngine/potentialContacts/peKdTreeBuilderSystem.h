#ifndef PE_KD_TREE_BUILDER_SYSTEM_H
#define PE_KD_TREE_BUILDER_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"

#include "physicsEngine/potentialContacts/peUpdateGeometryOBBsSystem.h"
#include "physicsEngine/potentialContacts/peGlobalBoundOBBsBuilderSystem.h"

namespace phys
{
#define PE_KD_TREE_BUILDER_MAX_ITERATIONS 19U

    class PeKdTreeBuilderSystem : public PeSystem
    {
    private:
        PeUpdateGeometryOBBsSystem *updateGeometrySystem;
        PeGlobalBoundOBBsBuilderSystem *globalBoundOBBsBuilderSystem;
        PeGpuData *gpuData;
        PeGpuStaticReaderSystem *staticReader;
        PeShapeDescriptionSystem *shapeDescriptionSystem;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;

        struct // i - shader input, o - shader output
        {
            PeGpuImage *oNodeCountOBBs[PE_KD_TREE_BUILDER_MAX_ITERATIONS];
            PeGpuImage *oNodeDistanceOBBs[PE_KD_TREE_BUILDER_MAX_ITERATIONS];
            PeGpuImage *iPreviosNodeOffsets[PE_KD_TREE_BUILDER_MAX_ITERATIONS];
            PeGpuImage *iPreviosActiveNodes[PE_KD_TREE_BUILDER_MAX_ITERATIONS];

            PeGpuImage *oCounterHeadPoints; // 1D image, size - PE_KD_TREE_BUILDER_MAX_ITERATIONS
            PeGpuBuffer *oIndexesKdBuffer[PE_KD_TREE_BUILDER_MAX_ITERATIONS + 1];

            uint32_t countOBBsInIndexesKdBuffer[PE_KD_TREE_BUILDER_MAX_ITERATIONS + 1];
            mth::Vector2Int sizeInputImages[PE_KD_TREE_BUILDER_MAX_ITERATIONS];
            mth::Vector2Int sizeOutputImages[PE_KD_TREE_BUILDER_MAX_ITERATIONS];

            PeGpuBuffer *oDispatchDataBuffer;
            PeGpuBuffer *iIndirectDispatchBuffer;

            uint32_t maxPreparedStep;
        } buffers;

        struct
        {
            vk::ComputePipeline kdTreeNodeBuilderP;
            VkPipelineLayout kdTreeNodeBuilderL;
            vk::ShaderAutoParameters *kdTreeNodeBuilderS;

            vk::ComputePipeline kdTreeNodeOffsetsBuilderP;
            VkPipelineLayout kdTreeNodeOffsetsBuilderL;
            vk::ShaderAutoParameters *kdTreeNodeOffsetsBuilderS;
        } shaders;

        // info before launch
        uint32_t countWrotedBefore;
        PeGpuReadMemory *readMemory;
        PeGpuReadImageMemory *readCounterHeadPoints;

        // info current launch
        uint32_t countUseSteps;
        uint32_t countTestOBBs;

    public:
        PeKdTreeBuilderSystem();

        virtual ~PeKdTreeBuilderSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        uint32_t getCountUseSteps() const { return countUseSteps; }

        uint32_t getCountOutputIndexOBBsOnStep(uint32_t _step) const { return buffers.countOBBsInIndexesKdBuffer[_step + 1]; }

        mth::Vector2Int getOutputImageSize(uint32_t _step) const { return buffers.sizeOutputImages[_step]; }

        PeGpuBuffer *getOutputIndexBuffer(uint32_t _step) const { return buffers.oIndexesKdBuffer[_step + 1]; }

        PeGpuImage *getOutputNodeCountOBBsImage(uint32_t _step) const { return buffers.oNodeCountOBBs[_step]; }

        PeGpuImage *getCounterHeadPointsImage() const { return buffers.oCounterHeadPoints; }

    private:
        void buildKdTree(PeSceneSimulationData *_simulationData);

        void buildShaderParameters(PeSceneSimulationData *_simulationData,
                                   uint32_t _countSteps, VkDescriptorSet *_outSetsTreeBuilder, VkDescriptorSet *_outSetsTreeOffsetsBuilder);

        void buildBarrierKdTreeNodeBuilder(PeSceneSimulationData *_simulationData, uint32_t _step);

        void buildBarrierKdTreeNodeOffsetsBuilder(PeSceneSimulationData *_simulationData, uint32_t _step);

        PeGpuBuffer *createIndexesKdBuffer(uint32_t _step, uint32_t _countOBBs);

        void prepareMemoryForSteps(uint32_t _steps, uint32_t _startCountOBBs);

        void ensureBufferKdTree(uint32_t _steps, uint32_t _startCountOBBs);

        void ensureBufferKdTreeNodes(uint32_t _steps);

        void updateStepCapacity(uint32_t _step, uint32_t _countOBBs);

        uint32_t getNeedCountSteps(uint32_t _countOBBs);

        uint32_t getNeedCountInSteps(uint32_t _step, uint32_t _countOBBs);

        mth::Vector2Int getSizeOutputKdTreeNodes(uint32_t _step);

        mth::Vector2Int getSizeInputKdTreeNodes(uint32_t _step);

        void clearImage(PeGpuImage *_image, VkClearColorValue &_value);
    };
}

#endif