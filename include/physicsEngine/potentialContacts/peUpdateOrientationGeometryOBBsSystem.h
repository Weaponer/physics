#ifndef PE_UPDATE_ORIENTATION_GEOMETRY_OBBS_SYSTEM_H
#define PE_UPDATE_ORIENTATION_GEOMETRY_OBBS_SYSTEM_H

#include <vulkan/vulkan.h>

#include "physics/phOutputDebugData.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/potentialContacts/peUpdateGeometryOBBsSystem.h"

namespace phys
{
    class PeUpdateOrientationGeometryOBBsSystem : public PeSystem
    {
    private:
        PeUpdateGeometryOBBsSystem *updateGeometrySystem;
        PeGpuData *gpuData;
        PeGpuStaticReaderSystem *staticReader;

        PeShapeDescriptionSystem *shapeDescriptionSystem;

        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;

        vk::ComputePipeline pipeline;
        VkPipelineLayout layout;
        vk::ShaderAutoParameters *parameters;

        PhEasyStack<PhOutputDebugData::OBBofActor> readedOBBofActorsData;
        PeGpuReadMemory *readOrientationOBBs;
        uint32_t wasReaded;

    public:
        PeUpdateOrientationGeometryOBBsSystem();

        virtual ~PeUpdateOrientationGeometryOBBsSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);
    };
}

#endif