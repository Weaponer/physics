#ifndef PE_MATERIALS_DESCRIPTION_SYSTEM_H
#define PE_MATERIALS_DESCRIPTION_SYSTEM_H

#include "common/phBase.h"

#include "physics/internal/inMaterial.h"
#include "physics/internal/inMaterialManager.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuDescription.h"
#include "physicsEngine/common/peGpuObjectsBuffer.h"

#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peGpuStaticLoaderSystem.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"

namespace phys
{
    class PeMaterialsDescriptionSystem : public PeSystem
    {
    private:
        PeGpuObjectType materialType;
        PeGpuObjectsBuffer *materialsBuffer;

        PeGpuData *gpu;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeGpuStaticLoaderSystem *staticLoaderSystem;
        PeGpuStaticReaderSystem *staticReaderSystem;

        InMaterialManager *materialManager;
        PeGpuReadMemory *readMemory;

    public:
        PeMaterialsDescriptionSystem();

        virtual ~PeMaterialsDescriptionSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        uint32_t getLocalIndexMaterial(InMaterial *_material);

        PeGpuObjectsBuffer *getMaterialBuffer() const { return materialsBuffer; }

    private:
        void printReadMemory();
    };
}

#endif