#ifndef PE_MEMORY_BARRIER_CONTROLLER_SYSTEM_H
#define PE_MEMORY_BARRIER_CONTROLLER_SYSTEM_H

#include "common/phFlags.h"

#include "peSystem.h"

#include "common/peGpuBarrier.h"
#include "common/peGpuData.h"
#include "common/peGpuMemoryBuffer.h"
#include "common/peGpuMemoryImage.h"

namespace phys
{
    class PeMemoryBarrierControllerSystem : public PeSystem
    {
    private:
        PeGpuData *gpu;

        uint32_t currentStep;

        static const uint32_t maxInternalMemory = 32;

        PeGpuMemoryImage *images[maxInternalMemory];
        PeGpuMemoryBuffer *buffers[maxInternalMemory];
        PeTypeAccessData imagesA[maxInternalMemory];
        PeTypeAccessData buffersA[maxInternalMemory];

    public:
        PeMemoryBarrierControllerSystem();

        virtual ~PeMemoryBarrierControllerSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void reset();

        void doNextStep();

        void initUnknowMemory(PeGpuMemory *_memory);

        void setImage(uint32_t _i, PeGpuMemoryImage *_m, PeTypeAccessData _a);

        void setBuffer(uint32_t _i, PeGpuMemoryBuffer *_m, PeTypeAccessData _a);

        void setupBarrier(PeTypeStageData _stage, uint32_t _buffes, uint32_t _images);

        void setupBarrier(PeTypeStageData _stage, uint32_t _countBuffers, PeGpuMemoryBuffer **_memoryBuffers, PeTypeAccessData *_accessBuffers,
                          uint32_t _countImages, PeGpuMemoryImage **_memoryImages, PeTypeAccessData *_accessImages);
    };
}

#endif