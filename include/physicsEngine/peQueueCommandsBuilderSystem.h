#ifndef PE_QUEUE_COMMANDS_BUILDER_SYSTEM_H
#define PE_QUEUE_COMMANDS_BUILDER_SYSTEM_H

#include "peSystem.h"
#include "peGpuStaticLoaderSystem.h"
#include "peGpuStaticReaderSystem.h"
#include "peSceneSimulationData.h"

#include "common/peGpuData.h"

#include "materialDescription/peMaterialsDescriptionSystem.h"
#include "shapeDescription/peShapeDescriptionSystem.h"

#include "gpuCommon/specialCommands/vulkanComputeCommands.h"

#include "actorDescription/peSceneActorsDescription.h"

#include "geometryObbDescription/peGeometryObbDescription.h"

#include "potentialContacts/pePotentialContactsSystem.h"
#include "tableContacts/peTableContactsSystem.h"
#include "contacts/peContactsSystem.h"
#include "inertialTensors/peInertialTensorsSystem.h"
#include "physicsSimulation/pePhysicsSimulationSystem.h"

namespace phys
{
    class PeQueueCommandsBuilderSystem : public PeSystem
    {
    private:
        PeGpuData *gpu;
        PeGpuStaticLoaderSystem *staticLoaderSystem;
        PeGpuStaticReaderSystem *staticReaderSystem;
        PeMaterialsDescriptionSystem *materialsDescriptionSystem;
        PeShapeDescriptionSystem *shapeDescriptionSystem;
        PeSceneActorsDescription *sceneActorsDescriptionSystem;

        PeGeometryObbDescription *geomteryObbDescriptionSystem;

        PePotentialContactsSystem *potentialContactsSystem;
        PeTableContactsSystem *tableContactsSystem;
        PeContactsSystem *contactsSystem;
        PeInertialTensorsSystem *inertialTensorsSystem;
        PePhysicsSimulationSystem *physicsSimulationSystem;

    public:
        PeQueueCommandsBuilderSystem();

        virtual ~PeQueueCommandsBuilderSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void buildPrepareStage(PeSceneSimulationData *_simulationData);

        void buildSimulationStage(PeSceneSimulationData *_simulationData);

        void buildReceiveDataStage(PeSceneSimulationData *_simulationData);
    };
}

#endif