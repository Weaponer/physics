#ifndef PE_GRAB_OBJECTS_DATAS_SYSTEM_H
#define PE_GRAB_OBJECTS_DATAS_SYSTEM_H

#include "peSystem.h"

#include "common/peGpuMemory.h"

#include "foundation/phEasyStack.h"

namespace phys
{
    class PeGrabObjectsDatasSystem : public PeSystem
    {
    private:
        PhEasyStack<PeGpuMemory *> stackData;

    public:
        PeGrabObjectsDatasSystem();

        virtual ~PeGrabObjectsDatasSystem() {}

        virtual void release() override;

        void addDataToUnload(PeGpuMemory *_data);

        void unloadData();
    };
}

#endif