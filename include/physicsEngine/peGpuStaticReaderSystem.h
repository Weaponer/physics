#ifndef PE_GPU_STATIC_READER_SYSTEM_H
#define PE_GPU_STATIC_READER_SYSTEM_H

#include "peSystem.h"

#include "common/phBase.h"
#include "common/peGpuData.h"

#include "foundation/phEasyStack.h"

#include "gpuCommon/memory/vulkanBuffer.h"

namespace phys
{
    class PeGpuStaticReaderSystem : public PeSystem
    {
    public:
        class ReadMemory : public PhBase
        {
        private:
            uint8_t *memory;
            uint32_t size;
            bool isValid;

        private:
            ReadMemory() : PhBase(), memory(NULL), size(0), isValid(false) {}

            ReadMemory(const ReadMemory &_mem) = delete;

            virtual ~ReadMemory() {}

        public:
            const uint8_t *getMemory() const { return memory; }

            uint32_t getSize() const { return size; }

            bool IsValid() const { return isValid; }

            friend class PeGpuStaticReaderSystem;
        };

        class ReadImageMemory : public PhBase
        {
        private:
            vk::VulkanBuffer *stagingBuffer;
            uint8_t *memory;
            uint32_t size;
            bool isValid;

        private:
            ReadImageMemory() : PhBase(), stagingBuffer(NULL), memory(NULL), size(0), isValid(false) {}

            ReadImageMemory(const ReadImageMemory &_mem) = delete;

            virtual ~ReadImageMemory() {}

        public:
            const uint8_t *getMemory() const { return memory; }

            uint32_t getSize() const { return size; }

            bool IsValid() const { return isValid; }

            friend class PeGpuStaticReaderSystem;
        };

    private:
        struct ReadEvent
        {
            vk::VulkanBuffer *srcBuffer;
            uint32_t offsetSrc;
            uint32_t indexDstBuffer;
            uint32_t offsetDstBuffer;

            ReadMemory *dstMemory;
            uint32_t offsetDstMemory;
            uint32_t size;
        };

        struct ReadImageEvent
        {
            vk::VulkanImage *srcImage;
            VkImageSubresourceLayers imageSubresource;
            VkOffset3D imageOffset;
            VkExtent3D imageExtent;

            ReadImageMemory *dstMemory;
            uint32_t offsetDstMemory;
        };

    private:
        PeGpuData *gpu;

        PhEasyStack<vk::VulkanBuffer *> buffers;

        PhEasyStack<ReadEvent> events;
        PhEasyStack<ReadImageEvent> imageEvents;

        uint32_t indexCurrentBuffer;
        uint32_t offsetCurrentBuffer;

        uint32_t sizeBuffer;

    public:
        PeGpuStaticReaderSystem();

        virtual ~PeGpuStaticReaderSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        ReadMemory *createReadMemoryPoint(uint32_t _size);

        void destroyReadMemoryPoint(ReadMemory *_memory);

        ReadImageMemory *createReadImageMemoryPoint(uint32_t _size);

        ReadImageMemory *createReadImageMemoryPoint(vk::VulkanImage *_image);

        void destroyReadImageMemoryPoint(ReadImageMemory *_memory);

        void sendEvents();

        void receiveMemory();

        void resetEvents();

        void addRequest(vk::VulkanBuffer *_src, uint32_t _srcOffset, ReadMemory *_dst, uint32_t _dstOffset, uint32_t _size);

        void addRequest(vk::VulkanImage *_src, ReadImageMemory *_dst, uint32_t _dstOffset);

        void addRequest(vk::VulkanImage *_src, ReadImageMemory *_dst, uint32_t _dstOffset,
                        VkImageSubresourceLayers _imageSubresource, VkOffset3D _imageOffset, VkExtent3D _imageExtent);

    private:
        vk::VulkanBuffer *createBuffer();

        void allocateNextBuffer();
    };

    typedef PeGpuStaticReaderSystem::ReadMemory PeGpuReadMemory;
    typedef PeGpuStaticReaderSystem::ReadImageMemory PeGpuReadImageMemory;
}

#endif