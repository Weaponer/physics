#ifndef PE_PHYSICS_SIMULATION_STRUCTS_H
#define PE_PHYSICS_SIMULATION_STRUCTS_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>

namespace phys
{
    struct PeGpuDataSimContactsContainer_2 // 32
    {
        float averageVelocity;      // 0 4
        float dynamicFriction;      // 4 8
        float staticFriction;       // 8 12
        float bounciness;           // 12 16
        int32_t actorA;             // 16 20
        int32_t actorB;             // 20 24
        uint32_t countContacts;     // 24 28
        uint32_t indexFirstContact; // 28 32
    };

    struct PeGpuCommonDataSimContact // 48
    {
        mth::vec3 positionA;  // 0 12
        float _freeMem_0;     // 12 16
        mth::vec3 positionB;  // 16 28
        float _freeMem_1;     // 28 32
        mth::vec3 normalAtoB; // 32 44
        float depth;          // 44 48
    };

    struct PeGpuCommonDataContactsContainer // 48
    {
        float averageVelocity;      // 0 4
        float dynamicFriction;      // 4 8
        float staticFriction;       // 8 12
        float bounciness;           // 12 16
        int32_t actorA;             // 16 20
        int32_t actorB;             // 20 24
        uint32_t orientationActorA; // 24 28
        uint32_t orientationActorB; // 28 32
        uint32_t countContacts;     // 32 36
        uint32_t indexFirstContact; // 36 40
        uint32_t flags;             // 40 44
        uint32_t freeMemory_1;      // 44 48
    };

    struct PeGpuDataSimContactModel // 128
    {
        mth::vec4 basis[3]; // 0 48
                            // basis[0] = { basis[0][0], basis[0][1], basis[0][2], dirContactA.x }
                            // basis[1] = { basis[1][0], basis[1][1], basis[1][2], dirContactA.y }
                            // basis[2] = { basis[2][0], basis[2][1], basis[2][2], dirContactA.z }
        mth::vec4 A[3]; // 48 96
                        // A[3] = { A[0][0], A[0][1], A[0][2], dirContactB.x }
                        // A[4] = { A[1][0], A[1][1], A[1][2], dirContactB.y }
                        // A[5] = { A[2][0], A[2][1], A[2][2], dirContactB.z }
        mth::vec4 velocity; // 96 112   xyz - velocity, w - friction
        mth::vec4 position; // 112 128   xyz - position, w - depth
        mth::vec4 testData;
    };

    struct PeGpuDataSimContactsContainerEPBD    // 128
    {
        mth::vec4 dataA[5];                     // 0 80
        mth::vec4 dataB[5];                     // 80 160
                                                // data[0] = { invTensor[0][0], invTensor[0][1], invTensor[0][2], invMass }
                                                // data[1] = { invTensor[1][0], invTensor[1][1], invTensor[1][2], angularVel.x }
                                                // data[2] = { invTensor[2][0], invTensor[2][1], invTensor[2][2], angularVel.y }
                                                // data[3] = { centerOfMass.x, centerOfMass.y, centerOfMass.z, angularVel.z }
                                                // dataA[4] = { linearVelA.x, linearVelA.y, linearVelA.z, frictionStatic }
                                                // dataB[4] = { linearVelB.x, linearVelB.y, linearVelB.z, frictionDynamic }

        uint32_t countContacts;                 // 160 164
        uint32_t indexFirstContact;             // 164 168
        int32_t indexGraph;                     // 168 172
        int32_t startListContainersA;           // 172 176
        uint32_t startSwapActorA;               // 176 180
        int32_t startListContainersB;           // 180 184
        uint32_t startSwapActorB;               // 184 188
        int32_t bounciness;                     // 188 192
    };

    struct PeGpuDataSimContactsContainerFinished // 128
    {
        mth::vec4 invMassTensorA[3]; // 0 48
        mth::vec4 invMassTensorB[3]; // 48 96
                                     // invMassTensor[0] = { invTensor[0][0], invTensor[0][1], invTensor[0][2], invMass }
                                     // invMassTensor[1] = { invTensor[1][0], invTensor[1][1], invTensor[1][2], 0 }
                                     // invMassTensor[2] = { invTensor[2][0], invTensor[2][1], invTensor[2][2], 0 }

        uint32_t countContacts;       // 96 100
        uint32_t indexFirstContact;   // 100 104
        int32_t indexGraph;           // 104 108
        int32_t startListContainersA; // 108 112
        uint32_t startSwapActorA;     // 112 116
        int32_t startListContainersB; // 116 120
        uint32_t startSwapActorB;     // 120 124
        uint32_t freeMemory_1;        // 124 128
    };

    struct PeGpuDataSimContactVel // 16
    {
        mth::vec4 velocity; // 0 16   xyz - vel, w - friction
    };

    struct PeGpuDataSimContainerNode // 8
    {
        uint32_t indexContainer; // 0 4
        int32_t next;            // 4 8
    };

    struct PeGpuDataActorGraphEdge_2 // 16
    {
        int32_t nodeA;            // 0 4
        int32_t nodeB;            // 4 8
        float totalForceContacts; // 8 12
        int32_t flags;            // 12 16
    };

    struct PeGpuDataAccamulatedContainerVelocity // 64
    {
        mth::vec4 velocities[3]; // 0 48
                                 // velocities[0] = { linearVelocityA.x, linearVelocityA.y, linearVelocityA.z, angularVelocityB.x }
                                 // velocities[1] = { angularVelocityA.x, angularVelocityA.y, angularVelocityA.z, angularVelocityB.y }
                                 // velocities[2] = { linearVelocityB.x, linearVelocityB.y, linearVelocityB.z, angularVelocityB.z }
        uint32_t subData[4]; // 48 64
                             // subData = { nextContainerA, nextContainerB, indexGraph, is need to swap actors }
    };

    struct PeGpuDataSimContactsContainer // 32
    {
        float dynamicFriction;      // 0 4
        float staticFriction;       // 4 8
        float bounciness;           // 8 12
        int32_t actorA;            // 12 16
        int32_t actorB;            // 16 20
        uint32_t countContacts;     // 20 24
        uint32_t indexFirstContact; // 24 28
        uint32_t nextContainer;     // 28 32
    };

    struct PeGpuDataSimContact // 48
    {
        mth::vec4 positionA;  // 0 16
        mth::vec4 positionB;  // 16 32
        mth::vec3 normalAtoB; // 32 44
        float depth;          // 44 48
    };

    struct PeGpuDataSimActor // 32
    {
        uint32_t indexMainActor;            //0 4
        uint32_t countContainers;           //4 8
        uint32_t indexFirstContainer;       //8 12
        int32_t indexStartLoopContainers;   //12 16
        uint32_t isStartLikeActorB;         //16 20
        uint32_t flags;                     //20 24
        uint32_t freeMemory_1;              //24 28
        uint32_t freeMemory_2;              //28 32
    };

    struct PeGpuDataActorGraphNode // 8
    {
        uint32_t countEdges;               // 0 4
        uint32_t indexFirstEdge;           // 4 8
        uint32_t haveContactWithKinematic; // 8 12
        uint32_t use;                      // 12 16
    };

    struct PeGpuDataActorGraphEdge // 16
    {
        uint32_t nodeA;              // 0 4
        uint32_t nodeB;              // 4 8
        uint32_t totalCountContacts; // 8 12
        float totalForceContacts;    // 12 16
    };

    struct PeGpuDataCluster // 8 -> 16
    {
        uint32_t countNodes;     // 0 4
        uint32_t firstIndexNode; // 4 8
        uint32_t freeMemory;     // 8 12
        uint32_t freeMemory_1;   // 8 12
    };

    struct PeGpuDataImpulseNode
    {
        mth::vec3 linearImpulse;
        float free_memory_0;
        mth::vec3 angularImpulse;
        int next;
    };
}

#endif