#ifndef PE_PHYSICS_DEBUG_FEEDBACK_SYSTEM_H
#define PE_PHYSICS_DEBUG_FEEDBACK_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"
#include "physicsEngine/peSceneSettingsSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "peGeneratePairsActorsSystem.h"

namespace phys
{
    class PePhysicsDebugFeedbackSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeGpuStaticReaderSystem *staticReader;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeReallocateDatasSystem *reallocateDatasSystem;
        PeGeneratePairsActorsSystem *generatePairsActorsSystem;

        PeGpuStaticReaderSystem::ReadMemory *pReadOrientationActors;
        PeGpuStaticReaderSystem::ReadMemory *pReadVelsActors;
        PeGpuStaticReaderSystem::ReadMemory *pReadSleepingActors;

        PeGpuStaticReaderSystem::ReadMemory *pReadContainers;
        PeGpuStaticReaderSystem::ReadMemory *pReadGraphEdges;

        PeGpuStaticReaderSystem::ReadMemory *pReadContacts;

    public:
        PePhysicsDebugFeedbackSystem();

        virtual ~PePhysicsDebugFeedbackSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        void buildReceiveActors(PeSceneSimulationData *_simulationData);

        void buildReceiveContainers(PeSceneSimulationData *_simulationData, bool _contacts);

    };
}

#endif