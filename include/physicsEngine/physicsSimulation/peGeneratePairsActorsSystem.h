#ifndef PE_GENERATE_PAIRS_ACTORS_SYSTEM_H
#define PE_GENERATE_PAIRS_ACTORS_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "foundation/phEasyStack.h"

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"
#include "physicsEngine/peSceneSettingsSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "pePhysicsSimulationStructs.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"
#include "physicsEngine/materialDescription/peMaterialsDescriptionSystem.h"
#include "physicsEngine/contacts/peContactsSystem.h"

#include "peGenerateSimulationDataSystem.h"

namespace phys
{
    class PeGeneratePairsActorsSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeGpuStaticReaderSystem *staticReader;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeReallocateDatasSystem *reallocateDatasSystem;
        PeSceneSettingsSystem *sceneSettings;

        PeGenerateSimulationDataSystem *generateSimulationDataSystem;

        struct
        {
            PeGpuBuffer *lockedActorBuffer;
            PeGpuBuffer *usedEdgeBuffer;

            PeGpuBuffer *indexesRootEdgeBuffer;

            PeGpuCounterImageUint *indexesRootEdgeCounter;

            PeGpuBuffer *rootEdgesIDB;
        } buffers;

        struct
        {
            vk::ComputePipeline defineRootEdgesP;
            VkPipelineLayout defineRootEdgesL;
            vk::ShaderAutoParameters *defineRootEdgesS;

            vk::ComputePipeline setupRootEdgesP;
            VkPipelineLayout setupRootEdgesL;
            vk::ShaderAutoParameters *setupRootEdgesS;

            vk::ComputePipeline generateSplitedPairsP;
            VkPipelineLayout generateSplitedPairsL;
            vk::ShaderAutoParameters *generateSplitedPairsS;
        } shaders;

        uint32_t countGraphs;

    public:
        PeGeneratePairsActorsSystem();

        virtual ~PeGeneratePairsActorsSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        PeGpuBuffer *getUsedEdgeBuffer() { return buffers.usedEdgeBuffer; }

        uint32_t getCountGraphs() { return countGraphs; }
    };
}

#endif