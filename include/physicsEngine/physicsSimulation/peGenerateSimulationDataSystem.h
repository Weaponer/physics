#ifndef PE_GENERATE_SIMULATION_DATA_SYSTEM_H
#define PE_GENERATE_SIMULATION_DATA_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "foundation/phEasyStack.h"

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "pePhysicsSimulationStructs.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"
#include "physicsEngine/materialDescription/peMaterialsDescriptionSystem.h"
#include "physicsEngine/contacts/peContactsSystem.h"

namespace phys
{
    class PeGenerateSimulationDataSystem : public PeSystem
    {
    public:
        struct SimulationData
        {
            PeGpuBuffer *simActorsBuffer;
            PeGpuBuffer *commonSimContainersBuffer;
            PeGpuBuffer *commonSimContactModelsBuffer;

            PeGpuBuffer *graphEdgeBuffer;
            PeGpuBuffer *containerLoopsDataBuffer;
            PeGpuBuffer *actorIndexesContainersBuffer;

            PeGpuBuffer *actorsUpdateIndexesBuffer;
            PeGpuBuffer *dynamicDataUpdateIndexesBuffer;

            PeContactsSystem::ResultContainersData containersData;

            uint32_t countUpdateActors;
        };

    private:
        PeGpuData *gpuData;
        PeGpuStaticReaderSystem *staticReader;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeReallocateDatasSystem *reallocateDatasSystem;

        PeShapeDescriptionSystem *shapeDescriptionSystem;
        PeMaterialsDescriptionSystem *materialsDescriptionSystem;
        PeContactsSystem *contactsSystem;

        struct
        {
            PeGpuBuffer *actorsUpdateIndexesBuffer;
            PeGpuBuffer *dynamicDataUpdateIndexesBuffer;

            PeGpuBuffer *commonSimContainersBuffer;
            PeGpuBuffer *commonSimContactModelsBuffer;

            PeGpuBuffer *simContainerNodesBuffer;
            PeGpuBuffer *actorHeadContainersBuffer;

            PeGpuCounterImageUint *simContainerNodesCounter;

            PeGpuBuffer *containerLoopsDataBuffer;
            PeGpuBuffer *actorIndexesContainersBuffer;
            PeGpuBuffer *simActorsBuffer;

            PeGpuCounterImageUint *indexesContainersCounter;

            PeGpuBuffer *graphEdgeBuffer;
        } buffers;

        struct
        {
            vk::ComputePipeline initContainersCommonDataP;
            VkPipelineLayout initContainersCommonDataL;
            vk::ShaderAutoParameters *initContainersCommonDataS;

            vk::ComputePipeline generateContainersListP;
            VkPipelineLayout generateContainersListL;
            vk::ShaderAutoParameters *generateContainersListS;

            vk::ComputePipeline generateActorsDataP;
            VkPipelineLayout generateActorsDataL;
            vk::ShaderAutoParameters *generateActorsDataS;

            vk::ComputePipeline generateEdgesDataP;
            VkPipelineLayout generateEdgesDataL;
            vk::ShaderAutoParameters *generateEdgesDataS;
        } shaders;

        uint32_t countUpdateActors;

    public:
        PeGenerateSimulationDataSystem();

        virtual ~PeGenerateSimulationDataSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        SimulationData getSimulationData();
    };
}

#endif