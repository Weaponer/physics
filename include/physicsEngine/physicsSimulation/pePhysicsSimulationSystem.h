#ifndef PE_PHYSICS_SIMULATION_SYSTEM_H
#define PE_PHYSICS_SIMULATION_SYSTEM_H

#include "foundation/phContext.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "peGenerateSimulationDataSystem.h"
#include "peGeneratePairsActorsSystem.h"
#include "pePhysicsSimulationSystemLCP.h"
#include "pePhysicsSimulationSystemPBD.h"
#include "pePhysicsDebugFeedbackSystem.h"


#include "physicsEngine/common/peGpuData.h"

namespace phys
{
    class PePhysicsSimulationSystem : public PeSystem
    {
    private:
        PhContext physicsSimulationContext;

        PeGenerateSimulationDataSystem generateSimulationDataSystem;
        PeGeneratePairsActorsSystem generatePairsActorsSystem;
        PePhysicsSimulationSystemLCP physicsSimulationSystemLCP;
        PePhysicsSimulationSystemPBD physicsSimulationSystemPBD;
        PePhysicsDebugFeedbackSystem physicsDebugFeedbackSystem;

        PeGpuData *gpu;
        PeSceneSettingsSystem *sceneSettings;

    public:
        PePhysicsSimulationSystem();

        virtual ~PePhysicsSimulationSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

    private:
        bool useSolverLCP();
    };
}

#endif