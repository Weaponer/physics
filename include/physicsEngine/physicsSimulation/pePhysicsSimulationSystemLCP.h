#ifndef PE_PHYSICS_SIMULATION_SYSTEM_LCP_H
#define PE_PHYSICS_SIMULATION_SYSTEM_LCP_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "foundation/phEasyStack.h"

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"
#include "physicsEngine/peSceneSettingsSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "pePhysicsSimulationStructs.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"
#include "physicsEngine/materialDescription/peMaterialsDescriptionSystem.h"
#include "physicsEngine/contacts/peContactsSystem.h"

#include "peGenerateSimulationDataSystem.h"
#include "peGeneratePairsActorsSystem.h"

namespace phys
{
    class PePhysicsSimulationSystemLCP : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeGpuStaticReaderSystem *staticReader;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeReallocateDatasSystem *reallocateDatasSystem;
        PeSceneSettingsSystem *sceneSettings;

        PeGenerateSimulationDataSystem *generateSimulationDataSystem;
        PeGeneratePairsActorsSystem *generatePairsActorsSystem;
        
        struct
        {
            PeGpuBuffer *containerVelocitiesBuffers[2];
            PeGpuBuffer *contactImpulsesBuffers[2];

            PeGpuBuffer *simContainersFinishedBuffer;
            PeGpuBuffer *simContactsModelsBuffer;
            PeGpuBuffer *updateDynamicDataBuffer;

            PeGpuCounterImageUint *errorStopFlag;
        } buffers;

        struct
        {
            vk::ComputePipeline accelerationInitLCPP;
            VkPipelineLayout accelerationInitLCPL;
            vk::ShaderAutoParameters *accelerationInitLCPS;

            vk::ComputePipeline initSimContainersDataLCPP;
            VkPipelineLayout initSimContainersDataLCPL;
            vk::ShaderAutoParameters *initSimContainersDataLCPS;

            vk::ComputePipeline solveContainerContactsP;
            VkPipelineLayout solveContainerContactsL;
            vk::ShaderAutoParameters *solveContainerContactsS;

            vk::ComputePipeline simulateActorsLCPP;
            VkPipelineLayout simulateActorsLCPL;
            vk::ShaderAutoParameters *simulateActorsLCPS;
        } shaders;

        bool error;
        int lastVelocitiesWrite;

    public:
        PePhysicsSimulationSystemLCP();

        virtual ~PePhysicsSimulationSystemLCP() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);
    };
}

#endif