#ifndef PE_PHYSICS_SIMULATION_SYSTEM_PBD_H
#define PE_PHYSICS_SIMULATION_SYSTEM_PBD_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "foundation/phEasyStack.h"

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"
#include "physicsEngine/peSceneSettingsSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "pePhysicsSimulationStructs.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"
#include "physicsEngine/materialDescription/peMaterialsDescriptionSystem.h"
#include "physicsEngine/contacts/peContactsSystem.h"

#include "peGenerateSimulationDataSystem.h"
#include "peGeneratePairsActorsSystem.h"

namespace phys
{
    class PePhysicsSimulationSystemPBD : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeGpuStaticReaderSystem *staticReader;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeReallocateDatasSystem *reallocateDatasSystem;
        PeSceneSettingsSystem *sceneSettings;

        PeGenerateSimulationDataSystem *generateSimulationDataSystem;
        PeGeneratePairsActorsSystem *generatePairsActorsSystem;
        
        struct
        {
            PeGpuBuffer *containerVelocitiesBuffers[2];

            PeGpuBuffer *simContainersEPBDbuffer;
            PeGpuBuffer *updateDynamicDataBuffer;
            PeGpuBuffer *orientaionTempBuffer;

            PeGpuCounterImageUint *errorStopFlag;
        } buffers;

        struct
        {
            vk::ComputePipeline simulateActorsPBD_initP;
            VkPipelineLayout simulateActorsPBD_initL;
            vk::ShaderAutoParameters *simulateActorsPBD_initS;

            vk::ComputePipeline simulateActorsPBD_updateP;
            VkPipelineLayout simulateActorsPBD_updateL;
            vk::ShaderAutoParameters *simulateActorsPBD_updateS;

            vk::ComputePipeline fillSimCotainersDataPBDP;
            VkPipelineLayout fillSimCotainersDataPBDL;
            vk::ShaderAutoParameters *fillSimCotainersDataPBDS;

            vk::ComputePipeline solveContactsPBDP;
            VkPipelineLayout solveContactsPBDL;
            vk::ShaderAutoParameters *solveContactsPBDS;
        } shaders;

        int lastVelocitiesWrite;

    public:
        PePhysicsSimulationSystemPBD();

        virtual ~PePhysicsSimulationSystemPBD() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);
    };
}

#endif