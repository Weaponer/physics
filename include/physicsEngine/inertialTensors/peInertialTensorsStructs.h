#ifndef PE_INERTIAL_TENSORS_STRUCTS_H
#define PE_INERTIAL_TENSORS_STRUCTS_H

#include <MTH/matrix.h>

namespace phys
{
    struct PeGpuDataActorDataForTensors // 32
    {
        mth::vec3 centerOfMass; // 0 12
        float mass;             // 12 16
        float totalVolume;      // 16 20
        uint32_t indexActor;    // 20 24
        uint32_t free_0;
        uint32_t free_1;
    };

    struct PeGpuGeometriesDataForTensors // 16
    {
        uint32_t startIndexToIndexesGeomtries; // 0 4
        uint32_t countIndexesGeometries;       // 4 8
        uint32_t free_0;
        uint32_t free_1;
    };

    struct PeGpuIndexToGeometry // 16
    {
        uint32_t index;     // 0 4
        float volumeFactor; // 4 8
        uint32_t free_0;
        uint32_t free_1;
    };
}

#endif