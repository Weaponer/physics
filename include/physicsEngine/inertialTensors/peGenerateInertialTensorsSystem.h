#ifndef PE_GENERATE_INERTIAL_TENSORS_SYSTEM
#define PE_GENERATE_INERTIAL_TENSORS_SYSTEM

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "foundation/phEasyStack.h"

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/actorDescription/peActorDynamicDescription.h"
#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"

#include "geometry/internal/inBoxGeometry.h"

#include "peInertialTensorsStructs.h"

namespace phys
{
    class PeGenerateInertialTensorsSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeGpuStaticReaderSystem *staticReader;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeReallocateDatasSystem *reallocateDatasSystem;
        PeActorDynamicDescription *actorDynamicDescription;
        PeShapeDescriptionSystem *shapeDescriptionDescription;

        uint32_t countElementsInBuffers;

        PeGpuObjectType dataActorsType;
        PeGpuObjectType dataGeometryType;
        PeGpuObjectType indexesGeometryType;

        PhEasyStack<PeGpuGeometriesDataForTensors> *geometryListsCaches[PeConstantsUtils::PeComputeConvexGeometry_CountAll];
        PhEasyStack<PeGpuIndexToGeometry> *indexesGeometriesCaches[PeConstantsUtils::PeComputeConvexGeometry_CountAll];

        struct
        {
            PeGpuObjectsBuffer *inputDataActorsBuffer;
            PeGpuObjectsBuffer *inputDataGeometryBuffer[PeConstantsUtils::PeComputeConvexGeometry_CountAll];
            PeGpuObjectsBuffer *inputIndexesGeometryBuffer[PeConstantsUtils::PeComputeConvexGeometry_CountAll];

            PeGpuBuffer *resultInertialTensorsBuffer;
        } buffers;

        struct
        {
            vk::ComputePipeline calculateInertialTensorP[PeConstantsUtils::PeComputeConvexGeometry_CountAll];
            VkPipelineLayout calculateInertialTensorL[PeConstantsUtils::PeComputeConvexGeometry_CountAll];
            vk::ShaderAutoParameters *calculateInertialTensorS[PeConstantsUtils::PeComputeConvexGeometry_CountAll];

            vk::ComputePipeline tensorsSumatorP;
            VkPipelineLayout tensorsSumatorL;
            vk::ShaderAutoParameters *tensorsSumatorS;
        } shaders;

        uint32_t countIterations;
        uint32_t countUpdate;
        
        PeGpuStaticReaderSystem::ReadMemory *pReadTensors;

    public:
        PeGenerateInertialTensorsSystem();

        virtual ~PeGenerateInertialTensorsSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

    private:
        PeConstantsUtils::PeComputeConvexGeometry getIndexTypeGeometry(InGeometryBase *_geometry);
    };
}

#endif