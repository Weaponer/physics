#ifndef PE_INERTIAL_TENSORS_SYSTEM_H
#define PE_INERTIAL_TENSORS_SYSTEM_H

#include "foundation/phContext.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/common/peGpuData.h"

#include "peGenerateInertialTensorsSystem.h"

namespace phys
{
    class PeInertialTensorsSystem : public PeSystem
    {
    private:
        PhContext inertialTensorsContext;

        PeGenerateInertialTensorsSystem generateInertialTensorsSystem;

        PeGpuData *gpu;

    public:
        PeInertialTensorsSystem();

        virtual ~PeInertialTensorsSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);
    };
}

#endif