#ifndef PE_SCENE_SIMULATOR_H
#define PE_SCENE_SIMULATOR_H

#include "common/phBase.h"

#include "foundation/phContext.h"

#include "gpuCommon/vulkanDevice.h"

#include "physics/internal/inScene.h"

#include "common/peGpuData.h"

#include "peShaderProvider.h"
#include "peMemoryBarrierContollerSystem.h"
#include "peGpuStaticLoaderSystem.h"
#include "peGpuStaticReaderSystem.h"
#include "peGrabObjectsDatasSystem.h"
#include "peReallocateDatasSystem.h"
#include "peSceneSettingsSystem.h"
#include "peResultReceiverSystem.h"
#include "peQueueCommandsBuilderSystem.h"

#include "materialDescription/peMaterialsDescriptionSystem.h"
#include "shapeDescription/peShapeDescriptionSystem.h"

#include "actorDescription/peSceneActorsDescription.h"
#include "actorDescription/peActorDynamicDescription.h"
#include "actorDescription/peActorShapesDescription.h"

#include "geometryObbDescription/peGeometryObbDescription.h"

#include "potentialContacts/pePotentialContactsSystem.h"
#include "tableContacts/peTableContactsSystem.h"
#include "contacts/peContactsSystem.h"
#include "inertialTensors/peInertialTensorsSystem.h"
#include "physicsSimulation/pePhysicsSimulationSystem.h"

#include "peFetchSceneData.h"

namespace phys
{
    class PeFoundation;
    class PeSceneSimulator : protected PhBase
    {
    private:
        PeFoundation *foundation;
        vk::VulkanDevice *device;

        PhContext simulationContext;
        PeGpuData gpuData;
        PeShaderProvider shaderProvider;
        PeMemoryBarrierControllerSystem memoryBarrierControllerSystem;
        PeGpuStaticLoaderSystem staticLoaderSystem;
        PeGpuStaticReaderSystem staticReaderSystem;
        PeGrabObjectsDatasSystem grabObjectsDatasSystem;
        PeReallocateDatasSystem reallocateDatasSystem;
        PeSceneSettingsSystem sceneSettingsSystem;
        PeResultReceiverSystem resultReceiverSystem;
        PeQueueCommandsBuilderSystem queueCommandsBuilderSystem;

        PeMaterialsDescriptionSystem materialDescriptionSystem;
        PeShapeDescriptionSystem shapeDescriptionSystem;

        PeSceneActorsDescription sceneActrosDescription;
        PeActorDynamicDescription actorDynamicDescription;
        PeActorShapesDescription actorShapesDescription;

        PeGeometryObbDescription geometryObbDescription;

        PePotentialContactsSystem potentialContactsSystem;
        PeTableContactsSystem tableContactsSystem;
        PeContactsSystem contactsSystem;
        PeInertialTensorsSystem inertialTensorsSystem;
        PePhysicsSimulationSystem physicsSimulationSystem;

        PeFetchSceneData fetchSceneData;

        InScene *sceneInSimulation;

    public:
        PeSceneSimulator(PeFoundation *_foundation);

        virtual void release() override;

        void addScene(InScene *_scene);

        void removeScene(InScene *_scene);

        void simulate(InScene *_scene, float _deltaTime, const PhSimulationSettings &_settings);

        bool fetchScene(InScene *_scene, bool _wait);

    private:
        PeSceneSimulationData *generateSceneSimulationData();

        void resetSimulationMemoryData(PeSceneSimulationData *_sceneSimulationData);

        void lockMainManagers();

        void unlockMainManagers();
    };
}

#endif