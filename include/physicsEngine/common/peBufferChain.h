#ifndef PE_BUFFER_CHAIN_H
#define PE_BUFFER_CHAIN_H

#include <vulkan/vulkan.h>

#include "common/phBase.h"

namespace phys
{
    class PeBufferChain : public PhBase
    {
    public:
        struct Segment
        {
            uint32_t count;
            uint32_t firstIndex;
        };

    private:
        struct SegmentNode
        {
            uint32_t count;
            uint32_t index;
            bool free;
            SegmentNode *next;
            SegmentNode *back;
        };

    private:
        SegmentNode *begin;
        SegmentNode *freeSegments;

    public:
        PeBufferChain();

        virtual ~PeBufferChain() {}

        virtual void release() override;

        Segment allocateSegment(uint32_t _count);

        Segment reallocateSegment(Segment _segment, uint32_t _newCount);

        void freeSegment(Segment _segment);

    private:
        bool tryFindFreeSegment(uint32_t _minCount, SegmentNode **_outEndSegment);

        void tryCompound(SegmentNode *_segment);

        void addSegmentAfter(SegmentNode *_main, SegmentNode *_next);

        SegmentNode *getFreeSegment(uint32_t _count = 0, uint32_t _index = 0, bool _free = false);

        void returnSegment(SegmentNode *_segment);

        void clearListSegments(SegmentNode *_begin);
    };
}

#endif