#ifndef PE_GPU_DATA_H
#define PE_GPU_DATA_H

#include "common/phBase.h"

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/memory/vulkanImageAllocator.h"

#include "gpuCommon/specialCommands/vulkanComputeCommands.h"
#include "gpuCommon/specialCommands/vulkanCommandReceiver.h"

#include "physicsEngine/peDebugMarkerColors.h"

namespace phys
{
    class PeGpuData : public PhBase
    {
    public:
        vk::VulkanDevice *device;
        vk::VulkanQueue *queue;
        vk::VulkanBufferAllocator *bufferAllocator;
        vk::VulkanImageAllocator *imageAllocator;
        uint32_t mainFamilyQueue;

        vk::VulkanComputeCommands *computeCommands;
        vk::VulkanCommandReceiver *commandReceiver;
    };
}

#endif