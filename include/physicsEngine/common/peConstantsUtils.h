#ifndef PE_CONSTANTS_UTILS_H
#define PE_CONSTANTS_UTILS_H

#include "physicsEngine/peGpuStructures.h"

namespace phys
{
    class PeConstantsUtils
    {
    public:
        enum PeComputeShaders : PhFlags
        {
            PeCompute_UpdateGeometryOBBs,
            PeCompute_BuildBoundOBBs,
            PeCompute_CalculateAverageOBBs,
            PeCompute_CalculateDispersionOBBs,
            PeCompute_CheckContactsBoundOBBs,
            PeCompute_RebalanceBoundOBBs,

            PeCompute_KdTreeNodeBuilder,
            PeCompute_KdTreeNodeOffsetsBuilder,
            PeCompute_LinkedListBuilder,
            PeCompute_SegmentsBuilder,

            PeCompute_FillNewIndexes,
            PeCompute_UpdateSwapTable,
            PeCompute_GenerateListContacts,
            PeCompute_GenerateLastListContacts,
            PeCompute_GenerateListContactsPlane,
            PeCompute_FillTableContacts,
            PeCompute_PotentialContactsCollector,
            PeCompute_GenerateDistapchGroupByCounter,
            PeCompute_CheckGeometriesContact,
            PeCompute_ContactsContainerExchanger,
            PeCompute_GenerateDispatchUpdateContacts,
            PeCompute_ContactsContainerDistributor,
            PeCompute_CheckLostContainer,
            PeCompute_CopyContacts,

            PeCompute_ContactsGenerator,
            PeCompute_ContactsUpdate,
            PeCompute_ContainersCounter,
            PeCompute_ContactsUniter,

            PeCompute_TensorGenerator,
            PeCompute_TensorSumator,

            PeCompute_InitContainersCommonData,
            PeCompute_GenerateContainersList,
            PeCompute_GenerateActorsData,
            PeCompute_GenerateEdgesData,
            PeCompute_DefineRootEdges,
            PeCompute_SetupRootEdges,
            PeCompute_GenerateSplitedPairs,

            PeCompute_AccelerationInitLCP,
            PeCompute_InitSimContainersDataLCP,
            PeCompute_SolveContainerContacts,
            PeCompute_SimulateActorsLCP,

            PeCompute_SimulateActorsPBD_Init,
            PeCompute_SimulateActorsPBD_Update,
            PeCompute_FillSimCotainersDataPBD,
            PeCompute_SolveContactsPBD,
            
            PeCompute_CountAll,
        };

        enum PeComputeTypeContact : PhFlags
        {
            PeComputeTypeContact_PlaneBox,
            PeComputeTypeContact_PlaneSphere,
            PeComputeTypeContact_PlaneCapsule,
            PeComputeTypeContact_BoxBox,
            PeComputeTypeContact_BoxSphere,
            PeComputeTypeContact_BoxCapsule,
            PeComputeTypeContact_SphereSphere,
            PeComputeTypeContact_SphereCapsule,
            PeComputeTypeContact_CapsuleCapsule,

            PeComputeTypeContact_CountAll,
        };

        enum PeComputeConvexGeometry : PhFlags 
        {
            PeComputeConvexGeometry_Sphere,
            PeComputeConvexGeometry_Box,
            PeComputeConvexGeometry_Capsule,

            PeComputeConvexGeometry_CountAll,
        };

    public:
        static const char *getShaderName(PeComputeShaders _shader);

        static bool isHaveTypeContactVariants(PeComputeShaders _shader);

        static bool isHaveConvexGeometryVariants(PeComputeShaders _shader);

        static const char *getShaderNameTypeContact(PeComputeShaders _shader, PeComputeTypeContact _typeContact);

        static const char *getShaderNameConvexGeometry(PeComputeShaders _shader, PeComputeConvexGeometry _geometry);

        static uint32_t *getCountContactsInContainers();

        static uint32_t (*getTypeAndCountContacts())[2];
    };
}

#endif