#ifndef PE_GPU_BARRIER_H
#define PE_GPU_BARRIER_H

#include "common/phFlags.h"

#include "peGpuData.h"

namespace phys
{

    enum PeStageData : PhFlags
    {
        PeStageData_Unknow = 0,
        PeStageData_TransferRead = 1,
        PeStageData_TransferWrite = 2,
        PeStageData_ShaderRead = 3,
        PeStageData_ShaderWrite = 4,
        PeStageData_IndirectCall = 5,
    };

    template <uint32_t CountBuffers, uint32_t CountImages>
    class PeGpuBarrier
    {
    public:
    private:
        struct StateBuffer
        {
            PeStageData src;
            PeStageData dst;
            vk::VulkanBuffer *buffer;
        };

        struct StateImage
        {
            PeStageData src;
            PeStageData dst;
            vk::VulkanImage *image;
        };

    private:
        StateBuffer buffers[CountBuffers];
        StateImage images[CountImages];

    public:
        PeGpuBarrier()
        {
            for (uint32_t i = 0; i < CountBuffers; i++)
                buffers[i] = {};

            for (uint32_t i = 0; i < CountImages; i++)
                images[i] = {};
        }

        virtual ~PeGpuBarrier() {}

    private:
        VkPipelineStageFlags getPipelineStage(PeStageData _state)
        {
            const static VkPipelineStageFlags flags[5] = {
                VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT};
            return flags[(uint32_t)_state];
        }

        VkAccessFlags getAccess(PeStageData _state)
        {
            const static VkAccessFlags flags[5] = {
                VK_ACCESS_NONE,
                VK_ACCESS_TRANSFER_READ_BIT,
                VK_ACCESS_TRANSFER_WRITE_BIT,
                VK_ACCESS_SHADER_READ_BIT,
                VK_ACCESS_SHADER_WRITE_BIT};
            return flags[(uint32_t)_state];
        }

        VkImageLayout getImageLayout(PeStageData _state)
        {
            const static VkImageLayout flags[5] = {
                VK_IMAGE_LAYOUT_UNDEFINED,
                VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                VK_IMAGE_LAYOUT_GENERAL,
                VK_IMAGE_LAYOUT_GENERAL};
            return flags[(uint32_t)_state];
        }

    public:
        void setBufferState(uint32_t _index, vk::VulkanBuffer *_buffer, PeStageData _src, PeStageData _dst)
        {
            StateBuffer buffer;
            buffer.buffer = _buffer;
            buffer.src = _src;
            buffer.dst = _dst;
            buffers[_index] = buffer;
        }

        void setImageState(uint32_t _index, vk::VulkanImage *_image, PeStageData _src, PeStageData _dst)
        {
            StateImage image;
            image.image = _image;
            image.src = _src;
            image.dst = _dst;
            images[_index] = image;
        }

        void setupBarrier(const PeGpuData *_gpu, PeStageData _srcStage, PeStageData _dstStage)
        {
            VkBufferMemoryBarrier bufferBarriers[CountBuffers];
            for (uint32_t i = 0; i < CountBuffers; i++)
            {
                VkBufferMemoryBarrier bufferB{};
                bufferB.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
                bufferB.offset = 0;
                bufferB.size = VK_WHOLE_SIZE;
                bufferB.buffer = *(buffers[i].buffer);
                bufferB.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                bufferB.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                bufferB.srcAccessMask = getAccess(buffers[i].src);
                bufferB.dstAccessMask = getAccess(buffers[i].dst);

                bufferBarriers[i] = bufferB;
            }

            VkImageMemoryBarrier imageBarriers[CountImages];
            for (uint32_t i = 0; i < CountImages; i++)
            {
                vk::VulkanImage *image = images[i].image;

                VkImageMemoryBarrier imageB{};
                imageB.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                imageB.image = *image;
                imageB.subresourceRange.aspectMask = image->getAspect();
                imageB.subresourceRange.baseArrayLayer = 0;
                imageB.subresourceRange.baseMipLevel = 0;
                imageB.subresourceRange.layerCount = image->getLayers();
                imageB.subresourceRange.levelCount = image->getCountMipmaps();
                imageB.srcAccessMask = getAccess(images[i].src);
                imageB.dstAccessMask = getAccess(images[i].dst);
                imageB.oldLayout = getImageLayout(images[i].src);
                imageB.newLayout = getImageLayout(images[i].dst);
                imageB.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                imageB.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

                imageBarriers[i] = imageB;
            }

            _gpu->computeCommands->pipelineBarrier(getPipelineStage(_srcStage), getPipelineStage(_dstStage), 0, 0, NULL,
                                                   CountBuffers, bufferBarriers, CountImages, imageBarriers);
        }
    };
}

#endif