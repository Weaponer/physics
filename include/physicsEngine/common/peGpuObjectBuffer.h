#ifndef PE_GPU_OBJECT_BUFFER_H
#define PE_GPU_OBJECT_BUFFER_H

#include "gpuCommon/memory/vulkanBuffer.h"

#include "peGpuObjectsData.h"

namespace phys
{
    class PeGpuObjectBuffer : public PeGpuObjectsData
    {
    public:
        PeGpuObjectBuffer(const char *_name) : PeGpuObjectsData(_name)
        {
        }

        PeGpuObjectBuffer(const char *_name, const PeGpuData *_gpuData, PeGpuObjectType *_typeDescription);
        
        virtual ~PeGpuObjectBuffer() {}

        void allocateData();

    public:
        virtual void release() override;
    };
}

#endif