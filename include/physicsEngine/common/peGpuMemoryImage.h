#ifndef PE_GPU_MEMORY_IMAGE_H
#define PE_GPU_MEMORY_IMAGE_H

#include "peGpuMemory.h"

namespace phys
{
    class PeGpuMemoryImage : public PeGpuMemory
    {
    protected:
        vk::VulkanImage *image;
        vk::VulkanImageView view;

    public:
        PeGpuMemoryImage(const char *_name) : PeGpuMemory(_name), image(NULL) {}

        virtual ~PeGpuMemoryImage() {}

        inline vk::VulkanImage *getImage() { return image; }

        inline vk::VulkanImageView getImageView() { return view; }

        inline VkImageType getImageType() const
        {
            return image->getImageType();
        }

        inline VkFormat getFormat() const
        {
            return image->getFormat();
        }

        inline uint32_t getCountLayers() const
        {
            return image->getLayers();
        }

        inline uint32_t getWidth() const
        {
            return image->getWidth();
        }

        inline uint32_t getHeight() const
        {
            return image->getHeight();
        }

        operator vk::VulkanImage *() { return image; }

        operator vk::VulkanImageView() { return view; }
    };
}

#endif