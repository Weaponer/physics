#ifndef PE_GPU_MEMORY_H
#define PE_GPU_MEMORY_H

#include <vulkan/vulkan.h>
#include <string>

#include "peGpuBarrier.h"
#include "peGpuData.h"
#include "peGpuDescription.h"

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/memory/vulkanImageAllocator.h"

#include "common/phBase.h"

namespace phys
{
    enum PeTypeStageData : PhFlags
    {
        PeTypeStage_Unknow = 0,
        PeTypeStage_Transfer = 1,
        PeTypeStage_Compute = 2,
        PeTypeStage_IndirectCallCompute = 3,
    };

    enum PeTypeAccessData : PhFlags
    {
        PeTypeAccess_Unknow = 0,
        PeTypeAccess_Write = 1,
        PeTypeAccess_Read = 2,
        PeTypeAccess_IndirectCallRead = 3,
    };

    class PeGpuMemory : public PhBase
    {
    public:
        struct Info
        {
            vk::VulkanDevice *pDevice;
            vk::VulkanBufferAllocator *pBufferAllocator;
            vk::VulkanImageAllocator *pImageAllocator;
            uint32_t mainFamilyIndex;
        };

    protected:
        std::string name;
        PeTypeStageData lastStage;
        PeTypeAccessData lastAccess;
        uint32_t numLastStep;

        uint32_t mainFamilyIndex;
        vk::VulkanDevice *device;
        vk::VulkanBufferAllocator *bufferAllocator;
        vk::VulkanImageAllocator *imageAllocator;

    public:
        PeGpuMemory(const char *_name) : PhBase(), name(_name), lastStage(PeTypeStage_Unknow),
                                         lastAccess(PeTypeAccess_Unknow), numLastStep(0) {}

        virtual ~PeGpuMemory() {}

        void init(const Info &_info)
        {
            mainFamilyIndex = _info.mainFamilyIndex;
            device = _info.pDevice;
            bufferAllocator = _info.pBufferAllocator;
            imageAllocator = _info.pImageAllocator;
        }

        void init(const PeGpuData *gpuData)
        {
            Info info{};
            info.mainFamilyIndex = gpuData->mainFamilyQueue;
            info.pBufferAllocator = gpuData->bufferAllocator;
            info.pImageAllocator = gpuData->imageAllocator;
            info.pDevice = gpuData->device;
            init(info);
        }

        const char *getName() const { return name.c_str(); }

        PeTypeStageData getLastStage() const { return lastStage; }

        PeTypeAccessData getLastAccess() const { return lastAccess; }

        uint32_t getNumLastStep() const { return numLastStep; }

        void setLastStage(PeTypeStageData _stage) { lastStage = _stage; }

        void setLastAccess(PeTypeAccessData _access) { lastAccess = _access; }

        void setNumLastStep(uint32_t _numLastStep) { numLastStep = _numLastStep; }
    };
}

#endif