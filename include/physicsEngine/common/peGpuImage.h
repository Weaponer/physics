#ifndef PE_GPU_IMAGE_H
#define PE_GPU_IMAGE_H

#include "peGpuMemoryImage.h"

namespace phys
{
    class PeGpuImage : public PeGpuMemoryImage
    {
    private:
        VkImageType imageType;
        VkFormat format;
        uint32_t width;
        uint32_t height;
        uint32_t countLayers;

    public:
        PeGpuImage(const char *_name) : PeGpuMemoryImage(_name),
                                        imageType(VK_IMAGE_TYPE_1D),
                                        format(VK_FORMAT_UNDEFINED), width(0),
                                        height(0), countLayers(0)
        {
        }

        virtual ~PeGpuImage() {}

        void allocateData(VkImageType _imageType, VkFormat _format, uint32_t _width, uint32_t _height,
                          uint32_t _countLayers, bool _hostVisibility = false);

    public:
        virtual void release() override;
    };
}

#endif