#ifndef PE_GPU_OBJECTS_TABLE_H
#define PE_GPU_OBJECTS_TABLE_H

#include <vulkan/vulkan.h>

#include "peGpuDescription.h"

#include "common/phBase.h"

#include "foundation/phTableContainer.h"

namespace phys
{
    class PeGpuObjectsTable : public PhBase
    {
    protected:
        PeGpuObjectType typeDescription;
        PhTableContainer<bool> tableObjects;

    public:
        PeGpuObjectsTable(PeGpuObjectType _typeDescription) : PhBase(), typeDescription(_typeDescription), tableObjects()
        {
            bool def = false;
            tableObjects.setUseDefault(true);
            tableObjects.setDefault(&def);
        }

        virtual ~PeGpuObjectsTable() {}

        virtual void release() override
        {
            tableObjects.release();
        }

        uint32_t allocateIndex()
        {
            if (tableObjects.getCountUse() >= typeDescription.getMaxCountObjects())
            {
                return typeDescription.getDefaultDescription();
            }
            bool def = true;
            uint32_t index = 0;
            tableObjects.assignmentMemoryAndCopy(&def, index);
            return index + typeDescription.getFirstDescription();
        }

        void freeIndex(uint32_t _index)
        {
            _index -= typeDescription.getFirstDescription();
            tableObjects.popMemory(_index);
        }
    };
}

#endif