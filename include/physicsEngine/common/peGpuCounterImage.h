#ifndef PE_GPU_COUNTER_IMAGE_H
#define PE_GPU_COUNTER_IMAGE_H

#include <vulkan/vulkan.h>

#include "foundation/phMemoryAllocator.h"

#include "peGpuMemoryImage.h"
#include "peGpuBarrier.h"

#include "common/phMacros.h"

#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/memory/vulkanImage.h"

namespace phys
{
    template <VkFormat FORMAT, typename TYPE>
    class PeGpuCounterImage : public PeGpuMemoryImage
    {
    private:
        uint32_t widthCounter;
        bool hostVisible;

        vk::VulkanBuffer *readbackBuffer;

        uint32_t sizeData;
        TYPE *data;

    public:
        void sendReadCommand(vk::VulkanComputeCommands *_computeCommands)
        {
            if (hostVisible)
                return;

            _computeCommands->copyImageToBuffer(image, readbackBuffer);
        }

        void receiveData(vk::VulkanComputeCommands *_computeCommands)
        {
            if (hostVisible)
            {
                _computeCommands->readFromImage(image, 0, sizeof(TYPE) * widthCounter, data);
            }
            else
            {
                _computeCommands->readFromBuffer(readbackBuffer, 0, sizeof(TYPE) * widthCounter, data);
            }
        }

        void clearCounter(const PeGpuData *_gpuData, TYPE _value, PeStageData _lastStage)
        {
            PeGpuBarrier<0, 1> barrier = PeGpuBarrier<0, 1>();
            barrier.setImageState(0, image, _lastStage, PeStageData_TransferWrite);
            barrier.setupBarrier(_gpuData, _lastStage, PeStageData_TransferWrite);
            VkClearColorValue value;
            value.int32[0] = value.int32[1] = value.int32[2] = value.int32[3] = (int32_t)_value;
            value.uint32[0] = value.uint32[1] = value.uint32[2] = value.uint32[3] = (uint32_t)_value;
            value.float32[0] = value.float32[1] = value.float32[2] = value.float32[3] = (float)_value;
            _gpuData->computeCommands->clearImage(image, &value);

            for (uint32_t i = 0; i < widthCounter; i++)
                data[i] = _value;
        }

        void clearCounter(const PeGpuData *_gpuData, TYPE _value)
        {
            VkClearColorValue value;
            value.int32[0] = value.int32[1] = value.int32[2] = value.int32[3] = (int32_t)_value;
            value.uint32[0] = value.uint32[1] = value.uint32[2] = value.uint32[3] = (uint32_t)_value;
            value.float32[0] = value.float32[1] = value.float32[2] = value.float32[3] = (float)_value;
            _gpuData->computeCommands->clearImage(image, &value);

            for (uint32_t i = 0; i < widthCounter; i++)
                data[i] = _value;
        }

        uint32_t getWidthCounter() const { return widthCounter; }

        bool getHostVisible() const { return hostVisible; }

        const TYPE *getData() const { return data; }

        void allocateData(uint32_t _widthCounter = 1U, bool _hostVisible = false)
        {
            const char *c_name = getName();

            widthCounter = _widthCounter;
            hostVisible = _hostVisible;

            sizeData = sizeof(TYPE) * widthCounter;
            if (hostVisible)
            {
                image = imageAllocator->createImage(vk::MemoryUseType_Long, vk::VulkanImageType_ImageDynamic, VK_IMAGE_TYPE_1D,
                                                    (VkFormat)FORMAT, VK_IMAGE_ASPECT_COLOR_BIT,
                                                    widthCounter, 1, 1, 1, 1, 1, &mainFamilyIndex, c_name);
            }
            else
            {
                image = imageAllocator->createImage(vk::MemoryUseType_Long, vk::VulkanImageType_ImageStatic, VK_IMAGE_TYPE_1D,
                                                    (VkFormat)FORMAT, VK_IMAGE_ASPECT_COLOR_BIT,
                                                    widthCounter, 1, 1, 1, 1, 1, &mainFamilyIndex, c_name);
                readbackBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_Staging,
                                                               sizeData, 1, &mainFamilyIndex, PH_CNAME(c_name, "_readback"));
            }
            data = (TYPE *)phAllocateMemory(sizeData);

            vk::VulkanImageView::createImageView(device, VK_IMAGE_VIEW_TYPE_1D, image, &view, PH_CNAME(c_name, "View"));
        }

        PeGpuCounterImage(const char *_name) : PeGpuMemoryImage(_name), widthCounter(0), hostVisible(0),
                                               readbackBuffer(NULL), sizeData(0), data(NULL) {}

        PeGpuCounterImage(const char *_name, const PeGpuData *_gpuData, uint32_t _widthCounter = 1U, bool _hostVisible = false)
            : PeGpuMemoryImage(_name), widthCounter(_widthCounter), hostVisible(_hostVisible),
              readbackBuffer(NULL), sizeData(0), data(NULL)
        {
            init(_gpuData);
            allocateData(widthCounter, hostVisible);
        }

        virtual ~PeGpuCounterImage() {}

        virtual void release() override
        {
            if (image != NULL)
            {
                vk::VulkanImageView::destroyImageView(device, view);
                imageAllocator->destroyImage(image);
                if (!hostVisible)
                {
                    bufferAllocator->destroyBuffer(readbackBuffer);
                    readbackBuffer = NULL;
                }
                phDeallocateMemory(data);
                image = NULL;
                data = NULL;
            }
        }
    };

    typedef PeGpuCounterImage<VK_FORMAT_R32_UINT, uint32_t> PeGpuCounterImageUint;
    typedef PeGpuCounterImage<VK_FORMAT_R32_SINT, int32_t> PeGpuCounterImageInt;
}

#endif