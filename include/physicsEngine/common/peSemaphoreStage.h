#ifndef PE_SEMAPHORE_STAGE_H
#define PE_SEMAPHORE_STAGE_H

#include "common/phBase.h"

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanSemaphore.h"

namespace phys
{
    class PeSemaphoreStage : public PhBase
    {
    private:
        bool useStage;
        vk::VulkanSemaphore sempahore;
        VkPipelineStageFlags waitStage;

    public:
        PeSemaphoreStage();

        PeSemaphoreStage(const char *_name, const PeSemaphoreStage &_stage);

        PeSemaphoreStage(const char *_name, vk::VulkanDevice *_device, VkPipelineStageFlags _waitStage);

        virtual ~PeSemaphoreStage() {}

        virtual void release() override;

        inline void setUseStage(bool _use)
        {
            useStage = _use;
        }

        inline bool getUseStage() const
        {
            return useStage;
        }

        vk::VulkanSemaphore getSemaphore()
        {
            return sempahore;
        }

        VkPipelineStageFlags getWaitStage()
        {
            return waitStage;
        }
    };
}

#endif