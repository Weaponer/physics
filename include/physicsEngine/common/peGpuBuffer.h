#ifndef PE_GPU_BUFFER_H
#define PE_GPU_BUFFER_H

#include "peGpuMemoryBuffer.h"

namespace phys
{
    class PeGpuBuffer : public PeGpuMemoryBuffer
    {
    public:
        PeGpuBuffer(const char *_name) : PeGpuMemoryBuffer(_name)
        {
        }

        PeGpuBuffer(const char *_name, PeGpuData *_gpu, vk::MemoryUseType _use, vk::VulkanBufferType _type, int _size);

        virtual ~PeGpuBuffer() {}

        void allocateData(vk::MemoryUseType _use, vk::VulkanBufferType _type, int _size);

    public:
        virtual void release() override;
    };
}

#endif