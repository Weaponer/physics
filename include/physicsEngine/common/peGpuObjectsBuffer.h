#ifndef PE_GPU_OBJECTS_BUFFER_H
#define PE_GPU_OBJECTS_BUFFER_H

#include "gpuCommon/memory/vulkanBuffer.h"

#include "peGpuObjectsData.h"

namespace phys
{
    class PeGpuObjectsBuffer : public PeGpuObjectsData
    {
    public:
        PeGpuObjectsBuffer(const char *_name) : PeGpuObjectsData(_name)
        {
        }

        PeGpuObjectsBuffer(const char *_name, const PeGpuData *_gpuData, PeGpuObjectType *_typeDescription, bool _hostVisibility = false);
        
        virtual ~PeGpuObjectsBuffer() {}

        void allocateData();

    public:
        virtual void release() override;
    };
}

#endif