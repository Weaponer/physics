#ifndef PE_MANAGER_CACHE_H
#define PE_MANAGER_CACHE_H

#include "common/phBase.h"

#include "foundation/phEasyStack.h"

namespace phys
{
    template <class T>
    class PeManagerCache : public PhBase
    {
    private:
        T *manager;
        PhEasyStack<typename T::ManagerEvent> bufferEvents;

    public:
        PeManagerCache()
            : PhBase(), manager(NULL), bufferEvents()
        {
        }

        virtual ~PeManagerCache(){};

        virtual void release() override
        {
            bufferEvents.release();
        }

        void updateCache(T *_manager)
        {
            bufferEvents.clearMemory();

            manager = _manager;

            typename T::ManagerEvent event;
            while (manager->getNextEvent(&event))
            {
                bufferEvents.assignmentMemoryAndCopy(&event);
            }
        }

        bool isValid() const { return manager != NULL; }

        T *getManager() const { return manager; }

        typename T::ManagerEvent *getEvents() const { return bufferEvents.getMemory(0); }

        uint32_t getCountEvents() const { return bufferEvents.getPos(); }
    };
}

#endif