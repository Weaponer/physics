#ifndef PE_GPU_TABLE_CONTACTS_BUFFER_H
#define PE_GPU_TABLE_CONTACTS_BUFFER_H

#include <vulkan/vulkan.h>

#include "gpuCommon/memory/vulkanImage.h"

#include "peGpuMemoryImage.h"

namespace phys
{
    class PeGpuTableContactsBuffer : public PeGpuMemoryImage
    {
    protected:
        uint32_t countIndexes;
        uint32_t countContactsPerIndex;
        uint32_t countLayers;
        uint32_t countIndexesInLayers;

    public:
        PeGpuTableContactsBuffer(const char *_name) : PeGpuMemoryImage(_name), countIndexes(0), countContactsPerIndex(0),
                                                      countLayers(0), countIndexesInLayers(0)
        {
        }

        PeGpuTableContactsBuffer(const char *_name, const PeGpuData *_gpuData);

        virtual ~PeGpuTableContactsBuffer() {}

        uint32_t getCountIndexes() const { return countIndexes; }

        uint32_t getCountContactsPerIndex() const { return countContactsPerIndex; }

        uint32_t getCountLayers() const { return countLayers; }

        uint32_t getCountIndexesInLayers() const { return countIndexesInLayers; }

        void allocateData(uint32_t _countIndexes, uint32_t _countContactsPerIndex, VkFormat _format);

    public:
        virtual void release() override;
    };
}

#endif