#ifndef PE_GPU_MEMORY_BUFFER_H
#define PE_GPU_MEMORY_BUFFER_H

#include "peGpuMemory.h"

namespace phys
{
    class PeGpuMemoryBuffer : public PeGpuMemory
    {
    protected:
        vk::VulkanBuffer *buffer;

    public:
        PeGpuMemoryBuffer(const char *_name) : PeGpuMemory(_name), buffer(NULL) {}

        virtual ~PeGpuMemoryBuffer() {}

        inline uint32_t getSize() const { return buffer->getSize(); }

        inline vk::VulkanBuffer *getBuffer() { return buffer; }

        operator vk::VulkanBuffer *() { return buffer; }
    };
}

#endif