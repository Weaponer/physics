#ifndef PE_GPU_DESCRIPTION_H
#define PE_GPU_DESCRIPTION_H

#include <limits>
#include <numbers>

#include <vulkan/vulkan.h>

namespace phys
{
    typedef unsigned short PeGpuDescriptionShot;
    typedef unsigned int PeGpuDescription;

    class PeGpuObjectType
    {
    private:
        uint32_t firstIndex;
        uint32_t maxIndex;
        uint32_t alignmentSize;
        uint32_t sizeObject;
        bool useDefaultElement;

    public:
        PeGpuObjectType() : firstIndex(0), maxIndex(0), alignmentSize(0), sizeObject(0), useDefaultElement(false)
        {
        }

        PeGpuObjectType(PeGpuObjectType &_sourceType, uint32_t _newMaxCount)
            : firstIndex(_sourceType.firstIndex), maxIndex(_sourceType.maxIndex),
              alignmentSize(_sourceType.alignmentSize), sizeObject(_sourceType.sizeObject),
              useDefaultElement(_sourceType.useDefaultElement)
        {
            maxIndex = firstIndex + _newMaxCount;
        }

        ~PeGpuObjectType()
        {
        }

        template <typename T, typename D>
        static PeGpuObjectType getObjectType(T _maxCountObjects, bool _useDefaultElement = true, uint32_t _alignment = 1)
        {
            PeGpuObjectType type;
            type.useDefaultElement = _useDefaultElement;
            if (type.useDefaultElement)
            {
                type.firstIndex = 1;
            }

            uint32_t max = (uint32_t)std::numeric_limits<T>::max();
            if (_maxCountObjects == max)
            {
                type.maxIndex = max;
            }
            else
            {
                type.maxIndex = type.firstIndex + _maxCountObjects;
            }

            uint32_t other = sizeof(D) % _alignment;
            if (other)
                type.alignmentSize = sizeof(D) + (_alignment - other);
            else
                type.alignmentSize = sizeof(D);
            return type;
        }

        template <typename T, typename D>
        static PeGpuObjectType getObjectType()
        {
            return getObjectType<T, D>(std::numeric_limits<T>::max() - 1);
        }

        inline bool getUseDefaultElement()
        {
            return useDefaultElement;
        }

        inline uint32_t getDefaultDescription()
        {
            return 0;
        }

        inline uint32_t getFirstDescription()
        {
            return firstIndex;
        }

        inline uint32_t getEndDiscription()
        {
            return maxIndex;
        }

        inline uint32_t getMaxCountObjects()
        {
            return getEndDiscription() - getFirstDescription();
        }

        inline uint32_t getObjectSize()
        {
            return sizeObject;
        }

        inline uint32_t getAlignmentSize()
        {
            return alignmentSize;
        }

        inline uint32_t toLocalIndex(uint32_t _globalIndex)
        {
            return _globalIndex + getFirstDescription();
        }

        inline uint32_t toGlobalIndex(uint32_t _localIndex)
        {
            return _localIndex - getFirstDescription();
        }

        inline uint32_t getOffsetToIndex(uint32_t globalIndex)
        {
            return sizeObject * globalIndex;
        }

        inline uint32_t getAlignmentOffsetToIndex(uint32_t globalIndex)
        {
            return alignmentSize * globalIndex;
        }
    };
}

#endif