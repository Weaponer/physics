#ifndef PE_GPU_OBJECTS_DATA_H
#define PE_GPU_OBJECTS_DATA_H

#include <vulkan/vulkan.h>

#include "peGpuMemoryBuffer.h"
#include "peGpuData.h"
#include "peGpuDescription.h"

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/memory/vulkanImageAllocator.h"

namespace phys
{
    class PeGpuObjectsData : public PeGpuMemoryBuffer
    {
    protected:
        PeGpuObjectType typeDescription;
        bool hostVisibility;
        bool shaderWriting;

    public:
        PeGpuObjectsData(const char *_name)
            : PeGpuMemoryBuffer(_name), typeDescription(),
              hostVisibility(false), shaderWriting(false) {}

        virtual ~PeGpuObjectsData() {}

    public:
        inline bool getHostVisibility() const
        {
            return hostVisibility;
        }

        inline bool getShaderWriting() const
        {
            return shaderWriting;
        }

        inline PeGpuObjectType getTypeDescription() const
        {
            return typeDescription;
        }

        inline void setHostVisibility(bool _enable)
        {
            hostVisibility = _enable;
        }

        inline void setShaderWriting(bool _enable)
        {
            shaderWriting = _enable;
        }

    public:
        void initType(const PeGpuData *gpuData, PeGpuObjectType *_typeDescription)
        {
            typeDescription = *_typeDescription;
            PeGpuMemory::init(gpuData);
        }
    };
}

#endif