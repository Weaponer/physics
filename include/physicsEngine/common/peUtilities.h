#ifndef PE_UTILITIES_H
#define PE_UTILITIES_H

#include <vulkan/vulkan.h>

namespace phys
{
    class PeUtilties
    {
    public:
        static uint32_t calculateGroups(uint32_t _sizeGroup, uint32_t _countLaunches);

    };
}

#endif