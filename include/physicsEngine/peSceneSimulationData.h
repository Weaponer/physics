#ifndef PE_SCENE_SIMULATION_DATA_H
#define PE_SCENE_SIMULATION_DATA_H

#include "foundation/phTableContainer.h"
#include "foundation/phBuffer.h"

#include "gpuCommon/vulkanFence.h"

#include "common/phBase.h"
#include "common/peSemaphoreStage.h"

#include "common/peGpuCounterImage.h"
#include "common/peGpuObjectBuffer.h"
#include "common/peGpuObjectsBuffer.h"
#include "common/peGpuImage.h"
#include "common/peGpuDescription.h"
#include "common/peGpuTableContactsBuffer.h"
#include "common/peBufferChain.h"
#include "common/peManagerCache.h"

#include "physics/internal/inActorManager.h"
#include "physics/internal/inOutputDebugData.h"
#include "physics/phSettingsFeedbackDebugData.h"

#include "peGpuStructures.h"

namespace phys
{
    class PeSceneSimulationData : public PhBase
    {
    public:
        InActorManager actorManager;
        PeManagerCache<InActorManager> actorMangerCache;

        PhTableContainer<int32_t> tableDynamicDatas; // keeps indexes of main actor for additional data
        PeBufferChain shapesChain;

        //
        InOutputDebugData outputDebugData;
        PhSettingsFeedbackDebugData settingsFeedbackDebugData;

        // scene settings
        PeGpuObjectBuffer *sceneSettingsBuffer;

        // actors data
        PeGpuObjectType actorType;
        PeGpuObjectType actorOrientationType;
        PeGpuObjectType actorDynamicType;
        PeGpuObjectType actorVelocityType;
        PeGpuObjectType actorSleepingType;
        PeGpuObjectType shapeListType;

        PeGpuObjectsBuffer *actorsBuffer;
        PeGpuObjectsBuffer *actorOrientationsBuffer;
        PeGpuObjectsBuffer *actorDynamicsBuffer;
        PeGpuObjectsBuffer *actorVelocitiesBuffer;
        PeGpuObjectsBuffer *actorSleepingBuffer;
        PeGpuObjectsBuffer *shapeListBuffer;

        // geometry OBBs
        struct GeometryOBBsData
        {
            PeGpuObjectType geometryObbType;
            PeGpuObjectType geometryOrientationObbType;
            PeGpuObjectsBuffer *geometryObbsBuffer;
            PeGpuObjectsBuffer *geometryOrientationObbsBuffer;

            PhBuffer<PeGpuDataListGeometryOBBs> actorAllocatedSegments;
            PeBufferChain segmentsChain;
            PhBuffer<uint32_t> obbsFlags;

            GeometryOBBsData();

            ~GeometryOBBsData() {}

            void release();

        } geometryOBBs;

        // table contacts
        struct TableContactsData
        {
            uint32_t indexLastBuffer;
            uint32_t lastMaxIndexOBB;
            uint32_t lastCountUsedContainers;

            PeGpuObjectsBuffer *swapIndexesBuffers[2];
            PeGpuTableContactsBuffer *tableContactsOBBsBuffers[2];
            PeGpuTableContactsBuffer *tableContainerContactsBuffers[2];
            PeGpuObjectsBuffer *actorLostContainerBuffer;
            PeGpuImage *statisticUsingImage;

            PeGpuObjectsBuffer *contactsContainersBuffers[2];
            PeGpuObjectsBuffer *contactsBuffers[2];

            PeGpuCounterImageUint *allocatedContactsContainersCounter;
            PeGpuCounterImageUint *allocatedContactsCounter;
            
            TableContactsData();

            ~TableContactsData() {}

            void release();

        } tableContacts;

        // table contacts
        struct SimulationData
        {
            PeGpuCounterImageUint *maxContainersOnActorCounter;
            uint32_t maxContainersOnActor;

            SimulationData();

            ~SimulationData() {}

            void release();
        } simulationData;

        // synchronization
        vk::VulkanFence finishFence;
        PeSemaphoreStage prepareDataStage;
        PeSemaphoreStage completeSimulationStage;

        bool firstSimulation;

        PeSceneSimulationData();

        PeSceneSimulationData(const PeSceneSimulationData &_data) = delete;

        virtual ~PeSceneSimulationData() {}

        virtual void release() override;

        
    };
}

#endif