#ifndef PE_SYSTEM_H
#define PE_SYSTEM_H

#include "common/phBase.h"

#include "foundation/phContext.h"

namespace phys
{
    class PeSystem : public PhBase
    {
    protected:
        const PhContext *simulationContext;

    public:
        PeSystem() : simulationContext(NULL) {}

        virtual ~PeSystem() {}

    protected:
        virtual void onInit() {}

    public:
        void init(const PhContext *_simulationContext)
        {
            simulationContext = _simulationContext;
            onInit();
        }
    };
}

#endif