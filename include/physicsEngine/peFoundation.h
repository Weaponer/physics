#ifndef PE_FOUNDATION_H
#define PE_FOUNDATION_H

#include "common/phBase.h"

#include "foundation/phFoundation.h"
#include "foundation/phContext.h"

#include "physics/internal/inMaterialManager.h"
#include "physics/internal/inShapeManager.h"

#include "peSceneSimulationData.h"
#include "peSceneSimulator.h"

namespace phys
{
    class PeFoundation : protected PhBase
    {
    private:
        PhFoundation *foundation;

        InMaterialManager *materialManager;
        InShapeManager *shapeManager;

        PeSceneSimulator *simulator;

    private:
        PeFoundation(PhFoundation *_foundation);

        virtual ~PeFoundation() {}

    public:
        static PeFoundation *createPeFoundation(PhFoundation *_foundation);

        static void destroyPeFoundation(PeFoundation *_physics);

        virtual void release() override;

        PhFoundation *getPhFoundation() { return foundation; }

        InMaterialManager *getMaterialManger() { return materialManager; }

        InShapeManager *getShapeManager() { return shapeManager; }

        PeSceneSimulator *getSceneSimulator() { return simulator; }
    };
};

#endif