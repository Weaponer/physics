#ifndef PE_SHAPE_DESCRIPTION_SYSTEM_H
#define PE_SHAPE_DESCRIPTION_SYSTEM_H

#include "foundation/phEasyStack.h"

#include "physics/internal/inShapeManager.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticLoaderSystem.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuDescription.h"
#include "physicsEngine/common/peGpuObjectsBuffer.h"
#include "physicsEngine/common/peBufferChain.h"

#include "materialDescription/peMaterialsDescriptionSystem.h"

namespace phys
{
    class PeShapeDescriptionSystem : public PeSystem
    {
    private:
        PeGpuObjectType shapeType;
        PeGpuObjectType geometryType;
        PeBufferChain geometryChainData;
        PhEasyStack<InShapeManager::ManagerEvent> bufferEvents;

        PeGpuData *gpu;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeMaterialsDescriptionSystem *materialsDescriptionSystem;
        PeGpuStaticLoaderSystem *staticLoader;
        InShapeManager *shapeManager;
        PeMemoryBarrierControllerSystem *memoryBarrierController;

        PeGpuObjectsBuffer *shapeBuffer;
        PeGpuObjectsBuffer *geometriesBuffer;

    public:
        PeShapeDescriptionSystem();

        virtual ~PeShapeDescriptionSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        uint32_t getLocalIndexShape(InShape *_shape);

        const PeGpuDataShape *getGpuDataShape(InShape *_shape);

        PeGpuObjectsBuffer *getShapeBuffer() const { return shapeBuffer; }
        
        PeGpuObjectsBuffer *getGeometriesBuffer() const { return geometriesBuffer; }

    private:
        void ensureShapesBufferCapacity(uint32_t _capacity);

        void ensureGeometriesBufferCapacity(uint32_t _capacity);

        uint32_t updateShapes(); // return count use geometries

        void updateGeometries(InShapeManager::ManagerEvent *_events, uint32_t _countEvents);
    };
}

#endif