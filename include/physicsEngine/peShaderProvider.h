#ifndef PE_SHADER_PROVIDER_H
#define PE_SHADER_PROVIDER_H

#include <vulkan/vulkan.h>

#include "common/phBase.h"
#include "common/phResourceProvider.h"

#include "foundation/phEasyStack.h"

#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/vulkanDescriptorPoolManager.h"

#include "gpuCommon/specialCommands/shaderAutoParameters.h"

namespace phys
{
    class PeShaderProvider : public PhBase
    {
    private:
        struct ComputePipelineData
        {
            char *name;
            vk::ShaderModule shaderModule;
            vk::ComputePipeline computePipeline;
            VkPipelineLayout pipelineLayout;
        };

    private:
        PhEasyStack<ComputePipelineData> computePipelines;

        PhResourceProvider *provider;
        vk::VulkanDevice *device;
        vk::ShaderManager *shaderManager;
        vk::VulkanDescriptorPoolManager *poolManager;

    public:
        PeShaderProvider();

        virtual ~PeShaderProvider() {}

        void init(PhResourceProvider *_provider, vk::VulkanDevice *_device);

        virtual void release() override;

        vk::ShaderModule getShaderModul(const char *_name);

        vk::ComputePipeline getPipeline(const char *_name);

        VkPipelineLayout getPipelineLayout(const char *_name);

        vk::VulkanDescriptorPoolManager *getVulkanDescriptorPoolManager();

        vk::ShaderAutoParameters *createShaderParameters(const char *_shaderName, uint _countSets);

        void destroyShaderAutoParameters(vk::ShaderAutoParameters *_parameters);

    private:
        bool tryToFindData(const char *_name, ComputePipelineData *_outData);
    };
}

#endif