#ifndef PE_GPU_STATIC_LOADER_SYSTEM_H
#define PE_GPU_STATIC_LOADER_SYSTEM_H

#include "peSystem.h"

#include "common/peGpuData.h"

#include "foundation/phEasyStack.h"

#include "gpuCommon/memory/vulkanBuffer.h"

namespace phys
{
    class PeGpuStaticLoaderSystem : public PeSystem
    {
    private:
        PeGpuData *gpu;

        PhEasyStack<vk::VulkanBuffer *> buffers;

        void *pWrite;
        uint32_t indexCurrentBuffer;
        uint32_t offsetCurrentBuffer;

        uint32_t sizeBuffer;

    public:
        PeGpuStaticLoaderSystem();

        virtual ~PeGpuStaticLoaderSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void begin();

        void end();

        void sendData(const void *_data, uint32_t _sizeData, vk::VulkanBuffer *_dstBuffer, uint32_t _dstOffset);

        void resetBuffers();

    private:
        vk::VulkanBuffer *createBuffer();

        void allocateNextBuffer();
    };
}

#endif