#ifndef PE_POTENTIAL_CONTACTS_STRUCTS_H
#define PE_POTENTIAL_CONTACTS_STRUCTS_H

#include <MTH/matrix.h>

namespace phys
{
    struct PeTableContactsUpdateData
    {
        uint32_t lastlastIndexTable;
        uint32_t lastIndexTable;
        uint32_t nextIndexTable;
    };

    struct PeGpuDataUpdateSwapIndex
    {
        uint32_t pos;
        uint32_t newIndex;
    };

    struct PeGpuDataContactNode
    {
        uint32_t index;
        int32_t next;
    };

    struct PeGpuDataPairOBBs
    {
        uint32_t first;
        uint32_t second;
        uint32_t indexContact;
        uint32_t numContact;  // index column in table contacts
    };

    struct PeGpuExchangeContainer
    {
        uint32_t firstOBBs;
        uint32_t secondOBBs;
        uint32_t offsetTableY;
        int32_t indexOldContainer;
    };

    struct PeGpuCopyContainer
    {
        uint32_t dstContainer;
        uint32_t srcContainer;
    };

    struct PeGpuDataContainersType
    {
        uint32_t firstContainer;
        uint32_t countContainers;
        uint32_t firstContact;  //all containers
        uint32_t countContacts; //all containers
    };
}

#endif