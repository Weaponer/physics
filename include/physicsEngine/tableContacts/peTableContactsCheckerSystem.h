#ifndef PE_TABLE_CONTACTS_CHECKER_SYSTEM_H
#define PE_TABLE_CONTACTS_CHECKER_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuCounterImage.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"

#include "peTableContactsStructs.h"
#include "peTableContactsPrepareCheckerSystem.h"

namespace phys
{
    class PeTableContactsCheckerSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;

        PeTableContactsPrepareCheckerSystem *tableContactsPrepareCheckerSystem;
        PeShapeDescriptionSystem *shapeDescriptionSystem;

        struct
        {
            vk::ComputePipeline checkGeometriesContactP[PeGpuPairGeometriesContact_Count];
            VkPipelineLayout checkGeometriesContactL[PeGpuPairGeometriesContact_Count];

            vk::ShaderAutoParameters *checkGeometriesContactS[PeGpuPairGeometriesContact_Count];
        } shaders;

        struct
        {
            PeGpuTableContactsBuffer *activeTableContacts;
        } buffers;

    public:
        PeTableContactsCheckerSystem();

        virtual ~PeTableContactsCheckerSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        PeGpuTableContactsBuffer *getActiveTableContacts() const { return buffers.activeTableContacts; }
    };
}

#endif