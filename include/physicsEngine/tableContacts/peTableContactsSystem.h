#ifndef PE_TABLE_CONTACTS_SYSTEM_H
#define PE_TABLE_CONTACTS_SYSTEM_H

#include "foundation/phContext.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/common/peGpuData.h"

#include "peTableContactsStructs.h"
#include "peContactsContainerMemorySystem.h"
#include "pePotentialContactsTableBuilderSystem.h"
#include "peTableContactsPrepareCheckerSystem.h"
#include "peTableContactsCheckerSystem.h"
#include "peContactsContainerExchangerSystem.h"
#include "peContactsContainerDistributorSystem.h"
#include "peContactsCopySystem.h"
#include "peUpdateSwapIndexesSystem.h"

#include "physicsEngine/potentialContacts/pePotentialContactsSystem.h"

namespace phys
{
    class PeTableContactsSystem : public PeSystem
    {
    private:
        PhContext tableContactsContext;

        PeContactsContainerMemorySystem contactsContainerMemorySystem;
        PePotentialContactsTableBuilderSystem potentialContactsTableBuilderSystem;
        PeTableContactsPrepareCheckerSystem tableContactsPrepareCheckerSystem;
        PeTableContactsCheckerSystem tableContactsCheckerSystem;
        PeContactsContainerExchangerSystem contactsContainerExchangerSystem;
        PeContactsContainerDistributorSystem contactsContainerDistributorSystem;
        PeContactsCopySystem contactsCopySystem; 
        PeUpdateSwapIndexesSystem updateSwapIndexesSystem;

        PeGpuData *gpu;
        PePotentialContactsSystem *potentialContactsSystem;

    public:
        PeTableContactsSystem();

        virtual ~PeTableContactsSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        PeContactsCopySystem *getContactsCopySystem() { return &contactsCopySystem; }

    private:
        PeTableContactsUpdateData getUpdateData(PeSceneSimulationData *_simulationData);
    };
}

#endif