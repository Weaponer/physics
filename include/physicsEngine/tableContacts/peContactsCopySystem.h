#ifndef PE_CONTACTS_COPY_SYSTEM_H
#define PE_CONTACTS_COPY_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuCounterImage.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "peTableContactsStructs.h"

#include "peContactsContainerDistributorSystem.h"
#include "peContactsContainerExchangerSystem.h"

namespace phys
{
    class PeContactsCopySystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;

        PeContactsContainerExchangerSystem *contactsContainerExchangerSystem;
        PeContactsContainerDistributorSystem *contactsContainerDistributorSystem;

        struct
        {
            vk::ComputePipeline generateDispatchUpdateContactsP;
            VkPipelineLayout generateDispatchUpdateContactsL;
            vk::ShaderAutoParameters *generateDispatchUpdateContactsS;

            vk::ComputePipeline copyContactsP;
            VkPipelineLayout copyContactsL;
            vk::ShaderAutoParameters *copyContactsS;
        } shaders;

        struct
        {
            PeGpuBuffer *copyContainersGroups;
            PeGpuBuffer *copyContainersGroupsIDB;

            PeGpuBuffer *containersData;
            PeGpuBuffer *updateContainersGroups;
            PeGpuBuffer *updateContainersGroupsIDB;
        } buffers;

        PeGpuStaticReaderSystem::ReadMemory *pReadContainersData;

    public:
        PeContactsCopySystem();

        virtual ~PeContactsCopySystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData);

        void buildPipeline(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage, PeTableContactsUpdateData _updateData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        PeGpuBuffer *getContainersData() { return buffers.containersData; }

        PeGpuBuffer *getUpdateContainersGroupIDB() { return buffers.updateContainersGroupsIDB; }
    };
}

#endif