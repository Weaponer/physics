#ifndef PE_CONTACTS_CONTAINER_EXCHANGER_SYSTEM_H
#define PE_CONTACTS_CONTAINER_EXCHANGER_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuCounterImage.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "peTableContactsStructs.h"
#include "peTableContactsCheckerSystem.h"

namespace phys
{
    class PeContactsContainerExchangerSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;

        PeTableContactsCheckerSystem *tableContactsCheckerSystem;

        struct
        {
            vk::ComputePipeline exchangeContactsContainerP;
            VkPipelineLayout exchangeContactsContainerL;
            vk::ShaderAutoParameters *exchangeContactsContainerS;
        } shaders;

        struct
        {
            PeGpuCounterImageUint *contactPairsCounter;
            PeGpuBuffer *exchangeContainersBuffer;
        } buffers;

    public:
        PeContactsContainerExchangerSystem();

        virtual ~PeContactsContainerExchangerSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData);

        void buildPipeline(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage, PeTableContactsUpdateData _updateData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        PeGpuCounterImageUint *getContactPairsCounter() const { return buffers.contactPairsCounter; }

        PeGpuBuffer *getExchangeContainersBuffer() const { return buffers.exchangeContainersBuffer; }
    };
}

#endif