#ifndef PE_CONTACTS_CONTAINER_DISTRIBUTOR_SYSTEM_H
#define PE_CONTACTS_CONTAINER_DISTRIBUTOR_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/memory/vulkanImage.h"

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuCounterImage.h"

#include "peTableContactsStructs.h"
#include "peTableContactsCheckerSystem.h"
#include "peContactsContainerExchangerSystem.h"

namespace phys
{
    class PeContactsContainerDistributorSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;

        PeContactsContainerExchangerSystem *contactsContainerExchangerSystem;

        struct
        {
            vk::ComputePipeline generateDispatchDistributorP;
            VkPipelineLayout generateDispatchDistributorL;
            vk::ShaderAutoParameters *generateDispatchDistributorS;

            vk::ComputePipeline contactsContainerDistributorP;
            VkPipelineLayout contactsContainerDistributorL;
            vk::ShaderAutoParameters *contactsContainerDistributorS;

            vk::ComputePipeline checkLostContainerP;
            VkPipelineLayout checkLostContainerL;
            vk::ShaderAutoParameters *checkLostContainerS;
        } shaders;

        struct
        {
            PeGpuBuffer *outputCountGroupsBuffer;
            PeGpuBuffer *indirectDispatchsBuffer;

            PeGpuBuffer *copyContainersBuffer;
            PeGpuBuffer *containerCopiedBuffer;
            PeGpuCounterImageUint *copyContainersCounter;
            PeGpuCounterImageUint *typesContactsCounter;
        } buffers;

    public:
        PeContactsContainerDistributorSystem();

        virtual ~PeContactsContainerDistributorSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData);

        void buildPipeline(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage, PeTableContactsUpdateData _updateData);

        void buildReceive(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData);

        PeGpuCounterImageUint *getCopyContainersCounter() const { return buffers.copyContainersCounter; }

        PeGpuCounterImageUint *getTypesContactsCounter() const { return buffers.typesContactsCounter; }

        PeGpuBuffer *getCopyContainersBuffer() const { return buffers.copyContainersBuffer; }
    };
}

#endif