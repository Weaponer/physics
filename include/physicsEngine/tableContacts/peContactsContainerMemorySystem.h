#ifndef PE_CONTACTS_CONTAINER_MEMORY_SYSTEM_H
#define PE_CONTACTS_CONTAINER_MEMORY_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuCounterImage.h"

namespace phys
{
    class PeContactsContainerMemorySystem : public PeSystem
    {
    public:
        struct LastStatistic
        {
            uint32_t lastCountContainers;
            uint32_t lastCountContacts;
        };
        
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;

        LastStatistic statistic;

    public:
        PeContactsContainerMemorySystem();

        virtual ~PeContactsContainerMemorySystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);
    
        LastStatistic getLastStatistic() const { return statistic; }

    private:
        PeGpuObjectsBuffer *createContactsContainersBuffer(uint32_t _countContainers);

        PeGpuObjectsBuffer *createContactsBuffer(uint32_t _countContacts);

        PeGpuObjectsBuffer *createActorLostContainersBuffer(uint32_t _countActors);

        PeGpuObjectsBuffer *createFreeContainersBuffer(uint32_t _countFreeContainers);
    };
}

#endif