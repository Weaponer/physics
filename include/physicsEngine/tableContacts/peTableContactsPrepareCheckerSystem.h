#ifndef PE_TABLE_CONTACTS_PREPARE_CHECKER_SYSTEM_H
#define PE_TABLE_CONTACTS_PREPARE_CHECKER_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"
#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuCounterImage.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/potentialContacts/pePotentialContactsSystem.h"

#include "peTableContactsStructs.h"

namespace phys
{
    class PeTableContactsPrepareCheckerSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;

    private:
        uint32_t countPotentialContactsDispatchs;

        struct
        {
            vk::ComputePipeline potentialContactsCollectorP;
            VkPipelineLayout potentialContactsCollectorL;
            vk::ShaderAutoParameters *potentialContactsCollectorS;

            vk::ComputePipeline checkContactsDispatchGeneratorP;
            VkPipelineLayout checkContactsDispatchGeneratorL;
            vk::ShaderAutoParameters *checkContactsDispatchGeneratorS;
        } shaders;

        struct
        {
            PeGpuBuffer *outputCheckPairContacts[PeGpuPairGeometriesContact_Count];
            PeGpuBuffer *outputCountGroupsBuffer;
            PeGpuBuffer *indirectDispatchsBuffer;
            PeGpuCounterImageUint *contactPairsCounter;

            uint32_t maxCountPairs[PeGpuPairGeometriesContact_Count];
            bool firstLaunch;
        } buffers;

        PeGpuStaticReaderSystem::ReadMemory *readPoint;
        
    public:
        PeTableContactsPrepareCheckerSystem();

        virtual ~PeTableContactsPrepareCheckerSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        PeGpuBuffer *getCheckPairContacts(PeGpuPairGeometriesContact _typeContact) const
        {
            return buffers.outputCheckPairContacts[_typeContact];
        }

        PeGpuBuffer *getIndirectDispatchsBuffer() const
        {
            return buffers.indirectDispatchsBuffer;
        }

        PeGpuCounterImageUint *getContactPairsCounter() const { return buffers.contactPairsCounter; }

        uint32_t getMaxCountPairs(PeGpuPairGeometriesContact _typeContact) const { return buffers.maxCountPairs[_typeContact]; } 

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage, PeTableContactsUpdateData _updateData);

        void buildReceive(PeSceneSimulationData *_simulationData);
    private:
        
    };
}

#endif