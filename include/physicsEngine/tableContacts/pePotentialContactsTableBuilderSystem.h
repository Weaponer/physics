#ifndef PE_POTENTIAL_CONTACTS_TABLE_BUILDER_SYSTEM_H
#define PE_POTENTIAL_CONTACTS_TABLE_BUILDER_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuCounterImage.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/potentialContacts/pePotentialContactsSystem.h"
#include "physicsEngine/potentialContacts/peUpdateGeometryOBBsSystem.h"

#include "peTableContactsStructs.h"
#include "peContactsContainerMemorySystem.h"

namespace phys
{
    class PePotentialContactsTableBuilderSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;

        PeContactsContainerMemorySystem *contactsConainerMemorySystem;

        struct
        {
            vk::ComputePipeline generateLastListContactsP;
            VkPipelineLayout generateLastListContactsL;
            vk::ShaderAutoParameters *generateLastListContactsS;

            vk::ComputePipeline generateListContactsP;
            VkPipelineLayout generateListContactsL;
            vk::ShaderAutoParameters *generateListContactsS;

            vk::ComputePipeline generateListContactsPlaneP;
            VkPipelineLayout generateListContactsPlaneL;
            vk::ShaderAutoParameters *generateListContactsPlaneS;

            vk::ComputePipeline fillTalbleContactsP;
            VkPipelineLayout fillTalbleContactsL;
            vk::ShaderAutoParameters *fillTalbleContactsS;
        } shaders;

        struct
        {
            PeGpuBuffer *nodesContactsBuffer;
            PeGpuCounterImageUint *nodesContactsCounter;
            PeGpuImage *pointerHeadsContactsImage;

            bool firstLaunch;
        } buffers;

        uint32_t countUpdateSwapTable;

        uint32_t startNewIndexes;
        uint32_t countNewIndexes;

    public:
        PePotentialContactsTableBuilderSystem();

        virtual ~PePotentialContactsTableBuilderSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage, PeTableContactsUpdateData _updateData);

        void buildPipeline(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage, PeTableContactsUpdateData _updateData);

        void buildReceive(PeSceneSimulationData *_simulationData);

    private:
        void dispatchGenerateLastContacts(PeSceneSimulationData *_simulationData,
                                          PeContactsContainerMemorySystem::LastStatistic _lastContactsStatistic,
                                          PeTableContactsUpdateData _updateData);

        void dispatchGenerateListContacts(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage,
                                          PeTableContactsUpdateData _updateData);

        void dispatchGenerateListContactsPlane(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage,
                                               PeTableContactsUpdateData _updateData);

        void dispatchFillTalbleContacts(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData);

        PeGpuTableContactsBuffer *createTableContacts(uint32_t _countIndexes);

        PeGpuObjectsBuffer *createSwapIndexesBuffer(uint32_t _countIndexes);

        void ensureMemory(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage);

        uint32_t getSizeSideImage(uint32_t _countElemtenst);
    };
}

#endif