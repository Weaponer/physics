#ifndef PE_CONTACTS_UPDATE_SYSTEM_H
#define PE_CONTACTS_UPDATE_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuCounterImage.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/peGpuStructures.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"
#include "physicsEngine/tableContacts/peTableContactsSystem.h"

namespace phys
{
    class PeContactsUpdateSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;
        
        PeShapeDescriptionSystem *shapeDescriptionSystem;
        PeTableContactsSystem *tableContactsSystem;
        struct
        {
            vk::ComputePipeline updateContactP[PeGpuPairGeometriesContact_Count];
            VkPipelineLayout updateContactL[PeGpuPairGeometriesContact_Count];
            vk::ShaderAutoParameters *updateContactS[PeGpuPairGeometriesContact_Count];

        } shaders;

        struct
        {
            PeGpuObjectsBuffer *updatedContactsBuffer;
            PeGpuObjectsBuffer *isContactActiveBuffer;
        } buffers;

    public:
        PeContactsUpdateSystem();

        virtual ~PeContactsUpdateSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        PeGpuObjectsBuffer *getUpdatedContacts() { return buffers.updatedContactsBuffer; }

        PeGpuObjectsBuffer *getIsContactActive() { return buffers.isContactActiveBuffer; }
    };
}

#endif