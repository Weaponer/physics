#ifndef PE_CONTACTS_SYSTEM_H
#define PE_CONTACTS_SYSTEM_H

#include "foundation/phContext.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"

#include "peContactsGenerateSystem.h"
#include "peContactsUpdateSystem.h"
#include "peContactsUniteSystem.h"

#include "physicsEngine/tableContacts/peTableContactsSystem.h"

namespace phys
{
    class PeContactsSystem : public PeSystem
    {
    public:
        struct ResultContainersData
        {
            PeGpuBuffer *countContainersBuffer;
            PeGpuBuffer *containersGroupIDB;
        };

    private:
        PhContext contactsContext;
        PeGpuData *gpu;

        PeContactsGenerateSystem contactsGenerateSystem;
        PeContactsUpdateSystem contactsUpdateSystem;
        PeContactsUniteSystem contactsUniteSystem;

    public:
        PeContactsSystem();

        virtual ~PeContactsSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        ResultContainersData getResultContainersData();
    };
}

#endif