#ifndef PE_CONTACTS_UNITY_SYSTEM_H
#define PE_CONTACTS_UNITY_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuBuffer.h"
#include "physicsEngine/common/peGpuCounterImage.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/peGpuStructures.h"

#include "physicsEngine/tableContacts/peTableContactsSystem.h"
#include "peContactsGenerateSystem.h"
#include "peContactsUpdateSystem.h"

namespace phys
{
    class PeContactsUniteSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;
        
        PeTableContactsSystem *tableContactsSystem;
        PeContactsGenerateSystem *contactsGenerateSystem;
        PeContactsUpdateSystem *contactsUpdateSystem;

        struct
        {
            vk::ComputePipeline containersCounterP;
            VkPipelineLayout containersCounterL;
            vk::ShaderAutoParameters *containersCounterS;

            vk::ComputePipeline uniteContactsP;
            VkPipelineLayout uniteContactsL;
            vk::ShaderAutoParameters *uniteContactsS;
        } shaders;

        struct
        {
            PeGpuBuffer *countContainersBuffer;
            PeGpuBuffer *uniteContactsGroups;
            PeGpuBuffer *uniteContactsGroupIDB;

            PeGpuObjectsBuffer *tempContainersBuffer;
        } buffers;

    public:
        PeContactsUniteSystem();

        virtual ~PeContactsUniteSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        PeGpuBuffer *getContainersGroupIDB() const { return buffers.uniteContactsGroupIDB; }

        PeGpuBuffer *getCountContainersBuffer() const { return buffers.countContainersBuffer; }
    };
}

#endif