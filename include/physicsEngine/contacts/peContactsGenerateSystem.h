#ifndef PE_CONTACTS_GENERATE_SYSTEM_H
#define PE_CONTACTS_GENERATE_SYSTEM_H

#include <vulkan/vulkan.h>

#include <MTH/vectors.h>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/shaders/shader.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peReallocateDatasSystem.h"
#include "physicsEngine/peShaderProvider.h"
#include "physicsEngine/peMemoryBarrierContollerSystem.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuCounterImage.h"
#include "physicsEngine/common/peConstantsUtils.h"

#include "physicsEngine/peGpuStructures.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"
#include "physicsEngine/tableContacts/peTableContactsSystem.h"

namespace phys
{
    class PeContactsGenerateSystem : public PeSystem
    {
    private:
        PeGpuData *gpuData;
        PeShaderProvider *shaderProvider;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGpuStaticReaderSystem *staticReader;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeReallocateDatasSystem *reallocateDatasSystem;

        PeShapeDescriptionSystem *shapeDescriptionSystem;
        PeTableContactsSystem *tableContactsSystem;
        struct
        {
            vk::ComputePipeline generateContactP[PeGpuPairGeometriesContact_Count];
            VkPipelineLayout generateContactL[PeGpuPairGeometriesContact_Count];
            vk::ShaderAutoParameters *generateContactS[PeGpuPairGeometriesContact_Count];

        } shaders;

        struct
        {
            PeGpuObjectsBuffer *newContactsBuffer;
            PeGpuObjectsBuffer *indexesTypeContainerBuffer;
        } buffers;

    public:
        PeContactsGenerateSystem();

        virtual ~PeContactsGenerateSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        void buildPipeline(PeSceneSimulationData *_simulationData);

        void buildReceive(PeSceneSimulationData *_simulationData);

        PeGpuObjectsBuffer *getGeneratedContacts() { return buffers.newContactsBuffer; }

        PeGpuObjectsBuffer *getIndexesTypeContainer() { return buffers.indexesTypeContainerBuffer; }
    };
}

#endif