#ifndef PE_ACTOR_SHAPES_DESCRIPTION_H
#define PE_ACTOR_SHAPES_DESCRIPTION_H

#include "foundation/phEasyStack.h"

#include "common/peGpuDescription.h"
#include "common/peGpuObjectsBuffer.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuDescription.h"
#include "physicsEngine/common/peGpuObjectsBuffer.h"

#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peGpuStaticLoaderSystem.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"

#include "physics/internal/inActorManager.h"
#include "physics/internal/inRigidBody.h"

namespace phys
{
    class PeActorShapesDescription : public PeSystem
    {
    private:
        struct UpdateShapeSegment
        {
            InRigidBody *rigidBody;
            uint32_t count;
            uint32_t firstIndex;
        };

    private:
        PeGpuData *gpu;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeGpuStaticLoaderSystem *staticLoaderSystem;
        PeShapeDescriptionSystem *shapeDescriptionSystem;

        PhEasyStack<UpdateShapeSegment> bufferUpdateSegments;

    public:
        PeActorShapesDescription();

        virtual ~PeActorShapesDescription() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

    private:
        PeGpuObjectType getShapesListType(uint32_t _capacity);

        PeGpuObjectsBuffer *createShapesListBuffer(PeGpuObjectType _type);

        uint32_t updateShapesList(PeSceneSimulationData *_simulationData, InActorManager::ManagerEvent *_events, uint32_t _countEvents);

        void ensureShapeList(PeSceneSimulationData *_simulationData, uint32_t _capacity);

        void sendUpdateShapeList(PeSceneSimulationData *_simulationData, UpdateShapeSegment *_segments, uint32_t _countSegments);
    };
}

#endif