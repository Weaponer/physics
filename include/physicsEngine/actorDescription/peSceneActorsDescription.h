#ifndef PE_SCENE_ACTOR_DESCRIPTION_H
#define PE_SCENE_ACTOR_DESCRIPTION_H

#include "foundation/phEasyStack.h"

#include "common/peGpuDescription.h"
#include "common/peGpuObjectsBuffer.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuDescription.h"
#include "physicsEngine/common/peGpuObjectsBuffer.h"

#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peGpuStaticLoaderSystem.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"

#include "physics/internal/inActorManager.h"
#include "physics/internal/inRigidBody.h"

#include "peActorDynamicDescription.h"
#include "peActorShapesDescription.h"

namespace phys
{
    class PeSceneActorsDescription : public PeSystem
    {
    private:
        struct UpdateShapeSegment
        {
            InRigidBody *rigidBody;
            uint32_t count;
            uint32_t firstIndex;
        };

    private:
        PeGpuData *gpu;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeGpuStaticLoaderSystem *staticLoaderSystem;
        PeShapeDescriptionSystem *shapeDescriptionSystem;

        PeActorDynamicDescription *actorDynamicDescription;
        PeActorShapesDescription *actorShapesDescription;

    public:
        PeSceneActorsDescription();

        virtual ~PeSceneActorsDescription() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

    private:
        PeGpuObjectType getActorType(uint32_t _capacity);

        PeGpuObjectType getActorOrientationType(uint32_t _capacity);

        PeGpuObjectsBuffer *createActorsBuffer(PeGpuObjectType _type);

        PeGpuObjectsBuffer *createActorOrientationsBuffer(PeGpuObjectType _type);

        uint32_t readEvents(PeSceneSimulationData *_simulationData);

        void ensureCapacityBuffers(PeSceneSimulationData *_simulationData, uint32_t _capacity);

        PeGpuActorFlags getActorFlags(InActor *_actor);

        void sendUpdateActors(PeSceneSimulationData *_simulationData);
    };
}

#endif