#ifndef PE_ACTOR_DYNAMIC_DESCRIPTION_H
#define PE_ACTOR_DYNAMIC_DESCRIPTION_H

#include "foundation/phEasyStack.h"

#include "common/peGpuDescription.h"
#include "common/peGpuObjectsBuffer.h"

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuDescription.h"
#include "physicsEngine/common/peGpuObjectsBuffer.h"

#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peGpuStaticLoaderSystem.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"

#include "physics/internal/inActorManager.h"
#include "physics/internal/inRigidBody.h"

namespace phys
{
    class PeActorDynamicDescription : public PeSystem
    {
    private:
        PeGpuData *gpu;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeGpuStaticLoaderSystem *staticLoaderSystem;

        PhEasyStack<InActorManager::ManagerEvent> bufferUpdateDynamicData;
        PhEasyStack<InActorManager::ManagerEvent> bufferUpdateVelocityData;

    public:
        PeActorDynamicDescription();

        virtual ~PeActorDynamicDescription() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

        uint32_t getCountUpdateDynamicData() const { return bufferUpdateDynamicData.getPos(); }

        const InActorManager::ManagerEvent *getEventsUpdateDynamicData() const { return bufferUpdateDynamicData.getMemory(0); }

    private:
        uint32_t getCapacityActorDynamicBuffers(PeSceneSimulationData *_simulationData, InActorManager::ManagerEvent *_events, uint32_t _countEvents);

        PeGpuObjectType getActorDynamicType(uint32_t _capacity);

        PeGpuObjectType getActorVelocityType(uint32_t _capacity);
        
        PeGpuObjectType getActorSleepingType(uint32_t _capacity);

        PeGpuObjectsBuffer *createActorDynamicsBuffer(PeGpuObjectType _type);

        PeGpuObjectsBuffer *createActorVelocitiesBuffer(PeGpuObjectType _type);

        PeGpuObjectsBuffer *createActorSleepingBuffer(PeGpuObjectType _type);

        void ensureCapacityDynamicBuffers(PeSceneSimulationData *_simulationData, uint32_t _capacity);

        void updateDynamicData(PeSceneSimulationData *_simulationData, InActorManager::ManagerEvent *_events, uint32_t _countEvents);

        void updateVelocityData(PeSceneSimulationData *_simulationData, InActorManager::ManagerEvent *_events, uint32_t _countEvents);
    };
}

#endif