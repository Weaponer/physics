#ifndef PE_GPU_STRUCTURES_H
#define PE_GPU_STRUCTURES_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>

#include "common/phFlags.h"
#include "common/peGpuDescription.h"

namespace phys
{
    struct PeGpuDataSceneSettings
    {
        mth::vec3 gravitation;          //0 12
        float deltaTime;                //12 16
        float stiffness;                //16 20
        float damping;                  //20 24
        float depthContactMin;          //24 28
        float depthContactMax;          //28 32
        float linearDamping;            //32 36
        float angularDamping;           //36 40
        float frictionStaticLimit;      //40 44
        float maxVelocitySleep;         //44 48
        float maxAngularVelocitySleep;  //48 52
        float minSleepTime;             //52 56
        uint percision;                 //56 60
        uint updateVelocities;          //60 64
    };

    enum PeGpuActorFlags : uint8_t
    {
        PeGpuActor_Static = 0,
        PeGpuActor_Kinematic = 1,
        PeGpuActor_Dynamic = 2,
    };

    struct PeGpuDataActor
    {
        uint32_t countShapes;         // 4
        uint32_t indexFirstShape;     // 8
                                      //
        uint32_t indexAdditionalData; // 12
                                      //
        uint8_t flags;                // 13    //static, kinematic, dynamic
    };

    struct PeGpuDataActorOrientation
    {
        mth::quat rotation; // 16
        mth::vec3 position; // 28
        float freeMemory_1; // 32
    };

    struct PeGpuDataDynamicActor
    {
        mth::vec4 inertialTensor[3];        //48
        mth::vec4 inverseInertialTensor[3]; //96
        mth::vec3 centerMass;               //108
        float mass;                         //112
        float linearDrag;                   //116
        float angularDrag;                  //120
        float freeMem_1;                    //124
        float freeMem_2;                    //128
    };

    struct PeGpuDataVelocityActor
    {
        mth::vec3 acceleration;        // 12
        float freeMemory_1;            // 16
        mth::vec3 angularAcceleration; // 28
        float freeMemory_2;            // 32
        mth::vec3 velocity;            // 44
        float freeMemory_3;            // 48
        mth::vec3 angularVelocity;     // 60
        uint32_t flags;                // 64 - is sleep
    };

    struct PeGpuDataSleepingActor   //8
    {
        float sleepTime;            //0 4
        uint32_t isSleeping;        //4 8
    };

    struct PeGpuDataMaterial
    {
        float staticFriction;  // 4
        float dynamicFriction; // 8
        float bounciness;      // 12
    };

    struct PeGpuDataGeometryOBB
    {
        PeGpuDescription indexActor;        // 4
        PeGpuDescription indexMainCollider; // 8
        PeGpuDescription indexShape;        // 12
                                            //
        uint32_t flags;                     // 16 - kinematic, static, dynamic, plane, was destroyed
    };

    struct PeGpuDataOrientationGeometryOBB
    {
        mth::quat rotation; // 16
        mth::vec4 size;     // 32
        mth::vec4 position; // 48
    };

    struct PeGpuDataShape
    {
        int32_t indexMaterial;      // 4 local index material 0 - NULL
        uint32_t countGeometries;    // 8
        uint32_t indexFirstGeometry; // 12
        uint32_t flags;              // 16
    };

    enum PeGpuGeometryFlags : uint8_t
    {
        PeGpuGeometry_Plane = 0,      // 0000 0000
        PeGpuGeometry_Box = 1,        // 0000 0001
        PeGpuGeometry_Sphere = 2,     // 0000 0010
        PeGpuGeometry_Capsule = 3,    // 0000 0011
                                      //
        PeGpuGeometry_Dynamic = 16,   // 0001 0000
        PeGpuGeometry_Kinematic = 32, // 0010 0000
        PeGpuGeometry_Static = 48,    // 0011 0000
                                      //
        PeGpuGeometry_Unsizable = 128 // 1000 0000
    };

    struct PeGpuDataGeometry
    {
        mth::vec4 data_1;  // 16
        mth::vec4 data_2;  // 32
                           //
        mth::quat rotate;  // 48
        mth::vec3 offset;  // 60
        float freeMemory;  // 64
        mth::vec3 obbSize; // 76
                           //
        uint8_t flags;     // 77
    };

    struct PeGpuDataListGeometryOBBs
    {
        uint32_t firstIndex; // 4
        uint32_t count;      // 8
    };

    struct PeGpuDataDispatchIndirect
    {
        uint32_t countGroupX; // 4
        uint32_t countGroupY; // 8
        uint32_t countGroupZ; // 12
        uint32_t freeMemory;  // 16
    };

    enum PeGpuContactsContainerFlags : PhFlags
    {
        PeGpuContactsContainer_1 = 0,
        PeGpuContactsContainer_2 = 1,
        PeGpuContactsContainer_4 = 2,
        PeGpuContacstContainer_8 = 3,
        PeGpuContactsContainer_16 = 4,

        PeGpuContactsContainer_CountTypes = 5,

        PeGpuContactsContainer_MaxSize = 16
    };

    struct PeGpuDataContactsContainer
    {
        uint32_t firstContactIndex;    // 4
        uint32_t countAndMaskContacts; // 8 0000 0000 ffff ffff - max count contacts, ffff ffff 0000 0000 - mask using contacts
        uint32_t firstIndexGeometry;   // 12
        uint32_t secondIndexGeometry;  // 16
    };

    struct PeGpuDataContact
    { 
        mth::vec3 firstLocalPosition;  // 12 - local position contact from fist geometry
        float depthPenetration;        // 16
        mth::vec3 secondLocalPosition; // 28 - local position contact from second geometry
        float freeMemory;              // 32
        mth::vec3 firstLocalNormal;    // 44
        float freeMemory_2;            // 48
        mth::vec3 secondLocalNormal;   // 60
        float freeMemory_3;            // 64

        // debug
        mth::vec3 firstPointGlobal;   // 76
        float freeMemory_4;           // 80
        mth::vec3 secondPointGlobal;  // 92
        float freeMemory_5;           // 96
        mth::vec3 firstOriginGlobal;  // 108
        float freeMemory_6;           // 112
        mth::vec3 secondOriginGlobal; // 124
        float freeMemory_7;           // 128
    };

    enum PeGpuPairGeometriesContact : uint32_t
    {
        PeGpuPairGeometriesContact_PlaneBox = 0,
        PeGpuPairGeometriesContact_PlaneSphere = 1,
        PeGpuPairGeometriesContact_PlaneCapsule = 2,
        PeGpuPairGeometriesContact_BoxBox = 3,
        PeGpuPairGeometriesContact_BoxSphere = 4,
        PeGpuPairGeometriesContact_BoxCapsule = 5,
        PeGpuPairGeometriesContact_SphereSphere = 6,
        PeGpuPairGeometriesContact_SphereCapsule = 7,
        PeGpuPairGeometriesContact_CapsuleCapsule = 8,

        PeGpuPairGeometriesContact_FirstGeometryOffset = 4,

        //                                        second            |   first
        PeGpuPairGeometriesContact_PlaneBoxMask = PeGpuGeometry_Box | (PeGpuGeometry_Plane << PeGpuPairGeometriesContact_FirstGeometryOffset),
        PeGpuPairGeometriesContact_PlaneSphereMask = PeGpuGeometry_Sphere | (PeGpuGeometry_Plane << PeGpuPairGeometriesContact_FirstGeometryOffset),
        PeGpuPairGeometriesContact_PlaneCapsuleMask = PeGpuGeometry_Capsule | (PeGpuGeometry_Plane << PeGpuPairGeometriesContact_FirstGeometryOffset),
        PeGpuPairGeometriesContact_BoxBoxMask = PeGpuGeometry_Box | (PeGpuGeometry_Box << PeGpuPairGeometriesContact_FirstGeometryOffset),
        PeGpuPairGeometriesContact_BoxSphereMask = PeGpuGeometry_Sphere | (PeGpuGeometry_Box << PeGpuPairGeometriesContact_FirstGeometryOffset),
        PeGpuPairGeometriesContact_BoxCapsuleMask = PeGpuGeometry_Capsule | (PeGpuGeometry_Box << PeGpuPairGeometriesContact_FirstGeometryOffset),
        PeGpuPairGeometriesContact_SphereSphereMask = PeGpuGeometry_Sphere | (PeGpuGeometry_Sphere << PeGpuPairGeometriesContact_FirstGeometryOffset),
        PeGpuPairGeometriesContact_SphereCapsuleMask = PeGpuGeometry_Capsule | (PeGpuGeometry_Sphere << PeGpuPairGeometriesContact_FirstGeometryOffset),
        PeGpuPairGeometriesContact_CapsuleCapsuleMask = PeGpuGeometry_Capsule | (PeGpuGeometry_Capsule << PeGpuPairGeometriesContact_FirstGeometryOffset),

        //
        PeGpuPairGeometriesCountContacts_PlaneBox = 4,
        PeGpuPairGeometriesCountContacts_PlaneSphere = 1,
        PeGpuPairGeometriesCountContacts_PlaneCapsule = 2,
        PeGpuPairGeometriesCountContacts_BoxBox = 4,
        PeGpuPairGeometriesCountContacts_BoxSphere = 1,
        PeGpuPairGeometriesCountContacts_BoxCapsule = 2,
        PeGpuPairGeometriesCountContacts_SphereSphere = 1,
        PeGpuPairGeometriesCountContacts_SphereCapsule = 1,
        PeGpuPairGeometriesCountContacts_CapsuleCapsule = 2,

        //
        PeGpuPairGeometriesContact_Count = 9,
    };
}
#endif