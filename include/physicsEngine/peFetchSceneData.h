#ifndef PE_FETCH_SCENE_DATA_H
#define PE_FETCH_SCENE_DATA_H


#include "peSystem.h"

#include "foundation/phContext.h"

#include "gpuCommon/vulkanDevice.h"

#include "physics/internal/inScene.h"

#include "common/peGpuData.h"

namespace phys
{
    class PeFetchSceneData : public PeSystem
    {
    private:
        PeGpuData *gpuData;
    
    public:
        PeFetchSceneData();

        virtual void onInit() override;

        virtual void release() override;

        void fetchSceneData(PeSceneSimulationData *_scene);
    };
}

#endif