#ifndef PE_SCENE_SETTINGS_SYSTEM_H
#define PE_SCENE_SETTINGS_SYSTEM_H

#include "peSystem.h"
#include "peSceneSimulationData.h"

#include "common/peGpuDescription.h"
#include "common/peGpuObjectsBuffer.h"

#include "gpuCommon/memory/vulkanBuffer.h"

#include "physics/phSceneSettings.h"
#include "physics/phSimulationSettings.h"

namespace phys
{
    class PeSceneSettingsSystem : public PeSystem
    {
    private:
        PeGpuData *gpu;
        PhSceneSettings sceneSettings;
        PhSimulationSettings simulationSettings;

    public:
        PeSceneSettingsSystem();

        virtual ~PeSceneSettingsSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(const PhSceneSettings *_settings, const PhSimulationSettings &_simSettings, PeSceneSimulationData *_simulationData, float _deltaTime);
    
        const PhSceneSettings &getSceneSettings() { return sceneSettings; }
        
        const PhSimulationSettings &getSimulationSettings() { return simulationSettings; }
    };
}

#endif