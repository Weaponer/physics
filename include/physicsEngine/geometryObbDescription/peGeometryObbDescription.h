#ifndef PE_GEOMETRY_OBB_DESCRIPTION_H
#define PE_GEOMETRY_OBB_DESCRIPTION_H

#include "physicsEngine/peSystem.h"
#include "physicsEngine/peSceneSimulationData.h"

#include "physicsEngine/common/peGpuData.h"
#include "physicsEngine/common/peGpuDescription.h"
#include "physicsEngine/common/peGpuObjectsBuffer.h"

#include "physicsEngine/peGrabObjectsDatasSystem.h"
#include "physicsEngine/peGpuStaticLoaderSystem.h"
#include "physicsEngine/peGpuStaticReaderSystem.h"

#include "physics/internal/inShapeManager.h"

namespace phys
{
    class PeGeometryObbDescription : public PeSystem
    {
    private:
        PeGpuData *gpu;
        PeGrabObjectsDatasSystem *grabObjectsSystem;
        PeGpuStaticLoaderSystem *staticLoaderSystem;
        InShapeManager *shapeManager;

        PhEasyStack<PeGpuDataListGeometryOBBs> bufferRemoveGeometryOBBs;
        PhEasyStack<InActorManager::ManagerEvent> bufferUpdateGeometryOBBs;

    public:
        PeGeometryObbDescription();

        virtual ~PeGeometryObbDescription() {}

        virtual void onInit() override;

        virtual void release() override;

        void prepareData(PeSceneSimulationData *_simulationData);

    private:
        uint32_t getCountGeometries(InActor *_actor);

        PeGpuObjectType getGeometryObbType(uint32_t _capacity);

        PeGpuObjectType getGeometryOrientationObbType(uint32_t _capacity);

        PeGpuObjectsBuffer *createGeometryObbsBuffer(PeGpuObjectType _type);

        PeGpuObjectsBuffer *createGeometryOrientationObbsBuffer(PeGpuObjectType _type);

        void ensureBuffers(uint32_t _capacity, PeSceneSimulationData::GeometryOBBsData *_geometryOBBs);

        void removeGeometryOBBs(PeSceneSimulationData *_simulationData);

        void updateGeometryOBBs(PeSceneSimulationData *_simulationData);

        uint8_t getGeometryAdditionalFlag(PeSceneSimulationData *_simulationData, InActor *_actor);
    };
}

#endif
