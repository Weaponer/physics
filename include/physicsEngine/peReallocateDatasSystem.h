#ifndef PE_REALLOCATE_DATAS_SYSTEM_H
#define PE_REALLOCATE_DATAS_SYSTEM_H

#include "common/phFlags.h"

#include "peSystem.h"

#include "common/peGpuData.h"
#include "common/peGpuObjectsBuffer.h"
#include "common/peGpuBuffer.h"
#include "common/peGpuImage.h"
#include "common/peGpuTableContactsBuffer.h"

#include "peMemoryBarrierContollerSystem.h"

#include "peGrabObjectsDatasSystem.h"

namespace phys
{
    class PeReallocateDatasSystem : public PeSystem
    {
    public:
        enum ReserveMemoryType : PhFlags
        {
            ReserveMemoryType_100p = 100,
            ReserveMemoryType_110p = 110,
            ReserveMemoryType_120p = 120,
            ReserveMemoryType_130p = 130,
            ReserveMemoryType_140p = 140,
            ReserveMemoryType_150p = 150,
            ReserveMemoryType_160p = 160,
            ReserveMemoryType_170p = 170,
            ReserveMemoryType_180p = 180,
            ReserveMemoryType_190p = 190,
            ReserveMemoryType_200p = 200
        };

    private:
        PeGpuData *gpu;
        PeMemoryBarrierControllerSystem *memoryBarrierController;
        PeGrabObjectsDatasSystem *grabObjectsDatas;

    public:
        PeReallocateDatasSystem();

        virtual ~PeReallocateDatasSystem() {}

        virtual void onInit() override;

        virtual void release() override;

        bool ensureObjectsBuffer(uint32_t _minimalCountObjects,
                                 PeGpuObjectsBuffer *_currentBuffer,
                                 PeGpuObjectsBuffer **_outputCurrentBuffer,
                                 ReserveMemoryType _reserveMemoryType = ReserveMemoryType_100p);

        bool ensureObjectsBufferWithCopyAndGrap(uint32_t _minimalCountObjects,
                                                PeGpuObjectsBuffer *_currentBuffer,
                                                PeGpuObjectsBuffer **_outputCurrentBuffer,
                                                ReserveMemoryType _reserveMemoryType = ReserveMemoryType_100p,
                                                uint32_t *_defaultValue = NULL);

        bool ensureTableContactsBuffer(uint32_t _minimalCountObjects,
                                       PeGpuTableContactsBuffer *_currentTableContacts,
                                       PeGpuTableContactsBuffer **_outputCurrentTableContacts,
                                       ReserveMemoryType _reserveMemoryType = ReserveMemoryType_100p);

        bool ensureTableContactsBufferWithCopyAndGrap(uint32_t _minimalCountObjects,
                                                      PeGpuTableContactsBuffer *_currentTableContacts,
                                                      PeGpuTableContactsBuffer **_outputCurrentTableContacts,
                                                      ReserveMemoryType _reserveMemoryType = ReserveMemoryType_100p,
                                                      VkClearColorValue *_clearColorValue = NULL);

        bool ensureBuffer(uint32_t _minimalSize, vk::MemoryUseType _useType, vk::VulkanBufferType _type,
                          PeGpuBuffer *_currentBuffer,
                          PeGpuBuffer **_outputCurrentBuffer,
                          ReserveMemoryType _reserveMemoryType = ReserveMemoryType_100p);

        bool ensureBufferWithCopyAndGrap(uint32_t _minimalSize, vk::MemoryUseType _useType, vk::VulkanBufferType _type,
                                         PeGpuBuffer *_currentBuffer,
                                         PeGpuBuffer **_outputCurrentBuffer,
                                         ReserveMemoryType _reserveMemoryType = ReserveMemoryType_100p,
                                         uint32_t *_defaultValue = NULL);

        bool ensureImage(uint32_t _minimalWidth, uint32_t _minimalHeight,
                         PeGpuImage *_currentImage,
                         PeGpuImage **_outputCurrentImage,
                         ReserveMemoryType _reserveMemoryType = ReserveMemoryType_100p);

        bool ensureImageWithCopyAndGrap(uint32_t _minimalWidth, uint32_t _minimalHeight,
                                        PeGpuImage *_currentImage,
                                        PeGpuImage **_outputCurrentImage,
                                        ReserveMemoryType _reserveMemoryType = ReserveMemoryType_100p,
                                        VkClearColorValue *_clearColorValue = NULL);

        uint32_t reserveCount(uint32_t _count, ReserveMemoryType _reserveMemoryType);
    };
}

#endif