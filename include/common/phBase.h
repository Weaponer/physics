#ifndef PH_BASE_H
#define PH_BASE_H

namespace phys
{
    class PhBase
    {
    public:
        PhBase() {}

        virtual ~PhBase() {}

        virtual void release();
    };
}

#endif