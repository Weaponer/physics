#ifndef PH_MACROS_H
#define PH_MACROS_H

#include <stdio.h>
#include <string>
#include "phRefCounter.h"

#define PH_ASSERT_ERROR 1
#define PH_SCENE_LAUNCHED_ERROR "Error: Scene launched. "
#define PH_GEOMETRY_HAS_OWNER "Error: Geometry already has owner. "
#define PH_THIS_SHAPE_ALLREADY_CONNECT "Error: This shape allready connect to this ShapeRegister. "

#define PH_ASSERT(condition, error)                        \
    if (!(condition))                                      \
    {                                                      \
        printf("%s %s : %i\n", error, __FILE__, __LINE__); \
        exit(PH_ASSERT_ERROR);                             \
    }

#define PH_NEW(T) new (phAllocateMemory(sizeof(T))) T

#define PH_RELEASE(obj)          \
    {                            \
        obj->release();          \
        phDeallocateMemory(obj); \
    }

#define PH_REF_RELEASE(obj)                  \
    {                                        \
        if (!PhRefCounter::isUseMemory(obj)) \
        {                                    \
            obj->release();                  \
            phDeallocateMemory(obj);         \
        }                                    \
    }
    
#define PH_TNAME(type) typeid(type).name()
#define PH_TANAME(type, add) (std::string(typeid(type).name()) + std::string(add)).c_str()
#define PH_CNAME(first, second) (std::string(first) + std::string(second)).c_str()



#endif