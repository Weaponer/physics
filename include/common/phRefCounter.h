#ifndef PH_REF_COUNTER_H
#define PH_REF_COUNTER_H

#include "phBase.h"

namespace phys
{
    class PhRefCounter : public PhBase
    {
    private:
        unsigned int countRefs;

    public:
        PhRefCounter() : PhBase(), countRefs(0) {}

        virtual ~PhRefCounter() {}

        inline static bool isUseMemory(const PhRefCounter *_counter) { return _counter->countRefs != 0; }

        inline static void incReference(PhRefCounter *_counter) { _counter->countRefs++; }

        inline static void decReference(PhRefCounter *_counter)
        {
            if ((_counter->countRefs & ~0) != 0)
                _counter->countRefs--;
        }
    };
};

#endif