#ifndef PH_VULKAN_LIBRARY_PROVIDER_H
#define PH_VULKAN_LIBRARY_PROVIDER_H

namespace phys
{
    class PhVulkanLibraryProvider
    {
    private:
        void *vulkanLibrary;

    public:
        bool load();

        void close();

        void *loadFunction(const char *_name);

        const void *getLibrary() { return vulkanLibrary; }

    protected:
        virtual void *loadLibrary() = 0;

        virtual void closeLibrary(void *_library) = 0;

        virtual void *loadFunctionOfLibrary(void *_library, const char *_name) = 0;
    };
}
#endif