#ifndef PH_OBJECT_MESH_H
#define PH_OBJECT_MESH_H

#include <MTH/vectors.h>
#include <stdio.h>

#include "phBase.h"

namespace phys
{
    class PhObjectMesh : public PhBase
    {
    public:
        struct Vertex
        {
            mth::vec3 pos;
            mth::vec2 uv;
            mth::vec3 normal;
            mth::vec3 tangent;
        };

        struct LineVertex
        {
            mth::vec3 pos;
        };

        class Mesh : public PhBase
        {
        private:
            int sizeName;
            char *name;
            int countVertexes;
            Vertex *vertexes;
            int countIndexes;
            uint *indexes;
            int countLineVertexes;
            LineVertex *lineVertexes;

        public:
            Mesh(int _sizeName, const char *_name,
                 int _countVertexes, Vertex *_vertexes,
                 int _countIndexes, uint *_indexes,
                 int _countLineVertexes, LineVertex *_lineVertex);

            virtual ~Mesh() {}

            virtual void release() override;

            const char *getName() const { return name; }

            int getCountVertexes() const { return countVertexes; }

            const Vertex *getVertexes() const { return vertexes; }

            int getCountIndexes() const { return countIndexes; }

            const uint *getIndexes() const { return indexes; }

            int getCountLineVertexes() const { return countLineVertexes; }

            const LineVertex *getLineVertexes() const { return lineVertexes; }
        };

    private:
        int countMeshes;
        Mesh **meshes;

    public:
        PhObjectMesh(int _countMeshes, Mesh **_meshes);

        virtual ~PhObjectMesh() {}

        virtual void release() override;

        int getCountMeshes() const { return countMeshes; }

        const Mesh *getMesh(int _index) const { return meshes[_index]; }
    };
}

#endif