#ifndef PH_RESOURCE_PROVIDER_H
#define PH_RESOURCE_PROVIDER_H

#include "phBase.h"
#include "phObjectMesh.h"
#include "foundation/phBuffer.h"
#include "gpuCommon/shaders/shader.h"

namespace phys
{
    class PhResourceProvider : public PhBase
    {
    protected:
        struct Packet
        {
            char *tag;
            PhBuffer<vk::ShaderData *> *shaders;
        };

    protected:
        PhBuffer<Packet> bufferShadersData;
        PhBuffer<PhObjectMesh *> bufferMeshes;

    public:
        PhResourceProvider() : bufferShadersData(), bufferMeshes() {}

        virtual ~PhResourceProvider();

        uint32_t getCountShaders(const char *_tag) const;

        vk::ShaderData *const *getShadersPerTag(const char *_tag);

        vk::ShaderData *getShaderData(const char *_name, const char *_tag);

        void loadShadersPacket(const char *_path, const char *_tag);

        PhObjectMesh *loadObjectMesh(const char *_path);

        virtual void release() override;

    protected:
        bool getFindPacket(const char *_tag, Packet *_outPacket) const;

    protected:
        virtual const char *getPathToShaders() = 0;

        virtual const char *getPathToObjectMeshes() = 0;

        virtual bool readFile(const char *_path, char **_data, int *_size) = 0;
    };
}

#endif