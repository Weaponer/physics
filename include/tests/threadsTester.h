#ifndef THREADS_TESTER_H
#define THREADS_TESTER_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <mutex>

#include "threads/phThreadsManager.h"
#include "threads/phThread.h"
#include "threads/phTask.h"
#include "threads/safePrint.h"

namespace phys
{
    class ThreadsTester
    {
    private:
        class TestTask : public PhTask
        {
        private:
            std::string name;

        public:
            std::atomic_int counter;

        public:
            TestTask(const char *_name) : PhTask(), name(_name), counter(0) {}

            virtual ~TestTask() {}

            virtual void complete(int _instance) override
            {
                counter.fetch_add(1);
                //SafePrint::print("%s: %i\n", name.c_str(), _instance);
            }
        };

    private:
        PhThreadsManager *manager;

    public:
        ThreadsTester();

        ~ThreadsTester();

        void test();
    };
}

#endif