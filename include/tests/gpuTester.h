#ifndef GPU_TESTER_H
#define GPU_TESTER_H

#include <MTH/vectors.h>

#include <vector>

#include "memory/vulkanBuffer.h"
#include "memory/vulkanBufferAllocator.h"
#include "memory/vulkanImageAllocator.h"
#include "specialCommands/vulkanCommandReceiver.h"
#include "window/window.h"
#include "vulkanSwapchain.h"
#include "shaders/shaderManager.h"
#include "shaders/vulkanPipelineLayout.h"
#include "rendering/vulkanRenderPass.h"
#include "rendering/renderPassesManager.h"
#include "rendering/vulkanFramebuffer.h"
#include "rendering/vulkanGraphicPipeline.h"
#include "rendering/graphicPipelinesManager.h"
#include "rendering/vulkanVertexAttributeState.h"
#include "rendering/vulkanBlendColorState.h"
#include "vulkanSemaphore.h"
#include "vulkanFence.h"
#include "vulkanQueue.h"
#include "specialCommands/vulkanGraphicCommands.h"

namespace phys
{
    class GpuTester
    {
    private:
        vk::VulkanDevice *device;
        vk::VulkanCommandReceiver *receiver;
        vk::VulkanBufferAllocator *bufferAllocator;
        vk::VulkanImageAllocator *imageAllocator;
        Window *window;

    private:
        std::vector<vk::ShaderData *> shaderDatas;
        vk::ShaderManager shaderManager;

    private:
        struct Vertex
        {
            mth::vec3 position;
            mth::vec3 color;
            mth::vec2 uv;
        };

    private:
        vk::RenderPassesManager renderPassesManager;
        vk::VulkanRenderPass renderPass;
        std::vector<vk::VulkanFramebuffer> framebuffers;
        vk::ShaderModule vertexShader;
        vk::ShaderModule fragmentShader;
        vk::VulkanPipelineLayout graphicPipelineLayout;
        vk::VulkanBlendColorState blendColorState;
        std::array<Vertex, 3> vertices;
        vk::VulkanVertexAttributeState vertexAttrubuteState;
        vk::GraphicPipelinesManager graphicPipelineManager;
        vk::VulkanGraphicPipeline *graphicPipeline;
        vk::VulkanSemaphore signalBeginRenderGUI;
        vk::VulkanFence renderingFence;
        vk::VulkanQueue *graphicQueue;
        vk::VulkanGraphicCommands *graphicCommands;
        vk::VulkanBuffer *vertexBuffer;

    public:
        GpuTester(vk::VulkanCommandReceiver *_receiver, vk::VulkanBufferAllocator *_bufferAllocator,
                  vk::VulkanImageAllocator *_imageAllocator, Window *_window);

        ~GpuTester();

        void testMemory();

        void test();

        void testWindowRender();

    private:
        void readPacketShader();

        void initWindowRendering();

        bool drawFrame(uint _indexImage);

        void endWindowRendering();
    };
}

#endif