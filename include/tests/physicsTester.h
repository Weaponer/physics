#ifndef PHYSICS_TESTER_H
#define PHYSICS_TESTER_H

#include <iostream>
#include <string>
#include <stdlib.h>

#include "foundation/phFoundation.h"
#include "physics/phPhysics.h"
#include "physics/phScene.h"

namespace phys
{
    class PhysicsTester
    {
    private:
        PhFoundation *foundation;
        PhPhysics *physics;
        PhScene *scene;

    public:
        PhysicsTester(PhFoundation *_foundation);

        ~PhysicsTester();

        void test();

    private:
        void printOBBsData();
    };
}

#endif