#ifndef PH_BOX_GEOMETRY_H
#define PH_BOX_GEOMETRY_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>
#include "phGeometry.h"

namespace phys
{
    class PhBoxGeometry : public PhGeometry
    {
    public:
        PhBoxGeometry() : PhGeometry() {}
        virtual ~PhBoxGeometry() {}

        virtual mth::vec3 getOffset() const = 0;

        virtual mth::vec3 getSize() const = 0;

        virtual mth::quat getRotate() const = 0;

        virtual void setOffset(mth::vec3 _offset) = 0;

        virtual void setSize(mth::vec3 _size) = 0;

        virtual void setRotate(mth::quat _rotate) = 0;
    };
};

#endif