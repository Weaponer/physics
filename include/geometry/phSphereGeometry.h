#ifndef PH_SPHERE_GEOMETRY_H
#define PH_SPHERE_GEOMETRY_H

#include <MTH/vectors.h>
#include "phGeometry.h"

namespace phys
{
    class PhSphereGeometry : public PhGeometry
    {
    public:
        PhSphereGeometry() : PhGeometry() {}
        virtual ~PhSphereGeometry() {}

        virtual float getRadius() const = 0;

        virtual mth::vec3 getOffset() const = 0;

        virtual void setRadius(float _radius) = 0;

        virtual void setOffset(mth::vec3 _offset) = 0;
    };
};

#endif