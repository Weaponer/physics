#ifndef PH_CAPSULE_GEOMETRY_H
#define PH_CAPSULE_GEOMETRY_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>
#include "phGeometry.h"

namespace phys
{
    class PhCapsuleGeometry : public PhGeometry
    {
    public:
        PhCapsuleGeometry() : PhGeometry() {}
        virtual ~PhCapsuleGeometry() {}

        virtual mth::vec3 getOffset() const = 0;

        virtual mth::quat getRotate() const = 0;

        virtual float getRadius() const = 0;

        virtual float getHeight() const = 0;

        virtual void setOffset(mth::vec3 _offset) = 0;

        virtual void setRotate(mth::quat _rotate) = 0;

        virtual void setRadius(float _radius) = 0;

        virtual void setHeight(float _height) = 0;
    };
};

#endif