#ifndef PH_PLANE_GEOMETRY_H
#define PH_PLANE_GEOMETRY_H

#include <MTH/vectors.h>
#include "phGeometry.h"

namespace phys
{
    class PhPlaneGeometry : public PhGeometry
    {
    public:
        PhPlaneGeometry() : PhGeometry() {}
        virtual ~PhPlaneGeometry() {}

        virtual float getOffset() const = 0;

        virtual void setOffset(float _offset) = 0;
    };
};

#endif