#ifndef IN_CAPSULE_GEOMETRY_H
#define IN_CAPSULE_GEOMETRY_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>

#include "inGeometryTemplate.h"
#include "geometry/phCapsuleGeometry.h"

namespace phys
{
    class InCapsuleGeometry : public InGeometryTemplate<PhCapsuleGeometry>
    {
    public:
        mth::vec3 offset;
        mth::quat rotate;
        float radius;
        float height;

    public:
        InCapsuleGeometry() : InGeometryTemplate<PhCapsuleGeometry>(), offset(), rotate(), radius(1.0f), height(1.0f) {}

        virtual ~InCapsuleGeometry() {}

        virtual uint8_t getGpuGeometryFlags() override
        {
            return PeGpuGeometryFlags::PeGpuGeometry_Capsule;
        }

        virtual PeGpuDataGeometry getGpuDataGeometry() override
        {
            PeGpuDataGeometry data = {};
            data.flags = getGpuGeometryFlags();
            data.data_1 = mth::vec4(radius, height, 0.0f, 0.0f);
            data.obbSize = mth::vec3(radius, height + radius, radius);
            data.offset = offset;
            data.rotate = rotate;
            return data;
        }

        virtual float getVolume() override
        {
            return 4.0f / 3.0f * PI * (radius * radius * radius) + height * PI * (radius * radius);
        }

        virtual mth::vec3 getOffset() const override { return offset; }

        virtual mth::quat getRotate() const override { return rotate; }

        virtual float getRadius() const override { return radius; }

        virtual float getHeight() const override { return height; }

        virtual void setOffset(mth::vec3 _offset) override
        {
            offset = _offset;
            setWasChanged();
        }

        virtual void setRotate(mth::quat _rotate) override
        {
            rotate = _rotate;
            setWasChanged();
        }

        virtual void setRadius(float _radius) override
        {
            radius = _radius;
            setWasChanged();
        }

        virtual void setHeight(float _height) override
        {
            height = _height;
            setWasChanged();
        }

        virtual PhFlags getGeometryType() const override { return PhGeometry::PhGeometry_Capsule; }

        virtual void release() override
        {
            InGeometryTemplate<PhCapsuleGeometry>::release();
        }
    };
};

#endif