#ifndef IN_PLANE_GEOMETRY_H
#define IN_PLANE_GEOMETRY_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>

#include "inGeometryTemplate.h"
#include "geometry/phPlaneGeometry.h"

namespace phys
{
    class InPlaneGeometry : public InGeometryTemplate<PhPlaneGeometry>
    {
    public:
        float offset;

    public:
        InPlaneGeometry() : InGeometryTemplate<PhPlaneGeometry>(), offset() {}

        virtual ~InPlaneGeometry() {}

        virtual uint8_t getGpuGeometryFlags() override
        {
            return PeGpuGeometryFlags::PeGpuGeometry_Plane | PeGpuGeometryFlags::PeGpuGeometry_Unsizable;
        }

        virtual PeGpuDataGeometry getGpuDataGeometry() override
        {
            PeGpuDataGeometry data = {};
            data.flags = getGpuGeometryFlags();
            data.offset = mth::vec3(0.0f, offset, 0.0f);
            data.obbSize = mth::vec3(1.0f, 1.0f, 1.0f);
            return data;
        }

        virtual float getVolume() override
        {
            return 0.0f;
        }

        virtual float getOffset() const override { return offset; }

        virtual void setOffset(float _offset) override
        {
            offset = _offset;
            setWasChanged();
        }

        virtual PhFlags getGeometryType() const override { return PhGeometry::PhGeometry_Plane; }

        virtual void release() override
        {
            InGeometryTemplate<PhPlaneGeometry>::release();
        }
    };
};

#endif