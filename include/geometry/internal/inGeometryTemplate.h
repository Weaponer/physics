#ifndef IN_GEOMETRY_TEMPLATE_H
#define IN_GEOMETRY_TEMPLATE_H

#include "common/phMacros.h"

#include "inGeometryBase.h"

namespace phys
{
    template <class T>
    class InGeometryTemplate : public InGeometryBase, public T
    {
    public:
        InGeometryTemplate() : InGeometryBase(), T()
        {
        }

        virtual ~InGeometryTemplate() {}
    };
}

#endif