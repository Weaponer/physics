#ifndef IN_SHAPE_EXCLUSIVE_H
#define IN_SHAPE_EXCLUSIVE_H

#include "inShape.h"
#include "foundation/phTableContainer.h"

#include "physics/internal/inShapeListener.h"
#include "physics/internal/inManager.h"

namespace phys
{
    class InShapeExclusive : public InShape
    {
    protected:
        InShapeListenerBase *own;

    public:
        InShapeExclusive(InShapeManager *_manager) : InShape(_manager, InShapeFlagBits::ExclusiveShape) {}

        virtual ~InShapeExclusive() {}

        virtual uint addShapeRegister(InShapeListenerBase *_register) override;

        virtual void removeShapeRegister(uint _index) override;

        virtual void release() override;

    protected:
        virtual void onChange() override;
    };
};

#endif