#ifndef IN_SHAPE_GLOBAL_H
#define IN_SHAPE_GLOBAL_H

#include "inShape.h"
#include "foundation/phTableContainer.h"
#include "physics/internal/inShapeListener.h"
#include "physics/internal/inManager.h"

namespace phys
{
    class InShapeGlobal : public InShape
    {
    protected:
        PhTableContainer<InShapeListenerBase *> registers;

    public:
        InShapeGlobal(InShapeManager *_manager)
            : InShape(_manager, InShapeFlagBits::GlobalShape), registers()
        {
            InShapeListenerBase *def = NULL;
            registers.setUseDefault(true);
            registers.setDefault(&def);
        }

        virtual ~InShapeGlobal() {}

        virtual uint addShapeRegister(InShapeListenerBase *_register) override;

        virtual void removeShapeRegister(uint _index) override;

        virtual void release() override;

    protected:
        virtual void onChange() override;
    };
};

#endif