#ifndef IN_BOX_GEOMETRY_H
#define IN_BOX_GEOMETRY_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>

#include "inGeometryTemplate.h"

#include "geometry/phBoxGeometry.h"

#include "physicsEngine/peGpuStructures.h"

namespace phys
{
    class InBoxGeometry : public InGeometryTemplate<PhBoxGeometry>
    {
    public:
        mth::vec3 offset;
        mth::vec3 size;
        mth::quat rotate;

    public:
        InBoxGeometry() : InGeometryTemplate<PhBoxGeometry>(), offset(), size(1.0f, 1.0f, 1.0f), rotate() {}

        virtual ~InBoxGeometry() {}

        virtual uint8_t getGpuGeometryFlags() override
        {
            return PeGpuGeometryFlags::PeGpuGeometry_Box;
        }

        virtual PeGpuDataGeometry getGpuDataGeometry() override
        {
            PeGpuDataGeometry data = {};
            data.flags = getGpuGeometryFlags();
            data.obbSize = size;
            data.offset = offset;
            data.rotate = rotate;
            return data;
        }

        virtual float getVolume() override
        {
            return size.x * size.y * size.z;
        }

        virtual mth::vec3 getOffset() const override { return offset; }

        virtual mth::vec3 getSize() const override { return size; }

        virtual mth::quat getRotate() const override { return rotate; }

        virtual void setOffset(mth::vec3 _offset) override
        {
            offset = _offset;
            setWasChanged();
        }

        virtual void setSize(mth::vec3 _size) override
        {
            size = _size;
            setWasChanged();
        }

        virtual void setRotate(mth::quat _rotate) override
        {
            rotate = _rotate;
            setWasChanged();
        }

        virtual PhFlags getGeometryType() const override { return PhGeometry::PhGeometry_Box; }

        virtual void release() override
        {
            InGeometryTemplate<PhBoxGeometry>::release();
        }
    };
}

#endif