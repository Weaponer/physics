#ifndef IN_SPHERE_GEOMETRY_H
#define IN_SPHERE_GEOMETRY_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>

#include "inGeometryTemplate.h"
#include "geometry/phSphereGeometry.h"

namespace phys
{
    class InSphereGeometry : public InGeometryTemplate<PhSphereGeometry>
    {
    public:
        mth::vec3 offset;
        float radius;

    public:
        InSphereGeometry() : InGeometryTemplate<PhSphereGeometry>(), offset(), radius(1.0f) {}

        virtual ~InSphereGeometry() {}

        virtual uint8_t getGpuGeometryFlags() override
        {
            return PeGpuGeometryFlags::PeGpuGeometry_Sphere;
        }

        virtual PeGpuDataGeometry getGpuDataGeometry() override
        {
            PeGpuDataGeometry data = {};
            data.flags = getGpuGeometryFlags();
            data.data_1 = mth::vec4(radius, 0.0f, 0.0f, 0.0f);
            data.obbSize = mth::vec3(radius, radius, radius);
            data.offset = offset;
            data.rotate = mth::quat();
            return data;
        }

        virtual float getVolume() override
        {
            return 4.0f / 3.0f * PI * (radius * radius * radius);
        }

        virtual mth::vec3 getOffset() const override { return offset; }

        virtual float getRadius() const override
        {
            return radius;
        }

        virtual void setOffset(mth::vec3 _offset) override
        {
            offset = _offset;
            setWasChanged();
        }

        virtual void setRadius(float _radius) override
        {
            radius = _radius;
            setWasChanged();
        }

        virtual PhFlags getGeometryType() const override { return PhGeometry::PhGeometry_Sphere; }

        virtual void release() override
        {
            InGeometryTemplate<PhSphereGeometry>::release();
        }
    };
};

#endif