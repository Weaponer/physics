#ifndef IN_SHAPE_H
#define IN_SHAPE_H

#include "geometry/phShape.h"
#include "foundation/phBuffer.h"

#include "physics/internal/inShapeManager.h"

#include "physicsEngine/peGpuStructures.h"

namespace phys
{
    class InGeometryBase;
    class InShapeListenerBase;
    class InShape : public PhShape
    {
    public:
        enum InShapeFlagBits : PhFlags
        {
            ShapeEvent_None = 0,
            ShapeEvent_ChangedGeometry = 1,
            ShapeEvent_AddedGeometry = 2,
            ShapeEvent_RemovedGeometry = 3,
            ShapeEvent_ChangedMaterial = 4,

            GlobalShape = 0,
            ExclusiveShape = 1,
        };

    public:
        InShapeManager::Item managerRegistration;

        PhMaterial *material;
        PhBuffer<InGeometryBase *> geometryBuffer;
        PhFlags typeInShape;

    protected:
        InShape(InShapeManager *_manager, PhFlags _typeInShape);

    public:
        virtual ~InShape() {}

        virtual void connectGeometry(PhGeometry *_geometry) override;

        virtual void disconnectGeometry(PhGeometry *_geometry) override;

        virtual void geometryWasChanged(InGeometryBase *_geometry);

        virtual int countGeometries() override;

        virtual PhGeometry *getGeometry(int _index) override;

        virtual void setMaterial(PhMaterial *_material) override;

        virtual PhMaterial *getMaterial() override;

        virtual unsigned int addShapeRegister(InShapeListenerBase *) = 0;

        virtual void removeShapeRegister(unsigned int _index) = 0;

        bool isExclusive() const { return typeInShape == InShapeFlagBits::ExclusiveShape; }

        virtual void release() override;

    protected:
        virtual void onChange() = 0;
    };
}

#endif