#ifndef IN_GEOMETRY_BASE_H
#define IN_GEOMETRY_BASE_H

#include <MTH/boundOBB.h>
#include <MTH/quaternion.h>
#include <MTH/vectors.h>

#include "common/phRefCounter.h"
#include "common/phMacros.h"

#include "inShape.h"

#include "physicsEngine/peGpuStructures.h"

namespace phys
{
    class InGeometryBase : public PhRefCounter
    {
    public:
        InShape *ownShape;

    public:
        InGeometryBase() : PhRefCounter(), ownShape(NULL) {}

        virtual ~InGeometryBase() {}

        virtual uint8_t getGpuGeometryFlags() = 0;

        virtual PeGpuDataGeometry getGpuDataGeometry() = 0;

        virtual float getVolume() = 0;

    public:
        void setOwnShape(InShape *_shape)
        {
            PH_ASSERT(ownShape == NULL || _shape == NULL, PH_GEOMETRY_HAS_OWNER);
            ownShape = _shape;
        }

        void setWasChanged()
        {
            if (ownShape != NULL)
                ownShape->geometryWasChanged(this);
        }

        void disconnectGeometry()
        {
            if (ownShape != NULL)
                ownShape->disconnectGeometry(dynamic_cast<PhGeometry *>(this));
        }

        virtual void release() override
        {
            disconnectGeometry();
            PhRefCounter::release();
        }
    };
}

#endif