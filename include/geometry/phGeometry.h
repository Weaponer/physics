#ifndef PH_GEOMETRY_H
#define PH_GEOMETRY_H

#include "common/phRefCounter.h"
#include "common/phFlags.h"

namespace phys
{
    class PhGeometry
    {
    public:
        enum : PhFlags
        {
            PhGeometry_None = 0,
            PhGeometry_Box = 1,
            PhGeometry_Sphere = 2,
            PhGeometry_Capsule = 3,
            PhGeometry_Plane = 4
        };

    public:
        PhGeometry() {}
        virtual ~PhGeometry() {}

        virtual PhFlags getGeometryType() const = 0;
    };
};

#endif