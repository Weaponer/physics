#ifndef PH_SHAPE_H
#define PH_SHAPE_H

#include "common/phRefCounter.h"

#include "geometry/phGeometry.h"

#include "physics/phMaterial.h"

namespace phys
{
    class PhShape : public PhRefCounter
    {
    public:
        PhShape() : PhRefCounter() {}

        virtual ~PhShape() {}

        virtual void connectGeometry(PhGeometry *_geometry) = 0;

        virtual void disconnectGeometry(PhGeometry *_geometry) = 0;

        virtual int countGeometries() = 0;

        virtual PhGeometry *getGeometry(int _index) = 0;

        virtual void setMaterial(PhMaterial *_material) = 0;

        virtual PhMaterial *getMaterial() = 0;
    };
};

#endif