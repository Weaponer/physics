#ifndef VULKAN_COMMAND_BUFFER_H
#define VULKAN_COMMAND_BUFFER_H

#include <vulkan/vulkan.h>

namespace phys::vk
{
    class VulkanDevice;
    class VulkanCommandBuffer
    {
    private:
        VkCommandBuffer commandBuffer;
        VulkanDevice *device;

    public:
        VulkanCommandBuffer(VkCommandBuffer _commandBuffer, VulkanDevice *_device)
            : commandBuffer(_commandBuffer), device(_device) {}

        ~VulkanCommandBuffer() {}

        bool isValid() const { return !(commandBuffer == VK_NULL_HANDLE || device == NULL); }

        VulkanDevice *getDevice() const { return device; }

        operator VkCommandBuffer() { return commandBuffer; }
    };
}

#endif