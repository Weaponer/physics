#ifndef VULKAN_IMAGE_ALLOCATOR_H
#define VULKAN_IMAGE_ALLOCATOR_H

#include <array>
#include <vulkan/vulkan.h>

#include "gpuMemoryController.h"
#include "gpuCommon/vulkanDevice.h"

#include "vulkanImage.h"
#include "memoryType.h"
#include "memoryTypeDescriptions.h"

namespace phys::vk
{
    class VulkanImageAllocator
    {
    protected:
        VulkanDevice *device;
        GpuMemoryController *memoryController;

        std::array<VkImageTiling, VulkanImageType_MaxEnum> typesTiling;
        std::array<VkImageUsageFlags, VulkanImageType_MaxEnum> typesUsages;
        std::array<RequireMemoryType, VulkanBufferType_MaxEnum> typesMemory;

    public:
        VulkanImageAllocator(VulkanDevice *_device, GpuMemoryController *_memoryController);

        ~VulkanImageAllocator();

        void setSettingForTypeImage(VulkanImageType _typeImage, VkImageTiling _imageTiling,
                                    VkImageUsageFlags _usages, RequireMemoryType _memoryType);

        VulkanImage *createImage(MemoryUseType _use, VkImageTiling _imageTiling, VkImageUsageFlags _usages, RequireMemoryType _memoryType,
                                 VkImageType _imageType, VkFormat _format, VkImageAspectFlags _aspect,
                                 int _width, int _height, int _depth, int _countMipmaps, int _layers,
                                 int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name = NULL);

        VulkanImage *createImage(MemoryUseType _use, VulkanImageType _type, VkImageUsageFlags _usages,
                                 VkImageType _imageType, VkFormat _format, VkImageAspectFlags _aspect,
                                 int _width, int _height, int _depth, int _countMipmaps, int _layers,
                                 int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name = NULL);

        VulkanImage *createImage(MemoryUseType _use, VulkanImageType _type, VkImageType _imageType, VkFormat _format, VkImageAspectFlags _aspect,
                                 int _width, int _height, int _depth, int _countMipmaps, int _layers,
                                 int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name = NULL);

        void destroyImage(VulkanImage *_image);

        GpuMemoryController *getMemoryController() const { return memoryController; }
    };

}

#endif