#ifndef DEVICE_MEMORY_H
#define DEVICE_MEMORY_H

#include <vulkan/vulkan.h>

#include "memoryType.h"

namespace phys::vk
{
    class GpuMemoryController;
    struct PrepareMemoryPage;

    class DeviceMemory
    {
    private:
        VkDeviceMemory memory;
        int size;
        int offset;         // start memory chunk
        int offsetAlignment; // start use memory chunk

        PrepareMemoryPage *page;

        bool useMemory;
        DeviceMemory *next;
        DeviceMemory *back;

        DeviceMemory()
            : memory(VK_NULL_HANDLE), size(0), offset(0), offsetAlignment(0), page(NULL), useMemory(false), next(NULL), back(NULL)
        {
        }

        DeviceMemory(VkDeviceMemory _memory, int _size, int _offset, int _offsetAlignment, PrepareMemoryPage *_page)
            : memory(_memory), size(_size), offset(_offset), offsetAlignment(_offsetAlignment), page(_page), useMemory(false), next(NULL), back(NULL)
        {
        }

        ~DeviceMemory() {}

    public:
        bool isValid() const { return memory != VK_NULL_HANDLE; }

        int getSize() const { return size; }

        int getOffset() const { return offset; }

        int getOffsetAlignment() const { return offsetAlignment; }

        

        operator VkDeviceMemory() { return memory; }

        friend class GpuMemoryController;
    };

    struct PrepareMemoryPage
    {
        MemoryType memoryType;
        VkDeviceMemory memory;
        int size;
        int freeSpace;

        DeviceMemory *beginDeviceMemoryIterators;

        PrepareMemoryPage *nextPage;
    };
};

#endif