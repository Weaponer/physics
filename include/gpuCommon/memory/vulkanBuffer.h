#ifndef VULKAN_BUFFER_H
#define VULKAN_BUFFER_H

#include <vulkan/vulkan.h>

#include "deviceMemory.h"

namespace phys::vk
{
    class VulkanBuffer
    {
    protected:
        VkBuffer buffer;
        DeviceMemory *memory;

        uint32_t size;
        VkBufferUsageFlags usage;
        bool hostVisible;

    public:
        VulkanBuffer()
            : buffer(VK_NULL_HANDLE), memory(NULL), size(0), usage(0), hostVisible(false) {}

        VulkanBuffer(VkBuffer _buffer, DeviceMemory *_memory, uint32_t _size, VkBufferUsageFlags _usage, bool _hostVisible)
            : buffer(_buffer), memory(_memory), size(_size), usage(_usage), hostVisible(_hostVisible) {}

        virtual ~VulkanBuffer() {}

        inline bool isValid() const { return buffer != VK_NULL_HANDLE; }

        inline DeviceMemory *getDeviceMemory() { return memory; }

        inline uint32_t getSize() const { return size; }

        inline VkBufferUsageFlags getUsage() const { return usage; }

        inline bool isHostVisible() const { return hostVisible; }

        operator VkBuffer() { return buffer; }
    };
}

#endif