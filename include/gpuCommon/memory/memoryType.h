#ifndef MEMORY_TYPE_H
#define MEMORY_TYPE_H

#include <vulkan/vulkan.h>

namespace phys::vk
{
    struct RequireMemoryType
    {
        VkMemoryPropertyFlags memoryProperties;
        VkMemoryHeapFlags heapProperties;

        bool operator<(const RequireMemoryType &_comp) const
        {
            uint64_t thisVal = (((uint64_t)heapProperties) << sizeof(uint32_t) * 8) | (uint64_t)memoryProperties;
            uint64_t compVal = (((uint64_t)_comp.heapProperties) << sizeof(uint32_t) * 8) | (uint64_t)_comp.memoryProperties;
            return thisVal < compVal;
        }

        bool operator==(const RequireMemoryType &_comp) const
        {
            return memoryProperties == _comp.memoryProperties && heapProperties == _comp.heapProperties;
        }
    };

    struct MemoryType
    {
        VkMemoryPropertyFlags memoryProperties;
        VkMemoryHeapFlags heapProperties;
        int indexMemory;

        bool operator<(const MemoryType &_comp) const
        {
            uint64_t thisVal = (((uint64_t)heapProperties) << sizeof(uint32_t) * 8) | (uint64_t)memoryProperties;
            uint64_t compVal = (((uint64_t)_comp.heapProperties) << sizeof(uint32_t) * 8) | (uint64_t)_comp.memoryProperties;
            return thisVal < compVal;
        }
    };
}

#endif