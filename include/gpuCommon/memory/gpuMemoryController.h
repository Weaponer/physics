#ifndef GPU_MEMORY_CONTROLLER_H
#define GPU_MEMORY_CONTROLLER_H

#include <vector>
#include <map>
#include <list>

#include "memoryType.h"
#include "deviceMemory.h"
#include "gpuCommon/vulkanDevice.h"

namespace phys::vk
{
    enum MemoryUseType : int
    {
        MemoryUseType_Temporary = 0,
        MemoryUseType_Medium = 1,
        MemoryUseType_Long = 2,
    };

    struct GpuMemoryControllerConfig
    {
        int sizePrepareDeviceMemoryNodes;
        int sizeLongMemoryPage;             // kB
        int sizeMediumMemoryPage;           // kB
        int sizeTemporaryMemoryPage;        // kB
        int averageSizeTemporaryAllocation; // bytes

        int reservedPages;
    };

    class GpuMemoryController
    {
    private:
        VulkanDevice *device;
        GpuMemoryControllerConfig config;
        std::vector<RequireMemoryType> requireMemoryTypes;

        std::map<RequireMemoryType, std::vector<MemoryType> *> aviableMemoryTypes;

        std::map<RequireMemoryType, PrepareMemoryPage *> beginLongMemoryPages;
        std::map<RequireMemoryType, PrepareMemoryPage *> beginMediumMemoryPages;

        DeviceMemory *beginDeviceMemoryNodes;

        std::list<DeviceMemory *> allocatedDeviceMemoryNodes;

    public:
        GpuMemoryController(VulkanDevice *_device);

        ~GpuMemoryController();

        bool addRequireMemoryType(RequireMemoryType _requireMemoryType);

        void prepareMemory();

        int getSizePage(MemoryUseType _use);

        DeviceMemory *getDeviceMemory(MemoryUseType _use, int _size, int _alignment, RequireMemoryType _memoryType);

        bool isHostVisibleMemory(const DeviceMemory *_memory) const;

        void returnDeviceMemory(DeviceMemory *_memory);

        bool checkMemoryStable();

    private:
        bool checkMemoryPage(PrepareMemoryPage *_page);

        void printErrorPage(PrepareMemoryPage *_page);

        void addPageToBuffer(std::map<RequireMemoryType, PrepareMemoryPage *> &_buffer, RequireMemoryType _type, PrepareMemoryPage *_page);

        PrepareMemoryPage *createPage(int _size, RequireMemoryType _memoryType);

        bool tryGetFreeMemoryFromBufferPages(MemoryUseType _use, int _size, int _alignment, RequireMemoryType _memoryType, DeviceMemory **_outMemoryNode);

        DeviceMemory *getFreeDeviceMemoryNode();

        void returnDeviceMemoryNode(DeviceMemory *_node);

        void allocateAdditionalDeviceMemoryNodes();

        void clearListPages(PrepareMemoryPage *_beginList);
    };
}

#endif