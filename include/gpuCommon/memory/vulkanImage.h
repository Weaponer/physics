#ifndef VULKAN_IMAGE_H
#define VULKAN_IMAGE_H

#include <vulkan/vulkan.h>

#include "gpuCommon/vulkanDevice.h"
#include "deviceMemory.h"

namespace phys::vk
{
    class VulkanImage
    {
    protected:
        VkImage image;
        DeviceMemory *memory;

        int size;
        VkImageUsageFlags usage;
        bool hostVisible;

        VkImageType imageType;
        VkFormat format;
        VkImageTiling tiling;
        VkImageAspectFlags aspect;

        int width;
        int height;
        int depth;

        int countMipmaps;
        int layers;

    public:
        VulkanImage()
            : image(VK_NULL_HANDLE), memory(NULL), size(0), usage(0), hostVisible(false),
              imageType(VK_IMAGE_TYPE_2D), format(VK_FORMAT_UNDEFINED), tiling(VK_IMAGE_TILING_OPTIMAL), aspect(VK_IMAGE_ASPECT_COLOR_BIT),
              width(1), height(1), depth(1), countMipmaps(1), layers(1) {}

        VulkanImage(VkImage _image, DeviceMemory *_memory, int _size, VkImageUsageFlags _usage, bool _hostVisible,
                    VkImageType _imageType, VkFormat _format, VkImageTiling _tiling, VkImageAspectFlags _aspect, int _width,
                    int _height, int _depth, int _countMipmaps, int _layers)
            : image(_image), memory(_memory), size(_size), usage(_usage), hostVisible(_hostVisible),
              imageType(_imageType), format(_format), tiling(_tiling), aspect(_aspect),
              width(_width), height(_height), depth(_depth), countMipmaps(_countMipmaps), layers(_layers) {}

        virtual ~VulkanImage() {}

        inline bool isValid() const { return image != VK_NULL_HANDLE; }

        inline DeviceMemory *getDeviceMemory() { return memory; }

        inline int getSize() const { return size; }

        inline VkImageUsageFlags getUsage() const { return usage; }

        inline bool isHostVisible() const { return hostVisible; }

        inline VkImageType getImageType() const { return imageType; }

        inline VkFormat getFormat() const { return format; }

        inline VkImageTiling getImageTiling() const { return tiling; }

        inline VkImageAspectFlags getAspect() const { return aspect; }

        inline int getWidth() const { return width; }

        inline int getHeight() const { return height; }

        inline int getDepth() const { return depth; }

        inline int getCountMipmaps() const { return countMipmaps; }

        inline int getLayers() const { return layers; }

        operator VkImage() { return image; }
    };

    class VulkanImageView
    {
    private:
        VkImageView view;

    public:
        VulkanImageView() : view(VK_NULL_HANDLE) {}

        VulkanImageView(VkImageView _view) : view(_view) {}

        ~VulkanImageView() {}

        bool isValid() const { return view != VK_NULL_HANDLE; }

        static bool createImageView(VulkanDevice *_device, VkImageViewType _viewType, VulkanImage *_image, VulkanImageView *_outImageView,
                                    const char *_name = NULL,
                                    VkComponentSwizzle _componentR = VK_COMPONENT_SWIZZLE_IDENTITY, VkComponentSwizzle _componentG = VK_COMPONENT_SWIZZLE_IDENTITY,
                                    VkComponentSwizzle _componentB = VK_COMPONENT_SWIZZLE_IDENTITY, VkComponentSwizzle _componentA = VK_COMPONENT_SWIZZLE_IDENTITY);

        static void destroyImageView(VulkanDevice *_device, VulkanImageView _imageView);

        operator VkImageView() { return view; }
    };

    class VulkanSampler
    {
    private:
        VulkanDevice *device;
        VkSamplerCreateInfo createInfo;
        VkSampler sampler;

    public:
        VulkanSampler(VulkanDevice *_device);

        ~VulkanSampler();

        bool update();

        VkFilter getMagFilter() const { return createInfo.magFilter; }

        void setMagFilter(VkFilter _filter) { createInfo.magFilter = _filter; }

        VkFilter getMinFilter() const { return createInfo.minFilter; }

        void setMinFilter(VkFilter _filter) { createInfo.minFilter = _filter; }

        VkSamplerMipmapMode getMinmapMode() const { return createInfo.mipmapMode; }

        void setMinmapMode(VkSamplerMipmapMode _mode) { createInfo.mipmapMode = _mode; }

        VkSamplerAddressMode getAddressModeU() const { return createInfo.addressModeU; }

        void setAddressModeU(VkSamplerAddressMode _addressMode) { createInfo.addressModeU = _addressMode; }

        VkSamplerAddressMode getAddressModeV() const { return createInfo.addressModeV; }

        void setAddressModeV(VkSamplerAddressMode _addressMode) { createInfo.addressModeV = _addressMode; }

        VkSamplerAddressMode getAddressModeW() const { return createInfo.addressModeV; }

        void setAddressModeW(VkSamplerAddressMode _addressMode) { createInfo.addressModeW = _addressMode; }

        float getMipLodBias() const { return createInfo.mipLodBias; }

        void setMipLodBias(float _bias) { createInfo.mipLodBias = _bias; }

        VkBool32 getAnisotropyEnable() const { return createInfo.anisotropyEnable; }

        void setAnisotropyEnable(VkBool32 _enable) { createInfo.anisotropyEnable = _enable; }

        float getMaxAnisotropy() const { return createInfo.maxAnisotropy; }

        void setMaxAnisotropy(float _anisotropy) { createInfo.maxAnisotropy = _anisotropy; }

        VkBool32 getCompareEnable() const { return createInfo.compareEnable; }

        void setCompareEnable(VkBool32 _enable) { createInfo.compareEnable = _enable; }

        VkCompareOp getCompareMode() const { return createInfo.compareOp; }

        void setCompareMode(VkCompareOp _mode) { createInfo.compareOp = _mode; }

        float getMinLod() const { return createInfo.minLod; }

        void setMinLod(float _lod) { createInfo.minLod = _lod; }

        float getMaxLod() const { return createInfo.minLod; }

        void setMaxLod(float _lod) { createInfo.minLod = _lod; }

        VkBorderColor getBorderColor() const { return createInfo.borderColor; }

        void setBorderColor(VkBorderColor _color) { createInfo.borderColor = _color; }

        VkBool32 getUnnormalizedCoordinates() const { return createInfo.unnormalizedCoordinates; }

        void setUnnormalizedCoordinates(VkBool32 _enable) { createInfo.unnormalizedCoordinates = _enable; }

        bool isValid() const { return sampler != VK_NULL_HANDLE; }

        operator VkSampler() { return sampler; }
    };
}

#endif