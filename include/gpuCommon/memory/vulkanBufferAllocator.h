#ifndef VULKAN_BUFFER_ALLOCATOR_H
#define VULKAN_BUFFER_ALLOCATOR_H

#include <array>
#include <vulkan/vulkan.h>

#include "gpuMemoryController.h"
#include "gpuCommon/vulkanDevice.h"

#include "vulkanBuffer.h"
#include "memoryType.h"
#include "memoryTypeDescriptions.h"

namespace phys::vk
{
    class VulkanBufferAllocator
    {
    private:
        VulkanDevice *device;
        GpuMemoryController *memoryController;

        std::array<RequireMemoryType, VulkanBufferType_MaxEnum> typesBuffers;
        std::array<VkBufferUsageFlags, VulkanBufferType_MaxEnum> typesUsageBuffers;

    public:
        VulkanBufferAllocator(VulkanDevice *_device, GpuMemoryController *_memoryController);

        ~VulkanBufferAllocator();

        void setTypeMemoryForTypeBuffer(VulkanBufferType _typeBuffer, RequireMemoryType _requireTypeMemory);

        void setUsageForTypeBuffer(VulkanBufferType _typeBuffer, VkBufferUsageFlags _usage);

        VulkanBuffer *createBuffer(MemoryUseType _use, RequireMemoryType _memoryType, VkBufferUsageFlags _usage, int _size,
                                   int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name = NULL);

        VulkanBuffer *createBuffer(MemoryUseType _use, VulkanBufferType _type, int _size,
                                   int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name = NULL);

        void destroyBuffer(VulkanBuffer *_buffer);

        GpuMemoryController *getMemoryController() const { return memoryController; }
    };
}

#endif