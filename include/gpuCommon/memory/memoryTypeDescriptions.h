#ifndef MEMORY_TYPE_DESCRIPTIONS_H
#define MEMORY_TYPE_DESCRIPTIONS_H

#include <vulkan/vulkan.h>
#include <array>

#include "memoryType.h"

namespace phys::vk
{
    enum VulkanBufferType : int
    {
        VulkanBufferType_UniformBlockStatic = 0,
        VulkanBufferType_UniformBlockDynamic = 1,

        VulkanBufferType_UniformStorageBufferStatic = 2,
        VulkanBufferType_UniformStorageBufferDynamic = 3,

        VulkanBufferType_VertexBufferStatic = 4,
        VulkanBufferType_VertexBufferDynamic = 5,

        VulkanBufferType_IndexBufferStatic = 6,
        VulkanBufferType_IndexBufferDynamic = 7,

        VulkanBufferType_IndirectBufferStatic = 8,
        VulkanBufferType_IndirectBufferDynamic = 9,

        VulkanBufferType_Staging = 10,
        VulkanBufferType_MaxEnum = 11,
    };

    enum VulkanImageType : int
    {
        VulkanImageType_ImageStatic = 0,
        VulkanImageType_ImageDynamic = 1,

        VulkanImageType_Staging = 2,
        VulkanImageType_MaxEnum = 3,
    };

    class VulkanMemoryDescriptor
    {
    private:
        std::array<VkMemoryPropertyFlags, VulkanBufferType_MaxEnum> memoryPropertiesBuffers;
        std::array<VkMemoryHeapFlags, VulkanBufferType_MaxEnum> heapPropertiesBuffers;

        std::array<VkMemoryPropertyFlags, VulkanImageType_MaxEnum> memoryPropertiesImages;
        std::array<VkMemoryHeapFlags, VulkanImageType_MaxEnum> heapPropertiesImages;

    public:
        VulkanMemoryDescriptor();

        ~VulkanMemoryDescriptor();

        RequireMemoryType getRequireMemoryType(VulkanBufferType _bufferType) const;

        RequireMemoryType getRequireMemoryType(VulkanImageType _imageType) const;
    };
}

#endif