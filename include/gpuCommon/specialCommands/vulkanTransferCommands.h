#ifndef VULKAN_TRANSFER_COMMANDS_H
#define VULKAN_TRANSFER_COMMANDS_H

#include <vulkan/vulkan.h>

#include "vulkanSpecialCommands.h"
#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/memory/vulkanImage.h"

namespace phys::vk
{
    class VulkanTransferCommands : public VulkanSpecialCommands
    {
    public:
        VulkanTransferCommands(bool _useRenderCommands);

        VulkanTransferCommands(VulkanQueue *_queue, bool _useRenderCommands);

        VulkanTransferCommands(VulkanQueue *_queue, VulkanCommandPool *_commandPool, bool _useRenderCommands);

        virtual ~VulkanTransferCommands();

        // Olny host local
        void writeToBuffer(VulkanBuffer *_buffer, int _offset, int _size, const void *_data);
        
        // Olny host local
        void *beginWrite(VulkanBuffer *_buffer, int _offset, int _size);

        // Olny host local
        void endWrite(VulkanBuffer *_buffer);

        // Olny host local
        void readFromBuffer(VulkanBuffer *_buffer, int _offset, int _size, void *_data);

        void copyBuffer(VulkanBuffer *_srcBuffer, int _srcOffset, VulkanBuffer *_dstBuffer,
                        int _dstOffset, int _size);

        // Olny host local
        void writeToImage(VulkanImage *_image, int _offset, int _size, const void *_data);

        // Olny host local
        void readFromImage(VulkanImage *_image, int _offset, int _size, void *_data);

        void fillBuffer(VulkanBuffer *_buffer, int _offset, int _size, uint32_t _data);

        void copyImage(VulkanImage *_srcImage, VulkanImage *_dstImage, VkImageLayout _srcLayout, VkImageLayout _dstLayout, VkImageCopy *_copyInfo);

        void copyImage(VulkanImage *_srcImage, VulkanImage *_dstImage, int _mipLevel = 0, int _baseArrayLayer = 0,
                       int _layerCount = 1, VkOffset3D _srcOffset = {0, 0, 0}, VkOffset3D _dstOffset = {0, 0, 0});

        void copyImageToBuffer(VulkanImage *_image, VkImageLayout _srcLayout, VulkanBuffer *_buffer, VkBufferImageCopy *_copyInfo);

        void copyImageToBuffer(VulkanImage *_image, VulkanBuffer *_buffer, int _bufferOffset = 0, int _mipLevel = 0, int _baseArrayLayer = 0,
                               int _layerCount = 1, VkOffset3D _srcOffset = {0, 0, 0});

        void copyBufferToImage(VulkanBuffer *_buffer, VulkanImage *_image, VkImageLayout _srcLayout, VkBufferImageCopy *_copyInfo);

        void copyBufferToImage(VulkanBuffer *_buffer, VulkanImage *_image, int _bufferOffset = 0, int _mipLevel = 0, int _baseArrayLayer = 0,
                               int _layerCount = 1, VkOffset3D _dstOffset = {0, 0, 0});

        void clearImage(VulkanImage *_image, VkImageLayout _layout, const VkClearColorValue *_color, const VkImageSubresourceRange *_range);

        void clearImage(VulkanImage *_image, const VkClearColorValue *_color, int _baseMipLevel = 0, int _mipLevelCount = 1, int _baseArrayLayer = 0,
                        int _layerCount = 1);

        void clearDepthStencilImage(VulkanImage *_image, VkImageLayout _layout, const VkClearDepthStencilValue *_depthStencil, const VkImageSubresourceRange *_range);

        void clearDepthStencilImage(VulkanImage *_image, const VkClearDepthStencilValue *_depthStencil, int _baseMipLevel = 0, int _mipLevelCount = 1, int _baseArrayLayer = 0,
                                    int _layerCount = 1);
    };
}

#endif