#ifndef VULKAN_STATE_CONTAINERS_H
#define VULKAN_STATE_CONTAINERS_H

#include <vulkan/vulkan.h>
#include <vector>

#include "gpuCommon/memory/vulkanBuffer.h"

namespace phys::vk
{
    enum SpaceUsing : int
    {
        SpaceUsing_Local = 0,
        SpaceUsing_Global = 1,
    };

    struct BufferState
    {
        VkAccessFlags accessFlags;
        int queueFamilyIndex;
        SpaceUsing spaceUsing;
    };

    class BufferStateContainer;
    struct VulkanBufferDescriptor
    {
    private:
        BufferState *state;
        VulkanBuffer *vulkanBuffer;

    public:
        BufferState *getState() { return state; };
        VulkanBuffer *getBuffer() const { return vulkanBuffer; }

        operator VulkanBuffer *() { return vulkanBuffer; }

        friend class BufferStateContainer;
    };

    class BufferStateContainer
    {
    private:
        std::vector<BufferState *> states;

    public:
        VulkanBufferDescriptor addBuffer(VulkanBuffer *buffer, VkAccessFlags _accessFlags,
                                         int _queueFamilyIndex, SpaceUsing _spaceUsing);

        void clear();

        void reserve(int _count) { states.reserve(_count); }
    };
}

#endif
