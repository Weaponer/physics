#ifndef VULKAN_COMMAND_RECEIVER_H
#define VULKAN_COMMAND_RECEIVER_H

#include <vector>

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/memory/vulkanBufferAllocator.h"

#include "gpuCommon/vulkanFence.h"
#include "gpuCommon/vulkanSemaphore.h"

#include "vulkanSpecialCommands.h"

namespace phys::vk
{
    struct VulkanQueueDescriptor
    {
        VulkanQueue *queue;
        bool used;
    };

    class VulkanCommandReceiver
    {
    private:
        VulkanDevice *device;

        std::vector<VulkanQueueDescriptor> bufferQueues;

    public:
        VulkanCommandReceiver(VulkanDevice *_device);

        ~VulkanCommandReceiver();

        void addQueue(VulkanQueue *_queue);

        VulkanQueue *getFreeQueue(VkQueueFlags _functions);

        void returnQueue(VulkanQueue *_queue);

        void submitCommands(VulkanSpecialCommands *_commands, VulkanFence _fence = VulkanFence());

        VulkanDevice *getDevice() const { return device; }
    };
}

#endif