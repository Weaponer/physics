#ifndef VULKAN_SPECIAL_COMMANDS_H
#define VULKAN_SPECIAL_COMMANDS_H

#include <vulkan/vulkan.h>

#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanCommandPool.h"

#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/memory/vulkanImage.h"

namespace phys::vk
{
    class VulkanSpecialCommands
    {
    protected:
        VulkanQueue *vulkanQueue;
        VulkanDevice *device;
        VulkanCommandPool *vulkanCommandPool;
        VulkanCommandBuffer cmd;

        int countWaitSemaphores;
        std::array<VulkanSemaphore, MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER> bufferWaitSemaphores;
        std::array<VkPipelineStageFlags, MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER> bufferStageSemaphores;

        VulkanSemaphore signalSemaphore;
        bool useRenderCommands;

    public:
        VulkanSpecialCommands(bool _useRenderCommands);

        VulkanSpecialCommands(VulkanQueue *_queue, bool _useRenderCommands);

        VulkanSpecialCommands(VulkanQueue *_queue, VulkanCommandPool *_commandPool, bool _useRenderCommands);

        virtual ~VulkanSpecialCommands();

        inline bool isValid() const { return vulkanQueue != NULL && cmd.isValid(); };

        inline VulkanQueue *getQueue() const { return vulkanQueue; }

        inline VulkanCommandPool *getCommandPool() const { return vulkanCommandPool; }

        void resetCommandBuffer();

        void updateCommandBuffer();

        void endCommandBuffer();

        void addWaitSemaphoreCmd(VulkanSemaphore _waitSemaphore, VkPipelineStageFlags _waitStage);

        void moveBufferInQueueFamily(VulkanBuffer *_buffer, int _srcQueueFamily, int _dstQueueFamily,
                                     VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                     VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask);

        void barrierBufferAccess(VulkanBuffer *_buffer, VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                 VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask);

        void moveImageInQueueFamily(VulkanImage *_image, int _srcQueueFamily, int _dstQueueFamily,
                                    VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                    VkImageLayout _srcLayout, VkImageLayout _dstLayout,
                                    VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask);

        void barrierImageAccess(VulkanImage *_image, VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                VkImageLayout _srcLayout, VkImageLayout _dstLayout, VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask);

        void barrierImagesAccess(uint32_t _countImages, VulkanImage **_images, VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                 VkImageLayout _srcLayout, VkImageLayout _dstLayout, VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask);

        void pipelineBarrier(VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask, VkDependencyFlags _dependencyFlags,
                             int _memoryBarrierCount, const VkMemoryBarrier *_memoryBarriers, int _bufferMemoryBarrierCount,
                             const VkBufferMemoryBarrier *_bufferMemoryBarriers, int _imageMemoryBarrierCount, const VkImageMemoryBarrier *_imageMemoryBarriers);

        void pipelineBarrier(VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask);

        void debugMarkerBegin(const char *_name, float *_col);

        void debugMarkerInsert(const char *_name, float *_col);

        void debugMarkerEnd();

        void setSignalSemaphoreCmd(VulkanSemaphore _signalSemaphore) { signalSemaphore = _signalSemaphore; }

        VulkanSemaphore getSignalSemaphoreCmd() const { return signalSemaphore; }

        int getCountWaitSemaphoreCmd() const { return countWaitSemaphores; }

        std::pair<VulkanSemaphore, VkPipelineStageFlags> getWaitSemaphoreCmd(int _index) const { return std::make_pair(bufferWaitSemaphores[_index], bufferStageSemaphores[_index]); }
    };
}
#endif