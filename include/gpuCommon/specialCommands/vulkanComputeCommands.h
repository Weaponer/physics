#ifndef VULKAN_COMPUTE_COMMANDS_H
#define VULKAN_COMPUTE_COMMANDS_H

#include <vulkan/vulkan.h>

#include "vulkanTransferCommands.h"
#include "gpuCommon/memory/vulkanBuffer.h"

namespace phys::vk
{
    class VulkanComputeCommands : public VulkanTransferCommands
    {
    public:
        VulkanComputeCommands();

        VulkanComputeCommands(VulkanQueue *_queue);

        VulkanComputeCommands(VulkanQueue *_queue, VulkanCommandPool *_commandPool);

        void bindPipeline(VkPipeline _pipeline);

        void bindDescriptorSet(VkPipelineLayout _layout, VkDescriptorSet _set, int _indexBinding);

        void dispatch(uint32_t _groupCountX, uint32_t _groupCountY, uint32_t _groupCountZ);

        void dispatchIndirect(VulkanBuffer *_buffer, VkDeviceSize _offset);

        void pushConstant(VkPipelineLayout _layout, uint32_t _offset, uint32_t _size, const void *_pValues);
    };
}

#endif