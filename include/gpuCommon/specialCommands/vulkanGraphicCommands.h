#ifndef VULKAN_GRAPHIC_COMMANDS_H
#define VULKAN_GRAPHIC_COMMANDS_H

#include <vulkan/vulkan.h>

#include "vulkanTransferCommands.h"
#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/rendering/vulkanFramebuffer.h"
#include "gpuCommon/rendering/vulkanRenderPass.h"

namespace phys::vk
{
    class VulkanGraphicCommands : public VulkanTransferCommands
    {
    public:
        VulkanGraphicCommands();

        VulkanGraphicCommands(VulkanQueue *_queue);

        VulkanGraphicCommands(VulkanQueue *_queue, VulkanCommandPool *_commandPool);

        void beginRenderPass(VulkanRenderPass &_renderPass, VulkanFramebuffer &_framebuffer);

        void beginRenderPass(VulkanRenderPass &_renderPass, VulkanFramebuffer &_framebuffer,
                             int _countClearValues, const VkClearValue *_values);

        void endRenderPass();

        void setViewport(uint32_t _firstViewport, uint32_t _coutViewports, const VkViewport *_viewports);

        void setScissor(uint32_t _firstScissor, uint32_t _coutScissors, const VkRect2D *_scissors);

        void setDefaultViewport(const VulkanFramebuffer &_framebuffer);

        void draw(uint32_t _countVertex, uint32_t _countInstance, uint32_t _firstVertex, uint32_t _firstInstance);

        void drawIndirect(VulkanBuffer *_buffer, VkDeviceSize _offset, uint32_t _drawCount, uint32_t _stride);

        void drawIndexed(uint32_t _countIndexes, uint32_t _countInstance, uint32_t _firstIndex,
                         uint32_t _vertexOffset, uint32_t _firstInstance);

        void drawIndexedIndirect(VulkanBuffer *_buffer, VkDeviceSize _offset, uint32_t _drawCount, uint32_t _stride);

        void bindPipeline(VkPipeline _pipeline);

        void bindDescriptorSet(VkPipelineLayout _layout, VkDescriptorSet _set, int _indexBinding);

        void pushConstant(VkPipelineLayout _layout, VkShaderStageFlags _stageFlags, uint32_t _offset, uint32_t _size, const void *_pValues);

        void bindVertexBuffer(VulkanBuffer *_buffer, VkDeviceSize _offset);

        void bindInstanceBuffer(VulkanBuffer *_buffer, VkDeviceSize _offset);

        void bindIndexBuffer(VulkanBuffer *_buffer, VkDeviceSize _offset, VkIndexType _type);
    };
}

#endif