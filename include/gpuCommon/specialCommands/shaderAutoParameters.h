#ifndef SHADER_AUTO_PARAMETERS_H
#define SHADER_AUTO_PARAMETERS_H

#include <vulkan/vulkan.h>
#include <vector>
#include "gpuCommon/shaders/shaderParameters.h"
#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/memory/vulkanImage.h"

namespace phys::vk
{
    class ShaderAutoParameters : public ShaderParameters
    {
    private:
        struct ParameterBase
        {
            int binding;
            bool wasUpdate;
        };

        enum Parameters
        {
            Parameter_UniformBuffer = 0,
            Parameter_StorageBuffer = 1,
            Parameter_StorageImage = 2,
            Parameter_SampledImage = 3,
            ParametersCount = 4,
        };

    public:
        struct UniformBuffer : protected ParameterBase
        {
        private:
            typedef VulkanBuffer *ParameterType;
            enum
            {
                type = Parameter_UniformBuffer
            };
            std::vector<ParameterType> datas;

            friend class ShaderAutoParameters;
        };

        struct StorageBuffer : protected ParameterBase
        {
        private:
            typedef VulkanBuffer *ParameterType;
            enum
            {
                type = Parameter_StorageBuffer
            };
            std::vector<ParameterType> datas;

            friend class ShaderAutoParameters;
        };

        struct StorageImage : protected ParameterBase
        {
        private:
            typedef VulkanImageView ParameterType;
            enum
            {
                type = Parameter_StorageImage
            };
            std::vector<ParameterType> datas;

            friend class ShaderAutoParameters;
        };

        struct SampledImage : protected ParameterBase
        {
        private:
            typedef VulkanImageView ParameterType;
            typedef VulkanSampler *ParameterType_2;
            enum
            {
                type = Parameter_SampledImage
            };
            std::vector<ParameterType> datas;
            std::vector<ParameterType_2> datas_2;

            friend class ShaderAutoParameters;
        };

    protected:
        struct SetAutoParameters
        {
            VkDescriptorSet previousSet;
            VkDescriptorSet currentSet;
            std::array<int, ParametersCount> countMainParameters;
            std::array<int, ParametersCount> countFullDescriptors;
            std::array<void *, ParametersCount> parameters;
            int countCopyInfos;
            VkCopyDescriptorSet *copyInfos;
        };

    protected:
        std::vector<SetAutoParameters> autoSets;

        bool isBeginUpdate;

    public:
        ShaderAutoParameters();

        ShaderAutoParameters(VulkanDevice *_device, ShaderModule _shader);

        virtual ~ShaderAutoParameters();

        void beginUpdate();

        void endUpdate();

        template <typename T>
        typename T::ParameterType getParameter(int _indexSet, int _indexParameter, int _arrElement = 0) const
        {
            void *params = autoSets[_indexSet].parameters[T::type];
            return reinterpret_cast<T *>(params)[_indexParameter].datas[_arrElement];
        }

        template <typename T>
        typename T::ParameterType_2 getParameterAdditionalData(int _indexSet, int _indexParameter, int _arrElement = 0) const
        {
            void *params = autoSets[_indexSet].parameters[T::type];
            return reinterpret_cast<T *>(params)[_indexParameter].datas_2[_arrElement];
        }

        template <typename T>
        void setParameter(int _indexSet, int _indexParameter, typename T::ParameterType _data, int _arrElement = 0)
        {
            void *params = autoSets[_indexSet].parameters[T::type];
            T &param = reinterpret_cast<T *>(params)[_indexParameter];
            param.datas[_arrElement] = _data;
            param.wasUpdate = isBeginUpdate;
        }

        template <typename T>
        void setParameter(int _indexSet, int _indexParameter, typename T::ParameterType _data, typename T::ParameterType_2 _data_2, int _arrElement = 0)
        {
            void *params = autoSets[_indexSet].parameters[T::type];
            T &param = reinterpret_cast<T *>(params)[_indexParameter];
            param.datas[_arrElement] = _data;
            param.datas_2[_arrElement] = _data_2;
            param.wasUpdate = isBeginUpdate;
        }

        void updateSet(int _indexSet);

        void updateSet();

        void clearParameters(int _indexSet);

        void clearParameters();

    protected:
        virtual void onSwitchSet(int _indexSet, VkDescriptorSet _previousSet) override;

        virtual void onReset() override;

    private:
        void updateSet(int _indexSet, bool _onlyChanged);

        void updateSetFillUnfiromBuffers(SetAutoParameters *_set, VkDescriptorSet _currentSet, bool _onlyChanged,
                                         VkWriteDescriptorSet *_writeDescriptorSets, uint32_t *_countUseWritings,
                                         VkDescriptorBufferInfo *_bufferInfos, uint32_t *_countUseBufferInfos);

        void updateSetFillStorageBuffers(SetAutoParameters *_set, VkDescriptorSet _currentSet, bool _onlyChanged,
                                         VkWriteDescriptorSet *_writeDescriptorSets, uint32_t *_countUseWritings,
                                         VkDescriptorBufferInfo *_bufferInfos, uint32_t *_countUseBufferInfos);

        void updateSetFillStorageImage(SetAutoParameters *_set, VkDescriptorSet _currentSet, bool _onlyChanged,
                                       VkWriteDescriptorSet *_writeDescriptorSets, uint32_t *_countUseWritings,
                                       VkDescriptorImageInfo *_imageInfos, uint32_t *_countUseImageInfos);

        void updateSetFillSampledImage(SetAutoParameters *_set, VkDescriptorSet _currentSet, bool _onlyChanged,
                                       VkWriteDescriptorSet *_writeDescriptorSets, uint32_t *_countUseWritings,
                                       VkDescriptorImageInfo *_imageInfos, uint32_t *_countUseImageInfos);
    };

    typedef ShaderAutoParameters::UniformBuffer ShaderParameterUniformBuffer;
    typedef ShaderAutoParameters::StorageBuffer ShaderParameterStorageBuffer;
    typedef ShaderAutoParameters::SampledImage ShaderParameterSampledImage;
    typedef ShaderAutoParameters::StorageImage ShaderParameterStorageImage;
}

#endif