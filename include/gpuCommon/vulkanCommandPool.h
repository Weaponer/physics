#ifndef VULKAN_COMMAND_POOL_H
#define VULKAN_COMMAND_POOL_H

#include <vulkan/vulkan.h>
#include <array>
#include <vector>

#include "vulkanCommandBuffer.h"
#include "vulkanSemaphore.h"

namespace phys::vk
{
#define SIZE_ALLOCATE_COMMAND_BUFFERS 5
#define MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER 16

    class VulkanDevice;
    class VulkanCommandPool
    {
    private:
        struct CommandBufferNode
        {
            VulkanCommandBuffer commandBuffer;
            std::pair<VulkanSemaphore, VkPipelineStageFlags> *waitSemaphores;
            int countWaitSemaphores;
            VulkanSemaphore signalSemaphore;

            CommandBufferNode *next;

            CommandBufferNode() : commandBuffer(VK_NULL_HANDLE, NULL), waitSemaphores(NULL), countWaitSemaphores(0), signalSemaphore(NULL, VK_NULL_HANDLE), next(NULL) {}

            ~CommandBufferNode() {}
        };

    private:
        VkCommandPool commandPool;
        VulkanDevice *device;

        int countReservedCommandBuffers;
        std::array<VkCommandBuffer, SIZE_ALLOCATE_COMMAND_BUFFERS> reservedCommandBuffers;

        std::vector<std::array<std::pair<VulkanSemaphore, VkPipelineStageFlags>, MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER> *> waitSemaphoresBuffer;
        int indexWaitSemaphoresBuffer;
        int freeWaitSemaphores;

        CommandBufferNode *beginFreeIterators;
        CommandBufferNode *beginFreeCommandBuffers;
        CommandBufferNode *beginWritedCommandBuffers;
        CommandBufferNode *endWritedCommandBuffers;
        bool lastCmdIsEndSubmitPart;
        int countWritedCommandBuffers;
        int countSubmitParts;
        int countWaitSemaphores;
        int countSignalSemaphores;

    public:
        VulkanCommandPool(VkCommandPool _commandPool, VulkanDevice *_device);

        ~VulkanCommandPool();

        operator VkCommandPool() { return commandPool; };

        void reset();

        VulkanCommandBuffer beginCommandBuffer(bool _useRenderings = false);

        void endCommandBuffer(VulkanCommandBuffer _commandBuffer, int _countWaitSemaphore = 0, VulkanSemaphore *_waitSemaphores = NULL,
                              VkPipelineStageFlags *_waitStages = NULL,
                              VulkanSemaphore _signalSemaphore = VulkanSemaphore());

        int fillQueueSubmitInfo(int _sizeSubmitInfosBuffer, VkSubmitInfo *_submitInfosBuffer,
                                int _sizeCommanBuffersBuffer, VkCommandBuffer *_commandBuffersBuffer,
                                int _sizeWaitSemaphoresBuffer, VkSemaphore *_waitSemaphoresBuffer, VkPipelineStageFlags *_waitStagesBuffer,
                                int _sizeSignalSemaphoresBuffer, VkSemaphore *_signalSemaphoresBuffer);

        int getWritedCommandBuffers(int _count, VulkanCommandBuffer **_bufferCommandBuffers);

        int getCountWritedCommandBuffers() const { return countWritedCommandBuffers; }

        int getCountSubmitParts() const { return countSubmitParts; };

        int getCountWaitSemaphores() const { return countWaitSemaphores; }

        int getCountSignalSemaphores() const { return countSignalSemaphores; }

    private:
        CommandBufferNode *getFreeNode();

        void returnFreeNode(CommandBufferNode *_node);

        VulkanCommandBuffer getFreeCommandBuffer();
        
        void returnFreeCommandBuffer(VulkanCommandBuffer _cmd);

        void clearListNodes(CommandBufferNode *_list);

        std::pair<VulkanSemaphore, VkPipelineStageFlags> *getWaitSemaphores(int _count);

        void resetWaitSemaphoresBuffer();
    };
}
#endif