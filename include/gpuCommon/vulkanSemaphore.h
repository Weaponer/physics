#ifndef VULKAN_SEMAPHORE_H
#define VULKAN_SEMAPHORE_H

#include <vulkan/vulkan.h>
namespace phys::vk
{
    class VulkanDevice;

    class VulkanSemaphore
    {
    private:
        VulkanDevice *device;
        VkSemaphore semaphore;

    public:
        VulkanSemaphore() : device(NULL), semaphore(VK_NULL_HANDLE) {}

        VulkanSemaphore(VulkanDevice *_device, VkSemaphore _semaphore) : device(_device), semaphore(_semaphore) {}

        ~VulkanSemaphore() {}

        bool isValid() const { return semaphore != VK_NULL_HANDLE; }

        VulkanDevice *getDevice() const { return device; }

        static bool createSemaphore(VulkanDevice *_device, VulkanSemaphore *_outSemaphore, const char *_name = NULL);

        static void destroySemaphore(VulkanSemaphore *_semaphore);

        operator VkSemaphore() { return semaphore; }
    };
}

#endif