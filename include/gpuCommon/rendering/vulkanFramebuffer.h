#ifndef VULKAN_FRAMEBUFFER_H
#define VULKAN_FRAMEBUFFER_H

#include <vulkan/vulkan.h>

#include "vulkanRenderPass.h"
#include "gpuCommon/memory/vulkanImage.h"

namespace phys::vk
{
    class VulkanFramebuffer
    {
    private:
        VkFramebuffer frameBuffer;
        int width;
        int height;

    public:
        VulkanFramebuffer() : frameBuffer(VK_NULL_HANDLE), width(0), height(0) {}

        VulkanFramebuffer(VkFramebuffer _frameBuffer, int _width, int _height)
            : frameBuffer(_frameBuffer), width(_width), height(_height) {}

        ~VulkanFramebuffer() {}

        bool isValid() const { return frameBuffer != VK_NULL_HANDLE; }

        int getWidth() const { return width; }

        int getHeight() const { return height; }

        static bool createFramebuffer(VulkanDevice *_device, VulkanRenderPass &_renderPass, int _countColorAttachments,
                                      VulkanImage **_imagesColor, VulkanImageView *_viewsColor,
                                      VulkanImage *_imageDepthStencil, VulkanImageView _viewDepthStencil,
                                      VulkanFramebuffer *_outFramebuffer);

        static void destroyFramebuffer(VulkanDevice *_device, VulkanFramebuffer *_framebuffer);

        operator VkFramebuffer() { return frameBuffer; }
    };
}

#endif