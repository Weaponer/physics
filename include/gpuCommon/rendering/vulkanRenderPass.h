#ifndef VULKAN_RENDER_PASS_H
#define VULKAN_RENDER_PASS_H

#include <vulkan/vulkan.h>

namespace phys::vk
{
    class VulkanRenderPass
    {
    private:
        VkRenderPass renderPass;
        int index;

    public:
        VulkanRenderPass() : renderPass(VK_NULL_HANDLE), index(-1) {}

        VulkanRenderPass(VkRenderPass _renderPass, int _index) : renderPass(_renderPass), index(_index){};

        ~VulkanRenderPass() {}

        int getIndex() const { return index; }

        bool isValid() const { return renderPass != VK_NULL_HANDLE; }

        operator VkRenderPass() { return renderPass; }
    };
}

#endif