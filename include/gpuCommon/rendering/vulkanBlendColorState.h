#ifndef VULKAN_BLEND_COLOR_STATE_H
#define VULKAN_BLEND_COLOR_STATE_H

#include <vulkan/vulkan.h>
#include <array>

namespace phys::vk
{
#define MAX_COLOR_ATTACHMENTS 32

    class VulkanBlendColorState
    {
    private:
        VkPipelineColorBlendStateCreateInfo colorBlendingState;
        std::array<VkPipelineColorBlendAttachmentState, MAX_COLOR_ATTACHMENTS> blends;

    public:
        VulkanBlendColorState();

        ~VulkanBlendColorState();

        int getCountColorAttachments() const { return colorBlendingState.attachmentCount; }

        void setCountColorAttachments(int _count) { colorBlendingState.attachmentCount = _count; }

        VkPipelineColorBlendAttachmentState getColorBlendAttachment(int _index) const { return blends[_index]; }

        bool getLogicEnable() const { return colorBlendingState.logicOpEnable; }

        void setLogicEnable(bool _enable) { colorBlendingState.logicOpEnable = _enable; }

        VkLogicOp getLogicOp() const { return colorBlendingState.logicOp; }

        void setLogicOp(VkLogicOp _op) { colorBlendingState.logicOp = _op; }

        // RGBA
        std::array<float, 4> getBlendConst() const { return {colorBlendingState.blendConstants[0], colorBlendingState.blendConstants[1],
                                                             colorBlendingState.blendConstants[2], colorBlendingState.blendConstants[3]}; }

        void setBlendConst(float _r, float _g, float _b, float _a)
        {
            colorBlendingState.blendConstants[0] = _r;
            colorBlendingState.blendConstants[1] = _g;
            colorBlendingState.blendConstants[2] = _b;
            colorBlendingState.blendConstants[3] = _a;
        }

        void setColorBlendAttachmentState(int _index, VkColorComponentFlags _maskWrite, bool _blendEnable,
                                          VkBlendOp _colorBlendOp, VkBlendOp _alphaBlendOp,
                                          VkBlendFactor _srcColor, VkBlendFactor _srcAlpha,
                                          VkBlendFactor _dstColor, VkBlendFactor _dstAlpha);

        operator VkPipelineColorBlendStateCreateInfo *() const { return const_cast<VkPipelineColorBlendStateCreateInfo *>(&colorBlendingState); }
    };
}

#endif