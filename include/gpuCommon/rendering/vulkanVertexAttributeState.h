#ifndef VULKAN_VERTEX_ATTRIBUTE_STATE_H
#define VULKAN_VERTEX_ATTRIBUTE_STATE_H

#include <vulkan/vulkan.h>
#include <array>

namespace phys::vk
{
#define MAX_INPUT_ATTRIBUTES 32

    class VulkanVertexAttributeState
    {
    private:
        VkPipelineVertexInputStateCreateInfo vertexInfoState;
        std::array<VkVertexInputAttributeDescription, MAX_INPUT_ATTRIBUTES> vertexInputAttributes;
        std::array<VkVertexInputBindingDescription, 2> vertexInputBindings; // 0 - per vertex, 1 - per instance
        int countInputAttributes;

    public:
        VulkanVertexAttributeState();

        ~VulkanVertexAttributeState();

        bool getEnableAttributtesInstance() const { return vertexInfoState.vertexBindingDescriptionCount > 1; }

        void setEnableAttributessInstance(bool _enable) { vertexInfoState.vertexBindingDescriptionCount = _enable ? 2 : 1; }

        uint32_t getSizeInputVertex() const { return vertexInputBindings[0].stride; }

        void setSizeInputVertex(uint32_t _size) { vertexInputBindings[0].stride = _size; }

        uint32_t getSizeInputInstance() const { return vertexInputBindings[1].stride; }

        void setSizeInputInstance(uint32_t _size) { vertexInputBindings[1].stride = _size; }

        bool addInputVertexAttribute(uint32_t _offset, VkFormat _format);

        bool addInputInstanceAttribute(uint32_t _offset, VkFormat _format);

        bool addInputInstanceAttributeMat4Float(uint32_t _offset);
        
        bool addInputInstanceAttributeMat3Float(uint32_t _offset);

        void clearAttributes();

        operator VkPipelineVertexInputStateCreateInfo *() const { return const_cast<VkPipelineVertexInputStateCreateInfo *>(&vertexInfoState); }
    };
}

#endif