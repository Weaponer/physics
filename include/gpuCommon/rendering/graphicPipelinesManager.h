#ifndef GRAPHIC_PIPELINES_MANAGER_H
#define GRAPHIC_PIPELINES_MANAGER_H

#include <vulkan/vulkan.h>

#include "gpuCommon/vulkanDevice.h"
#include "vulkanRenderPass.h"
#include "vulkanGraphicPipeline.h"

namespace phys::vk
{
    class GraphicPipelinesManager
    {
    private:
        struct GraphicPipelineNode
        {
            VulkanGraphicPipeline *graphicPipeline;
            GraphicPipelineNode *next;
        };

    private:
        VulkanDevice *device;
        VkPipelineCache cache;

        GraphicPipelineNode *freePipelines;
        GraphicPipelineNode *freeNodes;

    public:
        GraphicPipelinesManager(VulkanDevice *_device);

        ~GraphicPipelinesManager();

        VulkanGraphicPipeline *createGraphicPipeline();

        void deleteGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline);

        void updateGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline, VulkanRenderPass _renderPass);

        void updateGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline);

        void clearCacheGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline);

        void clearCache();

    private:
        void fillDynamicStates(VulkanGraphicPipeline *_graphicPipeline, VkDynamicState *_dynamicState);

        void clearGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline);

        VulkanGraphicPipeline *getFreePipeline();

        void returnFreePipeline(VulkanGraphicPipeline *_pipeline);

        GraphicPipelineNode *getFreeNode();

        void returnNode(GraphicPipelineNode *_node);

        void clearListNode(GraphicPipelineNode *_begin);
    };
}

#endif