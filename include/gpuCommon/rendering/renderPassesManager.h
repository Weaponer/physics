#ifndef RENDER_PASSES_MANAGER_H
#define RENDER_PASSES_MANAGER_H

#include <vulkan/vulkan.h>

#include "gpuCommon/vulkanDevice.h"

#include "vulkanFramebuffer.h"
#include "vulkanRenderPass.h"
#include "vulkanBlendColorState.h"

#include "gpuCommon/memory/vulkanImage.h"

namespace phys::vk
{

    class RenderPassesManager
    {
    private:
        struct Attachment
        {
            VkFormat format;
            VkImageLayout initialLayout;
            VkImageLayout finalLayout;

            bool operator==(const Attachment &_a)
            {
                return _a.format == format && _a.initialLayout == initialLayout && _a.finalLayout == finalLayout;
            }

            bool operator!=(const Attachment &_a)
            {
                return _a.format != format || _a.initialLayout != initialLayout || _a.finalLayout != finalLayout;
            }
        };

        struct RenderPassDescription
        {
            int countColorAttachments;
            std::array<Attachment, MAX_COLOR_ATTACHMENTS> colorAttachments;
            Attachment depthStencilAttachment;

            VulkanRenderPass renderPass;
            RenderPassDescription *next;
        };

    private:
        VulkanDevice *device;

        std::array<Attachment, MAX_COLOR_ATTACHMENTS> colorAttachments;
        int maxColorAttachments;
        int countColorAttachments;
        Attachment depthStencilAttachment;

        bool beginCreate;

    private:
        RenderPassDescription *renderPasses;
        int countRenderPasses;

    public:
        RenderPassesManager(VulkanDevice *_device);

        ~RenderPassesManager();

        void beginCreateRenderPass();

        bool endCreate(VulkanRenderPass *_outRenderPass);

        void setDepthStencil(VulkanImage *_depthStencil, VkImageLayout _initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                             VkImageLayout _finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

        void addColorAttachments(VulkanImage *_color, VkImageLayout _initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                 VkImageLayout _finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

    private:
        RenderPassDescription *tryGetCompatibilityRenderPass(RenderPassDescription *_descriptor);

        RenderPassDescription *createNewRenderPass(RenderPassDescription *_descriptor);
    };
}

#endif