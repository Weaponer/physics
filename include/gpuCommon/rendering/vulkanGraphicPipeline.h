#ifndef VULKAN_GRAPHIC_PIPELINE_H
#define VULKAN_GRAPHIC_PIPELINE_H

#include <vulkan/vulkan.h>
#include <map>
#include <array>

#include "vulkanVertexAttributeState.h"
#include "vulkanBlendColorState.h"
#include "vulkanRenderPass.h"
#include "gpuCommon/shaders/vulkanPipelineLayout.h"

class GraphicPipelinesManager;

namespace phys::vk
{
    class VulkanGraphicPipeline
    {
    private:
        VkGraphicsPipelineCreateInfo createInfo;
        VkPipelineTessellationStateCreateInfo tessellationState;
        VkPipelineMultisampleStateCreateInfo multisamplingState;
        VkViewport viewport;
        VkRect2D scissor;
        VkPipelineViewportStateCreateInfo viewportState;
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyState;
        VkPipelineRasterizationStateCreateInfo rasterizerInfoState;
        VkPipelineDepthStencilStateCreateInfo depthStencilState;
        const VulkanVertexAttributeState *vertexAttributeState;
        const VulkanBlendColorState *blendColorState;
        VkPipelineDynamicStateCreateInfo dynamicStates;
        std::array<bool, VK_DYNAMIC_STATE_STENCIL_REFERENCE + 1> enabledDynamicStates;
        int countEnableDynamicStates;
        std::array<VkPipelineShaderStageCreateInfo, 2> stagesModuls;
        ShaderModule vertexModule;
        ShaderModule fragmentModule;

        std::map<int, std::pair<VkPipeline, VulkanRenderPass>> pipelines;

    public:
        VulkanGraphicPipeline();

        ~VulkanGraphicPipeline();

        VkViewport getViewport() const { return viewport; }

        void setViewport(VkViewport _viewport) { viewport = _viewport; }

        VkRect2D getScissor() const { return scissor; }

        void setScissor(VkRect2D _scissor) { scissor = _scissor; }

        VkPrimitiveTopology getTopology() const { return inputAssemblyState.topology; }

        void setTopology(VkPrimitiveTopology _topology) { inputAssemblyState.topology = _topology; }

        VkPolygonMode getPolygonMode() const { return rasterizerInfoState.polygonMode; }

        void setPolygonMode(VkPolygonMode _polygonMode) { rasterizerInfoState.polygonMode = _polygonMode; }

        float getLineWidth() const { return rasterizerInfoState.lineWidth; }

        void setLineWidth(float _width) { rasterizerInfoState.lineWidth = _width; }

        VkCullModeFlags getCullMode() const { return rasterizerInfoState.cullMode; }

        void setCullMode(VkCullModeFlags _cullMode) { rasterizerInfoState.cullMode = _cullMode; }

        VkFrontFace getFrontFace() const { return rasterizerInfoState.frontFace; }

        void setFrontFace(VkFrontFace _frontFace) { rasterizerInfoState.frontFace = _frontFace; }

        bool getDepthWrite() const { return depthStencilState.depthWriteEnable; }

        void setDepthWrite(bool _enable) { depthStencilState.depthWriteEnable = _enable; }

        bool getDepthTest() const { return depthStencilState.depthTestEnable; }

        void setDepthTest(bool _enable) { depthStencilState.depthTestEnable = _enable; }

        VkCompareOp getDepthCompareOp() const { return depthStencilState.depthCompareOp; }

        void setDepthCompareOp(VkCompareOp _op) { depthStencilState.depthCompareOp = _op; }

        float getDepthMinBounds() const { return depthStencilState.minDepthBounds; }

        void setDepthMinBounds(float _bound) { depthStencilState.minDepthBounds = _bound; }

        float getDepthMaxBounds() const { return depthStencilState.maxDepthBounds; }

        void setDepthMaxBounds(float _bound) { depthStencilState.maxDepthBounds = _bound; }

        bool getStencilTestEnable() const { return depthStencilState.stencilTestEnable; }

        void setStencilTestEnable(bool _enable) { depthStencilState.stencilTestEnable = _enable; }

        VkStencilOpState getFrontStencilOp() const { return depthStencilState.front; }

        void setFrontStencilOp(const VkStencilOpState &_state) { depthStencilState.front = _state; }

        VkStencilOpState getBackStencilOp() const { return depthStencilState.back; }

        void setBackStencilOp(const VkStencilOpState &_state) { depthStencilState.back = _state; }

        void setStencilOp(const VkStencilOpState &_state) { depthStencilState.front = depthStencilState.back = _state; }

        const VulkanVertexAttributeState *getVertexAttributeState() const { return vertexAttributeState; }

        void setVertexAttributeState(const VulkanVertexAttributeState *_vertexAttributeState)
        {
            vertexAttributeState = _vertexAttributeState;
            createInfo.pVertexInputState = *_vertexAttributeState;
        }

        const VulkanBlendColorState *getBlendColorState() const { return blendColorState; }

        void setBlendColorState(const VulkanBlendColorState *_blendColorState)
        {
            blendColorState = _blendColorState;
            createInfo.pColorBlendState = *_blendColorState;
        }

        VkPipeline getPipeline(VulkanRenderPass *_renderPass);

        bool getEnableDynamicState(VkDynamicState _state) const;

        void setEnableDynamicState(VkDynamicState _state, bool _enable);

        VulkanPipelineLayout getPipelineLayout() const;

        void setPipelineLayout(VulkanPipelineLayout _layout);

        ShaderModule getVertexModule() const;

        void setVertexModule(ShaderModule _vertexModule);

        ShaderModule getFragmentModule() const;

        void setFragmentModule(ShaderModule _fragmentModule);

        void setDefaultState();

        friend class GraphicPipelinesManager;
    };
}

#endif