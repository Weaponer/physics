#ifndef VULKAN_SWAPCHAIN_H
#define VULKAN_SWAPCHAIN_H

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/memory/vulkanImage.h"

namespace phys::vk
{
    class VulkanSwapchain
    {
    private:
        VulkanDevice *device;
        VulkanQueue *queue;
        VkSurfaceKHR surface;
        int width;
        int height;
        VkSwapchainKHR swapchain;
        std::vector<VulkanImage> images;
        std::vector<VulkanImageView> imageViews;

    public:
        VulkanSwapchain();

        ~VulkanSwapchain();

        bool isValid() const { return swapchain != VK_NULL_HANDLE; }

        bool createSwapchain(VulkanDevice *_device, VkSurfaceKHR _surface, int _width, int _height);

        void destroySwapchain();

        int getCountImage() const { return (int)images.size(); }

        VulkanImage *getImage(int _index) { return &(images[_index]); }

        VulkanImageView getImageView(int _index) { return imageViews[_index]; }

        int getWidth() const { return width; }

        int getHeight() const { return height; }

        operator VkSwapchainKHR() { return swapchain; }
    };
}

#endif