#ifndef VULKAN_INSTANCE_H
#define VULKAN_INSTANCE_H

#include <vulkan/vulkan.h>

#include "vulkanExportedLibrary.h"
#include "vulkanMainFunctions.h"

namespace phys::vk
{
#define INSTANCE_LEVEL_VULKAN_FUNCTION(name) PFN_##name name;
#define INSTANCE_LEVEL_VULKAN_FUNCTION_EXTENSION(name, extension) PFN_##name name;

    class VulkanInstance
    {
    private:
        VulkanExportedLibrary *vkExportedLibrary;
        VulkanMainFunctions *vkMainFunctions;
        VkInstance instance;

    public:
        VulkanInstance(VkInstanceCreateInfo &_createInfo, VulkanExportedLibrary *_vkExportedLibrary, VulkanMainFunctions *_vkMainFunctions);

        ~VulkanInstance();

        operator VkInstance() { return instance; };

    private:
        PFN_vkVoidFunction loadFunction(const char *_name, const char *_extension, VkInstanceCreateInfo &_createInfo);

    public:
#include "exportedVulkanFunctions.inl"
    };
}

#endif