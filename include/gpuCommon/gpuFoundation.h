#ifndef GPU_FOUNDATION_H
#define GPU_FOUNDATION_H

#include "common/phVulkanLibraryProvider.h"
#include "vulkanExportedLibrary.h"
#include "vulkanInstance.h"
#include "vulkanDevice.h"
#include "memory/gpuMemoryController.h"
#include "memory/vulkanBufferAllocator.h"
#include "memory/vulkanImageAllocator.h"

#include "specialCommands/vulkanCommandReceiver.h"

#include "window/window.h"

namespace phys
{
    struct GPUFoundationSettings
    {
        int minCountGraphicQueues;
        int minCountComputeQueues;
    };

    class GPUFoundation
    {
    private:
        PhVulkanLibraryProvider *vulkanLibProvider;
        Window *window;

        vk::VulkanExportedLibrary *library;
        vk::VulkanMainFunctions *mainFunctions;
        vk::VulkanInstance *instance;
        VkDebugUtilsMessengerEXT debugMessenger;
        VkDebugReportCallbackEXT debugReport;
        vk::VulkanDevice *logicalDevice;

        vk::GpuMemoryController *memoryController;
        vk::VulkanBufferAllocator *bufferAllocator;
        vk::VulkanImageAllocator *imageAllocator;

        vk::VulkanCommandReceiver *commandReceiver;

    public:
        GPUFoundation(PhVulkanLibraryProvider *_vulkanLibProvider, const GPUFoundationSettings *_setting, Window *_window = NULL);

        ~GPUFoundation();

        vk::VulkanDevice *getDevice() { return logicalDevice; }

        vk::VulkanBufferAllocator *getBufferAllocator() { return bufferAllocator; }

        vk::VulkanImageAllocator *getImageAllocator() { return imageAllocator; }

        vk::VulkanCommandReceiver *getCommandReceiver() { return commandReceiver; }

    private:
        void enableDebugMessage();

        void createDevice(const GPUFoundationSettings *_setting, int _countLayers = 0, const char **_layerNames = NULL, int _countExtensions = 0, const char **_extensionNames = NULL, void *_pExtensionData = NULL);

        void createMemoryController();

        void createCommandReceiver(const GPUFoundationSettings *_setting);
    };
};

#endif