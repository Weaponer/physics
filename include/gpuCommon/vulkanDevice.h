#ifndef VULKAN_DEVICE_H
#define VULKAN_DEVICE_H

#include <vulkan/vulkan.h>
#include <list>

#include "vulkanInstance.h"
#include "vulkanQueue.h"

namespace phys::vk
{
#define DEVICE_LEVEL_VULKAN_FUNCTION(name) PFN_##name name;
#define DEVICE_LEVEL_VULKAN_FUNCTION_EXTENSION(name, extension) PFN_##name name;

    class VulkanDevice
    {
    private:
        VulkanInstance *instance;
        VkDevice device;

        VkPhysicalDevice physicalDevice;
        VkPhysicalDeviceFeatures features;
        VkPhysicalDeviceProperties properties;
        VkPhysicalDeviceMemoryProperties memoryProperties;
        std::vector<VkQueueFamilyProperties> queueFamiliesProperties;

        std::vector<VulkanQueue *> queues;
        std::vector<uint32_t> queueFamilies;
        std::list<VulkanQueue *> unusedQueues;

    public:
        VulkanDevice(VkPhysicalDevice _physicalDevice, const VkDeviceCreateInfo *_createInfo, VulkanInstance *_instance);

        ~VulkanDevice();

        const VkPhysicalDeviceProperties &getProperties() const { return properties; }

        const VkPhysicalDeviceMemoryProperties &getMemoryProperties() const { return memoryProperties; }

        const std::vector<VkQueueFamilyProperties> &getQueueFamiliesProperties() const { return queueFamiliesProperties; }

        VulkanQueue *getFreeQueue(VkQueueFlags _queueFlag);

        void returnQueue(VulkanQueue *_queue);

        void setNameObject(const char *_name, uint64_t _object, VkDebugReportObjectTypeEXT _type);

        VkPhysicalDevice getPhysicalDevice() const { return physicalDevice; }

        VulkanInstance *getInstance() const { return instance; }

        int getCountQueueFamilies() const { return (int)queueFamilies.size(); }

        const uint32_t *getQueueFamiliesIndexes() const { return queueFamilies.data(); }

        operator VkDevice() { return device; }

    private:
        PFN_vkVoidFunction loadFunction(const char *_name, const char *_extension, const VkDeviceCreateInfo *_createInfo);

    public:
#include "exportedVulkanFunctions.inl"
    };
}

#endif