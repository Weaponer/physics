#ifndef VULKAN_QUEUE_H
#define VULKAN_QUEUE_H

#include <vulkan/vulkan.h>
#include <vector>
#include <list>

#include "vulkanCommandPool.h"

#include "vulkanSemaphore.h"
#include "vulkanFence.h"

namespace phys::vk
{
    class VulkanDevice;

    class VulkanQueue
    {
    private:
        VulkanDevice *device;
        VkQueue queue;
        int familyIndex;
        VkQueueFlags queueFlag;

        std::list<VulkanCommandPool *> commandPools;

    public:
        VulkanQueue(VulkanDevice *_device, VkQueue _queue, int _familyIndex, VkQueueFlags _queueFlag);

        ~VulkanQueue();

        VulkanDevice *getDevice() const { return device; }

        int getFamilyIndex() const { return familyIndex; }

        VkQueueFlags getQueueFlag() const { return queueFlag; }

        VulkanCommandPool *getCommandPool();

        void returnCommandPool(VulkanCommandPool *_commandPool);

        void submit();

        void submit(int _countCommadPools, VulkanCommandPool **_commandPools,
                    int _countWaitSemaphores, VulkanSemaphore *_waitSemaphores, const VkPipelineStageFlags *_waitDstStages,
                    VulkanFence _fence, int _countSignalSemaphores, VulkanSemaphore *_signalsSemaphores);

        void waitQueue();

        operator VkQueue() { return queue; }
    };
}
#endif