#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <vector>
#include <map>
#include <vulkan/vulkan.h>

#include "gpuCommon/vulkanDevice.h"

namespace phys::vk
{
    class ShaderData
    {
    public:
        enum ShaderType : int
        {
            ShaderType_Vertex = 0,
            ShaderType_Fragment = 1,
            ShaderType_Compute = 2,
        };

        enum DataType : int
        {
            DataType_Float,
            DataType_Double,
            DataType_Float16,
            DataType_Int8,
            DataType_Uint8,
            DataType_Int16,
            DataType_Uint16,
            DataType_Int,
            DataType_Uint,
            DataType_Int64,
            DataType_Uint64,
            DataType_Bool,
        };

        enum SamplerFormat : int
        {
            SamplerFormat_1D,
            SamplerFormat_2D,
            SamplerFormat_3D,
            SamplerFormat_Cube,
            SamplerFormat_Rect,
            SamplerFormat_Buffer,
        };

        enum BlockStorageType : int
        {
            BlockStorage_UniformBuffer = 0,
            BlockStorage_StorageBuffer = 1,
            BlockStorage_PushConstant = 2,
        };

        struct UniformVariable
        {
            std::string name;
            DataType type;
            int offset;
            int size;
            int vectorSize;
            int implicitArraySize;
        };

        struct UniformBlock
        {
            std::string name;
            int size;
            int set;
            int binding;
            BlockStorageType blockType;
            bool isPushConstant;
            std::vector<UniformVariable> variables;

            inline bool isValid() const { return (set != -1 && binding != -1) || isPushConstant; }
        };

        struct UniformSampler
        {
            std::string name;
            int set;
            int binding;
            SamplerFormat samplerFormat;
            DataType dataType;
            int size;
            int vectorSize;
            int implicitArraySize;

            bool isImage;
            bool isShadow;
            bool isArray;

            inline bool isValid() const { return set != -1 && binding != -1; }
        };

        struct ShaderSetData
        {
            int countUniformBuffers;
            int *indexBlocksUniformBuffers;
            int countStorageBuffers;
            int *indexBlocksStorageBuffers;
            int countMainSamplers;
            int countAllSamplers;
            int *indexMainSamplers;
            int countMainStorageImages;
            int countAllStorageImages;
            int *indexMainStorageImages;

            VkDescriptorSetLayout setLayout;
        };

        struct PushConstantData
        {
            uint32_t offset;
            uint32_t size;
        };

    private:
        VulkanDevice *device;
        VkPipelineLayout vkLayout;

        std::string nameShader;
        ShaderType type;
        std::vector<unsigned int> spirvCode;
        std::vector<UniformBlock> uniformBlocks;
        std::vector<UniformSampler> uniformSamplers;

        PushConstantData pushConstantData;

        int countUseSets;
        std::vector<int> numbersSetsToIndexesSets;
        std::map<int, int> indexesSetsToNumbersSets;
        std::vector<ShaderSetData> shaderSetDatas;

        std::vector<std::vector<int>> uniformBlocksInSet;
        std::vector<std::vector<int>> uniformSamplersInSet;

    public:
        ShaderData(const std::string &_nameShader, ShaderType _type, const std::vector<unsigned int> &_spirvCode,
                   const std::vector<UniformBlock> &_uniformBlocks, const std::vector<UniformSampler> &_uniformSamplers);

        ~ShaderData();

        void prepareData(VulkanDevice *_device);

        const char *getName() const { return nameShader.c_str(); }

        const std::vector<unsigned int> &getSpirvCode() const { return spirvCode; }

        int getCountUniformBlocks() const { return uniformBlocks.size(); }

        const UniformBlock &getUniformBlock(int _index) const { return (uniformBlocks[_index]); }

        int getCountUniformSamplers() const { return uniformSamplers.size(); }

        const UniformSampler &getUniformSampler(int _index) const { return (uniformSamplers[_index]); }

        bool isUsePushConstant() const { return pushConstantData.size != 0; }

        const PushConstantData &getPushConstantData() const { return pushConstantData; }

        int getCountSets() const { return countUseSets; }

        const ShaderSetData &getSetData(int _indexSet) const { return shaderSetDatas[_indexSet]; }

        int getSetIndexBinding(int _indexSet) const { return numbersSetsToIndexesSets[_indexSet]; }

        int getCountUniformBlocksInSet(int _indexSet) const { return uniformBlocksInSet[_indexSet].size(); }

        const UniformBlock &getUniformBlockInSet(int _indexSet, int _indexUniformBlock) const { return getUniformBlock(uniformBlocksInSet[_indexSet][_indexUniformBlock]); }

        int getCountUniformSamplersInSet(int _indexSet) const { return uniformSamplersInSet[_indexSet].size(); }

        const UniformSampler &getUniformSamplerInSet(int _indexSet, int _indexUniformSampler) const { return getUniformSampler(uniformSamplersInSet[_indexSet][_indexUniformSampler]); }

        VkPipelineLayout getPipelineLayout() const { return vkLayout; }

        VkShaderStageFlags getStageFlag();

    private:
        void prepareSetLayouts(VulkanDevice *_device);

        void preparePipelineLayout(VulkanDevice *_device);
    };

    class ShaderModule
    {
    private:
        VkShaderModule module;
        ShaderData *data;
        int index;

    public:
        ShaderModule() : module(VK_NULL_HANDLE), data(NULL), index(-1) {}

        ShaderModule(VkShaderModule _module, ShaderData *_data, int _index) : module(_module), data(_data), index(_index) {}

        ~ShaderModule() {}

        bool isValid() const { return module != VK_NULL_HANDLE; }

        ShaderData *getData() const { return data; }

        int getIndex() const { return index; }

        operator VkShaderModule() { return module; }
    };
}

#endif