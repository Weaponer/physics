#ifndef VULKAN_DESCRIPTOR_POOL_MANAGER
#define VULKAN_DESCRIPTOR_POOL_MANAGER

#include <vulkan/vulkan.h>
#include <vector>

#include "shaderManager.h"

namespace phys::vk
{
    struct DescriptorPoolManagerConfig
    {
        int maxDescriptorsPerPool;
        int stepCountDescriptors;
    };

    struct VulkanDescriptorSetScheme
    {
        VkDescriptorSetLayout setLayoutScheme;
    };

    struct VulkanDescriptorPoolScheme
    {
        std::vector<VkDescriptorPoolSize> poolSizes;
    };

    struct VulkanDescriptorPoolPage
    {
        VkDescriptorPool pool;
        int countSets;
        int countFreeSets;

        const VulkanDescriptorPoolScheme *poolShceme;
        int indexScheme;

        VulkanDescriptorPoolPage *next;
    };

    class VulkanDescriptorPoolManager
    {
    private:
        VulkanDevice *device;

        DescriptorPoolManagerConfig config;

        std::vector<VulkanDescriptorPoolScheme *> poolSchemes;
        std::vector<VulkanDescriptorPoolPage *> poolPages;

        std::vector<std::pair<int, int>> poolSchemeByShaderIndex; // start index, count sets
        std::vector<int> poolSchemesByShaderSetIndex;

    public:
        VulkanDescriptorPoolManager(VulkanDevice *_device);

        VulkanDescriptorPoolManager(VulkanDevice *_device, DescriptorPoolManagerConfig _config);

        ~VulkanDescriptorPoolManager();

        void preparePools(ShaderManager *_shaderManager);

        VulkanDescriptorPoolPage *getDescriptorPools(ShaderModule _shader, int _indexSet, int _count);

        void retrunDescriptorPools(VulkanDescriptorPoolPage *_descriptorPools);

        int getMaxDescriptorSetsPerPool() const { return config.maxDescriptorsPerPool; }

    private:
        uint64_t getHashKeyPoolSetsScheme(const ShaderData::ShaderSetData &_setData);

        void fillPoolSchemeByShaderSetData(const ShaderData::ShaderSetData &_setData, VulkanDescriptorPoolScheme *_poolScheme);

        VulkanDescriptorPoolPage *getDescrpitorPoolPage(int _indexScheme);

        void returnDescrpitorPoolPage(VulkanDescriptorPoolPage *_page);

        void clear();
    };
}

#endif