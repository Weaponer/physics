#ifndef COMPUTE_PIPELINE_H
#define COMPUTE_PIPELINE_H

#include <vulkan/vulkan.h>
#include "shader.h"

#include "gpuCommon/vulkanDevice.h"

namespace phys::vk
{
    class ComputePipeline
    {
    private:
        VkPipeline pipeline;

    public:
        ComputePipeline() : pipeline(VK_NULL_HANDLE) {}

        ComputePipeline(VkPipeline _pipeline) : pipeline(_pipeline) {}

        ~ComputePipeline() {}

        bool isValid() const { return pipeline != VK_NULL_HANDLE; }

        static bool createComputePipeline(VulkanDevice *_device, ShaderModule _module, ComputePipeline *_pipeline);

        static void destroyComputePipeline(VulkanDevice *_device, ComputePipeline *_pipeline);

        operator VkPipeline() { return pipeline; };
    };
};

#endif