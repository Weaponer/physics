#ifndef SHADER_READER_H
#define SHADER_READER_H

#include <string>
#include <vector>

#include "shader.h"

namespace phys::vk
{
    class ShaderReader
    {
    private:
        struct SeekerDescriptor
        {
            int position;
            const char *data;
        };

    public:
        int getCountShadersInPacket(const char *_data) const;

        void getShadersFromPacket(int _count, ShaderData **_shaders, const char *_data) const;

    private:
        int readInt(SeekerDescriptor &_seeker) const;

        float readFloat(SeekerDescriptor &_seeker) const;

        std::string readString(SeekerDescriptor &_seeker) const;

        bool readBool(SeekerDescriptor &_seeker) const;

        bool readBufferUInt(SeekerDescriptor &_seeker, std::vector<unsigned int> &_buf) const;
    };
}

#endif