#ifndef VULKAN_PIPELINE_LAYOUT_H
#define VULKAN_PIPELINE_LAYOUT_H

#include <vulkan/vulkan.h>
#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/shaders/shader.h"

namespace phys::vk
{
    class VulkanPipelineLayout
    {
    private:
        VkPipelineLayout layout;

    public:
        VulkanPipelineLayout() : layout(VK_NULL_HANDLE) {}

        VulkanPipelineLayout(VkPipelineLayout _layout) : layout(_layout) {}

        ~VulkanPipelineLayout() {}

        bool isValid() const { return layout != VK_NULL_HANDLE; }

        static bool createPipelineLayout(VulkanDevice *_device, int _countShaders, ShaderModule *_modules, VulkanPipelineLayout *_outPipelineLayout);

        static void destroyPipelineLayout(VulkanDevice *_device, VulkanPipelineLayout *_pipelineLayout);

        operator VkPipelineLayout() { return layout; }
    };
}

#endif