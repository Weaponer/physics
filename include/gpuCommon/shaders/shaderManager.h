#ifndef SHADER_MANAGER_H
#define SHADER_MANAGER_H

#include <vulkan/vulkan.h>
#include <vector>

#include "shader.h"

#include "gpuCommon/vulkanDevice.h"

namespace phys::vk
{
    class ShaderManager
    {
    private:
        VulkanDevice *device;
        std::vector<ShaderModule> shaders;

    public:
        ShaderManager(VulkanDevice *_device);

        ~ShaderManager();

        bool registerShader(ShaderData *_shader, ShaderModule *_shaderModule);

        bool getShaderByName(const char *_name, ShaderModule *_shaderModule);

        int getCountShaders() const { return shaders.size(); }

        ShaderModule getShaderByIndex(int _index) { return shaders[_index]; }
    };
}

#endif