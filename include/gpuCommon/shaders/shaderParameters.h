#ifndef SHADER_PARAMETERS_H
#define SHADER_PARAMETERS_H

#include <vulkan/vulkan.h>

#include "shader.h"
#include "gpuCommon/vulkanDevice.h"
#include "vulkanDescriptorPoolManager.h"

namespace phys::vk
{
    class ShaderParameters
    {
    protected:
        struct Set
        {
            VkDescriptorSetLayout setLayout;
            int setBinding;

            VulkanDescriptorPoolPage *beginPage;
            VulkanDescriptorPoolPage *endPage;
            VulkanDescriptorPoolPage *currentPage;

            std::vector<VkDescriptorSet> freeSets;
            int countFreeSets;
            VkDescriptorSet currentSet;
        };

    protected:
        VulkanDevice *device;
        ShaderModule shader;

        uint bufferingDescriptorSets;

        int countSets;
        std::vector<Set> sets;

    public:
        ShaderParameters();

        ShaderParameters(VulkanDevice *_device, ShaderModule _shader);

        virtual ~ShaderParameters();

        void takeMemoryPools(VulkanDescriptorPoolManager *_poolManager, int _countSets);

        void freeMemoryPools(VulkanDescriptorPoolManager *_poolManager);

        void switchToNextSet(int _indexSet);

        void switchToNextSets();

        void reset();

        VkDescriptorSet getCurrentSet(int _indexSet);

        int getBindingSet(int _indexSet);

        uint getBufferingDescriptorSets() const { return bufferingDescriptorSets; }

        void setBufferingDescriptorSets(uint _bufferingSize) { bufferingDescriptorSets = _bufferingSize; }

        int getCountSets() const { return countSets; }

    protected:
        virtual void onSwitchSet(int _indexSet, VkDescriptorSet _previousSet);

        virtual void onReset();

    private:
        bool allocateDescriptorSets(int _indexSet);
    };
}

#endif