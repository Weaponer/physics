#ifndef VULKAN_EXPORTED_LIBRARY_H
#define VULKAN_EXPORTED_LIBRARY_H

#include <vulkan/vulkan.h>

#include "common/phVulkanLibraryProvider.h"

namespace phys::vk
{
    #define EXPORTED_VULKAN_FUNCTION( name ) PFN_##name name;

    class VulkanExportedLibrary
    {
    private:
        PhVulkanLibraryProvider *provider;

    public:
        VulkanExportedLibrary(PhVulkanLibraryProvider *_provider);

    private:
        void *loadFunction(const char *_name);

    public:    
        #include "exportedVulkanFunctions.inl"
    };
}

#endif