#ifndef VULKAN_MAIN_FUNCTIONS_H
#define VULKAN_MAIN_FUNCTIONS_H

#include <vulkan/vulkan.h>
#include <vector>
#include <string>

#include "vulkanExportedLibrary.h"

namespace phys::vk
{
    #define GLOBAL_LEVEL_VULKAN_FUNCTION( name ) PFN_##name name;

    class VulkanMainFunctions
    {
    private:
        VulkanExportedLibrary *vkExportedLibrary;

        std::vector<std::string> availableLayers;
        std::vector<std::string> availableExtensions;

    public:
        VulkanMainFunctions(VulkanExportedLibrary *_vkExportedLibrary);

        std::vector<const char *> getSupportedLayers(int _count, const char **_layers);

        bool layerAvailable(const char *_layer);

        std::vector<const char *> getSupportedExtensions(int _count, const char **_extensions);

        bool extensionsAvailable(const char *_extension);

    private:
        PFN_vkVoidFunction loadFunction(const char *_name);

    public:    
        #include "exportedVulkanFunctions.inl"
    };
}

#endif