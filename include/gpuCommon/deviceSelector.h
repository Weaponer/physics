#ifndef DEVICE_SELECTOR_H
#define DEVICE_SELECTOR_H

#include <vector>
#include <vulkan/vulkan.h>
#include <string>
#include "memory/memoryType.h"
#include "vulkanInstance.h"
#include "vulkanDevice.h"

namespace phys::vk
{
    class DeviceSelector
    {
    private:
        struct RequireQueueType
        {
            VkQueueFlagBits type;
            int count;
            bool supportSwapChain;
            VkSurfaceKHR swapChainSurface;
        };

    private:
        std::vector<RequireMemoryType> requireMemoryTypes;
        std::vector<RequireQueueType> requireQueueTypes;

        std::vector<std::string> requireLayers;
        std::vector<std::string> requireExtensions;

    public:
        void addRequireMemoryType(const RequireMemoryType &_type);

        void addRequireQueueType(VkQueueFlagBits _type, int _count, bool _supportSwapchain = false, VkSurfaceKHR _swapChainSurface = VK_NULL_HANDLE);

        void addRequireLayer(const char *_layerName);

        void addRequireExtension(const char *_extensionName);

        void clear();

        VkPhysicalDevice getDevice(VulkanInstance *_instance) const;

        VulkanDevice *createLogicalDevice(VkPhysicalDevice _physicalDevice, VulkanInstance *_instance, void *_pExtensionData) const;

    private:
        bool checkMemoryProperties(RequireMemoryType _requireType, VkPhysicalDeviceMemoryProperties &_properties) const;

        bool checkQueueFamily(VulkanInstance *_instance, VkPhysicalDevice _physDevice,
                              RequireQueueType _requireProperties, std::vector<VkQueueFamilyProperties> &_queueFamiliesProperties) const;

        int getSuitableFamilyIndex(VulkanInstance *_instance, VkPhysicalDevice _physDevice,
                                   RequireQueueType _requireProperties, std::vector<VkQueueFamilyProperties> &_queueFamiliesProperties) const;
    };
}

#endif