#ifndef VULKAN_FENCE_H
#define VULKAN_FENCE_H

#include <vulkan/vulkan.h>

namespace phys::vk
{
    class VulkanDevice; 

    class VulkanFence
    {
    private:
        VulkanDevice *device;
        VkFence fence;

    public:
        VulkanFence() : device(NULL), fence(VK_NULL_HANDLE) {}

        VulkanFence(VulkanDevice *_device, VkFence _fence) : device(_device), fence(_fence) {}

        ~VulkanFence() {}

        bool isValid() const { return fence != VK_NULL_HANDLE; }

        void reset();

        void wait(uint64_t _timeout = UINT64_MAX);

        bool isSignaled();

        static bool createFence(VulkanDevice *_device, VulkanFence *_outFence, bool _signalEnable = false, const char *_name = NULL);

        static void destroyFence(VulkanFence *_fence);

        operator VkFence() { return fence; }
    };
}

#endif