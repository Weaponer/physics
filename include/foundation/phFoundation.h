#ifndef PH_FOUNDATION_H
#define PH_FOUNDATION_H

#include "common/phResourceProvider.h"
#include "gpuCommon/gpuFoundation.h"
#include "threads/phThreadsManager.h"


namespace phys
{
    class PhFoundation
    {
    private:
        PhResourceProvider *resoureProvider;
        GPUFoundation *gpuFoundation;
        PhThreadsManager *threadsManager;

        PhFoundation(PhResourceProvider *_resoureProvider, GPUFoundation *_gpuFoundation,
                     PhThreadsManager *_threadsManager);

        ~PhFoundation();

    public:
        static PhFoundation *phCreateFoundation(PhResourceProvider *_resoureProvider, GPUFoundation *_gpuFoundation, PhThreadsManager *_threadsManager);

        static void phDestroyFoundation(PhFoundation *_foundation);

        PhResourceProvider *getResourceProvider() { return resoureProvider; }

        GPUFoundation *getGpuFoundation() { return gpuFoundation; }

        PhThreadsManager *getThreadsManager() { return threadsManager; }
    };
};

#endif