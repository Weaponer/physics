#ifndef PH_EASY_STACK_H
#define PH_EASY_STACK_H

#include <cstring>
#include <algorithm>

#include "phMemoryAllocator.h"

namespace phys
{
    template <typename T>
    class PhEasyStack
    {
    private:
        T *memory;
        unsigned int pos;
        unsigned int capacity;
        bool autoUpdate;

        T defaultValue;
        bool useDefault;

    private:
        bool checkIsBegin() const
        {
            return pos == 0;
        }

        bool checkIsEnd() const
        {
            return pos == capacity;
        }

    public:
        T *assignmentMemory()
        {
            if (pos + 1 > capacity)
            {
                if (autoUpdate)
                {
                    unsigned int c = 1;
                    setCapacity(std::max(c, capacity * 2));
                    return assignmentMemory();
                }
                {
                    return NULL;
                }
            }
            else
            {
                T *mem = memory + pos;
                pos++;
                return mem;
            }
        }

        T *assignmentMemoryAndCopy(const T *_src)
        {
            if (pos + 1 > capacity)
            {
                if (autoUpdate)
                {
                    unsigned int c = 1;
                    setCapacity(std::max(c, capacity * 2));
                    return assignmentMemoryAndCopy(_src);
                }
                {
                    return NULL;
                }
            }
            else
            {
                T *mem = memory + pos;
                *mem = *_src;
                pos++;
                return mem;
            }
        }

        void popMemory()
        {
            if (pos != 0)
            {
                pos--;
            }
        }

        T *getMemoryAndPop()
        {
            if (pos != 0)
            {
                T *m = getMemory(pos - 1);
                pos--;

                return m;
            }
            else
            {
                return NULL;
            }
        }

        T *getMemory(unsigned int index) const
        {
            return memory + index;
        }

        void clearMemory()
        {
            pos = 0;
        }

        void setCapacity(unsigned int _capacity)
        {
            T *new_memory = static_cast<T *>(phAllocateMemory(_capacity * sizeof(T)));
            if (useDefault)
            {
                for (int i = 0; i < (int)_capacity; i++)
                {
                    new_memory[i] = defaultValue;
                }
            }
            unsigned int __capacity = _capacity;

            if (memory == NULL)
            {
                memory = new_memory;
                capacity = __capacity;
                return;
            }

            std::memcpy((void *)new_memory, (void *)memory, std::min(capacity, _capacity) * sizeof(T));
            phDeallocateMemory(memory);

            memory = new_memory;
            capacity = __capacity;

            if (pos > _capacity)
            {
                pos = _capacity;
            }
        }

        void release()
        {
            if (memory != NULL)
                phDeallocateMemory(memory);
            memory = NULL;
            pos = 0;
            capacity = 0;
        }

    public:
        PhEasyStack()
        {
            memory = NULL;
            pos = 0;
            capacity = 0;
            autoUpdate = true;
            useDefault = false;
            setCapacity(1);
        }

        PhEasyStack(unsigned int _capacity)
        {
            memory = NULL;
            pos = 0;
            capacity = 0;
            autoUpdate = true;
            useDefault = false;
            setCapacity(_capacity);
        }

        PhEasyStack(const PhEasyStack &_easy_stack)
        {
            useDefault = false;
            capacity = _easy_stack.capacity;
            pos = _easy_stack.pos;
            memory = static_cast<T *>(phAllocateMemory(capacity * sizeof(T)));
            std::memcpy(memory, _easy_stack.memory, capacity);
        }

        ~PhEasyStack()
        {
            release();
        }

        bool empty() const { return pos == 0; }

        unsigned int getPos() const { return pos; }

        unsigned int getCapacity() const { return capacity; }

        bool getAutoUpdate() const { return autoUpdate; }

        void setAutoUpdate(bool state) { autoUpdate = state; }

        void setDefault(const T *def) { defaultValue = *def; }

        const T *getDefault() const { return &defaultValue; }

        void setUseDefault(bool use) { useDefault = use; }

        bool getUseDefault() const { return useDefault; }
    };
}
#endif