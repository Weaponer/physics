#ifndef PH_TABLE_CONTAINER_H
#define PH_TABLE_CONTAINER_H

#include "phEasyStack.h"

namespace phys
{
    template <typename T>
    class PhTableContainer
    {
    private:
        PhEasyStack<T> stack;
        PhEasyStack<unsigned int> freeElements;

    public:
        unsigned int getCapacity() const
        {
            return stack.getCapacity();
        }

        unsigned int getCountUse() const
        {
            return stack.getPos() - freeElements.getPos();
        }

        unsigned int getPosBuffer() const
        {
            return stack.getPos();
        }

        T *getMemory(unsigned int index)
        {
            return stack.getMemory(index);
        }

        const T *getMemory(unsigned int index) const
        {
            return stack.getMemory(index);
        }

        T *assignmentMemory(unsigned int &index)
        {
            if (freeElements.getPos() != 0)
            {
                index = *freeElements.getMemoryAndPop();
                return stack.getMemory(index);
            }

            T *el = stack.assignmentMemory();
            index = stack.getPos() - 1;
            return el;
        }

        T *assignmentMemoryAndCopy(const T *_src, unsigned int &index)
        {
            if (freeElements.getPos() != 0)
            {
                index = *freeElements.getMemoryAndPop();

                T *dest = stack.getMemory(index);
                *dest = *_src;
                return stack.getMemory(index);
            }

            T *el = stack.assignmentMemoryAndCopy(_src);
            index = stack.getPos() - 1;
            return el;
        }

        void setDefault(const T *def) { stack.setDefault(def); }

        const T *getDefault() const { return stack.getDefault(); }

        void setUseDefault(bool use) { stack.setUseDefault(use); }

        bool getUseDefault() const { return stack.getUseDefault(); }

        void popMemory(unsigned int index)
        {
            if (index == stack.getPos() - 1)
            {
                stack.popMemory();
            }
            else
            {
                freeElements.assignmentMemoryAndCopy(&index);
            }
            if (getUseDefault())
            {
                T *element = getMemory(index);
                *element = *getDefault();
            }
        }

        void clearMemory()
        {
            stack.clearMemory();
            freeElements.clearMemory();
        }

        void release()
        {
            stack.release();
            freeElements.release();
        }

    public:
        PhTableContainer() : stack(), freeElements()
        {
        }

        PhTableContainer(unsigned int _capacity) : stack(_capacity), freeElements(_capacity)
        {
        }

        PhTableContainer(const PhTableContainer &_table) : stack(_table.stack), freeElements(_table.freeElements)
        {
        }

        ~PhTableContainer()
        {
        }
    };

};

#endif