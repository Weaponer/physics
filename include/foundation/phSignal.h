#ifndef PH_SIGNAL_H
#define PH_SIGNAL_H

#include "common/phBase.h"
#include "common/phMacros.h"

#include "foundation/phTableContainer.h"

namespace phys
{
    template <typename T>
    class PhSignal;

    class PhConnection
    {
    private:
        int index;

    public:
        PhConnection() : index(-1) {}

        PhConnection(const PhConnection &_connection) : index(_connection.index) {}

    private:
        PhConnection(int _index) : index(_index) {}

    public:
        bool isValid() const { return index != -1; }

        template <typename T>
        friend class PhSignal;
    };

    template <typename D>
    class PhSignal : public PhBase
    {
    private:
        class BindBase : public PhBase
        {
        public:
            BindBase() : PhBase() {}
            virtual ~BindBase() {}
            virtual void release() override { PhBase::release(); }

            virtual void onSignal(D _argument) = 0;
        };

        template <class T>
        class Bind : public BindBase
        {
        protected:
            T *receiver;
            bool useArgument;
            void (T::*funcArg)(D);
            void (T::*func)();

        public:
            Bind(T *_receiver, void (T::*_func)(D)) : BindBase(), receiver(_receiver), useArgument(true), funcArg(_func), func(NULL) {}
            Bind(T *_receiver, void (T::*_func)()) : BindBase(), receiver(_receiver), useArgument(false), funcArg(NULL), func(_func) {}

            virtual ~Bind() {}

            virtual void release() override { BindBase::release(); }

            virtual void onSignal(D _argument) override { useArgument ? (*receiver.*funcArg)(_argument) : (*receiver.*func)(); }
        };

    private:
        PhTableContainer<BindBase *> binds;

    public:
        PhSignal() : PhBase(), binds()
        {
            BindBase *def = NULL;
            binds.setUseDefault(true);
            binds.setDefault(&def);
        }

        PhSignal(const PhSignal &_signal) = delete;

        virtual ~PhSignal() {}

        virtual void release() override
        {
            uint pos = binds.getPosBuffer();
            for (uint i = 0; i < pos; i++)
            {
                BindBase *base = *binds.getMemory(i);
                if (base != NULL)
                    phDeallocateMemory(base);
            }
            binds.release();
            PhBase::release();
        }

        void signal(D _agrument)
        {
            uint pos = binds.getPosBuffer();
            for (uint i = 0; i < pos; i++)
            {
                BindBase *base = *binds.getMemory(i);
                if (base != NULL)
                    base->onSignal(_agrument);
            }
        }

        void signal()
        {
            signal({});
        }

        template <class T>
        PhConnection bind(T *_receiver, void (T::*_func)(D))
        {
            Bind<T> *b = new (phAllocateMemory(sizeof(Bind<T>))) Bind<T>(_receiver, _func);
            uint index = 0;
            BindBase *base = (BindBase *)b;
            binds.assignmentMemoryAndCopy(&base, index);
            return PhConnection((int)index);
        }

        template <class T>
        PhConnection bind(T *_receiver, void (T::*_func)())
        {
            Bind<T> *b = new (phAllocateMemory(sizeof(Bind<T>))) Bind<T>(_receiver, _func);
            uint index = 0;
            BindBase *base = (BindBase *)b;
            binds.assignmentMemoryAndCopy(&base, index);
            return PhConnection((int)index);
        }

        void unbind(PhConnection &_connection)
        {
            if (_connection.isValid())
            {
                uint inedx = (uint)_connection.index;
                phDeallocateMemory(*binds.getMemory(inedx));

                binds.popMemory(inedx);
                _connection.index = -1;
            }
        }
    };

    class PhEmptyArgument
    {
    };

    typedef PhSignal<PhEmptyArgument> PhEmptySignal;
}

#endif