#ifndef PH_MEMORY_ALLOCATOR_H
#define PH_MEMORY_ALLOCATOR_H

#include <stdlib.h>
#include <malloc.h>
#include <atomic>
#include <iostream>

#define MEMORY_ALIGNMENT 16

namespace phys
{
    inline int __countAllocate;

    inline void *phAllocateMemory(unsigned int _size)
    {
        __countAllocate++;
        return memalign(MEMORY_ALIGNMENT, _size);
    }

    inline void phDeallocateMemory(void *_mem)
    {
        __countAllocate--;
        free(_mem);
    }

    inline void phPrintAllocateMemory()
    {
        std::cout << "Count allocate memory: " << __countAllocate << std::endl;
    }
}

#endif