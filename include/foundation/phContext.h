#ifndef PH_CONTEXT_H
#define PH_CONTEXT_H

#include <ios>
#include <type_traits>

#include "common/phBase.h"
#include "common/phMacros.h"

#include "phEasyStack.h"

namespace phys
{
#define T_WASNT_FOUND_ERROR "T wasn't found."

    class PhContext : public PhBase
    {
    private:
        struct NodeBinding
        {
            std::size_t hashCode;
            PhBase *data;

            NodeBinding *left;
            NodeBinding *right;
        };

    private:
        NodeBinding root;

    private:
        void recurseDeleteNode(NodeBinding *_node)
        {
            if (_node->left)
                recurseDeleteNode(_node->left);

            if (_node->right)
                recurseDeleteNode(_node->right);

            phDeallocateMemory(_node);
        }

        bool getNodeOrNearNode(std::size_t _id, NodeBinding **_out) const
        {
            NodeBinding *currentNode = const_cast<NodeBinding *>(&root);
            while (currentNode)
            {
                if (_id < currentNode->hashCode)
                {
                    if (!currentNode->left)
                    {
                        *_out = currentNode;
                        return false;
                    }
                    currentNode = currentNode->left;
                    continue;
                }

                if (currentNode->hashCode < _id)
                {
                    if (!currentNode->right)
                    {
                        *_out = currentNode;
                        return false;
                    }
                    currentNode = currentNode->right;
                    continue;
                }

                if (currentNode->hashCode == _id)
                {
                    *_out = currentNode;
                    return true;
                }
            }
            return false;
        }

        NodeBinding *tryFindOrCreate(std::size_t _id)
        {
            NodeBinding *node;
            if (getNodeOrNearNode(_id, &node))
            {
                return node;
            }
            else
            {
                NodeBinding *newNode = (NodeBinding *)phAllocateMemory(sizeof(NodeBinding));
                *newNode = {};
                newNode->hashCode = _id;
                if (newNode->hashCode < node->hashCode)
                    node->left = newNode;
                else
                    node->right = newNode;

                return newNode;
            }
        }

        NodeBinding *tryFind(std::size_t _id) const
        {
            if (root.hashCode == _id)
                return NULL;

            NodeBinding *node;
            if (getNodeOrNearNode(_id, &node))
            {
                return node;
            }
            return NULL;
        }

    public:
        PhContext() : PhBase(), root({})
        {
            root.hashCode = typeid(PhContext).hash_code();
        }

        PhContext(const PhContext &_context) = delete;

        virtual ~PhContext() {}

        virtual void release() override
        {
            if (root.left)
            {
                recurseDeleteNode(root.left);
                root.left = NULL;
            }
            if (root.right)
            {
                recurseDeleteNode(root.right);
                root.left = NULL;
            }

            PhBase::release();
        }

        void setParent(const PhContext *_context)
        {
            root.data = const_cast<PhContext *>(_context);
        }

        PhContext *getParent() const
        {
            return dynamic_cast<PhContext *>(root.data);
        }

        template <class T>
        void bind(T *_data)
        {
            std::size_t code = typeid(T).hash_code();
            NodeBinding *node = tryFindOrCreate(code);
            node->data = _data;
        }

        template <class T>
        T *bindDefault()
        {
            T *data = new (phAllocateMemory(sizeof(T))) T();
            bind<T>(data);
            return data;
        }

        template <class T>
        T *get() const
        {
            std::size_t code = typeid(T).hash_code();
            NodeBinding *node = tryFind(code);

            PhContext *nextParent = getParent();
            while (!node && nextParent)
            {
                node = nextParent->tryFind(code);
                if (!node || node->data == NULL)
                {
                    nextParent = nextParent->getParent();
                    continue;
                }
                else
                {
                    break;
                }
            }

            PH_ASSERT(node && node->data, T_WASNT_FOUND_ERROR);
            return dynamic_cast<T *>(node->data);
        }

        template <class T>
        void clearBind()
        {
            std::size_t code = typeid(T).hash_code();
            NodeBinding *node = tryFind(code);
            PH_ASSERT(node, T_WASNT_FOUND_ERROR);
            node->data = NULL;
        }

        template <class T>
        void releaseBind()
        {
            std::size_t code = typeid(T).hash_code();
            NodeBinding *node = tryFind(code);
            PH_ASSERT(node, T_WASNT_FOUND_ERROR);
            PH_ASSERT(node->data, T_WASNT_FOUND_ERROR);
            PH_RELEASE(node->data);
            node->data = NULL;
        }
    };
}

#endif