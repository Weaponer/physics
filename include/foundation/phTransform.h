#ifndef PH_RANSFORM_H
#define PH_RANSFORM_H

#include <MTH/vectors.h>
#include <MTH/matrix.h>

namespace phys
{
    struct PhTransform
    {
        mth::vec3 position;
        mth::mat3 rotation;

        PhTransform() : position(0, 0, 0), rotation(1.0f, 0.0f, 0.0f,
                                                    0.0f, 1.0f, 0.0f,
                                                    0.0f, 0.0f, 1.0f) {}

        ~PhTransform();
    };
}

#endif