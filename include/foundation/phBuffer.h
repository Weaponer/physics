#ifndef PH_BUFFER_H
#define PH_BUFFER_H

#include <cstring>
#include <algorithm>

#include "phMemoryAllocator.h"

namespace phys
{
    template <typename T>
    class PhBuffer
    {
    private:
        T *memory;
        uint size;
        uint capacity;

    public:
        void popElement(uint _index)
        {
            if (_index + 1 >= size)
            {
                size--;
            }
            else
            {
                int countMove = size - (_index + 1);
                std::memcpy((void *)(memory + _index), (void *)(memory + _index + 1), countMove * sizeof(T));
                size--;
            }
        }

        void pushElement(T *_element)
        {
            if (size >= capacity)
            {
                unsigned int c = 1;
                setCapacity(std::max(c, capacity * 2));
            }
            *(memory + size) = *_element;
            size++;
        }

        T getElement(uint _index) const
        {
            return *(memory + _index);
        }

        void setElement(T _data, uint _index)
        {
            *(memory + _index) = _data;
        }

        const T *getData() const
        {
            return memory;
        }

        T *getData()
        {
            return memory;
        }

        int countElements() const
        {
            return size;
        }

        int getCapacity() const
        {
            return capacity;
        }

        void setCapacity(uint _capacity)
        {
            T *new_memory = static_cast<T *>(phAllocateMemory(_capacity * sizeof(T)));

            if (memory == NULL)
            {
                memory = new_memory;
                capacity = _capacity;
                return;
            }

            std::memcpy((void *)new_memory, (void *)memory, std::min(_capacity, capacity) * sizeof(T));
            phDeallocateMemory(memory);
            memory = new_memory;
            capacity = _capacity;
            size = std::min(size, capacity);
        }

        void release()
        {
            if (memory != NULL)
                phDeallocateMemory(memory);
            memory = NULL;
            size = 0;
            capacity = 0;
        }

    public:
        PhBuffer()
        {
            memory = NULL;
            size = 0;
            capacity = 0;
            setCapacity(1);
        }

        PhBuffer(uint _capacity)
        {
            memory = NULL;
            size = 0;
            capacity = 0;
            setCapacity(_capacity);
        }

        PhBuffer(const PhBuffer &_buffer)
        {
            memory = NULL;
            size = 0;
            capacity = 0;
            setCapacity(_buffer.capacity);
            size = _buffer.size;
            if (_buffer.memory != NULL)
            {
                std::memcpy((void *)memory, (void *)_buffer.memory, _buffer.capacity * sizeof(T));
            }
            else
            {
                memory = NULL;
            }
        }

        ~PhBuffer()
        {
            release();
        }
    };
};

#endif