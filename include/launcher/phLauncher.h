#ifndef PH_LAUNCHER_H
#define PH_LAUNCHER_H

#include "common/phBase.h"
#include "common/phResourceProvider.h"
#include "foundation/phFoundation.h"
#include "physics/phPhysics.h"

#include "window/window.h"
#include "extension/phWindowImGUI.h"
#include "launcher/editor/phEditor.h"

#include "gpuCommon/gpuFoundation.h"
#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/vulkanFence.h"

#include "rendering/geShaderProvider.h"
#include "rendering/geFoundation.h"

namespace phys
{
    class PhLauncher : public PhBase
    {
    private:
        PhFoundation *foundation;
        PhPhysics *physics;
        PhScene *physicsScene;
        PhResourceProvider *resourceProvider;

        GPUFoundation *gpuFoundation;
        Window *window;
        PhWindowImGUI *imGUI;
        PhEditor *editor;
        vk::VulkanQueue *renderingComputeQueue;

        vk::VulkanSemaphore endRenderSceneSignal;
        vk::VulkanFence fence;

        ge::GeShaderProvider *geShaderProvider;
        ge::GeFoundation *geFoundation;
        ge::GeScene *geScene;
        ge::GeDebugLineRenderer *debugLines;

        PhLauncher();

        virtual ~PhLauncher() {}

    public:
        static PhLauncher *phCreateLauncher();

        static void phDestroyLauncher(PhLauncher *_launcher);

        void init(PhFoundation *_foundation, PhPhysics *_physics, PhResourceProvider *_resourceProvider);

        void initRendering(GPUFoundation *_foundation, Window *_window, vk::VulkanQueue *_renderingComputeQueue);

        void launch();

        virtual void release() override;

    private:
        void createScene();

        void drawOutDebugPhysics();
    };
}

#endif