#ifndef PH_EDITOR_CONTEXT_H
#define PH_EDITOR_CONTEXT_H

#include "window/window.h"

#include "rendering/geScene.h"
#include "common/phResourceProvider.h"

#include "phEditorMouseState.h"

#include "physics/phPhysics.h"
#include "physics/phScene.h"

namespace phys
{
    class PhEditorScenePickerSystem;

    struct PhEditorInitContext
    {
        Window *window;
        ge::GeScene *scene;
        PhResourceProvider *resourceProvider;
        PhPhysics *physics;
        PhScene *physicsScene;
    };

    struct PhEditorContext : PhEditorInitContext
    {
        PhEditorMouseState *mouseState;
        PhEditorScenePickerSystem *scenePickerSystem;
    };
}

#endif