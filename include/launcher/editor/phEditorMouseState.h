#ifndef PH_EDITOR_MOUSE_STATE_H
#define PH_EDITOR_MOUSE_STATE_H

#include "common/phBase.h"
#include "common/phFlags.h"

namespace phys
{
    class PhEditorMouseState : public PhBase
    {
    public:
        enum : PhFlags
        {
            PhEditorMouseState_None = 0,
            PhEditorMouseState_ImGUI = 1,
            PhEditorMouseState_CameraControl = 2,
            PhEditorMouseState_ObjectPivot = 3,
            PhEditorMouseState_ObjectSelect = 4,
        };

    private:
        PhFlags lockStage;

    public:
        PhEditorMouseState() : PhBase(), lockStage(PhEditorMouseState_None) {}

        virtual ~PhEditorMouseState() {}

        virtual void release() override {}

        bool tryLock(PhFlags srcState, PhFlags dstState);

        bool isLockMouse() const { return lockStage != PhEditorMouseState_None; }

        PhFlags getLockStage() const { return lockStage; }
    };
}

#endif