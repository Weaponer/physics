#ifndef PH_EDITOR_PHYSICS_WINDOW_H
#define PH_EDITOR_PHYSICS_WINDOW_H


#include "common/phFlags.h"

#include "foundation/phSignal.h"

#include "phEditorGuiWindow.h"

#include "launcher/editor/phEditorObject.h"
#include "launcher/editor/phEditorObjectManagerSystem.h"
#include "launcher/editor/phEditorScenePickerSystem.h"
#include "launcher/editor/phEditorPhysicsManagerSystem.h"
#include "launcher/editor/phEditorPhysicsSimulationSettings.h"
#include "launcher/editor/phEditorPhysicsSimulationSettings.h"

#include "rendering/geScene.h"

namespace phys
{
    class PhEditorPhysicsWindow : public PhEditorGuiWindow
    {
    private:

    private:
        PhEditorObjectManagerSystem *objectManager;
        PhEditorScenePickerSystem *scenePicker;
        PhEditorPhysicsManagerSystem *physicsManager;
        PhEditorPhysicsSimulationSettings *physicsSimulationSettings;
        ge::GeScene *geScene;
        ge::GeDebugLineRenderer *debugLineRenderer;

        bool autoUpdateVelocities;

        bool drawVelocities;
        bool drawSleeping;
        bool drawContacts;
        bool drawEdges;

        int indexGraph;

    public:
        PhEditorPhysicsWindow();

        virtual ~PhEditorPhysicsWindow() {}

        virtual void release() override;

    protected:
        virtual void onInit() override;

        virtual void onDraw() override;

        virtual const char *onGetName() override;

        virtual ImGuiWindowFlags getWindowFlags() override;

        void drawVelocitiesF();

        void drawActorSleepingF();

        void drawEdgeContainersF();

        void drawContactsF();
    };
}


#endif