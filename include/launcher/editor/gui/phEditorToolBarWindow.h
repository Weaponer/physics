#ifndef PH_EDITOR_TOOL_BAR_H
#define PH_EDITOR_TOOL_BAR_H

#include "common/phFlags.h"

#include "foundation/phSignal.h"

#include "phEditorGuiWindow.h"

#include "launcher/editor/phEditorObject.h"

namespace phys
{
    class PhEditorToolBarWindow : public PhEditorGuiWindow
    {
    private:
        PhConnection selectConnection;
        PhConnection unselectConnection;
        PhEditorObject *selectedObject;

        PhFlags currentState;

    public:
        PhEditorToolBarWindow();

        virtual ~PhEditorToolBarWindow() {}

        virtual void release() override;

    protected:
        void onSelectObject(PhEditorObject *_object);

        void onUnselectObject();

        virtual void onInit() override;

        virtual void onDraw() override;

        virtual const char *onGetName() override;

        virtual ImGuiWindowFlags getWindowFlags() override;
    };
}

#endif