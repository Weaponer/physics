#ifndef PH_EDITOR_INSPECTOR_WINDOW_H
#define PH_EDITOR_INSPECTOR_WINDOW_H

#include <MTH/color.h>

#include "foundation/phSignal.h"

#include "phEditorGuiWindow.h"

#include "launcher/editor/phEditorObject.h"
#include "launcher/editor/phEditorPhysicsManagerSystem.h"

#include "physics/phPhysics.h"
#include "physics/phScene.h"
#include "geometry/phGeometry.h"

namespace phys
{
    class PhEditorInspectorWindow : public PhEditorGuiWindow
    {
    protected:
        PhConnection selectConnection;
        PhConnection unselectConnection;

        PhPhysics *physics;
        PhScene *scene;
        PhEditorPhysicsManagerSystem *physicsManager;
        PhEditorObject *object;

        mth::vec3 safeEulerAngles;

        PhEasyStack<mth::vec3> safeGeometryEulers;

    public:
        PhEditorInspectorWindow();

        virtual ~PhEditorInspectorWindow() {}

        virtual void release() override;

    protected:
        virtual void onInit() override;

        void onSelectObject(PhEditorObject *_object);

        void onUnselectObject();

        virtual void onDraw() override;

        void drawName(float _mainNameOffset);

        void drawTransform(float _mainNameOffset);

        void drawRenderer(float _mainNameOffset);

        void drawAddRigidBody(float _mainNameOffset);

        void drawRigidDynamic(float _mainNameOffset);

        void drawRigidStatic(float _mainNameOffset);

        void drawShapes(float _mainNameOffset);

        void drawGeometry(PhGeometry *_geometry, uint32_t _index, float _offset, float _mainNameOffset, bool *_outNeedDestroy);

        mth::col getColor(ge::GeSelectableRenderer *_object);

        void setColor(ge::GeSelectableRenderer *_object, const mth::col &_color);

        virtual const char *onGetName() override;
    };
}

#endif