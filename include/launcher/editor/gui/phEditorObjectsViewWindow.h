#ifndef PH_EDITOR_OBJECTS_VIEW_WINDOW_H
#define PH_EDITOR_OBJECTS_VIEW_WINDOW_H

#include "common/phFlags.h"

#include "foundation/phSignal.h"

#include "phEditorGuiWindow.h"

#include "launcher/editor/phEditorObject.h"
#include "launcher/editor/phEditorObjectManagerSystem.h"

namespace phys
{
    class PhEditorObjectsViewWindow : public PhEditorGuiWindow
    {
    private:
        PhConnection selectConnection;
        PhConnection unselectConnection;

        PhEditorObjectManagerSystem *objectManagerSystem;
        int lastSelectedIndex;

    public:
        PhEditorObjectsViewWindow();

        virtual ~PhEditorObjectsViewWindow() {}

        virtual void release() override;

    protected:
        void onSelectObject(PhEditorObject *_object);

        void onUnselectObject();

        virtual void onInit() override;

        virtual void onDraw() override;

        virtual const char *onGetName() override;
    };
}

#endif