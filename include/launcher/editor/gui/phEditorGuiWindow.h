#ifndef PH_EDITOR_GUI_WINDOW_H
#define PH_EDITOR_GUI_WINDOW_H

#include "plugins/imgui/imgui.h"

#include "common/phBase.h"

#include "foundation/phContext.h"

namespace phys
{
    class PhEditorGuiWindow : public PhBase
    {
    protected:
        const PhContext *context;

    public:
        PhEditorGuiWindow();

        virtual ~PhEditorGuiWindow() {}

        virtual void release() override;

        void init(const PhContext *_context);

        void draw();

    protected:
        virtual void onInit() = 0;

        virtual void onDraw() = 0;

        virtual const char *onGetName() = 0;

        virtual ImGuiWindowFlags getWindowFlags() { return 0; }
    };
}

#endif