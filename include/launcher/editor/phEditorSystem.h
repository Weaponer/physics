#ifndef PH_EDITOR_SYSTEM_H
#define PH_EDITOR_SYSTEM_H

#include "common/phBase.h"

#include "foundation/phContext.h"

namespace phys
{
    class PhEditorSystem : public PhBase
    {
    protected:
        const PhContext *context;

    public:
        PhEditorSystem() : context(NULL) {}

        virtual ~PhEditorSystem() {}

        void init(const PhContext *_context);

        void update(float _deltaTime);

        void cleanup();

    protected:
        virtual void onInit(){};

        virtual void onUpdate(float _deltaTime){};

        virtual void onCleanup(){};
    };
}

#endif