#ifndef PH_EDITOR_PHYSICS_MANAGER_SYSTEM_H
#define PH_EDITOR_PHYSICS_MANAGER_SYSTEM_H

#include "phEditorSystem.h"
#include "phEditorObject.h"

#include "common/phFlags.h"

#include "physics/phPhysics.h"
#include "physics/phScene.h"

namespace phys
{
    class PhEditorObjectManagerSystem;
    class PhEditorPhysicsManagerSystem : public PhEditorSystem
    {
    private:
        PhPhysics *physics;
        PhScene *scene;
        PhEditorObjectManagerSystem *objectManager;

    public:
        PhEditorPhysicsManagerSystem();

        virtual ~PhEditorPhysicsManagerSystem() {}

        virtual void release() override;

        void destroyRigidBody(PhEditorObject *_object);

        void setRigidDynamic(PhEditorObject *_object);

        void setRigidStatic(PhEditorObject *_object);

        void addShape(PhEditorObject *_object);

        void removeShape(PhEditorObject *_object, uint32_t _index);

        void addSphere(PhEditorObject *_object, uint32_t _index);

        void addBox(PhEditorObject *_object, uint32_t _index);

        void addCapsule(PhEditorObject *_object, uint32_t _index);

        void addPlane(PhEditorObject *_object, uint32_t _index);

        void removeGeometry(PhEditorObject *_object, uint32_t _indexShape, uint32_t _indexGeometry);

        PhScene *getPhysicsScene() const { return scene; }

    protected:
        virtual void onInit() override;

        virtual void onUpdate(float _deltaTime) override;
        
    };
}
#endif