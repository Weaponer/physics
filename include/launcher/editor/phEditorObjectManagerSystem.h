#ifndef PH_EDITOR_OBJECT_MANAGER_SYSTEM_H
#define PH_EDITOR_OBJECT_MANAGER_SYSTEM_H

#include "phEditorSystem.h"
#include "phEditorObject.h"

#include "common/phFlags.h"

#include "foundation/phTableContainer.h"

#include "rendering/geMesh.h"
#include "rendering/geScene.h"
#include "common/phResourceProvider.h"

#include "phEditorPhysicsManagerSystem.h"

namespace phys
{
    class PhEditorScenePickerSystem;

    class PhEditorObjectManagerSystem : public PhEditorSystem
    {
    public:
        enum : PhFlags
        {
            PhEditorObject_None = 0,
            PhEditorObject_Cube = 1,
            PhEditorObject_Sphere = 2,
            PhEditorObject_Capsule = 3,
            PhEditorObject_Floor = 4,
        };

    protected:
        ge::GeScene *scene;
        PhResourceProvider *resourceProvider;
        PhEditorScenePickerSystem *scenePickerSystem;

        PhTableContainer<PhEditorObject *> objects;
        ge::GeMesh *sphereMesh;
        ge::GeMesh *cubeMesh;
        ge::GeMesh *capsuleMesh;

        PhEditorPhysicsManagerSystem *physicsManager;

    public:
        PhEditorObjectManagerSystem();

        virtual ~PhEditorObjectManagerSystem() {}

        virtual void release() override;

        PhEditorObject *createEditorObject(PhFlags _type);

        void destroyEditorObject(PhEditorObject *_object);

        PhEditorObject *const *getTableEditorObjects() const { return objects.getMemory(0); }

        unsigned int getTableEditorObjectsSize() const { return objects.getPosBuffer(); }

    protected:
        virtual void onInit() override;

        virtual void onUpdate(float _deltaTime) override;

        virtual void onCleanup() override;

        void destroyObject(PhEditorObject *_object);
    };
}

#endif