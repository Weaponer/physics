#ifndef PH_EDITOR_SCENE_PICKER_SYSTEM_H
#define PH_EDITOR_SCENE_PICKER_SYSTEM_H

#include "foundation/phTableContainer.h"
#include "foundation/phSignal.h"

#include "rendering/geSelectableRenderer.h"
#include "rendering/geScene.h"

#include "window/window.h"

#include "phEditorSystem.h"
#include "phEditorMouseState.h"
#include "phEditorSignals.h"
#include "phEditorObject.h"

namespace phys
{
    class PhEditorScenePickerSystem : public PhEditorSystem
    {
    private:
        Window *window;
        ge::GeScene *scene;
        PhEditorMouseState *mouseState;

        PhConnection createConnection;
        PhConnection destroyConnection;

        PhTableContainer<PhEditorObject *> indexes;
        PhEditorObject *selectObject;

        bool isBeginPress;

    public:
        PhEditorScenePickerSystem();

        virtual ~PhEditorScenePickerSystem() {}

        virtual void release() override;

        void selectEditorObject(PhEditorObject *_object);

        void registerRenderer(PhEditorObject *_object);

        void unregisterRenderer(PhEditorObject *_object);

    protected:
        virtual void onInit() override;

        virtual void onUpdate(float _deltaTime) override;

        virtual void onCleanup() override;
    };
}

#endif