#ifndef PH_EDITOR_PIVOT_SYSTEM_H
#define PH_EDITOR_PIVOT_SYSTEM_H

#include "foundation/phSignal.h"

#include "phEditorSystem.h"
#include "phPivotMouseControl.h"
#include "phEditorMouseState.h"
#include "phEditorSignals.h"
#include "phEditorObject.h"

#include "rendering/gePivotRenderer.h"
#include "rendering/geScene.h"

#include "window/window.h"

namespace phys
{
    class PhEditorPivotSystem : public PhEditorSystem
    {
    private:
        struct ObjectState
        {
            mth::vec3 startPosition;
            mth::vec3 startScale;
            mth::quat startRotate;
        };

    private:
        ObjectState startState;
        PhConnection selectConnection;
        PhConnection unselectConnection;
        PhConnection pivotModeConnection;
        PhEditorObject *controlEditorObject;
        PhFlags pivotMode;

        Window *window;
        ge::GeScene *scene;
        PhEditorMouseState *mouseState;

        PhPivotMouseControl pivotMouseControl;
        ge::GePivotRenderer *pivotRenderer;

    public:
        PhEditorPivotSystem();

        virtual ~PhEditorPivotSystem() {}

        virtual void release() override;

    private:
        void onSelectObject(PhEditorObject *_object);

        void onUnselectObject();

        void onChangePivotMode(PhFlags _mode);

    protected:
        virtual void onInit() override;

        virtual void onUpdate(float _deltaTime) override;

        virtual void onCleanup() override;

        bool tryLockPivot();

        void unlockPivot();
    };
}

#endif