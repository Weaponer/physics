#ifndef PH_EDITOR_H
#define PH_EDITOR_H

#include "common/phBase.h"

#include "foundation/phContext.h"

#include "phEditorContext.h"

#include "phEditorMouseState.h"
#include "phEditorPhysicsSimulationSettings.h"

#include "phEditorGuiSystem.h"
#include "phCameraControlSystem.h"
#include "phEditorScenePickerSystem.h"
#include "phEditorObjectManagerSystem.h"
#include "phEditorPivotSystem.h"
#include "phEditorSpawnObjectSystem.h"
#include "phEditorPhysicsManagerSystem.h"

namespace phys
{
    class PhEditor : public PhBase
    {
    private:
        PhContext editorContext;

        PhEditorMouseState mouseState;
        PhEditorPhysicsSimulationSettings physicsSimulationSettings;

    private:
        PhEditorGuiSystem guiSystem;
        PhCameraControlSystem cameraControl;
        PhEditorScenePickerSystem pickerSystem;
        PhEditorObjectManagerSystem objectManagerSystem;
        PhEditorPivotSystem pivotSystem;
        PhEditorSpawnObjectSystem spawnSystem;
        PhEditorPhysicsManagerSystem physicsManager;

    public:
        PhEditor();

        virtual ~PhEditor() {}

        virtual void release() override;

        void init(const PhEditorInitContext &_context);

        void update(float _deltaTime);

        PhEditorPhysicsSimulationSettings getPhysicsSimulationSettings() const { return physicsSimulationSettings; };
    };
}

#endif