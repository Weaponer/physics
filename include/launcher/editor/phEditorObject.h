#ifndef PH_EDITOR_OBJECT_H
#define PH_EDITOR_OBJECT_H

#include <string>

#include "common/phBase.h"

#include "rendering/geRenderer.h"
#include "rendering/geSelectableRenderer.h"

#include "physics/phRigidBody.h"

namespace phys
{
    class PhEditorObject : public PhBase
    {
    protected:
        std::string *name;
        ge::GeSelectableRenderer *renderer;
        uint indexRegister;

        PhRigidBody *rigidBody;

    public:
        PhEditorObject(ge::GeSelectableRenderer *_renderer, uint _indexRegister);

        virtual ~PhEditorObject() {}

        virtual void release() override;

        const char *getName() const;

        void setName(const char *_name);

        void syncWithRigidbody();

        ge::GeSelectableRenderer *getRenderer() { return renderer; }

        uint getIndex() const { return indexRegister; }

        PhRigidBody *getRigidBody() const { return rigidBody; }

        void setRigidBody(PhRigidBody *_actor);

        const mth::vec3 &getPosition() const { return renderer->getPosition(); }

        void setPosition(mth::vec3 _position);

        const mth::vec3 &getSize() const { return renderer->getSize(); }

        void setSize(mth::vec3 _size) { renderer->setSize(_size); }

        const mth::quat &getRotation() const { return renderer->getRotation(); }

        void setRotation(mth::quat _rotation);

    };
}

#endif