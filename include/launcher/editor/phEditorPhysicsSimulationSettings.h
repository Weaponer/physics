#ifndef PH_EDITOR_PHYSICS_SIMULATION_SETTINGS_H
#define PH_EDITOR_PHYSICS_SIMULATION_SETTINGS_H

#include "common/phBase.h"
#include "physics/phSimulationSettings.h"

namespace phys
{
    struct PhEditorPhysicsSimulationSettings : public PhBase
    {
        bool simulate;
        bool doOneStep;
        bool useDeltaTime;
        float timeStep;

        PhSimulationSettings simulationSettings;
    };
}

#endif