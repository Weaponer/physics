#ifndef PH_EDITOR_SPAWN_OBJECT_SYSTEM_H
#define PH_EDITOR_SPAWN_OBJECT_SYSTEM_H

#include "phEditorSystem.h"

#include "phEditorObjectManagerSystem.h"
#include "phEditorScenePickerSystem.h"

#include "rendering/geScene.h"

namespace phys
{
    class PhEditorSpawnObjectSystem : public PhEditorSystem
    {
    private:
        PhEditorObjectManagerSystem *objectManager;
        PhEditorScenePickerSystem *scenePicker;
        ge::GeScene *scene;

    public:
        PhEditorSpawnObjectSystem();

        virtual ~PhEditorSpawnObjectSystem() {}

        virtual void release() override;

        void spawCube();

        void spawnSphere();

        void spawnCapsule();

        void spawnFloor();

    protected:
        virtual void onInit() override;

        virtual void onUpdate(float _deltaTime) override;

        virtual void onCleanup() override;

        mth::vec3 getSpawnPosition();
    };
}

#endif