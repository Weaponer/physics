#ifndef PH_EDITOR_GUI_SYSTEM_H
#define PH_EDITOR_GUI_SYSTEM_H

#include "phEditorSystem.h"
#include "phEditorMouseState.h"

#include "foundation/phContext.h"

namespace phys
{
    class PhEditorGuiSystem : public PhEditorSystem
    {
    protected:
        PhEditorMouseState *mouseState;
        PhContext guiContext;

    public:
        PhEditorGuiSystem() : PhEditorSystem(), mouseState(NULL), guiContext() {}

        virtual ~PhEditorGuiSystem() {}

        virtual void release() override;

    protected:
        virtual void onInit() override;

        virtual void onUpdate(float _deltaTime) override;

        virtual void onCleanup() override;
    };
}

#endif