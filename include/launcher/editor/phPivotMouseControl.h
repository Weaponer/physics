#ifndef PH_PIVOT_MOUSE_CONTROL_H
#define PH_PIVOT_MOUSE_CONTROL_H

#include <MTH/vectors.h>

#include "common/phBase.h"
#include "common/phFlags.h"

#include "rendering/geCamera.h"
#include "rendering/gePivotRenderer.h"

namespace phys
{
    class PhPivotMouseControl : public PhBase
    {
    private:
        struct UpdateData
        {
            mth::vec3 pivotPosition;
            mth::vec3 cameraPosition;
            mth::vec3 cameraForward;
            mth::vec3 directionToPivot;      // direction from camera to pivot
            mth::vec3 localDirectionToPivot; // direction from camera to pivot in pivot space

            mth::vec3 pivotForward;
            mth::vec3 pivotUp;
            mth::vec3 pivotRight;
            mth::vec3 rayOrigin;
            mth::vec3 rayDirection;
            mth::vec3 rayEndPoint;
            mth::vec3 localRayOrigin;
            mth::vec3 localRayDirection;
            mth::vec3 localRayEnd;

            mth::vec2 mousePosition;
            
            float pivotScale;
        };

        enum : PhFlags
        {
            PhPivotContact_None = 0,
            PhPivotContact_Segment = 1,
            PhPivotContact_Plane = 2,

            PhPivotRotate_None = 0,
            PhPivotRotate_X = 1,
            PhPivotRotate_Y = 2,
            PhPivotRotate_Z = 3,
            PhPivotRotate_Global = 4,
            PhPivotRotate_Camera = 5,
        };

        struct ContactData
        {
            PhFlags contactType;

            // global space
            mth::vec3 originData;
            mth::vec3 directionData;
            mth::vec3 contactData;

            // pivot rotate additionals
            mth::mat3 rotate;
            PhFlags rotateAxis;

            // pivot scale additionals
            PhFlags activeScaleAxises;

            mth::vec2 mousePosition;
            float scale;
        };

    private:
        ContactData lockContactData;
        bool hasLockContact;

        ge::GePivotRenderer *pivot;
        bool isLock;

    public:
        PhPivotMouseControl() : PhBase(), pivot(NULL), isLock(false) {}

        virtual ~PhPivotMouseControl() {}

        virtual void release() override {}

        void initPivot(ge::GePivotRenderer *_pivot);

        void resetPivot();

        // mouse position [0.0, 1.0] - xy
        void updatePivot(ge::GeCamera *_camera, mth::vec2 _mousePosition);

        void updatePivotBackground(ge::GeCamera *_camera);

        bool tryLockPivot();

        void unlockPivot();

        bool getIsLock() const { return isLock; }

    private:
        UpdateData getUpdateData(ge::GeCamera *_camera, ge::GePivotRenderer *_pivot, mth::vec2 _mousePosition);

        PhFlags getFrontRotateAxis(UpdateData &_data, ge::GeCamera *_camera, ge::GePivotRenderer *_pivot);

        bool getActiveMoveScaleAxis(const UpdateData &_data, const ge::GeCamera *_camera, const ge::GePivotRenderer *_pivot,
                                    PhFlags &_activeAxies, ContactData &_contactData);

        bool getActiveRotateAxis(const UpdateData &_data, const ge::GeCamera *_camera, const ge::GePivotRenderer *_pivot,
                                 PhFlags &_activeAxies, ContactData &_contactData);

    private:
        void movePivot(const ContactData &_firstContactData, const UpdateData &_data, ge::GeCamera *_camera, ge::GePivotRenderer *_pivot);

        void scalePivot(const ContactData &_firstContactData, const UpdateData &_data, ge::GeCamera *_camera, ge::GePivotRenderer *_pivot);

        // need to have posible to change contact data
        void rotatePivot(ContactData &_firstContactData, const UpdateData &_data, ge::GeCamera *_camera, ge::GePivotRenderer *_pivot);
    };
}

#endif