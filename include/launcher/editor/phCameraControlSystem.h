#ifndef PH_CAMERA_CONTROL_SYSTEM_H
#define PH_CAMERA_CONTROL_SYSTEM_H

#include "phEditorSystem.h"

#include "rendering/geScene.h"
#include "rendering/geCamera.h"

#include "window/window.h"

#include "phEditorMouseState.h"

namespace phys
{
    class PhCameraControlSystem : public PhEditorSystem
    {
    protected:
        ge::GeCamera *camera;

        bool isControlCamera;

        float speedMove;
        mth::vec3 newRotate;

        Window *window;
        ge::GeScene *scene;
        PhEditorMouseState *mouseState;

    public:
        PhCameraControlSystem();

        virtual ~PhCameraControlSystem() {}

        virtual void release() override;

    protected:
        virtual void onInit() override;

        virtual void onUpdate(float _deltaTime) override;

        virtual void onCleanup() override;
    };
}

#endif