#ifndef PH_EDITOR_SIGNALS_H
#define PH_EDITOR_SIGNALS_H

#include "common/phFlags.h"
#include "foundation/phSignal.h"

#include "phEditorObject.h"

namespace phys
{
    class PhSelectObjectSig : public PhSignal<PhEditorObject *>
    {
    };

    class PhUnselectObjectSig : public PhEmptySignal
    {
    };

    class PhCreateObjectSig : public PhSignal<PhEditorObject *>
    {
    };

    class PhDestroyObjectSig : public PhSignal<PhEditorObject *>
    {
    };

    class PhChangePivotModeSig : public PhSignal<PhFlags>
    {
    };
}

#endif