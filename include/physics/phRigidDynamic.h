#ifndef PH_RIGID_DYNAMIC_H
#define PH_RIGID_DYNAMIC_H

#include "phRigidBody.h"

namespace phys
{
    class PhRigidDynamic : public PhRigidBody
    {
    public:
        PhRigidDynamic() : PhRigidBody() {}
        virtual ~PhRigidDynamic() {}

        virtual void setFlags(PhRigidBodyDynamicFlags _flag, bool _enable) = 0;

        virtual PhRigidBodyDynamicFlags getFlags() const = 0;

        virtual void setMass(float _mass) = 0;

        virtual float getMass() const = 0;

        virtual void setCenterOfMass(mth::vec3 _localP) = 0;

        virtual mth::vec3 getCenterOfMass() const = 0;

        virtual void setLinearDrag(float _linearDrag) = 0;

        virtual float getLinearDrag() const = 0;

        virtual void setAngularDrag(float _angularDrag) = 0;

        virtual float getAngularDrag() const = 0;
    };
}

#endif