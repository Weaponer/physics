#ifndef PH_PHYSICS_H
#define PH_PHYSICS_H

#include "phFoundation.h"
#include "common/phRefCounter.h"
#include "common/phBase.h"
#include "physics/phScene.h"
#include "phFactory.h"
#include "physicsEngine/peFoundation.h"

namespace phys
{
#ifndef FACTORY_COMMON_CREATE_FUNCTION
#define FACTORY_COMMON_CREATE_FUNCTION(function, outputType) \
    inline outputType function() { return factory->function(); }
#endif

#ifndef FACTORY_COMMON_DESTROY_FUNCTION
#define FACTORY_COMMON_DESTROY_FUNCTION(function, inputType) \
    inline void function(inputType _in) { factory->function(_in); }
#endif

    class PhPhysics : public PhBase
    {
    private:
        PhFoundation *foundation;
        PeFoundation *peFoundation;
        PhFactory *factory;

        PhPhysics(PhFoundation *_foundation, PeFoundation *_peFoundation);

        virtual ~PhPhysics() {}

    public:
        static PhPhysics *createPhysics(PhFoundation *_foundation);

        static void destroyPhysics(PhPhysics *_physics);

        virtual void release() override;

#include "phFactoryFuncs.inl"
    };
#undef FACTORY_COMMON_CREATE_FUNCTION
#undef FACTORY_COMMON_DESTROY_FUNCTION
}

#endif