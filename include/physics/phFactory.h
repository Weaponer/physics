#ifndef PH_FACTORY_H
#define PH_FACTORY_H

#include "common/phBase.h"
#include "physics/phRigidDynamic.h"
#include "physics/phRigidStatic.h"
#include "geometry/phShape.h"
#include "geometry/phBoxGeometry.h"
#include "geometry/phPlaneGeometry.h"
#include "geometry/phSphereGeometry.h"
#include "geometry/phCapsuleGeometry.h"
#include "phMaterial.h"

namespace phys
{
#ifndef FACTORY_COMMON_CREATE_FUNCTION
#define FACTORY_COMMON_CREATE_FUNCTION(function, outputType) virtual outputType function() = 0;
#endif

#ifndef FACTORY_COMMON_DESTROY_FUNCTION
#define FACTORY_COMMON_DESTROY_FUNCTION(function, inputType) virtual void function(inputType _in) = 0;
#endif

    class PhFactory : public PhBase
    {
    public:
        PhFactory() {}

        virtual ~PhFactory() {}

#include "phFactoryFuncs.inl"
    };

#undef FACTORY_COMMON_DESTROY_FUNCTION
#undef FACTORY_COMMON_CREATE_FUNCTION
}

#endif