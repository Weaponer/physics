#ifndef PH_RIGID_ACTOR_H
#define PH_RIGID_ACTOR_H

#include <MTH/matrix.h>
#include <MTH/vectors.h>
#include <MTH/quaternion.h>

#include "phActor.h"

#include "geometry/phShape.h"

namespace phys
{
    class PhRigidActor : public PhActor
    {
    public:
        PhRigidActor() : PhActor() {}
        virtual ~PhRigidActor() {}

        virtual mth::vec3 getPosition() const = 0;

        virtual mth::quat getRotation() const = 0;

        virtual void setPosition(const mth::vec3 &_position) = 0;

        virtual void setRotation(const mth::quat &_rotation) = 0;

        virtual void addShape(PhShape *_shape) = 0;

        virtual void removeShape(PhShape *_shape) = 0;

        virtual int getCountShapes() = 0;

        virtual PhShape *getShape(int _index) = 0;
    };
}

#endif
