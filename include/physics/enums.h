#ifndef ENUMS_H
#define ENUMS_H

#include "common/phFlags.h"

namespace phys
{
    enum PhRigidBodyDynamicFlags : PhFlags
    {
        RigidBodyKinematic = 1,
        OrientationFeedback = 2,
        VelocitiesFeedback = 4,
    };

    enum PhStateActorChangesFlags : PhFlags
    {
        ChangeTransform = 1,
        ChangeShape = 2,
        ChangeKinematicFlag = 4,
        ChangeMassCenter = 8,
    };

    enum PhStateShapeChangesFlags : PhFlags
    {
        ChangeGeometries = 1,
    };
}

#endif