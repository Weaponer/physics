#ifndef PH_ACTOR_H
#define PH_ACTOR_H

#include "common/phFlags.h"

namespace phys
{
    class PhScene;

    class PhActor
    {
    public:
        enum : PhFlags
        {
            PhActor_None = 0,
            PhActor_RigidDynamic = 1,
            PhActor_RigidStatic = 2,
        };

    public:
        PhActor() {}
        virtual ~PhActor() {}

        virtual const PhScene *getScene() const = 0;

        virtual PhFlags getActorType() const = 0;
    };
}

#endif
