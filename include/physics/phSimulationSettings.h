#ifndef PH_SIMULATION_SETTINGS_H
#define PH_SIMULATION_SETTINGS_H

namespace phys
{
    struct PhSimulationSettings
    {
    public:
        enum SolverMode : PhFlags
        {
            SolverMode_PBD = 0,
            SolverMode_LCP = 1,
            CountSolvers = 2,
        };

        SolverMode solveMode;

        bool updateVelocities;
        int countIterations;
        int countSubGraphs;

        float depthContactMin;
        float depthContactMax;

        float linearDamping;
        float angularDamping;

        float stiffnessPBD;

        float stiffnessLCP;
        float dampingLCP;

    public:
        PhSimulationSettings() : solveMode(SolverMode_PBD), updateVelocities(true), countIterations(6), countSubGraphs(8),
                                 depthContactMin(0.02f), depthContactMax(0.05f), linearDamping(0.5f), angularDamping(1.5f),
                                 stiffnessPBD(5.0f), stiffnessLCP(1000000), dampingLCP(100000) {}

        ~PhSimulationSettings() {}
    };
}

#endif