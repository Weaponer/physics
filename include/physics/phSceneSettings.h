#ifndef PH_SCENE_SETTINGS_H
#define PH_SCENE_SETTINGS_H

#include <MTH/vectors.h>

#include "common/phBase.h"
#include "common/phFlags.h"

namespace phys
{
    struct PhSceneSettings
    {
    public:
        enum Percision : PhFlags
        {
            Percision_0p1 = 1,
            Percision_0p01 = 10,
            Percision_0p001 = 100,
            Percision_0p0001 = 1000,
            Percision_0p00001 = 10000,
        };

        mth::vec3 gravitation;
        Percision percision;

    public:
        PhSceneSettings() : gravitation(0.0f, -9.81f, 0.0f), percision(Percision::Percision_0p01) {}

        ~PhSceneSettings() {}
    };
}

#endif