#ifndef PH_SCENE_H
#define PH_SCENE_H

#include "common/phBase.h"

#include "phOutputDebugData.h"

#include "phSceneSettings.h"
#include "phSimulationSettings.h"
#include "phSettingsFeedbackDebugData.h"

namespace phys
{
    class PhActor;
    class PhScene : public PhBase
    {
    public:
        PhScene() : PhBase() {}

        virtual ~PhScene() {}

        virtual void addActor(PhActor *_actor) = 0;

        virtual void removeActor(PhActor *_actor) = 0;

        virtual void simulation(float _deltaTime, const PhSimulationSettings &_settings) = 0;

        virtual void fetch(bool _waitFinish = true) = 0;

        virtual bool isSimulated() const = 0;

        virtual PhSceneSettings *getSceneSettings() = 0;

        virtual const PhSceneSettings &getSceneSettings() const = 0;

        virtual void setSceneSettings(const PhSceneSettings &_settings) = 0;

        virtual PhSettingsFeedbackDebugData *getSettingsFeedbackDebug() = 0;

        virtual const PhSettingsFeedbackDebugData &getSettingsFeedbackDebug() const = 0;

        virtual void setSettingsFeedbackDebug(const PhSettingsFeedbackDebugData &_settings) = 0;

        virtual const PhOutputDebugData *getOutputDebugData() const = 0;
    };
}

#endif