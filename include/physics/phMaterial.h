#ifndef PH_MATERIAL_H
#define PH_MATERIAL_H

#include "common/phRefCounter.h"

namespace phys
{
    class PhMaterial : public PhRefCounter
    {
    public:
        PhMaterial() {}

        virtual ~PhMaterial() {}

        virtual void setStaticFriction(float _friction) = 0;

        virtual void setDynamicFriction(float _friction) = 0;

        virtual void setBounciness(float _bounciness) = 0;

        virtual float getStaticFriction() const = 0;

        virtual float getDynamicFriction() const = 0;

        virtual float getBounciness() const = 0;
    };
}

#endif