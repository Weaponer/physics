#ifndef PH_SETTINGS_FEEDBACK_DEBUG_DATA_H
#define PH_SETTINGS_FEEDBACK_DEBUG_DATA_H

namespace phys
{
    struct PhSettingsFeedbackDebugData
    {
    public:
        bool enable;
        bool sleepingActors;
        bool containers;
        bool contacts;
        bool actorsOrientation;

    public:
        PhSettingsFeedbackDebugData() : enable(false), sleepingActors(false), containers(false), 
                                        contacts(false), actorsOrientation(false) {}

        ~PhSettingsFeedbackDebugData() {}
    };
}

#endif