#ifndef IN_OUTPUT_DEBUG_DATA_H
#define IN_OUTPUT_DEBUG_DATA_H

#include "foundation/phEasyStack.h"

#include "physics/phOutputDebugData.h"

namespace phys
{
    class InOutputDebugData : public PhOutputDebugData
    {

    private:
        PhEasyStack<Orientation> obbOrientaions;
        PhEasyStack<KeyOBB> keyOBBs;
        PhEasyStack<Contact> contacts;
        PhEasyStack<ContainerContacts> containers;
        PhEasyStack<ActorOrientation> actorOrientations;
        PhEasyStack<ActorVelocity> actorVelocity;

        mth::mat4 orientationGlobalBoundOBBs;

    public:
        InOutputDebugData();

        virtual ~InOutputDebugData() {}

        virtual void release() override;

        virtual void clearData() override;

        virtual uint32_t getCountObbs() const override;

        virtual const Orientation *getObbsBuffer() const override;

        virtual uint32_t getCountKeyOBBs() const override;

        virtual const KeyOBB *getKeyOBBsBuffer() const override;

        virtual const mth::mat4 &getOrientationGlobalBoundOBBs() const override;

        virtual uint32_t getCountContact() const override;

        virtual const Contact *getContactBuffer() const override;

        virtual uint32_t getCountContainersContacts() const override;

        virtual const ContainerContacts *getContainersContacts() const override;

        virtual uint32_t getCountActorOrientations() const override;

        virtual const ActorOrientation *getActorOrientations() const override;

        virtual uint32_t getCountActorVelocity() const override;

        virtual const ActorVelocity *getActorVelocity() const override;

    public:
        void addContact(mth::vec3 _firstPosition, mth::vec3 _secondPosition, mth::vec3 _firstOrigin, mth::vec3 _secondOrigin);

        void addObbOrientation(const mth::quat &_rot, const mth::vec3 &_size, const mth::vec3 &_pos, int _indexOBBsOfActor, int _indexActor, int _indexDynamicActor);

        void addKeyOBB(uint32_t _index, uint32_t _key);

        void setOrientationGlobalBoundOBBs(const mth::mat4 &_mat);

        void addCluster(uint32_t _mainActor, uint32_t _countActors, const uint32_t *_pActors);

        void addContactsContainer(const ContainerContacts &_container);

        void setIndexGraphGeneralContainer(uint32_t _index, int32_t _indexGraph);

        void addActorOrientation(const mth::quat &_rot, const mth::vec3 &_pos);

        void addActorVelocity(const mth::vec3 &_linearVelocity, const mth::vec3 &_angularVelocity, bool _sleeping, float _timeSleeping);
    };
}

#endif