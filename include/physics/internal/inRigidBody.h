#ifndef IN_RIGID_BODY_H
#define IN_RIGID_BODY_H

#include "inRigidActor.h"

#include "inShapeListener.h"

namespace phys
{
    class InRigidBody : public InRigidActor
    {
    public:
        virtual void onShapeChanged(InShape *_shape) = 0;

    private:
        void inOnShapeChanged(InShape *_shape) // The shapesListener will call it;
        {
            onShapeChanged(_shape);
        }

    public:
        InShapeListener<InRigidBody> shapesListener;

        InRigidBody() : InRigidActor(), shapesListener(this, &InRigidBody::inOnShapeChanged)
        {
        }

        virtual ~InRigidBody() {}

        virtual void release() override
        {
            shapesListener.release();
            InRigidActor::release();
        }
    };
}

#endif