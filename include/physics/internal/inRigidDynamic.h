#ifndef IN_RIGID_DYNAMIC_H
#define IN_RIGID_DYNAMIC_H

#include <stdlib.h>

#include "inRigidBodyTemplate.h"
#include "physics/phRigidDynamic.h"

namespace phys
{
    class InRigidDynamic : public InRigidBodyTemplate<PhRigidDynamic>
    {
    private:
        PhRigidBodyDynamicFlags dynamicFlags;

        mth::vec3 localCenterOfMass;
        float mass;
        float linearDrag;
        float angularDrag;

    public:
        InRigidDynamic()
            : InRigidBodyTemplate<PhRigidDynamic>(), dynamicFlags(OrientationFeedback),
              localCenterOfMass(0.0f, 0.0f, 0.0f), mass(10.0f), linearDrag(0.01f), angularDrag(0.1f) {}

        virtual ~InRigidDynamic() {}

        virtual void setFlags(PhRigidBodyDynamicFlags _flag, bool _enable) override;

        virtual PhRigidBodyDynamicFlags getFlags() const override { return dynamicFlags; };

        virtual const PhScene *getScene() const override { return (const PhScene *)scene; }

        virtual PhFlags getActorType() const override { return PhActor::PhActor_RigidDynamic; }

        virtual void setMass(float _mass) override;

        virtual float getMass() const override { return mass; }

        virtual void setCenterOfMass(mth::vec3 _localP) override;

        virtual mth::vec3 getCenterOfMass() const override { return localCenterOfMass; }

        virtual void setLinearDrag(float _linearDrag) { linearDrag = _linearDrag; }

        virtual float getLinearDrag() const override { return linearDrag; }

        virtual void setAngularDrag(float _angularDrag) { angularDrag = _angularDrag; }

        virtual float getAngularDrag() const override { return angularDrag; }

        virtual bool isDynamic() const override { return (getFlags() & PhRigidBodyDynamicFlags::RigidBodyKinematic) == 0; }
    };
}

#endif