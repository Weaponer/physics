#ifndef IN_SCENE_H
#define IN_SCENE_H

#include "physics/phScene.h"

#include "physicsEngine/peSceneSimulationData.h"

namespace phys
{
    class PeSceneSimulator;
    class InScene : public PhScene
    {
    private:
        PhSceneSettings settings;

        PeSceneSimulator *simulator;
        PeSceneSimulationData *data;
        bool wasLaunched;

    public:
        InScene(PeSceneSimulator *_simulator);

        virtual ~InScene() {}

        virtual void addActor(PhActor *_actor) override;

        virtual void removeActor(PhActor *_actor) override;

        virtual void simulation(float _deltaTime, const PhSimulationSettings &_settings) override;

        virtual void fetch(bool _waitFinish = true) override;

        virtual bool isSimulated() const override { return wasLaunched; }

        virtual PhSceneSettings *getSceneSettings() override;

        virtual const PhSceneSettings &getSceneSettings() const override;

        virtual void setSceneSettings(const PhSceneSettings &_settings) override;

        virtual PhSettingsFeedbackDebugData *getSettingsFeedbackDebug() override;

        virtual const PhSettingsFeedbackDebugData &getSettingsFeedbackDebug() const override;

        virtual void setSettingsFeedbackDebug(const PhSettingsFeedbackDebugData &_settings) override;

        virtual const PhOutputDebugData *getOutputDebugData() const override;

        virtual void release() override;

        friend class PeSceneSimulator;
    };
};

#endif