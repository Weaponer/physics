#ifndef IN_RIGID_ACTOR_H
#define IN_RIGID_ACTOR_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>

#include "inActor.h"

namespace phys
{
    class InRigidActor : public InActor
    {
    public:
        mth::vec3 position;
        mth::quat rotation;

        InRigidActor() : InActor(), position(), rotation() {}

        virtual ~InRigidActor() {}
    };
}

#endif