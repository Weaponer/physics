#ifndef IN_MATERIAL_H
#define IN_MATERIAL_H

#include <algorithm>
#include "physics/phMaterial.h"
#include "physics/internal/inMaterialManager.h"

namespace phys
{
    class InMaterial : public PhMaterial
    {
    public:
        InMaterialManager::Item managerRegistration;

    public:
        float staticFriction;
        float dynamicFriction;
        float bounciness;

    public:
        InMaterial(InMaterialManager *_manager);

        virtual ~InMaterial() {}

        virtual void setStaticFriction(float _friction) override;

        virtual void setDynamicFriction(float _friction) override;

        virtual void setBounciness(float _bounciness) override;

        virtual float getStaticFriction() const override { return staticFriction; }

        virtual float getDynamicFriction() const override { return dynamicFriction; }

        virtual float getBounciness() const override { return bounciness; }

        virtual void release() override;
    };
}

#endif