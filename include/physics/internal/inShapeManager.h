#ifndef IN_SHAPE_MANAGER_H
#define IN_SHAPE_MANAGER_H

#include "inManager.h"

#include "physicsEngine/peGpuStructures.h"

namespace phys
{
    class InShape;
    class InShapeManager : public InManager<InShape, PeGpuDataShape>
    {
    public:
        InShapeManager();

        virtual ~InShapeManager() {}

        virtual void release() override;
    };
}

#endif