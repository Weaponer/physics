#ifndef IN_RIGID_BODY_TEMPLATE_H
#define IN_RIGID_BODY_TEMPLATE_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>

#include "inRigidBody.h"
#include "inShapeListener.h"
#include "inManager.h"
#include "enums.h"

#include "common/phMacros.h"
#include "geometry/internal/inShape.h"

namespace phys
{
    template <typename T> // T outside interface
    class InRigidBodyTemplate : public InRigidBody, public T
    {
    public:
        InRigidBodyTemplate()
            : InRigidBody(), T()
        {
        }

        virtual ~InRigidBodyTemplate() {}

        virtual void release() override
        {
            InRigidBody::release();
        }

    protected:
        virtual void onShapeChanged(InShape *_shape) override
        {
            managerRegistration.onChange(PhStateActorChangesFlags::ChangeShape);
        }

    public:
        virtual mth::vec3 getPosition() const override
        {
            return position;
        }

        virtual mth::quat getRotation() const override
        {
            return rotation;
        }

        virtual void setPosition(const mth::vec3 &_position) override
        {
            if (_position != position)
            {
                position = _position;
                managerRegistration.onChange(PhStateActorChangesFlags::ChangeTransform);
            }
        }

        virtual void setRotation(const mth::quat &_rotation) override
        {
            if (rotation != _rotation)
            {
                rotation = _rotation;
                managerRegistration.onChange(PhStateActorChangesFlags::ChangeTransform);
            }
        }

        virtual void addShape(PhShape *_shape) override
        {
            shapesListener.registerShape(dynamic_cast<InShape *>(_shape));
            managerRegistration.onChange(PhStateActorChangesFlags::ChangeShape);
        }

        virtual void removeShape(PhShape *_shape) override
        {
            shapesListener.unregisterShape(dynamic_cast<InShape *>(_shape));
            managerRegistration.onChange(PhStateActorChangesFlags::ChangeShape);
        }

        virtual int getCountShapes() override
        {
            return shapesListener.countShapes();
        }

        virtual PhShape *getShape(int _index) override
        {
            return shapesListener.getShape(_index);
        }
    };
}

#endif