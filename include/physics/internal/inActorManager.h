#ifndef IN_ACTOR_MANAGER_H
#define IN_ACTOR_MANAGER_H

#include "inManager.h"

#include "physicsEngine/peGpuStructures.h"

namespace phys
{

    class InActor;
    class InActorManager : public InManager<InActor, PeGpuDataActor>
    {
    public:
        InActorManager() : InManager<InActor, PeGpuDataActor>() {}

        virtual ~InActorManager() {}
    };
}

#endif