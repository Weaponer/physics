#ifndef IN_MATERIAL_MANAGER_H
#define IN_MATERIAL_MANAGER_H

#include "inManager.h"

#include "foundation/phEasyStack.h"
#include "foundation/phTableContainer.h"

namespace phys
{
    class InMaterial;
    class InMaterialManager : public InManager<InMaterial, uint8_t>
    {
    public:
        InMaterialManager();

        virtual ~InMaterialManager() {}

        virtual void release() override;
    };
}

#endif