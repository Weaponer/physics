#ifndef IN_RIGID_STATIC_H
#define IN_RIGID_STATIC_H

#include <stdlib.h>

#include "inRigidBodyTemplate.h"
#include "inManager.h"

#include "physics/phRigidStatic.h"

namespace phys
{
    class InRigidStatic : public InRigidBodyTemplate<PhRigidStatic>
    {
    public:
        InRigidStatic() : InRigidBodyTemplate<PhRigidStatic>() {}

        virtual ~InRigidStatic() {}

        virtual const PhScene *getScene() const override { return (const PhScene *)scene; }

        virtual PhFlags getActorType() const override { return PhActor::PhActor_RigidStatic; }

        virtual bool isDynamic() const override { return false; }
    };
}

#endif