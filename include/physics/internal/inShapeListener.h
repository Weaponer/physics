#ifndef IN_SHAPE_LISTENER_H
#define IN_SHAPE_LISTENER_H

#include "foundation/phBuffer.h"
#include "physics/phRigidActor.h"

namespace phys
{
    class InShape;
    class InShapeListenerBase
    {
    protected:
        struct RegistredShape
        {
            InShape *shape;
            unsigned int indexRegister;
        };
        PhBuffer<RegistredShape> shapes;

    public:
        InShapeListenerBase() : shapes() {}

        InShapeListenerBase(const InShapeListenerBase &_listener) = delete;

        virtual ~InShapeListenerBase() {}

        virtual void onShapeChanged(InShape *_shape) {}

        void registerShape(InShape *_shape);

        void unregisterShape(InShape *_shape);

        void unregisterShape(int _index);

        InShape *getShape(int _index) const;

        int countShapes() const;

        void release();
    };

    template <class T>
    class InShapeListener : public InShapeListenerBase
    {
    private:
        T *receiver;
        void (T::*funcCall)(InShape *);

    public:
        InShapeListener(T *_receiver, void (T::*_funcCall)(InShape *))
            : InShapeListenerBase(), receiver(_receiver), funcCall(_funcCall) {}

        InShapeListener(const InShapeListener &_listener) = delete;

        virtual ~InShapeListener() {}

        virtual void onShapeChanged(InShape *_shape) override
        {
            (*receiver.*funcCall)(_shape);
        }
    };
}

#endif