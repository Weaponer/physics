#ifndef IN_FACTORY_H
#define IN_FACTORY_H

#include "physics/phFactory.h"
#include "physicsEngine/peFoundation.h"

namespace phys
{
#ifndef FACTORY_COMMON_CREATE_FUNCTION
#define FACTORY_COMMON_CREATE_FUNCTION(function, outputType) virtual outputType function() override;
#endif

#ifndef FACTORY_COMMON_DESTROY_FUNCTION
#define FACTORY_COMMON_DESTROY_FUNCTION(function, inputType) virtual void function(inputType _in) override;
#endif

    class InFactory : public PhFactory
    {
    protected:
        PeFoundation *peFoundation;
        InMaterialManager *materialManager;
        InShapeManager *shapeManager;


    public:
        InFactory(PeFoundation *_peFoundation);

        virtual ~InFactory() {}

        virtual void release() override;

#include "physics/phFactoryFuncs.inl"
    };
#undef FACTORY_COMMON_DESTROY_FUNCTION
#undef FACTORY_COMMON_CREATE_FUNCTION
}

#endif