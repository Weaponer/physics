#ifndef IN_MANAGER_H
#define IN_MANAGER_H

#include <atomic>

#include "common/phBase.h"
#include "common/phFlags.h"
#include "common/phMacros.h"

#include "foundation/phEasyStack.h"
#include "foundation/phTableContainer.h"

namespace phys
{

    template <class T, typename D> // T class object, D additional data
    class InManager : public PhBase
    {
    protected:
        struct ItemBase
        {
            unsigned int index;
            bool wasUpdate;
            InManager<T, D> *manager;
            T *item;
        };

    public:
        enum ManagerEventBits : PhFlags
        {
            ManagerEvent_None = 0,
            ManagerEvent_Changed = 1,
            ManagerEvent_Created = 2,
            ManagerEvent_Destroyed = 4,
        };

        struct ManagerEvent
        {
            uint index;
            ManagerEventBits event;
            PhFlags flagChanges;
        };

    protected:
        struct ObjectState
        {
            uint event;
            PhFlags flagChanges;
        };

        struct ObjectData
        {
            D data;
            ObjectState state;
            ItemBase *item;
        };

    protected:
        PhEasyStack<uint> events;
        PhTableContainer<ObjectData> objectContainer; // object, state
        uint posReadEvents;
        bool writeEvents;

        std::atomic_bool aIsLock;

    public:
        InManager() : PhBase(), events(), objectContainer(), posReadEvents(0), writeEvents(true), aIsLock(false)
        {
            /*objectContainer.setUseDefault(true);
            ObjectData def = {};
            objectContainer.setDefault(&def);*/
        }

        virtual ~InManager() {}

        bool isLock() const { return aIsLock.load(std::memory_order_acquire); }

        void lock() { aIsLock.store(true, std::memory_order_release); }

        void unlock() { aIsLock.store(false, std::memory_order_release); }

    protected:
        void addEvent(uint _index, ManagerEventBits _event, PhFlags _flagChanges = 0)
        {
            ObjectData *data = objectContainer.getMemory(_index);
            ObjectState state = data->state;

            if (state.event == ManagerEventBits::ManagerEvent_None)
            {
                events.assignmentMemoryAndCopy(&_index);
            }

            if (_event == ManagerEventBits::ManagerEvent_Created)
            {
                if ((state.event & ManagerEventBits::ManagerEvent_Destroyed) && !(state.event & ManagerEventBits::ManagerEvent_Created))
                {
                    state.flagChanges = ~0;
                    state.event = ManagerEventBits::ManagerEvent_Changed;
                }
                else
                {
                    state.flagChanges = 0;
                    state.event = ManagerEventBits::ManagerEvent_Created;
                }
            }
            else if (_event == ManagerEventBits::ManagerEvent_Changed)
            {
                if (!(state.event & ManagerEventBits::ManagerEvent_Created))
                {
                    state.flagChanges |= _flagChanges;
                    state.event = ManagerEventBits::ManagerEvent_Changed;
                }
            }
            else if (_event == ManagerEventBits::ManagerEvent_Destroyed)
            {
                if (state.event & ManagerEventBits::ManagerEvent_Created)
                    state.event = ManagerEventBits::ManagerEvent_Created | ManagerEventBits::ManagerEvent_Destroyed;
                else
                    state.event = ManagerEventBits::ManagerEvent_Destroyed;

                state.flagChanges = 0;
            }
            data->state = state;
        }

    protected:
        void connect(ItemBase *_object)
        {
            ObjectState state;
            state.event = ManagerEventBits::ManagerEvent_None;
            state.flagChanges = 0;

            ObjectData data;
            data.item = _object;
            data.state = state;
            data.data = {};
            objectContainer.assignmentMemoryAndCopy(&data, _object->index);
            if (writeEvents)
                addEvent(_object->index, ManagerEventBits::ManagerEvent_Created);
        }

        void disconnect(ItemBase *_object)
        {
            ObjectData *data = objectContainer.getMemory(_object->index);
            data->item = NULL;
            data->state = {};

            objectContainer.popMemory(_object->index);
            if (writeEvents)
                addEvent(_object->index, ManagerEventBits::ManagerEvent_Destroyed);
            _object->index = 0;
        }

        void objectChainged(ItemBase *_object, PhFlags _flagChanges = 0)
        {
            if (writeEvents)
                addEvent(_object->index, ManagerEventBits::ManagerEvent_Changed, _flagChanges);
        }

    public:
        bool getNextEvent(ManagerEvent *_event)
        {
            while (true)
            {
                if (events.getPos() == 0)
                    return false;

                uint indexMat = *events.getMemory(posReadEvents);
                ObjectData *data = objectContainer.getMemory(indexMat);
                ObjectState state = data->state;
                bool completeEvent = false;
                if (state.event != 0 && (state.event & (ManagerEvent_Created | ManagerEvent_Destroyed)) != (ManagerEvent_Created | ManagerEvent_Destroyed))
                {
                    _event->event = (ManagerEventBits)state.event;
                    _event->flagChanges = state.flagChanges;
                    _event->index = indexMat;

                    if ((state.event & ManagerEvent_Destroyed) == 0)
                        data->item->wasUpdate = false;

                    completeEvent = true;
                }

                data->state = {ManagerEventBits::ManagerEvent_None, 0};

                posReadEvents++;
                if (posReadEvents == events.getPos())
                {
                    events.clearMemory();
                    posReadEvents = 0;
                }

                if (completeEvent)
                {
                    return true;
                }
            }
            return false;
        }

        uint getCapacityTable()
        {
            return objectContainer.getCapacity();
        }

        uint getCountObjects()
        {
            return objectContainer.getCountUse();
        }

        uint getMaxIndexObject()
        {
            return objectContainer.getPosBuffer();
        }

        T *getObjectPerId(uint _id)
        {
            return (*objectContainer.getMemory(_id)).item->item;
        }

        D *getDataPerId(uint _id)
        {
            return &((*objectContainer.getMemory(_id)).data);
        }

        bool isValidId(uint _id)
        {
            return (objectContainer.getMemory(_id))->item != NULL;
        }

        virtual void release() override
        {
            writeEvents = false;

            events.release();
            objectContainer.release();
            PhBase::release();
        }

    public:
        class Item
        {
        private:
            ItemBase base;

        public:
            Item() : base({})
            {
            }

            virtual ~Item() {}

            inline bool isConnected() const { return base.manager != NULL; }

            void connect(InManager<T, D> *_manager, T *_item)
            {
                PH_ASSERT(base.manager == NULL, "The item`ve already connected.");

                base.manager = _manager;
                base.item = _item;
                base.index = 0;
                base.wasUpdate = false;

                PH_ASSERT(!base.manager->isLock(), PH_SCENE_LAUNCHED_ERROR);

                base.manager->connect(&base);
            }

            void onChange(PhFlags _flagChanges = 0)
            {
                if (!isConnected())
                    return;

                PH_ASSERT(!base.manager->isLock(), PH_SCENE_LAUNCHED_ERROR);
                base.wasUpdate = true;
                base.manager->objectChainged(&base, _flagChanges);
            }

            void disconnect()
            {
                PH_ASSERT(!base.manager->isLock(), PH_SCENE_LAUNCHED_ERROR);
                base.manager->disconnect(&base);

                base = {};
            }

            uint32_t getIndex()
            {
                return base.index;
            }
        };

        friend class Item;
    };
}

#endif