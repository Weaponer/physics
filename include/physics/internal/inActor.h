#ifndef IN_ACTOR_H
#define IN_ACTOR_H

#include <ios>

#include "common/phBase.h"

#include "inActorManager.h"

namespace phys
{
    class InScene;
    class InActor : public PhBase
    {
    public:
        InScene *scene;
        InActorManager::Item managerRegistration;

        InActor() : PhBase(), scene(NULL), managerRegistration() {}

        virtual ~InActor() {}

        virtual void release()
        {
            if (managerRegistration.isConnected())
                managerRegistration.disconnect();
            PhBase::release();
        }

        virtual bool isDynamic() const = 0;
    };
}

#endif