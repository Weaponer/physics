#ifndef PH_RIGID_BODY_H
#define PH_RIGID_BODY_H

#include "phRigidActor.h"
#include "enums.h"

namespace phys
{
    class PhRigidBody : public PhRigidActor
    {
    public:
        PhRigidBody() : PhRigidActor() {}
        virtual ~PhRigidBody() {}
    };
}

#endif
