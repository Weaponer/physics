#ifndef PH_OUTPUT_DEBUG_DATA_H
#define PH_OUTPUT_DEBUG_DATA_H

#include <MTH/quaternion.h>
#include <MTH/vectors.h>
#include <MTH/color.h>

#include "common/phBase.h"

namespace phys
{
    class PhOutputDebugData : public PhBase
    {
    public:
        struct Orientation
        {
            mth::quat rotation;
            mth::vec3 size;
            mth::vec3 position;
            int indexOBBsOfActor;
            int indexActor;
            int indexDynamicActor;
            bool isDynamic;
        };

        struct OBBofActor
        {
            int32_t indexOBBsOfActor;
            int32_t indexDynamicActor;
            int32_t indexActor;
        };

        struct KeyOBB
        {
            uint indexOBBs;
            uint key;
        };

        struct Contact
        {
            mth::vec3 firstPosition;
            mth::vec3 secondPosition;
            mth::vec3 firstOrigin;
            mth::vec3 secondOrigin;
        };

        struct ContainerContacts
        {
            int32_t obbA;
            int32_t obbB;
            uint32_t edgeIndex;
        };

        struct ActorOrientation
        {
            mth::vec3 position;
            mth::quat rotation;
        };

        struct ActorVelocity
        {
            mth::vec3 linearVelocity;
            mth::vec3 angularVelocity;
            bool sleeping;
            float timeSleeping;
        };

    public:
        PhOutputDebugData() : PhBase() {}

        virtual ~PhOutputDebugData() {}

        virtual void release() { PhBase::release(); }

        virtual void clearData() = 0;

        virtual uint32_t getCountObbs() const = 0;

        virtual const Orientation *getObbsBuffer() const = 0;

        virtual uint32_t getCountKeyOBBs() const = 0;

        virtual const KeyOBB *getKeyOBBsBuffer() const = 0;

        virtual const mth::mat4 &getOrientationGlobalBoundOBBs() const = 0;

        virtual uint32_t getCountContact() const = 0;

        virtual const Contact *getContactBuffer() const = 0;

        virtual uint32_t getCountContainersContacts() const = 0;

        virtual const ContainerContacts *getContainersContacts() const = 0;

        virtual uint32_t getCountActorOrientations() const = 0;

        virtual const ActorOrientation *getActorOrientations() const = 0;

        virtual uint32_t getCountActorVelocity() const = 0;

        virtual const ActorVelocity *getActorVelocity() const = 0;
    };
}

#endif