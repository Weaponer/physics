#ifndef PH_RIGID_STATIC_H
#define PH_RIGID_STATIC_H

#include "phRigidBody.h"

namespace phys
{
    class PhRigidStatic : public PhRigidBody
    {
    public:
        PhRigidStatic() : PhRigidBody() {}
        virtual ~PhRigidStatic() {}
    };
}

#endif