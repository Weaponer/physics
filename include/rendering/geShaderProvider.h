#ifndef GE_SHADER_PROVIDER_H
#define GE_SHADER_PROVIDER_H

#include "common/phBase.h"
#include "common/phResourceProvider.h"

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/shaders/shader.h"
#include "gpuCommon/shaders/vulkanDescriptorPoolManager.h"

namespace phys::ge
{
    class GeShaderProvider : public PhBase
    {
    protected:
        GeShaderProvider() : PhBase() {}

        virtual ~GeShaderProvider() {}

    public:
        static GeShaderProvider *craeteShaderProvider(vk::VulkanDevice *_device, PhResourceProvider *_provider);

        static void destroyShaderProvider(GeShaderProvider *_provider);

        virtual vk::ShaderModule getShaderModule(const char *_name) = 0;

        virtual vk::VulkanDescriptorPoolManager *getVulkanDescriptorPoolManager() = 0;
    };
}

#endif