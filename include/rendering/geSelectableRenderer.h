#ifndef GE_SELECTABLE_RENDERER_H
#define GE_SELECTABLE_RENDERER_H

#include <MTH/color.h>

#include "geRenderer.h"

namespace phys::ge
{
    class GeSelectableRenderer : public GeRenderer
    {
    public:
        GeSelectableRenderer(PhFlags _typeEntity, PhFlags _staticFlags = 0) : GeRenderer(_typeEntity, _staticFlags) {}

        virtual ~GeSelectableRenderer() {}

        virtual void setColorHighlightLine(const mth::col &_color) = 0;

        virtual const mth::col &getColorHighlightLine() const = 0;

        virtual void setSelected(bool _selected) = 0;

        virtual bool getSelected() const = 0;

        virtual uint getIndexScenePicker() const = 0;

        virtual void setIndexScenePicker(uint _index) = 0;
    };
}

#endif