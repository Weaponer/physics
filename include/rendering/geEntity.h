#ifndef GE_ENTITY_H
#define GE_ENTITY_H

#include <MTH/vectors.h>
#include <MTH/matrix.h>
#include <MTH/quaternion.h>

#include "common/phBase.h"
#include "common/phFlags.h"

namespace phys::ge
{
    class GeEntity : public PhBase
    {
    public:
        enum : PhFlags
        {
            GeEntityNoneResizable = 1,
            GeEntityNoneMovable = 2,
            GeEntityNoneRotatable = 4,
        };

        enum : PhFlags
        {
            GeEntity_None = 0,
            GeEntity_Camera = 1,
            GeEntity_FloorRenderer = 2,
            GeEntity_MeshRenderer = 3,
            GeEntity_SkyRenderer = 4,
            GeEntity_PivotRenderer = 5
        };

    protected:
        mth::vec3 position;
        mth::vec3 size;
        mth::quat rotation;

        mth::mat4 TRS;
        mth::mat4 inverseTRS;

    private:
        PhFlags typeEntity;
        PhFlags staticFlags;
        bool wasUpdate;

    public:
        GeEntity(PhFlags _typeEntity, PhFlags _staticFlags = 0) : position(), size(1, 1, 1), rotation(),
                                                                  TRS(1.0f, 0.0f, 0.0f, 0.0f,
                                                                      0.0f, 1.0f, 0.0f, 0.0f,
                                                                      0.0f, 0.0f, 1.0f, 0.0f,
                                                                      0.0f, 0.0f, 0.0f, 1.0f),
                                                                  inverseTRS(1.0f, 0.0f, 0.0f, 0.0f,
                                                                             0.0f, 1.0f, 0.0f, 0.0f,
                                                                             0.0f, 0.0f, 1.0f, 0.0f,
                                                                             0.0f, 0.0f, 0.0f, 1.0f),
                                                                  typeEntity(_typeEntity),
                                                                  staticFlags(_staticFlags), wasUpdate(true) {}

        virtual ~GeEntity() {}

        PhFlags getTypeEntity() const { return typeEntity; }

        bool isMovable() const { return (staticFlags & GeEntityNoneMovable) == 0; }

        bool isRotatable() const { return (staticFlags & GeEntityNoneRotatable) == 0; }

        bool isResizable() const { return (staticFlags & GeEntityNoneResizable) == 0; }

        const mth::vec3 &getPosition() const { return position; }

        void setPosition(mth::vec3 _position)
        {
            if (staticFlags & GeEntityNoneMovable)
                return;
            position = _position;
            wasUpdate = true;
        }

        const mth::vec3 &getSize() const { return size; }

        void setSize(mth::vec3 _size)
        {
            if (staticFlags & GeEntityNoneResizable)
                return;
            size = _size;
            wasUpdate = true;
        }

        const mth::quat &getRotation() const { return rotation; }

        void setRotation(mth::quat _rotation)
        {
            if (staticFlags & GeEntityNoneRotatable)
                return;
            rotation = _rotation;
            wasUpdate = true;
        }

        const mth::mat4 &getTRS();

        const mth::mat4 &getInverseTRS();

    protected:
        void checkAndRecalculateTRS();

        virtual void onRecalculateTRS() {}

    private:
        void calculateMatrixTRS();
    };
}

#endif