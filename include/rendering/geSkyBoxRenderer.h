#ifndef GE_SKY_BOX_RENDERER_H
#define GE_SKY_BOX_RENDERER_H

#include <MTH/color.h>
#include "geRenderer.h"

namespace phys::ge
{
    class GeSkyBoxRenderer : public GeRenderer
    {
    public:
        GeSkyBoxRenderer(PhFlags _staticFlags = 0) : GeRenderer(GeEntity::GeEntity_SkyRenderer, _staticFlags) {}

        virtual ~GeSkyBoxRenderer() {}

        virtual void setSkyColor(const mth::col &_color) = 0;

        virtual const mth::col &getSkyColor() = 0;
    };
}

#endif