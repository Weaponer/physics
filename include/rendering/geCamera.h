#ifndef GE_CAMERA_H
#define GE_CAMERA_H

#include <MTH/matrix.h>
#include <MTH/plane.h>
#include <MTH/boundOBB.h>
#include <MTH/boundAABB.h>
#include "geEntity.h"

namespace phys::ge
{
    class GeCamera : public GeEntity
    {
    protected:
        mth::mat4 projectionMatrix;
        mth::mat4 inverseProjectionMatrix;

        mth::vec3 pointsPiramidClipping[8];
        mth::plane clippingPlanes[6];

        mth::boundAABB boundCamera;
        float fovTangent;

    public:
        GeCamera();

        virtual ~GeCamera() {}

        void setCameraSetting(float _near, float _far, float _fov, float _aspect);

        mth::boundOBB calculateGlobalOBB() const;

        const mth::mat4 &getProjectionMatrix() const { return projectionMatrix; }

        const mth::mat4 &getInverseProjectionMatrix() const { return inverseProjectionMatrix; }

        int getCountPointsPiramidClipping() const { return 8; }

        const mth::vec3 *getPointsPiramidClipping() const { return pointsPiramidClipping; }

        int getCountClippingPlanes() const { return 6; }

        const mth::plane *getClippingPlanes() const { return clippingPlanes; }

        float getNear() const { return -clippingPlanes[0].dist; }
        
        float getFar() const { return clippingPlanes[1].dist; }

        float getFovTanget() const { return fovTangent; }

        virtual void release() override;
    };
}

#endif