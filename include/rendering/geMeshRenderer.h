#ifndef GE_MESH_RENDERER_H
#define GE_MESH_RENDERER_H

#include <MTH/color.h>

#include "geSelectableRenderer.h"
#include "geMesh.h"

namespace phys::ge
{
    class GeMeshRenderer : public virtual GeSelectableRenderer
    {
    public:
        GeMeshRenderer() {}

        virtual ~GeMeshRenderer() {}

        virtual void setMesh(GeMesh *_mesh) = 0;

        virtual GeMesh *getMesh() = 0;

        virtual void setColor(const mth::col &_color) = 0;

        virtual const mth::col &getColor() = 0;
    };
}

#endif