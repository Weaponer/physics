#ifndef GE_RENDERER_H
#define GE_RENDERER_H

#include <MTH/boundSphere.h>
#include <MTH/boundOBB.h>
#include "geEntity.h"

namespace phys::ge
{
    class GeRenderer : public GeEntity
    {
    public:
        GeRenderer(PhFlags _typeEntity, PhFlags _staticFlags = 0) : GeEntity(_typeEntity, _staticFlags) {}

        virtual ~GeRenderer() {}

        virtual mth::boundSphere getGlobalSphereBound() = 0;

        virtual mth::boundOBB getGlobalOBB() = 0;
    };
}

#endif