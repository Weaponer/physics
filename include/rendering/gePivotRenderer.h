#ifndef GE_PIVOT_RENDERER_H
#define GE_PIVOT_RENDERER_H

#include <MTH/vectors.h>

#include "geRenderer.h"
#include "geCamera.h"

#include "common/phFlags.h"

namespace phys::ge
{
    class GePivotRenderer : public GeRenderer
    {
    public:
        enum : PhFlags
        {
            PivotState_None = 0,
            PivotState_Move = 1,
            PivotState_Rotate = 2,
            PivotState_Scale = 3
        };

        enum : PhFlags
        {
            PivotAxis_None = 0,
            PivotAxis_X = 1,
            PivotAxis_Y = 2,
            PivotAxis_Z = 4,
        };

        struct MoveScaleAxisData
        {
            float sizeCenterCube;
            float radiusConus;
            float heightConus;
            float sizeEdgeCube;
            float sizePlaneRect;
            float offsetPlaneRect; // start from 0.0 0.0
        };

        struct RotateAxisData
        {
            float radiusMainAxis;
            float radiusFrontAxis;
            float radiusOutsideCircle;
            float cylindersHeight;
        };

    protected:
        PhFlags state;
        PhFlags activeAxises;
        PhFlags frontRotateAxis;
        mth::vec3 scaleOffsets;

    public:
        GePivotRenderer() : GeRenderer(GeEntity::GeEntity_PivotRenderer, GeEntity::GeEntityNoneResizable),
                            state(PivotState_None), activeAxises(PivotAxis_None), frontRotateAxis(PivotAxis_None),
                            scaleOffsets(1.0f, 1.0f, 1.0f) {}

        virtual ~GePivotRenderer() {}

        PhFlags getPivotState() const { return state; }

        void setPivotState(PhFlags _state) { state = _state; }

        PhFlags getActiveAxises() const { return activeAxises; }

        void setActiveAxises(PhFlags _activeAxises) { activeAxises = _activeAxises; }

        virtual float getSizePivot() const = 0;

        mth::vec3 getScaleOffsets() const { return scaleOffsets; }

        void setScaleOffsets(mth::vec3 _scaleOffsets) { scaleOffsets = _scaleOffsets; }

        PhFlags getFrontRotateAxis() const { return frontRotateAxis; }

        void setFrontRotateAxis(PhFlags _frontRotateAxis) { frontRotateAxis = _frontRotateAxis; }

        virtual MoveScaleAxisData getMoveScaleAxisData() const = 0;

        virtual RotateAxisData getRotateAxisData() const = 0;

        virtual float getSizePivot(GeCamera *_camera) const = 0;

        virtual float getSizePivotNoDistance(GeCamera *_camera) const = 0;
    };
}

#endif