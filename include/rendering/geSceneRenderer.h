#ifndef GE_SCENE_RENDERER_H
#define GE_SCENE_RENDERER_H

#include "common/phBase.h"
#include "geScene.h"

#include "gpuCommon/vulkanSemaphore.h"
#include "gpuCommon/vulkanFence.h"

namespace phys::ge
{
    class GeSceneRenderer : public PhBase
    {
    protected:
        GeSceneRenderer() : PhBase() {}

        virtual ~GeSceneRenderer() {}

    public:
        virtual void renderScene(GeScene *_scene, uint32_t _imageIndex,
                                 vk::VulkanSemaphore _waitBegin, vk::VulkanSemaphore _signalEnd, vk::VulkanFence _fence = vk::VulkanFence()) = 0;
    };
}

#endif