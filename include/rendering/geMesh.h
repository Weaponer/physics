#ifndef GE_MESH_H
#define GE_MESH_H

#include <MTH/boundAABB.h>

#include <vulkan/vulkan.h>

#include "common/phBase.h"

#include "gpuCommon/memory/vulkanBuffer.h"

namespace phys::ge
{
    class GeMesh : public PhBase
    {
    public:
        GeMesh() : PhBase() {}

        virtual ~GeMesh() {}

        virtual const mth::boundAABB &getBoundAABB() = 0;

        virtual const vk::VulkanBuffer *getVertexBuffer() = 0;

        virtual const vk::VulkanBuffer *getIndexBuffer() = 0;

        virtual const vk::VulkanBuffer *getLineVertexBuffer() = 0;

        virtual uint getCountVertex() = 0;

        virtual uint getCountIndex() = 0;

        virtual uint getCountLineVertex() = 0;

        virtual VkIndexType getIndexType() = 0;
    };
}

#endif