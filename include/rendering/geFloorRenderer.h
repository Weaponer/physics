#ifndef GE_FLOOR_RENDERER_H
#define GE_FLOOR_RENDERER_H

#include "geSelectableRenderer.h"

namespace phys::ge
{
    class GeFloorRenderer : public virtual GeSelectableRenderer
    {
    public:
        GeFloorRenderer() {}

        virtual ~GeFloorRenderer() {}

        virtual void setColor(const mth::col &_color) = 0;

        virtual const mth::col &getColor() = 0;
    };
}

#endif