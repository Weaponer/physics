#ifndef IN_PIVOT_RENDERER_H
#define IN_PIVOT_RENDERER_H

#include "rendering/gePivotRenderer.h"

namespace phys::ge
{
    class InPivotRenderer : public GePivotRenderer
    {
    protected:
        uint index;

    public:
        InPivotRenderer(uint _index) : GePivotRenderer(), index(_index) {}

        virtual ~InPivotRenderer() {}

        virtual void release() override {}

        virtual float getSizePivot() const override;

        virtual MoveScaleAxisData getMoveScaleAxisData() const override;

        virtual RotateAxisData getRotateAxisData() const override;

        virtual float getSizePivot(GeCamera *_camera) const override;

        virtual float getSizePivotNoDistance(GeCamera *_camera) const override;

        uint getIndex() const { return index; }

        virtual mth::boundSphere getGlobalSphereBound() override { return mth::boundSphere(mth::vec3(), MAXFLOAT); }

        virtual mth::boundOBB getGlobalOBB() override { return mth::boundOBB(mth::vec3(), mth::vec3(MAXFLOAT, MAXFLOAT, MAXFLOAT)); }
    };
}

#endif