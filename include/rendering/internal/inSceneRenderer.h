#ifndef IN_SCENE_RENDERER_H
#define IN_SCENE_RENDERER_H

#include "rendering/geSceneRenderer.h"

#include "graphicPipeline/inGraphicPipeline.h"

#include "gpuCommon/vulkanSemaphore.h"

namespace phys::ge
{
    class InSceneRenderer : public GeSceneRenderer
    {
    public:
        InGraphicPipeline *graphicPipeline;

    public:
        InSceneRenderer();

        virtual ~InSceneRenderer() {}

        virtual void release() override;

        virtual void renderScene(GeScene *_scene, uint32_t _imageIndex,
                                 vk::VulkanSemaphore _waitBegin, vk::VulkanSemaphore _signalEnd, vk::VulkanFence _fence = vk::VulkanFence()) override;
    };
}

#endif