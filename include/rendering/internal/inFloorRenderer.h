#ifndef IN_FLOOR_RENDERER_H
#define IN_FLOOR_RENDERER_H

#include "rendering/geFloorRenderer.h"
#include "inSelectableRenderer.h"

namespace phys::ge
{
    class InFloorRenderer : public GeFloorRenderer, public InSelectableRenderer
    {
    public:
        mth::col color;

        InFloorRenderer() : GeSelectableRenderer(GeEntity::GeEntity_FloorRenderer, GeEntityNoneResizable), GeFloorRenderer(), InSelectableRenderer(),
                            color(0.15f, 0.15f, 0.15f) {}

        virtual ~InFloorRenderer() {}

        virtual void release() override;

        virtual void setColor(const mth::col &_color) override { color = _color; }

        virtual const mth::col &getColor() override { return color; }

        virtual mth::boundSphere getGlobalSphereBound() override;

        virtual mth::boundOBB getGlobalOBB() override;
    };
}

#endif