#ifndef IN_SKY_BOX_RENDERER_H
#define IN_SKY_BOX_RENDERER_H

#include "rendering/geSkyBoxRenderer.h"

namespace phys::ge
{
    class InSkyBoxRenderer : public GeSkyBoxRenderer
    {
    public:
        mth::col color;

        InSkyBoxRenderer() : GeSkyBoxRenderer(GeEntityNoneMovable | GeEntityNoneRotatable | GeEntityNoneResizable), color() {}

        virtual ~InSkyBoxRenderer() {}

        virtual void release() override { GeSkyBoxRenderer::release(); }

        virtual void setSkyColor(const mth::col &_color) override { color = _color; }

        virtual const mth::col &getSkyColor() override { return color; }

        virtual mth::boundSphere getGlobalSphereBound() override;

        virtual mth::boundOBB getGlobalOBB() override;
    };
}

#endif