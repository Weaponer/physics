#ifndef IN_GRAPHIC_PIPELINE_H
#define IN_GRAPHIC_PIPELINE_H

#include "common/phBase.h"

#include "threads/phThreadsManager.h"

#include "inScene.h"

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanSemaphore.h"
#include "gpuCommon/vulkanFence.h"
#include "gpuCommon/vulkanSwapchain.h"
#include "gpuCommon/rendering/vulkanFramebuffer.h"
#include "gpuCommon/rendering/renderPassesManager.h"
#include "gpuCommon/rendering/vulkanRenderPass.h"
#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/memory/vulkanImageAllocator.h"
#include "gpuCommon/specialCommands/vulkanCommandReceiver.h"
#include "gpuCommon/specialCommands/vulkanGraphicCommands.h"
#include "gpuCommon/specialCommands/vulkanComputeCommands.h"
#include "gpuCommon/rendering/graphicPipelinesManager.h"

#include "rendering/geShaderProvider.h"

#include "inMemoryLoader.h"
#include "inCameraData.h"
#include "inFullScreenPassData.h"
#include "inMeshRendererCullingManager.h"

#include "passes/inScenePickerPass.h"
#include "passes/inSkyBoxPass.h"
#include "passes/inFloorPass.h"
#include "passes/inMeshRendererPass.h"
#include "passes/inDebugLinesPass.h"
#include "passes/inSelectedEntityPass.h"
#include "passes/inPivotRendererPass.h"

namespace phys::ge
{
    class InGraphicPipeline : public PhBase
    {
    public:
        PhThreadsManager *threadsManager;
        vk::VulkanSwapchain *swapchain;
        vk::VulkanQueue *graphicQueue;
        vk::VulkanQueue *computeQueue;
        vk::VulkanBufferAllocator *bufferAllocator;
        vk::VulkanImageAllocator *imageAllocator;

        vk::VulkanCommandReceiver *commandReceiver;

        GeShaderProvider *shaderProvider;

    private:
        vk::VulkanDevice *device;
        vk::VulkanGraphicCommands *graphicsCommands;
        vk::VulkanComputeCommands *computeCommands;
        vk::RenderPassesManager *renderPassesManager;

        vk::VulkanSemaphore signalEndLoading;

    private:
        struct
        {
            uint32_t countFramebuffers;
            vk::VulkanRenderPass renderPass;
            vk::VulkanImageView depthBufferView;
            vk::VulkanImage *depthBuffer;
            vk::VulkanFramebuffer *framebuffers;
        } framebuffers;

    private:
        InMemoryLoader *memoryLoader;
        vk::GraphicPipelinesManager *graphicPipelinesManager;
        InCamereData *cameraData;
        InFullScreenPassData *fullScreenPassData;
        InMeshRendererCullingManager *meshRendererCullingManager;

        InScenePickerPass *scenePickerPass;
        InSkyBoxPass *skyBoxPass;
        InFloorPass *floorPass;
        InMeshRendererPass *meshRendererPass;
        InDebugLinesPass *debugLinesPass;
        InSelectedEntityPass *selectedEntityPass;
        InPivotRendererPass *pivotRendererPass;
        
    public:
        InGraphicPipeline();

        virtual ~InGraphicPipeline() {}

        virtual void release() override;

        void init();

        bool render(InScene *_scene, uint32_t _imageIndex,
                    vk::VulkanSemaphore _waitBegin, vk::VulkanSemaphore _signalEnd, vk::VulkanFence _fence);

    private:
        void initFramebuffers();

        void destroyFramebuffers();

        void clearRendererTarget(uint32_t _imageIndex);

        void clearDepthBuffer();

        void fillCleanupContext(InPass::CleanupContextData *_context);

        void fillInitContext(InPass::InitContextData *_context);

        void fillRenderContext(uint32_t _imageIndex, InPass::RenderContextData *_context);
    };
}

#endif