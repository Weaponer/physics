#ifndef IN_RENDERER_CULLING_H
#define IN_RENDERER_CULLING_H

#include <MTH/boundOBB.h>

#include "common/phBase.h"

#include "foundation/phEasyStack.h"

#include "rendering/geRenderer.h"
#include "rendering/geCamera.h"

#include "threads/phTask.h"
#include "threads/phThreadsManager.h"

namespace phys::ge
{
    struct RendererCullingData
    {
        mth::boundOBB obb;
        GeRenderer *renderer;
    };

    struct CameraCullingData
    {
        mth::plane clipPlanes[6];
        mth::boundOBB obbCamera;
    };

    struct CullingGroup
    {
        uint32_t countRenderers;
        RendererCullingData *inData;
        bool *outResults;
    };

    class InRendererCulling : public PhBase
    {
    private:
        class CullingTask : public PhTask
        {
        private:
            CullingGroup group;
            CameraCullingData cameraData;

        public:
            CullingTask();

            virtual ~CullingTask();

            void setData(const CullingGroup &_group, const CameraCullingData &_cameraData);

            virtual void complete(int _instance) override;
        };

    private:
        PhEasyStack<CullingGroup> cullingGroups;
        PhEasyStack<CullingTask *> bufferTasks;
        uint32_t countUseTasks;
        bool isLaunchTasks;

        PhThreadsManager *threadsManager;

    public:
        InRendererCulling();

        virtual ~InRendererCulling() {}

        virtual void release() override;

        void init(PhThreadsManager *_threadsManager);

        void clearGroups();

        void addCullingGroup(CullingGroup _group);

        void runCulling(const GeCamera *_camera);

        void fetch();

        bool isLaunchCulling() const { return isLaunchTasks; }

        uint32_t getCountGroups() const { return cullingGroups.getPos(); }

        CullingGroup getCullingGroup(uint32_t _index) const { return *cullingGroups.getMemory(_index); }

    private:
        CullingTask *getFreeTask();
    };
}

#endif