#ifndef IN_MEMORY_LOADER_H
#define IN_MEMORY_LOADER_H

#include "common/phBase.h"

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/specialCommands/vulkanTransferCommands.h"
#include "gpuCommon/specialCommands/vulkanCommandReceiver.h"
#include "gpuCommon/vulkanFence.h"

#include "rendering/internal/inScene.h"
#include "inMeshManager.h"

namespace phys::ge
{
    class InMemoryLoader : public PhBase
    {
    public:
        vk::VulkanDevice *device;
        vk::VulkanTransferCommands *transferCommands;
        vk::VulkanCommandReceiver *receiver;

    private:
        vk::VulkanFence fenceLoading;

    public:
        InMemoryLoader();

        virtual ~InMemoryLoader() {}

        virtual void release() override;

        void init();

        void loadResourceScene(InScene *_scene, vk::VulkanSemaphore _signalEndLoading, bool *_needWaitLoading);
    };
}

#endif