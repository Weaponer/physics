#ifndef IN_MESH_RENDERER_CULLING_MANAGER_H
#define IN_MESH_RENDERER_CULLING_MANAGER_H

#include "common/phBase.h"

#include "foundation/phBuffer.h"

#include "inRendererCulling.h"
#include "inInstancingGenerator.h"

#include "rendering/geCamera.h"

namespace phys::ge
{
    class InMeshRendererCullingManager : public PhBase
    {
    private:
        PhEasyStack<GeRenderer *> bufferTempRenderers;
        PhEasyStack<RendererCullingData> bufferCullingRenderers;
        PhEasyStack<bool> bufferCullingResults;

        InRendererCulling *rendererCulling;

    public:
        InMeshRendererCullingManager();

        virtual ~InMeshRendererCullingManager() {}

        virtual void release() override;

        void init(PhThreadsManager *_threadsManager);

        void updateCulling(InMeshInstanceRegister *_meshInstanceRegister, const GeCamera *_camera);

        void updateInstancingGenerator(InInstancingGenerator *_generator, vk::VulkanTransferCommands *_transferCommands);
    };
}

#endif