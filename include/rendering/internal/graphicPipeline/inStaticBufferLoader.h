#ifndef IN_STATIC_BUFFER_LOADER_H
#define IN_STATIC_BUFFER_LOADER_H

#include "common/phBase.h"

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/vulkanFence.h"
#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/specialCommands/vulkanCommandReceiver.h"
#include "gpuCommon/specialCommands/vulkanTransferCommands.h"

#include "foundation/phEasyStack.h"

namespace phys::ge
{
    class InStaticBufferLoader : public PhBase
    {
    private:
        struct SendBuffer
        {
            vk::VulkanBuffer *buffer;
            const char *data;
            int sizeData;
        };

    private:
        vk::VulkanDevice *device;
        vk::VulkanBufferAllocator *bufferAllocator;
        uint32_t mainFamilyIndex;

        PhEasyStack<SendBuffer> sendsBuffer;
        vk::VulkanBuffer *stagingBuffer;

        vk::VulkanFence fence;

    public:
        InStaticBufferLoader(vk::VulkanDevice *_device, vk::VulkanBufferAllocator *_bufferAllocator, uint32_t _mainFamilyIndex);

        InStaticBufferLoader(InStaticBufferLoader &) = delete;

        virtual ~InStaticBufferLoader() {}

        virtual void release() override;

        void beginSend();

        void addBuffer(vk::VulkanBuffer *_staticBuffer, const char *_data, uint32_t _sizeData);

        void endSend(vk::VulkanCommandReceiver *_receiver, vk::VulkanQueue *_transferQueue);

    private:
        void sendRange(uint _start, uint _count, vk::VulkanCommandReceiver *_receiver, vk::VulkanQueue *_transferQueue);

        uint32_t getMaxSizeBuffer();

        void ensureStagingBuffer(uint32_t _size);
    };
}

#endif