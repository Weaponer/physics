#ifndef IN_MESH_INSTANCE_REGISTER_H
#define IN_MESH_INSTANCE_REGISTER_H

#include "common/phBase.h"

#include "foundation/phTableContainer.h"
#include "foundation/phMemoryAllocator.h"

#include "inMesh.h"

#include "inMeshRenderer.h"

namespace phys::ge
{
    template <class T>
    class InInstanceGroup : public PhBase
    {
    public:
        PhTableContainer<T *> instances;

        InInstanceGroup() : PhBase(), instances()
        {
            T *def = NULL;
            instances.setUseDefault(true);
            instances.setDefault(&def);
        }

        virtual ~InInstanceGroup() {}

        virtual void release() override { instances.release(); }
    };

    class InMeshInstanceRegister : public PhBase
    {
    public:
        struct MeshInstanceGroup
        {
            InMesh *mesh;
            InInstanceGroup<InMeshRenderer> *group;
        };

    private:
        PhTableContainer<MeshInstanceGroup> instanceGroups;

    public:
        InMeshInstanceRegister() : PhBase(), instanceGroups()
        {
            MeshInstanceGroup group{};
            instanceGroups.setUseDefault(true);
            instanceGroups.setDefault(&group);
        }

        virtual ~InMeshInstanceRegister() {}

        virtual void release() override;

    public:
        uint registerRenderer(InMeshRenderer *_renderer, InMesh *_mesh);

        void unregisterRenderer(InMesh *_mesh, uint _index);

        uint32_t getSizeTableInstanceGroups() const { return instanceGroups.getPosBuffer(); }

        const MeshInstanceGroup *getTableMeshInstanceGroups() const { return instanceGroups.getMemory(0); }
    };
}

#endif