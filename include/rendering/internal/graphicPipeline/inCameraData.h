#ifndef IN_CAMERA_DATA_H
#define IN_CAMERA_DATA_H

#include <MTH/matrix.h>
#include <MTH/vectors.h>

#include "common/phBase.h"

#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/specialCommands/vulkanTransferCommands.h"
#include "gpuCommon/specialCommands/vulkanCommandReceiver.h"
#include "gpuCommon/vulkanQueue.h"

#include "rendering/geCamera.h"

namespace phys::ge
{
    class InCamereData : public PhBase
    {
    public:
        struct CameraData
        {
            mth::mat4 cameraV;
            mth::mat4 cameraInverseV;
            mth::mat4 cameraP;
            mth::mat4 cameraInverseP;
            mth::mat4 cameraVP;
            mth::vec3 cameraPosition;
            float far;
            float near;
        };

    private:
        CameraData data;
        vk::VulkanBufferAllocator *bufferAllocator;
        vk::VulkanBuffer *dataBuffer;

    public:
        InCamereData();

        virtual ~InCamereData() {}

        virtual void release() override;

        void initBuffers(vk::VulkanBufferAllocator *_bufferAllocator, uint32_t _mainFamilyIndex);

        void updateData(GeCamera *_camera, vk::VulkanTransferCommands *_transferCommands);

        vk::VulkanBuffer *getCameraData();
    };
}

#endif