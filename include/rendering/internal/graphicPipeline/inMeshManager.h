#ifndef IN_MESH_MANAGER_H
#define IN_MESH_MANAGER_H

#include "common/phBase.h"
#include "common/phObjectMesh.h"

#include "foundation/phTableContainer.h"
#include "foundation/phEasyStack.h"

#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/specialCommands/vulkanTransferCommands.h"

#include "inMesh.h"

namespace phys::ge
{
    class InMeshManager : public PhBase
    {
    private:
        struct FillMeshDataEvent
        {
            uint sizeData;
            uint countVertex;
            uint countIndex;
            uint countLineVertex;

            uint beginVertex;
            uint beginIndex;
            uint beginLineVertex;
            uint indexWrited;
            void *data;

            InMesh *mesh;
            bool canceled;
        };

    private:
        PhTableContainer<InMesh *> meshes;
        PhEasyStack<FillMeshDataEvent> eventsWriteData;
        vk::VulkanBufferAllocator *bufferAllocator;
        uint32_t mainQueueFamily;

        vk::VulkanBuffer *stagingBuffer;

    public:
        InMeshManager();

        virtual ~InMeshManager() {}

        virtual void release() override;

        void init(vk::VulkanBufferAllocator *_bufferAllocator, uint32_t _mainQueueFamily);

        GeMesh *registerMesh(const PhObjectMesh::Mesh *_mesh);

        void unregisterMesh(GeMesh *_mesh);

        void sendData(vk::VulkanTransferCommands *_commands, bool *_endSend, bool *_wasWrited, bool *_endFreeSpace);

    private:
        void eventIsFinish(FillMeshDataEvent *_event);

        void sendCopyCommands(vk::VulkanTransferCommands *_commands, const FillMeshDataEvent *_event,
                              int _indexStagingBuffer, int _indexData, int _sizeData);

        mth::boundAABB calculateAABB(const PhObjectMesh::Mesh *_mesh);
    };
}

#endif