#ifndef IN_INSTANCE_BUFFERS_H
#define IN_INSTANCE_BUFFERS_H

#include "common/phBase.h"

#include "foundation/phEasyStack.h"

#include "gpuCommon/memory/vulkanBufferAllocator.h"

namespace phys::ge
{
    class InInstanceBuffers : public PhBase
    {
    private:
        uint sizeBuffer;
        vk::VulkanBufferAllocator *bufferAllocator;
        uint32_t mainFamilyIndex;

        PhEasyStack<vk::VulkanBuffer *> allocatedBuffers;
        uint32_t countUseBuffers;

    public:
        InInstanceBuffers(uint _sizeInstance, uint _instancesPerBuffer, vk::VulkanBufferAllocator *_bufferAllocator, uint32_t _mainFamilyIndex)
            : PhBase(),
              sizeBuffer(_sizeInstance * _instancesPerBuffer),
              bufferAllocator(_bufferAllocator),
              mainFamilyIndex(_mainFamilyIndex), allocatedBuffers(), countUseBuffers(0)
        {
        }

        virtual ~InInstanceBuffers() {}

        virtual void release() override
        {
            uint pos = allocatedBuffers.getPos();
            for (uint i = 0; i < pos; i++)
            {
                bufferAllocator->destroyBuffer(*allocatedBuffers.getMemory(i));
            }
            allocatedBuffers.release();
        }

        vk::VulkanBuffer *allocateBuffer()
        {
            if (allocatedBuffers.getPos() <= countUseBuffers)
            {
                vk::VulkanBuffer *buffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferDynamic,
                                                                         sizeBuffer, 1, &mainFamilyIndex, "InstancesBuffer");
                allocatedBuffers.assignmentMemoryAndCopy(&buffer);
                return allocateBuffer();
            }
            vk::VulkanBuffer *buffer = *allocatedBuffers.getMemory(countUseBuffers);
            countUseBuffers++;
            return buffer;
        }

        void resetUsedBuffers()
        {
            countUseBuffers = 0;
        }
    };
}

#endif