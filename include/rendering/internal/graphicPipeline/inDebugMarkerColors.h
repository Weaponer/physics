#ifndef IN_DEBUG_MARKER_COLORS_H
#define IN_DEBUG_MARKER_COLORS_H

namespace phys::ge
{
    class InDebugMarkersUtils
    {
    public:
        static float *mainSystemColor()
        {
            static float col[3] = {0.83f, 0.25f, 0.38f};
            return col;
        }

        static float *subGroupColor()
        {
            static float col[3] = {1.0f, 0.63f, 0.42f};
            return col;
        }

        static float *passCallColor()
        {
            static float col[3] = {0.91f, 0.85f, 0.52f};
            return col;
        }

        static float *subPassColor()
        {
            static float col[3] = {0.47f, 0.57f, 0.31f};
            return col;
        }
    };
}

#endif