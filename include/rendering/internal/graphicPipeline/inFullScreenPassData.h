#ifndef IN_FULL_SCREEN_PASS_DATA_H
#define IN_FULL_SCREEN_PASS_DATA_H

#include "common/phBase.h"

#include "rendering/geShaderProvider.h"
#include "rendering/internal/graphicPipeline/inCameraData.h"

#include "gpuCommon/rendering/vulkanVertexAttributeState.h"
#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/specialCommands/vulkanTransferCommands.h"
#include "gpuCommon/specialCommands/vulkanCommandReceiver.h"
#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/shaders/vulkanDescriptorPoolManager.h"

namespace phys::ge
{
    class InFullScreenPassData : public PhBase
    {
    private:
        vk::VulkanVertexAttributeState attributeState;
        vk::VulkanBufferAllocator *bufferAllocator;
        vk::VulkanBuffer *vertexBuffer;
        vk::VulkanBuffer *indexBuffer;
        vk::VulkanDescriptorPoolManager *descriptorPoolManager;        
        vk::ShaderAutoParameters *vertexParams;
        VkDescriptorSet vertexSet;

    public:
        InFullScreenPassData();

        virtual ~InFullScreenPassData() {}

        virtual void release() override;

        void initData(InCamereData *_cameraData, vk::VulkanBufferAllocator *_bufferAllocator, GeShaderProvider *_shaderProvider,
                      vk::VulkanCommandReceiver *_receiver, vk::VulkanQueue *_graphicQueue);

        const vk::VulkanVertexAttributeState *getVertexAttributeState() const { return &attributeState; }

        vk::VulkanBuffer *getVertexBuffer() { return vertexBuffer; }

        vk::VulkanBuffer *getIndexBuffer() { return indexBuffer; }

        VkDescriptorSet getVertexDescriptorSet() { return vertexSet; }
    };
}

#endif