#ifndef IN_INSTANCING_GENERATOR_H
#define IN_INSTANCING_GENERATOR_H

#include "common/phBase.h"

#include "rendering/internal/inScene.h"
#include "rendering/internal/inMesh.h"

#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/specialCommands/vulkanTransferCommands.h"
#include "gpuCommon/specialCommands/vulkanCommandReceiver.h"
#include "gpuCommon/vulkanQueue.h"

#include "inInstanceBuffers.h"

namespace phys::ge
{
    class InInstancingGenerator : public PhBase
    {
    public:
        struct DrawCall
        {
            vk::VulkanBuffer *buffer;
            InMesh *mesh;
            uint32_t offset;
            uint32_t countInstance;
        };

    private:
        vk::VulkanBufferAllocator *bufferAllocator;
        uint32_t mainFamilyIndex;

    private:
        InInstanceBuffers *instanceBuffers;
        PhEasyStack<DrawCall> drawCalls;
        uint32_t sizeInstanceData;

    private:
        vk::VulkanBuffer *currentBufferWrite;
        void *pCurrentWrite;
        uint currentIndexInstance;

    public:
        InInstancingGenerator();

        virtual ~InInstancingGenerator() {}

        virtual void release() override;

        void initBuffers(vk::VulkanBufferAllocator *_bufferAllocator, uint32_t _mainFamilyIndex);

        void cleanLastDrawCalls();

        void beginInstances(vk::VulkanTransferCommands *_transferCommands);

        void addRenderers(uint _count, GeRenderer **_renderers, vk::VulkanTransferCommands *_transferCommands);

        void endInstances(vk::VulkanTransferCommands *_transferCommands);

        uint32_t getCountDrawCalls() const { return drawCalls.getPos(); }

        const DrawCall *getDrawCalls() const { return drawCalls.getMemory(0); }

    private:
        void pushDrawCall(vk::VulkanBuffer *_buffer, InMesh *_mesh, uint32_t _offset, uint32_t _countInstance);

    protected:
        virtual uint32_t getSizeInstanceData() = 0;

        virtual bool pushInstanceData(GeRenderer *_renderer, void *_pData) = 0;

        virtual InMesh *getMesh(GeRenderer *_renderer) = 0;
    };
}

#endif