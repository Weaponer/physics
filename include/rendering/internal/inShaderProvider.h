#ifndef IN_SHADER_PROVIDER_H
#define IN_SHADER_PROVIDER_H

#include "rendering/geShaderProvider.h"

#include "common/phResourceProvider.h"

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/shaders/shader.h"
#include "gpuCommon/shaders/shaderManager.h"
#include "gpuCommon/shaders/vulkanDescriptorPoolManager.h"

namespace phys::ge
{
    class InShaderProvider : public GeShaderProvider
    {
    protected:
        PhResourceProvider *provider;
        vk::VulkanDevice *device;
        vk::ShaderManager *shaderManager;
        vk::VulkanDescriptorPoolManager *poolManager;

    public:
        InShaderProvider(PhResourceProvider *_provider, vk::VulkanDevice *_device);

        virtual ~InShaderProvider() {}

        virtual void release() override;

        virtual vk::ShaderModule getShaderModule(const char *_name) override;

        virtual vk::VulkanDescriptorPoolManager *getVulkanDescriptorPoolManager() override;
    };
}

#endif