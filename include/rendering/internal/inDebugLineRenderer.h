#ifndef IN_DEBUG_LINE_RENDERER_H
#define IN_DEBUG_LINE_RENDERER_H

#include "foundation/phEasyStack.h"

#include "rendering/geDebugLineRenderer.h"

namespace phys::ge
{
    class InDebugLineRenderer : public GeDebugLineRenderer
    {
    public:
        struct DrawLine
        {
            mth::vec3 start;
            mth::vec3 end;
            mth::vec3 color;
            float duration;
        };

    protected:
        uint index;
        PhEasyStack<DrawLine> *currentBuffer;
        PhEasyStack<DrawLine> *nextBuffer;

    public:
        InDebugLineRenderer(uint _index);

        virtual ~InDebugLineRenderer() {}

        virtual void release() override;

        virtual void drawLine(mth::vec3 _begin, mth::vec3 _end, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) override;

        virtual void drawCube(mth::vec3 _pos, mth::vec3 _size, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) override;

        virtual void drawCube(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) override;

        virtual void drawRect(mth::vec3 _pos, mth::vec3 _size, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) override;

        virtual void drawRect(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) override;

        void updateData(float _deltaTime);

        uint getIndex() const { return index; }

        uint getCountData() const { return currentBuffer->getPos(); }

        const DrawLine *getData() const { return currentBuffer->getMemory(0); }

    protected:
        void pushCube(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color, float _duration);

        void pushRect(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color, float _duration);

        void pushData(const mth::vec3 &_start, const mth::vec3 &_end, const mth::col &_color, float _duration);
    };
}

#endif