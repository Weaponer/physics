#ifndef IN_MESH_RENDERER_H
#define IN_MESH_RENDERER_H

#include "rendering/geSelectableRenderer.h"
#include "rendering/geMeshRenderer.h"
#include "inSelectableRenderer.h"
#include "inMesh.h"

namespace phys::ge
{
    class InMeshInstanceRegister;

    class InMeshRenderer : public GeMeshRenderer, public InSelectableRenderer
    {
    protected:
        InMeshInstanceRegister *meshRendererRegister;
        uint indexMeshRendererInstance;

    public:
        InMesh *mesh;
        mth::col color;
        mth::boundSphere globalSphereBound;
        mth::boundOBB globalOBB;

    public:
        InMeshRenderer(InMeshInstanceRegister *_meshRendererRegister)
            : GeSelectableRenderer(GeEntity::GeEntity_MeshRenderer), GeMeshRenderer(), InSelectableRenderer(),
              meshRendererRegister(_meshRendererRegister), indexMeshRendererInstance(0), mesh(NULL), color(0.46f, 0.46f, 0.46f),
              globalSphereBound(), globalOBB() {}

        virtual ~InMeshRenderer() {}

        virtual void release() override;

        virtual void setMesh(GeMesh *_mesh) override;

        virtual GeMesh *getMesh() override { return mesh; }

        virtual void setColor(const mth::col &_color) override { color = _color; }

        virtual const mth::col &getColor() override { return color; }

        virtual mth::boundSphere getGlobalSphereBound() override;

        virtual mth::boundOBB getGlobalOBB() override;

    protected:
        virtual void onRecalculateTRS() override;
    };
}

#endif