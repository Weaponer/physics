#ifndef IN_GE_SCENE_H
#define IN_GE_SCENE_H

#include "rendering/geScene.h"

#include "rendering/geSceneSettings.h"
#include "rendering/internal/inMeshRenderer.h"
#include "rendering/internal/inSceneEntityPicker.h"
#include "rendering/internal/inSkyBoxRenderer.h"
#include "rendering/internal/inFloorRenderer.h"
#include "rendering/internal/inPivotRenderer.h"
#include "rendering/internal/inDebugLineRenderer.h"
#include "graphicPipeline/inMeshManager.h"
#include "graphicPipeline/inMeshInstanceRegister.h"

#include "foundation/phTableContainer.h"

namespace phys::ge
{
    class InScene : public GeScene
    {
    public:
        GeSceneSettings settings;
        InMeshManager *meshManager;
        InMeshInstanceRegister *meshInstanceRegister;

        InSceneEntityPicker *sceneEntityPicker;
        InSkyBoxRenderer *skyBoxRenderer;
        InFloorRenderer *floorRenderer;
        PhTableContainer<InPivotRenderer *> *tablePivotRenderers;
        PhTableContainer<InDebugLineRenderer *> *tableDebugLineRenderers;

        GeCamera *camera;

    public:
        InScene(InMeshManager *_meshManager);

        virtual ~InScene() {}

        virtual void release() override;

        virtual GeCamera *createCamera() override;

        virtual void destroyCamera(GeCamera *_geCamera) override;

        virtual GeMeshRenderer *createMeshRenderer() override;

        virtual void destroyMeshRenderer(GeMeshRenderer *_meshRenderer) override;

        virtual GeFloorRenderer *createFloorRenderer() override;

        virtual void destroyFloorRenderer(GeFloorRenderer *_floorRendere) override;

        virtual GeFloorRenderer *getFloorRenderer() override;

        virtual GeSceneEntityPicker *getSceneEntityPicker() override;

        virtual GeSkyBoxRenderer *getSkyBoxRenderer() override;

        virtual GeMesh *createMesh(const PhObjectMesh::Mesh *_mesh) override;

        virtual void destroyMesh(GeMesh *_mesh) override;

        virtual GePivotRenderer *createPivotRenderer() override;

        virtual void destroyPivotRenderer(GePivotRenderer *) override;

        virtual GeDebugLineRenderer *createDebugLineRenderer() override;

        virtual void destroyDebugLineRenderer(GeDebugLineRenderer *_debugLineRenderer) override;

        virtual void updateScene(float _deltaTime) override;

        virtual void setActiveCamera(GeCamera *_camera) override { camera = _camera; }

        virtual GeCamera *getActiveCamera() override { return camera; }

        virtual void setSettings(const GeSceneSettings &_settings) override { settings = _settings; }

        virtual const GeSceneSettings &getSettings() const override { return settings; };
    };
}

#endif