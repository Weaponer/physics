#ifndef IN_SELECTABLE_RENDERER_H
#define IN_SELECTABLE_RENDERER_H

#include "rendering/geSelectableRenderer.h"

namespace phys::ge
{
    class InSelectableRenderer : public virtual GeSelectableRenderer
    {
    public:
        mth::col colorHighlightLine;
        bool selected;
        uint indexScenePicker;

        InSelectableRenderer() : colorHighlightLine(0.9f, 0.43f, 0.2f), selected(false), indexScenePicker(0) {}

        virtual ~InSelectableRenderer() {}

        virtual void release() override {}

        virtual void setColorHighlightLine(const mth::col &_color) override { colorHighlightLine = _color; }

        virtual const mth::col &getColorHighlightLine() const override { return colorHighlightLine; }

        virtual void setSelected(bool _selected) override { selected = _selected; };

        virtual bool getSelected() const override { return selected; };

        virtual uint getIndexScenePicker() const override { return indexScenePicker; };

        virtual void setIndexScenePicker(uint _index) override { indexScenePicker = _index; }
    };
}

#endif