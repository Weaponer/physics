#ifndef FLOOR_RENDER_TEMPLATE_H
#define FLOOR_RENDER_TEMPLATE_H

#include "renderTemplate.h"

namespace phys::ge
{
    class FloorRenderTemplate : public RenderTemplate
    {
    private:
        struct
        {
            mth::mat4 floorTRS;
            mth::mat4 floorInverseTRS;
            mth::vec3 floorColor;
        } data;

    private:
        vk::ShaderModule vertexModul;
        vk::ShaderModule fragmentModul;
        vk::VulkanPipelineLayout layout;
        vk::VulkanGraphicPipeline *graphicPipeline;
        vk::ShaderAutoParameters *vertexParameters;
        vk::ShaderAutoParameters *fragmentParameters;
        vk::VulkanBuffer *floorDataUBO;

    public:
        FloorRenderTemplate();

        FloorRenderTemplate(FloorRenderTemplate &_render) = delete;

        virtual ~FloorRenderTemplate() {}

        virtual void release() override;

        virtual void init(const InPass::InitContextData *_context, const InitRenderTemplate *_contextTemplate, vk::VulkanQueue *_graphicQueue) override;

        virtual void beginRender(InScene *_scene, const InPass::RenderContextData *_context,
                                 const BeginRenderTemplate *_beginRender, vk::VulkanGraphicCommands *_graphicCommands) override;

        virtual void endRender(vk::VulkanGraphicCommands *graphicCommands) override;

        virtual void cleanup(const InPass::CleanupContextData *_context) override;

        void draw(vk::VulkanGraphicCommands *_graphicCommands);

        virtual vk::VulkanPipelineLayout getLayout() override { return layout; };

        virtual vk::ShaderAutoParameters *getFragmentParameters() override { return fragmentParameters; };

        virtual vk::ShaderAutoParameters *getVertexParameters() override { return vertexParameters; };

    private:
        void updateData(InFloorRenderer *_floor, vk::VulkanGraphicCommands *graphicCommands);
    };
}

#endif