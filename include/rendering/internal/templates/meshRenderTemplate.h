#ifndef MESH_RENDER_TEMPLATE_H
#define MESH_RENDER_TEMPLATE_H

#include "renderTemplate.h"

namespace phys::ge
{
    class MeshRenderTemplate : public RenderTemplate
    {
    public:
        struct MeshTemplateInitData
        {
            bool isWireframe;
        };

    private:
        vk::ShaderModule vertexModul;
        vk::ShaderModule fragmentModul;
        vk::VulkanPipelineLayout layout;
        vk::VulkanGraphicPipeline *graphicPipeline;
        vk::ShaderAutoParameters *vertexParameters;

    public:
        MeshRenderTemplate();

        MeshRenderTemplate(MeshRenderTemplate &_render) = delete;

        virtual ~MeshRenderTemplate() {}

        virtual void release() override;

        virtual void init(const InPass::InitContextData *_context, const InitRenderTemplate *_contextTemplate, vk::VulkanQueue *_graphicQueue) override;

        virtual void beginRender(InScene *_scene, const InPass::RenderContextData *_context,
                                 const BeginRenderTemplate *_beginRender, vk::VulkanGraphicCommands *graphicCommands) override;

        virtual void endRender(vk::VulkanGraphicCommands *graphicCommands) override;

        virtual void cleanup(const InPass::CleanupContextData *_context) override;

        void draw(DrawRenderTemplate *_settings, vk::VulkanGraphicCommands *graphicCommands);

        void drawIndexed(DrawRenderTemplate *_settings, vk::VulkanGraphicCommands *graphicCommands);

        virtual vk::VulkanPipelineLayout getLayout() override { return layout; };

        virtual vk::ShaderAutoParameters *getVertexParameters() override { return vertexParameters; };
    };
}

#endif