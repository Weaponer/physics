#ifndef RENDER_TEMPLATE_H
#define RENDER_TEMPLATE_H

#include "common/phBase.h"

#include "passes/inPass.h"

#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/rendering/vulkanFramebuffer.h"
#include "gpuCommon/rendering/vulkanRenderPass.h"
#include "gpuCommon/rendering/vulkanBlendColorState.h"
#include "gpuCommon/rendering/vulkanVertexAttributeState.h"

#include "gpuCommon/specialCommands/shaderAutoParameters.h"

namespace phys::ge
{
    class RenderTemplate : public PhBase
    {
    public:
        struct InitRenderTemplate
        {
            const char *vertexShader;
            const char *fragmentShader;
            vk::VulkanRenderPass renderPass;
            vk::VulkanBlendColorState *blendColorState;
            vk::VulkanVertexAttributeState *vertexAttributeState;
            void *customData;
        };

        struct BeginRenderTemplate
        {
            vk::VulkanRenderPass renderPass;
            vk::VulkanFramebuffer framebuffer;
        };

        struct DrawRenderTemplate
        {
            uint32_t countVertex;
            uint32_t firstVertex;
            uint32_t countInstance;
            uint32_t firstInstance;
            uint32_t offsetInstanceBuffer;
            uint32_t countIndex;
            uint32_t firstIndex;
            VkIndexType indexType;
            vk::VulkanBuffer *vertexBuffer;
            vk::VulkanBuffer *instanceBuffer;
            vk::VulkanBuffer *indexBuffer;
        };

    public:
        RenderTemplate() : PhBase() {}

        RenderTemplate(RenderTemplate &_render) = delete;

        virtual ~RenderTemplate() {}

        virtual void init(const InPass::InitContextData *_context, const InitRenderTemplate *_contextTemplate, vk::VulkanQueue *_graphicQueue) = 0;

        virtual void beginRender(InScene *_scene, const InPass::RenderContextData *_context,
                                 const BeginRenderTemplate *_beginRender, vk::VulkanGraphicCommands *graphicCommands) = 0;

        virtual void endRender(vk::VulkanGraphicCommands *graphicCommands) = 0;

        virtual void cleanup(const InPass::CleanupContextData *_context) = 0;

        virtual vk::VulkanPipelineLayout getLayout() { return vk::VulkanPipelineLayout(); }

        virtual vk::ShaderAutoParameters *getVertexParameters() { return NULL; };

        virtual vk::ShaderAutoParameters *getFragmentParameters() { return NULL; };
    };
}

#endif