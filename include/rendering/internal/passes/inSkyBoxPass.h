#ifndef IN_SKY_BOX_PASS_H
#define IN_SKY_BOX_PASS_H

#include "inPass.h"

namespace phys::ge
{
    class InSkyBoxPass : public InPass
    {
    private:
    public:
        InSkyBoxPass();

        virtual ~InSkyBoxPass() {}

        virtual void release() override;

        virtual void render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context) override;
    };
}

#endif
