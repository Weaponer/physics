#ifndef IN_MESH_RENDERER_PASS_H
#define IN_MESH_RENDERER_PASS_H

#include "inPass.h"

#include "gpuCommon/rendering/graphicPipelinesManager.h"
#include "gpuCommon/shaders/vulkanPipelineLayout.h"
#include "gpuCommon/rendering/vulkanGraphicPipeline.h"
#include "gpuCommon/specialCommands/shaderAutoParameters.h"

#include "rendering/internal/graphicPipeline/inInstancingGenerator.h"
#include "rendering/internal/templates/meshRenderTemplate.h"

namespace phys::ge
{
    class InMeshRendererPass : public InPass
    {
    private:
        class InstancingMainPass : public InInstancingGenerator
        {
        public:
            struct Data
            {
                mth::mat4 TRS;
                mth::col color;
            };

        public:
            InstancingMainPass();

            virtual ~InstancingMainPass() {}

            virtual void release() override;

        protected:
            virtual uint32_t getSizeInstanceData() override;

            virtual bool pushInstanceData(GeRenderer *_renderer, void *_pData) override;

            virtual InMesh *getMesh(GeRenderer *_renderer) override;
        };

    private:
        InstancingMainPass *instancingMainPass;

        MeshRenderTemplate *meshRender;
        MeshRenderTemplate *wireframeRender;

    public:
        InMeshRendererPass();

        virtual ~InMeshRendererPass() {}

        virtual void release() override;

        virtual void init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue) override;

        virtual void render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context) override;

        virtual void cleanup(const CleanupContextData *_context) override;
    };
}

#endif