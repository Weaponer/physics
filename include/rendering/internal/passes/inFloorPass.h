#ifndef IN_FLOOR_PASS_H
#define IN_FLOOR_PASS_H

#include <vulkan/vulkan.h>

#include "inPass.h"

#include "gpuCommon/rendering/graphicPipelinesManager.h"
#include "gpuCommon/shaders/vulkanPipelineLayout.h"
#include "gpuCommon/rendering/vulkanGraphicPipeline.h"
#include "gpuCommon/specialCommands/shaderAutoParameters.h"

#include "rendering/internal/templates/floorRenderTemplate.h"

namespace phys::ge
{
    class InFloorPass : public InPass
    {
    private:
        FloorRenderTemplate *floorRender;

    public:
        InFloorPass();

        virtual ~InFloorPass() {}

        virtual void release() override;

        virtual void init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue) override;

        virtual void render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context) override;

        virtual void cleanup(const CleanupContextData *_context) override;
    };
}

#endif
