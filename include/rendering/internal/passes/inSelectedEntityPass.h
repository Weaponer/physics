#ifndef IN_SELECTED_ENTITY_PASS_H
#define IN_SELECTED_ENTITY_PASS_H

#include <vulkan/vulkan.h>

#include "inPass.h"

#include "gpuCommon/rendering/graphicPipelinesManager.h"
#include "gpuCommon/shaders/vulkanPipelineLayout.h"
#include "gpuCommon/rendering/vulkanGraphicPipeline.h"
#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/rendering/vulkanRenderPass.h"
#include "gpuCommon/rendering/vulkanFramebuffer.h"
#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/memory/vulkanImage.h"

#include "rendering/internal/graphicPipeline/inInstancingGenerator.h"
#include "rendering/internal/templates/floorRenderTemplate.h"
#include "rendering/internal/templates/meshRenderTemplate.h"

namespace phys::ge
{
    class InSelectedEntityPass : public InPass
    {
    private:
        class InstancingSelectedEntityPass : public InInstancingGenerator
        {
        public:
            struct Data
            {
                mth::mat4 TRS;
                mth::vec3 col;
            };

        public:
            InstancingSelectedEntityPass();

            virtual ~InstancingSelectedEntityPass() {}

            virtual void release() override;

        protected:
            virtual uint32_t getSizeInstanceData() override;

            virtual bool pushInstanceData(GeRenderer *_renderer, void *_pData) override;

            virtual InMesh *getMesh(GeRenderer *_renderer) override;
        };

    private:
        struct BlurData
        {
            int width;
            int height;
            int iterations;
        };

        struct PushConstants
        {
            int iteration;
            bool finalPass;
        };

    private:
        BlurData uboData;

        InstancingSelectedEntityPass *instancingSelectedEntityPass;

        vk::VulkanImage *blurTargets[2];
        vk::VulkanImageView blurTargetViews[2];
        vk::VulkanRenderPass renderPass;
        vk::VulkanFramebuffer blurFramebuffer[2];

        FloorRenderTemplate *floorRenderTemplate;
        MeshRenderTemplate *meshRenderTemplate;

        vk::VulkanBuffer *ubo;
        vk::VulkanSampler *sampler;

        vk::ShaderModule vertexModul;
        vk::ShaderModule fragmentModul;
        vk::VulkanPipelineLayout layout;
        vk::VulkanGraphicPipeline *blurPipeline;
        vk::VulkanGraphicPipeline *blurFinalPipeline;
        vk::ShaderAutoParameters *fragmentParameters;
        VkDescriptorSet fragmentSets[2];

    public:
        InSelectedEntityPass();

        virtual ~InSelectedEntityPass() {}

        virtual void release() override;

        virtual void init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue) override;

        virtual void render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context) override;

        virtual void cleanup(const CleanupContextData *_context) override;

    private:
        void initTargets(const InitContextData *_context, vk::VulkanQueue *_graphicQueue);

        void initTemplates(const InitContextData *_context, vk::VulkanQueue *_graphicQueue);

        void initBlurPass(const InitContextData *_context, vk::VulkanQueue *_graphicQueue);

    private:
        bool drawScene(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context);

        void generateBlur(vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context);

        void finalCombine(vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context);

        void clearImage(int _index, vk::VulkanGraphicCommands *graphicCommands);

        void prepareImageToDraw(int _index, vk::VulkanGraphicCommands *graphicCommands);

        void prepareImageToSampled(int _index, vk::VulkanGraphicCommands *graphicCommands);
    };
}

#endif