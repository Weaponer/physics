#ifndef IN_PIVOT_RENDERER_PASS_H
#define IN_PIVOT_RENDERER_PASS_H

#include <MTH/matrix.h>
#include <MTH/color.h>

#include <vulkan/vulkan.h>

#include "inPass.h"

#include "gpuCommon/rendering/graphicPipelinesManager.h"
#include "gpuCommon/shaders/vulkanPipelineLayout.h"
#include "gpuCommon/rendering/vulkanGraphicPipeline.h"
#include "gpuCommon/specialCommands/shaderAutoParameters.h"

#include "rendering/geCamera.h"
#include "rendering/internal/inPivotRenderer.h"

#include "rendering/internal/graphicPipeline/inInstanceBuffers.h"

namespace phys::ge
{
    class InPivotRendererPass : public InPass
    {
    private:
        struct VertexData
        {
            mth::vec3 position;
        };

        struct InstanceData
        {
            mth::mat4 TRS;
            mth::col color;
        };

    private:
        vk::ShaderModule vertexModul;
        vk::ShaderModule fragmentModul;
        vk::VulkanPipelineLayout layout;
        vk::VulkanGraphicPipeline *lineGraphicPipeline;
        vk::VulkanGraphicPipeline *opaqueGraphicPipeline;
        vk::VulkanGraphicPipeline *transparentGraphicPipeline;

        vk::ShaderAutoParameters *vertexParams;
        InInstanceBuffers *instanceBuffers;

        vk::VulkanBuffer *meshCube;
        vk::VulkanBuffer *meshCubeIndexes;
        vk::VulkanBuffer *meshConus;
        vk::VulkanBuffer *meshConusIndexes;
        vk::VulkanBuffer *meshCylinder;
        vk::VulkanBuffer *meshCylinderIndexes;
        vk::VulkanBuffer *meshRect;
        vk::VulkanBuffer *meshLineRect;
        vk::VulkanBuffer *meshLine;
        vk::VulkanBuffer *meshLineCircle;

    public:
        InPivotRendererPass();

        virtual ~InPivotRendererPass() {}

        virtual void release() override;

        virtual void init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue) override;

        virtual void render(InScene *_scene, vk::VulkanGraphicCommands *_graphicCommands, const RenderContextData *_context) override;

        virtual void cleanup(const CleanupContextData *_context) override;

    private:
        void initBuffers(const InitContextData *_context, vk::VulkanQueue *_graphicQueue);

        void drawPivot(GeCamera *_camera, InPivotRenderer *_pivot, vk::VulkanGraphicCommands *_graphicCommands, const RenderContextData *_context);

        void fillColorData(InstanceData *_pWrite, InPivotRenderer *_pivot);

        void fillMatrixesMoveScaleData(InstanceData *_pWrite, InPivotRenderer *_pivot, float _scale);

        void fillMatrixesRotateData(InstanceData *_pWrite, GeCamera *_camera, InPivotRenderer *_pivot, float _scale);
    };
}

#endif