#ifndef IN_SCENE_PICKER_PASS_H
#define IN_SCENE_PICKER_PASS_H

#include "inPass.h"

#include "gpuCommon/rendering/graphicPipelinesManager.h"
#include "gpuCommon/shaders/vulkanPipelineLayout.h"
#include "gpuCommon/rendering/vulkanGraphicPipeline.h"
#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/rendering/vulkanRenderPass.h"
#include "gpuCommon/rendering/vulkanFramebuffer.h"
#include "gpuCommon/memory/vulkanBuffer.h"
#include "gpuCommon/memory/vulkanImage.h"

#include "rendering/internal/graphicPipeline/inInstancingGenerator.h"
#include "rendering/internal/templates/floorRenderTemplate.h"
#include "rendering/internal/templates/meshRenderTemplate.h"

namespace phys::ge
{
    class InScenePickerPass : public InPass
    {
    private:
        class InstancingScenePickerPass : public InInstancingGenerator
        {
        public:
            struct Data
            {
                mth::mat4 TRS;
                uint indexObject;
            };

        public:
            InstancingScenePickerPass();

            virtual ~InstancingScenePickerPass() {}

            virtual void release() override;

        protected:
            virtual uint32_t getSizeInstanceData() override;

            virtual bool pushInstanceData(GeRenderer *_renderer, void *_pData) override;

            virtual InMesh *getMesh(GeRenderer *_renderer) override;
        };

    private:
        InstancingScenePickerPass *instancingScenePickerPass;

        vk::VulkanImage *indexesTarget;
        vk::VulkanImageView indexesTargetView;
        vk::VulkanBuffer *resultBuffer;
        vk::VulkanRenderPass renderPass;
        vk::VulkanFramebuffer framebuffer;

        FloorRenderTemplate *floorRenderTemplate;
        MeshRenderTemplate *meshRenderTemplate;

        bool firstUpdate;
        
    public:
        InScenePickerPass();

        virtual ~InScenePickerPass() {}

        virtual void release() override;

        virtual void init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue) override;

        virtual void render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context) override;

        virtual void cleanup(const CleanupContextData *_context) override;
    };
}

#endif