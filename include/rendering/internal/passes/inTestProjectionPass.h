#ifndef IN_TEST_PROJECTION_PASS
#define IN_TEST_PROJECTION_PASS

#include <vulkan/vulkan.h>

#include "inPass.h"

#include "gpuCommon/rendering/graphicPipelinesManager.h"
#include "gpuCommon/shaders/vulkanPipelineLayout.h"
#include "gpuCommon/rendering/vulkanGraphicPipeline.h"
#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/memory/vulkanImage.h"
#include "gpuCommon/memory/vulkanImageAllocator.h"

namespace phys::ge
{
    class InTestProjectionPass : public InPass
    {
    private:
        vk::ShaderModule vertexModul;
        vk::ShaderModule fragmentModul;
        vk::VulkanPipelineLayout layout;
        vk::VulkanGraphicPipeline *graphicPipeline;
        vk::ShaderAutoParameters *vertexParameters;
        vk::ShaderAutoParameters *fragmentParameters;
        vk::VulkanImage *tempImage;
        vk::VulkanImageView tempImageView;
        vk::VulkanSampler *sampler;

    public:
        InTestProjectionPass();

        virtual ~InTestProjectionPass() {}

        virtual void release() override;

        virtual void init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue) override;

        virtual void render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context) override;

        virtual void cleanup(const CleanupContextData *_context) override;

    };
}

#endif