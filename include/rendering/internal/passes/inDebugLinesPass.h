#ifndef IN_DEBUG_LINES_PASS_H
#define IN_DEBUG_LINES_PASS_H

#include <vulkan/vulkan.h>

#include "inPass.h"

#include "gpuCommon/rendering/graphicPipelinesManager.h"
#include "gpuCommon/shaders/vulkanPipelineLayout.h"
#include "gpuCommon/rendering/vulkanGraphicPipeline.h"
#include "gpuCommon/specialCommands/shaderAutoParameters.h"

namespace phys::ge
{
    class InDebugLinesPass : public InPass
    {
    private:
        struct LineVertex
        {
            mth::vec3 point;
            mth::vec3 color;
        };

    private:
        vk::ShaderModule vertexModul;
        vk::ShaderModule fragmentModul;
        vk::VulkanPipelineLayout layout;
        vk::VulkanGraphicPipeline *graphicOverDepthPipeline;
        vk::VulkanGraphicPipeline *graphicUnderDepthPipeline;
        vk::ShaderAutoParameters *vertexParameters;
        vk::VulkanBuffer *vertexBuffer;
        
    public:
        InDebugLinesPass();

        virtual ~InDebugLinesPass() {}

        virtual void release() override;

        virtual void init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue) override;

        virtual void render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context) override;

        virtual void cleanup(const CleanupContextData *_context) override;

    private:
        uint32_t updateData(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context);
    };
}

#endif
