#ifndef IN_PASS_H
#define IN_PASS_H

#include "common/phBase.h"

#include "rendering/internal/inScene.h"

#include "gpuCommon/specialCommands/vulkanGraphicCommands.h"
#include "gpuCommon/rendering/vulkanRenderPass.h"
#include "gpuCommon/rendering/vulkanFramebuffer.h"
#include "gpuCommon/memory/vulkanImage.h"

#include "gpuCommon/memory/vulkanBufferAllocator.h"
#include "gpuCommon/memory/vulkanImageAllocator.h"
#include "gpuCommon/rendering/renderPassesManager.h"
#include "gpuCommon/rendering/graphicPipelinesManager.h"

#include "gpuCommon/vulkanSwapchain.h"

#include "rendering/geShaderProvider.h"
#include "rendering/internal/graphicPipeline/inCameraData.h"
#include "rendering/internal/graphicPipeline/inFullScreenPassData.h"
#include "rendering/internal/graphicPipeline/inMeshRendererCullingManager.h"

#include "rendering/internal/graphicPipeline/inDebugMarkerColors.h"

namespace phys::ge
{
    class InPass : public PhBase
    {
    public:
        struct CleanupContextData
        {
            uint32_t mainFamilyIndex;

            vk::VulkanDevice *device;
            vk::VulkanBufferAllocator *bufferAllocator;
            vk::VulkanImageAllocator *imageAllocator;
            vk::VulkanCommandReceiver *commandReceiver;
            vk::GraphicPipelinesManager *graphicPipelinesManager;
            vk::VulkanDescriptorPoolManager *descriptorPoolManager;
            GeShaderProvider *shaderProvider;
        };

        struct InitContextData : CleanupContextData
        {
            vk::VulkanSwapchain *swapchain;
            InFullScreenPassData *fullScreenPassData;
            InCamereData *cameraData;
            vk::RenderPassesManager *renderPassesManager;
            InMeshRendererCullingManager *meshRendererCullingManager;

            vk::VulkanRenderPass renderPass;
            vk::VulkanImage *depthBuffer;
            vk::VulkanImageView depthBufferView;
        };

        struct RenderContextData : InitContextData
        {
            vk::VulkanFramebuffer framebuffer;
            vk::VulkanImageView imageBufferView;
            vk::VulkanImage *imageBuffer;
        };

    public:
        InPass() : PhBase() {}

        virtual ~InPass() {}

        virtual void init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue) {}

        virtual void render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context) = 0;

        virtual void cleanup(const CleanupContextData *_context) {}
    };
}

#endif