#ifndef IN_SCENE_ENTITY_PICKER_H
#define IN_SCENE_ENTITY_PICKER_H

#include "rendering/geSceneEntityPicker.h"

namespace phys::ge
{
    class InSceneEntityPicker : public GeSceneEntityPicker
    {
    public:
        mth::vec2 position;
        uint lastIndex;

    public:
        InSceneEntityPicker() : GeSceneEntityPicker(), position(), lastIndex(0) {}

        virtual ~InSceneEntityPicker() {}

        virtual void release() override {}

        virtual const mth::vec2 &getCheckCoordinates() const override { return position; }

        virtual void setCheckCoordinates(const mth::vec2 &_pos) override { position = _pos; }

        virtual uint getLastSelectedObject() const override { return lastIndex; }
    };
}

#endif