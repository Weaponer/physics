#ifndef IN_MESH_H
#define IN_MESH_H

#include <vulkan/vulkan.h>

#include "rendering/geMesh.h"

#include "gpuCommon/memory/vulkanBuffer.h"

namespace phys::ge
{
    class InMesh : public GeMesh
    {
    public:
        uint indexMesh;
        uint indexFillMeshEvent;
        bool finishLoad;

        uint countVertex;
        uint countIndex;
        uint countLineVertex;
        mth::boundAABB bound;

        vk::VulkanBuffer *vertexBuffer;
        vk::VulkanBuffer *indexBuffer;
        vk::VulkanBuffer *lineVertexBuffer;
        VkIndexType indexType;

        InMesh() : GeMesh(), indexMesh(0), indexFillMeshEvent(0),
                   finishLoad(false), countVertex(0), countIndex(0), countLineVertex(0), bound(),
                   vertexBuffer(NULL), indexBuffer(NULL), lineVertexBuffer(NULL) {}

        virtual ~InMesh() {}

        virtual void release() override { GeMesh::release(); }

        virtual const mth::boundAABB &getBoundAABB() override { return bound; }

        virtual vk::VulkanBuffer *getVertexBuffer() override { return vertexBuffer; }

        virtual vk::VulkanBuffer *getIndexBuffer() override { return indexBuffer; }

        virtual vk::VulkanBuffer *getLineVertexBuffer() override { return lineVertexBuffer; }

        virtual uint getCountVertex() override { return countVertex; }

        virtual uint getCountIndex() override { return countIndex; }

        virtual uint getCountLineVertex() override { return countLineVertex; }

        virtual VkIndexType getIndexType() override { return indexType; }

        bool isFinishLoad() const { return finishLoad; }
    };
}

#endif