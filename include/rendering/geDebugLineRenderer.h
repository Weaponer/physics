#ifndef GE_DEBUG_LINE_RENDERER_H
#define GE_DEBUG_LINE_RENDERER_H

#include <MTH/vectors.h>
#include <MTH/color.h>

#include "geRenderer.h"

namespace phys::ge
{
    class GeDebugLineRenderer : public PhBase
    {
    public:
        GeDebugLineRenderer() : PhBase() {}

        virtual ~GeDebugLineRenderer() {}

        virtual void drawLine(mth::vec3 _begin, mth::vec3 _end, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) = 0;

        virtual void drawCube(mth::vec3 _pos, mth::vec3 _size, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) = 0;

        virtual void drawCube(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) = 0;

        virtual void drawRect(mth::vec3 _pos, mth::vec3 _size, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) = 0;

        virtual void drawRect(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color = mth::col(1, 1, 1), float _duration = 0.0f) = 0;
    };
}

#endif