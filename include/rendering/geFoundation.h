#ifndef GE_FOUNDATION_H
#define GE_FOUNDATION_H

#include "common/phBase.h"

#include "threads/phThreadsManager.h"

#include "geScene.h"
#include "geShaderProvider.h"

#include "internal/inSceneRenderer.h"
#include "internal/graphicPipeline/inGraphicPipeline.h"

#include "window/window.h"

#include "gpuCommon/gpuFoundation.h"
#include "gpuCommon/vulkanQueue.h"

namespace phys::ge
{
    class GeFoundation : public PhBase
    {
    protected:
        PhThreadsManager *threadsManager;
        GPUFoundation *gpuFoundation;
        GeShaderProvider *shaderProvider;
        Window *window;
        vk::VulkanQueue *renderingComputeQueue;

        InSceneRenderer *inSceneRenderer;
        InGraphicPipeline *graphicPipeline;

        GeFoundation() : PhBase() {}

        virtual ~GeFoundation() {}

        virtual void release() override;

    public:
        static GeFoundation *createGeFoundation(PhThreadsManager *_threadsManager, GPUFoundation *_gpuFoundation, GeShaderProvider *_shaderProvider,
                                                Window *_window, vk::VulkanQueue *_renderingComputeQueue);

        static void destroyGeFoundation(GeFoundation *_geFoundation);

        GeScene *createScene();

        void destroy(GeScene *_scene);

        GeSceneRenderer *getSceneRenderer() { return inSceneRenderer; }
    };
}

#endif