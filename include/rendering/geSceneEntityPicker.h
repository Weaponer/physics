#ifndef GE_SCENE_ENTITY_PICKER_H
#define GE_SCENE_ENTITY_PICKER_H

#include <MTH/vectors.h>

#include "common/phBase.h"

namespace phys::ge
{
    class GeSceneEntityPicker : public PhBase
    {
    public:
        GeSceneEntityPicker() : PhBase() {}

        virtual ~GeSceneEntityPicker() {}

        /* [0.0f, 1.0f] - range x and y, left up edge*/
        virtual const mth::vec2 &getCheckCoordinates() const = 0;

        /* [0.0f, 1.0f] - range x and y, left up edge*/
        virtual void setCheckCoordinates(const mth::vec2 &_pos) = 0;

        virtual uint getLastSelectedObject() const = 0;
    };
}

#endif