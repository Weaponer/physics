#ifndef GE_SCENE_H
#define GE_SCENE_H

#include "common/phBase.h"
#include "common/phObjectMesh.h"
#include "geMesh.h"
#include "geRenderer.h"

#include "geSceneEntityPicker.h"
#include "geCamera.h"
#include "geMeshRenderer.h"
#include "geFloorRenderer.h"
#include "geSkyBoxRenderer.h"
#include "geDebugLineRenderer.h"
#include "gePivotRenderer.h"

#include "geSceneSettings.h"

namespace phys::ge
{
    class GeScene : public PhBase
    {
    protected:
        GeScene() : PhBase() {}

        virtual ~GeScene() {}

    public:
        virtual GeCamera *createCamera() = 0;

        virtual void destroyCamera(GeCamera *_geCamera) = 0;

        virtual GeMeshRenderer *createMeshRenderer() = 0;

        virtual void destroyMeshRenderer(GeMeshRenderer *_meshRenderer) = 0;

        virtual GeFloorRenderer *createFloorRenderer() = 0;

        virtual void destroyFloorRenderer(GeFloorRenderer *_floorRendere) = 0;

        virtual GeFloorRenderer *getFloorRenderer() = 0;

        virtual GeSceneEntityPicker *getSceneEntityPicker() = 0;

        virtual GeSkyBoxRenderer *getSkyBoxRenderer() = 0;

        virtual GeMesh *createMesh(const PhObjectMesh::Mesh *_mesh) = 0;

        virtual void destroyMesh(GeMesh *_mesh) = 0;

        virtual GePivotRenderer *createPivotRenderer() = 0;

        virtual void destroyPivotRenderer(GePivotRenderer *) = 0;

        virtual GeDebugLineRenderer *createDebugLineRenderer() = 0;

        virtual void destroyDebugLineRenderer(GeDebugLineRenderer *_debugLineRenderer) = 0;

        virtual void updateScene(float _deltaTime) = 0;

        virtual void setActiveCamera(GeCamera *_camera) = 0;

        virtual GeCamera *getActiveCamera() = 0;

        virtual void setSettings(const GeSceneSettings &_settings) = 0;

        virtual const GeSceneSettings &getSettings() const = 0;
    };
}

#endif