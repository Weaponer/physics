#ifndef WINDOW_H
#define WINDOW_H

#include <SDL2/SDL.h>
#include <string>
#include <list>

#include <MTH/vectors.h>

#include "common/phBase.h"

#include "gpuCommon/vulkanInstance.h"
#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/vulkanSwapchain.h"
#include "windowGUI.h"

namespace phys
{
    class Window : public PhBase
    {
    private:
        std::string title;
        int width;
        int height;
        SDL_Window *window;
        vk::VulkanInstance *instance;
        vk::VulkanDevice *device;
        vk::VulkanQueue *queue;
        VkSurfaceKHR surface;
        vk::VulkanSwapchain swapchain;
        vk::VulkanSemaphore waitBeginRenderingSamephore;
        vk::VulkanSemaphore signalRenderingSamephore;
        uint currentImageIndex;
        std::list<WindowGUI *> GUIs;
        mth::vec2 mousePosDelta;

    public:
        Window(const char *_title, int _width, int _height);

        virtual ~Window() {}

        virtual void release() override;

        int getWidth() const { return width; }

        int getHeight() const { return height; }

        vk::VulkanInstance *getVulkanInstance() const { return instance; }

        vk::VulkanDevice *getVulkanDevice() const { return device; }

        vk::VulkanQueue *getVulkanQueue() const { return queue; }

        VkSurfaceKHR getSurface() { return surface; }

        vk::VulkanSwapchain *getSwapchain() { return &swapchain; }

        vk::VulkanSemaphore getWaitBeginRenderingSemaphore() { return waitBeginRenderingSamephore; }

        vk::VulkanSemaphore getSignalEndRenderingSemaphore() { return signalRenderingSamephore; }

        uint getCurrentImageIndex() const { return currentImageIndex; }

        void connectVulkanInstance(vk::VulkanInstance *_instance);

        void connectDevice(vk::VulkanDevice *_device, vk::VulkanQueue *_queue);

        void connectGUI(WindowGUI *_gui);

        void disconnectGUI(WindowGUI *_gui);

        void getExtensions(uint *_count, const char **_names);

        bool updateEvents();

        bool beginRendering(uint *_imageIndex);

        bool endRendering();

        void showCursor();

        void hideCursor();

        mth::vec2 getDeltaMoveMouse();

        operator SDL_Window *() { return window; }
    };
}

#endif