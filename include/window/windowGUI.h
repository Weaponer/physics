#ifndef WINDIW_GUI_H
#define WINDIW_GUI_H

#include <SDL2/SDL.h>

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/vulkanSemaphore.h"
#include "gpuCommon/vulkanFence.h"

namespace phys
{
    class Window;

    class WindowGUI
    {
    protected:
        Window *window;
        vk::VulkanDevice *device;
        vk::VulkanQueue *queue;

    public:
        WindowGUI();

        virtual ~WindowGUI();

        void init(Window *_window, vk::VulkanDevice *_device, vk::VulkanQueue *_queue);

        void destroy();

        virtual void updateEvent(const SDL_Event *_event) = 0;

        virtual void drawFrame(vk::VulkanSemaphore _waitSemaphore, vk::VulkanSemaphore _signalSemaphore, vk::VulkanFence _fence) = 0;

    protected:
        virtual void onInit(Window *_window, vk::VulkanDevice *_device, vk::VulkanQueue *_queue) = 0;

        virtual void onDestory() = 0;
    };
}

#endif