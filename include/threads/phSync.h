#ifndef PH_SYNC_H
#define PH_SYNC_H

#include <mutex>
#include <condition_variable>
#include <atomic>

namespace phys
{
    class PhSync
    {
    private:
        std::mutex mutex;
        std::condition_variable cv;
        std::atomic_int countListeners;
        std::atomic_bool activeSignal;

    public:
        PhSync();

        PhSync(PhSync &_sync) = delete;

        virtual ~PhSync();

        bool wait();

        void enableSignal();

        void signal();
    };
}

#endif