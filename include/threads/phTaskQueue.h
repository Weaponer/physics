#ifndef PH_TASK_QUEUE_H
#define PH_TASK_QUEUE_H

#include <mutex>
#include <queue>
#include <condition_variable>
#include <atomic>

#include "phTask.h"

namespace phys
{
    class PhTaskQueue
    {
    public:
        struct TaskData
        {
            PhTask *task;
            int countInstances;
            int offsetInstance;
        };

    private:
        std::mutex mData;
        std::condition_variable cv;
        std::atomic_bool isStopQueue;
        std::atomic_int countListeners;
        std::queue<TaskData> tasks;

    public:
        PhTaskQueue();

        PhTaskQueue(PhTaskQueue &_queue) = delete;

        virtual ~PhTaskQueue();

        void pushTask(PhTask *_task, int _countInstance, int _offsetInstance);

        void stopQueue();

        bool waitToTakeTask(TaskData *_outData);

        bool isEmpty();

        bool isSomeoneWaitTask();
    };
}

#endif