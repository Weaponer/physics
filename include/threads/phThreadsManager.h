#ifndef PH_THREADS_MANAGER_H
#define PH_THREADS_MANAGER_H

#include <vector>
#include <mutex>
#include <atomic>
#include <queue>

#include "phSync.h"
#include "phTaskQueue.h"
#include "phThread.h"
#include "phTask.h"

namespace phys
{
    class PhThreadsManager
    {
    private:
        class TaskControllerThread;

        class TaskThread : public PhThread
        {
        private:
            PhTaskQueue taskQueue;
            PhSync eventFinish;

        public:
            TaskThread();

            virtual ~TaskThread();

        protected:
            virtual void onInit() override;

            virtual void onRun() override;

            friend class PhThreadsManager;
        };

    private:
        std::vector<TaskThread *> taskThreads;

    public:
        PhThreadsManager();

        ~PhThreadsManager();

        int countTaskThreads() const { return (int)taskThreads.size(); }

        bool isTaskSystemEnable() const { return taskThreads.size() != 0; }

        void allocateTaskThreads(int _count);

        void clear();

        void addTask(PhTask *_task, int _countInstance = 1);

        void waitTask(PhTask *_task);

        void waitAllTasks();
    
    private:
        void waitAnySignal();
    };
};

#endif