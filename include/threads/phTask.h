#ifndef PH_TASK_H
#define PH_TASK_H

#include <atomic>
#include <mutex>
#include <vector>
#include <condition_variable>

#include "phSync.h"

namespace phys
{
    class PhThreadsManager;

    class PhTask
    {
    private:
        std::atomic_int startedLaunchers;
        std::atomic_int finishedLaunchers;

    public:
        PhTask() : startedLaunchers(0),
                   finishedLaunchers(0) {}

        virtual ~PhTask() {}

        virtual void complete(int _instance) = 0;

        friend class PhThreadsManager;
    };
}

#endif