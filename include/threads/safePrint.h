#ifndef SAFE_PRINT_H
#define SAFE_PRINT_H

#include <mutex>
#include <stdio.h>

namespace phys
{
    class SafePrint
    {
    private:
        static std::mutex printLock;

    public:
        static void print(const char *c_str, ...);
    };
}

#endif