#ifndef PH_THREAD_H
#define PH_THREAD_H

#include <string>
#include <thread>
#include <mutex>
#include <atomic>

namespace phys
{
    class PhThread
    {
    protected:
        std::string name;
        int userData;

        std::thread thread;

        std::atomic_bool runFlag;
        std::atomic_bool stopLoopFlag;

    public:
        PhThread(const char *_name, int _userData);

        virtual ~PhThread();

        void init();

        bool run();

        bool runLoop();

        bool wait();

        bool quit();

        bool isRun() const { return runFlag.load(std::memory_order_acquire); }

        int getUserData() const { return userData; }

    protected:
        virtual void onRun() {}

        virtual void onInit() {}

    private:
        void singleRun();

        void loop();
    };

};

#endif