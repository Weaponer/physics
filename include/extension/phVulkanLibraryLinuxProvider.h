#ifndef PH_VULKAN_LIBRARY_LINUX_PROVIDER_H
#define PH_VULKAN_LIBRARY_LINUX_PROVIDER_H

#include "common/phVulkanLibraryProvider.h"

namespace phys
{
    class PhVulkanLibraryLinuxProvider : public PhVulkanLibraryProvider
    {
    protected:
        virtual void *loadLibrary() override;

        virtual void closeLibrary(void *_library) override;

        virtual void *loadFunctionOfLibrary(void *_library, const char *_name) override;
    };
}

#endif