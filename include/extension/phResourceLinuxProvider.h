#ifndef PH_RESOURCE_LINUX_PROVIDER_H
#define PH_RESOURCE_LINUX_PROVIDER_H

#include "common/phResourceProvider.h"

namespace phys
{
    class PhResourceLinuxProvider : public PhResourceProvider
    {
    public:
        PhResourceLinuxProvider() : PhResourceProvider() {}

        virtual ~PhResourceLinuxProvider() {}

    protected:
        virtual const char *getPathToShaders() override;

        virtual const char *getPathToObjectMeshes() override;

        virtual bool readFile(const char *_path, char **_data, int *_size) override;
    };
};

#endif