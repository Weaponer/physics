#ifndef PH_WINDOW_IM_GUI_H
#define PH_WINDOW_IM_GUI_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include <vector>

#include "window/windowGUI.h"
#include "plugins/imgui/imgui.h"
#include "plugins/imgui/backends/imgui_impl_sdl2.h"
#include "plugins/imgui/backends/imgui_impl_vulkan.h"

namespace phys
{
    class PhWindowImGUI : public WindowGUI
    {
    private:
        ImGui_ImplVulkanH_Window windowImGUI;
        VkDescriptorPool descriptorPool;
        VkPipelineCache pipelineCache;
        VkRenderPass renderPass;
        std::vector<VkFramebuffer> framebuffers;
        VkCommandPool commandPool;
        VkCommandBuffer cmd;

        bool showDemoWindow;
        bool otherTestWindow;

    public:
        PhWindowImGUI();

        virtual ~PhWindowImGUI();

        virtual void updateEvent(const SDL_Event *_event) override;

        virtual void beginImGui();

        virtual void endImGui();

        virtual void drawFrame(vk::VulkanSemaphore _waitSemaphore, vk::VulkanSemaphore _signalSemaphore, vk::VulkanFence _fence) override;

    protected:
        virtual void onInit(Window *_window, vk::VulkanDevice *_device, vk::VulkanQueue *_queue) override;

        virtual void onDestory() override;

    private:
        void createRenderPass();
    };
}

#endif