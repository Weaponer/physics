layout(location = 0) in vec3 vertColor;
layout(location = 1) in vec2 vertTexCoord;

layout(location = 0) out vec4 result;

void main()
{
    result = vec4(length(vertColor).xxx, 1.0);
}