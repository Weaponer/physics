layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 vertColor;
layout(location = 1) out vec2 vertTexCoord;

void main()
{
    vertColor = inPosition;
    vertTexCoord = inTexCoord;
    gl_Position = vec4(inPosition, 1.0);
}