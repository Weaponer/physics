#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "math.glsl"

layout(r32ui, set = 0, binding = 0) uniform uimage1D containersCounter; 

//storage buffers

layout(std430, set = 0, binding = 1) writeonly buffer countGroupsUpdateContainers
{
    uvec4 countGroups;
};

layout(std430, set = 0, binding = 2) writeonly buffer countContainersBuffer
{
    uint countContainers;
};

layout(push_constant, std430) uniform Constants
{
    uint sizeGroup;
} pConstants;


layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    uint cContainers = uint(imageLoad(containersCounter, 0).x);

    uint countGroupsX = cContainers / pConstants.sizeGroup;
    if((cContainers % pConstants.sizeGroup) != 0)
        countGroupsX += 1;

    countGroups = uvec4(countGroupsX, 1, 1, 0);
    countContainers = cContainers;
}