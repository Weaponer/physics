#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "math.glsl"

#include "updateContact.glsl"

#ifdef PLANE_BOX
    #define SIZE_GROUP_Y FLAG_GEOMETRY_COUNT_CONTACTS_PLANE_BOX
#endif

#ifdef BOX_BOX
    #define SIZE_GROUP_Y FLAG_GEOMETRY_COUNT_CONTACTS_BOX_BOX
#endif

#ifdef BOX_CAPSULE
    #define SIZE_GROUP_Y FLAG_GEOMETRY_COUNT_CONTACTS_BOX_CAPSULE
#endif

#ifdef CAPSULE_CAPSULE
    #define SIZE_GROUP_Y FLAG_GEOMETRY_COUNT_CONTACTS_CAPSULE_CAPSULE
#endif

#ifndef SIZE_GROUP_Y
    #define SIZE_GROUP_Y 1
#endif

//storage buffers

layout(std430, set = 0, binding = 0) readonly buffer typesContactsContainersBuffer
{
    uvec4 containersData[MAX_COUNT_TYPE_CONTACTS];
};

CONTACTS_CONTAINERS_DATA_R(0, 1)

ORIENTATION_GEOMETRY_OBBS_DATA_R(0, 2)
GEOMETRY_OBBS_DATA(0, 3)
GEOMETRIES_DATA(0, 4)

STORAGE_BUFFER_R(0, 5, inputContactsData, DataContact, inContacts) //input contacts for update
STORAGE_BUFFER_W(0, 6, outputContactsData, DataContact, outContacts) //output updated contacts
STORAGE_BUFFER_W(0, 7, outputIsContactActive, bool, outIsContactActive) //output is contact active 

layout(push_constant, std430) uniform Constants
{
    uint indexContactType;
} pConstants;

layout (local_size_x = SIZE_GROUP_CONTACT_GENERATOR, local_size_y = SIZE_GROUP_Y, local_size_z = 1) in;
void main()
{  
    uint firstContainer = containersData[pConstants.indexContactType].x;
    uint countContainers = containersData[pConstants.indexContactType].y;

    uint index = firstContainer + gl_GlobalInvocationID.x;
    if(gl_GlobalInvocationID.x >= countContainers)
        return;

    DataContactsContainer container = contactsContainers[index];

    bool isExistContact = ((container.countAndMaskContacts >> CONTACTS_CONTAINER_MASK_OFFSET_BITS_USE_CONTACTS) & (1 << gl_GlobalInvocationID.y)) != 0;
    if(!isExistContact)
        return;

    uint firstGeometry = container.firstIndexGeometry;
    uint secondGeometry = container.secondIndexGeometry;

    DataGeometryOBB firstDataOBB = geometryObbs[firstGeometry];
    DataGeometryOBB secondDataOBB = geometryObbs[secondGeometry];

    DataGeometry firstData = geometries[firstDataOBB.indexMainGeometry];
    DataGeometry secondData = geometries[secondDataOBB.indexMainGeometry];
    DataOrientationGeometryOBB firstOrientation = orientationObbs[firstGeometry];
    DataOrientationGeometryOBB secondOrientation = orientationObbs[secondGeometry];

    uint indexUpdateContact = container.firstContactIndex + gl_GlobalInvocationID.y; 
    uint indexWriteContact = containersData[pConstants.indexContactType].z + gl_GlobalInvocationID.x * SIZE_GROUP_Y + gl_GlobalInvocationID.y;

    DataContact updateContact = inContacts[indexUpdateContact];

    if((container.countAndMaskContacts & (1 << CONTACTS_CONTAINER_MASK_OFFSET_BITS_SLEEP_ACTORS)) != 0 )
    {
        outContacts[indexWriteContact] = updateContact;
        outIsContactActive[indexWriteContact] = true;
        return;
    }

    UpdatedContact updatedContact;
    updatedContact.positionA = vec3(0, 0, 0);
    updatedContact.positionB = vec3(0, 0, 0);

    bool isActualContact = false;

    #ifdef PLANE_BOX
        isActualContact = updateContactPlaneBox(firstData, firstOrientation, secondData, secondOrientation, 
                                                updateContact, updatedContact);
    #endif
    
    #ifdef BOX_BOX
        isActualContact = updateContactBoxBox(firstData, firstOrientation, secondData, secondOrientation, 
                                                updateContact, updatedContact);
    #endif

    #ifdef BOX_CAPSULE
        isActualContact = updateContactBoxCapsule(firstData, firstOrientation, secondData, secondOrientation, 
                                                    updateContact, updatedContact);
    #endif

    #ifdef CAPSULE_CAPSULE
        isActualContact = updateContactCapsuleCapsule(firstData, firstOrientation, secondData, secondOrientation, 
                                                        updateContact, updatedContact);
    #endif

    if(isActualContact)
    {
        vec3 pos_1 = firstOrientation.position.xyz;
        vec3 pos_2 = secondOrientation.position.xyz;
        mat3 invRot_1 = quaternionToMat3(firstOrientation.rotation);
        mat3 invRot_2 = quaternionToMat3(secondOrientation.rotation);

        vec3 normalAtoB = updatedContact.positionB - updatedContact.positionA;
        float depth = length(normalAtoB);
        normalAtoB /= depth;

        if(depth <= 0.00001)
            return;

        updateContact.depthPenetration = depth;
        updateContact.firstLocalPosition = invRot_1 * (updatedContact.positionA - pos_1);
        updateContact.secondLocalPosition = invRot_2 * (updatedContact.positionB - pos_2);
        updateContact.firstLocalNormal = invRot_1 * normalAtoB;
        updateContact.secondLocalNormal = invRot_2 * -normalAtoB;

        updateContact.firstPointGlobal.xyz = updatedContact.positionA;
        updateContact.secondPointGlobal.xyz = updatedContact.positionB;
        updateContact.firstOriginGlobal.xyz = pos_1;
        updateContact.secondOriginGlobal.xyz = pos_2;

        outContacts[indexWriteContact] = updateContact;
        outIsContactActive[indexWriteContact] = true;
    }
}