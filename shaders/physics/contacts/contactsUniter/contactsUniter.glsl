#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "math.glsl"

//storage buffers

layout(std430, set = 0, binding = 0) readonly buffer typesContactsContainersBuffer
{
    uvec4 containersData[MAX_COUNT_TYPE_CONTACTS]; //x - index first container, y - count containers, z - first index contact, w - count all contacts
};

layout(std430, set = 0, binding = 1) readonly buffer uniteContainersDataBuffer
{
    uint maxCountUniteContainers;
};

STORAGE_BUFFER_R(0, 2, inputContactsContainers, DataContactsContainer, inContactsContainers)
STORAGE_BUFFER_R(0, 3, inputGeneratedContactsData, DataContact, inGeneratedContacts) 
STORAGE_BUFFER_R(0, 4, inputIndexTypeContainer, uint, inIndexTypeContainer)

STORAGE_BUFFER_R(0, 5, inputUpdatedContactsData, DataContact, inUpdatedContacts) 
STORAGE_BUFFER_R(0, 6, inputIsContactActive, bool, inIsContactActive)  

STORAGE_BUFFER_W(0, 7, outputContactsContainers, DataContactsContainer, outContactsContainers)  //output contaciners
STORAGE_BUFFER_W(0, 8, outputContactsData, DataContact, outContacts) //output contacts

bool checkVector(vec3 vec)
{
    if(isinf(vec.x) || isnan(vec.x) || isinf(vec.y) || isnan(vec.y) || isinf(vec.z) || isnan(vec.z))
        return false;
    return true;
}

bool checkContact(DataContact contact)
{
    if(!checkVector(contact.firstLocalNormal) || 
        !checkVector(contact.secondLocalNormal) ||
        !checkVector(contact.firstLocalPosition) ||
        !checkVector(contact.secondLocalPosition))
        return false;
    return true;
}

layout (local_size_x = SIZE_GROUP_CONTACT_GENERATOR, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    uint indexContainer = gl_GlobalInvocationID.x;
    if(indexContainer >= maxCountUniteContainers)
        return;

    uint indexTypeContainer = inIndexTypeContainer[indexContainer];

    DataContactsContainer mainContainer = inContactsContainers[indexContainer];

    uint countContactsPerContainer = mainContainer.countAndMaskContacts & CONTACTS_CONTAINER_MASK_COUNT_CONTACTS;

    uint indexReadUpdatedContacts = 0;
    {
        uvec4 containersData = containersData[indexTypeContainer];
        uint subIndexContainer = indexContainer - containersData.x;
        indexReadUpdatedContacts = subIndexContainer * countContactsPerContainer + containersData.z;
    }

    DataContact generatedContact = inGeneratedContacts[indexContainer];

    DataContact checkContacts[CONTACTS_CONTAINER_MAX_COUNT_CONTACTS];
    uint countUpdatedContacts = 0;
    for(uint i = 0; i < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS && i < countContactsPerContainer;i++)
    {
        uint indexReadUpdateContact = indexReadUpdatedContacts + i;
        if(inIsContactActive[indexReadUpdateContact])
        {
            DataContact c = inUpdatedContacts[indexReadUpdateContact];
            if(checkContact(c))
            {
                checkContacts[countUpdatedContacts] = c;
                countUpdatedContacts++;
            }   
        }
    }


    vec3 nGeneratedA = normalize(generatedContact.firstLocalPosition);
    vec3 nGeneratedB = normalize(generatedContact.secondLocalPosition);

    float dotContactsA[CONTACTS_CONTAINER_MAX_COUNT_CONTACTS];
    float dotContactsB[CONTACTS_CONTAINER_MAX_COUNT_CONTACTS];
    float averageA = 0;
    float averageB = 0;

    for(uint i = 0; i < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS && i < countUpdatedContacts; i++)
    {
        dotContactsA[i] = dot(nGeneratedA, normalize(checkContacts[i].firstLocalPosition));
        dotContactsB[i] = dot(nGeneratedB, normalize(checkContacts[i].secondLocalPosition));

        averageA += dotContactsA[i];
        averageB += dotContactsB[i];
    }
    averageA /= countUpdatedContacts;
    averageB /= countUpdatedContacts;

    float dispersionA = 0;
    float dispersionB = 0;

    for(uint i = 0; i < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS && i < countUpdatedContacts; i++)
    {
        float difA = dotContactsA[i] - averageA;
        dispersionA += difA * difA;
        float difB = dotContactsB[i] - averageB;
        dispersionB += difB * difB;
    }

    float differenionA = sqrt(dispersionA / countUpdatedContacts);
    float differenionB = sqrt(dispersionB / countUpdatedContacts);

    float limitToRemoveContact = -1.0;
    //if(countContactsPerContainer > countUpdatedContacts)
        limitToRemoveContact = MIN_COS_FOR_REMOVING_SIMILAR_CONTACT;

    int removeContact = -1;
    {
        float minCos = -1.1;
        if(differenionA > differenionB)
        {
            for(int i = 0; i < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS && i < countUpdatedContacts; i++)
            {
                if(dotContactsA[i] >= limitToRemoveContact && dotContactsA[i] >= minCos)
                {
                    removeContact = i;
                    minCos = dotContactsA[i];
                }
            }
        }
        else
        {
            for(int i = 0; i < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS && i < countUpdatedContacts; i++)
            {
                if(dotContactsB[i] >= limitToRemoveContact && dotContactsB[i] >= minCos)
                {
                    removeContact = i;
                    minCos = dotContactsB[i];
                }
            }
        }
    }

    bool removeGeneratedContact = false;
    if(length(generatedContact.firstLocalPosition) <= 0.001 && length(generatedContact.secondLocalPosition) <= 0.001)
        removeGeneratedContact = true;
    
    if(!removeGeneratedContact && checkContact(generatedContact))
    {
        if(removeContact != -1)
        {
            checkContacts[removeContact] = generatedContact;
        }
        else
        {
            if(countUpdatedContacts == countContactsPerContainer)
            {
                //checkContacts[countUpdatedContacts - 1] = generatedContact;
            }
            else
            {
                checkContacts[countUpdatedContacts] = generatedContact;
                countUpdatedContacts++;
            }
        }
    }

    uint maskUseContactsAndCount = 0;
    for(uint i = 0; i < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS && i < countUpdatedContacts; i++)
        maskUseContactsAndCount |= 1 << i;
    maskUseContactsAndCount = (maskUseContactsAndCount << CONTACTS_CONTAINER_MASK_OFFSET_BITS_USE_CONTACTS) | countContactsPerContainer;
    maskUseContactsAndCount |= mainContainer.countAndMaskContacts & CONTACTS_CONTAINER_MASK_FLAGS;

    DataContactsContainer newContainerData = mainContainer;
    newContainerData.countAndMaskContacts = maskUseContactsAndCount;
    newContainerData.firstContactIndex = indexReadUpdatedContacts;

    outContactsContainers[indexContainer] = newContainerData;

    for(uint i = 0; i < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS && i < countUpdatedContacts; i++)
    {
        uint indexWriteContact = indexReadUpdatedContacts + i;
        outContacts[indexWriteContact] = checkContacts[i];
    }
}