#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "math.glsl"

#ifdef BOX_BOX
    #define CONTACT_BOX_BOX_GJK
#endif

#ifdef BOX_CAPSULE
    #define CONTACT_BOX_CAPSULE_GJK
#endif

#include "contact.glsl"

//storage buffers

layout(std430, set = 0, binding = 0) readonly buffer typesContactsContainersBuffer
{
    uvec4 containersData[MAX_COUNT_TYPE_CONTACTS];
};

CONTACTS_CONTAINERS_DATA_R(0, 1)

ORIENTATION_GEOMETRY_OBBS_DATA_R(0, 2)
GEOMETRY_OBBS_DATA(0, 3)
GEOMETRIES_DATA(0, 4)

CONTACTS_DATA_W(0, 5) //output 
STORAGE_BUFFER_W(0, 6, outputIndexTypeContainer, uint, outIndexTypeContainer) // output index type of container


layout(push_constant, std430) uniform Constants
{
    uint indexContactType;
} pConstants;

layout (local_size_x = SIZE_GROUP_CONTACT_GENERATOR, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    uint firstContainer = containersData[pConstants.indexContactType].x;
    uint countContainers = containersData[pConstants.indexContactType].y;

    uint index = firstContainer + gl_GlobalInvocationID.x;
    if(gl_GlobalInvocationID.x >= countContainers)
        return;

    DataContactsContainer container = contactsContainers[index];

    if((container.countAndMaskContacts & (1 << CONTACTS_CONTAINER_MASK_OFFSET_BITS_SLEEP_ACTORS)) != 0 )
    {
        DataContact contact;
        contact.firstLocalPosition = vec3(0);
        contact.secondLocalPosition = vec3(0);
        contact.firstLocalNormal = vec3(0);
        contact.secondLocalNormal = vec3(0);
        contact.depthPenetration = 0;

        contact.firstPointGlobal.xyz = vec3(0);
        contact.secondPointGlobal.xyz = vec3(0);
        contact.firstOriginGlobal.xyz = vec3(0);
        contact.secondOriginGlobal.xyz = vec3(0);

        contacts[index] = contact;
        outIndexTypeContainer[index] = pConstants.indexContactType;
        return;
    }

    uint firstGeometry = container.firstIndexGeometry;
    uint secondGeometry = container.secondIndexGeometry;

    DataGeometryOBB firstDataOBB = geometryObbs[firstGeometry];
    DataGeometryOBB secondDataOBB = geometryObbs[secondGeometry];

    DataGeometry firstData = geometries[firstDataOBB.indexMainGeometry];
    DataGeometry secondData = geometries[secondDataOBB.indexMainGeometry];
    DataOrientationGeometryOBB firstOrientation = orientationObbs[firstGeometry];
    DataOrientationGeometryOBB secondOrientation = orientationObbs[secondGeometry];

    GlobalContactData globalContact;
    #ifdef PLANE_BOX
        globalContact = generateContactPlaneBox(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef PLANE_SPHERE
        globalContact = generateContactPlaneSphere(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef PLANE_CAPSULE
        globalContact = generateContactPlaneCapsule(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef BOX_SPHERE
        globalContact = generateContactBoxSphere(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef BOX_BOX
        globalContact = generateContactBoxBox(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef BOX_CAPSULE
        globalContact = generateContactBoxCapsule(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef SPHERE_SPHERE
        globalContact = generateContactSphereSphere(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef SPHERE_CAPSULE
        globalContact = generateContactSphereCapsule(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef CAPSULE_CAPSULE
        globalContact = generateContactCapsuleCapsule(firstData, firstOrientation, secondData, secondOrientation);
    #endif

    mat3 firstRotate = quaternionToMat3(firstOrientation.rotation);
    mat3 secondRotate = quaternionToMat3(secondOrientation.rotation);
    vec3 firstPos = firstOrientation.position.xyz;
    vec3 secondPos = secondOrientation.position.xyz;

    vec3 normal = globalContact.positionB - globalContact.positionA;
    float penetration = length(normal);
    normal /= penetration;

    vec3 firstLocalPos = globalContact.positionA - firstPos;
    vec3 secondLocalPos = globalContact.positionB - secondPos;

    firstLocalPos = firstRotate * firstLocalPos;
    secondLocalPos = secondRotate * secondLocalPos;

    DataContact contact;
    contact.firstLocalPosition = firstLocalPos;
    contact.secondLocalPosition = secondLocalPos;
    contact.firstLocalNormal = firstRotate * normal;
    contact.secondLocalNormal = secondRotate * (-normal);
    contact.depthPenetration = penetration;

    contact.firstPointGlobal.xyz = globalContact.positionA;
    contact.secondPointGlobal.xyz = globalContact.positionB;
    contact.firstOriginGlobal.xyz = firstPos;
    contact.secondOriginGlobal.xyz = secondPos;

    contacts[index] = contact;
    outIndexTypeContainer[index] = pConstants.indexContactType;
}