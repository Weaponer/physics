#include "buffers.glsl"
#include "math.glsl"

layout(std430, set = 0, binding = 0) readonly buffer iMainPlanes  
{                        
    vec4 inPosition;
    vec4 inRight;
    vec4 inUp;
    vec4 inForward;                                                     
};

layout(std430, row_major, set = 0, binding = 1) readonly buffer oCountContacts  
{                                                                             
    uint xPlaneContacts;
    uint yPlaneContacts;
    uint zPlaneContacts;
};

layout(std430, set = 0, binding = 2) writeonly buffer oGlobalBound  
{                        
    mat4 outGlobalMatrix;                                            
};

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
void main()
{
    vec3 right = inRight.xyz;
    vec3 up = inUp.xyz;
    vec3 forward = inForward.xyz;

    uint countX = xPlaneContacts;
    uint countY = yPlaneContacts;
    uint countZ = zPlaneContacts;

    if(countY <= countX && countY <= countZ)
    {
        vec3 temp = right;
        right = up;
        up = -temp;
    }
    else if(countZ <= countX && countZ <= countY)
    {
        vec3 temp = right;
        right = forward;
        forward = -temp;
    }

    mat4 rot = transpose(mat4(vec4(right, 0.0), vec4(up, 0.0), vec4(forward, 0.0), vec4(0.0, 0.0, 0.0, 1.0)));
    vec3 pos = inPosition.xyz;
    mat4 translate = mat4(vec4(1.0, 0.0, 0.0, pos.x), vec4(0.0, 1.0, 0.0, pos.y), vec4(0.0, 0.0, 1.0, pos.z), vec4(0.0, 0.0, 0.0, 1.0));

    outGlobalMatrix = rot * translate; 
    outGlobalMatrix[3] = vec4(countX, countY, countZ, 1.0);
}