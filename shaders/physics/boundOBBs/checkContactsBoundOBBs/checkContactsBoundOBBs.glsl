#include "buffers.glsl"
#include "math.glsl"

layout(std430, set = 0, binding = 0) readonly buffer iMainPlanes  
{                        
    vec4 inPosition;
    vec4 inRight;
    vec4 inUp;
    vec4 inForward;                                                     
};

layout(std430, row_major, set = 0, binding = 1) writeonly buffer oCountContacts  
{                                                                             
    uint xPlaneContacts;
    uint yPlaneContacts;
    uint zPlaneContacts;
};

STORAGE_BUFFER_R(1, 0, inputIndexesTest, uint, indexesTest)
ORIENTATION_GEOMETRY_OBBS_DATA_R(1, 1)

layout(push_constant, std430) uniform Constants
{
    uint countTestOBBs;
} pConstants;

bool checkContactPlane(vec3 posP, vec3 dirP, vec3 posB, mat3 rotB, vec3 sizeB)
{
    float r = sizeB.x * abs(dot(dirP, rotB[0])) +
              sizeB.y * abs(dot(dirP, rotB[1])) +
              sizeB.z * abs(dot(dirP, rotB[2]));
    float d = (dot(dirP, posB)) - (dot(posP, dirP));
    return abs(d) <= r;
}

layout (local_size_x = 16, local_size_y = 1, local_size_z = 1) in;
void main()
{
    if(pConstants.countTestOBBs <= gl_GlobalInvocationID.x)
        return;
    
    uint curIndex = indexesTest[gl_GlobalInvocationID.x];

    vec3 position = orientationObbs[curIndex].position.xyz;
    mat3 rotate =  quaternionToMat3(orientationObbs[curIndex].rotation);
    vec3 size = orientationObbs[curIndex].size.xyz;

    vec3 right = inRight.xyz;
    vec3 up = inUp.xyz;
    vec3 forward = inForward.xyz;

    if(checkContactPlane(inPosition.xyz, right, position, rotate, size))
        atomicAdd(xPlaneContacts, 1);

    if(checkContactPlane(inPosition.xyz, up, position, rotate, size))
        atomicAdd(yPlaneContacts, 1);

    if(checkContactPlane(inPosition.xyz, forward, position, rotate, size))
        atomicAdd(zPlaneContacts, 1);
}