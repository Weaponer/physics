#include "buffers.glsl"

//storage buffers
layout(std430, row_major, set = 0, binding = 0) readonly buffer iSumAverage   
{                                                                             
    int averageX;
    int averageY;
    int averageZ;
};

layout(std430, row_major, set = 0, binding = 1) readonly buffer oDispersionSize  
{                                                                             
    int dispersionX;
    int dispersionY;
    int dispersionZ;
};

layout(std430, set = 0, binding = 2) writeonly buffer oMainPlanes  
{                        
    vec4 outPosition;
    vec4 outRight;
    vec4 outUp;
    vec4 outForward;                                                     
};

//uniform buffers
SCENE_SETTING_DATA(1, 0)

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
void main()
{
    vec3 centerPosition = vec3(float(averageX), float(averageY), float(averageZ)) / sceneSetting.percision;
    vec3 dispersion = vec3(float(dispersionX), float(dispersionY), float(dispersionZ)) / sceneSetting.percision;
    
    //calculate bound orientaion
    vec3 nDispersion = vec3(1, 0, 0);
    if(length(dispersion) != 0)
    {
        nDispersion = normalize(dispersion);
    }
    
    vec3 up = vec3(0, 1, 0);
    vec3 forward = vec3(0, 0, 1);
    if(abs(dot(up, nDispersion)) >= 0.9)
    {
        up = vec3(0, 0, 1);
        forward = vec3(0, 1, 0);
    }   
    forward = normalize(cross(nDispersion, up));
    up = normalize(cross(forward, nDispersion));
    
    outPosition = vec4(centerPosition, 0.0);
    outRight = vec4(nDispersion, 0.0);
    outUp = vec4(up, 0.0);
    outForward = vec4(forward, 0.0);
}