#extension GL_KHR_memory_scope_semantics : require

#include "buffers.glsl"

//storage buffers
layout(std430, row_major, set = 0, binding = 0) writeonly buffer oSumAverage   
{                                                                             
    int averageX;
    int averageY;
    int averageZ;
};

STORAGE_BUFFER_R(1, 0, inputIndexesTest, uint, indexesTest)
ORIENTATION_GEOMETRY_OBBS_DATA_R(1, 1)

//uniform buffers
SCENE_SETTING_DATA(1, 2)

layout(push_constant, std430) uniform Constants
{
    uint countTestOBBs;
} pConstants;

float getFracPart(float val)
{
    return val - int(val);
}

layout (local_size_x = 16, local_size_y = 1, local_size_z = 1) in;
void main()
{
    if(pConstants.countTestOBBs <= gl_GlobalInvocationID.x)
        return;
    
    uint curIndex = indexesTest[gl_GlobalInvocationID.x];

    vec3 position = orientationObbs[curIndex].position.xyz;

    atomicAdd(averageX, int(position.x * sceneSetting.percision / float(pConstants.countTestOBBs)));
    atomicAdd(averageY, int(position.y * sceneSetting.percision / float(pConstants.countTestOBBs)));
    atomicAdd(averageZ, int(position.z * sceneSetting.percision / float(pConstants.countTestOBBs)));
}