#include "buffers.glsl"

//storage buffers
layout(std430, row_major, set = 0, binding = 0) readonly buffer iSumAverage   
{                                                                             
    int averageX;
    int averageY;
    int averageZ;
};

layout(std430, row_major, set = 0, binding = 1) writeonly buffer oDispersionSize  
{                                                                             
    int dispersionX;
    int dispersionY;
    int dispersionZ;
};

STORAGE_BUFFER_R(1, 0, inputIndexesTest, uint, indexesTest)
ORIENTATION_GEOMETRY_OBBS_DATA_R(1, 1)

//uniform buffers
SCENE_SETTING_DATA(1, 2)

layout(push_constant, std430) uniform Constants
{
    uint countTestOBBs;
} pConstants;

float getFracPart(float val)
{
    return val - int(val);
}

layout (local_size_x = 16, local_size_y = 1, local_size_z = 1) in;
void main()
{
    if(pConstants.countTestOBBs <= gl_GlobalInvocationID.x)
        return;
    
    uint curIndex = indexesTest[gl_GlobalInvocationID.x];

    vec3 position = orientationObbs[curIndex].position.xyz;
    
    vec3 boundCenter = vec3(float(averageX), float(averageY), float(averageZ)) / sceneSetting.percision;
    vec3 dispersion = boundCenter - position;
    dispersion.x *= dispersion.x;
    dispersion.y *= dispersion.y;
    dispersion.z *= dispersion.z;

    atomicAdd(dispersionX, int(dispersion.x * sceneSetting.percision / float(pConstants.countTestOBBs)));
    atomicAdd(dispersionY, int(dispersion.y * sceneSetting.percision / float(pConstants.countTestOBBs)));
    atomicAdd(dispersionZ, int(dispersion.z * sceneSetting.percision / float(pConstants.countTestOBBs)));
}