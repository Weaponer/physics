#include "buffers.glsl"
#include "math.glsl"

//images
layout(r32ui, set = 0, binding = 0) uniform uimage2D previosNodeCountOBBs;
layout(r32ui, set = 0, binding = 1) uniform uimage2D nodeCountOBBs;
layout(r32i, set = 0, binding = 2) uniform iimage2D nodeDistanceOBBs;
layout(r32f, set = 0, binding = 3) uniform image2D resultNodeOffset;
layout(r8ui, set = 0, binding = 4) uniform uimage2D resultActiveNodes;

layout(r32ui, set = 0, binding = 5) uniform uimage1D counterHeadPoints; // count - MAX_ITERATIONS

//storage buffers
layout(std430, set = 0, binding = 6) writeonly buffer resultIndirectBuffer
{
    uint resultCountGroupsX;
    uint resultCountGroupsY;
    uint resultCountGroupsZ;
};

//uniform buffers
SCENE_SETTING_DATA(0, 7)

layout(push_constant, std430) uniform Constants
{
    uint indexIteration;
    uint maxSizeOutIndexes;
} pConstants;

layout (local_size_x = 2, local_size_y = 1, local_size_z = 1) in;
void main()
{
    ivec2 size = imageSize(nodeCountOBBs).xy;

    // check active node
    uint index = gl_GlobalInvocationID.y * size.x + gl_GlobalInvocationID.x;
    uint inverseIndex = index;
    if((index & 1) == 0)
        inverseIndex = inverseIndex | 1;
    else
        inverseIndex = inverseIndex & (~1);

    ivec2 inverseCoord = ivec2(inverseIndex % size.x, inverseIndex / size.x);

    uint currentCount = uint(imageLoad(nodeCountOBBs, ivec2(gl_GlobalInvocationID.xy)).x);
    uint inverseCount = uint(imageLoad(nodeCountOBBs, inverseCoord).x);

    uint activeNode = ~0;

    if(pConstants.indexIteration != 0)
    {
        uint previosIndex = index >> 1;
        ivec2 previosSize = imageSize(previosNodeCountOBBs).xy;
        ivec2 previosCoord = ivec2(previosIndex % previosSize.x, previosIndex / previosSize.x);

        uint previosCount = uint(imageLoad(previosNodeCountOBBs, previosCoord).x);

        if(previosCount == currentCount && previosCount == inverseCount)
        {
            if(inverseIndex > index)
                activeNode = 0;
        }
        else if(previosCount == inverseCount)
            activeNode = 0;
    }

    if(currentCount <= 1)
            activeNode = 0;
    
    imageStore(resultActiveNodes, ivec2(gl_GlobalInvocationID.xy), uvec4(activeNode));

    //generate offset
    float sumDist = float(imageLoad(nodeDistanceOBBs, ivec2(gl_GlobalInvocationID.xy)).x);

    float offset = 0;
    if(currentCount != 0)
        offset = (sumDist / float(sceneSetting.percision)) / float(currentCount);

    imageStore(resultNodeOffset, ivec2(gl_GlobalInvocationID.xy), vec4(offset));

    if(gl_GlobalInvocationID.xy != ivec2(0, 0))
        return;

    const uint sizeGroupX = 128;
    
    uint countOBBs = min(pConstants.maxSizeOutIndexes, uint(imageLoad(counterHeadPoints, int(pConstants.indexIteration)).x));
    
    uint groupsX = countOBBs / sizeGroupX;
    if(countOBBs % sizeGroupX != 0)
        groupsX++;

    resultCountGroupsX = groupsX; 
    resultCountGroupsY = 1;
    resultCountGroupsZ = 1;
}