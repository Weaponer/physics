#include "buffers.glsl"
#include "math.glsl"

//images
layout(r32ui, set = 0, binding = 0) uniform uimage2D nodeCountOBBsPrevios;         
layout(r32ui, set = 0, binding = 1) uniform uimage2D nodeCountOBBsFinal;         

layout(r32i, set = 0, binding = 2) uniform iimage2D headPointsOutput;         

layout(r32ui, set = 0, binding = 3) uniform uimage1D headPointsCounter;

layout(r32ui, set = 0, binding = 4) uniform uimage1D countUsedIndexesInSteps;

STORAGE_BUFFER_R(0, 5, inputObbs, uvec2, inIndexesObbs)
STORAGE_BUFFER_W(0, 6, outputListNodes, ivec2, outNodes)

layout(push_constant, std430) uniform Constants
{
    uint indexStep;
    uint countOutputNodes;
} pConstants;

ivec2 getCoord(uint index, ivec2 size)
{
    return ivec2(index % size.x, index / size.x);
}

layout (local_size_x = 128, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint countWrited =  uint(imageLoad(countUsedIndexesInSteps, int(pConstants.indexStep)).x);
    
    if(countWrited <= gl_GlobalInvocationID.x)
        return;

    uvec2 data = inIndexesObbs[gl_GlobalInvocationID.x];
    uint index = data.x;
    uint key = data.y;

    if(pConstants.indexStep != 0)
    {
        uint previosKey = key >> 1;
        uint inverseKey = key;
        if((key & 1) == 0)
            inverseKey = key | 1;
        else
            inverseKey = key & (~1);

        uint previosCount = uint(imageLoad(nodeCountOBBsPrevios, getCoord(previosKey, imageSize(nodeCountOBBsPrevios).xy)).x);
        uint inverseCount = uint(imageLoad(nodeCountOBBsFinal, getCoord(inverseKey, imageSize(nodeCountOBBsFinal).xy)).x);
        uint currentCount = uint(imageLoad(nodeCountOBBsFinal, getCoord(key, imageSize(nodeCountOBBsFinal).xy)).x);

        if(previosCount == currentCount && previosCount == inverseCount)
        {
            if(inverseKey > key)
                return;
        }
        else if(previosCount == inverseCount)
        {
            return;
        }

        if(currentCount <= 1)
            return;
    }

    uint writePoint = uint(imageAtomicAdd(headPointsCounter, 0, 1));

    if(writePoint >= pConstants.countOutputNodes)
        return;

    int lastPoint = int(imageAtomicExchange(headPointsOutput, getCoord(key, imageSize(nodeCountOBBsFinal).xy), int(writePoint)).x);
    ivec2 resultData = ivec2(index, lastPoint);
    outNodes[writePoint] = resultData;
}