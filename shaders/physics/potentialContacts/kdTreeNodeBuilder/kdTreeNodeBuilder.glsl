#include "buffers.glsl"
#include "math.glsl"

#define MAX_ITERATIONS 19

uint arrayIndexDirections[MAX_ITERATIONS] = {
    0, 1, 2,
    0, 1, 2,
    0, 1, 2,
    0, 1, 2,
    0, 1, 2,
    0, 1, 2,
    0
};

//images
layout(r32ui, set = 0, binding = 0) uniform uimage2D nodeCountOBBs;         
layout(r32i, set = 0, binding = 1) uniform iimage2D nodeDistanceOBBs;
layout(r32f, set = 0, binding = 2) uniform image2D previosNodeOffset;
layout(r8ui, set = 0, binding = 3) uniform uimage2D previosActiveNodes;

layout(r32ui, set = 0, binding = 4) uniform uimage1D counterHeadPoints; // count - MAX_ITERATIONS

//storage buffers
layout(std430, set = 0, binding = 5) readonly buffer oGlobalBound  
{                        
    mat4 outGlobalMatrix;
    mat4 outInverseGlobalMatrix;                                                 
};

STORAGE_BUFFER_R(0, 6, inputObbs, uvec2, inIndexesObbs)
ORIENTATION_GEOMETRY_OBBS_DATA_R(0, 7)
STORAGE_BUFFER_W(0, 8, outputObbs, uvec2, outIndexesObbs)

//unfirom buffers
SCENE_SETTING_DATA(0, 9)

layout(push_constant, std430) uniform Constants
{
    uint indexIteration;
    uint countStartOBBs;
    uint sizeInIndexes;
    uint sizeOutIndexes;
} pConstants;

ivec2 getInCoord(uint index)
{
    ivec2 size = imageSize(previosNodeOffset);
    return ivec2(index % size.x, index / size.x);
}

ivec2 getOutCoord(uint index)
{
    ivec2 size = imageSize(nodeCountOBBs);
    return ivec2(index % size.x, index / size.x);
}

bool isActiveNode(uint key)
{
    return uint(imageLoad(previosActiveNodes, getInCoord(key)).x) != 0;
}

vec4 getCheckPlane(uint key)
{
    vec3 directions[3] = { vec3(outGlobalMatrix[0].x, outGlobalMatrix[1].x, outGlobalMatrix[2].x),
                            vec3(outGlobalMatrix[0].y, outGlobalMatrix[1].y, outGlobalMatrix[2].y),
                            vec3(outGlobalMatrix[0].z, outGlobalMatrix[1].z, outGlobalMatrix[2].z) };
    
    vec3 position = vec3(outGlobalMatrix[0].w, outGlobalMatrix[1].w, outGlobalMatrix[2].w);
    
    float offset = float(imageLoad(previosNodeOffset, getInCoord(key)).x);
    
    vec3 direction = directions[arrayIndexDirections[pConstants.indexIteration]];
    vec3 positionPlane = position + direction * offset;

    return vec4(direction, dot(positionPlane, direction));
}

vec4 getNextPlane()
{
    vec3 directions[3] = { vec3(outGlobalMatrix[0].x, outGlobalMatrix[1].x, outGlobalMatrix[2].x),
                            vec3(outGlobalMatrix[0].y, outGlobalMatrix[1].y, outGlobalMatrix[2].y),
                            vec3(outGlobalMatrix[0].z, outGlobalMatrix[1].z, outGlobalMatrix[2].z) };
    
    vec3 position = vec3(outGlobalMatrix[0].w, outGlobalMatrix[1].w, outGlobalMatrix[2].w);
    
    vec3 direction = directions[arrayIndexDirections[pConstants.indexIteration + 1]];
    return vec4(direction, dot(position, direction));
}

float getDistToPlane(vec4 plane, vec3 position)
{
    return dot(plane.xyz, position) - plane.w;
}

ivec2 checkPlaneOBB(vec4 plane, vec3 position, mat3 rotation, vec3 size)
{
    float r = size.x * abs(dot(plane.xyz, rotation[0])) +
              size.y * abs(dot(plane.xyz, rotation[1])) +
              size.z * abs(dot(plane.xyz, rotation[2]));

    float d = dot(plane.xyz, position) - plane.w;
    
    if(abs(d) <= r)
        return ivec2(1, 1);
    else if(d >= 0.0f)
        return ivec2(1, 0);
    else
        return ivec2(0, 1);

    return ivec2(0, 0);
}

void addOBBsToNode(uint key, float dist)
{
    ivec2 coord = getOutCoord(key);
    int data = int(dist * sceneSetting.percision);

    imageAtomicAdd(nodeCountOBBs, coord, 1);
    imageAtomicAdd(nodeDistanceOBBs, coord, data);
}

layout (local_size_x = 128, local_size_y = 1, local_size_z = 1) in;
void main()
{
    if(pConstants.indexIteration != 0)
    {
        int indexRead = int(pConstants.indexIteration - 1);
        uint wasWrited = uint(imageLoad(counterHeadPoints, indexRead).x);
        if(wasWrited <= gl_GlobalInvocationID.x)
            return;
    }
    else if(pConstants.indexIteration == 0 && pConstants.countStartOBBs <= gl_GlobalInvocationID.x)
        return;

    if(pConstants.sizeInIndexes <= gl_GlobalInvocationID.x)
        return;

    uvec2 inData = inIndexesObbs[gl_GlobalInvocationID.x];
    
    uint indexOBB = inData.x;
    uint key = inData.y;
    
    if(!isActiveNode(key))
        return;

    vec4 plane = getCheckPlane(key);

    vec3 position = orientationObbs[indexOBB].position.xyz;
    mat3 rotate =  quaternionToMat3(orientationObbs[indexOBB].rotation);
    vec3 size = orientationObbs[indexOBB].size.xyz;

    ivec2 contactSides = checkPlaneOBB(plane, position, rotate, size); //x - positive, y - negative

    int countContacts = contactSides.x + contactSides.y; 
    uint writePoint = uint(imageAtomicAdd(counterHeadPoints, int(pConstants.indexIteration), countContacts).x);
    
    if((writePoint + countContacts - 1) >= pConstants.sizeOutIndexes)
       return;
    
    float distToNextPlane = getDistToPlane(getNextPlane(), position);
    
    if(countContacts == 2)
    {
        uint keyOne = key << 1;
        uint keyTwo = (key << 1) | 1;

        addOBBsToNode(keyOne, distToNextPlane);
        addOBBsToNode(keyTwo, distToNextPlane);

        outIndexesObbs[writePoint] = uvec2(indexOBB, keyOne);
        outIndexesObbs[writePoint + 1] = uvec2(indexOBB, keyTwo);
        return;
    }
    else if(countContacts == 1)
    {
        uint key = key << 1;
        key |= contactSides.x; // |= 1 if x true

        addOBBsToNode(key, distToNextPlane);

        outIndexesObbs[writePoint] = uvec2(indexOBB, key);
        return;
    }
}