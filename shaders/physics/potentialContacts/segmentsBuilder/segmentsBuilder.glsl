#include "buffers.glsl"
#include "math.glsl"
#include "limits.glsl"

//images       
layout(r32i, set = 0, binding = 0) uniform iimage2D headPointsInput;         

layout(r32ui, set = 0, binding = 1) uniform uimage1D segmentsCounter;
layout(r32ui, set = 0, binding = 2) uniform uimage1D indexesCounter;

STORAGE_BUFFER_R(0, 3, inputListNodes, ivec2, inNodes)
STORAGE_BUFFER_W(0, 4, outputSegments, uvec2, outSegments)
STORAGE_BUFFER_W(0, 5, outputIndexes, uint, outIndexes)

layout (local_size_x = 2, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    uint readIndexes[KD_TREE_MAX_COUNT_INDEXES_PER_SEGMENT];
    uint countIndexes = 0;

    int startIndex = int(imageLoad(headPointsInput, ivec2(gl_GlobalInvocationID.xy)).x);
    
    if(startIndex == -1)
        return;
    
    int curIndex = startIndex;
    for(int i = 0; i < KD_TREE_MAX_COUNT_INDEXES_PER_SEGMENT; i++)
    {
        if(curIndex == -1)
            break;
        
        ivec2 data = inNodes[curIndex];
        readIndexes[countIndexes] = uint(data.x);
        countIndexes++;

        curIndex = data.y;
    }

    if(countIndexes <= 1)
        return;

    uint pointWriteSegment = uint(imageAtomicAdd(segmentsCounter, 0, 1).x);
    uint pointWriteIndexes = uint(imageAtomicAdd(indexesCounter, 0, countIndexes).x);

    outSegments[pointWriteSegment] = uvec2(pointWriteIndexes, countIndexes);
    
    for(int i = 0; i < countIndexes && i < KD_TREE_MAX_COUNT_INDEXES_PER_SEGMENT; i++)
    {
        uint indexWrite = i + pointWriteIndexes;
        outIndexes[indexWrite] = readIndexes[i];
    }
}