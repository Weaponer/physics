#include "buffers.glsl"
#include "limits.glsl"

//images       
layout(r32i, set = 0, binding = 0) uniform iimage2D headPointsInput;   

layout(r32ui, set = 0, binding = 1) uniform uimage2DArray statisticsOutput; //layer 0 - count contacts, layer 1 - count mentions
layout(r32i, set = 0, binding = 2) uniform iimage2DArray tableContacts;

//storage buffers
STORAGE_BUFFER_R(0, 3, inputContactNodes, ivec2, inContactNodes)

layout (local_size_x = 4, local_size_y = 4, local_size_z = 1) in;
void main()
{  
    ivec2 size = imageSize(headPointsInput).xy;
    if(gl_GlobalInvocationID.x >= size.x || gl_GlobalInvocationID.y >= size.y)
        return;
   
    uint indexOBB = gl_GlobalInvocationID.y * size.x + gl_GlobalInvocationID.x;

    int startIndex = int(imageLoad(headPointsInput, ivec2(gl_GlobalInvocationID.xy)).x);
    
    if(startIndex == -1)
        return;

    ivec3 sizeTable = imageSize(tableContacts).xyz;
    ivec3 coordOBB = ivec3(indexOBB % sizeTable.x, 0, indexOBB / sizeTable.x);

    uint countContacts = 0;
    int contacts[MAX_CONTACTS_WITH_OBB];

    for(int i = 0; i < MAX_CONTACTS_WITH_OBB; i++)
        contacts[i] = -1;

    int readIndex = startIndex;
    while(readIndex != -1)
    {
        ivec2 data = inContactNodes[readIndex];
        int indexOBB = data.x;

        for(int i = 0; i < MAX_CONTACTS_WITH_OBB; i++)
        {
            if(contacts[i] == indexOBB)
                break;
            
            if(contacts[i] == -1)
            {
                contacts[i] = indexOBB;
                countContacts++;
                break;
            }
        }

        if(countContacts == MAX_CONTACTS_WITH_OBB || data.y == -1)
            break;
        
        readIndex = data.y;
    }   

    if(countContacts == 0)
        return;
    
    imageAtomicAdd(statisticsOutput, ivec3(gl_GlobalInvocationID.xy, 0), countContacts);
    imageAtomicAdd(statisticsOutput, ivec3(gl_GlobalInvocationID.xy, 1), countContacts);

    for(int i = 0; i < countContacts; i++)
    {   
        ivec3 c = coordOBB;
        c.y = i;

        int indexContactOBB = contacts[i];
        imageStore(tableContacts, c, ivec4(indexContactOBB));

        ivec2 coordContactOBB = ivec2(indexContactOBB % size.x, indexContactOBB / size.x);
        imageAtomicAdd(statisticsOutput, ivec3(coordContactOBB, 1), 1);
    }
}