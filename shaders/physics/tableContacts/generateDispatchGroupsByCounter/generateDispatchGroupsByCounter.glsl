#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"

layout(r32ui, set = 0, binding = 0) uniform uimage1D counter;

//storage buffers
STORAGE_BUFFER_W(0, 1, outputGroups, ivec3, outGroups)

layout(push_constant, std430) uniform Constants
{
    uint sizeGroup;
} pConstants;


layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    int index = int(gl_GlobalInvocationID.x);
    uint countWrote = uint(imageLoad(counter, index));

    uint countGroups = countWrote / pConstants.sizeGroup;
    if((countWrote % pConstants.sizeGroup) != 0)
        countGroups += 1;
    
    outGroups[index] = ivec3(countGroups, 1, 1);
}