#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "tableContactsUtils.glsl"

layout(r32ui, set = 0, binding = 0) uniform uimage1D copyContainersCounter;
layout(r32ui, set = 0, binding = 1) uniform uimage1D typesContactsCounter;

//storage buffers

layout(std430, set = 0, binding = 2) writeonly buffer copyContainersIndirectBuffer
{
    uvec3 copyContainersGroups;
};

layout(std430, set = 0, binding = 3) writeonly buffer typesContactsContainersBuffer
{
    uvec4 containersData[MAX_COUNT_TYPE_CONTACTS];
};

layout(std430, set = 0, binding = 4) writeonly buffer updateContainersIndirectsBuffer
{
    uvec4 updateContainersGroups[MAX_COUNT_TYPE_CONTACTS];
};

layout(push_constant, std430) uniform Constants
{
    uint contactsTypesCount;
    uint countContactsInTypes[MAX_COUNT_TYPE_CONTACTS];
} pConstants;

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
void main()
{
    {
        uint copyContainersCount = imageLoad(copyContainersCounter, 0).x;
        if(copyContainersCount % SIZE_GROUP_CHECK_GEOMETRY_CONTACT != 0)
            copyContainersGroups = uvec3(copyContainersCount / SIZE_GROUP_CHECK_GEOMETRY_CONTACT + 1, 1, 1);
        else
            copyContainersGroups = uvec3(copyContainersCount / SIZE_GROUP_CHECK_GEOMETRY_CONTACT, 1, 1);
    }

    uint countsContainersPerType[MAX_COUNT_TYPE_CONTACTS];
    {
        uint offsetContacts = 0;
        uint offsetContainers = 0;
        for(int i = 0; i < pConstants.contactsTypesCount; i++)
        {
            int pReadCountTypeContainer = i;
            uvec4 containersTypeData = uvec4(0, 
                                      imageLoad(typesContactsCounter, pReadCountTypeContainer).x, 0, 0);
                                      
            containersTypeData.x = offsetContainers;
            offsetContainers += containersTypeData.y;

            containersTypeData.z = offsetContacts;
            containersTypeData.w = containersTypeData.y * pConstants.countContactsInTypes[i];
            offsetContacts += containersTypeData.w;


            containersData[i] = containersTypeData;

            countsContainersPerType[i] = containersTypeData.y;
        }
    }
    {
        for(int i = 0; i < pConstants.contactsTypesCount; i++)
        {
            if(countsContainersPerType[i] % SIZE_GROUP_CHECK_GEOMETRY_CONTACT != 0)
                updateContainersGroups[i] = uvec4(countsContainersPerType[i] / SIZE_GROUP_CHECK_GEOMETRY_CONTACT + 1, 1, 1, 0);
            else
                updateContainersGroups[i] = uvec4(countsContainersPerType[i] / SIZE_GROUP_CHECK_GEOMETRY_CONTACT, 1, 1, 0);
        }
    }
}