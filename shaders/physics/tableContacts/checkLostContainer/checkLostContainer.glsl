#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "tableContactsUtils.glsl"

STORAGE_BUFFER_R(0, 0, inputContainerCopied, uint, inContainerCopied)
STORAGE_BUFFER_R(0, 1, inputLastContactsContainers, DataContactsContainer, inLastContactsContainers)
GEOMETRY_OBBS_DATA(0, 2)

STORAGE_BUFFER_W(0, 3, outputActorLostContainer, uint, outActorLostContainer) //output

layout(push_constant, std430) uniform Constants
{
    uint maxCountContainers;
} pConstants;

layout (local_size_x = SIZE_GROUP_CHECK_GEOMETRY_CONTACT, local_size_y = 1, local_size_z = 1) in;
void main()
{
    if(gl_GlobalInvocationID.x >= pConstants.maxCountContainers)
        return;

    uint index  = gl_GlobalInvocationID.x;
    if(inContainerCopied[index] != 0)
        return;

    DataContactsContainer container = inLastContactsContainers[index];
    DataGeometryOBB obb_1 = geometryObbs[container.firstIndexGeometry];
    DataGeometryOBB obb_2 = geometryObbs[container.secondIndexGeometry];

    if((obb_1.flags & FLAG_GEOMETRY_FLAGS_MASK) == FLAG_GEOMETRY_DYNAMIC)
        outActorLostContainer[obb_1.indexActor] = 1;

    if((obb_2.flags & FLAG_GEOMETRY_FLAGS_MASK) == FLAG_GEOMETRY_DYNAMIC)
        outActorLostContainer[obb_2.indexActor] = 1;
}