#include "buffers.glsl"
#include "math.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "tableContactsUtils.glsl"

#define EPSILON 0.00000001

//image storage
layout(r32ui, set = 0, binding = 0) uniform uimage1D segmentsCounter;

layout(r32ui, set = 0, binding = 1) uniform uimage1D headPointsCounter;
layout(r32i, set = 0, binding = 2) uniform iimage2D headPointsOutput;         

//storage buffers
STORAGE_BUFFER_R(0, 3, inputSegments, uvec2, inSegments)
STORAGE_BUFFER_R(0, 4, inputIndexes, uint, inIndexes)

STORAGE_BUFFER_R(0, 5, swapIndexesData, uint, swapIndexes)

GEOMETRY_OBBS_DATA(0, 6)
ORIENTATION_GEOMETRY_OBBS_DATA_R(0, 7)

STORAGE_BUFFER_W(0, 8, outputContactNodes, ivec2, outContactNodes)

layout(push_constant, std430) uniform Constants
{
    uint countOutputNodes;
} pConstants;


#include "potentialContactsUtils.glsl"

layout (local_size_x = 32, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    uint countSegments = uint(imageLoad(segmentsCounter, 0).x);
    
    if(gl_GlobalInvocationID.x >= countSegments)
        return;

    uvec2 segmentsData = inSegments[gl_GlobalInvocationID.x];
    uint startSegment = segmentsData.x;
    uint sizeSegment = segmentsData.y;

    uint testIndexes[KD_TREE_MAX_COUNT_INDEXES_PER_SEGMENT];
    for(uint i = 0; i < sizeSegment && i < KD_TREE_MAX_COUNT_INDEXES_PER_SEGMENT; i++)
    {
        uint pRead = startSegment + i;
        testIndexes[i] = inIndexes[pRead];
    }

    
    // test OBBs and write contacts

    uint maxI = sizeSegment - 1;
    for(uint i = 0; i < maxI && i < (KD_TREE_MAX_COUNT_INDEXES_PER_SEGMENT - 1); i++)
    {
        uint indexOne = testIndexes[i];

        DataGeometryOBB dataOBB = geometryObbs[indexOne];
        DataOrientationGeometryOBB orientationOBB = orientationObbs[indexOne];

        for(uint i2 = i + 1; i2 < sizeSegment && i2 < KD_TREE_MAX_COUNT_INDEXES_PER_SEGMENT; i2++)
        {
            uint indexTwo = testIndexes[i2];

            DataGeometryOBB dataOBB_2 = geometryObbs[indexTwo];
            if(!checkDataOBBs(dataOBB, dataOBB_2))
                continue;

            DataOrientationGeometryOBB orientationOBB_2 = orientationObbs[indexTwo];
            if(!checkOrientationOBBs(orientationOBB, orientationOBB_2))
                continue;

            uint writePoint = uint(imageAtomicAdd(headPointsCounter, 0, 1));
            if(writePoint >= pConstants.countOutputNodes)
                return;

            writeContact(writePoint, indexOne, indexTwo);
        }
    }
}