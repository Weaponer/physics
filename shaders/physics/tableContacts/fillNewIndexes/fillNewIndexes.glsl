#include "buffers.glsl"
#include "limits.glsl"

//images       
layout(r32i, set = 0, binding = 0) uniform iimage2DArray tableContacts;         

//storage buffers
STORAGE_BUFFER_W(0, 1, outputSwapIndexes_1, uint, outSwapIndexes_1)
STORAGE_BUFFER_W(0, 2, outputSwapIndexes_2, uint, outSwapIndexes_2)

layout(push_constant, std430) uniform Constants
{
    uint startNewIndexes;
    uint countNewIndexes;
} pConstants;

layout (local_size_x = 32, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    if(gl_GlobalInvocationID.x >= pConstants.countNewIndexes)
        return;

    uint index = gl_GlobalInvocationID.x + pConstants.startNewIndexes;
    
    ivec3 size = imageSize(tableContacts);
    uint layer = index / size.x;
    uint width = index % size.x;

    // write data
    outSwapIndexes_1[index] = 0;
    outSwapIndexes_2[index] = 0;

    for(int i = 0; i < size.y; i++)
        imageStore(tableContacts, ivec3(width, i, layer), ivec4(-1));
}