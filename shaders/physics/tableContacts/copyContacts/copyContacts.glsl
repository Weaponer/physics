#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "tableContactsUtils.glsl"

layout(r32ui, set = 0, binding = 0) uniform uimage1D copyContainersCounter;

//storage buffers

STORAGE_BUFFER_R(0, 1, inputCopyContainers, ivec2, inCopyContainers)

STORAGE_BUFFER_R(0, 2, inputLastContactsContainers, DataContactsContainer, inLastContactsContainers)
STORAGE_BUFFER_R(0, 3, inputCurrentContactsContainers, DataContactsContainer, inCurrentContactsContainers)
STORAGE_BUFFER_R(0, 4, inputLastContactsData, DataContact, inLastContacts)
STORAGE_BUFFER_W(0, 5, outputCurrentContactsData, DataContact, outCurrentContacts) //output

layout (local_size_x = SIZE_GROUP_CHECK_GEOMETRY_CONTACT, local_size_y = 1, local_size_z = 1) in;
void main()
{
    {
        uint maxCount = imageLoad(copyContainersCounter, 0).x;
        if(gl_GlobalInvocationID.x >= maxCount)
            return;
    }

    ivec2 copyData = inCopyContainers[gl_GlobalInvocationID.x];
    DataContactsContainer srcContainer = inLastContactsContainers[copyData.y];
    DataContactsContainer dstContainer = inCurrentContactsContainers[copyData.x];

    uint pRead = srcContainer.firstContactIndex;
    uint pWrite = dstContainer.firstContactIndex;

    uint countAndMaskContacts = dstContainer.countAndMaskContacts;

    uint countContacts = countAndMaskContacts & CONTACTS_CONTAINER_MASK_COUNT_CONTACTS;
    uint maskUseContacts = (countAndMaskContacts >> CONTACTS_CONTAINER_MASK_OFFSET_BITS_USE_CONTACTS) & CONTACTS_CONTAINER_MASK_USE_CONTACTS;
    
    for(uint i = 0; i < countContacts && i < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS; i++)
    {
        if((maskUseContacts & (1 << i)) != 0)
        {
            DataContact srcContact = inLastContacts[pRead + i];
            outCurrentContacts[pWrite + i] = srcContact;
        }
    }
}