#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "tableContactsUtils.glsl"

layout(r32ui, set = 0, binding = 0) uniform uimage1D contactPairsCounter;

layout(r32i, set = 0, binding = 1) uniform iimage2DArray containersTable;   // output

layout(r32ui, set = 0, binding = 2) uniform uimage1D containersCounter;     //output
layout(r32ui, set = 0, binding = 3) uniform uimage1D contactsCounter;       //output
layout(r32ui, set = 0, binding = 4) uniform uimage1D typesContactsCounter;  //output
layout(r32ui, set = 0, binding = 5) uniform uimage1D copyContainersCounter; //output

//storage buffers

GEOMETRY_OBBS_DATA(0, 6)
ACTORS_DATA(0, 7)
SLEEPING_ACTORS_DATA_R(0, 8)

STORAGE_BUFFER_R(0, 9, inputExchangeContainer, ivec4, inExchangeContainer)

STORAGE_BUFFER_R(0, 10, inputLastContactsContainers, DataContactsContainer, inLastContactsContainers)

STORAGE_BUFFER_W(0, 11, outputContainerCopied, uint, outContainerCopied)                                        //output
STORAGE_BUFFER_W(0, 12, outputCurrentContactsContainers, DataContactsContainer, outCurrentContactsContainers)   //output

STORAGE_BUFFER_W(0, 13, outputCopyContainers, ivec2, outCopyContainers)                                         //output

layout(push_constant, std430) uniform Constants
{
    int maxCountExchangeContainers;
    int maxCountNewContainers;
    int maxCountNewContacts;
    uint type;
    uint countContacts;
    uint indexType;
} pConstants;

layout (local_size_x = SIZE_GROUP_CHECK_GEOMETRY_CONTACT, local_size_y = 1, local_size_z = 1) in;
void main()
{
    int countExchangeContainers = int(imageLoad(contactPairsCounter, 0).x);
    if(gl_GlobalInvocationID.x >= countExchangeContainers || gl_GlobalInvocationID.x >= pConstants.maxCountExchangeContainers)
        return;

    ivec4 exchangeData = inExchangeContainer[gl_GlobalInvocationID.x];

    DataGeometryOBB dataFirst = geometryObbs[exchangeData.x];
    DataGeometryOBB dataSecond = geometryObbs[exchangeData.y];

    uint flagFirst = dataFirst.flags & FLAG_GEOMETRY_TYPE_MASK;
    uint flagSecond = dataSecond.flags & FLAG_GEOMETRY_TYPE_MASK;

    bool swap = false;
    uint type = 0;
    if(flagFirst > flagSecond)
    {
        swap = true;
        type = flagFirst | (flagSecond << FLAG_GEOMETRY_TYPE_COUNT_BITS);
    }
    else
    {
        type = flagSecond | (flagFirst << FLAG_GEOMETRY_TYPE_COUNT_BITS);
    }

    if(type != pConstants.type)
        return;

    int indexNewContainer = int(imageAtomicAdd(containersCounter, 0, 1)); 
    if(indexNewContainer >= pConstants.maxCountNewContainers)
        return;
    
    uint indexNewContacts = uint(imageAtomicAdd(contactsCounter, 0, pConstants.countContacts));
    if((indexNewContacts + pConstants.countContacts) > pConstants.maxCountNewContacts)
    {
        DataContactsContainer zeroContainer;
        zeroContainer.firstContactIndex = 0;
        zeroContainer.firstIndexGeometry = 0;
        zeroContainer.secondIndexGeometry = 0;
        zeroContainer.countAndMaskContacts = 0;
        outCurrentContactsContainers[indexNewContainer] = zeroContainer;
        return;
    }

    uint sleepContainer = 1;
    DataActor actor = actors[dataFirst.indexActor];
    if((actor.flags & FLAG_ACTOR_DYNAMIC) != 0)
        sleepContainer &= sleepingActors[actor.indexAdditionalData].isSleeping;

    actor = actors[dataSecond.indexActor];
    if((actor.flags & FLAG_ACTOR_DYNAMIC) != 0)
        sleepContainer &= sleepingActors[actor.indexAdditionalData].isSleeping;

    //write type statistic
    {
        imageAtomicAdd(typesContactsCounter, int(pConstants.indexType), 1);
    }

    // save index of new container into table
    {
        ivec3 size = imageSize(containersTable).xyz;
        ivec3 coord = ivec3(exchangeData.x % size.x, exchangeData.z, exchangeData.x / size.x);

        imageStore(containersTable, coord, ivec4(indexNewContainer));
    }

    if(exchangeData.w != -1)    //write copy container
    {
        uint pWriteCopy = imageAtomicAdd(copyContainersCounter, 0, 1);
        outCopyContainers[pWriteCopy] = ivec2(indexNewContainer, exchangeData.w);
    }

    // write container data
    {
        DataContactsContainer newContainer;
        newContainer.firstContactIndex = indexNewContacts;
        newContainer.firstIndexGeometry = exchangeData.x;
        newContainer.secondIndexGeometry = exchangeData.y;

        if(swap)
        {
            uint temp = newContainer.firstIndexGeometry;
            newContainer.firstIndexGeometry = newContainer.secondIndexGeometry;
            newContainer.secondIndexGeometry = temp;
        }
        

        bool isNewContainer = false;
        if(exchangeData.w != -1)    
        {
            DataContactsContainer lastContainer = inLastContactsContainers[exchangeData.w];
            outContainerCopied[exchangeData.w] = 1;

            newContainer.firstIndexGeometry = lastContainer.firstIndexGeometry;
            newContainer.secondIndexGeometry = lastContainer.secondIndexGeometry;

            uint lastCountData = lastContainer.countAndMaskContacts;
            if(pConstants.countContacts == (lastCountData & CONTACTS_CONTAINER_MASK_COUNT_CONTACTS))
            {
                newContainer.countAndMaskContacts = lastCountData;
            }
            else
            {
                newContainer.countAndMaskContacts = pConstants.countContacts;
                isNewContainer = true;
            }
        }
        else
        {
            newContainer.countAndMaskContacts = pConstants.countContacts;
            isNewContainer = true;
        }

        if(sleepContainer != 0)
            newContainer.countAndMaskContacts |= (1 << CONTACTS_CONTAINER_MASK_OFFSET_BITS_SLEEP_ACTORS);
        else
            newContainer.countAndMaskContacts &= (~(1 << CONTACTS_CONTAINER_MASK_OFFSET_BITS_SLEEP_ACTORS));

        if(isNewContainer)
            newContainer.countAndMaskContacts |= (1 << CONTACTS_CONTAINER_MASK_OFFSET_BITS_NEW);
        else
            newContainer.countAndMaskContacts &= (~(1 << CONTACTS_CONTAINER_MASK_OFFSET_BITS_NEW));

        outCurrentContactsContainers[indexNewContainer] = newContainer;
    }
}