#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "math.glsl"
#include "checkContact.glsl"

layout(r32ui, set = 0, binding = 0) uniform uimage1D contactPairsCounter;
layout(r32i, set = 0, binding = 1) uniform iimage2DArray activeContactsTable;

//storage buffers
STORAGE_BUFFER_R(0, 2, checkGeometriesBuffer, ivec4, checkGeometries)

ORIENTATION_GEOMETRY_OBBS_DATA_R(0, 3)
GEOMETRY_OBBS_DATA(0, 4)
GEOMETRIES_DATA(0, 5)

layout(push_constant, std430) uniform Constants
{
    uint maxSizeCheckBuffer;
    uint indexTypeContact;
} pConstants;


layout (local_size_x = SIZE_GROUP_CHECK_GEOMETRY_CONTACT, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    int maxCount = min(int(pConstants.maxSizeCheckBuffer), int(imageLoad(contactPairsCounter, int(pConstants.indexTypeContact)).x));
    if(gl_GlobalInvocationID.x >= maxCount)
        return;

    ivec4 data = checkGeometries[gl_GlobalInvocationID.x];
    int firstGeometry = data.x;
    int secondGeometry = data.y;
    int indexContact = data.z;
    int numContact = data.w;
    
    DataGeometryOBB firstDataOBB = geometryObbs[firstGeometry];
    DataGeometryOBB secondDataOBB = geometryObbs[secondGeometry];

    DataGeometry firstData = geometries[firstDataOBB.indexMainGeometry];
    DataGeometry secondData = geometries[secondDataOBB.indexMainGeometry];
    DataOrientationGeometryOBB firstOrientation = orientationObbs[firstGeometry];
    DataOrientationGeometryOBB secondOrientation = orientationObbs[secondGeometry];

    bool hasContact = true;
    
    #ifdef PLANE_SPHERE
        hasContact = checkContactPlaneSphere(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef PLANE_CAPSULE
        hasContact = checkContactPlaneCapsule(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef BOX_SPHERE
        hasContact = checkContactBoxSphere(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef BOX_CAPSULE
        hasContact = checkContactBoxCapsule(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef SPHERE_SPHERE
        hasContact = checkContactSphereSphere(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef SPHERE_CAPSULE
        hasContact = checkContactSphereCapsule(firstData, firstOrientation, secondData, secondOrientation);
    #endif
    #ifdef CAPSULE_CAPSULE
        hasContact = checkContactCapsuleCapsule(firstData, firstOrientation, secondData, secondOrientation);
    #endif

    if(!hasContact)
        return;
    
    //write active contact
    int activeMask = 1 << numContact;
    ivec3 size = imageSize(activeContactsTable).xyz;
    ivec3 coord = ivec3(indexContact % size.x, 0, indexContact / size.x);
    imageAtomicOr(activeContactsTable, coord, activeMask);
}