#include "buffers.glsl"
#include "math.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "tableContactsUtils.glsl"

//image storage
layout(r32ui, set = 0, binding = 0) uniform uimage1D headPointsCounter;
layout(r32i, set = 0, binding = 1) uniform iimage2D headPointsOutput;         

//storage buffers
STORAGE_BUFFER_R(0, 2, inputLastContactsContainers, DataContactsContainer, inLastContactsContainers)
STORAGE_BUFFER_R(0, 3, swapIndexesData, uint, swapIndexes)

GEOMETRY_OBBS_DATA(0, 4)
ORIENTATION_GEOMETRY_OBBS_DATA_R(0, 5)

STORAGE_BUFFER_W(0, 6, outputContactNodes, ivec2, outContactNodes)

layout(push_constant, std430) uniform Constants
{
    uint countInputContainers;
    uint countOutputNodes;
} pConstants;

#include "potentialContactsUtils.glsl"

layout (local_size_x = 32, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    if(gl_GlobalInvocationID.x >= pConstants.countInputContainers)
        return;

    DataContactsContainer container = inLastContactsContainers[gl_GlobalInvocationID.x];

    uint indexOne = container.firstIndexGeometry;
    uint indexTwo = container.secondIndexGeometry;

    DataGeometryOBB dataOBB = geometryObbs[indexOne];
    DataGeometryOBB dataOBB_2 = geometryObbs[indexTwo];
    
    if(dataOBB.flags == 0 || dataOBB_2.flags == 0)
        return;

    if(!checkDataOBBs(dataOBB, dataOBB_2))
        return;

    DataOrientationGeometryOBB orientationOBB = orientationObbs[indexOne];
    DataOrientationGeometryOBB orientationOBB_2 = orientationObbs[indexTwo];
    if(!checkOrientationOBBs(orientationOBB, orientationOBB_2))
        return;

    uint writePoint = uint(imageAtomicAdd(headPointsCounter, 0, 1));
    if(writePoint >= pConstants.countOutputNodes)
        return;

    writeContact(writePoint, indexOne, indexTwo);
}