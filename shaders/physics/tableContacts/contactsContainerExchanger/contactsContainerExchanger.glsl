#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "tableContactsUtils.glsl"

layout(r32i, set = 0, binding = 0) uniform iimage2DArray currentContactsTable;
layout(r32i, set = 0, binding = 1) uniform iimage2DArray lastContactsTable;

layout(r32i, set = 0, binding = 2) uniform iimage2DArray lastContainersTable;

layout(r32i, set = 0, binding = 3) uniform iimage2DArray activeContactsTable;
layout(r32ui, set = 0, binding = 4) uniform uimage1D contactPairsCounter;   //output

//storage buffers
STORAGE_BUFFER_R(0, 5, inputLastSwapIndexes, uint, inLastSwapIndexes)
STORAGE_BUFFER_W(0, 6, outputExchangeContainers, ivec4, outExchangeContainers) // output

layout(push_constant, std430) uniform Constants
{
    uint maxIndexOBBs;
    uint maxCountContainers;
} pConstants;

layout (local_size_x = SIZE_GROUP_CHECK_GEOMETRY_CONTACT, local_size_y = MAX_CONTACTS_WITH_OBB, local_size_z = 1) in;
void main()
{
    if(gl_GlobalInvocationID.x > pConstants.maxIndexOBBs)
        return;
    
    uint index = gl_GlobalInvocationID.x;
    uint indexY = gl_GlobalInvocationID.y;
    ivec3 size = ivec3(imageSize(activeContactsTable).xyz);

    ivec3 coord = ivec3(index % size.x, 0, index / size.x);  
    bool isActiveContact = (int(imageLoad(activeContactsTable, coord).x) & (1 << indexY)) != 0;

    if(!isActiveContact)
        return;
    
    coord.y = int(indexY);

    uint firstContactOBB = uint(index);
    uint secondContactOBB = uint(imageLoad(currentContactsTable, coord).x);
    
    uint priorityOne = inLastSwapIndexes[index];
    uint priorityTwo = inLastSwapIndexes[secondContactOBB];

    bool needSwap = isNeedSwapIndexes(firstContactOBB, secondContactOBB, priorityOne, priorityTwo);
    
    uint firstMainOBB = firstContactOBB;
    uint secondMainOBB = secondContactOBB;

    if(needSwap)
    {
        uint temp = secondContactOBB;
        secondContactOBB = firstContactOBB;
        firstContactOBB = temp;
    }

    ivec3 lastCoord = ivec3(firstContactOBB % size.x, 0, firstContactOBB / size.x);
    int containerCoordY = -1;
    for(int i = 0; i < MAX_CONTACTS_WITH_OBB && containerCoordY == -1; i++)
    {
        lastCoord.y = i;
        int checkSecondOBB = imageLoad(lastContactsTable, lastCoord).x;

        if(checkSecondOBB == secondContactOBB)
        {
            containerCoordY = i;
            break;
        }
    }
    
    int containerIndex = -1;

    if(containerCoordY != -1)
    {
        ivec3 coordContainer = ivec3(lastCoord.x, containerCoordY, lastCoord.z);
        containerIndex = imageLoad(lastContainersTable, coordContainer).x;
    }

    //containerIndex = -1;

    uint pWrite = imageAtomicAdd(contactPairsCounter, 0, 1);

    if(pWrite >= pConstants.maxCountContainers)
        return;

    ivec4 writeData = ivec4(firstMainOBB, secondMainOBB, indexY, containerIndex);
    outExchangeContainers[pWrite] = writeData;
}