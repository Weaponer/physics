#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"

layout(r32ui, set = 0, binding = 0) uniform uimage1D contactPairsCounter;

layout(r32i, set = 0, binding = 1) uniform iimage2DArray tableContacts;

//storage buffers
GEOMETRY_OBBS_DATA(0, 2)

// output x - first test geomtery, y - second test geometry, z - index contact, w - num contact
STORAGE_BUFFER_W(0, 3, outputBuffer_1, ivec4, outBuffer_1)  
STORAGE_BUFFER_W(0, 4, outputBuffer_2, ivec4, outBuffer_2)
STORAGE_BUFFER_W(0, 5, outputBuffer_3, ivec4, outBuffer_3)
STORAGE_BUFFER_W(0, 6, outputBuffer_4, ivec4, outBuffer_4)

layout(push_constant, std430) uniform Constants
{
    uint maxIndexOBBs;
    uint maxSizeBuffers[4];
    uint typeBuffers[4];
    int indexCounters[4];
} pConstants;

layout (local_size_x = SIZE_GROUP_CHECK_GEOMETRY_CONTACT, local_size_y = MAX_CONTACTS_WITH_OBB, local_size_z = 1) in;
void main()
{  
    if(gl_GlobalInvocationID.x > pConstants.maxIndexOBBs)
        return;

    uint index = gl_GlobalInvocationID.x;

    ivec3 size = imageSize(tableContacts).xyz;

    ivec3 coord = ivec3(index % size.x, gl_GlobalInvocationID.y, index / size.x);

    int indexSecondOBBs = int(imageLoad(tableContacts, coord));
    
    if(indexSecondOBBs == -1)
        return;
    
    DataGeometryOBB dataFirst = geometryObbs[index];
    DataGeometryOBB dataSecond = geometryObbs[indexSecondOBBs];

    uint flagFirst = dataFirst.flags & FLAG_GEOMETRY_TYPE_MASK;
    uint flagSecond = dataSecond.flags & FLAG_GEOMETRY_TYPE_MASK;

    bool swap = false;
    uint type = 0;
    if(flagFirst > flagSecond)
    {
        swap = true;
        type = flagFirst | (flagSecond << FLAG_GEOMETRY_TYPE_COUNT_BITS);
    }
    else
    {
        type = flagSecond | (flagFirst << FLAG_GEOMETRY_TYPE_COUNT_BITS);
    }

    int indexType = -1;
    int indexCounter = -1;
    uint pointWrite = 0;
    for(int i = 0; i < 4; i++)
    {
        if(pConstants.typeBuffers[i] == type)
        {
            indexType = i;
            indexCounter = pConstants.indexCounters[i];
            pointWrite = uint(imageAtomicAdd(contactPairsCounter, indexCounter, 1u));
            break;
        }
    }

    if(indexType == -1)
        return;
    
    if(pointWrite >= pConstants.maxSizeBuffers[indexType])
        return;

    int indexFirstOBBs = int(index);
    if(swap)
    {
        int temp = indexFirstOBBs;
        indexFirstOBBs = indexSecondOBBs;
        indexSecondOBBs = temp;
    }

    ivec4 dataWrite = ivec4(indexFirstOBBs, indexSecondOBBs, index, gl_GlobalInvocationID.y);
    switch(indexType)
    {
        case 0:
        {
            outBuffer_1[pointWrite] = dataWrite;
            return;
        }
        case 1:
        {
            outBuffer_2[pointWrite] = dataWrite;
            return;
        }
        case 2:
        {
            outBuffer_3[pointWrite] = dataWrite;
            return;
        }
        case 3:
        {
            outBuffer_4[pointWrite] = dataWrite;
            return;
        }
    }
}