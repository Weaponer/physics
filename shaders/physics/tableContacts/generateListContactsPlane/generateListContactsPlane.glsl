#include "buffers.glsl"
#include "math.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "tableContactsUtils.glsl"

//image storage
layout(r32ui, set = 0, binding = 0) uniform uimage1D headPointsCounter;
layout(r32i, set = 0, binding = 1) uniform iimage2D headPointsOutput;         

//storage buffers
STORAGE_BUFFER_R(0, 2, inputIndexesTest, uint, indexesTest)
STORAGE_BUFFER_R(0, 3, swapIndexesData, uint, swapIndexes)

GEOMETRY_OBBS_DATA(0, 4)
ORIENTATION_GEOMETRY_OBBS_DATA_R(0, 5)

STORAGE_BUFFER_W(0, 6, outputContactNodes, ivec2, outContactNodes)

layout(push_constant, std430) uniform Constants
{
    uint indexPlane;
    uint countInputIndexes;
    uint countOutputNodes;
} pConstants;

#include "potentialContactsUtils.glsl"

bool checkPlaneOBB(vec4 plane, vec3 position, mat3 rotation, vec3 size)
{
    float r = size.x * abs(dot(plane.xyz, rotation[0])) +
              size.y * abs(dot(plane.xyz, rotation[1])) +
              size.z * abs(dot(plane.xyz, rotation[2]));

    float d = dot(plane.xyz, position) - plane.w;
    
    if(d <= r)
        return true;
    return false;
}

layout (local_size_x = 32, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    if(gl_GlobalInvocationID.x >= pConstants.countInputIndexes)
        return;

    uint indexOne = indexesTest[gl_GlobalInvocationID.x];
    uint indexTwo = pConstants.indexPlane;

    DataGeometryOBB dataOBB = geometryObbs[indexOne];
    DataGeometryOBB dataOBB_2 = geometryObbs[indexTwo];
    
    if(dataOBB.flags == 0 || dataOBB_2.flags == 0)
        return;

    if(!checkDataOBBs(dataOBB, dataOBB_2))
        return;

    DataOrientationGeometryOBB orientationOBB = orientationObbs[indexOne];
    DataOrientationGeometryOBB orientationOBB_2 = orientationObbs[indexTwo];

    mat3 rotPlane = transpose(quaternionToMat3(orientationOBB_2.rotation));
    vec3 upDirect = rotPlane[1].xyz;
    vec3 posPlane = orientationOBB_2.position.xyz;
    float planeOffset = dot(posPlane, upDirect);
    

    if(!checkPlaneOBB(vec4(upDirect, planeOffset), orientationOBB.position.xyz, 
                        quaternionToMat3(orientationOBB.rotation), orientationOBB.size.xyz))
        return;

    uint writePoint = uint(imageAtomicAdd(headPointsCounter, 0, 1));
    if(writePoint >= pConstants.countOutputNodes)
        return;

    writeContact(writePoint, indexOne, indexTwo);
}