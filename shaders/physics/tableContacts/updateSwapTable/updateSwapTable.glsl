#include "buffers.glsl"
#include "limits.glsl"

layout(r32ui, set = 0, binding = 0) uniform uimage2DArray statisticsOutput; //layer 0 - count contacts, layer 1 - count mentions

//storage buffers
STORAGE_BUFFER_W(0, 1, outputSwapIndexes, uint, outSwapIndexes)

layout(push_constant, std430) uniform Constants
{
    uint countSwapIndexes;
} pConstants;

layout (local_size_x = 32, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    if(gl_GlobalInvocationID.x >= pConstants.countSwapIndexes)
        return;

    uint index = gl_GlobalInvocationID.x;

    ivec3 size = imageSize(statisticsOutput).xyz;

    ivec3 coord = ivec3(index % size.x, index / size.x, 0);

    uint countContacts = uint(imageLoad(statisticsOutput, coord).x);
    uint countMentions = uint(imageLoad(statisticsOutput, ivec3(coord.xy, 1)).x);

    uint result = max(countContacts * 10, countMentions * 6);
    outSwapIndexes[index] = result;
}