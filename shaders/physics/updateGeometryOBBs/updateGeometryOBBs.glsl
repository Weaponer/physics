#include "math.glsl"
#include "buffers.glsl"

STORAGE_BUFFER_R(0, 0, inputIndexesUpdate, uint, indexesUpdate)

ACTOR_ORIENTAIONS_DATA_R(0, 1)

GEOMETRIES_DATA(0, 2)

GEOMETRY_OBBS_DATA(0, 3)
ORIENTATION_GEOMETRY_OBBS_DATA_W(0, 4)

layout(push_constant, std430) uniform Constants
{
    uint countUpdateOBBs;
} pConstants;

layout (local_size_x = 16, local_size_y = 1, local_size_z = 1) in;
void main()
{
    if(pConstants.countUpdateOBBs <= gl_GlobalInvocationID.x)
        return;

    uint curIndex = indexesUpdate[gl_GlobalInvocationID.x];

    DataGeometryOBB dataObb = geometryObbs[curIndex];
    uint indexActor = dataObb.indexActor;
    uint indexMainGeometry = dataObb.indexMainGeometry;

    DataActorOrientation actorOrientation = actorOrientations[indexActor];
    DataGeometry mainGeometry = geometries[indexMainGeometry];

    DataOrientationGeometryOBB orient;
    orient.position = vec4(rotateVectorByQuaternion(actorOrientation.rotation, mainGeometry.offset) + actorOrientation.position, 0.0);
    orient.size = vec4(mainGeometry.obbSize, 0.0);
    orient.rotation = vec4(quaternionMultiply(actorOrientation.rotation, mainGeometry.rotate));

    orientationObbs[curIndex] = orient;
}