#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

SCENE_SETTING_DATA(0, 0)
layout(r32ui, set = 0, binding = 1) uniform uimage1D errorFlag;
STORAGE_BUFFER_R(0, 2, inputActorsUpdateIndexes, uint, inActorsUpdateIndexes)           

#ifndef UPDATE
ACTORS_DATA(0, 3)
ACTOR_ORIENTAIONS_DATA_R(0, 4)

DYNAMIC_ACTORS_DATA_R(0, 5)
SLEEPING_ACTORS_DATA_R(0, 6)
VELOCITY_ACTORS_DATA(0, 7) //in out
STORAGE_BUFFER_W(0, 8, outputDynamicActorsData, DataDynamicActor, outputDynamicActors)  //output
#else
STORAGE_BUFFER_R(0, 3, inputSimActors, DataSimActor, inSimActors)
STORAGE_BUFFER_R(0, 4, inputContainerVelocities, AccamulatedContainerVelocity, inContainerVelocities)

ACTORS_DATA(0, 5)
DYNAMIC_ACTORS_DATA_R(0, 6)
VELOCITY_ACTORS_DATA(0, 7) //in out
ACTOR_ORIENTAIONS_DATA(0, 8) //in out
SLEEPING_ACTORS_DATA(0, 9) //in out
STORAGE_BUFFER_W(0, 10, outputDynamicActorsData, DataDynamicActor, outputDynamicActors)  //output
#endif


layout(push_constant, std430) uniform Constants
{
    uint maxCountUpdateActors;
    int maxIndexVelocities;
    uint countIterations;
    uint isFinal;
} pConstants;

bool errorImpulse(vec3 impulse)
{
    if(isnan(impulse.x) || isnan(impulse.y) || isnan(impulse.z) || 
        isinf(impulse.x) || isinf(impulse.y) || isinf(impulse.z))
        return true;
    else
        return false;
}

void setErrorFlag()
{
    imageStore(errorFlag, 0, uvec4(0));
}

struct SumVelocities
{
    vec3 linearVelocity;
    vec3 angularVelocity;
};

#ifdef UPDATE
SumVelocities getSumVelocities(DataSimActor simActor)
{

    vec3 lImpulse = vec3(0, 0, 0);
    vec3 cImpulse = vec3(0, 0, 0);

    //read sum velocities from other containers
    {
        vec3 linearVels[2];
        linearVels[0] = vec3(0);
        linearVels[1] = vec3(0);
        vec3 angularVels[2];
        angularVels[0] = vec3(0);
        angularVels[1] = vec3(0);

        int next = simActor.indexStartLoopContainers;
        bool needSwap = simActor.isStartLikeActorB != 0;
        int nextIndexRead = needSwap ? 1 : 0;
        while(next != -1)
        {
            if(next >= pConstants.maxIndexVelocities)
                break;

            AccamulatedContainerVelocity velocities = inContainerVelocities[next];

            linearVels[0] = velocities.velocities[0].xyz;
            angularVels[0] = velocities.velocities[1].xyz;
            linearVels[1] = velocities.velocities[2].xyz;
            angularVels[1] = vec3(velocities.velocities[0].w, velocities.velocities[1].w, velocities.velocities[2].w);

            lImpulse += linearVels[nextIndexRead];
            cImpulse += angularVels[nextIndexRead];

            next = velocities.subData[nextIndexRead];
            needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
            nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
        }
    }

    SumVelocities velocities;
    velocities.linearVelocity = lImpulse;
    velocities.angularVelocity = cImpulse;
    return velocities;
}
#endif

layout (local_size_x = SIZE_GROUP_SIMULATE_ACTORS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexActor = gl_GlobalInvocationID.x;

    if(indexActor >= pConstants.maxCountUpdateActors)
        return;

    bool error = imageLoad(errorFlag, 0).x == 0;

    indexActor = inActorsUpdateIndexes[indexActor];
    DataActor actor = actors[indexActor];
    uint indexDynamicData = actor.indexAdditionalData;
    DataDynamicActor mainDynamicData = dynamicActors[indexDynamicData];

    float stepH = sceneSetting.deltaTime / pConstants.countIterations;

    DataVelocityActor vel = velocityActors[indexDynamicData];
    DataActorOrientation orientation = actorOrientations[indexActor];

    #ifdef UPDATE
    {
        DataSimActor simActor = inSimActors[indexDynamicData];
        SumVelocities velocities = getSumVelocities(simActor);

        if(errorImpulse(velocities.linearVelocity) || errorImpulse(velocities.angularVelocity))
        {
            setErrorFlag();
            return;
        }

        vel.velocity += velocities.linearVelocity;
        vel.angularVelocity += velocities.angularVelocity;

        mat3 rot = quaternionToMat3(orientation.rotation);
        mat3 intertial = rot * mainDynamicData.inertialTensor * transpose(rot);
        mat3 invIntertial = rot * mainDynamicData.inverseInertialTensor * transpose(rot);

        vel.angularVelocity += stepH * invIntertial * (-cross(vel.angularVelocity, intertial * vel.angularVelocity));

        float dampingForces = 1.0 - (sceneSetting.linearDamping * stepH);
        float dampingAngularForces = 1.0 - (sceneSetting.angularDamping * stepH);

        vel.velocity *= dampingForces;
        vel.angularVelocity *= dampingAngularForces;

        DataSleepingActor sleeping = sleepingActors[indexDynamicData];
        if(pConstants.isFinal != 0)
        {
            if((length(vel.velocity) >= sceneSetting.maxVelocitySleep || 
                length(vel.angularVelocity) >= sceneSetting.maxAngularVelocitySleep))
            {
                sleeping.sleepTime = 0.0;
                sleeping.isSleeping = 0;
            }
            else
            {
                if(sleeping.isSleeping == 0)
                    sleeping.sleepTime += sceneSetting.deltaTime;
            }
            
            if(sleeping.sleepTime >= sceneSetting.minSleepTime)
            {
                sleeping.isSleeping = 1;
                vel.velocity = vec3(0, 0, 0);
                vel.angularVelocity = vec3(0, 0, 0);
            }

            if((simActor.flags & FLAG_SIM_ACTOR_TRY_WAKE) != 0 && sleeping.isSleeping != 0)
            {
                sleeping.sleepTime = 0;
                sleeping.isSleeping = 0;
            }

            sleepingActors[indexDynamicData] = sleeping;
        }

        //disable if is sleeping
        if(sleeping.isSleeping == 0)
        {
            vec3 globalCenterOfMass = orientation.position.xyz + (mainDynamicData.centerMass * rot);

            globalCenterOfMass += stepH * vel.velocity;
            vec4 q = orientation.rotation;
            q = quaternionAddScaledVector(q, vel.angularVelocity, stepH);

            q = normalize(q);           
            orientation.rotation = q;

            rot = quaternionToMat3(q);
            orientation.position.xyz = globalCenterOfMass - (mainDynamicData.centerMass * rot);
        }

        if(sceneSetting.updateVelocities != 0 && error)
            actorOrientations[indexActor] = orientation;
    }
    #endif

    vel.acceleration = vec3(0);
    vel.angularAcceleration = vec3(0);

    vel.velocity += sceneSetting.gravitation * stepH;

    #ifndef UPDATE
    DataSleepingActor sleeping = sleepingActors[indexDynamicData];
    if(sleeping.isSleeping != 0)
        vel.velocity = vec3(0, 0, 0);
    #endif

    velocityActors[indexDynamicData] = vel;

    //update dynamic data
    DataDynamicActor updatedDynamicData = mainDynamicData;

    mat3 rot = quaternionToMat3(orientation.rotation);
    updatedDynamicData.inertialTensor = rot * updatedDynamicData.inertialTensor * transpose(rot);
    updatedDynamicData.inverseInertialTensor = rot * updatedDynamicData.inverseInertialTensor * transpose(rot);

    outputDynamicActors[indexDynamicData] = updatedDynamicData;
}