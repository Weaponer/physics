#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

layout(std430, set = 0, binding = 0) readonly buffer maxCountContainersBuffer
{
    uint maxCountContainers;
};

ACTOR_ORIENTAIONS_DATA_R(0, 1)
VELOCITY_ACTORS_DATA_R(0, 2)
DYNAMIC_ACTORS_DATA_R(0, 3)
STORAGE_BUFFER_R(0, 4, inputSimContainers, CommonDataContactsContainer, inSimContainers)        

STORAGE_BUFFER_R(0, 5, inputContainerLoopsData, ivec2, inContainerLoopsData)      
STORAGE_BUFFER_R(0, 6, inputSimActors, DataSimActor, inSimActors)
STORAGE_BUFFER_R(0, 7, inputUsedEdge, int, inUsedEdge)      
STORAGE_BUFFER_R(0, 8, inputGraphEdge, DataActorGraphEdge_2, inGraphEdge)      
STORAGE_BUFFER_W(0, 9, outputSimContactsContainerEPBD, DataSimContactsContainerEPBD, outSimContactsContainerEPBD)   //output
STORAGE_BUFFER_W(0, 10, outputContainerVelocities, AccamulatedContainerVelocity, outContainerVelocities)            //output

layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexContainer = gl_GlobalInvocationID.x;

    if(gl_GlobalInvocationID.x >= maxCountContainers)
        return;

    CommonDataContactsContainer container = inSimContainers[indexContainer];

    float invMasses[2];
    mat3 invInertialTensors[2];
    vec3 centerOfMass[2];
    
    invMasses[0] = 0.0;
    invInertialTensors[0] = mat3(0);
    centerOfMass[0] = vec3(0, 0, 0);

    if(container.actorA != -1)
    {
        DataDynamicActor data = dynamicActors[container.actorA];
        invMasses[0] = 1.0 / data.mass;
        invInertialTensors[0] = data.inverseInertialTensor;
        centerOfMass[0] = data.centerMass;
    }

    invMasses[1] = 0.0;
    invInertialTensors[1] = mat3(0);
    centerOfMass[1] = vec3(0, 0, 0);

    if(container.actorB != -1)
    {
        DataDynamicActor data = dynamicActors[container.actorB];
        invMasses[1] = 1.0 / data.mass;
        invInertialTensors[1] = data.inverseInertialTensor;
        centerOfMass[1] = data.centerMass;
    }

    DataActorOrientation actorOrientationA = actorOrientations[container.orientationActorA]; 
    DataActorOrientation actorOrientationB = actorOrientations[container.orientationActorB]; 

    vec3 positionActorA = actorOrientationA.position.xyz;
    vec3 positionActorB = actorOrientationB.position.xyz;
    mat3 rotateActorA = quaternionToMat3(actorOrientationA.rotation);
    mat3 rotateActorB = quaternionToMat3(actorOrientationB.rotation);

    DataVelocityActor velA;
    DataVelocityActor velB;

    if(container.actorA != -1)
    {
        velA = velocityActors[container.actorA];
    }
    else
    {
        velA.acceleration = vec3(0);
        velA.angularAcceleration = vec3(0);
        velA.velocity = vec3(0);
        velA.angularVelocity = vec3(0);
    }

    if(container.actorB != -1)
    {
        velB = velocityActors[container.actorB];
    }
    else
    {
        velB.acceleration = vec3(0);
        velB.angularAcceleration = vec3(0);
        velB.velocity = vec3(0);
        velB.angularVelocity = vec3(0);
    }

    vec3 globalCenterOfMassA = positionActorA + (centerOfMass[0] * rotateActorA);
    vec3 globalCenterOfMassB = positionActorB + (centerOfMass[1] * rotateActorB);

    DataSimContactsContainerEPBD contactsContainer;
    mat3 m = invInertialTensors[0];
    contactsContainer.dataA[0] = vec4(m[0][0], m[0][1], m[0][2], invMasses[0]);
    contactsContainer.dataA[1] = vec4(m[1][0], m[1][1], m[1][2], velA.angularVelocity.x);
    contactsContainer.dataA[2] = vec4(m[2][0], m[2][1], m[2][2], velA.angularVelocity.y);
    contactsContainer.dataA[3] = vec4(globalCenterOfMassA.x, globalCenterOfMassA.y, globalCenterOfMassA.z, velA.angularVelocity.z);
    contactsContainer.dataA[4] = vec4(velA.velocity.x, velA.velocity.y, velA.velocity.z, container.staticFriction);

    m = invInertialTensors[1];
    contactsContainer.dataB[0] = vec4(m[0][0], m[0][1], m[0][2], invMasses[1]);
    contactsContainer.dataB[1] = vec4(m[1][0], m[1][1], m[1][2], velB.angularVelocity.x);
    contactsContainer.dataB[2] = vec4(m[2][0], m[2][1], m[2][2], velB.angularVelocity.y);
    contactsContainer.dataB[3] = vec4(globalCenterOfMassB.x, globalCenterOfMassB.y, globalCenterOfMassB.z, velB.angularVelocity.z);
    contactsContainer.dataB[4] = vec4(velB.velocity.x, velB.velocity.y, velB.velocity.z, container.dynamicFriction);

    contactsContainer.indexFirstContact = container.indexFirstContact;
    contactsContainer.countContacts = container.countContacts;
    contactsContainer.indexGraph = inUsedEdge[indexContainer];

    if((inGraphEdge[indexContainer].flags & FLAG_GRAP_NODE_IS_USED) == 0)
        contactsContainer.indexGraph = -2;
    
    contactsContainer.startListContainersA = -1;
    contactsContainer.startSwapActorA = 0;

    if(container.actorA != -1)
    {
        DataSimActor actor = inSimActors[container.actorA];
        contactsContainer.startListContainersA = actor.indexStartLoopContainers;
        if(actor.isStartLikeActorB != 0)
            contactsContainer.startSwapActorA = 1;
    }

    contactsContainer.startListContainersB = -1;
    contactsContainer.startSwapActorB = 0;

    if(container.actorB != -1)
    {
        DataSimActor actor = inSimActors[container.actorB];
        contactsContainer.startListContainersB = actor.indexStartLoopContainers;
        if(actor.isStartLikeActorB == 0)
            contactsContainer.startSwapActorB = 1;
    }
    contactsContainer.bounciness = container.bounciness;

    outSimContactsContainerEPBD[indexContainer] = contactsContainer;

    //loop data init
    {
        ivec2 loopDataA = inContainerLoopsData[indexContainer * 2];
        ivec2 loopDataB = inContainerLoopsData[indexContainer * 2 + 1];

        AccamulatedContainerVelocity velocitiesData;
        velocitiesData.velocities[0] = vec4(0);
        velocitiesData.velocities[1] = vec4(0);
        velocitiesData.velocities[2] = vec4(0);

        int needToSwap = 0;
        if(loopDataA.x != -1 && loopDataA.y != 0)
            needToSwap |= 1;
        if(loopDataB.x != -1 && loopDataB.y != 0)
            needToSwap |= 2;

        velocitiesData.subData = ivec4(loopDataA.x, loopDataB.x, contactsContainer.indexGraph, needToSwap);
        outContainerVelocities[indexContainer] = velocitiesData;
    }
}