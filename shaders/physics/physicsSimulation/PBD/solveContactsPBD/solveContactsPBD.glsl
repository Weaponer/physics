#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

precision highp float;

layout(std430, set = 0, binding = 0) readonly buffer maxCountContainersBuffer
{
    uint maxCountContainers;
};

STORAGE_BUFFER_R(0, 1, inputSimContactsContainerEPBD, DataSimContactsContainerEPBD, inSimContactsContainerEPBD)
STORAGE_BUFFER_R(0, 2, inputSimContactModels, CommonDataSimContact, inSimContactsModels)

STORAGE_BUFFER_R(0, 3, inputContainerVelocities, AccamulatedContainerVelocity, inContainerVelocities)
STORAGE_BUFFER_W(0, 4, outputContainerVelocities, AccamulatedContainerVelocity, outContainerVelocities)  //output

layout(r32ui, set = 0, binding = 5) uniform uimage1D errorFlag;

SCENE_SETTING_DATA(0, 6)

layout(push_constant, std430) uniform Constants
{
    int indexCurrentStage;
    int maxIndexVelocities;
    uint countIterations;
} pConstants;

void setErrorFlag()
{
    imageStore(errorFlag, 0, uvec4(0));
}

layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{   
    uint indexContainer = gl_GlobalInvocationID.x;

    if(indexContainer >= maxCountContainers)
        return;

    DataSimContactsContainerEPBD container = inSimContactsContainerEPBD[indexContainer];
    int indexGraph = container.indexGraph;
    uint countContacts = container.countContacts; 

    if(indexGraph < -1)
    {
        AccamulatedContainerVelocity vel = inContainerVelocities[indexContainer];
        vel.velocities[0] = vec4(0, 0, 0, 0);
        vel.velocities[1] = vec4(0, 0, 0, 0);
        vel.velocities[2] = vec4(0, 0, 0, 0);
        outContainerVelocities[indexContainer] = vel;
        return;
    }


    if(indexGraph != pConstants.indexCurrentStage)
    {
        outContainerVelocities[indexContainer] = inContainerVelocities[indexContainer];
        return;
    }

    vec3 linerVelocityA = container.dataA[4].xyz;
    vec3 angularVelocityA = vec3(container.dataA[1].w, container.dataA[2].w, container.dataA[3].w);
    vec3 linerVelocityB = container.dataB[4].xyz;
    vec3 angularVelocityB = vec3(container.dataB[1].w, container.dataB[2].w, container.dataB[3].w);

    ivec4 currentSubData = inContainerVelocities[indexContainer].subData;

    {
        vec3 linearVels[2];
        linearVels[0] = vec3(0);
        linearVels[1] = vec3(0);
        vec3 angularVels[2];
        angularVels[0] = vec3(0);
        angularVels[1] = vec3(0);
        //read sum velocities from other containers for A
        {
            int next = container.startListContainersA;
            bool needSwap = container.startSwapActorA != 0;
            int nextIndexRead = needSwap ? 1 : 0;
            int counter = 0;
            while(next != -1 && counter < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS)
            {
                counter++;

                AccamulatedContainerVelocity velocities = inContainerVelocities[next];
                if(next == indexContainer)
                    currentSubData = velocities.subData;

                if(velocities.subData.z == -1 || velocities.subData.z == pConstants.indexCurrentStage)
                {
                    next = velocities.subData[nextIndexRead];
                    needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
                    nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
                    continue;
                }

                linearVels[0] = velocities.velocities[0].xyz;
                angularVels[0] = velocities.velocities[1].xyz;
                linearVels[1] = velocities.velocities[2].xyz;
                angularVels[1] = vec3(velocities.velocities[0].w, velocities.velocities[1].w, velocities.velocities[2].w);

                linerVelocityA += linearVels[nextIndexRead];
                angularVelocityA += angularVels[nextIndexRead];

                next = velocities.subData[nextIndexRead];
                needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
                nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
            }
            if(counter >= CONTACTS_CONTAINER_MAX_COUNT_CONTACTS)
                setErrorFlag();
        }

        //read sum velocities from other containers for B
        {
            int next = container.startListContainersB;
            bool needSwap = container.startSwapActorB != 0;
            int nextIndexRead = needSwap ? 0 : 1;
            int counter = 0;
            while(next != -1 && counter < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS)
            {
                counter++;

                AccamulatedContainerVelocity velocities = inContainerVelocities[next];
                if(next == indexContainer)
                    currentSubData = velocities.subData;

                if(velocities.subData.z == -1 || velocities.subData.z == pConstants.indexCurrentStage)
                {
                    next = velocities.subData[nextIndexRead];
                    needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
                    nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
                    continue;
                }

                linearVels[0] = velocities.velocities[0].xyz;
                angularVels[0] = velocities.velocities[1].xyz;
                linearVels[1] = velocities.velocities[2].xyz;
                angularVels[1] = vec3(velocities.velocities[0].w, velocities.velocities[1].w, velocities.velocities[2].w);

                linerVelocityB += linearVels[nextIndexRead];
                angularVelocityB += angularVels[nextIndexRead];

                next = velocities.subData[nextIndexRead];
                needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
                nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
            }
            if(counter >= CONTACTS_CONTAINER_MAX_COUNT_CONTACTS)
                setErrorFlag();
        }
    }


    vec3 centerMassA = container.dataA[3].xyz;
    vec3 centerMassB = container.dataB[3].xyz;
    float invMassA = container.dataA[0].w;
    float invMassB = container.dataB[0].w;
    mat3 invInertialA = mat3(container.dataA[0][0], container.dataA[0][1], container.dataA[0][2],
                                container.dataA[1][0], container.dataA[1][1], container.dataA[1][2],
                                container.dataA[2][0], container.dataA[2][1], container.dataA[2][2]);
    mat3 invInertialB = mat3(container.dataB[0][0], container.dataB[0][1], container.dataB[0][2],
                                container.dataB[1][0], container.dataB[1][1], container.dataB[1][2],
                                container.dataB[2][0], container.dataB[2][1], container.dataB[2][2]);

    float staticFriction = container.dataA[4].w;
    float dynamicFriction = container.dataB[4].w;
    float bounciness = container.bounciness;

    float stepH = sceneSetting.deltaTime / pConstants.countIterations;
    float stiffness = sceneSetting.stiffness;

    AccamulatedContainerVelocity containerVelocityRes;
    containerVelocityRes.velocities[0] = vec4(0, 0, 0, 0);
    containerVelocityRes.velocities[1] = vec4(0, 0, 0, 0);
    containerVelocityRes.velocities[2] = vec4(0, 0, 0, 0);
    containerVelocityRes.subData = currentSubData;

    for(uint i = 0; i < container.countContacts; i++)
    {
        uint indexContact = container.indexFirstContact + i;
        CommonDataSimContact contact = inSimContactsModels[indexContact];
        
        vec3 rA = contact.positionA - centerMassA;
        vec3 rB = contact.positionB - centerMassB;
        vec3 contactNormal = -contact.normalAtoB;

        vec3 vA = linerVelocityA + cross(angularVelocityA, rA);
        vec3 vB = linerVelocityB + cross(angularVelocityB, rB);
        vec3 vD = vB - vA;                                      //velocity distinct
        float vN = dot(contactNormal, vD);

        vec3 tangent;
        vec3 bitangent;

        {
            vec3 other = vec3(0, 1, 0);
            if(abs(contactNormal.y) >= 0.99)
                other = vec3(-1, 0, 0);
            
            vec3 next = cross(contactNormal, other);
            bitangent = normalize(cross(contactNormal, next));
            tangent = normalize(cross(contactNormal, bitangent));
        }

        mat3 contactM = mat3(tangent.x, tangent.y, tangent.z,
                            contactNormal.x, contactNormal.y, contactNormal.z,
                            bitangent.x, bitangent.y, bitangent.z);

        vec3 frictionDirect = contactM * vD;
        frictionDirect.y = 0.0;
        frictionDirect = frictionDirect * contactM;
        float frictionL = length(frictionDirect);
        if(frictionL > 0.00001)
            frictionDirect /= frictionL;
        else
            frictionDirect = vec3(0, 0, 0);

        vec3 crossRA = cross(rA, contactNormal);
        vec3 crossRB = cross(rB, -contactNormal);

        float nwA = invMassA + dot(invInertialA * crossRA, crossRA);
        float nwB = invMassB + dot(invInertialB * crossRB, crossRB);

        float nw = (1.0 / (nwA + nwB)); 

        vec3 crossFrictionA = cross(rA, frictionDirect);
        vec3 crossFrictionB = cross(rB, -frictionDirect);

        float twA = invMassA + dot(invInertialA * crossFrictionA, crossFrictionA);
        float twB = invMassB + dot(invInertialB * crossFrictionB, crossFrictionB);

        float tw = (1.0 / (twA + twB)) * dot(vD, frictionDirect); 

        float goalVelocity = 0.0; 
        if(vN < 0.0)
            goalVelocity = -bounciness * vN;
        
        float d = dot(contactNormal, contact.positionB - contact.positionA);

        float deltaVN = goalVelocity - vN;
        float correctionMagnitude = deltaVN * nw;
        if (correctionMagnitude < 0)
            correctionMagnitude = 0;
        
        d = max(-sceneSetting.depthContactMax, min(d + sceneSetting.depthContactMin, 0.0));
        if(d < 0)
            correctionMagnitude -= stiffness * nw * d;

        vec3 p = contactNormal * correctionMagnitude;

        float pn = min(dot(p, contactNormal), dot(frictionDirect, vD));
        vec3 frictionForce = vec3(0, 0, 0);
        if (dynamicFriction * pn > tw)
            frictionForce -= frictionDirect * tw;
        else if (dynamicFriction * pn < -tw)
            frictionForce += frictionDirect * tw;
        else
            frictionForce -= dynamicFriction * pn * frictionDirect;

        p += frictionForce;

        if (invMassA != 0)
        {
            vec3 lVelA = invMassA * -p;
            vec3 aVelA = invInertialA * cross(rA, -p);

            linerVelocityA += lVelA;
            angularVelocityA += aVelA;

            containerVelocityRes.velocities[0].xyz += lVelA;
            containerVelocityRes.velocities[1].xyz += aVelA;
        }
        if (invMassB != 0)
        {
            vec3 lVelB = invMassB * p;
            vec3 aVelB = invInertialB * cross(rB, p);

            linerVelocityB += lVelB;
            angularVelocityB += aVelB;

            containerVelocityRes.velocities[2].xyz += lVelB;
            containerVelocityRes.velocities[0].w += aVelB.x;
            containerVelocityRes.velocities[1].w += aVelB.y;
            containerVelocityRes.velocities[2].w += aVelB.z;
        }
    }
    outContainerVelocities[indexContainer] = containerVelocityRes;
}