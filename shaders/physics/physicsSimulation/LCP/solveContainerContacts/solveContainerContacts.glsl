#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

precision lowp float;

layout(std430, set = 0, binding = 0) readonly buffer maxCountContainersBuffer
{
    uint maxCountContainers;
};

STORAGE_BUFFER_R(0, 1, inputSimContactsContainerFinished, DataSimContactsContainerFinished, inSimContactsContainerFinished)
STORAGE_BUFFER_R(0, 2, inputSimContactModels, DataSimContactModel, inSimContactsModels)

STORAGE_BUFFER_R(0, 3, inputContainerVelocities, AccamulatedContainerVelocity, inContainerVelocities)
STORAGE_BUFFER_W(0, 4, outputContainerVelocities, AccamulatedContainerVelocity, outContainerVelocities)  //output

STORAGE_BUFFER_R(0, 5, inputContactImpulse, vec4, inContactImpulse)
STORAGE_BUFFER_W(0, 6, outputContactImpulse, vec4, outContactImpulse)  //output

layout(r32ui, set = 0, binding = 7) uniform uimage1D errorFlag;

layout(push_constant, std430) uniform Constants
{
    int indexCurrentStage;
    int maxIndexVelocities;
} pConstants;

#define MAX_CONTACTS_SOLVE_TO_CONTAINER 4

uint countContacts;
DataSimContactModel contacts[MAX_CONTACTS_SOLVE_TO_CONTAINER];
vec3 impulses[MAX_CONTACTS_SOLVE_TO_CONTAINER];
float invMasses[2];
mat3 invInertialTensors[2];

vec3 accamulatedLinerVelocityA;
vec3 accamulatedAngularVelocityA;
vec3 accamulatedLinerVelocityB;
vec3 accamulatedAngularVelocityB;

vec3 accamulateVelocities(uint index)
{
    DataSimContactModel contact = contacts[index];

    mat3 JL0;
    JL0[0] = contact.basis[0].xyz;
    JL0[1] = contact.basis[1].xyz;
    JL0[2] = contact.basis[2].xyz;
    JL0 = transpose(JL0);

    mat3 JL1;
    JL1[0] = -contact.basis[0].xyz;
    JL1[1] = -contact.basis[1].xyz;
    JL1[2] = -contact.basis[2].xyz;
    JL1 = transpose(JL1);

    mat3 JC0;
    mat3 JC1;

    {
        vec3 rA = vec3(contact.basis[0][3], contact.basis[1][3], contact.basis[2][3]);
        vec3 rB = vec3(contact.A[0][3], contact.A[1][3], contact.A[2][3]);

        JC0 = JL0 * mat3(0, -rA.z, rA.y,
                        rA.z, 0, -rA.x,
                        -rA.y, rA.x, 0);
        JC1 = JL1 * mat3(0, -rB.z, rB.y,
                        rB.z, 0, -rB.x,
                        -rB.y, rB.x, 0);
    }

    mat3 JLMinv0 = JL0 * invMasses[0];
    mat3 JCMinv0 = JC0 * invInertialTensors[0];
    mat3 JLMinv1 = JL1 * invMasses[1];
    mat3 JCMinv1 = JC1 * invInertialTensors[1];


    vec3 laccamulatedLinerVelocityA = accamulatedLinerVelocityA;
    vec3 laccamulatedAngularVelocityA = accamulatedAngularVelocityA;
    vec3 laccamulatedLinerVelocityB = accamulatedLinerVelocityB;
    vec3 laccamulatedAngularVelocityB = accamulatedAngularVelocityB;

    for(uint i = 0; i < countContacts; i++)
    {   
        if(i == index)
            continue;
            
        DataSimContactModel nearContact = contacts[i];
        vec3 impulse = impulses[i];

        mat3 JLA;
        mat3 JCA;
        {
            JLA[0] = nearContact.basis[0].xyz;
            JLA[1] = nearContact.basis[1].xyz;
            JLA[2] = nearContact.basis[2].xyz;
            JLA = transpose(JLA);

            vec3 rA = vec3(nearContact.basis[0][3], nearContact.basis[1][3], nearContact.basis[2][3]);
            JCA = JLA * mat3(0, -rA.z, rA.y,
                            rA.z, 0, -rA.x,
                            -rA.y, rA.x, 0);
        }

        mat3 JLB;
        mat3 JCB;
        {
            JLB[0] = -nearContact.basis[0].xyz;
            JLB[1] = -nearContact.basis[1].xyz;
            JLB[2] = -nearContact.basis[2].xyz;
            JLB = transpose(JLB);

            vec3 rB = vec3(nearContact.A[0][3], nearContact.A[1][3], nearContact.A[2][3]);
            JCB = JLB * mat3(0, -rB.z, rB.y,
                            rB.z, 0, -rB.x,
                            -rB.y, rB.x, 0);
        }

        laccamulatedLinerVelocityA += (impulse * JLA);
        laccamulatedAngularVelocityA += (impulse * JCA);
        laccamulatedLinerVelocityB += (impulse * JLB);
        laccamulatedAngularVelocityB += (impulse * JCB);
    }
    
    return -(JLMinv0 * laccamulatedLinerVelocityA + JCMinv0 * laccamulatedAngularVelocityA +
            JLMinv1 * laccamulatedLinerVelocityB + JCMinv1 * laccamulatedAngularVelocityB);
}


AccamulatedContainerVelocity getAccamulatedContainerVelocity()
{
    
    vec3 linearVelocityA = vec3(0, 0, 0);
    vec3 angularVelocityA = vec3(0, 0, 0);
    vec3 linearVelocityB = vec3(0, 0, 0);
    vec3 angularVelocityB = vec3(0, 0, 0);

    for(uint i = 0; i < countContacts; i++)
    {          
        DataSimContactModel contact = contacts[i];
        vec3 impulse = impulses[i];

        mat3 JLA;
        mat3 JCA;
        {
            JLA[0] = contact.basis[0].xyz;
            JLA[1] = contact.basis[1].xyz;
            JLA[2] = contact.basis[2].xyz;
            JLA = transpose(JLA);

            vec3 rA = vec3(contact.basis[0][3], contact.basis[1][3], contact.basis[2][3]);
            JCA = JLA * mat3(0, -rA.z, rA.y,
                            rA.z, 0, -rA.x,
                            -rA.y, rA.x, 0);
        }

        mat3 JLB;
        mat3 JCB;
        {
            JLB[0] = -contact.basis[0].xyz;
            JLB[1] = -contact.basis[1].xyz;
            JLB[2] = -contact.basis[2].xyz;
            JLB = transpose(JLB);

            vec3 rB = vec3(contact.A[0][3], contact.A[1][3], contact.A[2][3]);
            JCB = JLB * mat3(0, -rB.z, rB.y,
                            rB.z, 0, -rB.x,
                            -rB.y, rB.x, 0);
        }

        linearVelocityA += (impulse * JLA);
        angularVelocityA += (impulse * JCA);
        linearVelocityB += (impulse * JLB);
        angularVelocityB += (impulse * JCB);
    }

    AccamulatedContainerVelocity containerVelocity;
    containerVelocity.velocities[0] = vec4(linearVelocityA, angularVelocityB.x);
    containerVelocity.velocities[1] = vec4(angularVelocityA, angularVelocityB.y);
    containerVelocity.velocities[2] = vec4(linearVelocityB, angularVelocityB.z);
    return containerVelocity;
}

void setErrorFlag()
{
    imageStore(errorFlag, 0, uvec4(0));
}

layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{   
    uint indexContainer = gl_GlobalInvocationID.x;

    if(indexContainer >= maxCountContainers)
        return;

    DataSimContactsContainerFinished container = inSimContactsContainerFinished[indexContainer];
    int indexGraph = container.indexGraph;
    countContacts = container.countContacts; 

    if((container.flagsEdge & FLAG_GRAP_NODE_IS_USED) == 0)
    {
        AccamulatedContainerVelocity vel = inContainerVelocities[indexContainer];
        vel.velocities[0] = vec4(0, 0, 0, 0);
        vel.velocities[1] = vec4(0, 0, 0, 0);
        vel.velocities[2] = vec4(0, 0, 0, 0);

        outContainerVelocities[indexContainer] = vel;
        for(uint i = 0; i < countContacts; i++)
            outContactImpulse[container.indexFirstContact + i] = vec4(0, 0, 0, 0);   
        return;
    }

    if(indexGraph != pConstants.indexCurrentStage)
    {
        outContainerVelocities[indexContainer] = inContainerVelocities[indexContainer];
        for(uint i = 0; i < countContacts; i++)
            outContactImpulse[container.indexFirstContact + i] = inContactImpulse[container.indexFirstContact + i];   
        return;
    }

    for(uint i = 0; i < countContacts; i++)
    {
        contacts[i] = inSimContactsModels[container.indexFirstContact + i];
        impulses[i] = inContactImpulse[container.indexFirstContact + i].xyz;
    }

    invMasses[0] = container.invMassTensorA[0][3];
    invMasses[1] = container.invMassTensorB[0][3];

    invInertialTensors[0] = mat3(container.invMassTensorA[0][0], container.invMassTensorA[0][1], container.invMassTensorA[0][2],
                                 container.invMassTensorA[1][0], container.invMassTensorA[1][1], container.invMassTensorA[1][2],
                                 container.invMassTensorA[2][0], container.invMassTensorA[2][1], container.invMassTensorA[2][2]);
    invInertialTensors[1] = mat3(container.invMassTensorB[0][0], container.invMassTensorB[0][1], container.invMassTensorB[0][2],
                                 container.invMassTensorB[1][0], container.invMassTensorB[1][1], container.invMassTensorB[1][2],
                                 container.invMassTensorB[2][0], container.invMassTensorB[2][1], container.invMassTensorB[2][2]);
    
    accamulatedLinerVelocityA = vec3(0, 0, 0);
    accamulatedAngularVelocityA = vec3(0, 0, 0);
    accamulatedLinerVelocityB = vec3(0, 0, 0);
    accamulatedAngularVelocityB = vec3(0, 0, 0);

    ivec4 currentSubData = inContainerVelocities[indexContainer].subData;//ivec4(0);

    {
        vec3 linearVels[2];
        linearVels[0] = vec3(0);
        linearVels[1] = vec3(0);
        vec3 angularVels[2];
        angularVels[0] = vec3(0);
        angularVels[1] = vec3(0);
        //read sum velocities from other containers for A
        {
            int next = container.startListContainersA;
            bool needSwap = container.startSwapActorA != 0;
            int nextIndexRead = needSwap ? 1 : 0;
            int counter = 0;
            while(next != -1 && counter < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS)
            {
                //break;

                /*if(next >= pConstants.maxIndexVelocities)
                {
                    setErrorFlag();
                    break;
                }*/

                counter++;

                AccamulatedContainerVelocity velocities = inContainerVelocities[next];
                if(next == indexContainer)
                    currentSubData = velocities.subData;

                if(velocities.subData.z == -1 || velocities.subData.z == pConstants.indexCurrentStage)
                {
                    next = velocities.subData[nextIndexRead];
                    needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
                    nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
                    continue;
                }

                linearVels[0] = velocities.velocities[0].xyz;
                angularVels[0] = velocities.velocities[1].xyz;
                linearVels[1] = velocities.velocities[2].xyz;
                angularVels[1] = vec3(velocities.velocities[0].w, velocities.velocities[1].w, velocities.velocities[2].w);

                accamulatedLinerVelocityA += linearVels[nextIndexRead];
                accamulatedAngularVelocityA += angularVels[nextIndexRead];

                next = velocities.subData[nextIndexRead];
                needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
                nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
            }
            if(counter >= CONTACTS_CONTAINER_MAX_COUNT_CONTACTS)
                setErrorFlag();
        }

        //read sum velocities from other containers for B
        {
            int next = container.startListContainersB;
            bool needSwap = container.startSwapActorB != 0;
            int nextIndexRead = needSwap ? 0 : 1;
            int counter = 0;
            while(next != -1 && counter < CONTACTS_CONTAINER_MAX_COUNT_CONTACTS)
            {
                //break;

                /*if(next >= pConstants.maxIndexVelocities)
                {
                    setErrorFlag();
                    break;
                }*/

                counter++;

                AccamulatedContainerVelocity velocities = inContainerVelocities[next];
                if(next == indexContainer)
                    currentSubData = velocities.subData;

                if(velocities.subData.z == -1 || velocities.subData.z == pConstants.indexCurrentStage)
                {
                    next = velocities.subData[nextIndexRead];
                    needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
                    nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
                    continue;
                }

                linearVels[0] = velocities.velocities[0].xyz;
                angularVels[0] = velocities.velocities[1].xyz;
                linearVels[1] = velocities.velocities[2].xyz;
                angularVels[1] = vec3(velocities.velocities[0].w, velocities.velocities[1].w, velocities.velocities[2].w);

                accamulatedLinerVelocityB += linearVels[nextIndexRead];
                accamulatedAngularVelocityB += angularVels[nextIndexRead];

                next = velocities.subData[nextIndexRead];
                needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
                nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
            }
            if(counter >= CONTACTS_CONTAINER_MAX_COUNT_CONTACTS)
                setErrorFlag();
        }
    }

    const uint countIters = 4;

    for(uint iter = 0; iter < countIters; iter++)
    {
        for(uint i = 0; i < countContacts; i++)
        {
            vec3 vel = accamulateVelocities(i);
            
            DataSimContactModel contact = contacts[i];
            vel = vel + contact.velocity.xyz; 
            mat3 A = mat3(contact.A[0][0], contact.A[0][1], contact.A[0][2],
                                contact.A[1][0], contact.A[1][1], contact.A[1][2],
                                contact.A[2][0], contact.A[2][1], contact.A[2][2]);

            vec3 impulse = impulses[i];
            impulse[0] = max(0.0, (vel[0] - A[0][1] * impulse[1] - A[0][2] * impulse[2]) / A[0][0]);
            
            float frictionClamp = contact.velocity.w * impulse.x;

            impulse[1] = max(-frictionClamp, min((vel[1] - A[1][0] * impulse[0] - A[1][2] * impulse[2]) / A[1][1], frictionClamp));
            impulse[2] = max(-frictionClamp, min((vel[2] - A[2][0] * impulse[0] - A[2][1] * impulse[1]) / A[2][2], frictionClamp));
            
            impulses[i] = impulse;
        }
    }

    for(uint i = 0; i < countContacts; i++)
        outContactImpulse[container.indexFirstContact + i] = vec4(impulses[i], 1);
    
    AccamulatedContainerVelocity containerVelocityRes = getAccamulatedContainerVelocity();
    containerVelocityRes.subData = currentSubData;
    outContainerVelocities[indexContainer] = containerVelocityRes;
}