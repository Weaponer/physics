#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

STORAGE_BUFFER_R(0, 0, inputActorsUpdateIndexes, uint, inActorsUpdateIndexes)

ACTORS_DATA(0, 1)
STORAGE_BUFFER_R(0, 2, inputSimActors, DataSimActor, inSimActors)
ACTOR_ORIENTAIONS_DATA_R(0, 3)

DYNAMIC_ACTORS_DATA_R(0, 4)
VELOCITY_ACTORS_DATA(0, 5) //in out
STORAGE_BUFFER_W(0, 6, outputDynamicActorsData, DataDynamicActor, outputDynamicActors)  //output

SCENE_SETTING_DATA(0, 7)

layout(push_constant, std430) uniform Constants
{
    uint maxCountUpdateActors;
} pConstants;

layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexActor = gl_GlobalInvocationID.x;

    if(indexActor >= pConstants.maxCountUpdateActors)
        return;

    indexActor = inActorsUpdateIndexes[indexActor];
    DataActor actor = actors[indexActor];
    uint indexDynamicData = actor.indexAdditionalData;

    DataDynamicActor mainDynamicData = dynamicActors[indexDynamicData];
    DataSimActor simActor = inSimActors[indexDynamicData];

    float mass = mainDynamicData.mass;

    DataVelocityActor vel = velocityActors[indexDynamicData];
    vel.acceleration = vec3(0);
    vel.angularAcceleration = vec3(0);

    //if((simActor.flags & FLAG_SIM_ACTOR_TRY_WAKE) != 0 || (simActor.flags & FLAG_SIM_ACTOR_SLEEPING) == 0)
    if((simActor.flags & FLAG_SIM_ACTOR_SLEEPING) == 0)
        vel.acceleration += mass * sceneSetting.gravitation;

    velocityActors[indexDynamicData] = vel;

    //update dynamic data
    DataDynamicActor updatedDynamicData = mainDynamicData;
    DataActorOrientation orientation = actorOrientations[indexActor];

    mat3 rot = quaternionToMat3(orientation.rotation);
    updatedDynamicData.inertialTensor = rot * updatedDynamicData.inertialTensor * transpose(rot);
    updatedDynamicData.inverseInertialTensor = rot * updatedDynamicData.inverseInertialTensor * transpose(rot);

    outputDynamicActors[indexDynamicData] = updatedDynamicData;
}