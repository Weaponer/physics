#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

layout(std430, set = 0, binding = 0) readonly buffer maxCountContainersBuffer
{
    uint maxCountContainers;
};

ACTOR_ORIENTAIONS_DATA_R(0, 1)
VELOCITY_ACTORS_DATA_R(0, 2)
DYNAMIC_ACTORS_DATA_R(0, 3)
STORAGE_BUFFER_R(0, 4, inputSimContainers, CommonDataContactsContainer, inSimContainers)      
STORAGE_BUFFER_R(0, 5, inputCommonSimContacts, CommonDataSimContact, inCommonSimContacts)      

STORAGE_BUFFER_R(0, 6, inputContainerLoopsData, ivec2, inContainerLoopsData)      
STORAGE_BUFFER_R(0, 7, inputSimActors, DataSimActor, inSimActors)
STORAGE_BUFFER_R(0, 8, inputUsedEdge, int, inUsedEdge)      
STORAGE_BUFFER_R(0, 9, inputGraphEdge, DataActorGraphEdge_2, inGraphEdge)      
STORAGE_BUFFER_W(0, 10, outputSimContactsContainerFinished, DataSimContactsContainerFinished, outSimContactsContainerFinished)   //output
STORAGE_BUFFER_W(0, 11, outputSimContactModels, DataSimContactModel, outSimContactsModels)                                      //output
STORAGE_BUFFER_W(0, 12, outputContainerVelocities, AccamulatedContainerVelocity, outContainerVelocities)                        //output

SCENE_SETTING_DATA(0, 13)

mat3 mulJacobi(mat3 JLMinv0, mat3 JCMinv0, mat3 JL0, mat3 JC0)
{
    JLMinv0 = transpose(JLMinv0);
    JCMinv0 = transpose(JCMinv0);
    JL0 = transpose(JL0);
    JC0 = transpose(JC0);
    float m[9];
    m[0] = JLMinv0[0][0] * JL0[0][0] + JLMinv0[0][1] * JL0[0][1] + JLMinv0[0][2] * JL0[0][2] + JCMinv0[0][0] * JC0[0][0] + JCMinv0[0][1] * JC0[0][1] + JCMinv0[0][2] * JC0[0][2];
    m[1] = JLMinv0[0][0] * JL0[1][0] + JLMinv0[0][1] * JL0[1][1] + JLMinv0[0][2] * JL0[1][2] + JCMinv0[0][0] * JC0[1][0] + JCMinv0[0][1] * JC0[1][1] + JCMinv0[0][2] * JC0[1][2];
    m[2] = JLMinv0[0][0] * JL0[2][0] + JLMinv0[0][1] * JL0[2][1] + JLMinv0[0][2] * JL0[2][2] + JCMinv0[0][0] * JC0[2][0] + JCMinv0[0][1] * JC0[2][1] + JCMinv0[0][2] * JC0[2][2];

    m[3] = JLMinv0[1][0] * JL0[0][0] + JLMinv0[1][1] * JL0[0][1] + JLMinv0[1][2] * JL0[0][2] + JCMinv0[1][0] * JC0[0][0] + JCMinv0[1][1] * JC0[0][1] + JCMinv0[1][2] * JC0[0][2];
    m[4] = JLMinv0[1][0] * JL0[1][0] + JLMinv0[1][1] * JL0[1][1] + JLMinv0[1][2] * JL0[1][2] + JCMinv0[1][0] * JC0[1][0] + JCMinv0[1][1] * JC0[1][1] + JCMinv0[1][2] * JC0[1][2];
    m[5] = JLMinv0[1][0] * JL0[2][0] + JLMinv0[1][1] * JL0[2][1] + JLMinv0[1][2] * JL0[2][2] + JCMinv0[1][0] * JC0[2][0] + JCMinv0[1][1] * JC0[2][1] + JCMinv0[1][2] * JC0[2][2];
    
    m[6] = JLMinv0[2][0] * JL0[0][0] + JLMinv0[2][1] * JL0[0][1] + JLMinv0[2][2] * JL0[0][2] + JCMinv0[2][0] * JC0[0][0] + JCMinv0[2][1] * JC0[0][1] + JCMinv0[2][2] * JC0[0][2];
    m[7] = JLMinv0[2][0] * JL0[1][0] + JLMinv0[2][1] * JL0[1][1] + JLMinv0[2][2] * JL0[1][2] + JCMinv0[2][0] * JC0[1][0] + JCMinv0[2][1] * JC0[1][1] + JCMinv0[2][2] * JC0[1][2];
    m[8] = JLMinv0[2][0] * JL0[2][0] + JLMinv0[2][1] * JL0[2][1] + JLMinv0[2][2] * JL0[2][2] + JCMinv0[2][0] * JC0[2][0] + JCMinv0[2][1] * JC0[2][1] + JCMinv0[2][2] * JC0[2][2];
    return mat3(m[0], m[1], m[2],
                m[3], m[4], m[5],
                m[6], m[7], m[8]);
}


layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexContainer = gl_GlobalInvocationID.x;

    if(gl_GlobalInvocationID.x >= maxCountContainers)
        return;

    CommonDataContactsContainer container = inSimContainers[indexContainer];

    float invMasses[2];
    mat3 invInertialTensors[2];

    invMasses[0] = 0.0;
    invInertialTensors[0] = mat3(0);

    if(container.actorA != -1)
    {
        DataDynamicActor data = dynamicActors[container.actorA];
        invMasses[0] = 1.0 / data.mass;
        invInertialTensors[0] = data.inverseInertialTensor;
    }

    invMasses[1] = 0.0;
    invInertialTensors[1] = mat3(0);

    if(container.actorB != -1)
    {
        DataDynamicActor data = dynamicActors[container.actorB];
        invMasses[1] = 1.0 / data.mass;
        invInertialTensors[1] = data.inverseInertialTensor;
    }

    DataSimContactsContainerFinished finishedData;
    {
        mat3 m = invInertialTensors[0];
        finishedData.invMassTensorA[0] = vec4(m[0][0], m[0][1], m[0][2], invMasses[0]);
        finishedData.invMassTensorA[1] = vec4(m[1][0], m[1][1], m[1][2], 0);
        finishedData.invMassTensorA[2] = vec4(m[2][0], m[2][1], m[2][2], 0);
    }
    {
        mat3 m = invInertialTensors[1];
        finishedData.invMassTensorB[0] = vec4(m[0][0], m[0][1], m[0][2], invMasses[1]);
        finishedData.invMassTensorB[1] = vec4(m[1][0], m[1][1], m[1][2], 0);
        finishedData.invMassTensorB[2] = vec4(m[2][0], m[2][1], m[2][2], 0);
    }

    finishedData.indexFirstContact = container.indexFirstContact;
    finishedData.countContacts = container.countContacts;
    finishedData.flagsEdge = inGraphEdge[indexContainer].flags;
    finishedData.indexGraph = inUsedEdge[indexContainer];

    finishedData.startListContainersA = -1;
    finishedData.startSwapActorA = 0;

    if(container.actorA != -1)
    {
        DataSimActor actor = inSimActors[container.actorA];
        finishedData.startListContainersA = actor.indexStartLoopContainers;
        if(actor.isStartLikeActorB != 0)
            finishedData.startSwapActorA = 1;
    }

    finishedData.startListContainersB = -1;
    finishedData.startSwapActorB = 0;

    if(container.actorB != -1)
    {
        DataSimActor actor = inSimActors[container.actorB];
        finishedData.startListContainersB = actor.indexStartLoopContainers;
        if(actor.isStartLikeActorB == 0)
            finishedData.startSwapActorB = 1;
    }

    outSimContactsContainerFinished[indexContainer] = finishedData;

    //calculate contacts 
    if(finishedData.indexGraph != -1)
    {
        DataActorOrientation actorOrientationA = actorOrientations[container.orientationActorA]; 
        DataActorOrientation actorOrientationB = actorOrientations[container.orientationActorB]; 

        vec3 positionActorA = actorOrientationA.position.xyz;
        vec3 positionActorB = actorOrientationB.position.xyz;
        mat3 rotateActorA = quaternionToMat3(actorOrientationA.rotation);
        mat3 rotateActorB = quaternionToMat3(actorOrientationB.rotation);

        DataDynamicActor dataA;
        DataDynamicActor dataB;
        DataVelocityActor velA;
        DataVelocityActor velB;

        float invMassA;
        float invMassB;

        if(container.actorA != -1)
        {
            dataA = dynamicActors[container.actorA];
            velA = velocityActors[container.actorA];
            invMassA = 1.0 / dataA.mass;
        }
        else
        {
            dataA.mass = 0;
            invMassA = 0;
            dataA.centerMass = vec3(0);
            dataA.inertialTensor = mat3(0);
            dataA.inverseInertialTensor = mat3(0);

            velA.acceleration = vec3(0);
            velA.angularAcceleration = vec3(0);
            velA.velocity = vec3(0);
            velA.angularVelocity = vec3(0);
        }

        if(container.actorB != -1)
        {
            dataB = dynamicActors[container.actorB];
            velB = velocityActors[container.actorB];
            invMassB = 1.0 / dataB.mass;
        }
        else
        {
            dataB.mass = 0;
            invMassB = 0;
            dataB.centerMass = vec3(0);
            dataB.inertialTensor = mat3(0);
            dataB.inverseInertialTensor = mat3(0);

            velB.acceleration = vec3(0);
            velB.angularAcceleration = vec3(0);
            velB.velocity = vec3(0);
            velB.angularVelocity = vec3(0);
        }

        vec3 globalCenterOfMassA = positionActorA + (dataA.centerMass * rotateActorA);
        vec3 globalCenterOfMassB = positionActorB + (dataB.centerMass * rotateActorB);

        for(uint i = 0; i < container.countContacts; i++)
        {
            uint indexContact = container.indexFirstContact + i;
            CommonDataSimContact contact = inCommonSimContacts[indexContact];

            vec3 tangent = vec3(0);

            vec3 normal = normalize(contact.normalAtoB);
            if(abs(dot(normal, vec3(0, 1, 0))) > 0.9)
                tangent = normalize(cross(normal, vec3(1, 0, 0)));
            else
                tangent = normalize(cross(normal, vec3(0, 1, 0)));
            vec3 bitanget = normalize(cross(normal, tangent));
            tangent = normalize(cross(bitanget, normal));

            vec3 rA = contact.positionA - globalCenterOfMassA;
            vec3 rB = contact.positionB - globalCenterOfMassB;

            mat3 JL0 = mat3(0);
            JL0[0] = normal;
            JL0[1] = tangent;
            JL0[2] = bitanget;
            JL0 = transpose(JL0);
            mat3 JL1 = mat3(0);
            JL1[0] = -normal;
            JL1[1] = -tangent;
            JL1[2] = -bitanget;
            JL1 = transpose(JL1);

            mat3 rAm = mat3(0, -rA.z, rA.y,
                            rA.z, 0, -rA.x,
                            -rA.y, rA.x, 0);

            mat3 rBm = mat3(0, -rB.z, rB.y,
                            rB.z, 0, -rB.x,
                            -rB.y, rB.x, 0);

            mat3 JC0 = JL0 * rAm;
            mat3 JC1 = JL1 * rBm;

            mat3 JLMinv0 = invMassA * JL0;
            mat3 JLMinv1 = invMassB * JL1;
            mat3 JCMinv0 = JC0 * dataA.inverseInertialTensor;
            mat3 JCMinv1 = JC1 * dataB.inverseInertialTensor;

            vec3 velocity = vec3(0, 0, 0);
            velocity = -(JL0 * velA.velocity + JC0 * velA.angularVelocity)
                        -(JL1 * velB.velocity + JC1 * velB.angularVelocity)
                        -(sceneSetting.deltaTime * (JLMinv0 * velA.acceleration + JCMinv0 * velA.angularAcceleration));
                        -(sceneSetting.deltaTime * (JLMinv1 * velB.acceleration + JCMinv1 * velB.angularAcceleration));

            float friction = 0;
            if(length(velocity) >= sceneSetting.frictionStaticLimit)
                friction = container.dynamicFriction;
            else
                friction = container.staticFriction;

            float depth = contact.depth;
            float depthContact = max(-sceneSetting.depthContactMax, min(-depth + sceneSetting.depthContactMin, 0.0));

            vec3 spring = ((sceneSetting.deltaTime * sceneSetting.stiffness) / 
                            (sceneSetting.deltaTime * sceneSetting.stiffness + sceneSetting.damping)) * 
                            vec3(depthContact, 0, 0) * (1.0 / sceneSetting.deltaTime);

            velocity = (velocity - spring) + (velocity + spring) * container.bounciness;

            mat3 A = mat3(0);
            if(container.actorA != -1)
                A += mulJacobi(JLMinv0, JCMinv0, JL0, JC0);
            if(container.actorB != -1)
                A += mulJacobi(JLMinv1, JCMinv1, JL1, JC1);

            A[0][0] += 1.0 / (sceneSetting.stiffness * sceneSetting.deltaTime * sceneSetting.deltaTime + sceneSetting.deltaTime * sceneSetting.damping);

            DataSimContactModel model;
            model.basis[0] = vec4(normal.x, normal.y, normal.z, rA.x);
            model.basis[1] = vec4(tangent.x, tangent.y, tangent.z, rA.y);
            model.basis[2] = vec4(bitanget.x, bitanget.y, bitanget.z, rA.z);

            model.A[0] = vec4(A[0][0], A[0][1], A[0][2], rB.x);
            model.A[1] = vec4(A[1][0], A[1][1], A[1][2], rB.y);
            model.A[2] = vec4(A[2][0], A[2][1], A[2][2], rB.z);


            vec3 pointContact = (contact.positionA + contact.positionB) / 2.0;

            model.velocity = vec4(velocity, friction);
            model.position = vec4(pointContact, depthContact);

            model.testData = vec4(contact.normalAtoB.x, indexContact, depthContact, dataA.mass);

            outSimContactsModels[indexContact] = model;
        }
    }

    //loop data init
    {
        ivec2 loopDataA = inContainerLoopsData[indexContainer * 2];
        ivec2 loopDataB = inContainerLoopsData[indexContainer * 2 + 1];

        AccamulatedContainerVelocity velocitiesData;
        velocitiesData.velocities[0] = vec4(0);
        velocitiesData.velocities[1] = vec4(0);
        velocitiesData.velocities[2] = vec4(0);

        int needToSwap = 0;
        if(loopDataA.x != -1 && loopDataA.y != 0)
            needToSwap |= 1;
        if(loopDataB.x != -1 && loopDataB.y != 0)
            needToSwap |= 2;

        velocitiesData.subData = ivec4(loopDataA.x, loopDataB.x, finishedData.indexGraph, needToSwap);
        outContainerVelocities[indexContainer] = velocitiesData;
    }
}