#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

STORAGE_BUFFER_R(0, 0, inputActorsUpdateIndexes, uint, inActorsUpdateIndexes)           

STORAGE_BUFFER_R(0, 1, inputSimActors, DataSimActor, inSimActors)
STORAGE_BUFFER_R(0, 2, inputContainerVelocities, AccamulatedContainerVelocity, inContainerVelocities)

ACTORS_DATA(0, 3)
DYNAMIC_ACTORS_DATA_R(0, 4)

VELOCITY_ACTORS_DATA(0, 5)      //in out
ACTOR_ORIENTAIONS_DATA(0, 6)    //in out
SLEEPING_ACTORS_DATA(0, 7)      //in out

SCENE_SETTING_DATA(0, 8)

layout(r32ui, set = 0, binding = 9) uniform uimage1D errorFlag;

layout(push_constant, std430) uniform Constants
{
    uint maxCountUpdateActors;
    int maxIndexVelocities;
} pConstants;

bool errorImpulse(vec3 impulse)
{
    if(isnan(impulse.x) || isnan(impulse.y) || isnan(impulse.z) || 
        isinf(impulse.x) || isinf(impulse.y) || isinf(impulse.z))
        return true;
    else
        return false;
}

void setErrorFlag()
{
    imageStore(errorFlag, 0, uvec4(1));
}

layout (local_size_x = SIZE_GROUP_SIMULATE_ACTORS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexActor = gl_GlobalInvocationID.x;

    if(indexActor >= pConstants.maxCountUpdateActors)
        return;

    bool error = imageLoad(errorFlag, 0).x == 0;

    indexActor = inActorsUpdateIndexes[indexActor];
    DataActor actor = actors[indexActor];
    uint indexDynamicData = actor.indexAdditionalData;

    DataSimActor simActor = inSimActors[indexDynamicData];

    vec3 lImpulse = vec3(0, 0, 0);
    vec3 cImpulse = vec3(0, 0, 0);

    //read sum velocities from other containers
    {
        vec3 linearVels[2];
        linearVels[0] = vec3(0);
        linearVels[1] = vec3(0);
        vec3 angularVels[2];
        angularVels[0] = vec3(0);
        angularVels[1] = vec3(0);

        int next = simActor.indexStartLoopContainers;
        bool needSwap = simActor.isStartLikeActorB != 0;
        int nextIndexRead = needSwap ? 1 : 0;
        while(next != -1 && error)
        {
            if(next >= pConstants.maxIndexVelocities)
                break;

            AccamulatedContainerVelocity velocities = inContainerVelocities[next];

            linearVels[0] = velocities.velocities[0].xyz;
            angularVels[0] = velocities.velocities[1].xyz;
            linearVels[1] = velocities.velocities[2].xyz;
            angularVels[1] = vec3(velocities.velocities[0].w, velocities.velocities[1].w, velocities.velocities[2].w);

            lImpulse += linearVels[nextIndexRead] / sceneSetting.deltaTime;
            cImpulse += angularVels[nextIndexRead] / sceneSetting.deltaTime;

            next = velocities.subData[nextIndexRead];
            needSwap = (velocities.subData.w & (1 << nextIndexRead)) != 0;
            nextIndexRead = needSwap ? 1 - nextIndexRead : nextIndexRead;
        }
    }

    if(errorImpulse(lImpulse) || errorImpulse(cImpulse))
    {
        //setErrorFlag();
        return;
    }
    
    DataDynamicActor data = dynamicActors[indexDynamicData];
    DataActorOrientation orientation = actorOrientations[indexActor];
    DataVelocityActor vel = velocityActors[indexDynamicData];
    mat3 rot = quaternionToMat3(orientation.rotation);
    vec3 globalCenterOfMass = orientation.position.xyz + (data.centerMass * rot);

    vel.velocity += sceneSetting.deltaTime * (1.0 / data.mass) * (vel.acceleration + lImpulse);
    vel.angularVelocity += sceneSetting.deltaTime *  data.inverseInertialTensor * 
                             (vel.angularAcceleration + cImpulse - cross(vel.angularVelocity, data.inertialTensor * vel.angularVelocity));

    vel.acceleration = vec3(0);
    vel.angularAcceleration = vec3(0);
    
    vel.velocity = vel.velocity * max(0, 1 - sceneSetting.deltaTime * (data.linearDrag + sceneSetting.linearDamping));
    vel.angularVelocity = vel.angularVelocity * max(0, 1 - sceneSetting.deltaTime * (data.angularDrag + sceneSetting.angularDamping));

    DataSleepingActor sleeping = sleepingActors[indexDynamicData];

    if((length(vel.velocity) >= sceneSetting.maxVelocitySleep || 
        length(vel.angularVelocity) >= sceneSetting.maxAngularVelocitySleep))
    {
        sleeping.sleepTime = 0.0;
        sleeping.isSleeping = 0;
    }
    else
    {
        if(sleeping.isSleeping == 0)
            sleeping.sleepTime += sceneSetting.deltaTime;
    }
    
    if(sleeping.sleepTime >= sceneSetting.minSleepTime)
    {
        sleeping.isSleeping = 1;
        vel.velocity = vec3(0, 0, 0);
        vel.angularVelocity = vec3(0, 0, 0);
    }

    if((simActor.flags & FLAG_SIM_ACTOR_TRY_WAKE) != 0 && sleeping.isSleeping != 0)
    {
        sleeping.sleepTime = 0;
        sleeping.isSleeping = 0;
    }

    sleepingActors[indexDynamicData] = sleeping;

    if(sleeping.isSleeping == 0)
    {
        globalCenterOfMass += sceneSetting.deltaTime * vel.velocity;
        vec4 q = orientation.rotation;
        q = quaternionAddScaledVector(q, vel.angularVelocity, sceneSetting.deltaTime);

        q = normalize(q);           
        orientation.rotation = q;
        rot = quaternionToMat3(q);

        orientation.position.xyz = globalCenterOfMass - (data.centerMass * rot);
    }

    if(sceneSetting.updateVelocities != 0 && error)
    {
        velocityActors[indexDynamicData] = vel;
        actorOrientations[indexActor] = orientation;
    }
}