#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

STORAGE_BUFFER_R(0, 0, outputIndexesRootEdge, uint, outIndexesRootEdge)
STORAGE_BUFFER_R(0, 1, inputGraphEdge, DataActorGraphEdge_2, inGraphEdge)  

STORAGE_BUFFER_R(0, 3, inputSimActors, DataSimActor, inSimActors)
STORAGE_BUFFER_R(0, 4, inputActorIndexesContainers, uint, inActorIndexesContainers) 

STORAGE_BUFFER(0, 5, inputUsedEdge, int, inUsedEdge)         //output
STORAGE_BUFFER_W(0, 6, inputLockedActor, int, inLockedActor) //output

layout(push_constant, std430) uniform Constants
{
    int indexCurrentStage;
} pConstants;

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexEdge = gl_GlobalInvocationID.x;

    indexEdge = outIndexesRootEdge[indexEdge];
    DataActorGraphEdge_2 edge = inGraphEdge[indexEdge];
    if(inUsedEdge[indexEdge] != pConstants.indexCurrentStage)
        return;

    int nextNodeCheck[2];
    nextNodeCheck[0] = edge.nodeA;
    nextNodeCheck[1] = edge.nodeB;

    bool addNextToGroup[2];
    addNextToGroup[0] = false;
    addNextToGroup[1] = false;

    int countChecked = 0;
    bool wasMoved = true;
    while(wasMoved && countChecked < MAX_CHECKED_EDGES_FROM_ROOT)
    {
        wasMoved = false;

        for(int i = 0; i < 2; i++)
        {
            int node = nextNodeCheck[i];
            if(node == -1)
                continue;

            nextNodeCheck[i] = -1;
            
            DataSimActor actor = inSimActors[node];

            for(int i2 = 0; i2 < actor.countContainers; i2++)
            {
                uint checkEdge = inActorIndexesContainers[actor.indexFirstContainer + i2];
                if(inUsedEdge[checkEdge] != -1)
                    continue;
                    
                DataActorGraphEdge_2 edge = inGraphEdge[checkEdge];
                
                int otherNode = edge.nodeA;
                if(node == otherNode )
                    otherNode = edge.nodeB;
                
                if(otherNode != -1)
                {
                    int old = atomicCompSwap(inLockedActor[otherNode], -1, int(indexEdge));
                    if(old != -1)
                        continue;
                }
            
                nextNodeCheck[i] = otherNode;

                countChecked++;
                wasMoved = true;

                if(addNextToGroup[i])
                    inUsedEdge[checkEdge] = pConstants.indexCurrentStage;
                
                break;
            }
        }

        addNextToGroup[0] = !addNextToGroup[0];
        addNextToGroup[1] = !addNextToGroup[1];
    }
}