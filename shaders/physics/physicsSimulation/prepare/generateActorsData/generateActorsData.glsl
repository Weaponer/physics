#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

ACTORS_DATA(0, 0)
SHAPES_DATA(0, 1)
SLEEPING_ACTORS_DATA_R(0, 2)
STORAGE_BUFFER_R(0, 3, inputActorLostContainer, uint, inActorLostContainer)

STORAGE_BUFFER_R(0, 4, inputActorsUpdateIndexes, uint, inActorsUpdateIndexes)   
STORAGE_BUFFER_R(0, 5, inputSimContainerNodes, DataSimContainerNode, inSimContainerNodes)
STORAGE_BUFFER_R(0, 6, inputSimContainers, CommonDataContactsContainer, inSimContainers)
STORAGE_BUFFER_R(0, 7, inputActorHeadContainers, int, inActorHeadContainers)

STORAGE_BUFFER_W(0, 8, outputActorIndexesContainers, uint, outActorIndexesContainers)       //output   
STORAGE_BUFFER_W(0, 9, outputContainerLoopsData, ivec2, outContainerLoopsData)              //output      
STORAGE_BUFFER_W(0, 10, outputSimActors, DataSimActor, outSimActors)                        //output

layout(r32ui, set = 0, binding = 11) uniform uimage1D indexesContainersCounter;             //output
layout(r32ui, set = 0, binding = 12) uniform uimage1D maxContainersOnActorCounter;          //output

layout(push_constant, std430) uniform Constants
{
    uint maxCountUpdateActors;
} pConstants;

layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexActor = gl_GlobalInvocationID.x;

    if(indexActor >= pConstants.maxCountUpdateActors)
        return;

    indexActor = inActorsUpdateIndexes[indexActor];
    DataActor actor = actors[indexActor];
    uint indexDynamicData = actor.indexAdditionalData;

    uint readContainersNodes[MAX_CONTAINERS_ON_ACTOR];
    float readContainersNodesVelocity[MAX_CONTAINERS_ON_ACTOR];
    int allContainersSleep = 1;
    int hasNewContainers = 0;

    int countReadContainers = 0;
    {
        int next = inActorHeadContainers[indexDynamicData];
        while(next != -1 && countReadContainers < MAX_CONTAINERS_ON_ACTOR)
        {
            DataSimContainerNode node = inSimContainerNodes[next];
            next = node.next;
            readContainersNodes[countReadContainers] = node.indexContainer;
            CommonDataContactsContainer container = inSimContainers[node.indexContainer];

            allContainersSleep &= (container.flags & (1 << CONTACTS_CONTAINER_MASK_OFFSET_BITS_SLEEP_ACTORS)) != 0 ? 1 : 0;
            hasNewContainers |= (container.flags & (1 << CONTACTS_CONTAINER_MASK_OFFSET_BITS_NEW)) != 0 ? 1 : 0;

            readContainersNodesVelocity[countReadContainers] = container.averageVelocity;
            countReadContainers++;
        }
    }

    bool tryWake = false;
    bool actorSleep = false;
    //check sleeping
    {
        DataSleepingActor sleeping = sleepingActors[indexDynamicData];
        uint actorLostContainer = inActorLostContainer[indexActor];
        
        if(sleeping.isSleeping != 0)
            actorSleep = true;

        if((actorLostContainer != 0 || hasNewContainers != 0) && actorSleep)
            tryWake = true;
    }

    const int rangeSort = countReadContainers - 1;
    for(int i = 0; i < rangeSort; i++)
    {
        float maxElement = readContainersNodesVelocity[i];
        uint maxElementIndex = i;
        for(int i2 = i + 1; i2 < countReadContainers; i2++)
        {
            if(readContainersNodesVelocity[i2] > maxElement)
            {
                maxElementIndex = i2;
                maxElement = readContainersNodesVelocity[i2];
            }
        }

        float temp = readContainersNodesVelocity[i];
        readContainersNodesVelocity[i] = readContainersNodesVelocity[maxElementIndex];
        readContainersNodesVelocity[maxElementIndex] = temp;

        uint tempNode = readContainersNodes[i];
        readContainersNodes[i] = readContainersNodes[maxElementIndex];
        readContainersNodes[maxElementIndex] = tempNode;
    }

    imageAtomicMax(maxContainersOnActorCounter, 0, uint(countReadContainers));
    uint pWrite = imageAtomicAdd(indexesContainersCounter, 0, uint(countReadContainers));
    for(int i = 0; i < countReadContainers; i++)
    {
        outActorIndexesContainers[pWrite + i] = readContainersNodes[i];
    }


    int indexStartLoopContainers = -1;
    uint isStartLikeActorB = 0; 

    //sort by index
    {
        for(int i = 0; i < rangeSort; i++)
        {
            uint minElement = readContainersNodes[i];
            uint minElementIndex = i;
            for(int i2 = i + 1; i2 < countReadContainers; i2++)
            {
                if(readContainersNodes[i2] < minElement)
                {
                    minElementIndex = i2;
                    minElement = readContainersNodes[i2];
                }
            }

            uint tempNode = readContainersNodes[i];
            readContainersNodes[i] = readContainersNodes[minElementIndex];
            readContainersNodes[minElementIndex] = tempNode;
        }

        if(countReadContainers == 1)
        {
            CommonDataContactsContainer container = inSimContainers[readContainersNodes[0]];
            indexStartLoopContainers = int(readContainersNodes[0]);
                if(container.actorB == indexDynamicData)
                    isStartLikeActorB = 1;
        }

        for(int i = 1; i < countReadContainers; i++)
        {
            uint indexLastContainer = readContainersNodes[i - 1];
            uint indexNextContainer = readContainersNodes[i];

            CommonDataContactsContainer lastContainer = inSimContainers[readContainersNodes[i - 1]];
            CommonDataContactsContainer nextContainer = inSimContainers[readContainersNodes[i]];
            if(lastContainer.actorA == indexDynamicData)
            {
                if(nextContainer.actorA != indexDynamicData)
                    outContainerLoopsData[indexLastContainer * 2] = ivec2(indexNextContainer, 1);
                else
                    outContainerLoopsData[indexLastContainer * 2] = ivec2(indexNextContainer, 0);
            }
            else
            {
                if(nextContainer.actorB != indexDynamicData)
                    outContainerLoopsData[indexLastContainer * 2 + 1] = ivec2(indexNextContainer, 1);
                else
                    outContainerLoopsData[indexLastContainer * 2 + 1] = ivec2(indexNextContainer, 0);
            }

            if(i == 1)
            {
                indexStartLoopContainers = int(indexLastContainer);
                if(lastContainer.actorB == indexDynamicData)
                    isStartLikeActorB = 1;
            }
        }
    }


    bool isHardGeometry = false;
    if(actor.countShapes > 1)
    {
        isHardGeometry = true;
    }
    else if(actor.countShapes > 0)
    {   
        DataShape shape = shapes[actor.indexFirstShape];
        if(shape.countGeometries > 1)
            isHardGeometry = true;
    }
    DataSimActor simActor;
    simActor.indexMainActor = indexActor;
    simActor.countContainers = countReadContainers;
    simActor.indexFirstContainer = pWrite;
    simActor.indexStartLoopContainers = indexStartLoopContainers;
    simActor.isStartLikeActorB = isStartLikeActorB;
    simActor.flags = FLAG_SIM_ACTOR_IS_USED;
    if(isHardGeometry)
        simActor.flags |= FLAG_SIM_ACTOR_IS_HARD_GEOMETRY;
    if(actorSleep)
        simActor.flags |= FLAG_SIM_ACTOR_SLEEPING;
    if(tryWake)
        simActor.flags |= FLAG_SIM_ACTOR_TRY_WAKE;
    
    outSimActors[indexDynamicData] = simActor;
}