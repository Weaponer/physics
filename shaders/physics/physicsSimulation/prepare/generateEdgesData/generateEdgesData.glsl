#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

STORAGE_BUFFER_R(0, 0, inputSimContainers, CommonDataContactsContainer, inSimContainers)
STORAGE_BUFFER_R(0, 1, inputSimActors, DataSimActor, inSimActors)
STORAGE_BUFFER_W(0, 2, outputGraphEdge, DataActorGraphEdge_2, outGraphEdge)                     //output

layout(std430, set = 0, binding = 3) readonly buffer maxCountContainersBuffer
{
    uint maxCountContainers;
};

bool IsContactWithNoDynamic(CommonDataContactsContainer container)
{   
    return container.actorA == -1 || container.actorB == -1;
}

bool IsUsedEdge(CommonDataContactsContainer container, DataSimActor actorA, DataSimActor actorB)
{   
    bool isSleeping = true;
    bool someOneTryWake = false;
    if(container.actorA != -1)
    {
        isSleeping = (actorA.flags & FLAG_SIM_ACTOR_SLEEPING) != 0 && isSleeping;
        someOneTryWake = (actorA.flags & FLAG_SIM_ACTOR_TRY_WAKE) != 0 || someOneTryWake;
    }
    if(container.actorB != -1)
    {
        isSleeping = (actorB.flags & FLAG_SIM_ACTOR_SLEEPING) != 0 && isSleeping;
        someOneTryWake = (actorB.flags & FLAG_SIM_ACTOR_TRY_WAKE) != 0 || someOneTryWake;
    }

    if(isSleeping)
       return false;
    else
        return true;
}

bool IsBorderEdge(CommonDataContactsContainer container, DataSimActor actorA, DataSimActor actorB)
{   
    if(container.actorA == -1 || container.actorB == -1)
        return true;

    if(actorA.countContainers == 1 || actorB.countContainers == 1)
        return true;
    
    return false;
}

layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexContainer = gl_GlobalInvocationID.x;

    if(indexContainer >= maxCountContainers)
        return;

    CommonDataContactsContainer container = inSimContainers[indexContainer];
    DataSimActor actorA;
    DataSimActor actorB;
    if(container.actorA != -1)
        actorA = inSimActors[container.actorA];
    if(container.actorB != -1)
        actorB = inSimActors[container.actorB];

    bool useContainer = IsUsedEdge(container, actorA, actorB);
    bool haveContactWithKinematic = IsContactWithNoDynamic(container);
    bool isBorder = IsBorderEdge(container, actorA, actorB);
    DataActorGraphEdge_2 edge;
    edge.nodeA = container.actorA;
    edge.nodeB = container.actorB;
    edge.totalForceContacts = container.averageVelocity;
    edge.flags = (useContainer ? FLAG_GRAP_NODE_IS_USED : 0) | 
                (haveContactWithKinematic ? FLAG_GRAP_NODE_HAVE_KINEMATIC_CONTACT : 0) |
                (isBorder ? FLAG_GRAP_NODE_IS_BORDER : 0);

    outGraphEdge[indexContainer] = edge;
}