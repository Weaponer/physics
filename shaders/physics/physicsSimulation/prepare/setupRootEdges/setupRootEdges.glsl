#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

STORAGE_BUFFER_R(0, 0, outputIndexesRootEdge, uint, outIndexesRootEdge)
STORAGE_BUFFER_R(0, 1, inputGraphEdge, DataActorGraphEdge_2, inGraphEdge)  
STORAGE_BUFFER_W(0, 2, inputLockedActor, int, inLockedActor)                //output
STORAGE_BUFFER_W(0, 3, inputUsedEdge, int, inUsedEdge)                      //output

layout(push_constant, std430) uniform Constants
{
    int indexCurrentStage;
} pConstants;

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexEdge = gl_GlobalInvocationID.x;

    indexEdge = outIndexesRootEdge[indexEdge];
    DataActorGraphEdge_2 edge = inGraphEdge[indexEdge];

    if(edge.nodeA != -1)
    {
        int old = atomicCompSwap(inLockedActor[edge.nodeA], -1, int(indexEdge));
        if(old != -1)
            return;
    }

    if(edge.nodeB != -1)
    {
        int old = atomicCompSwap(inLockedActor[edge.nodeB], -1, int(indexEdge));
        if(old != -1)
        {
            if(edge.nodeA != -1)
                atomicCompSwap(inLockedActor[edge.nodeA], int(indexEdge), -1);  //restore A actor

            return;
        }
    }
    
    inUsedEdge[indexEdge] = pConstants.indexCurrentStage;
}