#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

#define VELOCITY_CONTACT_EPSILON 0.1

layout(std430, set = 0, binding = 0) readonly buffer maxCountEdgesBuffer
{
    uint maxCountEdges;
};

STORAGE_BUFFER_R(0, 1, inputSimActors, DataSimActor, inSimActors)
STORAGE_BUFFER_R(0, 2, inputGraphEdge, DataActorGraphEdge_2, inGraphEdge)
STORAGE_BUFFER_R(0, 3, inputActorIndexesContainers, uint, inActorIndexesContainers)

STORAGE_BUFFER_R(0, 4, inputLockedActor, int, inLockedActor) 
STORAGE_BUFFER_R(0, 5, inputUsedEdge, int, inUsedEdge) 

STORAGE_BUFFER_W(0, 6, outputIndexesRootEdge, uint, outIndexesRootEdge)    //output

layout(r32ui, set = 0, binding = 7) uniform uimage1D indexesRootEdgeCounter;//output

bool isEdgeLocked(DataActorGraphEdge_2 edge, uint indexEdge)
{
    if(inUsedEdge[indexEdge] != -1)
        return true;

    if(edge.nodeA != -1 && inLockedActor[edge.nodeA] != -1)
        return true;

    if(edge.nodeB != -1 && inLockedActor[edge.nodeB] != -1)
        return true;

    return false;
}

bool isNotRootEdge(DataActorGraphEdge_2 mainEdge, uint indexMainEdge, 
                    DataActorGraphEdge_2 otherEdge, uint indexOtherEdge)
{
    if(isEdgeLocked(otherEdge, indexOtherEdge))
        return false;

    if(indexMainEdge == indexOtherEdge)
        return false;

    
    int flagOne = mainEdge.flags;
    int flagTwo = otherEdge.flags;

    if(flagOne < flagTwo)
        return true;
    else if(flagOne > flagTwo)
        return false;

    if(indexMainEdge < indexOtherEdge)
        return true;

    return false;
}

layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexEdge = gl_GlobalInvocationID.x;

    if(indexEdge >= maxCountEdges)
        return;

    DataActorGraphEdge_2 edge = inGraphEdge[indexEdge];

    if((edge.flags & FLAG_GRAP_NODE_IS_USED) == 0)
        return;

    if(isEdgeLocked(edge, indexEdge))
        return;

    if(edge.nodeA != -1)
    {
        DataSimActor actor = inSimActors[edge.nodeA];
        for(int i = 0; i < actor.countContainers; i++)
        {
            uint otherEdgeIndex = inActorIndexesContainers[actor.indexFirstContainer + i] & FLAG_INV_INDEX_OF_CONTAINER_FOR_B_ACTOR;
            DataActorGraphEdge_2 otherEdge = inGraphEdge[otherEdgeIndex];
            if(isNotRootEdge(edge, indexEdge, otherEdge, otherEdgeIndex))
                return;
        }
    }

    if(edge.nodeB != -1)
    {
        DataSimActor actor = inSimActors[edge.nodeB];
        for(int i = 0; i < actor.countContainers; i++)
        {
            uint otherEdgeIndex = inActorIndexesContainers[actor.indexFirstContainer + i] & FLAG_INV_INDEX_OF_CONTAINER_FOR_B_ACTOR;
            DataActorGraphEdge_2 otherEdge = inGraphEdge[otherEdgeIndex];
            if(isNotRootEdge(edge, indexEdge, otherEdge, otherEdgeIndex))
                return;
        }
    }

    uint pWrite = imageAtomicAdd(indexesRootEdgeCounter, 0, 1);

    outIndexesRootEdge[pWrite] = indexEdge;
}