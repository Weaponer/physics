#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

CONTACTS_CONTAINERS_DATA_R(0, 0)
CONTACTS_DATA_R(0, 1)
GEOMETRY_OBBS_DATA(0, 2)
ORIENTATION_GEOMETRY_OBBS_DATA_R(0, 3)
SHAPES_DATA(0, 4)
MATEIRLAS_DATA(0, 5)
ACTORS_DATA(0, 6)
ACTOR_ORIENTAIONS_DATA_R(0, 7)
DYNAMIC_ACTORS_DATA_R(0, 8)
VELOCITY_ACTORS_DATA_R(0, 9)

layout(std430, set = 0, binding = 10) readonly buffer maxCountContainersBuffer
{
    uint maxCountContainers;
};

STORAGE_BUFFER_W(0, 11, outputCommonSimContainers, CommonDataContactsContainer, outCommonSimContainers) //output
STORAGE_BUFFER_W(0, 12, outputCommonSimContacts, CommonDataSimContact, outCommonSimContacts)            //output

uint getCountActiveContacts(DataContactsContainer container)
{
    uint maxCount = container.countAndMaskContacts & CONTACTS_CONTAINER_MASK_COUNT_CONTACTS;
    uint maskContacts = (container.countAndMaskContacts >> CONTACTS_CONTAINER_MASK_OFFSET_BITS_USE_CONTACTS) & 
                        CONTACTS_CONTAINER_MASK_USE_CONTACTS;
    uint countActive = 0;
    for(uint i = 0; i < maxCount; i++)
    {
        if((maskContacts & (1 << i)) != 0)
            countActive++;
    }

    return countActive;
}

layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexContainer = gl_GlobalInvocationID.x;

    if(gl_GlobalInvocationID.x >= maxCountContainers)
        return;

    DataContactsContainer container = contactsContainers[indexContainer];
    uint indexObbA = container.firstIndexGeometry;
    uint indexObbB = container.secondIndexGeometry;

    DataGeometryOBB obbA = geometryObbs[indexObbA];
    DataGeometryOBB obbB = geometryObbs[indexObbB];

    DataOrientationGeometryOBB orientationObbA = orientationObbs[indexObbA];
    DataOrientationGeometryOBB orientationObbB = orientationObbs[indexObbB];

    int indexMaterialA = shapes[obbA.indexShape].indexMaterial;
    DataMaterial materialA;
    if(indexMaterialA != -1)
    { 
       materialA = materials[indexMaterialA];
    }
    else
    {
        materialA.staticFriction = 1.0;
        materialA.dynamicFriction = 0.7;
        materialA.bounciness = 0.0;
    }

    int indexMaterialB = shapes[obbB.indexShape].indexMaterial;
    DataMaterial materialB;
    if(indexMaterialB != -1)
    { 
       materialB = materials[indexMaterialB];
    }
    else
    {
        materialB.staticFriction = 1.0;
        materialB.dynamicFriction = 0.7;
        materialB.bounciness = 0.0;
    }

    DataActor actorA = actors[obbA.indexActor];
    DataActor actorB = actors[obbB.indexActor];
    
    DataActorOrientation actorOrientationA = actorOrientations[obbA.indexActor]; 
    DataActorOrientation actorOrientationB = actorOrientations[obbB.indexActor]; 

    CommonDataContactsContainer simContainer;
    simContainer.flags = container.countAndMaskContacts & CONTACTS_CONTAINER_MASK_FLAGS;

    if((actorA.flags & FLAG_ACTOR_DYNAMIC) != 0)
        simContainer.actorA = int(actorA.indexAdditionalData);
    else
        simContainer.actorA = -1;

    if((actorB.flags & FLAG_ACTOR_DYNAMIC) != 0)
        simContainer.actorB = int(actorB.indexAdditionalData);
    else
        simContainer.actorB = -1;

    simContainer.orientationActorA = obbA.indexActor;
    simContainer.orientationActorB = obbB.indexActor;

    simContainer.indexFirstContact = container.firstContactIndex;
    simContainer.countContacts = getCountActiveContacts(container);
    
    simContainer.dynamicFriction = (materialA.dynamicFriction + materialB.dynamicFriction) / 2.0;
    simContainer.staticFriction = (materialA.staticFriction + materialB.staticFriction) / 2.0;
    simContainer.bounciness = (materialA.bounciness + materialB.bounciness) / 2.0;

    vec3 averageVelocity = vec3(0, 0, 0);

    //calculate contacts 
    {
        mat3 rotateObbA = quaternionToMat3(orientationObbA.rotation);
        mat3 rotateObbB = quaternionToMat3(orientationObbB.rotation);

        vec3 positionActorA = actorOrientationA.position.xyz;
        vec3 positionActorB = actorOrientationB.position.xyz;
        mat3 rotateActorA = quaternionToMat3(actorOrientationA.rotation);
        mat3 rotateActorB = quaternionToMat3(actorOrientationB.rotation);

        DataDynamicActor dataA;
        DataDynamicActor dataB;
        DataVelocityActor velA;
        DataVelocityActor velB;

        if(simContainer.actorA != -1)
        {
            dataA = dynamicActors[simContainer.actorA];
            velA = velocityActors[simContainer.actorA];
        }
        else
        {
            velA.acceleration = vec3(0);
            velA.angularAcceleration = vec3(0);
            velA.velocity = vec3(0);
            velA.angularVelocity = vec3(0);
        }

        if(simContainer.actorB != -1)
        {
            dataB = dynamicActors[simContainer.actorB];
            velB = velocityActors[simContainer.actorB];
        }
        else
        {
            velB.acceleration = vec3(0);
            velB.angularAcceleration = vec3(0);
            velB.velocity = vec3(0);
            velB.angularVelocity = vec3(0);
        }

        vec3 globalCenterOfMassA;
        if(simContainer.actorA != -1)
            globalCenterOfMassA = positionActorA + (dataA.centerMass * rotateActorA);
        else
            globalCenterOfMassA = positionActorA;

        vec3 globalCenterOfMassB;
        if(simContainer.actorB != -1)
            globalCenterOfMassB = positionActorB + (dataB.centerMass * rotateActorB);
        else
            globalCenterOfMassB = positionActorB;

        for(uint i = 0; i < simContainer.countContacts; i++)
        {
            uint indexContact = simContainer.indexFirstContact + i;
            DataContact contact = contacts[indexContact];

            vec3 globalPosA = orientationObbA.position.xyz + (contact.firstLocalPosition * rotateObbA);
            vec3 globalPosB = orientationObbB.position.xyz + (contact.secondLocalPosition * rotateObbB);
            vec3 globalNormalAtoB = contact.firstLocalNormal * rotateObbA;
            
            vec3 rA = globalPosA - globalCenterOfMassA;
            vec3 rB = globalPosB - globalCenterOfMassB;

            vec3 velA = velA.velocity + cross(velA.angularVelocity, rA);
            vec3 velB = velB.velocity + cross(velB.angularVelocity, rB);
            
            averageVelocity += velB - velA;

            CommonDataSimContact simContact;
            simContact.positionA = globalPosA;
            simContact.positionB = globalPosB;
            simContact.normalAtoB = globalNormalAtoB;
            simContact.depth = contact.depthPenetration;

            outCommonSimContacts[indexContact] = simContact;
        }

        averageVelocity /= simContainer.countContacts;
    }

    simContainer.averageVelocity = length(averageVelocity);
    outCommonSimContainers[indexContainer] = simContainer;
}