#include "buffers.glsl"
#include "flags.glsl"
#include "limits.glsl"
#include "math.glsl"
#include "simulationStructs.glsl"

layout(std430, set = 0, binding = 0) readonly buffer maxCountContainersBuffer
{
    uint maxCountContainers;
};

STORAGE_BUFFER_R(0, 1, inputSimContainers, CommonDataContactsContainer, inSimContainers)
STORAGE_BUFFER_W(0, 2, outputSimContainerNodes, DataSimContainerNode, outSimContainerNodes)     //output
STORAGE_BUFFER_W(0, 3, outputActorHeadContainers, int, outActorHeadContainers)                  //output

layout(r32ui, set = 0, binding = 4) uniform uimage1D simContainerNodesCounter;                  //output

layout (local_size_x = SIZE_GROUP_SIMULATION_PHYSICS, local_size_y = 1, local_size_z = 1) in;
void main()
{
    uint indexContainer = gl_GlobalInvocationID.x;

    if(gl_GlobalInvocationID.x >= maxCountContainers)
        return;

    CommonDataContactsContainer container = inSimContainers[indexContainer];

   if(container.actorA != -1)
    {
        int pWrite = int(imageAtomicAdd(simContainerNodesCounter, 0, 1U));
        int lastIndex = atomicExchange(outActorHeadContainers[container.actorA], pWrite);

        DataSimContainerNode node;
        node.indexContainer = indexContainer;
        node.next = lastIndex;

        outSimContainerNodes[pWrite] = node;
    }
    if(container.actorB != -1)
    {
        int pWrite = int(imageAtomicAdd(simContainerNodesCounter, 0, 1U));
        int lastIndex = atomicExchange(outActorHeadContainers[container.actorB], pWrite);

        DataSimContainerNode node;
        node.indexContainer = indexContainer;
        node.next = lastIndex;

        outSimContainerNodes[pWrite] = node;
    }
}