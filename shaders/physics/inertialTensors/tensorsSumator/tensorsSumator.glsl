#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "math.glsl"

struct DataActorDataForTensors
{
    vec3 centerOfMass;
    float mass;
    float totalVolume;
    uint indexActor;
};

STORAGE_BUFFER_R(0, 0, inputActors, DataActorDataForTensors, inActors)
STORAGE_BUFFER_R(0, 1, inputInertialTensor, mat4, inInertialTensor)
DYNAMIC_ACTORS_DATA_W(0, 2)

layout(push_constant, std430) uniform Constants
{
    uint maxCountActors;
    uint offsetIndexActor;
    uint indexCountTensors;
} pConstants;

layout (local_size_x = SIZE_GROUP_INERTIAL_TENSORS, local_size_y = 1, local_size_z = 1) in;
void main()
{  
    uint index = gl_GlobalInvocationID.x; 
    if(index >= pConstants.maxCountActors)
        return;

    DataActorDataForTensors actorData = inActors[index + pConstants.offsetIndexActor];
    mat3 sumTensors = mat3(0, 0, 0,
                            0, 0, 0,
                            0, 0, 0);
    for(uint i = 0; i < pConstants.indexCountTensors; i++)
    {
        mat3 tensor = mat3(inInertialTensor[index * pConstants.indexCountTensors + i]);   
        sumTensors += tensor;
    }

    float det = determinant(sumTensors);
    mat3 tensor = mat3(0, 0, 0,
                        0, 0, 0,
                        0, 0, 0);
    mat3 invTensor = tensor;

    float mass = actorData.mass;

    if(abs(det) <= 0.0000001)
    {
        tensor = mat3(mass, 0, 0,
                        0, mass, 0,
                        0, 0, mass);
        invTensor = mat3(1.0 / mass, 0, 0,
                        0, 1.0 / mass, 0,
                        0, 0, 1.0 / mass);
    }
    else
    {
        tensor = sumTensors;
        invTensor = inverse(tensor);
    }

    DataDynamicActor dataDynamic;
    dataDynamic.mass = mass;
    dataDynamic.centerMass = actorData.centerOfMass;
    dataDynamic.inertialTensor = mat3(tensor[0][0], tensor[0][1], tensor[0][2],
                                        tensor[1][0], tensor[1][1], tensor[1][2],
                                        tensor[2][0], tensor[2][1], tensor[2][2]);
    dataDynamic.inverseInertialTensor = mat3(invTensor[0][0], invTensor[0][1], invTensor[0][2],
                                            invTensor[1][0], invTensor[1][1], invTensor[1][2],
                                            invTensor[2][0], invTensor[2][1], invTensor[2][2]);

    dynamicActors[actorData.indexActor] = dataDynamic;
}