#extension GL_EXT_shader_atomic_float : require

#include "buffers.glsl"
#include "limits.glsl"
#include "flags.glsl"
#include "math.glsl"

#include "consistPoint.glsl"

GEOMETRIES_DATA(0, 0)

struct DataActorDataForTensors
{
    vec3 centerOfMass;
    float mass;
    float totalVolume;
    uint indexActor;
    uint free_0;
    uint free_1;
};

struct GeometriesDataForTensors
{
    uint startIndexToIndexesGeomtries;
    uint countIndexesGeometries;
    uint free_0;
    uint free_1;
};

struct IndexToGeometry
{
    uint index;
    float volumeFactor;
    uint free_0;
    uint free_1;
};

STORAGE_BUFFER_R(0, 1, inputActors, DataActorDataForTensors, inActors)
STORAGE_BUFFER_R(0, 2, inputGeometryLists, GeometriesDataForTensors, inGeometryLists)
STORAGE_BUFFER_R(0, 3, inputIndexesToGeometry, IndexToGeometry, inIndexesToGeometry)

shared mat4[16] globalRotateMatrixes;
shared float[16] inverseVolumes;

shared float[9] dataInertialTensor;

STORAGE_BUFFER_W(0, 4, outputInertialTensor, mat4, outInertialTensor)

layout(push_constant, std430) uniform Constants
{
    uint offsetIndex;
    uint indexTensor;
    uint indexCountTensors;
} pConstants;

layout (local_size_x = 4, 
        local_size_y = 4, 
        local_size_z = 2) in;
void main()
{ 
    {
        barrier();

        if(gl_LocalInvocationID == ivec3(0, 0, 0))
        {
            for(uint i = 0; i < 9; i++)
                dataInertialTensor[i] = 0;
        }

        barrier();
    }

    const uint totalSamples = 32 * 32 * 32; // !!!
    const uint countSamples = 8 * 8 * 16;    // count samples in thread
    vec3 threadPositions[countSamples];
    {
        uint countThreadPositions = 0;
        for(uint x = 0; x < 8; x++)
        {
            for(uint y = 0; y < 8; y++)
            {
                for(uint z = 0; z < 16; z++)
                {
                    ivec3 index = ivec3(gl_LocalInvocationID.x * 8 + x, 
                                        gl_LocalInvocationID.y * 8 + y,
                                        gl_LocalInvocationID.z * 16 + z);

                    vec3 pos = vec3(index) / 31.0; //total samples per axis
                    pos -= vec3(0.5, 0.5, 0.5);
                    pos *= 2.0;

                    threadPositions[countThreadPositions] = pos;
                    countThreadPositions++;
                }
            }
        }
    }

    uint indexActor = gl_WorkGroupID.x + pConstants.offsetIndex;
    
    DataActorDataForTensors actor = inActors[indexActor];
    GeometriesDataForTensors geometriesList = inGeometryLists[indexActor];

    for(uint i = 0; i < geometriesList.countIndexesGeometries; i += 16)
    {
        uint indexStart = i;
        uint countAviable = min(16, geometriesList.countIndexesGeometries - indexStart);

        uint indexStartGeometry = geometriesList.startIndexToIndexesGeomtries;

        barrier();

        if(gl_LocalInvocationID == ivec3(0, 0, 0))
        {
            for(uint i2 = 0; i2 < countAviable; i2++)
            {
                IndexToGeometry indexToGeometry = inIndexesToGeometry[indexStartGeometry + i2];
                uint indexGeometryData = indexToGeometry.index;

                vec4 quatRotate = geometries[indexGeometryData].rotate;
                globalRotateMatrixes[i2] = quaternionToMat4(quatRotate);
                float massFactor = actor.mass / totalSamples;
                inverseVolumes[i2] = (indexToGeometry.volumeFactor / actor.totalVolume) * massFactor;
            }
        }

        barrier();

        {
            for(uint i2 = 0; i2 < countAviable; i2++)
            {
                mat4 rotateMatrix = globalRotateMatrixes[i2];
                mat3 rotateMatrix3 = mat3(rotateMatrix);
                float inverseVolume = inverseVolumes[i2];

                IndexToGeometry indexToGeometry = inIndexesToGeometry[indexStartGeometry + i2];
                DataGeometry dataGeometry = geometries[indexToGeometry.index];
                
                vec3 centerGeometry = dataGeometry.offset;

                float tempInertial[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
                
                for(uint i3 = 0; i3 < countSamples; i3++)
                {
                    vec3 samplePos = threadPositions[i3];
                    samplePos = samplePos * dataGeometry.obbSize;
                    samplePos = (vec4(samplePos, 0) * rotateMatrix).xyz + centerGeometry;

                    bool consist = true;

                    #ifdef BOX
                    #endif
                    #ifdef SPHERE
                        consist = checkSphereConsistPoint(samplePos, centerGeometry, dataGeometry.data_1.x);
                    #endif
                    #ifdef CAPSULE
                        consist = checkCapsuleConsistPont(samplePos, centerGeometry, dataGeometry.data_1.y, dataGeometry.data_1.x, rotateMatrix3);
                    #endif

                    if(consist)
                    {
                        vec3 diff = samplePos - actor.centerOfMass;
                        tempInertial[0] += (diff.y * diff.y + diff.z * diff.z) * inverseVolume;    //I-xx
                        tempInertial[4] += (diff.x * diff.x + diff.z * diff.z) * inverseVolume;    //I-yy
                        tempInertial[8] += (diff.x * diff.x + diff.y * diff.y) * inverseVolume;    //I-zz

                        tempInertial[1] += (diff.x * diff.y) * inverseVolume;    //I-xy
                        tempInertial[2] += (diff.x * diff.z) * inverseVolume;    //I-xz
                        tempInertial[3] += (diff.y * diff.x) * inverseVolume;    //I-yx
                        tempInertial[5] += (diff.y * diff.z) * inverseVolume;    //I-yz
                        tempInertial[6] += (diff.z * diff.x) * inverseVolume;    //I-zx
                        tempInertial[7] += (diff.z * diff.y) * inverseVolume;    //I-zy
                    }
                }

                for(uint i3 = 0; i3 < 9; i3++)
                    atomicAdd(dataInertialTensor[i3], tempInertial[i3]);
            }
        }
    }

    barrier();

    if(gl_LocalInvocationID == ivec3(0, 0, 0))
    {
        mat4 inertialTensor = mat4(dataInertialTensor[0], dataInertialTensor[1], dataInertialTensor[2], 0,
                                    dataInertialTensor[3], dataInertialTensor[4], dataInertialTensor[5], 0,
                                    dataInertialTensor[6], dataInertialTensor[7], dataInertialTensor[8], 0,
                                    0, 0, 0, 1);

        /*inertialTensor = mat4(1, 1, 1, 0,
                                    1, 1, 1, 0,
                                    1, 1, 1, 0,
                                    0, 0, 0, 1);
          */                          
        uint pWrite = pConstants.indexCountTensors * gl_WorkGroupID.x + pConstants.indexTensor;
        outInertialTensor[pWrite] = inertialTensor;
    }
}