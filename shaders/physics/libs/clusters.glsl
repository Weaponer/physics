#ifndef CLOSEST_POINT_GLSL
#define CLOSEST_POINT_GLSL

#ifdef CHECK_SUM_FUNCTION
shared int sum;

bool checkSim(uint u, uint v, float e)
{
    barrier();
    sum = 0;
    barrier();

    uint indexStart = inVertexArray[u];
    uint count = inVertexArray[u + 1] - indexStart;
    
    if(count > gl_LocalInvocationID.x)
    {
        uint currentIndexN = indexStart + gl_LocalInvocationID.x;

        int low = int(inVertexArray[v]);
        int high = int(inVertexArray[v + 1]);
        while(low < high)
        {
            uint w = inAdjacentsArray[currentIndexN];
            int mid = (low + high) / 2;
            if(w < inAdjacentsArray[mid])
            {
                high = mid;
            }
            else if(w > inAdjacentsArray[mid])
            {
                low = mid + 1;
            }
            else
            {
                atomicAdd(sum, 1);
                break;
            }
        }
    }
    
    barrier();

    int du = int(inVertexArray[u + 1] - inVertexArray[u]);
    int dv = int(inVertexArray[v + 1] - inVertexArray[v]);

    return (sum + 2) >= e * sqrt((du + 1) * (dv + 1));
}
#endif

#ifdef ROOT_FUNCTION
int root(uint u)
{
    int pu = outParent[u];
    int next = outParent[pu];
    while (pu != next)
    {
        pu = next;
        next = outParent[pu];
    }
    return pu;
}
#endif

#ifdef UNION_FUNCTION
void unionF(uint u, uint v)
{
    if(outHeight[u] < outHeight[v])
    {
        uint temp = v;
        v = u;
        u = temp;
    }

    bool fg = true;
    while (fg)
    {
        fg = false;
        int res = atomicCompSwap(outParent[v], int(v), int(u));
        if(res != int(v))
        {
            v = res;
            fg = true;
        }
    }

    if(u != v && outHeight[u] == outHeight[v])
        atomicAdd(outHeight[u], 1);
}
#endif

#endif