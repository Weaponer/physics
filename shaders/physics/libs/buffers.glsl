#ifndef BUFFERS_GLSL
#define BUFFERS_GLSL

struct DataSceneSettings            // 64
{
    vec3 gravitation;               //0 12
    float deltaTime;                //12 16
    float stiffness;                //16 20
    float damping;                  //20 24
    float depthContactMin;          //24 28
    float depthContactMax;          //28 32
    float linearDamping;            //32 36
    float angularDamping;           //36 40
    float frictionStaticLimit;      //40 44
    float maxVelocitySleep;         //44 48
    float maxAngularVelocitySleep;  //48 52
    float minSleepTime;             //52 56
    uint percision;                 //56 60
    uint updateVelocities;          //60 64
};

struct DataActor               //16 - 16
{
    uint countShapes;         
    uint indexFirstShape;    
    uint indexAdditionalData; 
    uint flags;                
};

struct DataActorOrientation    //28 - 32
{
    vec4 rotation;
    vec3 position; 
};

struct DataDynamicActor        //120 - 128
{
    mat3 inertialTensor;        
    mat3 inverseInertialTensor; 
    vec3 centerMass;            
    float mass;
    float linearDrag;
    float angularDrag;     
};

struct DataVelocityActor       //64 - 64
{
    vec3 acceleration;        
    vec3 angularAcceleration;
    vec3 velocity;            
    vec3 angularVelocity;     
    uint flags;                
};

struct DataSleepingActor//8
{
    float sleepTime;    //0 4
    uint isSleeping;    //4 8
};

struct DataMaterial         //12 - 16
{
    float staticFriction;  
    float dynamicFriction;
    float bounciness;
};

struct DataGeometryOBB      //16 - 16
{
    uint indexActor;
    uint indexMainGeometry;
    uint indexShape;
    uint flags;
};

struct DataOrientationGeometryOBB   //48 - 48
{
    vec4 rotation;
    vec4 size;
    vec4 position;
};

struct DataShape                //16 - 16
{
    int indexMaterial;
    uint countGeometries;
    uint indexFirstGeometry;
    uint flags;     
};

struct DataGeometry         //80 - 80
{
    vec4 data_1;
    vec4 data_2;
 
    vec4 rotate;
    vec3 offset;
    float freeMemory;
    vec3 obbSize;

    uint flags;
};

struct DataListGeometryOBBs //8
{
    uint firstIndex; // 4
    uint count;      // 8
};

struct DataContactsContainer
{
    uint firstContactIndex;     // 4
    uint countAndMaskContacts;  // 8 0000 0000 ffff ffff - max count contacts, ffff ffff 0000 0000 - mask using contacts
    uint firstIndexGeometry;    // 12
    uint secondIndexGeometry;   // 16
};

struct DataContact
{
    vec3 firstLocalPosition;  // 12 - local position contact from fist geometry
    float depthPenetration;   // 16
    vec3 secondLocalPosition; // 28 - local position contact from second geometry
    float freeMemory;         // 32
    vec3 firstLocalNormal;    // 44
    float freeMemory_2;       // 48
    vec3 secondLocalNormal;   // 60
    float freeMemory_3;       // 64

    //debug

    vec4 firstPointGlobal;    // 80
    vec4 secondPointGlobal;   // 96
    vec4 firstOriginGlobal;   // 112
    vec4 secondOriginGlobal;  // 128
};


#define STORAGE_BUFFER(setIndex, bindingIndex, name, type, variableName)        \
layout(std430, row_major, set = setIndex, binding = bindingIndex) buffer name   \
{                                                                               \
    type variableName[];                                                        \
};

#define STORAGE_BUFFER_R(setIndex, bindingIndex, name, type, variableName)                  \
layout(std430, row_major, set = setIndex, binding = bindingIndex) readonly buffer name      \
{                                                                                           \
    type variableName[];                                                                    \
};

#define STORAGE_BUFFER_W(setIndex, bindingIndex, name, type, variableName)                  \
layout(std430, row_major, set = setIndex, binding = bindingIndex) writeonly buffer name     \
{                                                                                           \
    type variableName[];                                                                    \
};

// SceneSettingData

#define SCENE_SETTING_DATA(setIndex, bindingIndex)                                  \
layout(std140, set = setIndex, binding = bindingIndex) uniform dataSceneSettings    \
{                                                                                   \
    DataSceneSettings sceneSetting;                                                 \
};


//  ActorsData

#define ACTORS_DATA(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, actorsData, DataActor, actors)

//  ActorOrientationsData

#define ACTOR_ORIENTAIONS_DATA(setIndex, bindingIndex) STORAGE_BUFFER(setIndex, bindingIndex, actorOrientationsData, DataActorOrientation, actorOrientations)
#define ACTOR_ORIENTAIONS_DATA_R(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, actorOrientationsData, DataActorOrientation, actorOrientations)
#define ACTOR_ORIENTAIONS_DATA_W(setIndex, bindingIndex) STORAGE_BUFFER_W(setIndex, bindingIndex, actorOrientationsData, DataActorOrientation, actorOrientations)

//  DynamicActorsData

#define DYNAMIC_ACTORS_DATA(setIndex, bindingIndex) STORAGE_BUFFER(setIndex, bindingIndex, dynamicActorsData, DataDynamicActor, dynamicActors)
#define DYNAMIC_ACTORS_DATA_R(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, dynamicActorsData, DataDynamicActor, dynamicActors)
#define DYNAMIC_ACTORS_DATA_W(setIndex, bindingIndex) STORAGE_BUFFER_W(setIndex, bindingIndex, dynamicActorsData, DataDynamicActor, dynamicActors)

//  VelocityActorsData

#define VELOCITY_ACTORS_DATA(setIndex, bindingIndex) STORAGE_BUFFER(setIndex, bindingIndex, velocityActorsData, DataVelocityActor, velocityActors)
#define VELOCITY_ACTORS_DATA_R(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, velocityActorsData, DataVelocityActor, velocityActors)
#define VELOCITY_ACTORS_DATA_W(setIndex, bindingIndex) STORAGE_BUFFER_W(setIndex, bindingIndex, velocityActorsData, DataVelocityActor, velocityActors)

//  SleepingActorsData

#define SLEEPING_ACTORS_DATA(setIndex, bindingIndex) STORAGE_BUFFER(setIndex, bindingIndex, sleepingActorsData, DataSleepingActor, sleepingActors)
#define SLEEPING_ACTORS_DATA_R(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, sleepingActorsData, DataSleepingActor, sleepingActors)
#define SLEEPING_ACTORS_DATA_W(setIndex, bindingIndex) STORAGE_BUFFER_W(setIndex, bindingIndex, sleepingActorsData, DataSleepingActor, sleepingActors)


// MaterialsData

#define MATEIRLAS_DATA(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, materialsData, DataMaterial, materials)

// GeometryOBBsData

#define GEOMETRY_OBBS_DATA(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, geometryOBBsData, DataGeometryOBB, geometryObbs)

// OrientationGeometryOBBsData

#define ORIENTATION_GEOMETRY_OBBS_DATA(setIndex, bindingIndex) STORAGE_BUFFER(setIndex, bindingIndex, orientaionGeometryOBBsData, DataOrientationGeometryOBB, orientationObbs)
#define ORIENTATION_GEOMETRY_OBBS_DATA_R(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, orientaionGeometryOBBsData, DataOrientationGeometryOBB, orientationObbs)
#define ORIENTATION_GEOMETRY_OBBS_DATA_W(setIndex, bindingIndex) STORAGE_BUFFER_W(setIndex, bindingIndex, orientaionGeometryOBBsData, DataOrientationGeometryOBB, orientationObbs)

// ShapeData

#define SHAPES_DATA(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, shapesData, DataShape, shapes)

// GeometryData

#define GEOMETRIES_DATA(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, geometriesData, DataGeometry, geometries)

// ContactsContainers
#define CONTACTS_CONTAINERS_DATA(setIndex, bindingIndex) STORAGE_BUFFER(setIndex, bindingIndex, contactsContainersData, DataContactsContainer, contactsContainers)
#define CONTACTS_CONTAINERS_DATA_R(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, contactsContainersData, DataContactsContainer, contactsContainers)
#define CONTACTS_CONTAINERS_DATA_W(setIndex, bindingIndex) STORAGE_BUFFER_W(setIndex, bindingIndex, contactsContainersData, DataContactsContainer, contactsContainers)

// Contacts
#define CONTACTS_DATA(setIndex, bindingIndex) STORAGE_BUFFER(setIndex, bindingIndex, contactsData, DataContact, contacts)
#define CONTACTS_DATA_R(setIndex, bindingIndex) STORAGE_BUFFER_R(setIndex, bindingIndex, contactsData, DataContact, contacts)
#define CONTACTS_DATA_W(setIndex, bindingIndex) STORAGE_BUFFER_W(setIndex, bindingIndex, contactsData, DataContact, contacts)


#endif