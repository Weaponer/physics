#ifndef SIMULATION_STRUCTS_GLSL
#define SIMULATION_STRUCTS_GLSL

#include "buffers.glsl"


struct DataSimContactsContainer     //32
{
    float dynamicFriction;          //0 4
    float staticFriction;           //4 8
    float bounciness;               //8 12
    int actorA;                     //12 16
    int actorB;                     //16 20
    uint countContacts;             //20 24
    uint indexFirstContact;         //24 28
    int nextContainer;              //28 32
};

struct DataSimContact   //48
{
    vec3 positionA;     //0 16
    vec3 positionB;     //16 32
    vec3 normalAtoB;    //32 44
    float depth;        //44 48
};

struct CommonDataSimContact//48
{
    vec3 positionA;             //0 16  
    vec3 positionB;             //16 32
    vec3 normalAtoB;            //32 44
    float depth;                //44 48
};

struct CommonDataContactsContainer   //48
{
    float averageVelocity;           //0 4
    float dynamicFriction;          //4 8
    float staticFriction;           //8 12
    float bounciness;               //12 16
    int actorA;                     //16 20
    int actorB;                     //20 24
    uint orientationActorA;         //24 28
    uint orientationActorB;         //28 32
    uint countContacts;             //32 36
    uint indexFirstContact;         //36 40
    uint flags;                     //40 44
    uint freeMemory_1;              //44 48
};



struct DataSimContactsContainerFinished //128
{
    vec4 invMassTensorA[3];             //0 48
    vec4 invMassTensorB[3];             //48 96
                                        //invMassTensor[0] = { invTensor[0][0], invTensor[0][1], invTensor[0][2], invMass }
                                        //invMassTensor[1] = { invTensor[1][0], invTensor[1][1], invTensor[1][2], 0 }
                                        //invMassTensor[2] = { invTensor[2][0], invTensor[2][1], invTensor[2][2], 0 }

    uint countContacts;                 //96 100 
    uint indexFirstContact;             //100 104
    int indexGraph;                     //104 108
    int startListContainersA;           //108 112
    uint startSwapActorA;               //112 116  
    int startListContainersB;           //116 120
    uint startSwapActorB;               //120 124
    uint flagsEdge;                     //124 128
};


struct DataSimContactsContainerEPBD //192
{
    vec4 dataA[5];                  //0 80
    vec4 dataB[5];                  //80 160
                                    //data[0] = { invTensor[0][0], invTensor[0][1], invTensor[0][2], invMass }
                                    //data[1] = { invTensor[1][0], invTensor[1][1], invTensor[1][2], angularVel.x }
                                    //data[2] = { invTensor[2][0], invTensor[2][1], invTensor[2][2], angularVel.y }
                                    //data[3] = { centerOfMass.x, centerOfMass.y, centerOfMass.z, angularVel.z }
                                    //dataA[4] = { linearVelA.x, linearVelA.y, linearVelA.z, frictionStatic }
                                    //dataB[4] = { linearVelB.x, linearVelB.y, linearVelB.z, frictionDynamic }


    uint countContacts;             //160 164 
    uint indexFirstContact;         //164 168
    int indexGraph;                 //168 172
    int startListContainersA;       //172 176
    uint startSwapActorA;           //176 180  
    int startListContainersB;       //180 184
    uint startSwapActorB;           //184 188
    float bounciness;               //188 192
};


struct DataSimContactVel //16
{
    vec4 velocity;      //0 16   xyz - vel, w - friction
};

struct DataSimContactModel   //128
{
    vec4 basis[3];          //0 48
                            //basis[0] = { basis[0][0], basis[0][1], basis[0][2], dirContactA.x }
                            //basis[1] = { basis[1][0], basis[1][1], basis[1][2], dirContactA.y }
                            //basis[2] = { basis[2][0], basis[2][1], basis[2][2], dirContactA.z }
    vec4 A[3];              //48 96

                            //A[3] = { A[0][0], A[0][1], A[0][2], dirContactB.x }
                            //A[4] = { A[1][0], A[1][1], A[1][2], dirContactB.y }
                            //A[5] = { A[2][0], A[2][1], A[2][2], dirContactB.z }
    vec4 velocity;          //96 112   xyz - velocity, w - friction
    vec4 position;          //112 128   xyz - position, w - depth
    vec4 testData;
};

struct DataSimContainerNode //8
{
    uint indexContainer;    //0 4 - 31 - bite is 1 for "b" actor
    int next;               //4 8
};

struct DataSimActor                 //16
{
    uint indexMainActor;            //0 4
    uint countContainers;           //4 8
    uint indexFirstContainer;       //8 12
    int indexStartLoopContainers;   //12 16
    uint isStartLikeActorB;         //16 20
    uint flags;                     //20 24
    uint freeMemory_1;              //24 28
    uint freeMemory_2;              //28 32
};

struct DataActorGraphNode           //16
{
    int countEdges;                 //0 4
    int indexFirstEdge;             //4 8
    int totalCountActors;           //8 12
    int flags;                      //12 16
};

struct DataActorGraphEdge_2       //16
{
    int nodeA;                 //0 4
    int nodeB;                 //4 8
    float totalForceContacts;   //8 12
    int flags;                  //12 16
};

struct DataActorGraphEdge       //16
{
    uint nodeA;                 //0 4
    uint nodeB;                 //4 8
    uint totalCountContacts;    //8 12
    float totalForceContacts;   //12 16
};

struct DataCluster          //8 -> 16
{
    uint countNodes;        //0 4
    uint firstIndexNode;    //4 8
    uint freeMemory;        //8 12
    uint freeMemory_1;      //12 16
};


struct ImpulseNode
{
    vec3 linearImpulse;
    vec3 angularImpulse;
    int next;
};

struct AccamulatedContainerVelocity //64
{
    vec4 velocities[3];             //0 48
                                    //velocities[0] = { linearVelocityA.x, linearVelocityA.y, linearVelocityA.z, angularVelocityB.x }
                                    //velocities[1] = { angularVelocityA.x, angularVelocityA.y, angularVelocityA.z, angularVelocityB.y }
                                    //velocities[2] = { linearVelocityB.x, linearVelocityB.y, linearVelocityB.z, angularVelocityB.z }
    ivec4 subData;                  //48 64
                                    //subData = { nextContainerA, nextContainerB, indexGraph, is need to swap actors }
};

#endif