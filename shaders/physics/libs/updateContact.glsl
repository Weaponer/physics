#ifndef UPDATE_CONTACT_GLSL
#define UPDATE_CONTACT_GLSL

#include "buffers.glsl"
#include "math.glsl"
#include "closestPoint.glsl"
#include "consistPoint.glsl"

struct UpdatedContact
{
    vec3 positionA;
    vec3 positionB;
};

vec3 projToPlane(vec3 p, vec3 o, vec3 n)
{
    return p - dot(p - o, n) * n;
}

bool updateContactPlaneBox(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                            DataGeometry secondData, DataOrientationGeometryOBB secondOrientation,
                            DataContact updateContact, out UpdatedContact updatedConact)
{
    vec3 planeRotate = (transpose(quaternionToMat3(firstOrientation.rotation))[1]).xyz;
    vec3 posPlane = firstOrientation.position.xyz;

    mat3 rotInv_2 = quaternionToMat3(secondOrientation.rotation);
    mat3 rot_2 = transpose(rotInv_2);
    vec3 pos_2 = secondOrientation.position.xyz;
    vec3 size = secondOrientation.size.xyz;

    vec3 globalContactPosB = rot_2 * updateContact.secondLocalPosition + pos_2;

    float pointIsUnderPlane = dot(globalContactPosB - posPlane, planeRotate);
    vec3 projectToPlaneA = globalContactPosB - pointIsUnderPlane * planeRotate;
    
    if(pointIsUnderPlane <= 0.0)
    {
        updatedConact.positionA = projectToPlaneA;
        updatedConact.positionB = globalContactPosB;
        return true;
    }
    return false;
}

bool updateContactBoxBox(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                            DataGeometry secondData, DataOrientationGeometryOBB secondOrientation,
                            DataContact updateContact, out UpdatedContact updatedConact)
{
    mat3 rotInv_1 = quaternionToMat3(firstOrientation.rotation);
    mat3 rot_1 = transpose(rotInv_1);
    vec3 pos_1 = firstOrientation.position.xyz;
    vec3 size_1 = firstOrientation.size.xyz;

    mat3 rotInv_2 = quaternionToMat3(secondOrientation.rotation);
    mat3 rot_2 = transpose(rotInv_2);
    vec3 pos_2 = secondOrientation.position.xyz;
    vec3 size_2 = secondOrientation.size.xyz;

    vec3 globalContactPosA = rot_1 * updateContact.firstLocalPosition + pos_1;
    vec3 globalContactPosB = rot_2 * updateContact.secondLocalPosition + pos_2;

    vec3 globalContactNormalA = rot_1 * updateContact.firstLocalNormal;
    vec3 globalContactNormalB = rot_2 * updateContact.secondLocalNormal;

    vec3 projectToPlaneA = projToPlane(globalContactPosB, globalContactPosA, globalContactNormalA);
    vec3 projectToPlaneB = projToPlane(globalContactPosA, globalContactPosB, globalContactNormalB);
    
    if(checkBoxConsistPont(projectToPlaneA, pos_1, size_1, rotInv_1) && checkBoxConsistPont(globalContactPosB, pos_1, size_1, rotInv_1))
    {
        updatedConact.positionA = projectToPlaneA;
        updatedConact.positionB = globalContactPosB;
        return true;
    }

    if(checkBoxConsistPont(projectToPlaneB, pos_2, size_2, rotInv_2) && checkBoxConsistPont(globalContactPosA, pos_2, size_2, rotInv_2))
    {
        updatedConact.positionA = globalContactPosA;
        updatedConact.positionB = projectToPlaneB;
        return true;
    }
    return false;
}


bool updateContactBoxCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation,
                                DataContact updateContact, out UpdatedContact updatedConact)
{
    mat3 rotInv_1 = quaternionToMat3(firstOrientation.rotation);
    mat3 rot_1 = transpose(rotInv_1);
    vec3 pos_1 = firstOrientation.position.xyz;
    vec3 size_1 = firstOrientation.size.xyz;

    mat3 rotInv_2 = quaternionToMat3(secondOrientation.rotation);
    mat3 rot_2 = transpose(rotInv_2);
    vec3 pos_2 = secondOrientation.position.xyz;
    float capsuleRadius = secondData.data_1.x;
    float capsuleHeight = secondData.data_1.y;
    
    vec3 upCapsule = rot_2[1].xyz;
    vec3 pointA = pos_2 + upCapsule * capsuleHeight;
    vec3 pointB = pos_2 - upCapsule * capsuleHeight;

    vec3 globalContactPosA = rot_1 * updateContact.firstLocalPosition + pos_1;
    vec3 globalContactPosB = rot_2 * updateContact.secondLocalPosition + pos_2;

    vec3 globalContactNormalA = rot_1 * updateContact.firstLocalNormal;
    vec3 globalContactNormalB = rot_2 * updateContact.secondLocalNormal;

    vec3 projectToPlaneA = projToPlane(globalContactPosB, globalContactPosA, globalContactNormalA);
    vec3 projectToPlaneB = projToPlane(globalContactPosA, globalContactPosB, globalContactNormalB);
    
    return false;

    //check is posible more then one contact
    {
        vec3 projA = projToPlane(pointA, globalContactPosA, globalContactNormalA);
        vec3 projB = projToPlane(pointB, globalContactPosA, globalContactNormalA);
        float sqrRadius = capsuleRadius * capsuleRadius;
        vec3 diffA = projA - pointA;
        vec3 diffB = projB - pointB;
        float sqrtDistA = diffA.x * diffA.x + diffA.y * diffA.y + diffA.z * diffA.z;
        float sqrtDistB = diffB.x * diffB.x + diffB.y * diffB.y + diffB.z * diffB.z;
        if(sqrtDistA > sqrRadius || sqrtDistB > sqrRadius)
            return false;
    }

    {
        vec3 projMainContactOnSegment = closestPointSegment(globalContactPosB, pointA, pointB);

        vec3 normalGetContactB = normalize(cross(upCapsule, normalize(cross(upCapsule, -globalContactNormalA))));
        vec3 newPointContactB = projMainContactOnSegment + normalGetContactB * capsuleRadius;

        vec3 newPointContactA = projToPlane(newPointContactB, globalContactPosA, globalContactNormalA);

        if(checkBoxConsistPont(newPointContactA, pos_1, size_1, rotInv_1) && 
            checkBoxConsistPont(newPointContactB, pos_1, size_1, rotInv_1))
        {
            updatedConact.positionA = newPointContactA;
            updatedConact.positionB = newPointContactB;
            return true;
        }
    }

    if(checkBoxConsistPont(projectToPlaneA, pos_1, size_1, rotInv_1) && checkBoxConsistPont(globalContactPosB, pos_1, size_1, rotInv_1))
    {
        updatedConact.positionA = projectToPlaneA;
        updatedConact.positionB = globalContactPosB;
        return true;
    }

    return false;
}

bool updateContactCapsuleCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                    DataGeometry secondData, DataOrientationGeometryOBB secondOrientation,
                                    DataContact updateContact, out UpdatedContact updatedConact)
{
    vec3 firstPos = firstOrientation.position.xyz;
    vec3 secondPos = secondOrientation.position.xyz;

    float firstRadius = firstData.data_1.x;
    float secondRadius = secondData.data_1.x;
    float firstHeight = firstData.data_1.y;
    float secondHeight = secondData.data_1.y;

    mat3 firstRotate = transpose(quaternionToMat3(firstOrientation.rotation));
    mat3 secondRotate = transpose(quaternionToMat3(secondOrientation.rotation));

    vec3 firstCapsulaUp = firstRotate[1].xyz;
    vec3 secondCapsulaUp = secondRotate[1].xyz;

    vec3 firstA = firstPos + firstCapsulaUp * firstHeight;
    vec3 firstB = firstPos - firstCapsulaUp * firstHeight;

    vec3 secondA = secondPos + secondCapsulaUp * secondHeight;
    vec3 secondB = secondPos - secondCapsulaUp * secondHeight;

    vec3 globalContactPosA = firstRotate * updateContact.firstLocalPosition + firstPos;
    vec3 globalContactPosB = secondRotate * updateContact.secondLocalPosition + secondPos;
    
    return false;
    {
        vec3 projAtoSegmentA = closestPointSegment(globalContactPosA, firstA, firstB);
        vec3 projToSegmentB = closestPointSegment(projAtoSegmentA, secondA, secondB);
        
        vec3 normalToSegmentB = normalize(projToSegmentB - projAtoSegmentA);
        vec3 normalToGenContactA = normalize(cross(firstCapsulaUp, normalize(cross(normalToSegmentB, firstCapsulaUp))));
    
        vec3 newContactA = projAtoSegmentA + normalToGenContactA * firstRadius;

        vec3 projNewContactAtoSegmentB = closestPointSegment(newContactA, secondA, secondB);
        vec3 normalToGenContactB = normalize(projNewContactAtoSegmentB - newContactA);

        if(dot(normalToGenContactA, normalToGenContactB) > 0)
            normalToGenContactB = -normalToGenContactB;

        vec3 newContactB = projNewContactAtoSegmentB + normalToGenContactB * secondRadius;

        if(checkCapsuleConsistPont(newContactA, secondPos, secondHeight, secondRadius, transpose(secondRotate)))
        {
            updatedConact.positionA = newContactA;
            updatedConact.positionB = newContactB;
            return true;
        }
    }

    {
        vec3 projBtoSegmentB = closestPointSegment(globalContactPosB, secondA, secondB);
        vec3 projToSegmentA = closestPointSegment(projBtoSegmentB, firstA, firstB);
        
        vec3 normalToSegmentA = normalize(projToSegmentA - projBtoSegmentB);
        vec3 normalToGenContactB = normalize(cross(secondCapsulaUp, normalize(cross(normalToSegmentA, secondCapsulaUp))));
    
        vec3 newContactB = projBtoSegmentB + normalToGenContactB * secondRadius;

        vec3 projNewContactBtoSegmentA = closestPointSegment(newContactB, firstA, firstB);
        vec3 normalToGenContactA = normalize(projNewContactBtoSegmentA - newContactB);

        if(dot(normalToGenContactA, normalToGenContactB) > 0)
            normalToGenContactA = -normalToGenContactA;

        vec3 newContactA = projNewContactBtoSegmentA + normalToGenContactA * firstRadius;

        if(checkCapsuleConsistPont(newContactB, firstPos, firstHeight, firstRadius, transpose(firstRotate)))
        {
            updatedConact.positionA = newContactA;
            updatedConact.positionB = newContactB;
            return true;
        }
    }

    return false;
}   

#endif