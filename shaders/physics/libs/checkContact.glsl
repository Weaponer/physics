#ifndef CHECK_CONTACT_GLSL
#define CHECK_CONTACT_GLSL

#include "buffers.glsl"
#include "math.glsl"
#include "closestPoint.glsl"

bool checkContactPlaneSphere(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 planePos = firstOrientation.position.xyz;
    vec3 planeRotate = (transpose(quaternionToMat3(firstOrientation.rotation))[1]).xyz;

    vec3 spherePos = secondOrientation.position.xyz;
    float sphereRadius = secondData.data_1.x;
    
    float distToPlane = dot(planeRotate, planePos);
    float distToSphere = dot(planeRotate, spherePos);

    return (distToSphere - distToPlane) <= sphereRadius;
} 

bool checkContactPlaneCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                            DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 planePos = firstOrientation.position.xyz;
    vec3 planeRotate = (transpose(quaternionToMat3(firstOrientation.rotation))[1]).xyz;

    vec3 capsulePos = secondOrientation.position.xyz;

    float capsuleRadius = secondData.data_1.x;
    float capsuleHeight = secondData.data_1.y;

    mat3 capsuleRotate = transpose(quaternionToMat3(secondOrientation.rotation));
    vec3 upCapsule = capsuleRotate[1].xyz;

    vec3 pointA = capsulePos + upCapsule * capsuleHeight;
    vec3 pointB = capsulePos - upCapsule * capsuleHeight;

    float distToPlane = dot(planeRotate, planePos);
    float distToPointA = dot(planeRotate, pointA);
    float distToPointB = dot(planeRotate, pointB);
    float diff = min(distToPointA - distToPlane, distToPointB - distToPlane);

    return diff <= capsuleRadius;
}       

bool checkContactBoxSphere(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                            DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 boxPos = firstOrientation.position.xyz;
    vec3 spherePos = secondOrientation.position.xyz;
    
    vec3 boxSize = firstOrientation.size.xyz;
    mat3 boxRotate = transpose(quaternionToMat3(firstOrientation.rotation));

    float sphereRadius = secondData.data_1.x;

    vec3 d = spherePos - boxPos;
    vec3 closestP = boxPos;
    for(int i = 0; i < 3; i++)
    {
        vec3 axis = boxRotate[i];
        float dist = dot(d, axis);
        if(dist > boxSize[i]) dist = boxSize[i];
        if(dist < -boxSize[i]) dist = -boxSize[i];

        closestP += axis * dist;
    }

    vec3 diff = closestP - spherePos;

    float sqrDist = diff.x * diff.x + diff.y * diff.y + diff.z * diff.z;
    return sqrDist <= (sphereRadius * sphereRadius);
}

bool checkContactSphereSphere(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 firstPos = firstOrientation.position.xyz;
    vec3 secondPos = secondOrientation.position.xyz;
    float firstRadius = firstData.data_1.x;
    float secondRadius = secondData.data_1.x;

    float sumRadius = firstRadius + secondRadius;
    
    vec3 diff = secondPos - firstPos;
    float sqrDist = diff.x * diff.x + diff.y * diff.y + diff.z * diff.z;
    return sqrDist <= (sumRadius * sumRadius);
}

bool checkContactSphereCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 spherePos = firstOrientation.position.xyz;
    vec3 capsulePos = secondOrientation.position.xyz;

    float sphereRadius = firstData.data_1.x;
    float capsuleRadius = secondData.data_1.x;
    float capsuleHeight = secondData.data_1.y;

    mat3 capsuleRotate = transpose(quaternionToMat3(secondOrientation.rotation));
    vec3 upCapsule = capsuleRotate[1].xyz;

    vec3 pointA = capsulePos + upCapsule * capsuleHeight;
    vec3 pointB = capsulePos - upCapsule * capsuleHeight;

    float minDist = 0.0;
    {
        vec3 ab = pointB - pointA;
        vec3 ac = spherePos - pointA;
        vec3 bc = spherePos - pointB;

        float e = dot(ac, ab);
        float f = dot(ab, ab);
        if(e <= 0.0)
            minDist = dot(ac, ac);
        else if(e >= f)
            minDist = dot(bc, bc);
        else
            minDist = dot(ac, ac) - e * e / f;
    }

    float sumRadius = capsuleRadius + sphereRadius;

    return minDist <= sumRadius * sumRadius;
}

bool checkContactCapsuleCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 firstPos = firstOrientation.position.xyz;
    vec3 secondPos = secondOrientation.position.xyz;

    float firstRadius = firstData.data_1.x;
    float secondRadius = secondData.data_1.x;
    float firstHeight = firstData.data_1.y;
    float secondHeight = secondData.data_1.y;

    mat3 firstRotate = transpose(quaternionToMat3(firstOrientation.rotation));
    mat3 secondRotate = transpose(quaternionToMat3(secondOrientation.rotation));

    vec3 firstA = firstPos + firstRotate[1].xyz * firstHeight;
    vec3 firstB = firstPos - firstRotate[1].xyz * firstHeight;

    vec3 secondA = secondPos + secondRotate[1].xyz * secondHeight;
    vec3 secondB = secondPos - secondRotate[1].xyz * secondHeight;

    vec3 c1 = vec3(0, 0, 0);
    vec3 c2 = vec3(0, 0, 0);
    float sqrDist = sqrDistSegmentSegment(firstA, firstB, secondA, secondB, c1, c2);

    float sumRadius = firstRadius + secondRadius;
    return sqrDist <= sumRadius * sumRadius;
}   

vec3 pointBoxQuery(vec3 o, vec3 b) 
{
    vec3 closest = o;
    for (int i = 0; i < 3; ++i) 
    {
        if (o[i] < -b[i])
            closest[i] = -b[i];
        else if (o[i] > b[i])
            closest[i] = b[i];
    }
    return closest;
}

vec4 lineFaceQuery(ivec3 i, vec3 o, vec3 d, vec3 b)
{
    vec3 PmE = o - b;
    vec3 PpE = o + b;
    
    vec3 bi = vec3(b[i[0]], b[i[1]], b[i[2]]);
    vec3 oi = vec3(o[i[0]], o[i[1]], o[i[2]]);
    vec3 di = vec3(d[i[0]], d[i[1]], d[i[2]]);
    vec3 PmEi = vec3(PmE[i[0]], PmE[i[1]], PmE[i[2]]);
    vec3 PpEi = vec3(PpE[i[0]], PpE[i[1]], PpE[i[2]]);
    
    vec4 c;
    if (di[0] * PpEi[1] >= di[1] * PmEi[0])
    {
        if (di[0] * PpEi[2] >= di[2] * PmEi[0])
        {
            c = vec4(bi[0], oi[1] - di[1] * PmEi[0] / di[0], oi[2] - di[2] * PmEi[0] / di[0], -PmEi[0] / di[0]);
        }
        else
        {
            float lenSqr = di[0] * di[0] + di[2] * di[2];
            float tmp = lenSqr * PpEi[1] - di[1] * (di[0] * PmEi[0] + di[2] * PpEi[2]);
            if (tmp <= 2.0 * lenSqr * bi[1])
            {
                float t = tmp / lenSqr;
                lenSqr += di[1] * di[1];
                tmp = PpEi[1] - t;
                float delta = di[0] * PmEi[0] + di[1] * tmp + di[2] * PpEi[2];
                c = vec4(bi[0], t - bi[1], -bi[2], -delta / lenSqr);
            }
            else
            {
                lenSqr += di[1] * di[1];
                float delta = di[0] * PmEi[0] + di[1] * PmEi[1] + di[2] * PpEi[2];
                c = vec4(bi[0], bi[1], -bi[2], -delta / lenSqr);
            }
        }
    }
    else
    {
        if (di[0] * PpEi[2] >= di[2] * PmEi[0])
        {
            float lenSqr = di[0] * di[0] + di[1] * di[1];
            float tmp = lenSqr * PpEi[2] - di[2] * (di[0] * PmEi[0] + di[1] * PpEi[1]);
            if (tmp <= 2.0 * lenSqr * bi[2])
            {
                float t = tmp / lenSqr;
                lenSqr += di[2] * di[2];
                tmp = PpEi[2] - t;
                float delta = di[0] * PmEi[0] + di[1] * PpEi[1] + di[2] * tmp;
                c = vec4(bi[0], -bi[1], t - bi[2], -delta / lenSqr);
            }
            else
            {
                lenSqr += di[2] * di[2];
                float delta = di[0] * PmEi[0] + di[1] * PpEi[1] + di[2] * PmEi[2];
                c = vec4(bi[0], -bi[1], bi[2], -delta / lenSqr);
            }
        }
        else
        {
            float lenSqr = di[0] * di[0] + di[2] * di[2];
            float tmp = lenSqr * PpEi[1] - di[1] * (di[0] * PmEi[0] + di[2] * PpEi[2]);
            if (tmp >= 0.0)
            {
                if (tmp <= 2.0 * lenSqr * bi[1])
                {
                    float t = tmp / lenSqr;
                    lenSqr += di[1] * di[1];
                    tmp = PpEi[1] - t;
                    float delta = di[0] * PmEi[0] + di[1] * tmp + di[2] * PpEi[2];
                    c = vec4(bi[0], t - bi[1], -bi[2], -delta / lenSqr);
                }
                else
                {
                    lenSqr += di[1] * di[1];
                    float delta = di[0] * PmEi[0] + di[1] * PmEi[1] + di[2] * PpEi[2];
                    c = vec4(bi[0], bi[1], -bi[2], -delta / lenSqr);
                }
            }
            else 
            {
                lenSqr = di[0] * di[0] +
                di[1] * di[1];
                tmp = lenSqr * PpEi[2] - di[2] * (di[0] * PmEi[0] + di[1] * PpEi[1]);
                if (tmp >= 0.0)
                {
                    if (tmp <= 2.0 * lenSqr * bi[2])
                    {
                        float t = tmp / lenSqr;
                        lenSqr += di[2] * di[2];
                        tmp = PpEi[2] - t;
                        float delta = di[0] * PmEi[0] + di[1] * PpEi[1] + di[2] * tmp;
                        c = vec4(bi[0], -bi[1], t - bi[2], -delta / lenSqr);
                    }
                    else
                    {
                        lenSqr += di[2] * di[2];
                        float delta = di[0] * PmEi[0] + di[1] * PpEi[1] + di[2] * PmEi[2];
                        c = vec4(bi[0], -bi[1], bi[2], -delta / lenSqr);
                    }
                }
                else 
                {
                    lenSqr += di[2] * di[2];
                    float delta = di[0] * PmEi[0] + di[1] * PpEi[1] + di[2] * PpEi[2];
                    c = vec4(bi[0], -bi[1], -bi[2], -delta / lenSqr);
                }
            }            
        }
    }
    
    ivec3 map;
    map[i[0]] = 0;
    map[i[1]] = 1;
    map[i[2]] = 2;
    return vec4(c[map[0]], c[map[1]], c[map[2]], c.w);
}
            
vec4 lineBoxQuery(vec3 o, vec3 d, vec3 b)
{
    vec4 closest;

    bvec3 reflected = lessThan(d, vec3(0.0));
    o = mix(o, -o, reflected);
    d = mix(d, -d, reflected);
    
    vec3 PmE = o - b;
    
    ivec3 i;
    
    if (d[1] * PmE[0] >= d[0] * PmE[1])
        i = (d[2] * PmE[0] >= d[0] * PmE[2]) ? ivec3(0, 1, 2) : ivec3(2, 0, 1);
    else
        i = (d[2] * PmE[1] >= d[1] * PmE[2]) ? ivec3(1, 2, 0) : ivec3(2, 0, 1);
    
    closest = lineFaceQuery(i, o, d, b);
    
    closest.xyz = mix(closest.xyz, -closest.xyz, reflected);
    return closest;
}


vec4 segmentBoxQuery( vec3 s, vec3 e, vec3 b ) 
{
    vec3 o = s;
    vec3 d = e - s;
    vec4 lbOutput = lineBoxQuery(o, d, b);
    if (lbOutput.w >= 0.0 && lbOutput.w <= 1.0) 
        return lbOutput;
    
    float parameter = (lbOutput.w < 0.0) ? 0.0 : 1.0;
    vec3 pbOutput = pointBoxQuery(o + d * parameter, b);
    return vec4(pbOutput, parameter);
}

bool checkContactBoxCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 boxPos = firstOrientation.position.xyz;
    
    vec3 boxSize = firstOrientation.size.xyz;
    mat3 boxRotate = quaternionToMat3(firstOrientation.rotation);

    vec3 capsulaPos = secondOrientation.position.xyz;

    float capsulaRadius = secondData.data_1.x;
    float capsulaHeight = secondData.data_1.y;

    mat3 capsulaRotate = transpose(quaternionToMat3(secondOrientation.rotation));

    vec3 a = capsulaPos + capsulaRotate[1].xyz * capsulaHeight;
    vec3 b = capsulaPos - capsulaRotate[1].xyz * capsulaHeight;

    vec3 localA = (a - boxPos) * boxRotate;
    vec3 localB = (b - boxPos) * boxRotate;

    vec4 closestP = segmentBoxQuery(localA, localB, boxSize);

    vec3 diff = closestP.xyz - (localA + (localB - localA) * closestP.w);
    float sqrDist = diff.x * diff.x + diff.y * diff.y + diff.z * diff.z;
    
    return sqrDist <= capsulaRadius * capsulaRadius;
}   

#endif