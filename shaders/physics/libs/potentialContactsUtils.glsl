#ifndef POTENTIAL_CONTACTS_UTILS_GLSL
#define POTENTIAL_CONTACTS_UTILS_GLSL

#define EPSILON 0.00000001

bool checkDataOBBs(DataGeometryOBB one, DataGeometryOBB two)
{
    return one.indexActor != two.indexActor && ((one.flags & FLAG_GEOMETRY_DYNAMIC) != 0 || (two.flags & FLAG_GEOMETRY_DYNAMIC) != 0);
}

bool checkOrientationOBBs(DataOrientationGeometryOBB one, DataOrientationGeometryOBB two)
{
    mat3 oneM = transpose(quaternionToMat3(one.rotation));
    mat3 twoM = transpose(quaternionToMat3(two.rotation));

    float ra;
    float rb;

    mat3 R;
    mat3 absR;
    
    one.size *= 1.01;
    two.size *= 1.01;
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            R[i][j] = dot(oneM[i], twoM[j]);

    vec3 t = two.position.xyz - one.position.xyz;
    t = vec3(dot(t, oneM[0]), dot(t, oneM[1]), dot(t, oneM[2]));

    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            absR[i][j] = abs(R[i][j]) + EPSILON;

    for (int i = 0; i < 3; i++)
    {
        ra = one.size[i];
        rb = two.size.x * absR[i].x + two.size.y * absR[i].y + two.size.z * absR[i].z;
        if (abs(t[i]) > ra + rb)
            return false;
    }

    for (int i = 0; i < 3; i++)
    {
        ra = one.size.x * absR[0][i] + one.size.y * absR[1][i] + one.size.z * absR[2][i];
        rb = two.size[i];
        if (abs(t.x * R[0][i] + t.y * R[1][i] + t.z * R[2][i]) > ra + rb)
            return false;
    }

    //Test axis L = AO x BO
    ra = one.size[1] * absR[2][0] + one.size[2] * absR[1][0];
    rb = two.size[1] * absR[0][2] + two.size[2] * absR[0][1];
    if (abs(t[2] * R[1][0] - t[1] * R[2][0]) > ra + rb)
        return false;

    //Test axis L = AO x B1
    ra = one.size[1] * absR[2][1] + one.size[2] * absR[1][1];
    rb = two.size[0] * absR[0][2] + two.size[2] * absR[0][0];
    if (abs(t[2] * R[1][1] - t[1] * R[2][1]) > ra + rb)
        return false;

    //Test axis L = AO x B2
    ra = one.size[1] * absR[2][2] + one.size[2] * absR[1][2];
    rb = two.size[0] * absR[0][1] + two.size[1] * absR[0][0];
    if (abs(t[2] * R[1][2] - t[1] * R[2][2]) > ra + rb)
        return false;

    //Test axis L = A1 x B0
    ra = one.size[0] * absR[2][0] + one.size[2] * absR[0][0];
    rb = two.size[1] * absR[1][2] + two.size[2] * absR[1][1];
    if (abs(t[0] * R[2][0] - t[2] * R[0][0]) > ra + rb)
        return false;

    //Test axis L = A1 x B1
    ra = one.size[0] * absR[2][1] + one.size[2] * absR[0][1];
    rb = two.size[0] * absR[1][2] + two.size[2] * absR[1][0];
    if (abs(t[0] * R[2][1] - t[2] * R[0][1]) > ra + rb)
        return false;

     //Test axis L = A1 x B2
    ra = one.size[0] * absR[2][2] + one.size[2] * absR[0][2];
    rb = two.size[0] * absR[1][1] + two.size[1] * absR[1][0];
    if (abs(t[0] * R[2][2] - t[2] * R[0][2]) > ra + rb)
        return false;

     //Test axis L = A2 x B0
    ra = one.size[0] * absR[1][0] + one.size[1] * absR[0][0];
    rb = two.size[1] * absR[2][2] + two.size[2] * absR[2][1];
    if (abs(t[1] * R[0][0] - t[0] * R[1][0]) > ra + rb)
        return false;

     //Test axis L = A2 x B1
    ra = one.size[0] * absR[1][1] + one.size[1] * absR[0][1];
    rb = two.size[0] * absR[2][2] + two.size[2] * absR[2][0];
    if (abs(t[1] * R[0][1] - t[0] * R[1][1]) > ra + rb)
        return false;

     //Test axis L = A2 x B2
    ra = one.size[0] * absR[1][2] + one.size[1] * absR[0][2];
    rb = two.size[0] * absR[2][1] + two.size[1] * absR[2][0];
    if (abs(t[1] * R[0][2] - t[0] * R[1][2]) > ra + rb)
        return false;

    return true;
}

void writeContact(uint writePoint, uint indexOne, uint indexTwo)
{
    uint priorityOne = swapIndexes[indexOne];
    uint priorityTwo = swapIndexes[indexTwo];

    bool needSwap = isNeedSwapIndexes(indexOne, indexTwo, priorityOne, priorityTwo);
    
    if(needSwap)
    {
        uint temp = indexTwo;
        indexTwo = indexOne;
        indexOne = temp;
    }

    ivec2 size = imageSize(headPointsOutput);
    
    ivec2 coord = ivec2(indexOne % size.x, indexOne / size.x);
    int lastIndex = int(imageAtomicExchange(headPointsOutput, coord, int(writePoint)).x);

    ivec2 data = ivec2(indexTwo, lastIndex);

    outContactNodes[writePoint] = data;
}

#endif