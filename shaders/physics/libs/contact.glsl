#ifndef CONTACT_GLSL_GLSL
#define CONTACT_GLSL_GLSL

#define EPSILON_CONTACT 0.0000001

#include "buffers.glsl"
#include "math.glsl"
#include "closestPoint.glsl"
#include "gjk.glsl"

struct GlobalContactData
{
    vec3 positionA;
    vec3 positionB;
};

GlobalContactData generateContactPlaneSphere(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 planePos = firstOrientation.position.xyz;
    vec3 planeRotate = (transpose(quaternionToMat3(firstOrientation.rotation))[1]).xyz;

    vec3 spherePos = secondOrientation.position.xyz;
    float sphereRadius = secondData.data_1.x;
    
    float distToPlane = dot(planeRotate, planePos);
    float distToSphere = dot(planeRotate, spherePos);

    float diff = distToSphere - distToPlane;
    
    GlobalContactData data;
    data.positionA = spherePos - planeRotate * diff;
    data.positionB = spherePos - sphereRadius * planeRotate;
    return data;
}   

GlobalContactData generateContactPlaneBox(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                            DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 planePos = firstOrientation.position.xyz;
    vec3 planeRotate = (transpose(quaternionToMat3(firstOrientation.rotation))[1]).xyz;

    vec3 boxPos = secondOrientation.position.xyz;
    
    vec3 boxSize = secondOrientation.size.xyz;
    mat3 boxRotate = quaternionToMat3(secondOrientation.rotation);

    vec3 localDir = boxRotate * (-planeRotate);
    
    vec3 posOnBox;
    posOnBox.x = (localDir.x > 0) ? boxSize.x : -boxSize.x;
    posOnBox.y = (localDir.y > 0) ? boxSize.y : -boxSize.y;
    posOnBox.z = (localDir.z > 0) ? boxSize.z : -boxSize.z;
    posOnBox = posOnBox * boxRotate + boxPos;

    float distToPlane = dot(planeRotate, planePos);
    float ditsPosOnBoxToPlane = dot(planeRotate, posOnBox);

    GlobalContactData data;
    data.positionA = posOnBox + planeRotate * (distToPlane - ditsPosOnBoxToPlane);
    data.positionB = posOnBox;
    return data;
}   

GlobalContactData generateContactPlaneCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 planePos = firstOrientation.position.xyz;
    vec3 planeRotate = (transpose(quaternionToMat3(firstOrientation.rotation))[1]).xyz;

    vec3 capsulePos = secondOrientation.position.xyz;

    float capsuleRadius = secondData.data_1.x;
    float capsuleHeight = secondData.data_1.y;

    mat3 capsuleRotate = transpose(quaternionToMat3(secondOrientation.rotation));
    vec3 upCapsule = capsuleRotate[1].xyz;

    vec3 pointA = capsulePos + upCapsule * capsuleHeight;
    vec3 pointB = capsulePos - upCapsule * capsuleHeight;

    float distToPlane = dot(planeRotate, planePos);
    float distToPointA = dot(planeRotate, pointA);
    float distToPointB = dot(planeRotate, pointB);

    float diffToA = distToPointA - distToPlane;
    float diffToB = distToPointB - distToPlane;

    GlobalContactData data;

    if(diffToA < diffToB)
    {
        data.positionA = pointA - planeRotate * diffToA;
        data.positionB = pointA - planeRotate * capsuleRadius;

    }
    else
    {
        data.positionA = pointB - planeRotate * diffToB;
        data.positionB = pointB - planeRotate * capsuleRadius;
    }
    return data;
}   


GlobalContactData generateContactSphereSphere(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 firstPos = firstOrientation.position.xyz;
    vec3 secondPos = secondOrientation.position.xyz;
    float firstRadius = firstData.data_1.x;
    float secondRadius = secondData.data_1.x;

    vec3 diffToFirst = firstPos - secondPos;
    float dist = length(diffToFirst);
    vec3 normal = diffToFirst / dist;

    GlobalContactData data;
    data.positionA = -normal * firstRadius + firstPos;
    data.positionB = normal * secondRadius + secondPos;
    return data;
}   

GlobalContactData generateContactBoxSphere(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                            DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 boxPos = firstOrientation.position.xyz;
    vec3 spherePos = secondOrientation.position.xyz;
    
    vec3 boxSize = firstOrientation.size.xyz;
    mat3 boxRotate = quaternionToMat3(firstOrientation.rotation);

    float sphereRadius = secondData.data_1.x;

    vec3 closestP = closestPointBox(spherePos, boxPos, boxSize, boxRotate);
    
    vec3 diffToCenter = boxPos - spherePos;
    vec3 diffToClosestP = closestP - spherePos;
    float dist = length(diffToClosestP);

    vec3 closestSpherePoint;

    if(dist <= EPSILON_CONTACT)
    {
        vec3 dir = boxRotate * (spherePos - boxPos);
        vec3 absDir = abs(dir);
        float maxSide = max(absDir.x, max(absDir.y, absDir.z));
        if(absDir.x == maxSide)
            dir.x = boxSize.x * sign(dir.x);
        else if(absDir.y == maxSide)
            dir.y = boxSize.y * sign(dir.y);
        else
            dir.z = boxSize.z * sign(dir.z);

        closestP = dir * boxRotate + boxPos;

        vec3 d = normalize(spherePos - closestP);

        closestSpherePoint = d * sphereRadius + spherePos;
    }
    else
    {
        closestSpherePoint = (diffToClosestP / dist) * sphereRadius + spherePos;
    }


    GlobalContactData data;
    data.positionA = closestP;
    data.positionB = closestSpherePoint;
    return data;
}

GlobalContactData generateContactSphereCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 spherePos = firstOrientation.position.xyz;
    vec3 capsulePos = secondOrientation.position.xyz;

    float sphereRadius = firstData.data_1.x;
    float capsuleRadius = secondData.data_1.x;
    float capsuleHeight = secondData.data_1.y;

    mat3 capsuleRotate = transpose(quaternionToMat3(secondOrientation.rotation));
    vec3 upCapsule = capsuleRotate[1].xyz;

    vec3 pointA = capsulePos + upCapsule * capsuleHeight;
    vec3 pointB = capsulePos - upCapsule * capsuleHeight;

    vec3 closestP = closestPointSegment(spherePos, pointA, pointB);

    GlobalContactData data;
    data.positionA = spherePos + normalize(closestP - spherePos) * sphereRadius;
    data.positionB = closestP + normalize(spherePos - closestP) * capsuleRadius;
    return data;
}   

GlobalContactData generateContactCapsuleCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                                DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    vec3 firstPos = firstOrientation.position.xyz;
    vec3 secondPos = secondOrientation.position.xyz;

    float firstRadius = firstData.data_1.x;
    float secondRadius = secondData.data_1.x;
    float firstHeight = firstData.data_1.y;
    float secondHeight = secondData.data_1.y;

    mat3 firstRotate = transpose(quaternionToMat3(firstOrientation.rotation));
    mat3 secondRotate = transpose(quaternionToMat3(secondOrientation.rotation));

    vec3 firstA = firstPos + firstRotate[1].xyz * firstHeight;
    vec3 firstB = firstPos - firstRotate[1].xyz * firstHeight;

    vec3 secondA = secondPos + secondRotate[1].xyz * secondHeight;
    vec3 secondB = secondPos - secondRotate[1].xyz * secondHeight;

    vec3 c1 = vec3(0, 0, 0);
    vec3 c2 = vec3(0, 0, 0);
    float sqrDist = sqrDistSegmentSegment(firstA, firstB, secondA, secondB, c1, c2);
    vec3 diffC1toC2 = normalize(c2 - c1);

    GlobalContactData data;
    data.positionA = c1 + diffC1toC2 * firstRadius;
    data.positionB = c2 - diffC1toC2 * secondRadius;
    return data;
}

//gjk contacts: ---------

vec3 supportBox(vec3 searchDir, in GeometryGJK gjk)
{
    searchDir = gjk.rotInv * searchDir;
    
    vec3 result;
    result.x = (searchDir.x > 0) ? gjk.data_1.x : -gjk.data_1.x;
    result.y = (searchDir.y > 0) ? gjk.data_1.y : -gjk.data_1.y;
    result.z = (searchDir.z > 0) ? gjk.data_1.z : -gjk.data_1.z;
    return gjk.rot * result + gjk.pos;
}

vec3 supportCapsule(vec3 searchDir, in GeometryGJK gjk)
{
    searchDir = gjk.rotInv * searchDir;
    
    vec3 result = normalize(searchDir) * gjk.data_1.x;
    result.y += (searchDir.y > 0) ? gjk.data_1.y : -gjk.data_1.y;
    return gjk.rot * result + gjk.pos;
}


#ifdef CONTACT_BOX_BOX_GJK

vec3 supportA(vec3 searchDir, in GeometryGJK gjk)
{
    return supportBox(searchDir, gjk);
}

vec3 supportB(vec3 searchDir, in GeometryGJK gjk)
{
    return supportBox(searchDir, gjk);
}

GlobalContactData generateContactBoxBox(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                        DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    GeometryGJK gGJK_1;
    gGJK_1.data_1.xyz = firstOrientation.size.xyz;
    gGJK_1.data_2 = firstData.data_2;
    gGJK_1.rotInv = quaternionToMat3(firstOrientation.rotation);
    gGJK_1.rot = transpose(gGJK_1.rotInv);
    gGJK_1.pos = firstOrientation.position.xyz;

    GeometryGJK gGJK_2;
    gGJK_2.data_1.xyz = secondOrientation.size.xyz;
    gGJK_2.data_2 = secondData.data_2;
    gGJK_2.rotInv = quaternionToMat3(secondOrientation.rotation);
    gGJK_2.rot = transpose(gGJK_2.rotInv);
    gGJK_2.pos = secondOrientation.position.xyz;

    ContactInfoGJK contactInfo;
    generateContactGJK(gGJK_1, gGJK_2, contactInfo);

    GlobalContactData data;
    data.positionA = contactInfo.localPosA + gGJK_1.pos;
    data.positionB = contactInfo.localPosB + gGJK_2.pos;
    return data;
}

#endif

#ifdef CONTACT_BOX_CAPSULE_GJK

vec3 supportA(vec3 searchDir, in GeometryGJK gjk)
{
    return supportBox(searchDir, gjk);
}

vec3 supportB(vec3 searchDir, in GeometryGJK gjk)
{
    return supportCapsule(searchDir, gjk);
}

GlobalContactData generateContactBoxCapsule(DataGeometry firstData, DataOrientationGeometryOBB firstOrientation,
                                            DataGeometry secondData, DataOrientationGeometryOBB secondOrientation)
{
    GeometryGJK gGJK_1;
    gGJK_1.data_1.xyz = firstOrientation.size.xyz;
    gGJK_1.data_2 = firstData.data_2;
    gGJK_1.rotInv = quaternionToMat3(firstOrientation.rotation);
    gGJK_1.rot = transpose(gGJK_1.rotInv);
    gGJK_1.pos = firstOrientation.position.xyz;

    GeometryGJK gGJK_2;
    gGJK_2.data_1 = secondData.data_1;
    gGJK_2.data_2 = secondData.data_2;
    gGJK_2.rotInv = quaternionToMat3(secondOrientation.rotation);
    gGJK_2.rot = transpose(gGJK_2.rotInv);
    gGJK_2.pos = secondOrientation.position.xyz;

    ContactInfoGJK contactInfo;
    generateContactGJK(gGJK_1, gGJK_2, contactInfo);

    GlobalContactData data;
    data.positionA = contactInfo.localPosA + gGJK_1.pos;
    data.positionB = contactInfo.localPosB + gGJK_2.pos;
    return data;
}

#endif

#endif