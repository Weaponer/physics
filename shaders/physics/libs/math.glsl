#ifndef MATH_GLSL
#define MATH_GLSL

mat3 quaternionToMat3(vec4 quaternion)
{
    vec4 q = quaternion;
    return mat3(1.0 - 2.0 * (q.y * q.y + q.z * q.z), 2.0 * (q.x * q.y - q.w * q.z), 2.0 * (q.x * q.z + q.w * q.y),
                2.0 * (q.x * q.y + q.w * q.z), 1.0 - 2.0 * (q.x * q.x + q.z * q.z), 2.0* (q.y * q.z - q.w * q.x),
                2.0 * (q.x * q.z - q.w * q.y), 2.0 * (q.y * q.z + q.w * q.x), 1.0 - 2.0 * (q.x * q.x + q.y * q.y));
}

mat4 quaternionToMat4(vec4 quaternion)
{
    mat3 m3 = quaternionToMat3(quaternion);
    mat4 m4 = mat4(1.0);
    m4[0] = vec4(m3[0].xyz, 0.0);
    m4[1] = vec4(m3[1].xyz, 0.0);
    m4[2] = vec4(m3[2].xyz, 0.0);
    m4[3] = vec4(0.0, 0.0, 0.0, 1.0);
    return m4;
}

mat3 mat4ToMat3(mat4 mat)
{
    return mat3(mat[0][0], mat[0][1], mat[0][2],
                mat[1][0], mat[1][1], mat[1][2],
                mat[2][0], mat[2][1], mat[2][2]);
}

mat4 mat3ToMat4(mat3 mat)
{
    return mat4(mat[0][0], mat[0][1], mat[0][2], 0,
                mat[1][0], mat[1][1], mat[1][2], 0,
                mat[2][0], mat[2][1], mat[2][2], 0,
                0, 0, 0, 1);
}

vec4 quaternionInverse(vec4 quaternion)
{
    quaternion.x = -quaternion.x;
    quaternion.y = -quaternion.y;
    quaternion.z = -quaternion.z;
    return quaternion;
}

vec4 quaternionMultiply(vec4 quaternion1, vec4 quaternion2)
{
    vec4 q1 = quaternion1;
    vec4 q2 = quaternion2;
    return vec4(q1.w * q2.x + q1.x * q2.w +
                q1.y * q2.z - q1.z * q2.y,

                q1.w * q2.y + q1.y * q2.w +
                q1.z * q2.x - q1.x * q2.z,
                
                q1.w * q2.z + q1.z * q2.w +
                q1.x * q2.y - q1.y * q2.x,
                
                q1.w * q2.w - q1.x * q2.x -
                q1.y * q2.y - q1.z * q2.z);
}

vec4 quaternionAddScaledVector(vec4 quaternion, vec3 vector, float scale)
{
    vec4 q2 = vec4(vector.x * scale, vector.y * scale, vector.z * scale, 0);
    vec4 q = quaternionMultiply(q2, quaternion);
    
    quaternion += q * 0.5;
    return quaternion;
}

vec4 rotateQuaternionByVector(vec4 quaternion, vec3 vector)
{
    vec4 q2 = vec4(vector, 0);
    return quaternionMultiply(quaternion, q2);
}

vec3 rotateVectorByQuaternion(vec4 quaternion, vec3 vector)
{
    vec4 q = quaternion;
    vec3 v = vector;
    return vec3(2.0 * (v.x * (0.5 - (q.y * q.y + q.z * q.z)) + v.y * ((q.x * q.y - q.w * q.z)) + v.z * (q.x * q.z + q.w * q.y)),
                2.0 * (v.x * (q.x * q.y + q.w * q.z) + v.y * (0.5 - (q.x * q.x + q.z * q.z)) + v.z * (q.y * q.z - q.w * q.x)),
                2.0 * (v.x * (q.x * q.z - q.w * q.y) + v.y * (q.y * q.z + q.w * q.x) + v.z * (0.5 - (q.x * q.x + q.y * q.y))));
}

#endif