#ifndef CLOSEST_POINT_GLSL
#define CLOSEST_POINT_GLSL

#include "math.glsl"

vec3 closestPointBox(vec3 p, vec3 origin, vec3 size, mat3 rotate)
{
    vec3 d = p - origin;

    vec3 localD = rotate * d;

    localD.x = clamp(localD.x, -size.x, size.x);
    localD.y = clamp(localD.y, -size.y, size.y);
    localD.z = clamp(localD.z, -size.z, size.z);

    return localD * rotate + origin;
}

vec3 closestPointSegment(vec3 p, vec3 a, vec3 b)
{
    vec3 ab = b - a;
    float t = dot(p - a, ab) / dot(ab, ab);
    if (t < 0.0)
        t = 0.0;
    if (t > 1.0)
        t = 1.0;

    return a + t * ab;
}

float sqrDistSegmentSegment(vec3 p1, vec3 q1, vec3 p2, vec3 q2, out vec3 c1, out vec3 c2)
{
    vec3 d1 = q1 - p1;
    vec3 d2 = q2 - p2;
    vec3 r = p1 - p2;
    float a = dot(d1, d1);
    float e = dot(d2, d2);
    float f = dot(d2, r);

    float s = 0.0;
    float t = 0.0;

    c1 = vec3(0, 0, 0);
    c2 = vec3(0, 0, 0);

    float epsilon = 0.000001;
    if(a <= epsilon && e <= epsilon)
    {
        s = t = 0.0;
        c1 = p1;
        c2 = p2;
        return dot((c1 - c2), (c1 - c2));
    }

    if(a <= epsilon)
    {
        s = 0.0;
        t = f / e;
        t = clamp(t, 0.0, 1.0);
    }
    else
    {
        float c = dot(d1, r);
        if(e <= epsilon)
        {
            t = 0.0;
            s = clamp(-c / a, 0.0, 1.0);
        }
        else
        {
            float b = dot(d1, d2);
            float denom = a * e - b * b;
            if(denom != 0.0)
            {
                s = clamp((b * f - c * e) / denom, 0.0, 1.0);
            }
            else
            {
                s = 0.0;
            }

            t = (b * s + f) / e;

            if(t < 0.0)
            {
                t = 0.0;
                s = clamp(-c / a, 0.0, 1.0);
            }
            else if(t > 1.0)
            {
                t = 1.0;
                s = clamp((b - c) / a, 0.0, 1.0);
            } 
        }
    }
    c1 = p1 + d1 * s;
    c2 = p2 + d2 * t;
    return dot((c1 - c2), (c1 - c2)); 
}

#endif