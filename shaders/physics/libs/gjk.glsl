#ifndef GJK_GLSL
#define GJK_GLSL

#define GJK_ITERATIONS 64

#define EPA_TOLERANCE 0.0001
#define EPA_MAX_NUM_FACES 64
#define EPA_MAX_LOOSE_EDGES 32
#define EPA_MAX_NUM_ITERATIONS 64

struct GeometryGJK
{
    vec4 data_1;
    vec4 data_2;

    mat3 rot;
    mat3 rotInv;

    vec3 pos;
};

struct ContactInfoGJK
{
    vec3 localPosA;
    vec3 localPosB;
    vec3 normal;
    float penetration;
};

struct PointGJK
{
    vec3 searchDir;
    vec3 p;             //support(searchDir, b) - support(-searchDir, a) 
};

vec3 supportA(vec3 searchDir, in GeometryGJK geom);
vec3 supportB(vec3 searchDir, in GeometryGJK geom);

PointGJK calculateSearchPointGJK(vec3 searchDir, GeometryGJK geomA, GeometryGJK geomB)
{
    searchDir = normalize(searchDir);

    PointGJK p;
    p.searchDir = searchDir;
    p.p = supportB(searchDir, geomB) - supportA(-searchDir, geomA);
    return p;
}

void updateSimplex3GJK(inout PointGJK a, inout PointGJK b, inout PointGJK c, inout PointGJK d, inout int simDim, inout vec3 searchDir)
{
    vec3 n = cross(b.p - a.p, c.p - a.p);
    vec3 AO = -a.p;

    simDim = 2;
    if(dot(cross(b.p - a.p, n), AO) > 0)
    {
        c = a;
        searchDir = cross(cross(b.p - a.p, AO), b.p - a.p);
        return;
    }
    if(dot(cross(b.p, c.p - a.p), AO) > 0)
    {
        b = a;
        searchDir = cross(cross(c.p - a.p, AO), c.p - a.p);
        return;
    }

    simDim = 3;
    if(dot(n, AO) > 0)
    {
        d = c;
        c = b;
        b = a;
        searchDir = n;
        return;
    }

    d = b;
    b = a;
    searchDir = -n;
    return;
}

bool updateSimplex4GJK(inout PointGJK a, inout PointGJK b, inout PointGJK c, inout PointGJK d, inout int simDim, inout vec3 searchDir)
{
    vec3 abc = cross(b.p - a.p, c.p - a.p);
    vec3 acd = cross(c.p - a.p, d.p - a.p);
    vec3 adb = cross(d.p - a.p, b.p - a.p);

    vec3 AO = -a.p;
    simDim = 3;
    if(dot(abc, AO) > 0)
    {
        d = c;
        c = b;
        b = a;
        searchDir = abc;
        return false;
    }

    if(dot(acd, AO) > 0)
    {
        b = a;
        searchDir = acd;
        return false;
    }

    if(dot(adb, AO) > 0)
    {
        c = d;
        d = b;
        b = a;
        searchDir = adb;
        return false;
    }

    return true;
}

void barycentricGJK(vec3 a, vec3 b, vec3 c, vec3 p, out float u, out float v, out float w)
{
    vec3 v0 = b - a;
    vec3 v1 = c - a;
    vec3 v2 = p - a;
    float d00 = dot(v0, v0);
    float d01 = dot(v0, v1);
    float d11 = dot(v1, v1);
    float d20 = dot(v2, v0);
    float d21 = dot(v2, v1);
    float denom = d00 * d11 - d01 * d01;
    v = (d11 * d20 - d01 * d21) / denom;
    w = (d00 * d21 - d01 * d20) / denom;
    u = 1.0 - v - w;
}

void generateOutputInfoEPA(PointGJK a, PointGJK b, PointGJK c, PointGJK d, in GeometryGJK geomA, in GeometryGJK geomB, out ContactInfoGJK contactInfo)
{
    vec3 dir = d.p;
    vec3 zeroD = -a.p;
    float dist = dot(zeroD, dir);
    vec3 projPoint = - dist * dir;

    float u = 0.0;
    float v = 0.0;
    float w = 0.0;
    barycentricGJK(a.p, b.p, c.p, projPoint, u, v, w);

    vec3 localA = supportA(-a.searchDir, geomA) * u +
                    supportA(-b.searchDir, geomA) * v +
                    supportA(-c.searchDir, geomA) * w;

    vec3 localB = supportB(a.searchDir, geomB) * u +
                    supportB(b.searchDir, geomB) * v +
                    supportB(c.searchDir, geomB) * w;
    
    vec3 diff = localB - localA;
    float penetration = length(diff);
    vec3 normal = diff / penetration;

    localA -= geomA.pos;
    localB -= geomB.pos;

    contactInfo.localPosA = localA;
    contactInfo.localPosB = localB;
    contactInfo.normal = normal;
    contactInfo.penetration = penetration;
}

void generateContactEPA(PointGJK a, PointGJK b, PointGJK c, PointGJK d, in GeometryGJK geomA, in GeometryGJK geomB, out ContactInfoGJK contactInfo)
{
    PointGJK faces[EPA_MAX_NUM_FACES][4];

    faces[0][0] = a;
    faces[0][1] = b;
    faces[0][2] = c;
    faces[0][3].p = normalize(cross(b.p - a.p, c.p - a.p));
    faces[1][0] = a;
    faces[1][1] = c;
    faces[1][2] = d;
    faces[1][3].p = normalize(cross(c.p - a.p, d.p - a.p));
    faces[2][0] = a;
    faces[2][1] = d;
    faces[2][2] = b;
    faces[2][3].p = normalize(cross(d.p - a.p, b.p - a.p));
    faces[3][0] = b;
    faces[3][1] = d;
    faces[3][2] = c;
    faces[3][3].p = normalize(cross(d.p - b.p, c.p - b.p));

    PointGJK looseEdges[EPA_MAX_LOOSE_EDGES][2];

    int numFaces = 4;
    int closestFace = 0;

    for(int iter = 0; iter < EPA_MAX_NUM_ITERATIONS; iter++)
    {
        float minDist = dot(faces[0][0].p, faces[0][3].p);
        closestFace = 0;
        for(int i = 1; i < numFaces; i++)
        {
            float dist = dot(faces[i][0].p, faces[i][3].p);
            if(dist < minDist)
            {
                minDist = dist;
                closestFace = i;
            }
        }

        vec3 searchDir = faces[closestFace][3].p;
        PointGJK p = calculateSearchPointGJK(searchDir, geomA, geomB);

        if(dot(p.p, searchDir) - minDist < EPA_TOLERANCE)
        {
            // output
            generateOutputInfoEPA(faces[closestFace][0], faces[closestFace][1], faces[closestFace][2], faces[closestFace][3], 
                                    geomA, geomB, contactInfo);
            return;
        }

        int numLooseEdges = 0;
        for(int i = 0; i < numFaces; i++)
        {
            if(dot(faces[i][3].p, p.p - faces[i][0].p) > 0)
            {
                for(int j = 0; j < 3; j++)
                {
                    PointGJK currentEdge[2];
                    currentEdge[0] = faces[i][j];
                    currentEdge[1] = faces[i][(j + 1) % 3];

                    bool foundEdge = false;
                    for(int k = 0; k < numLooseEdges; k++)
                    {
                        if(looseEdges[k][1].p == currentEdge[0].p && looseEdges[k][0].p == currentEdge[1].p)
                        {
                            looseEdges[k][0] = looseEdges[numLooseEdges - 1][0];
                            looseEdges[k][1] = looseEdges[numLooseEdges - 1][1];
                            numLooseEdges--;
                            foundEdge = true;
                            k = numLooseEdges;
                        }
                    }

                    if(!foundEdge)
                    {
                        if(numLooseEdges >= EPA_MAX_LOOSE_EDGES)
                            break;
                        
                        looseEdges[numLooseEdges][0] = currentEdge[0];
                        looseEdges[numLooseEdges][1] = currentEdge[1];
                        numLooseEdges++;
                    }
                }   

                numFaces--;
                faces[i][0] = faces[numFaces][0];
                faces[i][1] = faces[numFaces][1];
                faces[i][2] = faces[numFaces][2];
                faces[i][3] = faces[numFaces][3];
                i--;
            }
        }
        for(int i = 0; i < numLooseEdges; i++)
        {
            if(numFaces >= EPA_MAX_NUM_FACES)
                break;
            
            float bias = 0.000001;

            faces[numFaces][0] = looseEdges[i][0];
            faces[numFaces][1] = looseEdges[i][1];
            faces[numFaces][2] = p;
            vec3 dir = cross(looseEdges[i][0].p - looseEdges[i][1].p, looseEdges[i][0].p - p.p);
            float l = length(dir);
            
            if(l <= bias)
                continue;
            
            faces[numFaces][3].p = dir / l;

            if(dot(faces[numFaces][0].p, faces[numFaces][3].p) + bias < 0)
            {
                PointGJK temp = faces[numFaces][0];
                faces[numFaces][0] = faces[numFaces][1];
                faces[numFaces][1] = temp;
                faces[numFaces][3].p = -faces[numFaces][3].p;
            }
            numFaces++;
        }
    }

    //output
    generateOutputInfoEPA(faces[closestFace][0], faces[closestFace][1], faces[closestFace][2],  faces[closestFace][3], 
                            geomA, geomB, contactInfo);
    return;
}

bool generateContactGJK(in GeometryGJK geomA, in GeometryGJK geomB, out ContactInfoGJK contactInfo)
{
    contactInfo.localPosA = vec3(0.0, 0.0, 0.0); 
    contactInfo.localPosB = vec3(0.0, 0.0, 0.0); 
    contactInfo.normal = vec3(0.0, 0.0, 0.0); 
    contactInfo.penetration = 0.0;

    vec3 geomPosA = geomA.pos;
    vec3 geomPosB = geomB.pos;

    PointGJK a;
    PointGJK b;
    PointGJK c;
    PointGJK d;

    vec3 searchDir = geomPosA - geomPosB;

    c = calculateSearchPointGJK(searchDir, geomA, geomB);
    searchDir = -c.p;
    b = calculateSearchPointGJK(searchDir, geomA, geomB);

    if(dot(b.p, searchDir) < 0.0)
        return false;

    searchDir = cross(cross(c.p - b.p, -b.p), c.p - b.p);
    if(searchDir == vec3(0.0, 0.0, 0.0))
    {
        searchDir = cross(c.p - b.p, vec3(1.0, 0.0, 0.0));

        if(searchDir == vec3(0.0, 0.0, 0.0))
            searchDir = cross(c.p - b.p, vec3(0.0, 0.0, 1.0));
    }

    int simDim = 2;
    for(int iter = 0; iter < GJK_ITERATIONS; iter++)
    {
        a = calculateSearchPointGJK(searchDir, geomA, geomB);

        if(dot(a.p, searchDir) < 0)
            return false;
        
        simDim++;
        if(simDim == 3)
        {
            updateSimplex3GJK(a, b, c, d, simDim, searchDir);
        }
        else if(updateSimplex4GJK(a, b, c, d, simDim, searchDir))
        {
            generateContactEPA(a, b, c, d, geomA, geomB, contactInfo);
            return true;
        }
    }

    return false;
}

#endif