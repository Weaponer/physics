#ifndef TABLE_CONTACTS_UTILS_GLSL
#define TABLE_CONTACTS_UTILS_GLSL

bool isNeedSwapIndexes(uint indexOne, uint indexTwo, uint priorityOne, uint priorityTwo)
{
    if(priorityOne > priorityTwo)
        return true;
    else if(priorityOne == priorityTwo && indexOne < indexTwo)
        return true;
    return false;
}

#endif