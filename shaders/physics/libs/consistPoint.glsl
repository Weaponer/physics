#ifndef CONSIST_POINT_GLSL
#define CONSIST_POINT_GLSL

bool checkBoxConsistPont(vec3 p, vec3 origin, vec3 size, mat3 rotate)
{
    vec3 d = p - origin;
    vec3 localP = rotate * d;
    if(abs(localP.x) <= size.x && abs(localP.y) <= size.y && abs(localP.z) <= size.z)
        return true;
    else
        return false;
}

bool checkSphereConsistPoint(vec3 p, vec3 origin, float radius)
{
    vec3 diff = p - origin;
    float sqrL = diff.x * diff.x + diff.y + diff.y + diff.z + diff.z;
    return sqrL <= (radius * radius);
}

bool checkCapsuleConsistPont(vec3 p, vec3 origin, float height, float radius, mat3 rotate)
{
    vec3 d = p - origin;
    vec3 localP = rotate * d;

    vec3 a = vec3(0, -height, 0);
    vec3 b = vec3(0, height, 0);

    vec3 ab = (b - a);
    float t = dot(localP - a, ab) / dot(ab, ab);

    if (t < 0.0)
        t = 0.0;
    if (t > 1.0)
        t = 1.0;

    float checkDist = length(localP - (ab * t + a));

    if(checkDist <= radius)
        return true;
    else
        return false;
}

#endif