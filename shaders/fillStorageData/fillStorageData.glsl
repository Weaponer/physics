layout(std430, set = 0, binding = 0) buffer outStorageBuffer
{
    int countCalls;
    int firstValue;
} outBuffer;

uniform layout(r32i, set = 0, binding = 1) iimage2D _storeImageData;
uniform layout(r32i, set = 0, binding = 2) iimage1D _atomicCounter;

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
void main()
{
    outBuffer.countCalls = int(imageLoad(_atomicCounter, 0).x);
    outBuffer.firstValue = int(imageLoad(_storeImageData, ivec2(0, 0)).x);
}