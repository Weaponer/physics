./output/sprvCompiler compile -i \
shaders/fillImage/fillImge.cfg \
shaders/fillStorageData/fillStorageData.cfg \
shaders/fillStorageData/fillStorageData.cfg \
shaders/basicRender/vert.cfg \
shaders/basicRender/frag.cfg \
-nshaders -oshaders/cshaders


./output/sprvCompiler compile -i \
shaders/render/fullScreenVertex/fullScreenVertex.cfg \
shaders/render/floor/floorFragment/floorFragment.cfg \
shaders/render/floor/floorScenePicker/floorScenePicker.cfg \
shaders/render/floor/floorSelected/floorSelected.cfg \
shaders/render/debugLineVertex/debugLineVertex.cfg \
shaders/render/debugLineFragment/debugLineFragment.cfg \
shaders/render/mesh/meshVertex/meshVertex.cfg \
shaders/render/mesh/meshFragment/meshFragment.cfg \
shaders/render/mesh/meshLineFragment/meshLineFragment.cfg \
shaders/render/mesh/meshLineVertex/meshLineVertex.cfg \
shaders/render/mesh/meshSelectedVertex/meshSelectedVertex.cfg \
shaders/render/mesh/meshSelectedFragment/meshSelectedFragment.cfg \
shaders/render/testProjectionFragment/testProjectionFragment.cfg \
shaders/render/mesh/meshScenePickerFragment/meshScenePickerFragment.cfg \
shaders/render/mesh/meshScenePickerVertex/meshScenePickerVertex.cfg \
shaders/render/blurPass/blurPass.cfg \
shaders/render/pivot/pivotVertex/pivotVertex.cfg \
shaders/render/pivot/pivotFragment/pivotFragment.cfg \
-nrender -oshaders/cshaders

./output/sprvCompiler compile -i \
shaders/physics/updateGeometryOBBs/updateGeometryOBBs.cfg \
shaders/physics/boundOBBs/buildBoundOBBs/buildBoundOBBs.cfg \
shaders/physics/boundOBBs/calculateAverageOBBs/calculateAverageOBBs.cfg \
shaders/physics/boundOBBs/calculateDispersionOBBs/calculateDispersionOBBs.cfg \
shaders/physics/boundOBBs/checkContactsBoundOBBs/checkContactsBoundOBBs.cfg \
shaders/physics/boundOBBs/rebalanceBoundOBBs/rebalanceBoundOBBs.cfg \
                                                                    \
shaders/physics/potentialContacts/kdTreeNodeBuilder/kdTreeNodeBuilder.cfg \
shaders/physics/potentialContacts/kdTreeNodeOffsetsBuilder/kdTreeNodeOffsetsBuilder.cfg \
shaders/physics/potentialContacts/linkedListBuilder/linkedListBuilder.cfg \
shaders/physics/potentialContacts/segmentsBuilder/segmentsBuilder.cfg \
                                                                    \
shaders/physics/tableContacts/fillNewIndexes/fillNewIndexes.cfg \
shaders/physics/tableContacts/updateSwapTable/updateSwapTable.cfg \
shaders/physics/tableContacts/generateListContacts/generateListContacts.cfg \
shaders/physics/tableContacts/generateLastListContacts/generateLastListContacts.cfg \
shaders/physics/tableContacts/generateListContactsPlane/generateListContactsPlane.cfg \
shaders/physics/tableContacts/fillTableContacts/fillTableContacts.cfg \
shaders/physics/tableContacts/potentialContactsCollector/potentialContactsCollector.cfg \
shaders/physics/tableContacts/generateDispatchGroupsByCounter/generateDispatchGroupsByCounter.cfg \
shaders/physics/tableContacts/checkGeometriesContact/checkGeometriesContact.cfg \
shaders/physics/tableContacts/contactsContainerExchanger/contactsContainerExchanger.cfg \
shaders/physics/tableContacts/generateDispatchUpdateContacts/generateDispatchUpdateContacts.cfg \
shaders/physics/tableContacts/contactsContainerDistributor/contactsContainerDistributor.cfg \
shaders/physics/tableContacts/checkLostContainer/checkLostContainer.cfg \
shaders/physics/tableContacts/copyContacts/copyContacts.cfg \
                                                                \
shaders/physics/contacts/contactsGenerator/contactsGenerator.cfg \
shaders/physics/contacts/contactsUpdate/contactsUpdate.cfg \
shaders/physics/contacts/containersCounter/containersCounter.cfg \
shaders/physics/contacts/contactsUniter/contactsUniter.cfg \
                                                            \
shaders/physics/inertialTensors/tensorGenerator/tensorGenerator.cfg \
shaders/physics/inertialTensors/tensorsSumator/tensorsSumator.cfg   \
                                                                    \
shaders/physics/physicsSimulation/prepare/initContainersCommonData/initContainersCommonData.cfg \
shaders/physics/physicsSimulation/prepare/generateContainersList/generateContainersList.cfg \
shaders/physics/physicsSimulation/prepare/generateActorsData/generateActorsData.cfg \
shaders/physics/physicsSimulation/prepare/generateEdgesData/generateEdgesData.cfg \
shaders/physics/physicsSimulation/prepare/defineRootEdges/defineRootEdges.cfg \
shaders/physics/physicsSimulation/prepare/setupRootEdges/setupRootEdges.cfg \
shaders/physics/physicsSimulation/prepare/generateSplitedPairs/generateSplitedPairs.cfg \
                                                                                    \
shaders/physics/physicsSimulation/LCP/accelerationInitLCP/accelerationInitLCP.cfg \
shaders/physics/physicsSimulation/LCP/initSimContainersDataLCP/initSimContainersDataLCP.cfg \
shaders/physics/physicsSimulation/LCP/solveContainerContacts/solveContainerContacts.cfg \
shaders/physics/physicsSimulation/LCP/simulateActorsLCP/simulateActorsLCP.cfg \
                                                                            \
shaders/physics/physicsSimulation/PBD/simulateActorsPBD/simulateActorsPBD.cfg \
shaders/physics/physicsSimulation/PBD/fillSimCotainersDataPBD/fillSimCotainersDataPBD.cfg \
shaders/physics/physicsSimulation/PBD/solveContactsPBD/solveContactsPBD.cfg \
-nphysics -oshaders/cshaders