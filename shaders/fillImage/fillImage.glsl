layout(std140, set = 0, binding = 0) uniform _dataBuffer
{
    int value;
};

layout(r32i, set = 0, binding = 1) uniform iimage2D _storeImageData;
layout(r32i, set = 0, binding = 2) uniform iimage1D _atomicCounter;
 
layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
void main()
{
    imageStore(_storeImageData, ivec2(gl_GlobalInvocationID.xy), ivec4(value.xxxx));
    imageAtomicAdd(_atomicCounter, 0, 1);
}