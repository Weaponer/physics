#define FLOOR_DATA(setIndex, bindingIndex)                                              \
layout(std140, row_major, set = setIndex, binding = bindingIndex) uniform floorData     \
{                                                                                       \
    mat4 FloorTRS;                                                                      \
    mat4 FloorInverseTRS;                                                               \
    vec3 FloorColor;                                                                    \
};