struct FloorSample
{
    vec3 worldPos;
    vec3 locPosition;
    vec2 uv;
    vec3 projection;
};

FloorSample getFloorSample(vec3 cameraPosition, vec3 cameraDirection)
{
    vec3 direction = cameraDirection;
    vec3 camPos = cameraPosition;
    
    vec3 locDirection = (FloorInverseTRS * vec4(direction, 0.0)).xyz;
    vec3 locPosition = (FloorInverseTRS * vec4(camPos, 1.0)).xyz;
    float d = dot(locDirection, vec3(0, -sign(locPosition.y), 0)); 
    if(d <= 0)
    {
        discard;
    }
    float ac = locPosition.y;

    float t = ac / d;
    vec2 uv = (locPosition + locDirection * t).xz;

    vec3 worldPos = camPos + direction * t;
    vec4 projection = CameraVP * vec4(worldPos, 1.0);
    projection /= projection.w;
    if(projection.z >= 1.0)
    {
        discard;
    }

    FloorSample s;
    s.worldPos = worldPos;
    s.locPosition = locPosition;
    s.uv = uv;
    s.projection = projection.xyz;
    return s;
}