#define CAMERA_DATA(setIndex, bindingIndex)                                             \
layout(std140, row_major, set = setIndex, binding = bindingIndex) uniform cameraMatrixes\
{                                                                                       \
    mat4 CameraV;                                                                       \
    mat4 CameraInverseV;                                                                \
    mat4 CameraP;                                                                       \
    mat4 CameraInverseP;                                                                \
    mat4 CameraVP;                                                                      \
    vec3 CameraPosition;                                                                \
    float farPlane;                                                                     \
    float nearPlane;                                                                    \
};