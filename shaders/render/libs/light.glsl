

vec3 blinPhong(vec3 position, vec3 normal, vec3 view, vec3 diffuseColor)
{
    const vec3 sunDirection = normalize(vec3(0.0, 1.2, 1.0));
    const float sunIntensity = 0.04;
    const vec3 ambientColor = vec3(1, 1, 1) * 0.01;
    const float gammaCorrection = 2.2;

    float NdotL = dot(normal, sunDirection);
	float intensity = clamp(NdotL, 0, 1);

    vec3 colorLinear = intensity * diffuseColor * sunIntensity;

    vec3 H = normalize(sunDirection + view);
    float NdotH = clamp(dot(normal, H), 0, 1);

    intensity = pow(NdotH, 128);

    colorLinear += intensity * diffuseColor * sunIntensity + ambientColor * diffuseColor;

    vec3 colorGammaCorrected = pow(colorLinear, vec3(1.0 / gammaCorrection));

    return colorGammaCorrected;
}   