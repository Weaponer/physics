layout(location = 0) in vec2 inUV;
layout(location = 1) in vec3 inNormal;

layout(location = 0) out vec4 outColor;

layout(std140, row_major, set = 1, binding = 0) uniform floorData     
{                          
    int width;
    int height;                                                            
    int iterations;
};

layout(set = 1, binding = 1) uniform sampler2D readTexture;

layout(push_constant, std430) uniform Constants
{
    int iteration;
    bool finalPass;
} pConstants;

void main()
{
    vec4 data = texture(readTexture, inUV).rgba;
    if(pConstants.finalPass)
    {
        if(data.a == 1.0)
            data.a = 0.0;
        
        outColor = data;
        return;
    }
    else
    {
        if(data.a != 0)
        {
            outColor = data;
            return;
        }

        float deltaX = 1.0 / float(width);
        float deltaY = 1.0 / float(height);
        vec4 up = textureLod(readTexture, inUV + vec2(0, deltaY), 0).rgba;
        vec4 down = textureLod(readTexture, inUV - vec2(0, deltaY), 0).rgba;
        vec4 left = textureLod(readTexture, inUV - vec2(deltaX, 0), 0).rgba;
        vec4 right = textureLod(readTexture, inUV + vec2(deltaX, 0), 0).rgba;

        int count = 0;
        vec4 sum = vec4(0, 0, 0, 0);
        if(up.a != 0)
        {
            count++;
            sum += up;
        }
        if(down.a != 0)
        {
            count++;
            sum += down;
        }
        if(left.a != 0)
        {
            count++;
            sum += left;
        }
        if(right.a != 0)
        {
            count++;
            sum += right;
        }
        
        if(count == 0)
        {
            outColor = vec4(0, 0, 0, 0);
            return;
        }
        else
        {
            vec4 average = sum / count;
            average.a = pow((float(iterations - pConstants.iteration - 1) / iterations), 2); 
            outColor = average; 
            return;
        }
    }
}