layout(location = 0) in vec3 inColor;

layout(location = 0) out vec4 outColor;

layout(push_constant, std430) uniform Constants
{
    float alpha;
} pConstants;

void main()
{
    outColor = vec4(inColor, pConstants.alpha);
}