#include "cameraData.glsl"

layout(location = 0) in vec3 inPos;

//instance
layout(location = 1) in mat4 inTRS; // use 1 2 3 4
layout(location = 5) in vec4 color;

layout(location = 0) out vec4 outColor;

CAMERA_DATA(0, 0)

void main()
{
    outColor = color;
    gl_Position = CameraVP * (vec4(inPos, 1.0) * inTRS);
}