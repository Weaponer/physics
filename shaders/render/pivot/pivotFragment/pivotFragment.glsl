layout(location = 0) in vec4 inColor;

layout(location = 0) out vec4 outColor;

void main()
{
    outColor = vec4(pow(inColor.rgb, vec3(2.2)).rgb, inColor.a);
}