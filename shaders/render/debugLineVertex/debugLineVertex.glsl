#include "cameraData.glsl"

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 outColor;

CAMERA_DATA(0, 0)

void main()
{
    outColor = inColor;
    gl_Position = CameraVP * vec4(inPos, 1.0);
}