#include "cameraData.glsl"
#include "floorData.glsl"

layout(location = 0) in vec2 inUV;
layout(location = 1) in vec3 inNormal;

layout(location = 0) out vec4 outColor;

CAMERA_DATA(1, 0)
FLOOR_DATA(1, 1)

layout(push_constant, std430) uniform Constants
{
    vec4 colorHiglight;
} pConstants;

#include "floorSample.glsl"

void main()
{
    FloorSample s = getFloorSample(CameraPosition, normalize(inNormal));
    gl_FragDepth = s.projection.z;
    outColor = vec4(pConstants.colorHiglight.xyz, 1.0);
}