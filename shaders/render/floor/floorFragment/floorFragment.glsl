#include "cameraData.glsl"
#include "floorData.glsl"
#include "light.glsl"

layout(location = 0) in vec2 inUV;
layout(location = 1) in vec3 inNormal;

layout(location = 0) out vec4 outColor;

CAMERA_DATA(1, 0)
FLOOR_DATA(1, 1)

#include "floorSample.glsl"

float getLinePattern(vec2 uv, float size)
{
    const float lineDist = 0.03;
    vec2 f = fract(uv / size);
    
    f.x = min(f.x, 1.0 - f.x);
    f.y = min(f.y, 1.0 - f.y);
    float minDist = min(f.x, f.y);
    return clamp((minDist) * (1.0 / lineDist), 0, 1);
}

float combineLines(vec2 uv, vec2 cameraPosition)
{
    vec2 delta = abs(uv - cameraPosition) / 6;
    float dist = max(delta.x, delta.y);

    float l = log2(dist);
    float s = floor(l);
    float b = ceil(l);

    float sizeMin = max(pow(2, s), 2);
    float sizeMax = max(pow(2, b), 2);

    return mix(getLinePattern(uv, sizeMin), getLinePattern(uv, sizeMax), 0.5);
}

void main()
{
    vec3 view = normalize(inNormal);
    FloorSample s = getFloorSample(CameraPosition, view);

    float pattern = combineLines(s.uv, s.locPosition.xz);

    gl_FragDepth = s.projection.z;

    vec3 normalFloor = (FloorTRS * vec4(0.0, 1.0, 0.0, 0.0)).xyz;

    vec3 color = FloorColor * pattern;
    outColor = vec4(blinPhong(s.worldPos, normalFloor, view, color), 1.0);
}