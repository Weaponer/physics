#include "cameraData.glsl"
#include "floorData.glsl"

layout(location = 0) in vec2 inUV;
layout(location = 1) in vec3 inNormal;

layout(location = 0) out uint outIndex;

CAMERA_DATA(1, 0)
FLOOR_DATA(1, 1)

layout(push_constant, std430) uniform Constants
{
    uint selectableIndex;
} pConstants;

#include "floorSample.glsl"

void main()
{
    FloorSample s = getFloorSample(CameraPosition, normalize(inNormal));
    gl_FragDepth = s.projection.z;
    outIndex = uint(pConstants.selectableIndex);
}