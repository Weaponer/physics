#include "cameraData.glsl"

layout(location = 0) in vec3 inPos;

//instance inputs
layout(location = 1) in mat4 inTRS; // use 1 2 3 4
layout(location = 5) in vec3 inColorHiglight;

layout(location = 0) out vec3 outColorHiglight;

CAMERA_DATA(0, 0)

void main()
{
    vec4 worldPos =  vec4(inPos, 1.0) * inTRS;
    outColorHiglight = inColorHiglight;    
    gl_Position = CameraVP * worldPos;
}