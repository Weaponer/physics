layout(location = 0) flat in uint inSelectableIndex;

layout(location = 0) out uint outIndex;

void main()
{
    outIndex = inSelectableIndex;
}