#include "light.glsl"

layout(location = 0) in vec3 inWorldPos;
layout(location = 1) in vec2 inUV;
layout(location = 2) in vec3 inWorldNormal;
layout(location = 3) in vec3 inView;
layout(location = 4) in vec3 inColor;

layout(location = 0) out vec4 outColor;

void main()
{
    outColor = vec4(blinPhong(inWorldPos, inWorldNormal, inView, inColor), 1.0);
}