layout(location = 0) in vec3 inColorHiglight;

layout(location = 0) out vec4 outColor;

void main()
{
    outColor = vec4(inColorHiglight, 1.0);
}