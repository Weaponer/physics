#include "cameraData.glsl"

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec2 inUV;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec3 inTangent;

//instance inputs
layout(location = 4) in mat4 inTRS; // use 4 5 6 7
layout(location = 8) in vec4 inColor;

layout(location = 0) out vec3 outWorldPos;
layout(location = 1) out vec2 outUV;
layout(location = 2) out vec3 outWorldNormal;
layout(location = 3) out vec3 outView;
layout(location = 4) out vec3 outColor;

CAMERA_DATA(0, 0)

void main()
{
    vec4 worldPos =  vec4(inPos, 1.0) * inTRS;
    vec3 worldNomral = normalize((vec4(inNormal, 0.0) * inTRS).xyz);
    
    outWorldPos = worldPos.xyz;
    outUV = inUV;
    outWorldNormal = worldNomral;
    outView = normalize(worldPos.xyz - CameraPosition);
    outColor = inColor.xyz;

    gl_Position = CameraVP * worldPos;
}