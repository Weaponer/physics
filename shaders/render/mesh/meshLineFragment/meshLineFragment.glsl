layout(location = 0) out vec4 outColor;

void main()
{
    gl_FragDepth = gl_FragCoord.z - 1.0 / 99999;
    outColor = vec4(0.0, 0.0, 0.0, 1.0);
}