#include "cameraData.glsl"

layout(location = 0) in vec3 inPos;

//instance inputs
layout(location = 1) in mat4 inTRS; // use 1 2 3 4

CAMERA_DATA(0, 0)

void main()
{
    vec4 worldPos =  vec4(inPos, 1.0) * inTRS;
    gl_Position = CameraVP * worldPos;
}