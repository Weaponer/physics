#include "cameraData.glsl"

layout(location = 0) in vec2 inUV;
layout(location = 1) in vec3 inNormal;

layout(location = 0) out vec4 outColor;

CAMERA_DATA(1, 0)

layout(set = 1, binding = 1) uniform sampler2D tempTexture;

void main()
{
    /*vec3 up = normalize((CameraInverseV * vec4(0, 1, 0, 0)).xyz);
    float forceY = (1.0 - inUV.y) / 2;
    float c = dot(up, vec3(0, 1, 0));
    float aspect = 1400.0 / 800.0;

    float dist = (forceY * (1.0 - c));

    float forceX = (inUV.x - dist) / (1.0 - dist * 2);

    vec2 newUV = vec2( forceX, inUV.y);*/

    if(inUV.x < 0.0 || inUV.x > 1.0)
    {
        outColor =  vec4(0, 0, 0, 1);
    }
    else
    {
        outColor = vec4(texture(tempTexture, inUV).xyz, max(texture(tempTexture, inUV).a, 1.0));
    }
}