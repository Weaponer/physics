#include "cameraData.glsl"

layout(location = 0) in vec2 inPos;
layout(location = 1) in vec2 inUV;


layout(location = 0) out vec2 outUV;
layout(location = 1) out vec3 outNormal;

CAMERA_DATA(0, 0)

void main()
{
    outUV = inUV;
    vec4 proj = CameraInverseP * vec4(vec3(inPos.x, inPos.y, 1.0), 1.0);
    proj /= proj.w;
    outNormal = normalize((CameraInverseV * vec4(proj.xyz, 0.0)).xyz);
    gl_Position = vec4(inPos.x, inPos.y, 0.0, 1.0);
}