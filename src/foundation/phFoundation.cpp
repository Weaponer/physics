#include "foundation/phFoundation.h"

namespace phys
{
    PhFoundation::PhFoundation(PhResourceProvider *_resoureProvider, GPUFoundation *_gpuFoundation,
                               PhThreadsManager *_threadsManager)
        : resoureProvider(_resoureProvider), gpuFoundation(_gpuFoundation),
          threadsManager(_threadsManager)
    {
    }

    PhFoundation::~PhFoundation()
    {
    }

    PhFoundation *PhFoundation::phCreateFoundation(PhResourceProvider *_resoureProvider, GPUFoundation *_gpuFoundation, PhThreadsManager *_threadsManager)
    {
        return new PhFoundation(_resoureProvider, _gpuFoundation, _threadsManager);
    }

    void PhFoundation::phDestroyFoundation(PhFoundation *_foundation)
    {
        delete _foundation;
    }
}