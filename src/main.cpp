#include <iostream>
#include <SDL2/SDL.h>

#include "gpuCommon/gpuFoundation.h"

#include "foundation/phMemoryAllocator.h"
#include "foundation/phFoundation.h"
#include "foundation/phMemoryAllocator.h"
#include "foundation/phBuffer.h"

#include "extension/phVulkanLibraryLinuxProvider.h"
#include "extension/phResourceLinuxProvider.h"

#include "window/window.h"

#include "threads/phThreadsManager.h"

#include "tests/threadsTester.h"
#include "tests/gpuTester.h"
#include "tests/physicsTester.h"

#include "launcher/phLauncher.h"

int main(int argc, char *argv[])
{
    phys::PhVulkanLibraryLinuxProvider vulkanProvider = phys::PhVulkanLibraryLinuxProvider();
    phys::PhResourceLinuxProvider *resourceProvider = new (phys::phAllocateMemory(sizeof(phys::PhResourceLinuxProvider))) phys::PhResourceLinuxProvider();
    vulkanProvider.load();

    SDL_Init(SDL_INIT_VIDEO);
    phys::Window *win = new (phys::phAllocateMemory(sizeof(phys::Window))) phys::Window("Test", 1400, 800);
    phys::GPUFoundationSettings gpuSettings{};
    gpuSettings.minCountGraphicQueues = 1;
    gpuSettings.minCountComputeQueues = 2;

    phys::GPUFoundation *gpufondation = new phys::GPUFoundation(&vulkanProvider, &gpuSettings, win);

   phys::PhThreadsManager *threadsManager = new phys::PhThreadsManager();
    threadsManager->allocateTaskThreads(15);

    phys::PhFoundation *physFoundation = phys::PhFoundation::phCreateFoundation(resourceProvider, gpufondation, threadsManager);
    /*phys::PhysicsTester *physicsTest = new phys::PhysicsTester(physFoundation);
    physicsTest->test();
    delete physicsTest;*/

    phys::PhPhysics *physics = phys::PhPhysics::createPhysics(physFoundation);

    phys::PhLauncher *launcher = phys::PhLauncher::phCreateLauncher();
    launcher->init(physFoundation, physics, resourceProvider);
    launcher->initRendering(gpufondation, win, gpufondation->getCommandReceiver()->getFreeQueue(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT));
    launcher->launch();
    phys::PhLauncher::phDestroyLauncher(launcher);

    phys::PhPhysics::destroyPhysics(physics);
    phys::PhFoundation::phDestroyFoundation(physFoundation);

    delete threadsManager;

    PH_RELEASE(win);
    PH_RELEASE(resourceProvider);

    delete gpufondation;

    phys::phPrintAllocateMemory();
}
