#include "physicsEngine/peFoundation.h"
#include "physicsEngine/peSceneSimulator.h"

namespace phys
{
    PeFoundation::PeFoundation(PhFoundation *_foundation) : foundation(_foundation)

    {
        materialManager = new (phAllocateMemory(sizeof(InMaterialManager))) InMaterialManager();
        shapeManager = new (phAllocateMemory(sizeof(InShapeManager))) InShapeManager();
        simulator = new (phAllocateMemory(sizeof(PeSceneSimulator))) PeSceneSimulator(this);
    }

    PeFoundation *PeFoundation::createPeFoundation(PhFoundation *_foundation)
    {
        return new (phAllocateMemory(sizeof(PeFoundation))) PeFoundation(_foundation);
    }

    void PeFoundation::destroyPeFoundation(PeFoundation *_physics)
    {
        PH_RELEASE(_physics);
    }

    void PeFoundation::release()
    {
        PH_RELEASE(simulator);
        PH_RELEASE(shapeManager);
        PH_RELEASE(materialManager);
        PhBase::release();
    }
}