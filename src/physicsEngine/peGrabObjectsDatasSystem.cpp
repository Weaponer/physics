#include "physicsEngine/peGrabObjectsDatasSystem.h"

#include "common/phMacros.h"

namespace phys
{
    PeGrabObjectsDatasSystem::PeGrabObjectsDatasSystem() : PeSystem(),
                                                           stackData()
    {
    }

    void PeGrabObjectsDatasSystem::release()
    {
        unloadData();
        stackData.release();
    }

    void PeGrabObjectsDatasSystem::addDataToUnload(PeGpuMemory *_data)
    {
        stackData.assignmentMemoryAndCopy(&_data);
    }

    void PeGrabObjectsDatasSystem::unloadData()
    {
        while (!stackData.empty())
        {
            PeGpuMemory *data = *stackData.getMemoryAndPop();
            PH_RELEASE(data);
        }
    }
}