#include "physicsEngine/shapeDescription/peShapeDescriptionSystem.h"
#include "geometry/internal/inGeometryBase.h"

namespace phys
{
    PeShapeDescriptionSystem::PeShapeDescriptionSystem()
        : PeSystem(), shapeType(), geometryType(),
          geometryChainData(), bufferEvents(),
          gpu(NULL),
          grabObjectsSystem(NULL),
          materialsDescriptionSystem(NULL),
          staticLoader(NULL),
          shapeManager(NULL),
          shapeBuffer(NULL), geometriesBuffer(NULL)
    {
    }

    void PeShapeDescriptionSystem::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        materialsDescriptionSystem = simulationContext->get<PeMaterialsDescriptionSystem>();
        staticLoader = simulationContext->get<PeGpuStaticLoaderSystem>();
        shapeManager = simulationContext->get<InShapeManager>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();

        shapeType = PeGpuObjectType::getObjectType<PeGpuDescriptionShot, PeGpuDataShape>(10, false, 16);
        geometryType = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataGeometry>(10, false, 16);

        shapeBuffer = PH_NEW(PeGpuObjectsBuffer)("ShapesBuffer", gpu, &shapeType);
        geometriesBuffer = PH_NEW(PeGpuObjectsBuffer)("GeometriesBuffer", gpu, &geometryType);
    }

    void PeShapeDescriptionSystem::release()
    {
        PH_RELEASE(geometriesBuffer);
        PH_RELEASE(shapeBuffer);

        bufferEvents.release();
        geometryChainData.release();
    }

    void PeShapeDescriptionSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        memoryBarrierController->initUnknowMemory(geometriesBuffer);
        memoryBarrierController->initUnknowMemory(shapeBuffer);

        gpu->computeCommands->debugMarkerBegin("ShapeDescriptionPrepareData", PeDebugMarkersUtils::subGroupColor());

        ensureShapesBufferCapacity(shapeManager->getCapacityTable());

        uint32_t capacityGeometries = updateShapes();
        ensureGeometriesBufferCapacity(capacityGeometries);

        updateGeometries(bufferEvents.getMemory(0), bufferEvents.getPos());

        gpu->computeCommands->debugMarkerEnd();
    }

    uint32_t PeShapeDescriptionSystem::getLocalIndexShape(InShape *_shape)
    {
        return _shape->managerRegistration.getIndex();
    }

    const PeGpuDataShape *PeShapeDescriptionSystem::getGpuDataShape(InShape *_shape)
    {
        return shapeManager->getDataPerId(_shape->managerRegistration.getIndex());
    }

    void PeShapeDescriptionSystem::ensureShapesBufferCapacity(uint32_t _capacity)
    {
        if (_capacity > shapeType.getMaxCountObjects())
        {
            uint32_t oldSize = shapeBuffer->getSize();
            shapeType = PeGpuObjectType::getObjectType<PeGpuDescriptionShot, PeGpuDataShape>(_capacity, false);

            PeGpuObjectsBuffer *oldBuffer = shapeBuffer;

            shapeBuffer = PH_NEW(PeGpuObjectsBuffer)("ShapesBuffer", gpu, &shapeType);

            gpu->computeCommands->copyBuffer(*oldBuffer, 0, *shapeBuffer, 0, oldSize);
            grabObjectsSystem->addDataToUnload(oldBuffer);
        }
    }

    void PeShapeDescriptionSystem::ensureGeometriesBufferCapacity(uint32_t _capacity)
    {
        if (_capacity > geometryType.getMaxCountObjects())
        {
            uint32_t oldSize = geometriesBuffer->getSize();
            geometryType = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataGeometry>(_capacity, false, 16);

            PeGpuObjectsBuffer *oldBuffer = geometriesBuffer;

            geometriesBuffer = PH_NEW(PeGpuObjectsBuffer)("GeometriesBuffer", gpu, &geometryType);

            gpu->computeCommands->copyBuffer(*oldBuffer, 0, *geometriesBuffer, 0, oldSize);
            grabObjectsSystem->addDataToUnload(oldBuffer);
        }
    }

    uint32_t PeShapeDescriptionSystem::updateShapes()
    {
        staticLoader->begin();

        bufferEvents.clearMemory();
        uint32_t maxCountGeomtries = 0;

        InShapeManager::ManagerEvent event;
        while (shapeManager->getNextEvent(&event))
        {
            PeGpuDataShape *dataShape = shapeManager->getDataPerId(event.index);
            bool sendData = false;

            if (event.event == InShapeManager::ManagerEvent_Created)
            {
                InShape *shape = shapeManager->getObjectPerId(event.index);
                uint32_t countGeometries = shape->countGeometries();

                if (countGeometries)
                {
                    PeBufferChain::Segment segment = geometryChainData.allocateSegment(countGeometries);
                    dataShape->countGeometries = segment.count;
                    dataShape->indexFirstGeometry = segment.firstIndex;
                    maxCountGeomtries = std::max(maxCountGeomtries, dataShape->countGeometries + dataShape->indexFirstGeometry);
                }
                else
                {
                    dataShape->countGeometries = 0;
                    dataShape->indexFirstGeometry = 0;
                }

                PhMaterial *mat = shape->getMaterial();
                if (mat == NULL)
                    dataShape->indexMaterial = -1;
                else
                    dataShape->indexMaterial = materialsDescriptionSystem->getLocalIndexMaterial(dynamic_cast<InMaterial *>(mat));
                dataShape->flags = 0;

                printf("create shape\n");
                sendData = true;
            }
            else if (event.event == InShapeManager::ManagerEvent_Destroyed)
            {
                PeBufferChain::Segment segment;
                segment.count = dataShape->countGeometries;
                segment.firstIndex = dataShape->indexFirstGeometry;
                geometryChainData.freeSegment(segment);

                printf("destroy shape\n");
            }
            else if (event.event == InShapeManager::ManagerEvent_Changed)
            {
                InShape *shape = shapeManager->getObjectPerId(event.index);
                uint32_t countGeometries = shape->countGeometries();
                if (dataShape->countGeometries != countGeometries)
                {
                    PeBufferChain::Segment segment;
                    segment.count = dataShape->countGeometries;
                    segment.firstIndex = dataShape->indexFirstGeometry;

                    geometryChainData.freeSegment(segment);
                    if (countGeometries)
                    {
                        segment = geometryChainData.allocateSegment(shape->countGeometries());
                    }
                    else // if zero geometries
                    {
                        segment.count = 0;
                        segment.firstIndex = 0;
                    }
                    dataShape->countGeometries = segment.count;
                    dataShape->indexFirstGeometry = segment.firstIndex;

                    maxCountGeomtries = std::max(maxCountGeomtries, dataShape->countGeometries + dataShape->indexFirstGeometry);
                }

                PhMaterial *mat = shape->getMaterial();
                if (mat == NULL)
                    dataShape->indexMaterial = 0;
                else
                    dataShape->indexMaterial = materialsDescriptionSystem->getLocalIndexMaterial(dynamic_cast<InMaterial *>(mat));
                dataShape->flags = 0;

                sendData = true;
                printf("change shape\n");
            }

            if (sendData)
            {
                staticLoader->sendData(dataShape, sizeof(PeGpuDataShape),
                                       *shapeBuffer, shapeType.getAlignmentOffsetToIndex(event.index));
                bufferEvents.assignmentMemoryAndCopy(&event);
            }
        }

        staticLoader->end();
        return maxCountGeomtries;
    }

    void PeShapeDescriptionSystem::updateGeometries(InShapeManager::ManagerEvent *_events, uint32_t _countEvents)
    {
        if (_countEvents)
            staticLoader->begin();

        for (uint32_t i = 0; i < _countEvents; i++)
        {
            InShapeManager::ManagerEvent event = _events[i];
            InShape *shpae = shapeManager->getObjectPerId(event.index);
            PeGpuDataShape shadeData = *shapeManager->getDataPerId(event.index);

            if (shadeData.countGeometries == 0)
                continue;

            uint32_t alignmentSize = geometryType.getAlignmentSize();
            uint8_t data[alignmentSize * shadeData.countGeometries];

            for (uint32_t i2 = 0; i2 < shadeData.countGeometries; i2++)
            {
                PhGeometry *geometry = shpae->getGeometry(i2);
                PeGpuDataGeometry dataGeometry = dynamic_cast<InGeometryBase *>(geometry)->getGpuDataGeometry();
                std::memcpy(data + alignmentSize * i2, &dataGeometry, alignmentSize);
            }

            staticLoader->sendData(data, sizeof(data), *geometriesBuffer,
                                   geometryType.getAlignmentOffsetToIndex(shadeData.indexFirstGeometry));

            printf("send shape geometries\n");
        }

        if (_countEvents)
            staticLoader->end();
    }
}