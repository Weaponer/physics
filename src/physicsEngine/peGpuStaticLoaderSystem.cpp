#include "physicsEngine/peGpuStaticLoaderSystem.h"

namespace phys
{
    PeGpuStaticLoaderSystem::PeGpuStaticLoaderSystem()
        : PeSystem(),
          gpu(NULL), buffers(), pWrite(NULL), indexCurrentBuffer(0), offsetCurrentBuffer(0),
          sizeBuffer(4096U)
    {
    }

    void PeGpuStaticLoaderSystem::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        vk::VulkanBuffer *mainBuffer = createBuffer();
        buffers.assignmentMemoryAndCopy(&mainBuffer);
    }

    void PeGpuStaticLoaderSystem::release()
    {
        uint32_t count = buffers.getPos();
        for (uint32_t i = 0; i < count; i++)
            gpu->bufferAllocator->destroyBuffer(*buffers.getMemory(i));
        buffers.release();
    }

    void PeGpuStaticLoaderSystem::begin()
    {
        pWrite = gpu->computeCommands->beginWrite(*buffers.getMemory(indexCurrentBuffer), 0, sizeBuffer);
    }

    void PeGpuStaticLoaderSystem::end()
    {
        gpu->computeCommands->endWrite(*buffers.getMemory(indexCurrentBuffer));
        pWrite = NULL;
    }

    void PeGpuStaticLoaderSystem::sendData(const void *_data, uint32_t _sizeData, vk::VulkanBuffer *_dstBuffer, uint32_t _dstOffset)
    {
        bool handleWrite = false;
        if (!pWrite)
            handleWrite = true;

        if (handleWrite)
            begin();

        uint32_t wrote = 0;
        while (wrote < _sizeData)
        {
            uint32_t canWrite = std::min(std::max(sizeBuffer - offsetCurrentBuffer, 0U), _sizeData - wrote);
            if (canWrite)
            {
                std::memcpy((uint8_t *)pWrite + offsetCurrentBuffer, (uint8_t *)_data + wrote, canWrite);

                vk::VulkanBuffer *buffer = *buffers.getMemory(indexCurrentBuffer);
                gpu->computeCommands->copyBuffer(buffer, offsetCurrentBuffer, _dstBuffer, _dstOffset + wrote, canWrite);
            }

            offsetCurrentBuffer += canWrite;
            wrote += canWrite;
            if (offsetCurrentBuffer >= sizeBuffer)
                allocateNextBuffer();
        }

        if (handleWrite)
            end();
    }

    void PeGpuStaticLoaderSystem::resetBuffers()
    {
        indexCurrentBuffer = 0;
        offsetCurrentBuffer = 0;
    }

    vk::VulkanBuffer *PeGpuStaticLoaderSystem::createBuffer()
    {
        return gpu->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_Staging, sizeBuffer,
                                                  1, &gpu->mainFamilyQueue);
    }

    void PeGpuStaticLoaderSystem::allocateNextBuffer()
    {
        end(); // stop writing

        if ((indexCurrentBuffer + 1) >= buffers.getPos())
        {
            vk::VulkanBuffer *buffer = createBuffer();
            buffers.assignmentMemoryAndCopy(&buffer);
        }

        indexCurrentBuffer += 1;
        offsetCurrentBuffer = 0;

        begin(); // begin new writing
    }
}