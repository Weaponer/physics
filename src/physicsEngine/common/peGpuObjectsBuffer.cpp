#include <vulkan/vulkan.h>
#include <cstring>

#include "physicsEngine/common/peGpuObjectsBuffer.h"

namespace phys
{
    PeGpuObjectsBuffer::PeGpuObjectsBuffer(const char *_name, const PeGpuData *_gpuData,
                                           PeGpuObjectType *_typeDescription, bool _hostVisibility)
        : PeGpuObjectsData(_name)
    {
        initType(_gpuData, _typeDescription);
        setHostVisibility(_hostVisibility);
        allocateData();
    }

    void PeGpuObjectsBuffer::allocateData()
    {
        uint32_t sizeBuffer = typeDescription.getMaxCountObjects() * typeDescription.getAlignmentSize();

        vk::VulkanBufferType type;
        if (hostVisibility)
        {
            type = shaderWriting ? vk::VulkanBufferType_UniformStorageBufferDynamic : vk::VulkanBufferType_UniformStorageBufferDynamic;
        }
        else
        {
            type = shaderWriting ? vk::VulkanBufferType_UniformStorageBufferStatic : vk::VulkanBufferType_UniformStorageBufferStatic;
        }
        buffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, type, sizeBuffer, 1, &mainFamilyIndex, name.c_str());
    }

    void PeGpuObjectsBuffer::release()
    {
        if (buffer != NULL)
        {
            bufferAllocator->destroyBuffer(buffer);
            buffer = NULL;
        }
    }
}