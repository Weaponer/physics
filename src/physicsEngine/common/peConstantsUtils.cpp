#include <string>

#include "physicsEngine/common/peConstantsUtils.h"

namespace phys
{
    const char *PeConstantsUtils::getShaderName(PeComputeShaders _shader)
    {
        const char *names[PeCompute_CountAll];
        names[PeCompute_UpdateGeometryOBBs] = "updateGeometryOBBs";
        names[PeCompute_BuildBoundOBBs] = "buildBoundOBBs";
        names[PeCompute_CalculateAverageOBBs] = "calculateAverageOBBs";
        names[PeCompute_CalculateDispersionOBBs] = "calculateDispersionOBBs";
        names[PeCompute_CheckContactsBoundOBBs] = "checkContactsBoundOBBs";
        names[PeCompute_RebalanceBoundOBBs] = "rebalanceBoundOBBs";

        names[PeCompute_KdTreeNodeBuilder] = "kdTreeNodeBuilder";
        names[PeCompute_KdTreeNodeOffsetsBuilder] = "kdTreeNodeOffsetsBuilder";
        names[PeCompute_LinkedListBuilder] = "linkedListBuilder";
        names[PeCompute_SegmentsBuilder] = "segmentsBuilder";

        names[PeCompute_FillNewIndexes] = "fillNewIndexes";
        names[PeCompute_UpdateSwapTable] = "updateSwapTable";
        names[PeCompute_GenerateListContacts] = "generateListContacts";
        names[PeCompute_GenerateLastListContacts] = "generateLastListContacts";
        names[PeCompute_GenerateListContactsPlane] = "generateListContactsPlane";
        names[PeCompute_FillTableContacts] = "fillTableContacts";
        names[PeCompute_PotentialContactsCollector] = "potentialContactsCollector";
        names[PeCompute_GenerateDistapchGroupByCounter] = "generateDispatchGroupsByCounter";
        names[PeCompute_CheckGeometriesContact] = "checkGeometriesContact";
        names[PeCompute_ContactsContainerExchanger] = "contactsContainerExchanger";
        names[PeCompute_GenerateDispatchUpdateContacts] = "generateDispatchUpdateContacts";
        names[PeCompute_ContactsContainerDistributor] = "contactsContainerDistributor";
        names[PeCompute_CheckLostContainer] = "checkLostContainer";
        names[PeCompute_CopyContacts] = "copyContacts";

        names[PeCompute_ContactsGenerator] = "contactsGenerator";
        names[PeCompute_ContactsUpdate] = "contactsUpdate";
        names[PeCompute_ContainersCounter] = "containersCounter";
        names[PeCompute_ContactsUniter] = "contactsUniter";

        names[PeCompute_TensorGenerator] = "tensorGenerator";
        names[PeCompute_TensorSumator] = "tensorsSumator";

        names[PeCompute_InitContainersCommonData] = "initContainersCommonData";
        names[PeCompute_GenerateContainersList] = "generateContainersList";
        names[PeCompute_GenerateActorsData] = "generateActorsData";
        names[PeCompute_GenerateEdgesData] = "generateEdgesData";
        names[PeCompute_DefineRootEdges] = "defineRootEdges";
        names[PeCompute_SetupRootEdges] = "setupRootEdges";
        names[PeCompute_GenerateSplitedPairs] = "generateSplitedPairs";

        names[PeCompute_AccelerationInitLCP] = "accelerationInitLCP";
        names[PeCompute_InitSimContainersDataLCP] = "initSimContainersDataLCP";
        names[PeCompute_SolveContainerContacts] = "solveContainerContacts";
        names[PeCompute_SimulateActorsLCP] = "simulateActorsLCP";


        names[PeCompute_SimulateActorsPBD_Init] = "simulateActorsPBD";
        names[PeCompute_SimulateActorsPBD_Update] = "simulateActorsPBD_update";
        names[PeCompute_FillSimCotainersDataPBD] = "fillSimCotainersDataPBD";
        names[PeCompute_SolveContactsPBD] = "solveContactsPBD";
        
        
        return names[_shader];
    }

    bool PeConstantsUtils::isHaveTypeContactVariants(PeComputeShaders _shader)
    {
        bool flags[PeCompute_CountAll];
        for (uint32_t i = 0; i < PeCompute_CountAll; i++)
            flags[i] = false;

        flags[PeCompute_CheckGeometriesContact] = true;
        flags[PeCompute_ContactsGenerator] = true;
        flags[PeCompute_ContactsUpdate] = true;
        return flags[_shader];
    }

    bool PeConstantsUtils::isHaveConvexGeometryVariants(PeComputeShaders _shader)
    {
        bool flags[PeCompute_CountAll];
        for (uint32_t i = 0; i < PeCompute_CountAll; i++)
            flags[i] = false;

        flags[PeCompute_TensorGenerator] = true;

        return flags[_shader];
    }

    const char *PeConstantsUtils::getShaderNameTypeContact(PeComputeShaders _shader, PeComputeTypeContact _typeContact)
    {
        static const char *names[PeComputeTypeContact_CountAll];
        for (uint32_t i = 0; i < PeComputeTypeContact_CountAll; i++)
            names[i] = NULL;

        switch (_shader)
        {
        case PeCompute_CheckGeometriesContact:
            names[PeComputeTypeContact_PlaneBox] = "checkGeometriesContact_planeBox";
            names[PeComputeTypeContact_PlaneSphere] = "checkGeometriesContact_planeSphere";
            names[PeComputeTypeContact_PlaneCapsule] = "checkGeometriesContact_planeCapsule";
            names[PeComputeTypeContact_BoxBox] = "checkGeometriesContact_boxBox";
            names[PeComputeTypeContact_BoxSphere] = "checkGeometriesContact_boxSphere";
            names[PeComputeTypeContact_BoxCapsule] = "checkGeometriesContact_boxCapsule";
            names[PeComputeTypeContact_SphereSphere] = "checkGeometriesContact_sphereSphere";
            names[PeComputeTypeContact_SphereCapsule] = "checkGeometriesContact_sphereCapsule";
            names[PeComputeTypeContact_CapsuleCapsule] = "checkGeometriesContact_capsuleCapsule";
            return names[_typeContact];
        case PeCompute_ContactsGenerator:
            names[PeComputeTypeContact_PlaneBox] = "contactsGenerator_planeBox";
            names[PeComputeTypeContact_PlaneSphere] = "contactsGenerator_planeSphere";
            names[PeComputeTypeContact_PlaneCapsule] = "contactsGenerator_planeCapsule";
            names[PeComputeTypeContact_BoxBox] = "contactsGenerator_boxBox";
            names[PeComputeTypeContact_BoxSphere] = "contactsGenerator_boxSphere";
            names[PeComputeTypeContact_BoxCapsule] = "contactsGenerator_boxCapsule";
            names[PeComputeTypeContact_SphereSphere] = "contactsGenerator_sphereSphere";
            names[PeComputeTypeContact_SphereCapsule] = "contactsGenerator_sphereCapsule";
            names[PeComputeTypeContact_CapsuleCapsule] = "contactsGenerator_capsuleCapsule";
            return names[_typeContact];
        case PeCompute_ContactsUpdate:
            names[PeComputeTypeContact_PlaneBox] = "contactsUpdate_planeBox";
            names[PeComputeTypeContact_BoxBox] = "contactsUpdate_boxBox";
            names[PeComputeTypeContact_BoxCapsule] = "contactsUpdate_boxCapsule";
            names[PeComputeTypeContact_CapsuleCapsule] = "contactsUpdate_capsuleCapsule";
            return names[_typeContact];
        default:
            return NULL;
        }
        return NULL;
    }

    const char *PeConstantsUtils::getShaderNameConvexGeometry(PeComputeShaders _shader, PeComputeConvexGeometry _geometry)
    {
        static const char *names[PeComputeConvexGeometry_CountAll];
        for (uint32_t i = 0; i < PeComputeConvexGeometry_CountAll; i++)
            names[i] = NULL;

        switch (_shader)
        {
        case PeCompute_TensorGenerator:
            names[PeComputeConvexGeometry_Sphere] = "tensorGenerator_sphere";
            names[PeComputeConvexGeometry_Box] = "tensorGenerator_box";
            names[PeComputeConvexGeometry_Capsule] = "tensorGenerator_capsule";
            return names[_geometry];
        default:
            return NULL;
        }

        return NULL;
    }

    uint32_t *PeConstantsUtils::getCountContactsInContainers()
    {
        static uint32_t countContactsInContainers[PeGpuPairGeometriesContact_Count] =
            {
                PeGpuPairGeometriesCountContacts_PlaneBox,
                PeGpuPairGeometriesCountContacts_PlaneSphere,
                PeGpuPairGeometriesCountContacts_PlaneCapsule,
                PeGpuPairGeometriesCountContacts_BoxBox,
                PeGpuPairGeometriesCountContacts_BoxSphere,
                PeGpuPairGeometriesCountContacts_BoxCapsule,
                PeGpuPairGeometriesCountContacts_SphereSphere,
                PeGpuPairGeometriesCountContacts_SphereCapsule,
                PeGpuPairGeometriesCountContacts_CapsuleCapsule,
            };

        return countContactsInContainers;
    }

    uint32_t (*PeConstantsUtils::getTypeAndCountContacts())[2]
    {
        // TYPE | COUNT CONTACTS
        static uint32_t typeAndCountContacts[PeGpuPairGeometriesContact_Count][2] =
            {
                {PeGpuPairGeometriesContact_PlaneBoxMask, PeGpuPairGeometriesCountContacts_PlaneBox},
                {PeGpuPairGeometriesContact_PlaneSphereMask, PeGpuPairGeometriesCountContacts_PlaneSphere},
                {PeGpuPairGeometriesContact_PlaneCapsuleMask, PeGpuPairGeometriesCountContacts_PlaneCapsule},
                {PeGpuPairGeometriesContact_BoxBoxMask, PeGpuPairGeometriesCountContacts_BoxBox},
                {PeGpuPairGeometriesContact_BoxSphereMask, PeGpuPairGeometriesCountContacts_BoxSphere},
                {PeGpuPairGeometriesContact_BoxCapsuleMask, PeGpuPairGeometriesCountContacts_BoxCapsule},
                {PeGpuPairGeometriesContact_SphereSphereMask, PeGpuPairGeometriesCountContacts_SphereSphere},
                {PeGpuPairGeometriesContact_SphereCapsuleMask, PeGpuPairGeometriesCountContacts_SphereCapsule},
                {PeGpuPairGeometriesContact_CapsuleCapsuleMask, PeGpuPairGeometriesCountContacts_CapsuleCapsule},
            };
        return typeAndCountContacts;
    }
}