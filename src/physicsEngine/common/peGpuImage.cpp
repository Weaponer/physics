#include "physicsEngine/common/peGpuImage.h"
#include "common/phMacros.h"

namespace phys
{
    void PeGpuImage::allocateData(VkImageType _imageType, VkFormat _format, uint32_t _width, uint32_t _height,
                                  uint32_t _countLayers, bool _hostVisibility)
    {
        const char *c_name = getName();

        imageType = _imageType;
        format = _format;
        width = _width;
        height = _height;
        countLayers = _countLayers;

        if (_hostVisibility)
        {
            image = imageAllocator->createImage(vk::MemoryUseType_Long, vk::VulkanImageType_ImageDynamic, imageType,
                                                _format, VK_IMAGE_ASPECT_COLOR_BIT,
                                                width, height, 1, 1, countLayers, 1, &mainFamilyIndex, c_name);
        }
        else
        {
            image = imageAllocator->createImage(vk::MemoryUseType_Long, vk::VulkanImageType_ImageStatic, imageType,
                                                _format, VK_IMAGE_ASPECT_COLOR_BIT,
                                                width, height, 1, 1, countLayers, 1, &mainFamilyIndex, c_name);
        }

        VkImageViewType viewType;
        if (_imageType == VK_IMAGE_TYPE_1D)
            viewType = _countLayers > 1 ? VK_IMAGE_VIEW_TYPE_1D_ARRAY : VK_IMAGE_VIEW_TYPE_1D;
        else
            viewType = _countLayers > 1 ? VK_IMAGE_VIEW_TYPE_2D_ARRAY : VK_IMAGE_VIEW_TYPE_2D;

        vk::VulkanImageView::createImageView(device, viewType, image, &view, PH_CNAME(c_name, "View"));
    }

    void PeGpuImage::release()
    {
        imageAllocator->destroyImage(image);
        vk::VulkanImageView::destroyImageView(device, view);
    }
}