#include "physicsEngine/common/peUtilities.h"

namespace phys
{
    uint32_t PeUtilties::calculateGroups(uint32_t _sizeGroup, uint32_t _countLaunches)
    {
        if (_countLaunches % _sizeGroup != 0)
            return _countLaunches / _sizeGroup + 1;
        else
            return _countLaunches / _sizeGroup;
    }
}