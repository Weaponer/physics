#include "physicsEngine/common/peGpuTableContactsBuffer.h"
#include "common/phMacros.h"

namespace phys
{
    PeGpuTableContactsBuffer::PeGpuTableContactsBuffer(const char *_name, const PeGpuData *_gpuData)
        : PeGpuMemoryImage(_name), countIndexes(0), countContactsPerIndex(0),
          countLayers(0), countIndexesInLayers(0)
    {
    }

    void PeGpuTableContactsBuffer::allocateData(uint32_t _countIndexes, uint32_t _countContactsPerIndex, VkFormat _format)
    {
        const uint32_t maxSizeImageLayer = (1 << 14);

        countIndexes = _countIndexes;
        countLayers = 1;
        countIndexesInLayers = _countIndexes;
        countContactsPerIndex = _countContactsPerIndex;

        uint32_t width = _countIndexes;
        if (_countIndexes > maxSizeImageLayer)
        {
            countLayers = _countIndexes / maxSizeImageLayer;
            if (_countIndexes % maxSizeImageLayer != 0)
            {
                countLayers += 1;
            }

            width = maxSizeImageLayer;
            countIndexes = maxSizeImageLayer * countLayers;
            countIndexesInLayers = maxSizeImageLayer;
        }

        const char *c_name = name.c_str();

        image = imageAllocator->createImage(vk::MemoryUseType_Long, vk::VulkanImageType_ImageStatic, VK_IMAGE_TYPE_2D,
                                            _format, VK_IMAGE_ASPECT_COLOR_BIT, width, countContactsPerIndex,
                                            1, 1, countLayers,
                                            1, &mainFamilyIndex, c_name);
        vk::VulkanImageView::createImageView(device, VK_IMAGE_VIEW_TYPE_2D_ARRAY, image, &view, PH_CNAME(c_name, "View"));
    }

    void PeGpuTableContactsBuffer::release()
    {
        if (image)
        {
            vk::VulkanImageView::destroyImageView(device, view);
            imageAllocator->destroyImage(image);
        }
    }
}