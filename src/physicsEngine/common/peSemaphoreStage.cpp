#include "physicsEngine/common/peSemaphoreStage.h"

namespace phys
{
    PeSemaphoreStage::PeSemaphoreStage()
        : useStage(false), sempahore(), waitStage()
    {
    }

    PeSemaphoreStage::PeSemaphoreStage(const char *_name, const PeSemaphoreStage &_stage)
        : useStage(_stage.useStage), sempahore(), waitStage(_stage.waitStage)
    {
        if (_stage.sempahore.isValid())
        {
            vk::VulkanSemaphore::createSemaphore(_stage.sempahore.getDevice(), &sempahore, _name);
        }
    }

    PeSemaphoreStage::PeSemaphoreStage(const char *_name, vk::VulkanDevice *_device, VkPipelineStageFlags _waitStage)
        : useStage(false), sempahore(), waitStage(_waitStage)
    {
        vk::VulkanSemaphore::createSemaphore(_device, &sempahore, _name);
    }

    void PeSemaphoreStage::release()
    {
        if (sempahore.isValid())
        {
            vk::VulkanSemaphore::destroySemaphore(&sempahore);
            sempahore = vk::VulkanSemaphore();
        }
    }
}