#include "physicsEngine/common/peGpuObjectBuffer.h"

namespace phys
{

    PeGpuObjectBuffer::PeGpuObjectBuffer(const char *_name, const PeGpuData *_gpuData, PeGpuObjectType *_typeDescription)
        : PeGpuObjectsData(_name)
    {
        initType(_gpuData, _typeDescription);
        allocateData();
    }

    void PeGpuObjectBuffer::allocateData()
    {
        uint32_t sizeBuffer = typeDescription.getAlignmentSize();

        vk::VulkanBufferType type;
        if (hostVisibility)
        {
            type = shaderWriting ? vk::VulkanBufferType_UniformStorageBufferDynamic : vk::VulkanBufferType_UniformBlockDynamic;
        }
        else
        {
            type = shaderWriting ? vk::VulkanBufferType_UniformStorageBufferStatic : vk::VulkanBufferType_UniformBlockStatic;
        }
        buffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, type, sizeBuffer, 1, &mainFamilyIndex, name.c_str());
    }

    void PeGpuObjectBuffer::release()
    {
        if (buffer != NULL)
        {
            bufferAllocator->destroyBuffer(buffer);
            buffer = NULL;
        }
    }
}