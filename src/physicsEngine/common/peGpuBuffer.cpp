#include "physicsEngine/common/peGpuBuffer.h"

namespace phys
{

    PeGpuBuffer::PeGpuBuffer(const char *_name, PeGpuData *_gpu, vk::MemoryUseType _use, vk::VulkanBufferType _type, int _size)
        : PeGpuMemoryBuffer(_name)
    {
        init(_gpu);
        allocateData(_use, _type, _size);
    }

    void PeGpuBuffer::allocateData(vk::MemoryUseType _use, vk::VulkanBufferType _type, int _size)
    {
        buffer = bufferAllocator->createBuffer(_use, _type, _size, 1, &mainFamilyIndex, getName());
    }

    void PeGpuBuffer::release()
    {
        if (buffer)
        {
            bufferAllocator->destroyBuffer(buffer);
            buffer = NULL;
        }
    }
}