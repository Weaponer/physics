#include "physicsEngine/common/peBufferChain.h"

#include "foundation/phMemoryAllocator.h"

namespace phys
{
    PeBufferChain::PeBufferChain()
        : PhBase(), begin(NULL), freeSegments(NULL)
    {
        begin = getFreeSegment(1, 0, true);
    }

    void PeBufferChain::release()
    {
        clearListSegments(begin);
        clearListSegments(freeSegments);

        PhBase::release();
    }

    PeBufferChain::Segment PeBufferChain::allocateSegment(uint32_t _count)
    {
        Segment segment;
        segment.count = _count;

        if (_count == 0)
        {
            segment.firstIndex = 0;
            return segment;
        }

        SegmentNode *end;
        if (tryFindFreeSegment(_count, &end))
        {
            if (end->count == _count)
            {
                end->free = false;
                segment.firstIndex = end->index;

                tryCompound(end);
                return segment;
            }
            else
            {
                uint32_t otherSize = end->count - _count;
                segment.firstIndex = end->index;

                end->free = false;
                end->count = _count;

                SegmentNode *apartSegment = getFreeSegment(otherSize, end->index + _count, true);

                addSegmentAfter(end, apartSegment);

                tryCompound(end);
                tryCompound(apartSegment);

                return segment;
            }
        }
        else
        {
            if (end->free)
            {
                end->count = _count;
                segment.firstIndex = end->index;

                end->free = false;
                tryCompound(end);

                return segment;
            }
            else
            {
                uint32_t startIndex = end->count + end->index;
                end->count = end->count + _count;
                segment.firstIndex = startIndex;
                return segment;
            }
        }
    }

    PeBufferChain::Segment PeBufferChain::reallocateSegment(Segment _segment, uint32_t _newCount)
    {
        if (_newCount == 0)
        {
            freeSegment(_segment);
            _segment.count = 0;
            _segment.firstIndex = 0;
            return _segment;
        }

        if (_segment.count > _newCount)
        {
            uint32_t otherSize = _segment.count - _newCount;
            Segment otherSegment;
            otherSegment.count = otherSize;
            otherSegment.firstIndex = _segment.firstIndex + _newCount;
            freeSegment(otherSegment);

            _segment.count = _newCount;
            return _segment;
        }
        else
        {
            freeSegment(_segment);
            return allocateSegment(_newCount);
        }
    }

    void PeBufferChain::freeSegment(Segment _segment)
    {
        if (_segment.count == 0)
            return;

        SegmentNode *seg = begin;
        while (seg)
        {
            SegmentNode *current = seg;
            if (_segment.firstIndex >= current->index &&
                (_segment.firstIndex + _segment.count) <= (current->index + current->count) &&
                !current->free)

            {
                break;
            }
            seg = current->next;
        }
        if (!seg)
            return;

        if (_segment.count == seg->count && _segment.firstIndex == seg->index)
        {
            seg->free = true;
            tryCompound(seg);
        }
        else if (_segment.firstIndex == seg->index)
        {
            uint32_t otherSize = seg->count - _segment.count;

            SegmentNode *apartSegment = getFreeSegment(otherSize, seg->index + _segment.count, false);

            seg->free = true;
            seg->count = _segment.count;

            addSegmentAfter(seg, apartSegment);

            tryCompound(seg);
            tryCompound(apartSegment);
        }
        else if ((_segment.count + _segment.firstIndex) == (seg->index + seg->count))
        {
            uint32_t otherSize = seg->count - _segment.count;
            SegmentNode *apartSegment = getFreeSegment(_segment.count, seg->index + otherSize, true);

            seg->count = otherSize;

            addSegmentAfter(seg, apartSegment);

            tryCompound(seg);
            tryCompound(apartSegment);
        }
        else
        {
            uint32_t backSize = _segment.firstIndex - seg->index;
            uint32_t centerSize = _segment.count;
            uint32_t forwardSize = (seg->index + seg->count) - (_segment.count + _segment.firstIndex);

            SegmentNode *firstSegments = seg;
            firstSegments->count = backSize;

            SegmentNode *secondSegments = getFreeSegment(centerSize, firstSegments->index + backSize, true);
            SegmentNode *thirdSegments = getFreeSegment(forwardSize, secondSegments->index + centerSize, false);

            addSegmentAfter(firstSegments, secondSegments);
            addSegmentAfter(secondSegments, thirdSegments);
        }
    }

    bool PeBufferChain::tryFindFreeSegment(uint32_t _minCount, SegmentNode **_outEndSegment)
    {
        SegmentNode *next = begin;
        while (true)
        {
            if (next->free && next->count >= _minCount)
            {
                *_outEndSegment = next;
                return true;
            }

            if (next->next == NULL)
            {
                *_outEndSegment = next;
                return false;
            }
            next = next->next;
        }
        *_outEndSegment = begin;
        return false;
    }

    void PeBufferChain::tryCompound(SegmentNode *_segment)
    {
        if (_segment->back != NULL && _segment->back->free == _segment->free)
        {
            SegmentNode *back = _segment->back;
            back->count += _segment->count;
            back->next = _segment->next;
            if (back->next != NULL)
                back->next->back = back;

            returnSegment(_segment);

            _segment = back;
        }

        if (_segment->next != NULL && _segment->next->free == _segment->free)
        {
            SegmentNode *next = _segment->next;
            _segment->count += next->count;

            _segment->next = next->next;
            if (_segment->next != NULL)
                _segment->next->back = _segment;

            returnSegment(next);
        }
    }

    void PeBufferChain::addSegmentAfter(SegmentNode *_main, SegmentNode *_next)
    {
        _next->back = _main;
        _next->next = _main->next;

        if (_main->next != NULL)
            _main->next->back = _next;

        _main->next = _next;
    }

    PeBufferChain::SegmentNode *PeBufferChain::getFreeSegment(uint32_t _count, uint32_t _index, bool _free)
    {
        SegmentNode *segment;
        if (freeSegments == NULL)
        {
            segment = (SegmentNode *)phAllocateMemory(sizeof(SegmentNode));
        }
        else
        {
            segment = freeSegments;
            freeSegments = segment->next;
        }

        segment->back = NULL;
        segment->next = NULL;
        segment->count = _count;
        segment->index = _index;
        segment->free = _free;
        return segment;
    }

    void PeBufferChain::returnSegment(SegmentNode *_segment)
    {
        _segment->next = freeSegments;
        freeSegments = _segment;
    }

    void PeBufferChain::clearListSegments(SegmentNode *_begin)
    {
        SegmentNode *next = _begin;
        while (next)
        {
            SegmentNode *current = next;
            next = next->next;

            phDeallocateMemory(current);
        }
    }
}