#include "physicsEngine/peQueueCommandsBuilderSystem.h"

namespace phys
{
    PeQueueCommandsBuilderSystem::PeQueueCommandsBuilderSystem()
        : PeSystem(),
          gpu(NULL),
          staticLoaderSystem(NULL), staticReaderSystem(NULL),
          materialsDescriptionSystem(NULL),
          shapeDescriptionSystem(NULL),
          sceneActorsDescriptionSystem(NULL),
          geomteryObbDescriptionSystem(NULL),
          potentialContactsSystem(NULL),
          tableContactsSystem(NULL),
          contactsSystem(NULL),
          inertialTensorsSystem(NULL),
          physicsSimulationSystem(NULL)
    {
    }

    void PeQueueCommandsBuilderSystem::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        staticLoaderSystem = simulationContext->get<PeGpuStaticLoaderSystem>();
        staticReaderSystem = simulationContext->get<PeGpuStaticReaderSystem>();
        materialsDescriptionSystem = simulationContext->get<PeMaterialsDescriptionSystem>();
        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();
        sceneActorsDescriptionSystem = simulationContext->get<PeSceneActorsDescription>();
        geomteryObbDescriptionSystem = simulationContext->get<PeGeometryObbDescription>();
        potentialContactsSystem = simulationContext->get<PePotentialContactsSystem>();
        tableContactsSystem = simulationContext->get<PeTableContactsSystem>();
        contactsSystem = simulationContext->get<PeContactsSystem>();
        inertialTensorsSystem = simulationContext->get<PeInertialTensorsSystem>();
        physicsSimulationSystem = simulationContext->get<PePhysicsSimulationSystem>();
    }

    void PeQueueCommandsBuilderSystem::release()
    {
    }

    void PeQueueCommandsBuilderSystem::buildPrepareStage(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("Physics.PrepareStage", PeDebugMarkersUtils::mainSystemColor());

        staticLoaderSystem->resetBuffers();

        _simulationData->actorMangerCache.updateCache(&_simulationData->actorManager);

        materialsDescriptionSystem->prepareData(_simulationData);
        shapeDescriptionSystem->prepareData(_simulationData);
        sceneActorsDescriptionSystem->prepareData(_simulationData);

        geomteryObbDescriptionSystem->prepareData(_simulationData);

        // systems
        potentialContactsSystem->prepareData(_simulationData);
        tableContactsSystem->prepareData(_simulationData);
        contactsSystem->prepareData(_simulationData);
        inertialTensorsSystem->prepareData(_simulationData);
        physicsSimulationSystem->prepareData(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    void PeQueueCommandsBuilderSystem::buildSimulationStage(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("Physics.SimulationStage", PeDebugMarkersUtils::mainSystemColor());

        potentialContactsSystem->buildPipeline(_simulationData);
        tableContactsSystem->buildPipeline(_simulationData);
        contactsSystem->buildPipeline(_simulationData);
        inertialTensorsSystem->buildPipeline(_simulationData);
        physicsSimulationSystem->buildPipeline(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    void PeQueueCommandsBuilderSystem::buildReceiveDataStage(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("Physics.ReceiveDataStage", PeDebugMarkersUtils::mainSystemColor());

        potentialContactsSystem->buildReceive(_simulationData);
        tableContactsSystem->buildReceive(_simulationData);
        contactsSystem->buildReceive(_simulationData);
        inertialTensorsSystem->buildReceive(_simulationData);
        physicsSimulationSystem->buildReceive(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }
}