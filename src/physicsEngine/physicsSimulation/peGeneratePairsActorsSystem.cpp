#include "physicsEngine/physicsSimulation/peGeneratePairsActorsSystem.h"
#include "physicsEngine/peLimits.h"

namespace phys
{
    PeGeneratePairsActorsSystem::PeGeneratePairsActorsSystem()
        : PeSystem(),
          gpuData(NULL),
          staticReader(NULL),
          shaderProvider(NULL),
          memoryBarrierController(NULL),
          reallocateDatasSystem(NULL),
          generateSimulationDataSystem(NULL),
          countGraphs(0)
    {
    }

    void PeGeneratePairsActorsSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        sceneSettings = simulationContext->get<PeSceneSettingsSystem>();

        generateSimulationDataSystem = simulationContext->get<PeGenerateSimulationDataSystem>();

        buffers.lockedActorBuffer = PH_NEW(PeGpuBuffer)("LockedActorBuffer", gpuData, vk::MemoryUseType_Long,
                                                        vk::VulkanBufferType_UniformStorageBufferStatic,
                                                        sizeof(int32_t));

        buffers.usedEdgeBuffer = PH_NEW(PeGpuBuffer)("UsedEdgeBuffer", gpuData, vk::MemoryUseType_Long,
                                                     vk::VulkanBufferType_UniformStorageBufferStatic,
                                                     sizeof(int32_t));

        buffers.indexesRootEdgeBuffer = PH_NEW(PeGpuBuffer)("IndexesRootEdgeBuffer", gpuData, vk::MemoryUseType_Long,
                                                            vk::VulkanBufferType_UniformStorageBufferStatic,
                                                            sizeof(uint32_t));

        buffers.indexesRootEdgeCounter = PH_NEW(PeGpuCounterImageUint)("IndexesRootEdgeCounter", gpuData, 1);

        buffers.rootEdgesIDB = PH_NEW(PeGpuBuffer)("RootEdgesIDB", gpuData, vk::MemoryUseType_Long,
                                                   vk::VulkanBufferType_IndirectBufferStatic,
                                                   sizeof(PeGpuDataDispatchIndirect));

        const char *nameDefineRootEdges = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_DefineRootEdges);

        shaders.defineRootEdgesP = shaderProvider->getPipeline(nameDefineRootEdges);
        shaders.defineRootEdgesL = shaderProvider->getPipelineLayout(nameDefineRootEdges);
        shaders.defineRootEdgesS = shaderProvider->createShaderParameters(nameDefineRootEdges, 1);

        const char *nameSetupRootEdges = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_SetupRootEdges);

        shaders.setupRootEdgesP = shaderProvider->getPipeline(nameSetupRootEdges);
        shaders.setupRootEdgesL = shaderProvider->getPipelineLayout(nameSetupRootEdges);
        shaders.setupRootEdgesS = shaderProvider->createShaderParameters(nameSetupRootEdges, 1);

        const char *nameGenerateSplitedPairs = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateSplitedPairs);

        shaders.generateSplitedPairsP = shaderProvider->getPipeline(nameGenerateSplitedPairs);
        shaders.generateSplitedPairsL = shaderProvider->getPipelineLayout(nameGenerateSplitedPairs);
        shaders.generateSplitedPairsS = shaderProvider->createShaderParameters(nameGenerateSplitedPairs, 1);
    }

    void PeGeneratePairsActorsSystem::release()
    {
        shaderProvider->destroyShaderAutoParameters(shaders.defineRootEdgesS);
        shaderProvider->destroyShaderAutoParameters(shaders.setupRootEdgesS);
        shaderProvider->destroyShaderAutoParameters(shaders.generateSplitedPairsS);

        PH_RELEASE(buffers.lockedActorBuffer);
        PH_RELEASE(buffers.usedEdgeBuffer);
        PH_RELEASE(buffers.indexesRootEdgeBuffer);
        PH_RELEASE(buffers.indexesRootEdgeCounter);
        PH_RELEASE(buffers.rootEdgesIDB);
    }

    void PeGeneratePairsActorsSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        memoryBarrierController->initUnknowMemory(buffers.lockedActorBuffer);
        memoryBarrierController->initUnknowMemory(buffers.usedEdgeBuffer);
        memoryBarrierController->initUnknowMemory(buffers.indexesRootEdgeBuffer);
        memoryBarrierController->initUnknowMemory(buffers.indexesRootEdgeCounter);
        memoryBarrierController->initUnknowMemory(buffers.rootEdgesIDB);

        uint32_t maxIndexActor = _simulationData->actorManager.getMaxIndexObject();
        uint32_t maxCountContainers = _simulationData->tableContacts.contactsContainersBuffers[0]->getTypeDescription().getMaxCountObjects();

        {
            PeGpuBuffer *buffer = buffers.usedEdgeBuffer;
            reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(int32_t), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
            buffers.usedEdgeBuffer = buffer;
        }

        {
            PeGpuBuffer *buffer = buffers.lockedActorBuffer;
            reallocateDatasSystem->ensureBuffer(maxIndexActor * sizeof(int32_t), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
            buffers.lockedActorBuffer = buffer;
        }

        {
            PeGpuBuffer *buffer = buffers.indexesRootEdgeBuffer;
            reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(uint32_t), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
            buffers.indexesRootEdgeBuffer = buffer;
        }
    }

    void PeGeneratePairsActorsSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        PeGenerateSimulationDataSystem::SimulationData data = generateSimulationDataSystem->getSimulationData();

        memoryBarrierController->setBuffer(0, buffers.usedEdgeBuffer, PeTypeAccess_Write);
        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 1, 0);
        {
            uint32_t data = -1;
            gpuData->computeCommands->fillBuffer(*buffers.usedEdgeBuffer, 0, buffers.usedEdgeBuffer->getSize(), data);
        }

        countGraphs = sceneSettings->getSimulationSettings().countSubGraphs;
        countGraphs = std::max(1U, std::min(countGraphs, MAX_SPLITTED_CONTACT_GROUPS));
        const uint32_t countIterationOnGroup = 1;

        printf("Max containers on actor: %u\n", _simulationData->simulationData.maxContainersOnActor);

        bool lockParameters = false;
        for (uint32_t i = 0; i < countGraphs; i++)
        {
            memoryBarrierController->setBuffer(0, buffers.lockedActorBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 1, 0);
            {
                uint32_t data = -1;
                gpuData->computeCommands->fillBuffer(*buffers.lockedActorBuffer, 0, buffers.lockedActorBuffer->getSize(), data);
            }

            for (uint32_t i2 = 0; i2 < countIterationOnGroup; i2++)
            {
                memoryBarrierController->setImage(0, buffers.indexesRootEdgeCounter, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, 1);
                buffers.indexesRootEdgeCounter->clearCounter(gpuData, 0);

                // define root edges
                {
                    {
                        memoryBarrierController->setBuffer(0, data.containersData.countContainersBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(1, data.simActorsBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(2, data.graphEdgeBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(3, data.actorIndexesContainersBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(4, buffers.lockedActorBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(5, buffers.usedEdgeBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(6, buffers.indexesRootEdgeBuffer, PeTypeAccess_Write);
                        memoryBarrierController->setImage(0, buffers.indexesRootEdgeCounter, PeTypeAccess_Write);

                        memoryBarrierController->setupBarrier(PeTypeStage_Compute, 7, 1);

                        memoryBarrierController->setBuffer(0, data.containersData.containersGroupIDB, PeTypeAccess_IndirectCallRead);
                        memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
                    }

                    if (!lockParameters)
                    {
                        shaders.defineRootEdgesS->beginUpdate();
                        shaders.defineRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *data.containersData.countContainersBuffer);
                        shaders.defineRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *data.simActorsBuffer);
                        shaders.defineRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *data.graphEdgeBuffer);
                        shaders.defineRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *data.actorIndexesContainersBuffer);
                        shaders.defineRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *buffers.lockedActorBuffer);
                        shaders.defineRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *buffers.usedEdgeBuffer);
                        shaders.defineRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *buffers.indexesRootEdgeBuffer);
                        shaders.defineRootEdgesS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.indexesRootEdgeCounter);
                        shaders.defineRootEdgesS->endUpdate();
                    }

                    gpuData->computeCommands->bindPipeline(shaders.defineRootEdgesP);
                    gpuData->computeCommands->bindDescriptorSet(shaders.defineRootEdgesL,
                                                                shaders.defineRootEdgesS->getCurrentSet(0), 0);

                    gpuData->computeCommands->dispatchIndirect(*data.containersData.containersGroupIDB, 0);
                }

                {
                    memoryBarrierController->setBuffer(0, buffers.rootEdgesIDB, PeTypeAccess_Write);
                    memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 3, 0);
                    gpuData->computeCommands->fillBuffer(*buffers.rootEdgesIDB, 0, buffers.rootEdgesIDB->getSize(), 1);

                    memoryBarrierController->setBuffer(0, buffers.rootEdgesIDB, PeTypeAccess_Write);
                    memoryBarrierController->setImage(0, buffers.indexesRootEdgeCounter, PeTypeAccess_Read);
                    memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 1, 1);
                    gpuData->computeCommands->copyImageToBuffer(*buffers.indexesRootEdgeCounter, *buffers.rootEdgesIDB);

                    memoryBarrierController->setBuffer(0, buffers.rootEdgesIDB, PeTypeAccess_IndirectCallRead);
                    memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
                }

                // setup root edges
                {
                    {
                        memoryBarrierController->setBuffer(0, buffers.indexesRootEdgeBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(1, data.graphEdgeBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(2, buffers.lockedActorBuffer, PeTypeAccess_Write);
                        memoryBarrierController->setBuffer(3, buffers.usedEdgeBuffer, PeTypeAccess_Write);

                        memoryBarrierController->setupBarrier(PeTypeStage_Compute, 4, 0);
                    }

                    if (!lockParameters)
                    {
                        shaders.setupRootEdgesS->beginUpdate();
                        shaders.setupRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.indexesRootEdgeBuffer);
                        shaders.setupRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *data.graphEdgeBuffer);
                        shaders.setupRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.lockedActorBuffer);
                        shaders.setupRootEdgesS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *buffers.usedEdgeBuffer);
                        shaders.setupRootEdgesS->endUpdate();
                    }
                    struct
                    {
                        uint32_t indexCurrentStage;
                    } pConstants;
                    pConstants.indexCurrentStage = i;

                    gpuData->computeCommands->bindPipeline(shaders.setupRootEdgesP);
                    gpuData->computeCommands->bindDescriptorSet(shaders.setupRootEdgesL,
                                                                shaders.setupRootEdgesS->getCurrentSet(0), 0);
                    gpuData->computeCommands->pushConstant(shaders.setupRootEdgesL, 0,
                                                           sizeof(pConstants), &pConstants);
                    gpuData->computeCommands->dispatchIndirect(*buffers.rootEdgesIDB, 0);
                }

                // generate splited pairs
                {
                    {
                        memoryBarrierController->setBuffer(0, buffers.indexesRootEdgeBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(1, data.graphEdgeBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(2, data.simActorsBuffer, PeTypeAccess_Write);
                        memoryBarrierController->setBuffer(3, data.actorIndexesContainersBuffer, PeTypeAccess_Write);
                        memoryBarrierController->setBuffer(4, buffers.usedEdgeBuffer, PeTypeAccess_Write);
                        memoryBarrierController->setBuffer(5, buffers.lockedActorBuffer, PeTypeAccess_Write);

                        memoryBarrierController->setupBarrier(PeTypeStage_Compute, 6, 0);
                    }

                    if (!lockParameters)
                    {
                        shaders.generateSplitedPairsS->beginUpdate();
                        shaders.generateSplitedPairsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.indexesRootEdgeBuffer);
                        shaders.generateSplitedPairsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *data.graphEdgeBuffer);
                        shaders.generateSplitedPairsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *data.simActorsBuffer);
                        shaders.generateSplitedPairsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *data.actorIndexesContainersBuffer);
                        shaders.generateSplitedPairsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *buffers.usedEdgeBuffer);
                        shaders.generateSplitedPairsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *buffers.lockedActorBuffer);
                        shaders.generateSplitedPairsS->endUpdate();
                    }

                    struct
                    {
                        uint32_t indexCurrentStage;
                    } pConstants;
                    pConstants.indexCurrentStage = i;

                    gpuData->computeCommands->bindPipeline(shaders.generateSplitedPairsP);
                    gpuData->computeCommands->bindDescriptorSet(shaders.generateSplitedPairsL,
                                                                shaders.generateSplitedPairsS->getCurrentSet(0), 0);
                    gpuData->computeCommands->pushConstant(shaders.generateSplitedPairsL, 0,
                                                           sizeof(pConstants), &pConstants);
                    gpuData->computeCommands->dispatchIndirect(*buffers.rootEdgesIDB, 0);
                }

                lockParameters = true;
            }
        }
    }

    void PeGeneratePairsActorsSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
    }
}