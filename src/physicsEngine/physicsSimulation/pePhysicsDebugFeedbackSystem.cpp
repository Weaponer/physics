#include "physicsEngine/physicsSimulation/pePhysicsDebugFeedbackSystem.h"
#include "physicsEngine/peLimits.h"

namespace phys
{

    PePhysicsDebugFeedbackSystem::PePhysicsDebugFeedbackSystem()
        : PeSystem(),
          gpuData(NULL),
          staticReader(NULL),
          memoryBarrierController(NULL),
          reallocateDatasSystem(NULL),
          generatePairsActorsSystem(NULL),
          pReadOrientationActors(NULL),
          pReadVelsActors(NULL),
          pReadSleepingActors(NULL),
          pReadContainers(NULL),
          pReadGraphEdges(NULL),
          pReadContacts(NULL)
    {
    }

    void PePhysicsDebugFeedbackSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();

        generatePairsActorsSystem = simulationContext->get<PeGeneratePairsActorsSystem>();

        pReadOrientationActors = staticReader->createReadMemoryPoint(sizeof(PeGpuDataActorOrientation));
        pReadVelsActors = staticReader->createReadMemoryPoint(sizeof(PeGpuDataVelocityActor));
        pReadSleepingActors = staticReader->createReadMemoryPoint(sizeof(PeGpuDataSleepingActor));

        pReadContainers = staticReader->createReadMemoryPoint(sizeof(PeGpuDataContactsContainer));
        pReadGraphEdges = staticReader->createReadMemoryPoint(sizeof(int32_t));

        pReadContacts = staticReader->createReadMemoryPoint(sizeof(PeGpuDataContact));
    }

    void PePhysicsDebugFeedbackSystem::release()
    {
        staticReader->destroyReadMemoryPoint(pReadContacts);

        staticReader->destroyReadMemoryPoint(pReadContainers);
        staticReader->destroyReadMemoryPoint(pReadGraphEdges);

        staticReader->destroyReadMemoryPoint(pReadOrientationActors);
        staticReader->destroyReadMemoryPoint(pReadVelsActors);
        staticReader->destroyReadMemoryPoint(pReadSleepingActors);
    }

    void PePhysicsDebugFeedbackSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
    }

    void PePhysicsDebugFeedbackSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
    }

    void PePhysicsDebugFeedbackSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        if (!_simulationData->settingsFeedbackDebugData.enable)
            return;

        if (_simulationData->settingsFeedbackDebugData.actorsOrientation)
            buildReceiveActors(_simulationData);

        if (_simulationData->settingsFeedbackDebugData.containers)
            buildReceiveContainers(_simulationData, _simulationData->settingsFeedbackDebugData.contacts);
    }

    void PePhysicsDebugFeedbackSystem::buildReceiveActors(PeSceneSimulationData *_simulationData)
    {
        // orientation
        {
            if (pReadOrientationActors->IsValid())
            {
                uint32_t countOrientation = pReadOrientationActors->getSize() / sizeof(PeGpuDataActorOrientation);
                PeGpuDataActorOrientation *orientations = (PeGpuDataActorOrientation *)pReadOrientationActors->getMemory();
                for (uint32_t i = 0; i < countOrientation; i++)
                {
                    PeGpuDataActorOrientation orientation = orientations[i];
                    _simulationData->outputDebugData.addActorOrientation(orientation.rotation, orientation.position);
                }
            }

            uint32_t needSize = _simulationData->actorManager.getMaxIndexObject() * sizeof(PeGpuDataActorOrientation);
            if (pReadOrientationActors->getSize() < needSize)
            {
                staticReader->destroyReadMemoryPoint(pReadOrientationActors);
                pReadOrientationActors = staticReader->createReadMemoryPoint(needSize);
            }
            staticReader->addRequest(*_simulationData->actorOrientationsBuffer,
                                     0, pReadOrientationActors, 0, needSize);
        }

        // velocity
        {
            if (pReadVelsActors->IsValid() && pReadSleepingActors->IsValid())
            {
                uint32_t countVelocities = pReadVelsActors->getSize() / sizeof(PeGpuDataVelocityActor);
                PeGpuDataVelocityActor *velocities = (PeGpuDataVelocityActor *)pReadVelsActors->getMemory();
                PeGpuDataSleepingActor *sleeping = (PeGpuDataSleepingActor *)pReadSleepingActors->getMemory();
                for (uint32_t i = 0; i < countVelocities; i++)
                {
                    PeGpuDataVelocityActor velocity = velocities[i];
                    PeGpuDataSleepingActor sleep = sleeping[i];
                    _simulationData->outputDebugData.addActorVelocity(velocity.velocity, velocity.angularVelocity,
                                                                      sleep.isSleeping, sleep.sleepTime);
                }
            }

            uint32_t needSizeVels = _simulationData->actorVelocitiesBuffer->getSize();
            if (pReadVelsActors->getSize() < needSizeVels)
            {
                staticReader->destroyReadMemoryPoint(pReadVelsActors);
                pReadVelsActors = staticReader->createReadMemoryPoint(needSizeVels);
            }
            staticReader->addRequest(*_simulationData->actorVelocitiesBuffer,
                                     0, pReadVelsActors, 0, needSizeVels);

            uint32_t needSizeSleeping = _simulationData->actorSleepingBuffer->getSize();
            if (pReadSleepingActors->getSize() < needSizeSleeping)
            {
                staticReader->destroyReadMemoryPoint(pReadSleepingActors);
                pReadSleepingActors = staticReader->createReadMemoryPoint(needSizeSleeping);
            }
            staticReader->addRequest(*_simulationData->actorSleepingBuffer,
                                     0, pReadSleepingActors, 0, needSizeSleeping);
        }
    }

    void PePhysicsDebugFeedbackSystem::buildReceiveContainers(PeSceneSimulationData *_simulationData, bool _contacts)
    {
        {
            if (pReadContainers->IsValid() && pReadGraphEdges->IsValid() && (!_contacts || pReadContacts->IsValid()))
            {
                uint32_t countContainers = _simulationData->tableContacts.lastCountUsedContainers;
                PeGpuDataContactsContainer *containers = (PeGpuDataContactsContainer *)pReadContainers->getMemory();
                PeGpuDataContact *contacts = (PeGpuDataContact *)pReadContacts->getMemory();
                int32_t *graph = (int32_t *)pReadGraphEdges->getMemory();
                for (uint32_t i = 0; i < countContainers; i++)
                {
                    PeGpuDataContactsContainer container = containers[i];
                    InOutputDebugData::ContainerContacts resContainer;
                    resContainer.obbA = container.firstIndexGeometry;
                    resContainer.obbB = container.secondIndexGeometry;
                    resContainer.edgeIndex = graph[i];
                    _simulationData->outputDebugData.addContactsContainer(resContainer);

                    if (_contacts)
                    {
                        uint32_t countContacts = container.countAndMaskContacts & CONTACTS_CONTAINER_MASK_COUNT_CONTACTS;
                        uint32_t maskContacts = (container.countAndMaskContacts >> CONTACTS_CONTAINER_MASK_OFFSET_BITS_USE_CONTACTS) &
                                                CONTACTS_CONTAINER_MASK_USE_CONTACTS;

                        for (uint32_t i2 = 0; i2 < countContacts; i2++)
                        {
                            if ((maskContacts & (1 << i2)) == 0)
                                continue;

                            uint32_t indexContact = container.firstContactIndex + i2;

                            PeGpuDataContact contact = contacts[indexContact];
                            _simulationData->outputDebugData.addContact(contact.firstPointGlobal, contact.secondPointGlobal,
                                                                        contact.firstOriginGlobal, contact.secondOriginGlobal);
                        }
                    }
                }
            }

            uint32_t needSizeContainers = _simulationData->tableContacts.contactsContainersBuffers[0]->getSize();
            if (pReadContainers->getSize() < needSizeContainers)
            {
                staticReader->destroyReadMemoryPoint(pReadContainers);
                pReadContainers = staticReader->createReadMemoryPoint(needSizeContainers);
            }
            staticReader->addRequest(*_simulationData->tableContacts.contactsContainersBuffers[_simulationData->tableContacts.indexLastBuffer],
                                     0, pReadContainers, 0, needSizeContainers);

            PeGpuBuffer *edgeBuffer = generatePairsActorsSystem->getUsedEdgeBuffer();
            uint32_t needSizeGraphs = edgeBuffer->getSize();
            if (pReadGraphEdges->getSize() < needSizeGraphs)
            {
                staticReader->destroyReadMemoryPoint(pReadGraphEdges);
                pReadGraphEdges = staticReader->createReadMemoryPoint(needSizeGraphs);
            }
            staticReader->addRequest(*edgeBuffer,
                                     0, pReadGraphEdges, 0, needSizeGraphs);

            if (_contacts)
            {
                PeGpuObjectsBuffer *contacts = _simulationData->tableContacts.contactsBuffers[_simulationData->tableContacts.indexLastBuffer];

                uint32_t needSize = contacts->getSize();
                if (pReadContacts->getSize() < needSize)
                {
                    staticReader->destroyReadMemoryPoint(pReadContacts);
                    pReadContacts = staticReader->createReadMemoryPoint(needSize);
                }
                staticReader->addRequest(*contacts,
                                         0, pReadContacts, 0, needSize);
            }
        }
    }
}