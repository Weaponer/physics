#include "physicsEngine/physicsSimulation/peGenerateSimulationDataSystem.h"
#include "physicsEngine/common/peUtilities.h"

#include "physics/internal/inActor.h"

namespace phys
{
    PeGenerateSimulationDataSystem::PeGenerateSimulationDataSystem()
        : PeSystem(),
          gpuData(NULL),
          staticReader(NULL),
          shaderProvider(NULL),
          memoryBarrierController(NULL),
          reallocateDatasSystem(NULL),
          shapeDescriptionSystem(NULL),
          materialsDescriptionSystem(NULL),
          contactsSystem(NULL)
    {
    }

    void PeGenerateSimulationDataSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();

        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();
        materialsDescriptionSystem = simulationContext->get<PeMaterialsDescriptionSystem>();
        contactsSystem = simulationContext->get<PeContactsSystem>();

        buffers.actorsUpdateIndexesBuffer = PH_NEW(PeGpuBuffer)("ActorsUpdateIndexesBuffer", gpuData, vk::MemoryUseType_Long,
                                                                vk::VulkanBufferType_UniformStorageBufferDynamic,
                                                                sizeof(uint32_t));

        buffers.dynamicDataUpdateIndexesBuffer = PH_NEW(PeGpuBuffer)("DynamicDataUpdateIndexesBuffer", gpuData, vk::MemoryUseType_Long,
                                                                     vk::VulkanBufferType_UniformStorageBufferDynamic,
                                                                     sizeof(uint32_t));

        buffers.commonSimContainersBuffer = PH_NEW(PeGpuBuffer)("CommonSimContainersBuffer", gpuData, vk::MemoryUseType_Long,
                                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                sizeof(PeGpuCommonDataContactsContainer));

        buffers.commonSimContactModelsBuffer = PH_NEW(PeGpuBuffer)("CommonSimContactModelsBuffer", gpuData, vk::MemoryUseType_Long,
                                                                   vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                   sizeof(PeGpuCommonDataSimContact));

        buffers.simContainerNodesBuffer = PH_NEW(PeGpuBuffer)("SimContainerNodesBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_UniformStorageBufferStatic,
                                                              sizeof(PeGpuDataSimContainerNode));

        buffers.actorHeadContainersBuffer = PH_NEW(PeGpuBuffer)("ActorHeadContainersBuffer", gpuData, vk::MemoryUseType_Long,
                                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                sizeof(int32_t));

        buffers.simContainerNodesCounter = PH_NEW(PeGpuCounterImageUint)("SimContainerNodesCounter", gpuData, 1);

        buffers.containerLoopsDataBuffer = PH_NEW(PeGpuBuffer)("ContainerLoopsDataBuffer", gpuData, vk::MemoryUseType_Long,
                                                               vk::VulkanBufferType_UniformStorageBufferStatic,
                                                               sizeof(mth::vec2));
        buffers.actorIndexesContainersBuffer = PH_NEW(PeGpuBuffer)("ActorIndexesContainersBuffer", gpuData, vk::MemoryUseType_Long,
                                                                   vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                   sizeof(uint32_t));

        buffers.simActorsBuffer = PH_NEW(PeGpuBuffer)("SimActorsBuffer", gpuData, vk::MemoryUseType_Long,
                                                      vk::VulkanBufferType_UniformStorageBufferStatic,
                                                      sizeof(PeGpuDataSimActor));

        buffers.indexesContainersCounter = PH_NEW(PeGpuCounterImageUint)("IndexesContainersCounter", gpuData, 1);

        buffers.graphEdgeBuffer = PH_NEW(PeGpuBuffer)("GraphEdgeBuffer", gpuData, vk::MemoryUseType_Long,
                                                      vk::VulkanBufferType_UniformStorageBufferStatic,
                                                      sizeof(PeGpuDataActorGraphEdge_2));

        const char *nameInitContainersCommonData = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_InitContainersCommonData);

        shaders.initContainersCommonDataP = shaderProvider->getPipeline(nameInitContainersCommonData);
        shaders.initContainersCommonDataL = shaderProvider->getPipelineLayout(nameInitContainersCommonData);
        shaders.initContainersCommonDataS = shaderProvider->createShaderParameters(nameInitContainersCommonData, 1);

        const char *nameGenerateContainersList = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateContainersList);

        shaders.generateContainersListP = shaderProvider->getPipeline(nameGenerateContainersList);
        shaders.generateContainersListL = shaderProvider->getPipelineLayout(nameGenerateContainersList);
        shaders.generateContainersListS = shaderProvider->createShaderParameters(nameGenerateContainersList, 1);

        const char *nameGenerateActorsDataP = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateActorsData);

        shaders.generateActorsDataP = shaderProvider->getPipeline(nameGenerateActorsDataP);
        shaders.generateActorsDataL = shaderProvider->getPipelineLayout(nameGenerateActorsDataP);
        shaders.generateActorsDataS = shaderProvider->createShaderParameters(nameGenerateActorsDataP, 1);

        const char *nameGenerateEdgesDataP = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateEdgesData);

        shaders.generateEdgesDataP = shaderProvider->getPipeline(nameGenerateEdgesDataP);
        shaders.generateEdgesDataL = shaderProvider->getPipelineLayout(nameGenerateEdgesDataP);
        shaders.generateEdgesDataS = shaderProvider->createShaderParameters(nameGenerateEdgesDataP, 1);
    }

    void PeGenerateSimulationDataSystem::release()
    {
        shaderProvider->destroyShaderAutoParameters(shaders.initContainersCommonDataS);
        shaderProvider->destroyShaderAutoParameters(shaders.generateContainersListS);
        shaderProvider->destroyShaderAutoParameters(shaders.generateActorsDataS);
        shaderProvider->destroyShaderAutoParameters(shaders.generateEdgesDataS);

        PH_RELEASE(buffers.actorsUpdateIndexesBuffer);
        PH_RELEASE(buffers.dynamicDataUpdateIndexesBuffer);

        PH_RELEASE(buffers.commonSimContainersBuffer);
        PH_RELEASE(buffers.commonSimContactModelsBuffer);

        PH_RELEASE(buffers.simContainerNodesBuffer);
        PH_RELEASE(buffers.actorHeadContainersBuffer);
        PH_RELEASE(buffers.simContainerNodesCounter);

        PH_RELEASE(buffers.containerLoopsDataBuffer);
        PH_RELEASE(buffers.actorIndexesContainersBuffer);
        PH_RELEASE(buffers.simActorsBuffer);
        PH_RELEASE(buffers.indexesContainersCounter);

        PH_RELEASE(buffers.graphEdgeBuffer);
    }

    void PeGenerateSimulationDataSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        memoryBarrierController->initUnknowMemory(buffers.actorsUpdateIndexesBuffer);
        memoryBarrierController->initUnknowMemory(buffers.dynamicDataUpdateIndexesBuffer);

        memoryBarrierController->initUnknowMemory(buffers.commonSimContainersBuffer);
        memoryBarrierController->initUnknowMemory(buffers.commonSimContactModelsBuffer);

        memoryBarrierController->initUnknowMemory(buffers.simContainerNodesBuffer);
        memoryBarrierController->initUnknowMemory(buffers.actorHeadContainersBuffer);
        memoryBarrierController->initUnknowMemory(buffers.simContainerNodesCounter);

        memoryBarrierController->initUnknowMemory(buffers.containerLoopsDataBuffer);
        memoryBarrierController->initUnknowMemory(buffers.actorIndexesContainersBuffer);
        memoryBarrierController->initUnknowMemory(buffers.simActorsBuffer);
        memoryBarrierController->initUnknowMemory(buffers.indexesContainersCounter);

        memoryBarrierController->initUnknowMemory(buffers.graphEdgeBuffer);

        uint32_t maxIndexActor = _simulationData->actorManager.getMaxIndexObject();
        uint32_t maxCountContainers = _simulationData->tableContacts.contactsContainersBuffers[0]->getTypeDescription().getMaxCountObjects();
        uint32_t maxCountContacts = _simulationData->tableContacts.contactsBuffers[0]->getTypeDescription().getMaxCountObjects();

        if (_simulationData->simulationData.maxContainersOnActorCounter == NULL)
            _simulationData->simulationData.maxContainersOnActorCounter = PH_NEW(PeGpuCounterImageUint)("MaxContainersOnActor", gpuData, 1);

        {
            PeGpuBuffer *buffer = buffers.actorsUpdateIndexesBuffer;
            reallocateDatasSystem->ensureBuffer(maxIndexActor * sizeof(uint32_t), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferDynamic, buffer, &buffer);
            buffers.actorsUpdateIndexesBuffer = buffer;
        }

        {
            PeGpuBuffer *buffer = buffers.dynamicDataUpdateIndexesBuffer;
            reallocateDatasSystem->ensureBuffer(maxIndexActor * sizeof(uint32_t), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferDynamic, buffer, &buffer);
            buffers.dynamicDataUpdateIndexesBuffer = buffer;
        }

        //
        {
            reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(PeGpuCommonDataContactsContainer), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                buffers.commonSimContainersBuffer, &buffers.commonSimContainersBuffer);
        }

        {
            reallocateDatasSystem->ensureBuffer(maxCountContacts * sizeof(PeGpuCommonDataSimContact), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                buffers.commonSimContactModelsBuffer, &buffers.commonSimContactModelsBuffer);
        }
        //
        {
            reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(PeGpuDataSimContainerNode) * 2, vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                buffers.simContainerNodesBuffer, &buffers.simContainerNodesBuffer);
        }

        {
            reallocateDatasSystem->ensureBuffer(maxIndexActor * sizeof(int32_t), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                buffers.actorHeadContainersBuffer, &buffers.actorHeadContainersBuffer);
        }

        //
        {
            reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(mth::vec2) * 2, vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                buffers.containerLoopsDataBuffer, &buffers.containerLoopsDataBuffer);
        }
        {
            reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(uint32_t) * 2, vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                buffers.actorIndexesContainersBuffer, &buffers.actorIndexesContainersBuffer);
        }

        {
            reallocateDatasSystem->ensureBuffer(maxIndexActor * sizeof(PeGpuDataSimActor), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                buffers.simActorsBuffer, &buffers.simActorsBuffer);
        }

        //
        {
            reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(PeGpuDataActorGraphEdge_2), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic,
                                                buffers.graphEdgeBuffer, &buffers.graphEdgeBuffer);
        }

        { // clear data

            uint32_t data = -1;
            gpuData->computeCommands->fillBuffer(*buffers.actorHeadContainersBuffer, 0, buffers.actorHeadContainersBuffer->getSize(), data);

            /*data = 0;
            gpuData->computeCommands->fillBuffer(*buffers.simActorsBuffer, 0, buffers.simActorsBuffer->getSize(), data);*/

            buffers.simContainerNodesCounter->clearCounter(gpuData, 0, PeStageData_Unknow);
            buffers.indexesContainersCounter->clearCounter(gpuData, 0, PeStageData_Unknow);

            PeGpuCounterImageUint *maxContainers = _simulationData->simulationData.maxContainersOnActorCounter;
            maxContainers->receiveData(gpuData->computeCommands);

            _simulationData->simulationData.maxContainersOnActor = maxContainers->getData()[0];

            maxContainers->clearCounter(gpuData, 0, PeStageData_Unknow);
        }

        // fill indexes dynamic actors
        {
            countUpdateActors = 0;
            uint32_t *pWrite = (uint32_t *)gpuData->computeCommands->beginWrite(*buffers.actorsUpdateIndexesBuffer, 0,
                                                                                buffers.actorsUpdateIndexesBuffer->getSize());

            for (uint32_t i = 0; i < maxIndexActor; i++)
            {
                if (!_simulationData->actorManager.isValidId(i))
                    continue;

                bool isDynamic = _simulationData->actorManager.getObjectPerId(i)->isDynamic();
                if (isDynamic)
                {
                    pWrite[countUpdateActors] = i;
                    countUpdateActors++;
                }
            }

            gpuData->computeCommands->endWrite(*buffers.actorsUpdateIndexesBuffer);
        }

        // fill indexes dynamic data
        {
            countUpdateActors = 0;
            uint32_t *pWrite = (uint32_t *)gpuData->computeCommands->beginWrite(*buffers.dynamicDataUpdateIndexesBuffer, 0,
                                                                                buffers.dynamicDataUpdateIndexesBuffer->getSize());

            for (uint32_t i = 0; i < maxIndexActor; i++)
            {
                if (!_simulationData->actorManager.isValidId(i))
                    continue;

                bool isDynamic = _simulationData->actorManager.getObjectPerId(i)->isDynamic();
                if (isDynamic)
                {
                    uint32_t indexData = _simulationData->actorManager.getDataPerId(i)->indexAdditionalData;
                    pWrite[countUpdateActors] = indexData;
                    countUpdateActors++;
                }
            }

            gpuData->computeCommands->endWrite(*buffers.dynamicDataUpdateIndexesBuffer);
        }
    }

    void PeGenerateSimulationDataSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        PeContactsSystem::ResultContainersData containersData = contactsSystem->getResultContainersData();
        PeGpuObjectsBuffer *contactsContainerBuffer = _simulationData->tableContacts.contactsContainersBuffers[_simulationData->tableContacts.indexLastBuffer];
        PeGpuObjectsBuffer *contactsBuffer = _simulationData->tableContacts.contactsBuffers[_simulationData->tableContacts.indexLastBuffer];
        PeGpuObjectsBuffer *geometryObbsBuffer = _simulationData->geometryOBBs.geometryObbsBuffer;
        PeGpuObjectsBuffer *geometryObbsOrientationsBuffer = _simulationData->geometryOBBs.geometryOrientationObbsBuffer;

        // init sim containers data
        {
            {
                memoryBarrierController->setBuffer(0, contactsContainerBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, contactsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, geometryObbsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, geometryObbsOrientationsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(4, shapeDescriptionSystem->getShapeBuffer(), PeTypeAccess_Read);
                memoryBarrierController->setBuffer(5, materialsDescriptionSystem->getMaterialBuffer(), PeTypeAccess_Read);
                memoryBarrierController->setBuffer(6, _simulationData->actorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(7, _simulationData->actorOrientationsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(8, _simulationData->actorDynamicsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(9, _simulationData->actorVelocitiesBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(10, containersData.countContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(11, buffers.commonSimContainersBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(12, buffers.commonSimContactModelsBuffer, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 13, 0);

                memoryBarrierController->setBuffer(0, containersData.containersGroupIDB, PeTypeAccess_IndirectCallRead);
                memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
            }

            shaders.initContainersCommonDataS->beginUpdate();
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *contactsContainerBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *contactsBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *geometryObbsBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *geometryObbsOrientationsBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *shapeDescriptionSystem->getShapeBuffer());
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *materialsDescriptionSystem->getMaterialBuffer());
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *_simulationData->actorsBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 7, *_simulationData->actorOrientationsBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 8, *_simulationData->actorDynamicsBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 9, *_simulationData->actorVelocitiesBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 10, *containersData.countContainersBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 11, *buffers.commonSimContainersBuffer);
            shaders.initContainersCommonDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 12, *buffers.commonSimContactModelsBuffer);
            shaders.initContainersCommonDataS->endUpdate();

            gpuData->computeCommands->bindPipeline(shaders.initContainersCommonDataP);
            gpuData->computeCommands->bindDescriptorSet(shaders.initContainersCommonDataL,
                                                        shaders.initContainersCommonDataS->getCurrentSet(0), 0);

            gpuData->computeCommands->dispatchIndirect(*containersData.containersGroupIDB, 0);
        }

        // generate containers list 2
        {
            {
                memoryBarrierController->setBuffer(0, containersData.countContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, buffers.commonSimContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, buffers.simContainerNodesBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(3, buffers.actorHeadContainersBuffer, PeTypeAccess_Write);
                memoryBarrierController->setImage(0, buffers.simContainerNodesCounter, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 4, 1);

                memoryBarrierController->setBuffer(0, containersData.containersGroupIDB, PeTypeAccess_IndirectCallRead);
                memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
            }

            shaders.generateContainersListS->beginUpdate();
            shaders.generateContainersListS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *containersData.countContainersBuffer);
            shaders.generateContainersListS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.commonSimContainersBuffer);
            shaders.generateContainersListS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.simContainerNodesBuffer);
            shaders.generateContainersListS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *buffers.actorHeadContainersBuffer);
            shaders.generateContainersListS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.simContainerNodesCounter);
            shaders.generateContainersListS->endUpdate();

            gpuData->computeCommands->bindPipeline(shaders.generateContainersListP);
            gpuData->computeCommands->bindDescriptorSet(shaders.generateContainersListL,
                                                        shaders.generateContainersListS->getCurrentSet(0), 0);

            gpuData->computeCommands->dispatchIndirect(*containersData.containersGroupIDB, 0);
        }

        // generate actors data 2
        {
            memoryBarrierController->setBuffer(0, buffers.containerLoopsDataBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 1, 0);

            gpuData->computeCommands->fillBuffer(*buffers.containerLoopsDataBuffer, 0, buffers.containerLoopsDataBuffer->getSize(), -1);

            {
                memoryBarrierController->setBuffer(0, _simulationData->actorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, shapeDescriptionSystem->getShapeBuffer(), PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, _simulationData->actorSleepingBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, _simulationData->tableContacts.actorLostContainerBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(4, buffers.actorsUpdateIndexesBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(5, buffers.simContainerNodesBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(6, buffers.commonSimContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(7, buffers.actorHeadContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(8, buffers.actorIndexesContainersBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(9, buffers.containerLoopsDataBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(10, buffers.simActorsBuffer, PeTypeAccess_Write);

                memoryBarrierController->setImage(0, buffers.indexesContainersCounter, PeTypeAccess_Write);
                memoryBarrierController->setImage(1, _simulationData->simulationData.maxContainersOnActorCounter, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 11, 2);

                memoryBarrierController->setBuffer(0, containersData.containersGroupIDB, PeTypeAccess_IndirectCallRead);
                memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
            }

            shaders.generateActorsDataS->beginUpdate();
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *_simulationData->actorsBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *shapeDescriptionSystem->getShapeBuffer());
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->actorSleepingBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->tableContacts.actorLostContainerBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *buffers.actorsUpdateIndexesBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *buffers.simContainerNodesBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *buffers.commonSimContainersBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 7, *buffers.actorHeadContainersBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 8, *buffers.actorIndexesContainersBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 9, *buffers.containerLoopsDataBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 10, *buffers.simActorsBuffer);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.indexesContainersCounter);
            shaders.generateActorsDataS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *_simulationData->simulationData.maxContainersOnActorCounter);
            shaders.generateActorsDataS->endUpdate();

            struct
            {
                uint32_t maxCountUpdateActors;
            } pConstants;
            pConstants.maxCountUpdateActors = countUpdateActors;

            gpuData->computeCommands->bindPipeline(shaders.generateActorsDataP);
            gpuData->computeCommands->bindDescriptorSet(shaders.generateActorsDataL,
                                                        shaders.generateActorsDataS->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.generateActorsDataL, 0,
                                                   sizeof(pConstants), &pConstants);
            gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, countUpdateActors), 1, 1);
        }

        // generate edges data
        {
            {
                memoryBarrierController->setBuffer(0, buffers.commonSimContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, buffers.simActorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, buffers.graphEdgeBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(3, containersData.countContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 4, 0);

                memoryBarrierController->setBuffer(0, containersData.containersGroupIDB, PeTypeAccess_IndirectCallRead);
                memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
            }

            shaders.generateEdgesDataS->beginUpdate();
            shaders.generateEdgesDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.commonSimContainersBuffer);
            shaders.generateEdgesDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.simActorsBuffer);
            shaders.generateEdgesDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.graphEdgeBuffer);
            shaders.generateEdgesDataS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *containersData.countContainersBuffer);
            shaders.generateEdgesDataS->endUpdate();

            gpuData->computeCommands->bindPipeline(shaders.generateEdgesDataP);
            gpuData->computeCommands->bindDescriptorSet(shaders.generateEdgesDataL,
                                                        shaders.generateEdgesDataS->getCurrentSet(0), 0);

            gpuData->computeCommands->dispatchIndirect(*containersData.containersGroupIDB, 0);
        }
    }

    void PeGenerateSimulationDataSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
    }

    PeGenerateSimulationDataSystem::SimulationData PeGenerateSimulationDataSystem::getSimulationData()
    {
        PeGenerateSimulationDataSystem::SimulationData data{};
        data.simActorsBuffer = buffers.simActorsBuffer;
        data.commonSimContactModelsBuffer = buffers.commonSimContactModelsBuffer;
        data.commonSimContainersBuffer = buffers.commonSimContainersBuffer;

        data.graphEdgeBuffer = buffers.graphEdgeBuffer;
        data.containerLoopsDataBuffer = buffers.containerLoopsDataBuffer;
        data.actorIndexesContainersBuffer = buffers.actorIndexesContainersBuffer;

        data.actorsUpdateIndexesBuffer = buffers.actorsUpdateIndexesBuffer;
        data.dynamicDataUpdateIndexesBuffer = buffers.dynamicDataUpdateIndexesBuffer;

        data.containersData = contactsSystem->getResultContainersData();
        data.countUpdateActors = countUpdateActors;
        return data;
    }
}