#include "physicsEngine/physicsSimulation/pePhysicsSimulationSystemPBD.h"

#include "physicsEngine/peLimits.h"
#include "physicsEngine/common/peUtilities.h"

namespace phys
{
    PePhysicsSimulationSystemPBD::PePhysicsSimulationSystemPBD()
        : PeSystem(),
          gpuData(NULL),
          staticReader(NULL),
          shaderProvider(NULL),
          memoryBarrierController(NULL),
          reallocateDatasSystem(NULL),
          sceneSettings(NULL),
          generateSimulationDataSystem(NULL),
          generatePairsActorsSystem(NULL),
          lastVelocitiesWrite(0)
    {
    }

    void PePhysicsSimulationSystemPBD::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        sceneSettings = simulationContext->get<PeSceneSettingsSystem>();

        generateSimulationDataSystem = simulationContext->get<PeGenerateSimulationDataSystem>();
        generatePairsActorsSystem = simulationContext->get<PeGeneratePairsActorsSystem>();

        for (uint32_t i = 0; i < 2; i++)
        {
            buffers.containerVelocitiesBuffers[i] = PH_NEW(PeGpuBuffer)("ContainerVelocitiesBuffer", gpuData, vk::MemoryUseType_Long,
                                                                        vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                        sizeof(PeGpuDataAccamulatedContainerVelocity));
        }

        buffers.simContainersEPBDbuffer = PH_NEW(PeGpuBuffer)("SimContainersEPBDbuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_UniformStorageBufferStatic,
                                                              sizeof(PeGpuDataSimContactsContainerEPBD));

        buffers.updateDynamicDataBuffer = PH_NEW(PeGpuBuffer)("UpdateDynamicDataBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_UniformStorageBufferStatic,
                                                              sizeof(PeGpuDataDynamicActor));
        buffers.orientaionTempBuffer = PH_NEW(PeGpuBuffer)("OrientaionTempBuffer", gpuData, vk::MemoryUseType_Long,
                                                           vk::VulkanBufferType_UniformStorageBufferStatic,
                                                           sizeof(PeGpuDataActorOrientation));

        buffers.errorStopFlag = PH_NEW(PeGpuCounterImageUint)("ErrorStopFlag", gpuData, 1, false);

        const char *nameSimulateActorsEPBD_Init = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_SimulateActorsPBD_Init);

        shaders.simulateActorsPBD_initP = shaderProvider->getPipeline(nameSimulateActorsEPBD_Init);
        shaders.simulateActorsPBD_initL = shaderProvider->getPipelineLayout(nameSimulateActorsEPBD_Init);
        shaders.simulateActorsPBD_initS = shaderProvider->createShaderParameters(nameSimulateActorsEPBD_Init, 1);

        const char *nameSimulateActorsEPBD_Update = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_SimulateActorsPBD_Update);

        shaders.simulateActorsPBD_updateP = shaderProvider->getPipeline(nameSimulateActorsEPBD_Update);
        shaders.simulateActorsPBD_updateL = shaderProvider->getPipelineLayout(nameSimulateActorsEPBD_Update);
        shaders.simulateActorsPBD_updateS = shaderProvider->createShaderParameters(nameSimulateActorsEPBD_Update, 1);

        const char *nameFillSimCotainersDataEPBD = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_FillSimCotainersDataPBD);

        shaders.fillSimCotainersDataPBDP = shaderProvider->getPipeline(nameFillSimCotainersDataEPBD);
        shaders.fillSimCotainersDataPBDL = shaderProvider->getPipelineLayout(nameFillSimCotainersDataEPBD);
        shaders.fillSimCotainersDataPBDS = shaderProvider->createShaderParameters(nameFillSimCotainersDataEPBD, 1);

        const char *nameSolveContactsEPBD = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_SolveContactsPBD);

        shaders.solveContactsPBDP = shaderProvider->getPipeline(nameSolveContactsEPBD);
        shaders.solveContactsPBDL = shaderProvider->getPipelineLayout(nameSolveContactsEPBD);
        shaders.solveContactsPBDS = shaderProvider->createShaderParameters(nameSolveContactsEPBD, 2);
    }

    void PePhysicsSimulationSystemPBD::release()
    {
        shaderProvider->destroyShaderAutoParameters(shaders.simulateActorsPBD_initS);
        shaderProvider->destroyShaderAutoParameters(shaders.simulateActorsPBD_updateS);
        shaderProvider->destroyShaderAutoParameters(shaders.fillSimCotainersDataPBDS);
        shaderProvider->destroyShaderAutoParameters(shaders.solveContactsPBDS);

        for (uint32_t i = 0; i < 2; i++)
        {
            PH_RELEASE(buffers.containerVelocitiesBuffers[i]);
        }

        PH_RELEASE(buffers.simContainersEPBDbuffer);
        PH_RELEASE(buffers.updateDynamicDataBuffer);
        PH_RELEASE(buffers.orientaionTempBuffer);
        PH_RELEASE(buffers.errorStopFlag);
    }

    void PePhysicsSimulationSystemPBD::prepareData(PeSceneSimulationData *_simulationData)
    {
        uint32_t maxIndexActor = _simulationData->actorManager.getMaxIndexObject();
        uint32_t maxCountContainers = _simulationData->tableContacts.contactsContainersBuffers[0]->getTypeDescription().getMaxCountObjects();

        memoryBarrierController->initUnknowMemory(buffers.simContainersEPBDbuffer);
        memoryBarrierController->initUnknowMemory(buffers.updateDynamicDataBuffer);
        memoryBarrierController->initUnknowMemory(buffers.orientaionTempBuffer);
        memoryBarrierController->initUnknowMemory(buffers.errorStopFlag);

        for (uint32_t i = 0; i < 2; i++)
        {
            memoryBarrierController->initUnknowMemory(buffers.containerVelocitiesBuffers[i]);

            {
                PeGpuBuffer *buffer = buffers.containerVelocitiesBuffers[i];
                reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(PeGpuDataAccamulatedContainerVelocity), vk::MemoryUseType_Long,
                                                    vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
                buffers.containerVelocitiesBuffers[i] = buffer;
            }
        }

        {
            PeGpuBuffer *buffer = buffers.simContainersEPBDbuffer;
            reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(PeGpuDataSimContactsContainerEPBD), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
            buffers.simContainersEPBDbuffer = buffer;
        }

        {
            PeGpuBuffer *buffer = buffers.updateDynamicDataBuffer;
            reallocateDatasSystem->ensureBuffer(maxIndexActor * sizeof(PeGpuDataDynamicActor), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
            buffers.updateDynamicDataBuffer = buffer;
        }

        {
            PeGpuBuffer *buffer = buffers.orientaionTempBuffer;
            reallocateDatasSystem->ensureBuffer(_simulationData->actorOrientationsBuffer->getSize(), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
            buffers.orientaionTempBuffer = buffer;
        }
    }

    void PePhysicsSimulationSystemPBD::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        PeGenerateSimulationDataSystem::SimulationData data = generateSimulationDataSystem->getSimulationData();
        PeGpuBuffer *usedEdgeBuffer = generatePairsActorsSystem->getUsedEdgeBuffer();

        {
            memoryBarrierController->setBuffer(0, buffers.containerVelocitiesBuffers[0], PeTypeAccess_Write);
            memoryBarrierController->setBuffer(1, buffers.containerVelocitiesBuffers[1], PeTypeAccess_Write);
            memoryBarrierController->setBuffer(2, buffers.simContainersEPBDbuffer, PeTypeAccess_Write);
            memoryBarrierController->setBuffer(3, buffers.orientaionTempBuffer, PeTypeAccess_Write);
            memoryBarrierController->setBuffer(4, _simulationData->actorOrientationsBuffer, PeTypeAccess_Read);
            memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 5, 0);

            gpuData->computeCommands->fillBuffer(*buffers.simContainersEPBDbuffer, 0, buffers.simContainersEPBDbuffer->getSize(), 0);

            for (uint32_t i = 0; i < 2; i++)
                gpuData->computeCommands->fillBuffer(*buffers.containerVelocitiesBuffers[i], 0, buffers.containerVelocitiesBuffers[i]->getSize(), -1);

            gpuData->computeCommands->copyBuffer(*_simulationData->actorOrientationsBuffer, 0,
                                                 *buffers.orientaionTempBuffer, 0, _simulationData->actorOrientationsBuffer->getSize());
        }

        memoryBarrierController->setBuffer(0, data.containersData.containersGroupIDB, PeTypeAccess_IndirectCallRead);
        memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);

        ///
        const uint32_t maxIndexVelocities = buffers.containerVelocitiesBuffers[0]->getSize() / sizeof(PeGpuDataAccamulatedContainerVelocity);
        const uint32_t countSubGraphs = generatePairsActorsSystem->getCountGraphs();
        const uint32_t countIteration = sceneSettings->getSimulationSettings().countIterations;
        ///

        // acceleration Init PBD
        {
            {
                memoryBarrierController->setBuffer(0, _simulationData->sceneSettingsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, data.actorsUpdateIndexesBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, _simulationData->actorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, buffers.orientaionTempBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(4, _simulationData->actorDynamicsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(5, _simulationData->actorSleepingBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(6, _simulationData->actorVelocitiesBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(7, buffers.updateDynamicDataBuffer, PeTypeAccess_Write);
                memoryBarrierController->setImage(0, buffers.errorStopFlag, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 8, 1);
            }

            shaders.simulateActorsPBD_initS->beginUpdate();
            shaders.simulateActorsPBD_initS->setParameter<vk::ShaderParameterUniformBuffer>(0, 0, *_simulationData->sceneSettingsBuffer);
            shaders.simulateActorsPBD_initS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.errorStopFlag);
            shaders.simulateActorsPBD_initS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *data.actorsUpdateIndexesBuffer);
            shaders.simulateActorsPBD_initS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *_simulationData->actorsBuffer);
            shaders.simulateActorsPBD_initS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.orientaionTempBuffer);
            shaders.simulateActorsPBD_initS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->actorDynamicsBuffer);
            shaders.simulateActorsPBD_initS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *_simulationData->actorSleepingBuffer);
            shaders.simulateActorsPBD_initS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *_simulationData->actorVelocitiesBuffer);
            shaders.simulateActorsPBD_initS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *buffers.updateDynamicDataBuffer);
            shaders.simulateActorsPBD_initS->endUpdate();

            struct
            {
                uint32_t maxCountUpdateActors;
                uint32_t maxIndexVelocities;
                uint32_t countIterations;
                uint32_t isFinal;
            } pConstants;
            pConstants.maxCountUpdateActors = data.countUpdateActors;
            pConstants.maxIndexVelocities = maxIndexVelocities;
            pConstants.countIterations = countIteration;
            pConstants.isFinal = 0;

            gpuData->computeCommands->bindPipeline(shaders.simulateActorsPBD_initP);
            gpuData->computeCommands->bindDescriptorSet(shaders.simulateActorsPBD_initL,
                                                        shaders.simulateActorsPBD_initS->getCurrentSet(0), 0);

            gpuData->computeCommands->pushConstant(shaders.simulateActorsPBD_initL, 0,
                                                   sizeof(pConstants), &pConstants);
            gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, data.countUpdateActors), 1, 1);
        }

        // solve contacts

        VkDescriptorSet fillContainersData;
        VkDescriptorSet solveContainerSets[2];
        VkDescriptorSet simulateActorsUpdate;
        {
            shaders.fillSimCotainersDataPBDS->beginUpdate();
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *data.containersData.countContainersBuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.orientaionTempBuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->actorVelocitiesBuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->actorDynamicsBuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *data.commonSimContainersBuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *data.containerLoopsDataBuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *data.simActorsBuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 7, *usedEdgeBuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 8, *data.graphEdgeBuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 9, *buffers.simContainersEPBDbuffer);
            shaders.fillSimCotainersDataPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 10, *buffers.containerVelocitiesBuffers[0]);
            shaders.fillSimCotainersDataPBDS->endUpdate();
            fillContainersData = shaders.fillSimCotainersDataPBDS->getCurrentSet(0);
            //

            shaders.solveContactsPBDS->clearParameters();
            shaders.solveContactsPBDS->reset();

            for (uint32_t i = 0; i < 2; i++)
            {
                uint32_t inverseI = 1 - i;
                shaders.solveContactsPBDS->beginUpdate();
                shaders.solveContactsPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *data.containersData.countContainersBuffer);
                shaders.solveContactsPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.simContainersEPBDbuffer);
                shaders.solveContactsPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *data.commonSimContactModelsBuffer);
                shaders.solveContactsPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *buffers.containerVelocitiesBuffers[i]);
                shaders.solveContactsPBDS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *buffers.containerVelocitiesBuffers[inverseI]);
                shaders.solveContactsPBDS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.errorStopFlag);
                shaders.solveContactsPBDS->setParameter<vk::ShaderParameterUniformBuffer>(0, 0, *_simulationData->sceneSettingsBuffer);
                shaders.solveContactsPBDS->endUpdate();

                solveContainerSets[i] = shaders.solveContactsPBDS->getCurrentSet(0);
                if (i == 0)
                    shaders.solveContactsPBDS->switchToNextSet(0);
            }

            //
            shaders.simulateActorsPBD_updateS->beginUpdate();
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterUniformBuffer>(0, 0, *_simulationData->sceneSettingsBuffer);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.errorStopFlag);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *data.actorsUpdateIndexesBuffer);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *data.simActorsBuffer);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.containerVelocitiesBuffers[countSubGraphs % 2]);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->actorsBuffer);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *_simulationData->actorDynamicsBuffer);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *_simulationData->actorVelocitiesBuffer);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *buffers.orientaionTempBuffer);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageBuffer>(0, 7, *_simulationData->actorSleepingBuffer);
            shaders.simulateActorsPBD_updateS->setParameter<vk::ShaderParameterStorageBuffer>(0, 8, *buffers.updateDynamicDataBuffer);
            shaders.simulateActorsPBD_updateS->endUpdate();
            simulateActorsUpdate = shaders.simulateActorsPBD_updateS->getCurrentSet(0);

            printf("------------------------------------- %i - %i\n", countIteration, countSubGraphs);
            for (uint32_t i = 0; i < countIteration; i++)
            {
                uint32_t lastIndexWritten = 0;

                {
                    memoryBarrierController->setBuffer(0, data.containersData.countContainersBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(1, buffers.orientaionTempBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(2, _simulationData->actorVelocitiesBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(3, _simulationData->actorDynamicsBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(4, data.commonSimContainersBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(5, data.containerLoopsDataBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(6, data.simActorsBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(7, usedEdgeBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(8, data.graphEdgeBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(9, buffers.simContainersEPBDbuffer, PeTypeAccess_Write);
                    memoryBarrierController->setBuffer(10, buffers.containerVelocitiesBuffers[0], PeTypeAccess_Write);
                    memoryBarrierController->setupBarrier(PeTypeStage_Compute, 11, 0);

                    gpuData->computeCommands->bindPipeline(shaders.fillSimCotainersDataPBDP);
                    gpuData->computeCommands->bindDescriptorSet(shaders.fillSimCotainersDataPBDL,
                                                                fillContainersData, 0);
                    gpuData->computeCommands->dispatchIndirect(*data.containersData.containersGroupIDB, 0);
                }

                gpuData->computeCommands->bindPipeline(shaders.solveContactsPBDP);
                for (uint32_t i2 = 0; i2 < countSubGraphs; i2++)
                {
                    uint32_t nextIndexWritten = 1 - lastIndexWritten;

                    memoryBarrierController->setBuffer(0, data.containersData.countContainersBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(1, buffers.simContainersEPBDbuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(2, data.commonSimContactModelsBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(3, buffers.containerVelocitiesBuffers[lastIndexWritten], PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(4, buffers.containerVelocitiesBuffers[nextIndexWritten], PeTypeAccess_Write);
                    memoryBarrierController->setBuffer(5, _simulationData->sceneSettingsBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setImage(0, buffers.errorStopFlag, PeTypeAccess_Write);
                    memoryBarrierController->setupBarrier(PeTypeStage_Compute, 6, 1);

                    struct
                    {
                        uint32_t indexCurrentStage;
                        uint32_t maxIndexVelocities;
                        uint32_t countIterations;
                    } pConstants;
                    pConstants.indexCurrentStage = i2;
                    pConstants.maxIndexVelocities = maxIndexVelocities;
                    pConstants.countIterations = countIteration;

                    gpuData->computeCommands->bindDescriptorSet(shaders.solveContactsPBDL,
                                                                solveContainerSets[lastIndexWritten], 0);
                    gpuData->computeCommands->pushConstant(shaders.solveContactsPBDL, 0,
                                                           sizeof(pConstants), &pConstants);
                    gpuData->computeCommands->dispatchIndirect(*data.containersData.containersGroupIDB, 0);

                    lastIndexWritten = nextIndexWritten;
                }

                {
                    memoryBarrierController->setBuffer(0, _simulationData->sceneSettingsBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(1, data.actorsUpdateIndexesBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(2, data.simActorsBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(3, buffers.containerVelocitiesBuffers[countSubGraphs % 2], PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(4, _simulationData->actorsBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(5, _simulationData->actorDynamicsBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(6, _simulationData->actorVelocitiesBuffer, PeTypeAccess_Write);
                    memoryBarrierController->setBuffer(7, buffers.orientaionTempBuffer, PeTypeAccess_Write);
                    memoryBarrierController->setBuffer(8, _simulationData->actorSleepingBuffer, PeTypeAccess_Write);
                    memoryBarrierController->setBuffer(9, buffers.updateDynamicDataBuffer, PeTypeAccess_Write);
                    memoryBarrierController->setImage(0, buffers.errorStopFlag, PeTypeAccess_Write);
                    memoryBarrierController->setupBarrier(PeTypeStage_Compute, 10, 1);

                    struct
                    {
                        uint32_t maxCountUpdateActors;
                        uint32_t maxIndexVelocities;
                        uint32_t countIterations;
                        uint32_t isFinal;
                    } pConstants;
                    pConstants.maxCountUpdateActors = data.countUpdateActors;
                    pConstants.maxIndexVelocities = maxIndexVelocities;
                    pConstants.countIterations = countIteration;
                    pConstants.isFinal = (countIteration - 1 == i) ? 1 : 0;

                    gpuData->computeCommands->bindPipeline(shaders.simulateActorsPBD_updateP);
                    gpuData->computeCommands->bindDescriptorSet(shaders.simulateActorsPBD_updateL,
                                                                simulateActorsUpdate, 0);
                    gpuData->computeCommands->pushConstant(shaders.simulateActorsPBD_updateL, 0,
                                                           sizeof(pConstants), &pConstants);
                    gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, data.countUpdateActors), 1, 1);
                }
            }
        }

        {
            memoryBarrierController->setBuffer(0, buffers.orientaionTempBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(1, _simulationData->actorOrientationsBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 2, 0);
            gpuData->computeCommands->copyBuffer(*buffers.orientaionTempBuffer, 0,
                                                 *_simulationData->actorOrientationsBuffer, 0, _simulationData->actorOrientationsBuffer->getSize());
        }
    }

    void PePhysicsSimulationSystemPBD::buildReceive(PeSceneSimulationData *_simulationData)
    {
    }
}
