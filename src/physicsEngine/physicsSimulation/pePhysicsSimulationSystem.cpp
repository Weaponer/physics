#include "physicsEngine/physicsSimulation/pePhysicsSimulationSystem.h"

namespace phys
{
    PePhysicsSimulationSystem::PePhysicsSimulationSystem()
        : PeSystem(),
          physicsSimulationContext(),
          generateSimulationDataSystem(),
          generatePairsActorsSystem(),
          physicsSimulationSystemLCP(),
          physicsSimulationSystemPBD(),
          physicsDebugFeedbackSystem(),
          gpu(NULL),
          sceneSettings(NULL)
    {
    }

    void PePhysicsSimulationSystem::onInit()
    {
        physicsSimulationContext.setParent(simulationContext);
        physicsSimulationContext.bind(&generateSimulationDataSystem);
        physicsSimulationContext.bind(&generatePairsActorsSystem);
        physicsSimulationContext.bind(&physicsSimulationSystemLCP);
        physicsSimulationContext.bind(&physicsSimulationSystemPBD);
        physicsSimulationContext.bind(&physicsDebugFeedbackSystem);
        
        gpu = simulationContext->get<PeGpuData>();
        generateSimulationDataSystem.init(&physicsSimulationContext);
        generatePairsActorsSystem.init(&physicsSimulationContext);
        physicsSimulationSystemLCP.init(&physicsSimulationContext);
        physicsSimulationSystemPBD.init(&physicsSimulationContext);
        physicsDebugFeedbackSystem.init(&physicsSimulationContext);

        sceneSettings = simulationContext->get<PeSceneSettingsSystem>();
    }

    void PePhysicsSimulationSystem::release()
    {   
        physicsDebugFeedbackSystem.release();
        physicsSimulationSystemPBD.release();
        physicsSimulationSystemLCP.release();
        generateSimulationDataSystem.release();
        generatePairsActorsSystem.release();
        physicsSimulationContext.release();
    }

    void PePhysicsSimulationSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("NewPhysicsSimulationPrepareData", PeDebugMarkersUtils::subGroupColor());
        generateSimulationDataSystem.prepareData(_simulationData);
        generatePairsActorsSystem.prepareData(_simulationData);
        if(useSolverLCP())
            physicsSimulationSystemLCP.prepareData(_simulationData);
        else
            physicsSimulationSystemPBD.prepareData(_simulationData);
        physicsDebugFeedbackSystem.prepareData(_simulationData);
        gpu->computeCommands->debugMarkerEnd();
    }

    void PePhysicsSimulationSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("NewPhysicsSimulation", PeDebugMarkersUtils::subGroupColor());
        generateSimulationDataSystem.buildPipeline(_simulationData);
        generatePairsActorsSystem.buildPipeline(_simulationData);
        if(useSolverLCP())
            physicsSimulationSystemLCP.buildPipeline(_simulationData);
        else
            physicsSimulationSystemPBD.buildPipeline(_simulationData);
        physicsDebugFeedbackSystem.buildPipeline(_simulationData);
        gpu->computeCommands->debugMarkerEnd();
    }

    void PePhysicsSimulationSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("NewPhysicsSimulationReceive", PeDebugMarkersUtils::subGroupColor());
        generateSimulationDataSystem.buildReceive(_simulationData);
        generatePairsActorsSystem.buildReceive(_simulationData);
        if(useSolverLCP())
            physicsSimulationSystemLCP.buildReceive(_simulationData);
        else
            physicsSimulationSystemPBD.buildReceive(_simulationData);
        physicsDebugFeedbackSystem.buildReceive(_simulationData);
        gpu->computeCommands->debugMarkerEnd();
    }

    bool PePhysicsSimulationSystem::useSolverLCP()
    {
        return sceneSettings->getSimulationSettings().solveMode == PhSimulationSettings::SolverMode_LCP;
    }
}