#include "physicsEngine/physicsSimulation/pePhysicsSimulationSystemLCP.h"
#include "physicsEngine/peLimits.h"
#include "physicsEngine/common/peUtilities.h"

namespace phys
{
    PePhysicsSimulationSystemLCP::PePhysicsSimulationSystemLCP()
        : PeSystem(),
          gpuData(NULL),
          staticReader(NULL),
          shaderProvider(NULL),
          memoryBarrierController(NULL),
          reallocateDatasSystem(NULL),
          sceneSettings(NULL),
          generateSimulationDataSystem(NULL),
          generatePairsActorsSystem(NULL),
          error(false),
          lastVelocitiesWrite(0)
    {
    }

    void PePhysicsSimulationSystemLCP::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        sceneSettings = simulationContext->get<PeSceneSettingsSystem>();

        generateSimulationDataSystem = simulationContext->get<PeGenerateSimulationDataSystem>();
        generatePairsActorsSystem = simulationContext->get<PeGeneratePairsActorsSystem>();

        for (uint32_t i = 0; i < 2; i++)
        {
            buffers.containerVelocitiesBuffers[i] = PH_NEW(PeGpuBuffer)("ContainerVelocitiesBuffer", gpuData, vk::MemoryUseType_Long,
                                                                        vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                        sizeof(PeGpuDataAccamulatedContainerVelocity));
            buffers.contactImpulsesBuffers[i] = PH_NEW(PeGpuBuffer)("ContactImpulsesBuffer", gpuData, vk::MemoryUseType_Long,
                                                                    vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                    sizeof(mth::vec4));
        }

        buffers.simContainersFinishedBuffer = PH_NEW(PeGpuBuffer)("SimContainersFinishedBuffer", gpuData, vk::MemoryUseType_Long,
                                                                  vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                  sizeof(PeGpuDataSimContactsContainerFinished));

        buffers.simContactsModelsBuffer = PH_NEW(PeGpuBuffer)("SimContactsModelsBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_UniformStorageBufferStatic,
                                                              sizeof(PeGpuDataSimContactModel));

        buffers.updateDynamicDataBuffer = PH_NEW(PeGpuBuffer)("UpdateDynamicDataBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_UniformStorageBufferStatic,
                                                              sizeof(PeGpuDataDynamicActor));

        buffers.errorStopFlag = PH_NEW(PeGpuCounterImageUint)("ErrorStopFlag", gpuData, 1, false);

        const char *nameAccelerationInitLCP = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_AccelerationInitLCP);

        shaders.accelerationInitLCPP = shaderProvider->getPipeline(nameAccelerationInitLCP);
        shaders.accelerationInitLCPL = shaderProvider->getPipelineLayout(nameAccelerationInitLCP);
        shaders.accelerationInitLCPS = shaderProvider->createShaderParameters(nameAccelerationInitLCP, 1);

        const char *nameInitSimContainersDataLCP = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_InitSimContainersDataLCP);

        shaders.initSimContainersDataLCPP = shaderProvider->getPipeline(nameInitSimContainersDataLCP);
        shaders.initSimContainersDataLCPL = shaderProvider->getPipelineLayout(nameInitSimContainersDataLCP);
        shaders.initSimContainersDataLCPS = shaderProvider->createShaderParameters(nameInitSimContainersDataLCP, 1);

        const char *nameSolveContainerContacts = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_SolveContainerContacts);

        shaders.solveContainerContactsP = shaderProvider->getPipeline(nameSolveContainerContacts);
        shaders.solveContainerContactsL = shaderProvider->getPipelineLayout(nameSolveContainerContacts);
        shaders.solveContainerContactsS = shaderProvider->createShaderParameters(nameSolveContainerContacts, 2);

        const char *nameSimulateActorsLCP = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_SimulateActorsLCP);

        shaders.simulateActorsLCPP = shaderProvider->getPipeline(nameSimulateActorsLCP);
        shaders.simulateActorsLCPL = shaderProvider->getPipelineLayout(nameSimulateActorsLCP);
        shaders.simulateActorsLCPS = shaderProvider->createShaderParameters(nameSimulateActorsLCP, 1);
    }

    void PePhysicsSimulationSystemLCP::release()
    {
        shaderProvider->destroyShaderAutoParameters(shaders.accelerationInitLCPS);
        shaderProvider->destroyShaderAutoParameters(shaders.initSimContainersDataLCPS);
        shaderProvider->destroyShaderAutoParameters(shaders.solveContainerContactsS);
        shaderProvider->destroyShaderAutoParameters(shaders.simulateActorsLCPS);

        for (uint32_t i = 0; i < 2; i++)
        {
            PH_RELEASE(buffers.containerVelocitiesBuffers[i]);
            PH_RELEASE(buffers.contactImpulsesBuffers[i]);
        }

        PH_RELEASE(buffers.simContainersFinishedBuffer);
        PH_RELEASE(buffers.simContactsModelsBuffer);
        PH_RELEASE(buffers.updateDynamicDataBuffer);
        PH_RELEASE(buffers.errorStopFlag);
    }

    void PePhysicsSimulationSystemLCP::prepareData(PeSceneSimulationData *_simulationData)
    {
        uint32_t maxIndexActor = _simulationData->actorManager.getMaxIndexObject();
        uint32_t maxCountContainers = _simulationData->tableContacts.contactsContainersBuffers[0]->getTypeDescription().getMaxCountObjects();
        uint32_t maxCountContacts = _simulationData->tableContacts.contactsBuffers[0]->getTypeDescription().getMaxCountObjects();

        memoryBarrierController->initUnknowMemory(buffers.simContainersFinishedBuffer);
        memoryBarrierController->initUnknowMemory(buffers.simContactsModelsBuffer);
        memoryBarrierController->initUnknowMemory(buffers.updateDynamicDataBuffer);
        memoryBarrierController->initUnknowMemory(buffers.errorStopFlag);

        for (uint32_t i = 0; i < 2; i++)
        {
            memoryBarrierController->initUnknowMemory(buffers.containerVelocitiesBuffers[i]);
            memoryBarrierController->initUnknowMemory(buffers.contactImpulsesBuffers[i]);

            {
                PeGpuBuffer *buffer = buffers.containerVelocitiesBuffers[i];
                reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(PeGpuDataAccamulatedContainerVelocity), vk::MemoryUseType_Long,
                                                    vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
                buffers.containerVelocitiesBuffers[i] = buffer;
            }

            {
                PeGpuBuffer *buffer = buffers.contactImpulsesBuffers[i];
                reallocateDatasSystem->ensureBuffer(maxCountContacts * sizeof(mth::vec4), vk::MemoryUseType_Long,
                                                    vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
                buffers.contactImpulsesBuffers[i] = buffer;
            }
        }

        {
            PeGpuBuffer *buffer = buffers.simContainersFinishedBuffer;
            reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(PeGpuDataSimContactsContainerFinished), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
            buffers.simContainersFinishedBuffer = buffer;
        }

        {
            PeGpuBuffer *buffer = buffers.simContactsModelsBuffer;
            reallocateDatasSystem->ensureBuffer(maxCountContacts * sizeof(PeGpuDataSimContactModel), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
            buffers.simContactsModelsBuffer = buffer;
        }

        {
            PeGpuBuffer *buffer = buffers.updateDynamicDataBuffer;
            reallocateDatasSystem->ensureBuffer(maxIndexActor * sizeof(PeGpuDataDynamicActor), vk::MemoryUseType_Long,
                                                vk::VulkanBufferType_UniformStorageBufferStatic, buffer, &buffer);
            buffers.updateDynamicDataBuffer = buffer;
        }

        buffers.errorStopFlag->receiveData(gpuData->computeCommands);

        uint32_t erF = buffers.errorStopFlag->getData()[0];
        error = erF != 0;
        if (error)
            printf("Error!\n");
    }

    void PePhysicsSimulationSystemLCP::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        // if(error)
        //     return;
        PeGenerateSimulationDataSystem::SimulationData data = generateSimulationDataSystem->getSimulationData();
        PeGpuBuffer *usedEdgeBuffer = generatePairsActorsSystem->getUsedEdgeBuffer();

        {
            memoryBarrierController->setBuffer(0, buffers.containerVelocitiesBuffers[0], PeTypeAccess_Write);
            memoryBarrierController->setBuffer(1, buffers.containerVelocitiesBuffers[1], PeTypeAccess_Write);
            memoryBarrierController->setBuffer(2, buffers.contactImpulsesBuffers[0], PeTypeAccess_Write);
            memoryBarrierController->setBuffer(3, buffers.contactImpulsesBuffers[1], PeTypeAccess_Write);
            memoryBarrierController->setBuffer(4, buffers.simContainersFinishedBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 5, 0);

            gpuData->computeCommands->fillBuffer(*buffers.simContainersFinishedBuffer, 0, buffers.simContainersFinishedBuffer->getSize(), 0);

            for (uint32_t i = 0; i < 2; i++)
            {
                gpuData->computeCommands->fillBuffer(*buffers.containerVelocitiesBuffers[i], 0, buffers.containerVelocitiesBuffers[i]->getSize(), -1);
                gpuData->computeCommands->fillBuffer(*buffers.contactImpulsesBuffers[i], 0, buffers.contactImpulsesBuffers[i]->getSize(), 0);
            }
        }

        memoryBarrierController->setBuffer(0, data.containersData.containersGroupIDB, PeTypeAccess_IndirectCallRead);
        memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);

        // acceleration Init LCP
        {
            {
                memoryBarrierController->setBuffer(0, data.actorsUpdateIndexesBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, _simulationData->actorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, data.simActorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, _simulationData->actorOrientationsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(4, _simulationData->actorDynamicsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(5, _simulationData->actorVelocitiesBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(6, buffers.updateDynamicDataBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(7, _simulationData->sceneSettingsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 8, 0);
            }

            shaders.accelerationInitLCPS->beginUpdate();
            shaders.accelerationInitLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *data.actorsUpdateIndexesBuffer);
            shaders.accelerationInitLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *_simulationData->actorsBuffer);
            shaders.accelerationInitLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *data.simActorsBuffer);
            shaders.accelerationInitLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->actorOrientationsBuffer);
            shaders.accelerationInitLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *_simulationData->actorDynamicsBuffer);
            shaders.accelerationInitLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *_simulationData->actorVelocitiesBuffer);
            shaders.accelerationInitLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *buffers.updateDynamicDataBuffer);
            shaders.accelerationInitLCPS->setParameter<vk::ShaderParameterUniformBuffer>(0, 0, *_simulationData->sceneSettingsBuffer);
            shaders.accelerationInitLCPS->endUpdate();

            struct
            {
                uint32_t maxCountUpdateActors;
            } pConstants;
            pConstants.maxCountUpdateActors = data.countUpdateActors;

            gpuData->computeCommands->bindPipeline(shaders.accelerationInitLCPP);
            gpuData->computeCommands->bindDescriptorSet(shaders.accelerationInitLCPL,
                                                        shaders.accelerationInitLCPS->getCurrentSet(0), 0);

            gpuData->computeCommands->pushConstant(shaders.accelerationInitLCPL, 0,
                                                   sizeof(pConstants), &pConstants);
            gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, data.countUpdateActors), 1, 1);
        }

        // init Sim Containers Data LCP
        {
            {
                memoryBarrierController->setBuffer(0, data.containersData.countContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, _simulationData->actorOrientationsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, _simulationData->actorVelocitiesBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, buffers.updateDynamicDataBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(4, data.commonSimContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(5, data.commonSimContactModelsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(6, data.containerLoopsDataBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(7, data.simActorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(8, usedEdgeBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(9, data.graphEdgeBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(10, buffers.simContainersFinishedBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(11, buffers.simContactsModelsBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(12, buffers.containerVelocitiesBuffers[0], PeTypeAccess_Write);
                memoryBarrierController->setBuffer(13, _simulationData->sceneSettingsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 14, 0);
            }

            shaders.initSimContainersDataLCPS->beginUpdate();
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *data.containersData.countContainersBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *_simulationData->actorOrientationsBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->actorVelocitiesBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *buffers.updateDynamicDataBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *data.commonSimContainersBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *data.commonSimContactModelsBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *data.containerLoopsDataBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 7, *data.simActorsBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 8, *usedEdgeBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 9, *data.graphEdgeBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 10, *buffers.simContainersFinishedBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 11, *buffers.simContactsModelsBuffer);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 12, *buffers.containerVelocitiesBuffers[0]);
            shaders.initSimContainersDataLCPS->setParameter<vk::ShaderParameterUniformBuffer>(0, 0, *_simulationData->sceneSettingsBuffer);
            shaders.initSimContainersDataLCPS->endUpdate();

            gpuData->computeCommands->bindPipeline(shaders.initSimContainersDataLCPP);
            gpuData->computeCommands->bindDescriptorSet(shaders.initSimContainersDataLCPL,
                                                        shaders.initSimContainersDataLCPS->getCurrentSet(0), 0);
            gpuData->computeCommands->dispatchIndirect(*data.containersData.containersGroupIDB, 0);
        }

        // solve contacts

        VkDescriptorSet solveContainerSets[2];
        uint32_t lastIndexWritten = 0;
        {
            shaders.solveContainerContactsS->clearParameters();
            shaders.solveContainerContactsS->reset();

            for (uint32_t i = 0; i < 2; i++)
            {
                uint32_t inverseI = 1 - i;
                shaders.solveContainerContactsS->beginUpdate();
                shaders.solveContainerContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *data.containersData.countContainersBuffer);
                shaders.solveContainerContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.simContainersFinishedBuffer);
                shaders.solveContainerContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.simContactsModelsBuffer);
                shaders.solveContainerContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *buffers.containerVelocitiesBuffers[i]);
                shaders.solveContainerContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *buffers.containerVelocitiesBuffers[inverseI]);
                shaders.solveContainerContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *buffers.contactImpulsesBuffers[i]);
                shaders.solveContainerContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *buffers.contactImpulsesBuffers[inverseI]);
                shaders.solveContainerContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.errorStopFlag);
                shaders.solveContainerContactsS->endUpdate();

                solveContainerSets[i] = shaders.solveContainerContactsS->getCurrentSet(0);
                if (i == 0)
                    shaders.solveContainerContactsS->switchToNextSet(0);
            }

            gpuData->computeCommands->bindPipeline(shaders.solveContainerContactsP);
            const uint32_t countSubGraphs = generatePairsActorsSystem->getCountGraphs();
            const uint32_t countIteration = sceneSettings->getSimulationSettings().countIterations;
            printf("------------------------------------- %i - %i\n", countIteration, countSubGraphs);
            for (uint32_t i = 0; i < countIteration; i++)
            {
                for (uint32_t i2 = 0; i2 < countSubGraphs; i2++)
                {
                    uint32_t nextIndexWritten = 1 - lastIndexWritten;

                    memoryBarrierController->setBuffer(0, data.containersData.countContainersBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(1, buffers.simContainersFinishedBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(2, buffers.simContactsModelsBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(3, buffers.containerVelocitiesBuffers[lastIndexWritten], PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(4, buffers.containerVelocitiesBuffers[nextIndexWritten], PeTypeAccess_Write);
                    memoryBarrierController->setBuffer(5, buffers.contactImpulsesBuffers[lastIndexWritten], PeTypeAccess_Read);
                    memoryBarrierController->setBuffer(6, buffers.contactImpulsesBuffers[nextIndexWritten], PeTypeAccess_Write);
                    memoryBarrierController->setImage(0, buffers.errorStopFlag, PeTypeAccess_Write);
                    memoryBarrierController->setupBarrier(PeTypeStage_Compute, 7, 1);

                    struct
                    {
                        uint32_t indexCurrentStage;
                        int32_t maxIndexVelocities;
                    } pConstants;
                    pConstants.indexCurrentStage = i2;
                    pConstants.maxIndexVelocities = buffers.containerVelocitiesBuffers[0]->getSize() / sizeof(PeGpuDataAccamulatedContainerVelocity);

                    gpuData->computeCommands->bindDescriptorSet(shaders.solveContainerContactsL,
                                                                solveContainerSets[lastIndexWritten], 0);
                    gpuData->computeCommands->pushConstant(shaders.solveContainerContactsL, 0,
                                                           sizeof(pConstants), &pConstants);
                    gpuData->computeCommands->dispatchIndirect(*data.containersData.containersGroupIDB, 0);

                    lastIndexWritten = nextIndexWritten;
                }
            }
        }

        lastVelocitiesWrite = lastIndexWritten;

        // solve actors 2
        {
            {
                memoryBarrierController->setBuffer(0, data.actorsUpdateIndexesBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, data.simActorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, buffers.containerVelocitiesBuffers[lastIndexWritten], PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, _simulationData->actorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(4, buffers.updateDynamicDataBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(5, _simulationData->actorVelocitiesBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(6, _simulationData->actorOrientationsBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(7, _simulationData->actorSleepingBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(8, _simulationData->sceneSettingsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setImage(0, buffers.errorStopFlag, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 9, 1);
            }

            shaders.simulateActorsLCPS->beginUpdate();
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *data.actorsUpdateIndexesBuffer);
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *data.simActorsBuffer);
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.containerVelocitiesBuffers[lastIndexWritten]);
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->actorsBuffer);
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *buffers.updateDynamicDataBuffer);
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *_simulationData->actorVelocitiesBuffer);
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *_simulationData->actorOrientationsBuffer);
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterStorageBuffer>(0, 7, *_simulationData->actorSleepingBuffer);
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterUniformBuffer>(0, 0, *_simulationData->sceneSettingsBuffer);
            shaders.simulateActorsLCPS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.errorStopFlag);
            shaders.simulateActorsLCPS->endUpdate();

            struct
            {
                uint32_t maxCountUpdateActors;
                int32_t maxIndexVelocities;
            } pConstants;
            pConstants.maxCountUpdateActors = data.countUpdateActors;
            pConstants.maxIndexVelocities = buffers.containerVelocitiesBuffers[0]->getSize() / sizeof(PeGpuDataAccamulatedContainerVelocity);

            gpuData->computeCommands->bindPipeline(shaders.simulateActorsLCPP);
            gpuData->computeCommands->bindDescriptorSet(shaders.simulateActorsLCPL,
                                                        shaders.simulateActorsLCPS->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.simulateActorsLCPL, 0,
                                                   sizeof(pConstants), &pConstants);
            gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, data.countUpdateActors), 1, 1);
        }
    }

    void PePhysicsSimulationSystemLCP::buildReceive(PeSceneSimulationData *_simulationData)
    {
    }
}