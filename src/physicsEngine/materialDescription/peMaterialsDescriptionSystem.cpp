#include "physicsEngine/materialDescription/peMaterialsDescriptionSystem.h"

#include "physicsEngine/peGpuStructures.h"

namespace phys
{
    PeMaterialsDescriptionSystem::PeMaterialsDescriptionSystem()
        : PeSystem(),
          materialType(), materialsBuffer(),
          gpu(NULL),
          grabObjectsSystem(NULL),
          staticLoaderSystem(NULL),
          staticReaderSystem(NULL),
          materialManager(NULL),
          readMemory(NULL)
    {
    }

    void PeMaterialsDescriptionSystem::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        staticLoaderSystem = simulationContext->get<PeGpuStaticLoaderSystem>();
        staticReaderSystem = simulationContext->get<PeGpuStaticReaderSystem>();

        materialManager = simulationContext->get<InMaterialManager>();

        materialType = PeGpuObjectType::getObjectType<PeGpuDescriptionShot, PeGpuDataMaterial>(2);
        materialsBuffer = PH_NEW(PeGpuObjectsBuffer)("MaterialsBuffer", gpu, &materialType);

        readMemory = staticReaderSystem->createReadMemoryPoint(materialsBuffer->getSize());
    }

    void PeMaterialsDescriptionSystem::release()
    {
        staticReaderSystem->destroyReadMemoryPoint(readMemory);
        PH_RELEASE(materialsBuffer);
    }

    void PeMaterialsDescriptionSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("MaterialsDescriptionPrepareData", PeDebugMarkersUtils::subGroupColor());

        uint32_t currentCapacity = materialManager->getCapacityTable();
        if (currentCapacity > materialType.getMaxCountObjects())
        {
            uint32_t oldSize = materialsBuffer->getSize();
            materialType = PeGpuObjectType::getObjectType<PeGpuDescriptionShot, PeGpuDataMaterial>(currentCapacity);

            PeGpuObjectsBuffer *oldBuffer = materialsBuffer;

            materialsBuffer = PH_NEW(PeGpuObjectsBuffer)("MaterialsBuffer", gpu, &materialType);

            gpu->computeCommands->copyBuffer(*oldBuffer, 0, *materialsBuffer, 0, oldSize);

            grabObjectsSystem->addDataToUnload(oldBuffer);

            staticReaderSystem->destroyReadMemoryPoint(readMemory);
            readMemory = staticReaderSystem->createReadMemoryPoint(materialsBuffer->getSize());
        }

        printReadMemory();

        {
            // std::cout << "Materials" << std::endl;
            staticLoaderSystem->begin();

            InMaterialManager::ManagerEvent event;
            while (materialManager->getNextEvent(&event))
            {

                if (event.event != materialManager->ManagerEvent_Changed && event.event != materialManager->ManagerEvent_Created)
                {
                    // std::cout << event.index << ": type: " << event.event << std::endl;
                    continue;
                }
                InMaterial *material = materialManager->getObjectPerId(event.index);
                /*std::cout << event.index << ": type: " << event.event
                          << " bounciness: " << material->bounciness
                          << " staticFriction: " << material->staticFriction
                          << " dynamicFriction: " << material->dynamicFriction << std::endl;*/

                PeGpuDataMaterial dataMat;
                dataMat.bounciness = material->bounciness;
                dataMat.dynamicFriction = material->dynamicFriction;
                dataMat.staticFriction = material->staticFriction;

                staticLoaderSystem->sendData(&dataMat, sizeof(dataMat),
                                             *materialsBuffer, materialType.getAlignmentOffsetToIndex(event.index));
            }
            staticLoaderSystem->end();

            staticReaderSystem->addRequest(*materialsBuffer, 0, readMemory, 0, readMemory->getSize());
        }

        gpu->computeCommands->debugMarkerEnd();
    }

    uint32_t PeMaterialsDescriptionSystem::getLocalIndexMaterial(InMaterial *_material)
    {
        return _material->managerRegistration.getIndex() + materialType.getFirstDescription();
    }

    void PeMaterialsDescriptionSystem::printReadMemory()
    {
        if (readMemory->IsValid())
        {
            // uint8_t *data = (uint8_t *)readMemory->getMemory();
            uint32_t index = 0;
            for (uint32_t i = 0; i < readMemory->getSize(); i += materialType.getAlignmentSize())
            {
                // PeGpuDataMaterial *mat = (PeGpuDataMaterial *)(data + i);

                /*std::cout << "rd: " << index << " bounciness: " << mat->bounciness
                          << " staticFriction: " << mat->staticFriction
                          << " dynamicFriction: " << mat->dynamicFriction << std::endl;*/

                index++;
            }
        }
    }
}