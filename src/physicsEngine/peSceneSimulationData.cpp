#include "physicsEngine/peSceneSimulationData.h"

namespace phys
{
    PeSceneSimulationData::GeometryOBBsData::GeometryOBBsData()
        : geometryObbType(),
          geometryOrientationObbType(),
          geometryObbsBuffer(NULL),
          geometryOrientationObbsBuffer(NULL),
          actorAllocatedSegments(),
          segmentsChain(),
          obbsFlags()
    {
    }

    void PeSceneSimulationData::GeometryOBBsData::release()
    {
        obbsFlags.release();
        segmentsChain.release();
        actorAllocatedSegments.release();

        if (geometryObbsBuffer)
        {
            PH_RELEASE(geometryOrientationObbsBuffer);
            PH_RELEASE(geometryObbsBuffer);
        }
    }

    PeSceneSimulationData::TableContactsData::TableContactsData()
        : indexLastBuffer(0),
          lastMaxIndexOBB(0),
          lastCountUsedContainers(0),
          swapIndexesBuffers(),
          tableContactsOBBsBuffers(),
          tableContainerContactsBuffers(),
          actorLostContainerBuffer(),
          statisticUsingImage(NULL),
          contactsContainersBuffers(),
          contactsBuffers(),
          allocatedContactsContainersCounter(NULL),
          allocatedContactsCounter(NULL)
    {
    }

    void PeSceneSimulationData::TableContactsData::release()
    {
        if (statisticUsingImage)
        {
            PH_RELEASE(statisticUsingImage);

            for (uint32_t i = 0; i < 2; i++)
            {
                PH_RELEASE(tableContainerContactsBuffers[i]);
                PH_RELEASE(tableContactsOBBsBuffers[i]);
                PH_RELEASE(swapIndexesBuffers[i]);
            }
        }

        if (allocatedContactsContainersCounter)
        {
            for (uint32_t i = 0; i < 2; i++)
            {
                PH_RELEASE(contactsContainersBuffers[i]);
                PH_RELEASE(contactsBuffers[i]);
            }

            PH_RELEASE(actorLostContainerBuffer);
            PH_RELEASE(allocatedContactsContainersCounter);
            PH_RELEASE(allocatedContactsCounter);
        }
    }

    PeSceneSimulationData::SimulationData::SimulationData()
        : maxContainersOnActorCounter(),
          maxContainersOnActor(0)
    {
    }

    void PeSceneSimulationData::SimulationData::release()
    {
        if (maxContainersOnActorCounter)
            PH_RELEASE(maxContainersOnActorCounter);
    }

    PeSceneSimulationData::PeSceneSimulationData()
        : PhBase(),
          actorManager(),
          actorMangerCache(),
          tableDynamicDatas(),
          shapesChain(),
          outputDebugData(),
          settingsFeedbackDebugData(),
          sceneSettingsBuffer(NULL),
          actorType(),
          actorOrientationType(),
          actorDynamicType(),
          actorVelocityType(),
          actorSleepingType(),
          shapeListType(),
          actorsBuffer(NULL),
          actorOrientationsBuffer(NULL),
          actorDynamicsBuffer(NULL),
          actorVelocitiesBuffer(NULL),
          actorSleepingBuffer(NULL),
          shapeListBuffer(NULL),

          geometryOBBs(),

          tableContacts(),

          simulationData(),

          finishFence(),
          prepareDataStage(), completeSimulationStage(),
          firstSimulation(false)
    {
        tableDynamicDatas.setUseDefault(true);
        int32_t def = -1;
        tableDynamicDatas.setDefault(&def);
    }

    void PeSceneSimulationData::release()
    {
        vk::VulkanFence::destroyFence(&finishFence);
        prepareDataStage.release();
        completeSimulationStage.release();

        tableContacts.release();
        geometryOBBs.release();
        simulationData.release();

        if (actorsBuffer)
        {
            PH_RELEASE(actorsBuffer);
            PH_RELEASE(actorOrientationsBuffer);
        }

        if (shapeListBuffer)
            PH_RELEASE(shapeListBuffer);

        if (actorDynamicsBuffer)
        {
            PH_RELEASE(actorDynamicsBuffer);
            PH_RELEASE(actorVelocitiesBuffer);
            PH_RELEASE(actorSleepingBuffer);
        }

        if (sceneSettingsBuffer)
            PH_RELEASE(sceneSettingsBuffer);

        outputDebugData.release();
        shapesChain.release();
        tableDynamicDatas.release();
        actorMangerCache.release();
        actorManager.release();

        PhBase::release();
    }
}