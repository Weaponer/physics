#include "peGeometryObbDescription.h"

#include "physics/enums.h"
#include "physics/internal/inRigidBody.h"

#include "geometry/internal/inShape.h"
#include "geometry/internal/inGeometryBase.h"

namespace phys
{
    PeGeometryObbDescription::PeGeometryObbDescription()
        : PeSystem(),
          gpu(NULL),
          grabObjectsSystem(NULL),
          staticLoaderSystem(NULL),
          shapeManager(NULL),
          bufferRemoveGeometryOBBs(),
          bufferUpdateGeometryOBBs()

    {
    }

    void PeGeometryObbDescription::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        staticLoaderSystem = simulationContext->get<PeGpuStaticLoaderSystem>();
        shapeManager = simulationContext->get<InShapeManager>();
    }

    void PeGeometryObbDescription::release()
    {
        bufferRemoveGeometryOBBs.release();
        bufferUpdateGeometryOBBs.release();
    }

    void PeGeometryObbDescription::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("GeometryObbsDescriptionPrepareData", PeDebugMarkersUtils::subGroupColor());

        bufferRemoveGeometryOBBs.clearMemory();
        bufferUpdateGeometryOBBs.clearMemory();

        uint32_t countEvents = _simulationData->actorMangerCache.getCountEvents();
        InActorManager::ManagerEvent *events = _simulationData->actorMangerCache.getEvents();
        InActorManager *manager = _simulationData->actorMangerCache.getManager();

        { // ensure count actors
            uint32_t capacityTable = manager->getCapacityTable();
            if (capacityTable > (uint)_simulationData->geometryOBBs.actorAllocatedSegments.getCapacity())
                _simulationData->geometryOBBs.actorAllocatedSegments.setCapacity(capacityTable);
        }

        uint32_t countGeometryOBBs = 0;

        for (uint32_t i = 0; i < countEvents; i++)
        {
            InActorManager::ManagerEvent event = events[i];

            if (event.event == InActorManager::ManagerEvent_Destroyed)
            {
                PeGpuDataListGeometryOBBs segment = _simulationData->geometryOBBs.actorAllocatedSegments.getElement(event.index);
                if (!segment.count)
                    continue;

                PeBufferChain::Segment seg;
                seg.count = segment.count;
                seg.firstIndex = segment.firstIndex;
                _simulationData->geometryOBBs.segmentsChain.freeSegment(seg);

                bufferRemoveGeometryOBBs.assignmentMemoryAndCopy(&segment);
            }
            else if (event.event == InActorManager::ManagerEvent_Created)
            {
                InActor *actor = manager->getObjectPerId(event.index);

                uint32_t countGeometries = getCountGeometries(actor);
                PeBufferChain::Segment seg = _simulationData->geometryOBBs.segmentsChain.allocateSegment(countGeometries);
                PeGpuDataListGeometryOBBs segment;
                segment.count = seg.count;
                segment.firstIndex = seg.firstIndex;

                countGeometryOBBs = std::max(countGeometryOBBs, seg.count + seg.firstIndex);

                _simulationData->geometryOBBs.actorAllocatedSegments.setElement(segment, event.index);

                bufferUpdateGeometryOBBs.assignmentMemoryAndCopy(&event);
            }
            else if (event.event == InActorManager::ManagerEvent_Changed)
            {
                if ((event.flagChanges & (PhStateActorChangesFlags::ChangeShape | PhStateActorChangesFlags::ChangeKinematicFlag)) == 0)
                    continue;

                InActor *actor = manager->getObjectPerId(event.index);

                PeGpuDataListGeometryOBBs oldSegment = _simulationData->geometryOBBs.actorAllocatedSegments.getElement(event.index);
                uint32_t countGeometries = getCountGeometries(actor);

                if (oldSegment.count != countGeometries)
                {
                    PeBufferChain::Segment seg;
                    seg.count = oldSegment.count;
                    seg.firstIndex = oldSegment.firstIndex;

                    seg = _simulationData->geometryOBBs.segmentsChain.reallocateSegment(seg, countGeometries);

                    countGeometryOBBs = std::max(countGeometryOBBs, seg.count + seg.firstIndex);

                    PeGpuDataListGeometryOBBs segment;
                    segment.count = seg.count;
                    segment.firstIndex = seg.firstIndex;
                    _simulationData->geometryOBBs.actorAllocatedSegments.setElement(segment, event.index);
                }

                bufferUpdateGeometryOBBs.assignmentMemoryAndCopy(&event);
            }
        }

        ensureBuffers(countGeometryOBBs, &(_simulationData->geometryOBBs));

        removeGeometryOBBs(_simulationData);
        updateGeometryOBBs(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    uint32_t PeGeometryObbDescription::getCountGeometries(InActor *_actor)
    {
        uint32_t totalCount = 0;
        InRigidBody *rigidBody = dynamic_cast<InRigidBody *>(_actor);
        uint32_t countShapes = rigidBody->shapesListener.countShapes();
        for (uint32_t i = 0; i < countShapes; i++)
        {
            InShape *shape = rigidBody->shapesListener.getShape(i);
            totalCount += shape->countGeometries();
        }
        return totalCount;
    }

    PeGpuObjectType PeGeometryObbDescription::getGeometryObbType(uint32_t _capacity)
    {
        return PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataGeometryOBB>(_capacity, false, 16);
    }

    PeGpuObjectType PeGeometryObbDescription::getGeometryOrientationObbType(uint32_t _capacity)
    {
        return PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataOrientationGeometryOBB>(_capacity, false, 16);
    }

    PeGpuObjectsBuffer *PeGeometryObbDescription::createGeometryObbsBuffer(PeGpuObjectType _type)
    {
        PeGpuObjectsBuffer *buffer = PH_NEW(PeGpuObjectsBuffer)("GeometryObbsBuffer");
        buffer->initType(gpu, &_type);
        buffer->allocateData();
        return buffer;
    }

    PeGpuObjectsBuffer *PeGeometryObbDescription::createGeometryOrientationObbsBuffer(PeGpuObjectType _type)
    {
        PeGpuObjectsBuffer *buffer = PH_NEW(PeGpuObjectsBuffer)("GeometryOrientationObbsBuffer");
        buffer->initType(gpu, &_type);
        buffer->setShaderWriting(true);
        buffer->allocateData();
        return buffer;
    }

    void PeGeometryObbDescription::ensureBuffers(uint32_t _capacity, PeSceneSimulationData::GeometryOBBsData *_geometryOBBs)
    {
        if (_capacity == 0)
            _capacity = 1;
        if (_geometryOBBs->geometryObbType.getMaxCountObjects() < _capacity)
        {
            _geometryOBBs->geometryObbType = getGeometryObbType(_capacity);
            _geometryOBBs->geometryOrientationObbType = getGeometryOrientationObbType(_capacity);

            PeGpuObjectsBuffer *geometryObbsBuffer = createGeometryObbsBuffer(_geometryOBBs->geometryObbType);
            PeGpuObjectsBuffer *geometryOrientationObbsBuffer = createGeometryOrientationObbsBuffer(_geometryOBBs->geometryOrientationObbType);

            if (_geometryOBBs->geometryObbsBuffer != NULL)
            {
                gpu->computeCommands->copyBuffer(*_geometryOBBs->geometryObbsBuffer, 0, *geometryObbsBuffer, 0,
                                                 _geometryOBBs->geometryObbsBuffer->getSize());

                gpu->computeCommands->copyBuffer(*_geometryOBBs->geometryOrientationObbsBuffer, 0, *geometryOrientationObbsBuffer, 0,
                                                 _geometryOBBs->geometryOrientationObbsBuffer->getSize());

                grabObjectsSystem->addDataToUnload(_geometryOBBs->geometryObbsBuffer);
                grabObjectsSystem->addDataToUnload(_geometryOBBs->geometryOrientationObbsBuffer);
            }

            _geometryOBBs->geometryObbsBuffer = geometryObbsBuffer;
            _geometryOBBs->geometryOrientationObbsBuffer = geometryOrientationObbsBuffer;

            _geometryOBBs->obbsFlags.setCapacity(_capacity);
        }
    }

    void PeGeometryObbDescription::removeGeometryOBBs(PeSceneSimulationData *_simulationData)
    {
        staticLoaderSystem->begin();

        uint32_t alignmentSize = _simulationData->geometryOBBs.geometryObbType.getAlignmentSize();

        uint32_t countEvents = bufferRemoveGeometryOBBs.getPos();
        for (uint32_t i = 0; i < countEvents; i++)
        {
            PeGpuDataListGeometryOBBs list = *bufferRemoveGeometryOBBs.getMemory(i);

            if (list.count == 0)
                continue;

            uint32_t size = alignmentSize * list.count;
            uint8_t geometriesData[size];
            uint32_t zeroFlags[list.count];

            for(uint32_t i = 0; i < size; i++)
                geometriesData[i] = 0;
            for(uint32_t i = 0; i < list.count; i++)
                zeroFlags[i] = 0;
                
            staticLoaderSystem->sendData(geometriesData, size, *_simulationData->geometryOBBs.geometryObbsBuffer,
                                         _simulationData->geometryOBBs.geometryObbType.getAlignmentOffsetToIndex(list.firstIndex));
            std::memcpy(_simulationData->geometryOBBs.obbsFlags.getData() + list.firstIndex, zeroFlags, sizeof(zeroFlags));

        }

        staticLoaderSystem->end();
    }

    void PeGeometryObbDescription::updateGeometryOBBs(PeSceneSimulationData *_simulationData)
    {
        staticLoaderSystem->begin();

        uint32_t alignmentSize = _simulationData->geometryOBBs.geometryObbType.getAlignmentSize();

        InActorManager *manager = _simulationData->actorMangerCache.getManager();

        uint32_t countEvents = bufferUpdateGeometryOBBs.getPos();
        InActorManager::ManagerEvent event;
        for (uint32_t i = 0; i < countEvents; i++)
        {
            event = *bufferUpdateGeometryOBBs.getMemory(i);
            PeGpuDataListGeometryOBBs geometries = _simulationData->geometryOBBs.actorAllocatedSegments.getElement(event.index);

            if (geometries.count == 0)
                continue;

            uint8_t geometriesData[alignmentSize * geometries.count];
            uint32_t flagsGeometries[geometries.count];

            uint32_t indexCurrentGeometry = 0;

            InActor *actor = manager->getObjectPerId(event.index);
            InRigidBody *rigidBody = dynamic_cast<InRigidBody *>(actor);

            uint8_t additionalFlag = getGeometryAdditionalFlag(_simulationData, actor);

            uint32_t countShapes = rigidBody->shapesListener.countShapes();
            for (uint32_t i2 = 0; i2 < countShapes; i2++)
            {
                InShape *shape = rigidBody->shapesListener.getShape(i2);
                uint32_t shapeIndex = shape->managerRegistration.getIndex();
                PeGpuDataShape gpuDataShape = *shapeManager->getDataPerId(shapeIndex);

                for (uint32_t i3 = 0; i3 < gpuDataShape.countGeometries; i3++)
                {
                    InGeometryBase *geometry = dynamic_cast<InGeometryBase *>(shape->getGeometry(i3));

                    PeGpuDataGeometryOBB geometryObb;
                    geometryObb.indexActor = event.index;
                    geometryObb.indexShape = shapeIndex;
                    geometryObb.indexMainCollider = gpuDataShape.indexFirstGeometry + i3;
                    geometryObb.flags = geometry->getGpuGeometryFlags() | additionalFlag;

                    std::memcpy(geometriesData + alignmentSize * indexCurrentGeometry, &geometryObb, sizeof(geometryObb));
                    flagsGeometries[indexCurrentGeometry] = geometryObb.flags;

                    indexCurrentGeometry++;
                }
            }

            printf("update geometry obbs : count - %i  first index - %i\n", geometries.count, geometries.firstIndex);
            staticLoaderSystem->sendData(geometriesData, sizeof(geometriesData), *_simulationData->geometryOBBs.geometryObbsBuffer,
                                         _simulationData->geometryOBBs.geometryObbType.getAlignmentOffsetToIndex(geometries.firstIndex));
        
            std::memcpy(_simulationData->geometryOBBs.obbsFlags.getData() + geometries.firstIndex, flagsGeometries, sizeof(flagsGeometries));
        }

        staticLoaderSystem->end();
    }

    uint8_t PeGeometryObbDescription::getGeometryAdditionalFlag(PeSceneSimulationData *_simulationData, InActor *_actor)
    {
        PeGpuDataActor data = *_simulationData->actorManager.getDataPerId(_actor->managerRegistration.getIndex());
        if ((data.flags & PeGpuActorFlags::PeGpuActor_Dynamic))
        {
            return PeGpuGeometryFlags::PeGpuGeometry_Dynamic;
        }
        else if ((data.flags & PeGpuActorFlags::PeGpuActor_Kinematic))
        {
            return PeGpuGeometryFlags::PeGpuGeometry_Kinematic;
        }
        else
        {
            return PeGpuGeometryFlags::PeGpuGeometry_Static;
        }
    }
}