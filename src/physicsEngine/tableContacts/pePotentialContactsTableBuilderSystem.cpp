#include "physicsEngine/tableContacts/pePotentialContactsTableBuilderSystem.h"
#include "physicsEngine/tableContacts/peTableContactsStructs.h"

#include "physicsEngine/common/peUtilities.h"

#include "physicsEngine/common/peGpuBarrier.h"

#include "physicsEngine/peLimits.h"

namespace phys
{
    PePotentialContactsTableBuilderSystem::PePotentialContactsTableBuilderSystem()
        : PeSystem()
    {
    }

    void PePotentialContactsTableBuilderSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        contactsConainerMemorySystem = simulationContext->get<PeContactsContainerMemorySystem>();

        const char *nameGenerateLastListContacts = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateLastListContacts);
        const char *nameGenerateListContacts = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateListContacts);
        const char *nameGenerateListContactsPlane = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateListContactsPlane);
        const char *nameFillTalbleContacts = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_FillTableContacts);

        shaders.generateLastListContactsP = shaderProvider->getPipeline(nameGenerateLastListContacts);
        shaders.generateLastListContactsL = shaderProvider->getPipelineLayout(nameGenerateLastListContacts);
        shaders.generateLastListContactsS = shaderProvider->createShaderParameters(nameGenerateLastListContacts, 1);

        shaders.generateListContactsP = shaderProvider->getPipeline(nameGenerateListContacts);
        shaders.generateListContactsL = shaderProvider->getPipelineLayout(nameGenerateListContacts);
        shaders.generateListContactsS = shaderProvider->createShaderParameters(nameGenerateListContacts, 1);

        shaders.generateListContactsPlaneP = shaderProvider->getPipeline(nameGenerateListContactsPlane);
        shaders.generateListContactsPlaneL = shaderProvider->getPipelineLayout(nameGenerateListContactsPlane);
        shaders.generateListContactsPlaneS = shaderProvider->createShaderParameters(nameGenerateListContactsPlane, 1);

        shaders.fillTalbleContactsP = shaderProvider->getPipeline(nameFillTalbleContacts);
        shaders.fillTalbleContactsL = shaderProvider->getPipelineLayout(nameFillTalbleContacts);
        shaders.fillTalbleContactsS = shaderProvider->createShaderParameters(nameFillTalbleContacts, 1);

        buffers.nodesContactsBuffer = PH_NEW(PeGpuBuffer)("NodesContactsBuffer", gpuData, vk::MemoryUseType_Long,
                                                          vk::VulkanBufferType_UniformStorageBufferStatic, sizeof(PeGpuDataContactNode) * 256);

        buffers.nodesContactsCounter = PH_NEW(PeGpuCounterImageUint)("NodesContactsCounter", gpuData, 1, false);

        buffers.pointerHeadsContactsImage = PH_NEW(PeGpuImage)("PointerHeadsContactsImage");
        buffers.pointerHeadsContactsImage->init(gpuData);
        buffers.pointerHeadsContactsImage->allocateData(VK_IMAGE_TYPE_2D, VK_FORMAT_R32_SINT, 1, 1, 1, false);

        buffers.firstLaunch = true;

        countUpdateSwapTable = 0;
        startNewIndexes = 0;
        countNewIndexes = 0;
    }

    void PePotentialContactsTableBuilderSystem::release()
    {
        shaderProvider->destroyShaderAutoParameters(shaders.fillTalbleContactsS);
        shaderProvider->destroyShaderAutoParameters(shaders.generateListContactsS);
        shaderProvider->destroyShaderAutoParameters(shaders.generateLastListContactsS);
        shaderProvider->destroyShaderAutoParameters(shaders.generateListContactsPlaneS);

        PH_RELEASE(buffers.pointerHeadsContactsImage);
        PH_RELEASE(buffers.nodesContactsCounter);
        PH_RELEASE(buffers.nodesContactsBuffer);
    }

    void PePotentialContactsTableBuilderSystem::prepareData(PeSceneSimulationData *_simulationData,
                                                            PePotentialContactsSystem::ResultStage _previosStage,
                                                            PeTableContactsUpdateData _updateData)
    {
        memoryBarrierController->initUnknowMemory(buffers.pointerHeadsContactsImage);
        memoryBarrierController->initUnknowMemory(buffers.nodesContactsCounter);
        memoryBarrierController->initUnknowMemory(buffers.nodesContactsBuffer);

        gpuData->computeCommands->debugMarkerBegin("PotentialContactsTablePrepareData", PeDebugMarkersUtils::passCallColor());

        PeSceneSimulationData::TableContactsData &tableContacts = _simulationData->tableContacts;

        bool firstLaunch = false;
        if (!tableContacts.statisticUsingImage)
        {
            firstLaunch = true;

            uint32_t countIndexes = std::max(_previosStage.maxIndexOBB + 1, 256U);
            uint sizeSide = getSizeSideImage(countIndexes);

            tableContacts.statisticUsingImage = PH_NEW(PeGpuImage)("StatisticUsingImage");
            tableContacts.statisticUsingImage->init(gpuData);
            tableContacts.statisticUsingImage->allocateData(VK_IMAGE_TYPE_2D, VK_FORMAT_R32_UINT, sizeSide, sizeSide, 2);

            for (uint32_t i = 0; i < 2; i++)
            {
                tableContacts.swapIndexesBuffers[i] = createSwapIndexesBuffer(countIndexes);

                tableContacts.tableContactsOBBsBuffers[i] = createTableContacts(countIndexes);
                tableContacts.tableContainerContactsBuffers[i] = createTableContacts(countIndexes);
            }
        }

        uint32_t needCountIndexes = _previosStage.maxIndexOBB + 1;
        uint32_t currentCountIndex = tableContacts.tableContactsOBBsBuffers[0]->getCountIndexes();

        startNewIndexes = 0;
        countNewIndexes = 0;

        if (firstLaunch)
        {
            countNewIndexes = currentCountIndex;
        }
        else
        {
            if (needCountIndexes > currentCountIndex)
            {
                startNewIndexes = currentCountIndex;
                startNewIndexes = needCountIndexes - currentCountIndex;
            }
        }

        // resize statistic of using image
        {
            uint sizeSide = getSizeSideImage(needCountIndexes);

            reallocateDatasSystem->ensureImage(sizeSide, sizeSide, tableContacts.statisticUsingImage, &tableContacts.statisticUsingImage,
                                               PeReallocateDatasSystem::ReserveMemoryType_100p);
        }

        // resize swap indexes buffers
        {
            uint32_t needSize = needCountIndexes;
            for (uint32_t i = 0; i < 2; i++)
            {
                uint32_t defaultValue = 0;
                reallocateDatasSystem->ensureObjectsBufferWithCopyAndGrap(needSize,
                                                                          tableContacts.swapIndexesBuffers[i],
                                                                          &tableContacts.swapIndexesBuffers[i],
                                                                          PeReallocateDatasSystem::ReserveMemoryType_100p,
                                                                          &defaultValue);
            }
        }

        // resize and clear tables contacts
        {
            for (uint32_t i = 0; i < 2; i++)
            {
                VkClearColorValue value{};
                value.int32[0] = value.int32[1] = value.int32[2] = value.int32[3] = -1;

                reallocateDatasSystem->ensureTableContactsBufferWithCopyAndGrap(needCountIndexes,
                                                                                tableContacts.tableContactsOBBsBuffers[i],
                                                                                &tableContacts.tableContactsOBBsBuffers[i],
                                                                                PeReallocateDatasSystem::ReserveMemoryType_100p,
                                                                                &value);
                reallocateDatasSystem->ensureTableContactsBufferWithCopyAndGrap(needCountIndexes,
                                                                                tableContacts.tableContainerContactsBuffers[i],
                                                                                &tableContacts.tableContainerContactsBuffers[i],
                                                                                PeReallocateDatasSystem::ReserveMemoryType_100p,
                                                                                &value);
                if (i == _updateData.nextIndexTable || firstLaunch)
                {
                    const uint32_t countImgs = 2;
                    PeGpuMemoryImage *imgs[countImgs] = {tableContacts.tableContactsOBBsBuffers[i], tableContacts.tableContainerContactsBuffers[i]};
                    PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Write, PeTypeAccess_Write};
                    memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, countImgs, imgs, imgsA);

                    VkClearColorValue value;
                    value.int32[0] = value.int32[1] = value.int32[2] = value.int32[3] = -1;
                    gpuData->computeCommands->clearImage(*tableContacts.tableContactsOBBsBuffers[i], &value);
                    gpuData->computeCommands->clearImage(*tableContacts.tableContainerContactsBuffers[i], &value);
                }
            }
        }

        // prepare internal memory
        ensureMemory(_simulationData, _previosStage);

        // clear memory
        {
            const uint32_t countImgs = 2;
            PeGpuMemoryImage *imgs[countImgs] = {buffers.nodesContactsCounter, buffers.pointerHeadsContactsImage};
            PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Write, PeTypeAccess_Write};
            memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, countImgs, imgs, imgsA);

            VkClearColorValue value{};
            gpuData->computeCommands->clearImage(*imgs[0], &value);

            value.int32[0] = value.int32[1] = value.int32[2] = value.int32[3] = -1;
            gpuData->computeCommands->clearImage(*imgs[1], &value);
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PePotentialContactsTableBuilderSystem::buildPipeline(PeSceneSimulationData *_simulationData,
                                                              PePotentialContactsSystem::ResultStage _previosStage,
                                                              PeTableContactsUpdateData _updateData)
    {
        gpuData->computeCommands->debugMarkerBegin("PotentialContactsTableSimulation", PeDebugMarkersUtils::passCallColor());

        dispatchGenerateListContacts(_simulationData, _previosStage, _updateData);
        dispatchGenerateLastContacts(_simulationData, contactsConainerMemorySystem->getLastStatistic(), _updateData);
        dispatchGenerateListContactsPlane(_simulationData, _previosStage, _updateData);
        dispatchFillTalbleContacts(_simulationData, _updateData);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PePotentialContactsTableBuilderSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("PotentialContactsTableReceive", PeDebugMarkersUtils::passCallColor());

        const uint32_t countImgs = 1;
        PeGpuMemoryImage *imgs[countImgs] = {buffers.nodesContactsCounter};
        PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Read};
        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, countImgs, imgs, imgsA);

        buffers.nodesContactsCounter->sendReadCommand(gpuData->computeCommands);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PePotentialContactsTableBuilderSystem::dispatchGenerateLastContacts(PeSceneSimulationData *_simulationData,
                                                                             PeContactsContainerMemorySystem::LastStatistic _lastContactsStatistic,
                                                                             PeTableContactsUpdateData _updateData)
    {
        if (_lastContactsStatistic.lastCountContainers == 0)
            return;

        gpuData->computeCommands->debugMarkerBegin("GenerateLastListContacts", PeDebugMarkersUtils::subPassColor());

        PeSceneSimulationData::TableContactsData &tableContacts = _simulationData->tableContacts;
        memoryBarrierController->setImage(0, buffers.nodesContactsCounter, PeTypeAccess_Write);
        memoryBarrierController->setImage(1, buffers.pointerHeadsContactsImage, PeTypeAccess_Write);
        memoryBarrierController->setBuffer(0, _simulationData->tableContacts.contactsContainersBuffers[_updateData.lastIndexTable], PeTypeAccess_Read);
        memoryBarrierController->setBuffer(1, tableContacts.swapIndexesBuffers[_updateData.lastIndexTable], PeTypeAccess_Read);
        memoryBarrierController->setBuffer(2, _simulationData->geometryOBBs.geometryObbsBuffer, PeTypeAccess_Read);
        memoryBarrierController->setBuffer(3, _simulationData->geometryOBBs.geometryOrientationObbsBuffer, PeTypeAccess_Read);
        memoryBarrierController->setBuffer(4, buffers.nodesContactsBuffer, PeTypeAccess_Write);
        memoryBarrierController->setupBarrier(PeTypeStage_Compute, 5, 2);

        shaders.generateLastListContactsS->beginUpdate();
        shaders.generateLastListContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.nodesContactsCounter);
        shaders.generateLastListContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *buffers.pointerHeadsContactsImage);

        shaders.generateLastListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *_simulationData->tableContacts.contactsContainersBuffers[_updateData.lastIndexTable]);
        shaders.generateLastListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *tableContacts.swapIndexesBuffers[_updateData.lastIndexTable]);
        shaders.generateLastListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->geometryOBBs.geometryObbsBuffer);
        shaders.generateLastListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->geometryOBBs.geometryOrientationObbsBuffer);
        shaders.generateLastListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *buffers.nodesContactsBuffer);
        shaders.generateLastListContactsS->endUpdate();

        struct
        {
            uint countInputContainers;
            uint countOutputNodes;
        } constants;

        constants.countInputContainers = _lastContactsStatistic.lastCountContainers;
        constants.countOutputNodes = buffers.nodesContactsBuffer->getSize() / sizeof(PeGpuDataContactNode);

        gpuData->computeCommands->bindPipeline(shaders.generateLastListContactsP);
        gpuData->computeCommands->bindDescriptorSet(shaders.generateLastListContactsL, shaders.generateLastListContactsS->getCurrentSet(0), 0);
        gpuData->computeCommands->pushConstant(shaders.generateLastListContactsL, 0,
                                               sizeof(constants), &constants);

        gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, _lastContactsStatistic.lastCountContainers), 1, 1);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PePotentialContactsTableBuilderSystem::dispatchGenerateListContacts(PeSceneSimulationData *_simulationData,
                                                                             PePotentialContactsSystem::ResultStage _previosStage,
                                                                             PeTableContactsUpdateData _updateData)
    {
        gpuData->computeCommands->debugMarkerBegin("GenerateListContacts", PeDebugMarkersUtils::subPassColor());

        PeSceneSimulationData::TableContactsData &tableContacts = _simulationData->tableContacts;
        memoryBarrierController->setImage(0, _previosStage.segmentsCounter, PeTypeAccess_Read);
        memoryBarrierController->setImage(1, buffers.nodesContactsCounter, PeTypeAccess_Write);
        memoryBarrierController->setImage(2, buffers.pointerHeadsContactsImage, PeTypeAccess_Write);
        memoryBarrierController->setBuffer(0, _previosStage.segmentsBuffer, PeTypeAccess_Read);
        memoryBarrierController->setBuffer(1, _previosStage.indexesBuffer, PeTypeAccess_Read);
        memoryBarrierController->setBuffer(2, tableContacts.swapIndexesBuffers[_updateData.lastIndexTable], PeTypeAccess_Read);
        memoryBarrierController->setBuffer(3, _simulationData->geometryOBBs.geometryObbsBuffer, PeTypeAccess_Read);
        memoryBarrierController->setBuffer(4, _simulationData->geometryOBBs.geometryOrientationObbsBuffer, PeTypeAccess_Read);
        memoryBarrierController->setBuffer(5, buffers.nodesContactsBuffer, PeTypeAccess_Write);
        memoryBarrierController->setupBarrier(PeTypeStage_Compute, 6, 3);

        shaders.generateListContactsS->beginUpdate();
        shaders.generateListContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *_previosStage.segmentsCounter);
        shaders.generateListContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *buffers.nodesContactsCounter);
        shaders.generateListContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 2, *buffers.pointerHeadsContactsImage);

        shaders.generateListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *_previosStage.segmentsBuffer);
        shaders.generateListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *_previosStage.indexesBuffer);
        shaders.generateListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *tableContacts.swapIndexesBuffers[_updateData.lastIndexTable]);
        shaders.generateListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->geometryOBBs.geometryObbsBuffer);
        shaders.generateListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *_simulationData->geometryOBBs.geometryOrientationObbsBuffer);
        shaders.generateListContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *buffers.nodesContactsBuffer);
        shaders.generateListContactsS->endUpdate();

        struct
        {
            uint32_t countOutputNodes;
        } constants;

        constants.countOutputNodes = buffers.nodesContactsBuffer->getSize() / sizeof(PeGpuDataContactNode);

        gpuData->computeCommands->bindPipeline(shaders.generateListContactsP);
        gpuData->computeCommands->bindDescriptorSet(shaders.generateListContactsL, shaders.generateListContactsS->getCurrentSet(0), 0);
        gpuData->computeCommands->pushConstant(shaders.generateListContactsL, 0,
                                               sizeof(constants), &constants);

        gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, _previosStage.maxCountSegments), 1, 1);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PePotentialContactsTableBuilderSystem::dispatchGenerateListContactsPlane(PeSceneSimulationData *_simulationData, 
                                                                                    PePotentialContactsSystem::ResultStage _previosStage,
                                                                                    PeTableContactsUpdateData _updateData)
    {

        if(_previosStage.indexPlaneGeomtery == -1)
            return;

        gpuData->computeCommands->debugMarkerBegin("GenerateListContactsPlane", PeDebugMarkersUtils::subPassColor());

        PeSceneSimulationData::TableContactsData &tableContacts = _simulationData->tableContacts;
        memoryBarrierController->setImage(0, buffers.nodesContactsCounter, PeTypeAccess_Write);
        memoryBarrierController->setImage(1, buffers.pointerHeadsContactsImage, PeTypeAccess_Write);
        memoryBarrierController->setBuffer(0, _previosStage.indexesToTestBuffer, PeTypeAccess_Read);
        memoryBarrierController->setBuffer(1, tableContacts.swapIndexesBuffers[_updateData.lastIndexTable], PeTypeAccess_Read);
        memoryBarrierController->setBuffer(2, _simulationData->geometryOBBs.geometryObbsBuffer, PeTypeAccess_Read);
        memoryBarrierController->setBuffer(3, _simulationData->geometryOBBs.geometryOrientationObbsBuffer, PeTypeAccess_Read);
        memoryBarrierController->setBuffer(4, buffers.nodesContactsBuffer, PeTypeAccess_Write);
        memoryBarrierController->setupBarrier(PeTypeStage_Compute, 5, 2);

        shaders.generateListContactsPlaneS->beginUpdate();
        shaders.generateListContactsPlaneS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.nodesContactsCounter);
        shaders.generateListContactsPlaneS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *buffers.pointerHeadsContactsImage);

        shaders.generateListContactsPlaneS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *_previosStage.indexesToTestBuffer);
        shaders.generateListContactsPlaneS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *tableContacts.swapIndexesBuffers[_updateData.lastIndexTable]);
        shaders.generateListContactsPlaneS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->geometryOBBs.geometryObbsBuffer);
        shaders.generateListContactsPlaneS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->geometryOBBs.geometryOrientationObbsBuffer);
        shaders.generateListContactsPlaneS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *buffers.nodesContactsBuffer);
        shaders.generateListContactsPlaneS->endUpdate();

        struct
        {
            int indexPlane;
            uint countInputIndexes;
            uint countOutputNodes;
        } constants;

        constants.indexPlane = _previosStage.indexPlaneGeomtery;
        constants.countInputIndexes = _previosStage.countIndexesToTest;
        constants.countOutputNodes = buffers.nodesContactsBuffer->getSize() / sizeof(PeGpuDataContactNode);

        gpuData->computeCommands->bindPipeline(shaders.generateListContactsPlaneP);
        gpuData->computeCommands->bindDescriptorSet(shaders.generateListContactsPlaneL, shaders.generateListContactsPlaneS->getCurrentSet(0), 0);
        gpuData->computeCommands->pushConstant(shaders.generateListContactsPlaneL, 0,
                                               sizeof(constants), &constants);

        gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, constants.countInputIndexes), 1, 1);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PePotentialContactsTableBuilderSystem::dispatchFillTalbleContacts(PeSceneSimulationData *_simulationData,
                                                                           PeTableContactsUpdateData _updateData)
    {
        gpuData->computeCommands->debugMarkerBegin("FillTablContacts", PeDebugMarkersUtils::subPassColor());

        PeSceneSimulationData::TableContactsData &tableContacts = _simulationData->tableContacts;

        PeGpuMemoryImage *images[2] = {tableContacts.statisticUsingImage,
                                       tableContacts.tableContactsOBBsBuffers[_updateData.nextIndexTable]};

        {
            const uint32_t countImgs = 2;
            PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Write, PeTypeAccess_Write};
            memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, countImgs, images, imgsA);
        }
        VkClearColorValue value{};
        gpuData->computeCommands->clearImage(*images[0], &value, 0, 1, 0, images[0]->getCountLayers());

        value.int32[0] = value.int32[1] = value.int32[2] = value.int32[3] = -1;
        gpuData->computeCommands->clearImage(*images[1], &value, 0, 1, 0, images[1]->getCountLayers());
        {
            const uint32_t countImgs = 3;
            PeGpuMemoryImage *imgs[countImgs] = {
                buffers.pointerHeadsContactsImage,
                tableContacts.statisticUsingImage,
                tableContacts.tableContactsOBBsBuffers[_updateData.nextIndexTable],
            };
            PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Read, PeTypeAccess_Write, PeTypeAccess_Write};
            PeGpuMemoryBuffer *buff = buffers.nodesContactsBuffer;
            PeTypeAccessData buffA = PeTypeAccess_Read;

            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 1, &buff, &buffA, countImgs, imgs, imgsA);
        }

        shaders.fillTalbleContactsS->beginUpdate();
        shaders.fillTalbleContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.pointerHeadsContactsImage);
        shaders.fillTalbleContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *tableContacts.statisticUsingImage);
        shaders.fillTalbleContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 2, *tableContacts.tableContactsOBBsBuffers[_updateData.nextIndexTable]);

        shaders.fillTalbleContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.nodesContactsBuffer);
        shaders.fillTalbleContactsS->endUpdate();

        gpuData->computeCommands->bindPipeline(shaders.fillTalbleContactsP);
        gpuData->computeCommands->bindDescriptorSet(shaders.fillTalbleContactsL, shaders.fillTalbleContactsS->getCurrentSet(0), 0);

        uint32_t sizePointsImage = buffers.pointerHeadsContactsImage->getWidth();
        gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(4, sizePointsImage),
                                           PeUtilties::calculateGroups(4, sizePointsImage), 1);

        gpuData->computeCommands->debugMarkerEnd();
    }

    PeGpuTableContactsBuffer *PePotentialContactsTableBuilderSystem::createTableContacts(uint32_t _countIndexes)
    {
        PeGpuTableContactsBuffer *table = PH_NEW(PeGpuTableContactsBuffer)("TableContacts");
        table->init(gpuData);
        table->allocateData(_countIndexes, MAX_CONTACTS_WITH_OBB, VK_FORMAT_R32_SINT);
        return table;
    }

    PeGpuObjectsBuffer *PePotentialContactsTableBuilderSystem::createSwapIndexesBuffer(uint32_t _countIndexes)
    {
        PeGpuObjectType typeIndex = PeGpuObjectType::getObjectType<PeGpuDescription, uint32_t>(
            _countIndexes,
            false, sizeof(uint32_t));
        return PH_NEW(PeGpuObjectsBuffer)("SwapIndexesBuffer", gpuData, &typeIndex);
    }

    void PePotentialContactsTableBuilderSystem::ensureMemory(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage)
    {
        // resize contact nodes buffer
        {
            uint32_t countTestOBBs = _previosStage.countTestOBBs;
            uint32_t countWasWrote = 0;

            if (!buffers.firstLaunch)
            {
                buffers.nodesContactsCounter->receiveData(gpuData->computeCommands);
                countWasWrote = buffers.nodesContactsCounter->getData()[0];
                printf("Count contacts wrote: %i\n", countWasWrote);
            }
            else
            {
                buffers.firstLaunch = false;
            }

            uint32_t needSize = std::max(countWasWrote, countTestOBBs) * sizeof(PeGpuDataContactNode);

            reallocateDatasSystem->ensureBuffer(needSize, vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferStatic,
                                                buffers.nodesContactsBuffer, &buffers.nodesContactsBuffer,
                                                PeReallocateDatasSystem::ReserveMemoryType_140p);
        }

        // resize
        {
            uint32_t sizeSide = getSizeSideImage(_previosStage.maxIndexOBB + 1);

            reallocateDatasSystem->ensureImage(sizeSide, sizeSide,
                                               buffers.pointerHeadsContactsImage,
                                               &buffers.pointerHeadsContactsImage);
        }
    }

    uint32_t PePotentialContactsTableBuilderSystem::getSizeSideImage(uint32_t _countElemtenst)
    {
        float sqr = std::max(1.0f, (float)std::sqrt(_countElemtenst));
        uint32_t sizeSide = (uint32_t)std::ceil(sqr);
        return sizeSide;
    }
}