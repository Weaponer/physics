#include "physicsEngine/tableContacts/peContactsContainerMemorySystem.h"

#include "physicsEngine/peGpuStructures.h"

namespace phys
{
    PeContactsContainerMemorySystem::PeContactsContainerMemorySystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL),
          statistic()
    {
    }

    void PeContactsContainerMemorySystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
    }

    void PeContactsContainerMemorySystem::release()
    {
    }

    void PeContactsContainerMemorySystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        PeSceneSimulationData::TableContactsData &tableContacts = _simulationData->tableContacts;

        statistic.lastCountContacts = 0;
        statistic.lastCountContainers = 0;

        bool firstLaunch = false;
        if (tableContacts.contactsContainersBuffers[0] == NULL)
        {
            firstLaunch = true;

            //--
            const uint32_t countContainers = std::max(16U, (uint)(_simulationData->geometryOBBs.geometryObbType.getMaxCountObjects() * 1.1f));
            for (uint32_t i = 0; i < 2; i++)
                tableContacts.contactsContainersBuffers[i] = createContactsContainersBuffer(countContainers);

            //--
            const uint32_t countContacts = countContainers * 1;
            for (uint32_t i = 0; i < 2; i++)
                tableContacts.contactsBuffers[i] = createContactsBuffer(countContacts);

            //--
            const uint32_t actorsCount = _simulationData->actorManager.getMaxIndexObject();
            tableContacts.actorLostContainerBuffer = createActorLostContainersBuffer(actorsCount);
                
            //--
            tableContacts.allocatedContactsContainersCounter = PH_NEW(PeGpuCounterImageUint)("AllocatedContactsContainersCounter");
            tableContacts.allocatedContactsContainersCounter->init(gpuData);
            tableContacts.allocatedContactsContainersCounter->allocateData(1, false);

            //--
            tableContacts.allocatedContactsCounter = PH_NEW(PeGpuCounterImageUint)("AllocatedContactsCounter");
            tableContacts.allocatedContactsCounter->init(gpuData);
            tableContacts.allocatedContactsCounter->allocateData(1, false);

            tableContacts.allocatedContactsContainersCounter->clearCounter(gpuData, 0, PeStageData_Unknow);
            tableContacts.allocatedContactsCounter->clearCounter(gpuData, 0, PeStageData_Unknow);
        }

        if (firstLaunch)
            return;

        gpuData->computeCommands->debugMarkerBegin("ContactsContainerMemoryPrepareData", PeDebugMarkersUtils::passCallColor());

        tableContacts.allocatedContactsContainersCounter->receiveData(gpuData->computeCommands);
        tableContacts.allocatedContactsCounter->receiveData(gpuData->computeCommands);

        statistic.lastCountContainers = tableContacts.allocatedContactsContainersCounter->getData()[0];
        statistic.lastCountContacts = tableContacts.allocatedContactsCounter->getData()[0];

        // update size contacts containers buffer
        {
            uint32_t countAllocatedContainers = statistic.lastCountContainers;
            uint32_t maxCountContainers = tableContacts.contactsContainersBuffers[0]->getTypeDescription().getMaxCountObjects();
            printf("countAllocatedContainers: %i, max: %i\n", countAllocatedContainers, maxCountContainers);

            for (uint32_t i = 0; i < 2; i++)
            {
                PeGpuObjectsBuffer *currentBuffer = tableContacts.contactsContainersBuffers[i];
                reallocateDatasSystem->ensureObjectsBufferWithCopyAndGrap(countAllocatedContainers,
                                                                          currentBuffer,
                                                                          &currentBuffer,
                                                                          PeReallocateDatasSystem::ReserveMemoryType_130p);
                tableContacts.contactsContainersBuffers[i] = currentBuffer;
            }

            tableContacts.allocatedContactsContainersCounter->clearCounter(gpuData, 0, PeStageData_Unknow);
        }

        // update size contacts buffer
        {
            uint32_t countAllocatedContacts = statistic.lastCountContacts;
            uint32_t maxCountContacts = tableContacts.contactsBuffers[0]->getTypeDescription().getMaxCountObjects();
            printf("countAllocatedContacts: %i, max: %i\n", countAllocatedContacts, maxCountContacts);

            for (uint32_t i = 0; i < 2; i++)
            {
                PeGpuObjectsBuffer *currentBuffer = tableContacts.contactsBuffers[i];
                reallocateDatasSystem->ensureObjectsBufferWithCopyAndGrap(countAllocatedContacts,
                                                                          currentBuffer,
                                                                          &currentBuffer,
                                                                          PeReallocateDatasSystem::ReserveMemoryType_130p);
                tableContacts.contactsBuffers[i] = currentBuffer;
            }

            tableContacts.allocatedContactsCounter->clearCounter(gpuData, 0, PeStageData_Unknow);
        }

        // update size actor lost containers
        {
            const uint32_t actorsCount = _simulationData->actorManager.getMaxIndexObject();
            reallocateDatasSystem->ensureObjectsBuffer(actorsCount,
                                                        tableContacts.actorLostContainerBuffer, &tableContacts.actorLostContainerBuffer,
                                                        PeReallocateDatasSystem::ReserveMemoryType_120p);
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsContainerMemorySystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
    }

    void PeContactsContainerMemorySystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("ContactsContainerMemoryReceive", PeDebugMarkersUtils::passCallColor());

        PeSceneSimulationData::TableContactsData &tableContacts = _simulationData->tableContacts;

        const uint32_t countImgs = 2;
        PeGpuMemoryImage *imgs[countImgs] = {tableContacts.allocatedContactsContainersCounter, tableContacts.allocatedContactsCounter};
        PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Read, PeTypeAccess_Read};
        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, countImgs, imgs, imgsA);

        tableContacts.allocatedContactsContainersCounter->sendReadCommand(gpuData->computeCommands);
        tableContacts.allocatedContactsCounter->sendReadCommand(gpuData->computeCommands);

        gpuData->computeCommands->debugMarkerEnd();
    }

    PeGpuObjectsBuffer *PeContactsContainerMemorySystem::createContactsContainersBuffer(uint32_t _countContainers)
    {
        PeGpuObjectType typeContactsContainers = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataContactsContainer>(
            _countContainers, false, sizeof(PeGpuDataContactsContainer));
        return PH_NEW(PeGpuObjectsBuffer)("ContactsContainersBuffer", gpuData, &typeContactsContainers);
    }

    PeGpuObjectsBuffer *PeContactsContainerMemorySystem::createContactsBuffer(uint32_t _countContacts)
    {
        PeGpuObjectType typeContacts = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataContact>(
            _countContacts, false, sizeof(PeGpuDataContact));
        return PH_NEW(PeGpuObjectsBuffer)("ContactsBuffer", gpuData, &typeContacts);
    }

    PeGpuObjectsBuffer *PeContactsContainerMemorySystem::createActorLostContainersBuffer(uint32_t _countActors)
    {
        PeGpuObjectType typeContacts = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataContact>(
            _countActors, false, sizeof(uint32_t));
        return PH_NEW(PeGpuObjectsBuffer)("ActorLostContainersBuffer", gpuData, &typeContacts);
    }

    PeGpuObjectsBuffer *PeContactsContainerMemorySystem::createFreeContainersBuffer(uint32_t _countFreeContainers)
    {
        PeGpuObjectType typeFreeContacts = PeGpuObjectType::getObjectType<PeGpuDescription, uint32_t>(
            _countFreeContainers, false, sizeof(uint32_t));
        return PH_NEW(PeGpuObjectsBuffer)("FreeContainersBuffer", gpuData, &typeFreeContacts);
    }
}