#include "physicsEngine/tableContacts/peContactsContainerExchangerSystem.h"

#include "physicsEngine/common/peUtilities.h"

namespace phys
{
    PeContactsContainerExchangerSystem::PeContactsContainerExchangerSystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL),
          tableContactsCheckerSystem(NULL)
    {
    }

    void PeContactsContainerExchangerSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        tableContactsCheckerSystem = simulationContext->get<PeTableContactsCheckerSystem>();

        const char *nameExchangeContactsContainer = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_ContactsContainerExchanger);
        shaders.exchangeContactsContainerP = shaderProvider->getPipeline(nameExchangeContactsContainer);
        shaders.exchangeContactsContainerL = shaderProvider->getPipelineLayout(nameExchangeContactsContainer);
        shaders.exchangeContactsContainerS = shaderProvider->createShaderParameters(nameExchangeContactsContainer, 1);

        buffers.contactPairsCounter = PH_NEW(PeGpuCounterImageUint)("ContactPairsCounter", gpuData, 1, false);

        buffers.exchangeContainersBuffer = PH_NEW(PeGpuBuffer)("ExchangeContainersBuffer", gpuData,
                                                               vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferStatic,
                                                               sizeof(PeGpuExchangeContainer) * 256);
    }

    void PeContactsContainerExchangerSystem::release()
    {
        shaderProvider->destroyShaderAutoParameters(shaders.exchangeContactsContainerS);

        PH_RELEASE(buffers.contactPairsCounter);
        PH_RELEASE(buffers.exchangeContainersBuffer);
    }

    void PeContactsContainerExchangerSystem::prepareData(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData)
    {
        memoryBarrierController->initUnknowMemory(buffers.contactPairsCounter);
        memoryBarrierController->initUnknowMemory(buffers.exchangeContainersBuffer);

        gpuData->computeCommands->debugMarkerBegin("ContactsContainerExchangerPrepareData", PeDebugMarkersUtils::passCallColor());

        buffers.contactPairsCounter->receiveData(gpuData->computeCommands);
        uint32_t countContactPairs = buffers.contactPairsCounter->getData()[0];
        uint32_t maxCountContainers = countContactPairs;
        uint32_t needSize = maxCountContainers * sizeof(PeGpuExchangeContainer);
        reallocateDatasSystem->ensureBuffer(needSize, vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferStatic,
                                            buffers.exchangeContainersBuffer,
                                            &buffers.exchangeContainersBuffer);


        buffers.contactPairsCounter->clearCounter(gpuData, 0, PeStageData_Unknow);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsContainerExchangerSystem::buildPipeline(PeSceneSimulationData *_simulationData,
                                                           PePotentialContactsSystem::ResultStage _previosStage,
                                                           PeTableContactsUpdateData _updateData)
    {
        gpuData->computeCommands->debugMarkerBegin("ContactsContainerExchangerSimulation", PeDebugMarkersUtils::passCallColor());

        gpuData->computeCommands->debugMarkerBegin("EchangeContactsContainers", PeDebugMarkersUtils::subPassColor());

        PeGpuTableContactsBuffer *currentContactsOBBsTable = _simulationData->tableContacts.tableContactsOBBsBuffers[_updateData.nextIndexTable];
        PeGpuTableContactsBuffer *lastContactsOBBsTable = _simulationData->tableContacts.tableContactsOBBsBuffers[_updateData.lastIndexTable];

        PeGpuTableContactsBuffer *lastContactsContainersTable = _simulationData->tableContacts.tableContainerContactsBuffers[_updateData.lastIndexTable];

        // Already have Barrier Shader Read
        PeGpuTableContactsBuffer *activeTableContacts = tableContactsCheckerSystem->getActiveTableContacts();

        PeGpuObjectsBuffer *lastSwapIndexesBuffers = _simulationData->tableContacts.swapIndexesBuffers[_updateData.lastlastIndexTable];

        shaders.exchangeContactsContainerS->beginUpdate();
        shaders.exchangeContactsContainerS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *currentContactsOBBsTable);
        shaders.exchangeContactsContainerS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *lastContactsOBBsTable);
        shaders.exchangeContactsContainerS->setParameter<vk::ShaderParameterStorageImage>(0, 2, *lastContactsContainersTable);
        shaders.exchangeContactsContainerS->setParameter<vk::ShaderParameterStorageImage>(0, 3, *activeTableContacts);
        shaders.exchangeContactsContainerS->setParameter<vk::ShaderParameterStorageImage>(0, 4, *buffers.contactPairsCounter);
        shaders.exchangeContactsContainerS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *lastSwapIndexesBuffers);
        shaders.exchangeContactsContainerS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.exchangeContainersBuffer);
        shaders.exchangeContactsContainerS->endUpdate();

        {
            const uint32_t countImgs = 5;
            PeGpuMemoryImage *imgs[countImgs] = {
                currentContactsOBBsTable,
                lastContactsOBBsTable,
                lastContactsContainersTable,
                activeTableContacts,
                buffers.contactPairsCounter,
            };
            PeTypeAccessData imgsA[countImgs] = {
                PeTypeAccess_Read,
                PeTypeAccess_Read,
                PeTypeAccess_Read,
                PeTypeAccess_Read,
                PeTypeAccess_Write,
            };
            const uint32_t countBuffs = 2;
            PeGpuMemoryBuffer *buffs[countBuffs] = {
                lastSwapIndexesBuffers,
                buffers.exchangeContainersBuffer,
            };
            PeTypeAccessData buffsA[countBuffs] = {
                PeTypeAccess_Read,
                PeTypeAccess_Write,
            };
            memoryBarrierController->setupBarrier(PeTypeStage_Compute, countBuffs, buffs, buffsA, countImgs, imgs, imgsA);
        }

        struct
        {
            uint32_t maxIndexOBBs;
            uint32_t maxCountContainers;
        } pConstants;
        pConstants.maxIndexOBBs = _previosStage.maxIndexOBB;
        pConstants.maxCountContainers = buffers.exchangeContainersBuffer->getSize() / sizeof(PeGpuExchangeContainer);

        gpuData->computeCommands->bindPipeline(shaders.exchangeContactsContainerP);
        gpuData->computeCommands->bindDescriptorSet(shaders.exchangeContactsContainerL, shaders.exchangeContactsContainerS->getCurrentSet(0), 0);
        gpuData->computeCommands->pushConstant(shaders.exchangeContactsContainerL, 0,
                                               sizeof(pConstants), &pConstants);

        gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, pConstants.maxIndexOBBs), 1, 1);

        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsContainerExchangerSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("ContactsContainerExchangerReceive", PeDebugMarkersUtils::passCallColor());

        PeGpuMemoryImage *img = buffers.contactPairsCounter;
        PeTypeAccessData imgA = PeTypeAccess_Read;
        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, 1, &img, &imgA);

        buffers.contactPairsCounter->sendReadCommand(gpuData->computeCommands);

        gpuData->computeCommands->debugMarkerEnd();
    }
}