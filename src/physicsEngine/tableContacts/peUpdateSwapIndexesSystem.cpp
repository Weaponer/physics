#include "physicsEngine/tableContacts/peUpdateSwapIndexesSystem.h"
#include "physicsEngine/common/peUtilities.h"

namespace phys
{
    PeUpdateSwapIndexesSystem::PeUpdateSwapIndexesSystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL)

    {
    }

    void PeUpdateSwapIndexesSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();

        const char *nameUpdateSwapTable = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_UpdateSwapTable);

        shaders.updateSwapTableP = shaderProvider->getPipeline(nameUpdateSwapTable);
        shaders.updateSwapTableL = shaderProvider->getPipelineLayout(nameUpdateSwapTable);
        shaders.updateSwapTableS = shaderProvider->createShaderParameters(nameUpdateSwapTable, 1);
    }

    void PeUpdateSwapIndexesSystem::release()
    {
        shaderProvider->destroyShaderAutoParameters(shaders.updateSwapTableS);
    }

    void PeUpdateSwapIndexesSystem::prepareData(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData)
    {
    }

    void PeUpdateSwapIndexesSystem::buildPipeline(PeSceneSimulationData *_simulationData, PePotentialContactsSystem::ResultStage _previosStage, PeTableContactsUpdateData _updateData)
    {
        gpuData->computeCommands->debugMarkerBegin("UpdateSwapIndexesSimulation", PeDebugMarkersUtils::passCallColor());

        gpuData->computeCommands->debugMarkerBegin("UpdateSwapTable", PeDebugMarkersUtils::subPassColor());
        PeSceneSimulationData::TableContactsData &tableContacts = _simulationData->tableContacts;

        shaders.updateSwapTableS->beginUpdate();
        shaders.updateSwapTableS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *tableContacts.statisticUsingImage);
        shaders.updateSwapTableS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *tableContacts.swapIndexesBuffers[_updateData.nextIndexTable]);
        shaders.updateSwapTableS->endUpdate();

        {
            memoryBarrierController->setImage(0, tableContacts.statisticUsingImage, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(0, tableContacts.swapIndexesBuffers[_updateData.nextIndexTable], PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 1, 1);
        }

        struct
        {
            uint32_t countSwapIndexes;
        } constants;

        constants.countSwapIndexes = tableContacts.swapIndexesBuffers[_updateData.nextIndexTable]->getSize() / sizeof(uint32_t);

        gpuData->computeCommands->bindPipeline(shaders.updateSwapTableP);
        gpuData->computeCommands->bindDescriptorSet(shaders.updateSwapTableL, shaders.updateSwapTableS->getCurrentSet(0), 0);
        gpuData->computeCommands->pushConstant(shaders.updateSwapTableL, 0,
                                               sizeof(constants), &constants);

        gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, constants.countSwapIndexes), 1, 1);
        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeUpdateSwapIndexesSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
    }
}
