#include "physicsEngine/tableContacts/peContactsContainerDistributorSystem.h"
#include "physicsEngine/peLimits.h"
#include "physicsEngine/common/peUtilities.h"
#include "physicsEngine/common/peConstantsUtils.h"

namespace phys
{
    PeContactsContainerDistributorSystem::PeContactsContainerDistributorSystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL),
          contactsContainerExchangerSystem(NULL)
    {
    }

    void PeContactsContainerDistributorSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        contactsContainerExchangerSystem = simulationContext->get<PeContactsContainerExchangerSystem>();

        const char *nameGenerateDispatchDistributor = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateDistapchGroupByCounter);
        shaders.generateDispatchDistributorP = shaderProvider->getPipeline(nameGenerateDispatchDistributor);
        shaders.generateDispatchDistributorL = shaderProvider->getPipelineLayout(nameGenerateDispatchDistributor);
        shaders.generateDispatchDistributorS = shaderProvider->createShaderParameters(nameGenerateDispatchDistributor, 1);

        const char *nameContactsContainerDistributor = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_ContactsContainerDistributor);
        shaders.contactsContainerDistributorP = shaderProvider->getPipeline(nameContactsContainerDistributor);
        shaders.contactsContainerDistributorL = shaderProvider->getPipelineLayout(nameContactsContainerDistributor);
        shaders.contactsContainerDistributorS = shaderProvider->createShaderParameters(nameContactsContainerDistributor, 1);

        const char *nameCheckLostContainer = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_CheckLostContainer);
        shaders.checkLostContainerP = shaderProvider->getPipeline(nameCheckLostContainer);
        shaders.checkLostContainerL = shaderProvider->getPipelineLayout(nameCheckLostContainer);
        shaders.checkLostContainerS = shaderProvider->createShaderParameters(nameCheckLostContainer, 1);

        uint32_t sizeDispatchs = sizeof(PeGpuDataDispatchIndirect);
        buffers.outputCountGroupsBuffer = PH_NEW(PeGpuBuffer)("CountGroupsBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_UniformStorageBufferStatic, sizeDispatchs);

        buffers.indirectDispatchsBuffer = PH_NEW(PeGpuBuffer)("IndirectDispatchsBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_IndirectBufferStatic, sizeDispatchs);

        buffers.copyContainersBuffer = PH_NEW(PeGpuBuffer)("IndirectDispatchsBuffer", gpuData, vk::MemoryUseType_Long,
                                                           vk::VulkanBufferType_UniformStorageBufferStatic,
                                                           sizeof(PeGpuCopyContainer) * 256);

        buffers.containerCopiedBuffer = PH_NEW(PeGpuBuffer)("ContainerCopiedBuffer", gpuData, vk::MemoryUseType_Long,
                                                            vk::VulkanBufferType_UniformStorageBufferStatic,
                                                            sizeof(uint) * 256);

        reallocateDatasSystem->ensureBufferWithCopyAndGrap(sizeof(PeGpuCopyContainer) * 400, vk::MemoryUseType_Long,
                                                           vk::VulkanBufferType_UniformStorageBufferStatic,
                                                           buffers.copyContainersBuffer, &buffers.copyContainersBuffer);

        buffers.copyContainersCounter = PH_NEW(PeGpuCounterImageUint)("CopyContainersCounter");
        buffers.copyContainersCounter->init(gpuData);
        buffers.copyContainersCounter->allocateData(1, false);

        buffers.typesContactsCounter = PH_NEW(PeGpuCounterImageUint)("TypesContactsCounter");
        buffers.typesContactsCounter->init(gpuData);
        buffers.typesContactsCounter->allocateData(PeGpuPairGeometriesContact_Count, false);
    }

    void PeContactsContainerDistributorSystem::release()
    {
        shaderProvider->destroyShaderAutoParameters(shaders.checkLostContainerS);
        shaderProvider->destroyShaderAutoParameters(shaders.contactsContainerDistributorS);
        shaderProvider->destroyShaderAutoParameters(shaders.generateDispatchDistributorS);

        PH_RELEASE(buffers.typesContactsCounter);
        PH_RELEASE(buffers.copyContainersCounter);

        PH_RELEASE(buffers.containerCopiedBuffer);
        PH_RELEASE(buffers.copyContainersBuffer);
        PH_RELEASE(buffers.indirectDispatchsBuffer);
        PH_RELEASE(buffers.outputCountGroupsBuffer);
    }

    void PeContactsContainerDistributorSystem::prepareData(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData)
    {
        memoryBarrierController->initUnknowMemory(buffers.typesContactsCounter);
        memoryBarrierController->initUnknowMemory(buffers.copyContainersCounter);

        memoryBarrierController->initUnknowMemory(buffers.containerCopiedBuffer);
        memoryBarrierController->initUnknowMemory(buffers.copyContainersBuffer);
        memoryBarrierController->initUnknowMemory(buffers.indirectDispatchsBuffer);
        memoryBarrierController->initUnknowMemory(buffers.outputCountGroupsBuffer);

        gpuData->computeCommands->debugMarkerBegin("ContactsContainerDistributorPrepareData", PeDebugMarkersUtils::passCallColor());

        {
            printf("typesContactsCounter: ");
            _simulationData->tableContacts.lastCountUsedContainers = 0;
            buffers.typesContactsCounter->receiveData(gpuData->computeCommands);
            const uint32_t *data = buffers.typesContactsCounter->getData();
            for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
            {
                _simulationData->tableContacts.lastCountUsedContainers += data[i];
                printf("%i ", data[i]);
            }
            printf("\n");
        }

        buffers.typesContactsCounter->clearCounter(gpuData, 0, PeStageData_Unknow);
        buffers.copyContainersCounter->clearCounter(gpuData, 0, PeStageData_Unknow);

        uint32_t maxCountContainers = _simulationData->tableContacts.contactsContainersBuffers[0]->getTypeDescription().getMaxCountObjects();
        uint32_t needSize = maxCountContainers * sizeof(PeGpuCopyContainer);
        reallocateDatasSystem->ensureBuffer(needSize, vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferStatic,
                                            buffers.copyContainersBuffer,
                                            &buffers.copyContainersBuffer);

        reallocateDatasSystem->ensureBuffer(maxCountContainers * sizeof(uint32_t), vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferStatic,
                                            buffers.containerCopiedBuffer,
                                            &buffers.containerCopiedBuffer);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsContainerDistributorSystem::buildPipeline(PeSceneSimulationData *_simulationData,
                                                             PePotentialContactsSystem::ResultStage _previosStage,
                                                             PeTableContactsUpdateData _updateData)
    {
        gpuData->computeCommands->debugMarkerBegin("ContactsContainerDistributorSimulation", PeDebugMarkersUtils::passCallColor());

        PeGpuCounterImageUint *counter = contactsContainerExchangerSystem->getContactPairsCounter();
        PeGpuBuffer *exchangeContainersBuffer = contactsContainerExchangerSystem->getExchangeContainersBuffer();
        {
            gpuData->computeCommands->debugMarkerBegin("GenerateDispatchDistributor", PeDebugMarkersUtils::subPassColor());

            shaders.generateDispatchDistributorS->beginUpdate();
            shaders.generateDispatchDistributorS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *counter);
            shaders.generateDispatchDistributorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.outputCountGroupsBuffer);
            shaders.generateDispatchDistributorS->endUpdate();

            {
                memoryBarrierController->setImage(0, counter, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(0, buffers.outputCountGroupsBuffer, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 1, 1);
            }

            struct
            {
                uint32_t sizeGroup;
            } pContacts;
            pContacts.sizeGroup = 32;

            gpuData->computeCommands->bindPipeline(shaders.generateDispatchDistributorP);
            gpuData->computeCommands->bindDescriptorSet(shaders.generateDispatchDistributorL, shaders.generateDispatchDistributorS->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.generateDispatchDistributorL, 0,
                                                   sizeof(pContacts), &pContacts);
            gpuData->computeCommands->dispatch(1, 1, 1);

            {
                memoryBarrierController->setBuffer(0, buffers.outputCountGroupsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 1, 0);
            }

            gpuData->computeCommands->copyBuffer(*buffers.outputCountGroupsBuffer, 0,
                                                 *buffers.indirectDispatchsBuffer, 0,
                                                 buffers.outputCountGroupsBuffer->getSize());

            gpuData->computeCommands->debugMarkerEnd();
        }

        {
            PeGpuTableContactsBuffer *currentContainersTable = _simulationData->tableContacts.tableContainerContactsBuffers[_updateData.nextIndexTable];

            PeGpuObjectsBuffer *lastContactsContainer = _simulationData->tableContacts.contactsContainersBuffers[_updateData.lastIndexTable];
            PeGpuObjectsBuffer *currentContactsContainer = _simulationData->tableContacts.contactsContainersBuffers[_updateData.nextIndexTable];

            {
                memoryBarrierController->setBuffer(0, buffers.containerCopiedBuffer, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(1, _simulationData->tableContacts.actorLostContainerBuffer, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 2, 0);
                gpuData->computeCommands->fillBuffer(*buffers.containerCopiedBuffer, 0, buffers.containerCopiedBuffer->getSize(), 0);
                gpuData->computeCommands->fillBuffer(*_simulationData->tableContacts.actorLostContainerBuffer, 0,
                                                     _simulationData->tableContacts.actorLostContainerBuffer->getSize(), 0);
            }

            {
                shaders.contactsContainerDistributorS->beginUpdate();
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *counter);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *currentContainersTable);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageImage>(0, 2, *_simulationData->tableContacts.allocatedContactsContainersCounter);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageImage>(0, 3, *_simulationData->tableContacts.allocatedContactsCounter);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageImage>(0, 4, *buffers.typesContactsCounter);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageImage>(0, 5, *buffers.copyContainersCounter);

                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *_simulationData->geometryOBBs.geometryObbsBuffer);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *_simulationData->actorsBuffer);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->actorSleepingBuffer);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *exchangeContainersBuffer);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *lastContactsContainer);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *buffers.containerCopiedBuffer);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *currentContactsContainer);
                shaders.contactsContainerDistributorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 7, *buffers.copyContainersBuffer);
                shaders.contactsContainerDistributorS->endUpdate();

                struct
                {
                    int maxCountExchangeContainers;
                    int maxCountNewContainers;
                    int maxCountNewContacts;
                    uint32_t type;
                    uint32_t countContacts;
                    uint32_t indexType;
                } pConstants;

                PeGpuObjectsBuffer *contactsBuffer = _simulationData->tableContacts.contactsBuffers[0];

                pConstants.maxCountExchangeContainers = exchangeContainersBuffer->getSize() / sizeof(PeGpuExchangeContainer);
                pConstants.maxCountNewContainers = currentContactsContainer->getTypeDescription().getMaxCountObjects();
                pConstants.maxCountNewContacts = contactsBuffer->getTypeDescription().getMaxCountObjects();
                printf("maxCountExchangeContainers %i\n", pConstants.maxCountExchangeContainers);
                printf("maxCountNewContainers %i\n", pConstants.maxCountNewContainers);
                printf("maxCountNewContacts %i\n", pConstants.maxCountNewContacts);
                {
                    memoryBarrierController->setBuffer(0, buffers.indirectDispatchsBuffer, PeTypeAccess_IndirectCallRead);
                    memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
                }

                gpuData->computeCommands->bindPipeline(shaders.contactsContainerDistributorP);
                gpuData->computeCommands->bindDescriptorSet(shaders.contactsContainerDistributorL,
                                                            shaders.contactsContainerDistributorS->getCurrentSet(0), 0);

                for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
                {
                    gpuData->computeCommands->debugMarkerBegin("ContactsContainerDistributor", PeDebugMarkersUtils::subPassColor());

                    {
                        memoryBarrierController->setImage(0, counter, PeTypeAccess_Read);
                        memoryBarrierController->setImage(1, currentContainersTable, PeTypeAccess_Write);
                        memoryBarrierController->setImage(2, _simulationData->tableContacts.allocatedContactsContainersCounter, PeTypeAccess_Write);
                        memoryBarrierController->setImage(3, _simulationData->tableContacts.allocatedContactsCounter, PeTypeAccess_Write);
                        memoryBarrierController->setImage(4, buffers.typesContactsCounter, PeTypeAccess_Write);
                        memoryBarrierController->setImage(5, buffers.copyContainersCounter, PeTypeAccess_Write);

                        memoryBarrierController->setBuffer(0, _simulationData->geometryOBBs.geometryObbsBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(1, _simulationData->actorsBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(2, _simulationData->actorSleepingBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(3, exchangeContainersBuffer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(4, lastContactsContainer, PeTypeAccess_Read);
                        memoryBarrierController->setBuffer(5, buffers.containerCopiedBuffer, PeTypeAccess_Write);
                        memoryBarrierController->setBuffer(6, currentContactsContainer, PeTypeAccess_Write);
                        memoryBarrierController->setBuffer(7, buffers.copyContainersBuffer, PeTypeAccess_Write);
                        memoryBarrierController->setupBarrier(PeTypeStage_Compute, 8, 6);
                    }

                    pConstants.type = PeConstantsUtils::getTypeAndCountContacts()[i][0];
                    pConstants.countContacts = PeConstantsUtils::getTypeAndCountContacts()[i][1];
                    pConstants.indexType = i;

                    gpuData->computeCommands->pushConstant(shaders.contactsContainerDistributorL, 0,
                                                           sizeof(pConstants), &pConstants);

                    gpuData->computeCommands->dispatchIndirect(*buffers.indirectDispatchsBuffer, 0);

                    gpuData->computeCommands->debugMarkerEnd();
                }
            }

            gpuData->computeCommands->debugMarkerBegin("Check lost containers", PeDebugMarkersUtils::subPassColor());

            memoryBarrierController->setBuffer(0, buffers.containerCopiedBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(1, lastContactsContainer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(2, _simulationData->geometryOBBs.geometryObbsBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(3, _simulationData->tableContacts.actorLostContainerBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 4, 0);

            shaders.checkLostContainerS->beginUpdate();
            shaders.checkLostContainerS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.containerCopiedBuffer);
            shaders.checkLostContainerS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *lastContactsContainer);
            shaders.checkLostContainerS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->geometryOBBs.geometryObbsBuffer);
            shaders.checkLostContainerS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->tableContacts.actorLostContainerBuffer);
            shaders.checkLostContainerS->endUpdate();

            struct
            {
                int maxCountContainers;
            } pConstants;
            pConstants.maxCountContainers = _simulationData->tableContacts.lastCountUsedContainers;

            gpuData->computeCommands->bindPipeline(shaders.checkLostContainerP);
            gpuData->computeCommands->bindDescriptorSet(shaders.checkLostContainerL,
                                                        shaders.checkLostContainerS->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.checkLostContainerL, 0, sizeof(pConstants), &pConstants);
            gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, pConstants.maxCountContainers), 1, 1);

            gpuData->computeCommands->debugMarkerEnd();
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsContainerDistributorSystem::buildReceive(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData)
    {
        memoryBarrierController->setImage(0, buffers.typesContactsCounter, PeTypeAccess_Read);
        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, 1);
        buffers.typesContactsCounter->sendReadCommand(gpuData->computeCommands);
    }
}