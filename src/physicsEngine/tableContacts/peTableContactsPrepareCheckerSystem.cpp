#include "physicsEngine/tableContacts/peTableContactsPrepareCheckerSystem.h"

#include "physicsEngine/tableContacts/peTableContactsStructs.h"

#include "physicsEngine/common/peUtilities.h"

#include <map>
#include <queue>

namespace phys
{
    PeTableContactsPrepareCheckerSystem::PeTableContactsPrepareCheckerSystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL),
          countPotentialContactsDispatchs(0)
    {
    }

    void PeTableContactsPrepareCheckerSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();

        countPotentialContactsDispatchs = PeUtilties::calculateGroups(4, PeGpuPairGeometriesContact_Count);

        const char *namePotentialContactsCollector = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_PotentialContactsCollector);
        const char *nameCheckContactsDispatchGenerator = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateDistapchGroupByCounter);

        shaders.potentialContactsCollectorP = shaderProvider->getPipeline(namePotentialContactsCollector);
        shaders.potentialContactsCollectorL = shaderProvider->getPipelineLayout(namePotentialContactsCollector);
        shaders.potentialContactsCollectorS = shaderProvider->createShaderParameters(namePotentialContactsCollector, countPotentialContactsDispatchs);

        shaders.checkContactsDispatchGeneratorP = shaderProvider->getPipeline(nameCheckContactsDispatchGenerator);
        shaders.checkContactsDispatchGeneratorL = shaderProvider->getPipelineLayout(nameCheckContactsDispatchGenerator);
        shaders.checkContactsDispatchGeneratorS = shaderProvider->createShaderParameters(nameCheckContactsDispatchGenerator, 1);

        const uint32_t countPairs = 256;
        uint32_t prepareSizeBuffer = sizeof(PeGpuDataPairOBBs) * countPairs;
        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            buffers.maxCountPairs[i] = countPairs;
            buffers.outputCheckPairContacts[i] = PH_NEW(PeGpuBuffer)("CheckPairContactsBuffer", gpuData, vk::MemoryUseType_Long,
                                                                     vk::VulkanBufferType_UniformStorageBufferStatic, prepareSizeBuffer);
        }

        uint32_t sizeDispatchs = sizeof(PeGpuDataDispatchIndirect) * PeGpuPairGeometriesContact_Count;
        buffers.outputCountGroupsBuffer = PH_NEW(PeGpuBuffer)("CountGroupsBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_UniformStorageBufferStatic, sizeDispatchs);
        buffers.indirectDispatchsBuffer = PH_NEW(PeGpuBuffer)("IndirectDispatchsBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_IndirectBufferStatic, sizeDispatchs);

        buffers.contactPairsCounter = PH_NEW(PeGpuCounterImageUint)("ContactPairsCounter", gpuData, PeGpuPairGeometriesContact_Count, false);

        buffers.firstLaunch = true;

        shaders.checkContactsDispatchGeneratorS->beginUpdate();
        shaders.checkContactsDispatchGeneratorS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.contactPairsCounter);
        shaders.checkContactsDispatchGeneratorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.outputCountGroupsBuffer);
        shaders.checkContactsDispatchGeneratorS->endUpdate();

        readPoint = staticReader->createReadMemoryPoint(buffers.outputCountGroupsBuffer->getSize());
    }

    void PeTableContactsPrepareCheckerSystem::release()
    {
        staticReader->destroyReadMemoryPoint(readPoint);

        shaderProvider->destroyShaderAutoParameters(shaders.potentialContactsCollectorS);
        shaderProvider->destroyShaderAutoParameters(shaders.checkContactsDispatchGeneratorS);

        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
            PH_RELEASE(buffers.outputCheckPairContacts[i]);

        PH_RELEASE(buffers.outputCountGroupsBuffer);
        PH_RELEASE(buffers.indirectDispatchsBuffer);

        PH_RELEASE(buffers.contactPairsCounter);
    }

    void PeTableContactsPrepareCheckerSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
            memoryBarrierController->initUnknowMemory(buffers.outputCheckPairContacts[i]);

        memoryBarrierController->initUnknowMemory(buffers.outputCountGroupsBuffer);
        memoryBarrierController->initUnknowMemory(buffers.indirectDispatchsBuffer);
        memoryBarrierController->initUnknowMemory(buffers.contactPairsCounter);

        gpuData->computeCommands->debugMarkerBegin("TableContactsPrepareCheckerPrepareData", PeDebugMarkersUtils::passCallColor());

        if (!buffers.firstLaunch)
        {
            buffers.contactPairsCounter->receiveData(gpuData->computeCommands);

            for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
            {
                uint32_t countWrote = buffers.contactPairsCounter->getData()[i];
                uint32_t needSize = sizeof(PeGpuDataPairOBBs) * countWrote;
                std::cout << countWrote << " ";
                PeGpuBuffer *current = buffers.outputCheckPairContacts[i];
                bool result = reallocateDatasSystem->ensureBuffer(needSize, vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                  current, &current,
                                                                  reallocateDatasSystem->ReserveMemoryType_120p);
                buffers.outputCheckPairContacts[i] = current;
                if (result)
                    buffers.maxCountPairs[i] = current->getSize() / sizeof(PeGpuDataPairOBBs);
            }
            std::cout << std::endl;

            if (readPoint->IsValid())
            {
                /*PeGpuDataDispatchIndirect *mem = (PeGpuDataDispatchIndirect *)readPoint->getMemory();
                for (int i = 0; i < (int)PeGpuPairGeometriesContact_Count; i++)
                {
                    PeGpuDataDispatchIndirect dis = mem[i];
                    printf("disptach: %i %i %i\n", dis.countGroupX, dis.countGroupY, dis.countGroupZ);
                }*/
            }
        }
        else
        {
            buffers.firstLaunch = false;
        }
        buffers.contactPairsCounter->clearCounter(gpuData, 0, PeStageData_Unknow);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeTableContactsPrepareCheckerSystem::buildPipeline(PeSceneSimulationData *_simulationData,
                                                            PePotentialContactsSystem::ResultStage _previosStage,
                                                            PeTableContactsUpdateData _updateData)
    {
        gpuData->computeCommands->debugMarkerBegin("TableContactsPrepareCheckerSimulation", PeDebugMarkersUtils::passCallColor());

        uint32_t indexTableContacts = _updateData.nextIndexTable;
        PeGpuTableContactsBuffer *tableContacts = _simulationData->tableContacts.tableContactsOBBsBuffers[indexTableContacts];

        // collect potectial contacts pairs
        {
            struct
            {
                uint maxIndexOBBs;
                uint maxSizeBuffers[4];
                uint typeBuffers[4];
                uint indexCounters[4];
            } collectCheckPairs;
            collectCheckPairs.maxIndexOBBs = _previosStage.maxIndexOBB;

            PeGpuPairGeometriesContact typeBuffers[PeGpuPairGeometriesContact_Count] = {
                PeGpuPairGeometriesContact_PlaneBoxMask,
                PeGpuPairGeometriesContact_PlaneSphereMask,
                PeGpuPairGeometriesContact_PlaneCapsuleMask,
                PeGpuPairGeometriesContact_BoxBoxMask,
                PeGpuPairGeometriesContact_BoxSphereMask,
                PeGpuPairGeometriesContact_BoxCapsuleMask,
                PeGpuPairGeometriesContact_SphereSphereMask,
                PeGpuPairGeometriesContact_SphereCapsuleMask,
                PeGpuPairGeometriesContact_CapsuleCapsuleMask,
            };

            shaders.potentialContactsCollectorS->clearParameters();
            shaders.potentialContactsCollectorS->reset();

            for (uint32_t i = 0; i < countPotentialContactsDispatchs; i++)
            {
                gpuData->computeCommands->debugMarkerBegin("PotentialContactsCollector", PeDebugMarkersUtils::subPassColor());

                PeGpuBuffer *outputBuffers[4];

                for (uint32_t i2 = 0; i2 < 4; i2++)
                {
                    uint32_t indexType = i * 4 + i2;
                    indexType = std::min(indexType, PeGpuPairGeometriesContact_Count - 1);

                    collectCheckPairs.indexCounters[i2] = indexType;
                    collectCheckPairs.maxSizeBuffers[i2] = buffers.maxCountPairs[indexType];
                    collectCheckPairs.typeBuffers[i2] = typeBuffers[indexType];

                    outputBuffers[i2] = buffers.outputCheckPairContacts[indexType];
                }

                shaders.potentialContactsCollectorS->beginUpdate();
                shaders.potentialContactsCollectorS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.contactPairsCounter);
                shaders.potentialContactsCollectorS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *tableContacts);

                shaders.potentialContactsCollectorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *_simulationData->geometryOBBs.geometryObbsBuffer);
                shaders.potentialContactsCollectorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *outputBuffers[0]);
                shaders.potentialContactsCollectorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *outputBuffers[1]);
                shaders.potentialContactsCollectorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *outputBuffers[2]);
                shaders.potentialContactsCollectorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *outputBuffers[3]);
                shaders.potentialContactsCollectorS->endUpdate();

                VkDescriptorSet set = shaders.potentialContactsCollectorS->getCurrentSet(0);

                if (i != (countPotentialContactsDispatchs - 1))
                    shaders.potentialContactsCollectorS->switchToNextSet(0);

                {
                    const uint32_t countImgs = 2;
                    PeGpuMemoryImage *imgs[countImgs] = {tableContacts, buffers.contactPairsCounter};
                    PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Read, PeTypeAccess_Write};
                    const uint32_t countBuffs = 5;
                    PeGpuMemoryBuffer *buffs[countBuffs] = {
                        _simulationData->geometryOBBs.geometryObbsBuffer,
                        outputBuffers[0],
                        outputBuffers[1],
                        outputBuffers[2],
                        outputBuffers[3],
                    };
                    PeTypeAccessData buffsA[countBuffs] = {
                        PeTypeAccess_Read,
                        PeTypeAccess_Write,
                        PeTypeAccess_Write,
                        PeTypeAccess_Write,
                        PeTypeAccess_Write,
                    };

                    memoryBarrierController->setupBarrier(PeTypeStage_Compute, countBuffs, buffs, buffsA, countImgs, imgs, imgsA);
                }

                gpuData->computeCommands->bindPipeline(shaders.potentialContactsCollectorP);
                gpuData->computeCommands->bindDescriptorSet(shaders.potentialContactsCollectorL, set, 0);
                gpuData->computeCommands->pushConstant(shaders.potentialContactsCollectorL, 0,
                                                       sizeof(collectCheckPairs), &collectCheckPairs);

                gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, _previosStage.maxIndexOBB), 1, 1);

                gpuData->computeCommands->debugMarkerEnd();
            }
        }

        // generate dispatchs check contacts
        gpuData->computeCommands->debugMarkerBegin("CheckContactsDispatchGenerator", PeDebugMarkersUtils::subPassColor());
        {
            struct
            {
                uint sizeGroup;
            } pContacts;
            pContacts.sizeGroup = 32;

            {
                const uint32_t countImgs = 1;
                PeGpuMemoryImage *imgs[countImgs] = {buffers.contactPairsCounter};
                PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Read};
                const uint32_t countBuffs = 1;
                PeGpuMemoryBuffer *buffs[countBuffs] = {
                    buffers.outputCountGroupsBuffer,
                };
                PeTypeAccessData buffsA[countBuffs] = {
                    PeTypeAccess_Write,
                };
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, countBuffs, buffs, buffsA, countImgs, imgs, imgsA);
            }

            gpuData->computeCommands->bindPipeline(shaders.checkContactsDispatchGeneratorP);
            gpuData->computeCommands->bindDescriptorSet(shaders.checkContactsDispatchGeneratorL, shaders.checkContactsDispatchGeneratorS->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.potentialContactsCollectorL, 0,
                                                   sizeof(pContacts), &pContacts);
            gpuData->computeCommands->dispatch(PeGpuPairGeometriesContact_Count, 1, 1);

            {
                const uint32_t countBuffs = 1;
                PeGpuMemoryBuffer *buffs[countBuffs] = {buffers.outputCountGroupsBuffer};
                PeTypeAccessData buffsA[countBuffs] = {PeTypeAccess_Read};
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, countBuffs, buffs, buffsA, 0, NULL, NULL);
            }

            gpuData->computeCommands->copyBuffer(*buffers.outputCountGroupsBuffer, 0,
                                                 *buffers.indirectDispatchsBuffer, 0,
                                                 buffers.outputCountGroupsBuffer->getSize());
        }
        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeTableContactsPrepareCheckerSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("TableContactsPrepareCheckerReceive", PeDebugMarkersUtils::passCallColor());

        const uint32_t countImgs = 1;
        PeGpuMemoryImage *imgs[countImgs] = {buffers.contactPairsCounter};
        PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Read};

        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, countImgs, imgs, imgsA);

        buffers.contactPairsCounter->sendReadCommand(gpuData->computeCommands);

        staticReader->addRequest(*buffers.outputCountGroupsBuffer, 0, readPoint, 0, readPoint->getSize());

        gpuData->computeCommands->debugMarkerEnd();
    }
}