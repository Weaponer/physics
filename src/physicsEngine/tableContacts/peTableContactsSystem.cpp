#include "physicsEngine/tableContacts/peTableContactsSystem.h"

namespace phys
{
    PeTableContactsSystem::PeTableContactsSystem()
        : PeSystem(),
          tableContactsContext(),
          contactsContainerMemorySystem(),
          potentialContactsTableBuilderSystem(),
          tableContactsPrepareCheckerSystem(),
          tableContactsCheckerSystem(),
          contactsContainerExchangerSystem(),
          contactsContainerDistributorSystem(),
          contactsCopySystem(),
          updateSwapIndexesSystem(),
          gpu(NULL),
          potentialContactsSystem(NULL)
    {
    }

    void PeTableContactsSystem::onInit()
    {
        tableContactsContext.setParent(simulationContext);
        tableContactsContext.bind(&contactsContainerMemorySystem);
        tableContactsContext.bind(&potentialContactsTableBuilderSystem);
        tableContactsContext.bind(&tableContactsPrepareCheckerSystem);
        tableContactsContext.bind(&tableContactsCheckerSystem);
        tableContactsContext.bind(&contactsContainerExchangerSystem);
        tableContactsContext.bind(&contactsContainerDistributorSystem);
        tableContactsContext.bind(&contactsCopySystem);
        tableContactsContext.bind(&updateSwapIndexesSystem);

        contactsContainerMemorySystem.init(&tableContactsContext);
        potentialContactsTableBuilderSystem.init(&tableContactsContext);
        tableContactsPrepareCheckerSystem.init(&tableContactsContext);
        tableContactsCheckerSystem.init(&tableContactsContext);
        contactsContainerExchangerSystem.init(&tableContactsContext);
        contactsContainerDistributorSystem.init(&tableContactsContext);
        contactsCopySystem.init(&tableContactsContext);
        updateSwapIndexesSystem.init(&tableContactsContext);

        gpu = simulationContext->get<PeGpuData>();
        potentialContactsSystem = simulationContext->get<PePotentialContactsSystem>();
    }

    void PeTableContactsSystem::release()
    {
        updateSwapIndexesSystem.release();
        contactsCopySystem.release();
        contactsContainerDistributorSystem.release();
        contactsContainerExchangerSystem.release();
        tableContactsCheckerSystem.release();
        tableContactsPrepareCheckerSystem.release();
        potentialContactsTableBuilderSystem.release();
        contactsContainerMemorySystem.release();
        tableContactsContext.release();
    }

    void PeTableContactsSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("TableContactsPrepareData", PeDebugMarkersUtils::subGroupColor());

        PeTableContactsUpdateData updateData = getUpdateData(_simulationData);

        contactsContainerMemorySystem.prepareData(_simulationData);
        potentialContactsTableBuilderSystem.prepareData(_simulationData, potentialContactsSystem->getResultStage(), updateData);
        tableContactsPrepareCheckerSystem.prepareData(_simulationData);
        tableContactsCheckerSystem.prepareData(_simulationData, updateData);
        contactsContainerExchangerSystem.prepareData(_simulationData, updateData);
        contactsContainerDistributorSystem.prepareData(_simulationData, updateData);
        contactsCopySystem.prepareData(_simulationData, updateData);
        updateSwapIndexesSystem.prepareData(_simulationData, updateData);

        gpu->computeCommands->debugMarkerEnd();
    }

    void PeTableContactsSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("TableContactsSimulation", PeDebugMarkersUtils::subGroupColor());

        PeTableContactsUpdateData updateData = getUpdateData(_simulationData);

        PePotentialContactsSystem::ResultStage potentialContactsResult = potentialContactsSystem->getResultStage();

        contactsContainerMemorySystem.buildPipeline(_simulationData);
        potentialContactsTableBuilderSystem.buildPipeline(_simulationData, potentialContactsResult, updateData);
        tableContactsPrepareCheckerSystem.buildPipeline(_simulationData, potentialContactsResult, updateData);
        tableContactsCheckerSystem.buildPipeline(_simulationData);
        contactsContainerExchangerSystem.buildPipeline(_simulationData, potentialContactsResult, updateData);
        contactsContainerDistributorSystem.buildPipeline(_simulationData, potentialContactsResult, updateData);
        contactsCopySystem.buildPipeline(_simulationData, potentialContactsResult, updateData);
        updateSwapIndexesSystem.buildPipeline(_simulationData, potentialContactsResult, updateData);

        _simulationData->tableContacts.indexLastBuffer = updateData.nextIndexTable;
        _simulationData->tableContacts.lastMaxIndexOBB = potentialContactsResult.maxIndexOBB;

        gpu->computeCommands->debugMarkerEnd();
    }

    void PeTableContactsSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("TableContactsReceive", PeDebugMarkersUtils::subGroupColor());

        PeTableContactsUpdateData updateData = getUpdateData(_simulationData);

        contactsContainerMemorySystem.buildReceive(_simulationData);
        potentialContactsTableBuilderSystem.buildReceive(_simulationData);
        tableContactsPrepareCheckerSystem.buildReceive(_simulationData);
        tableContactsCheckerSystem.buildReceive(_simulationData);
        contactsContainerExchangerSystem.buildReceive(_simulationData);
        contactsContainerDistributorSystem.buildReceive(_simulationData, updateData);
        contactsCopySystem.buildReceive(_simulationData);
        updateSwapIndexesSystem.buildReceive(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    PeTableContactsUpdateData PeTableContactsSystem::getUpdateData(PeSceneSimulationData *_simulationData)
    {
        PeTableContactsUpdateData updateData{};
        updateData.lastIndexTable = _simulationData->tableContacts.indexLastBuffer;
        updateData.nextIndexTable = 1 - updateData.lastIndexTable;
        updateData.lastlastIndexTable = updateData.nextIndexTable;
        return updateData;
    }
}