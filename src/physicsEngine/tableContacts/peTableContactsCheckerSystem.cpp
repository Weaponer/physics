#include "physicsEngine/tableContacts/peTableContactsCheckerSystem.h"

#include "physicsEngine/common/peUtilities.h"

#include <bitset>

namespace phys
{
    PeTableContactsCheckerSystem::PeTableContactsCheckerSystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL),
          tableContactsPrepareCheckerSystem(NULL),
          shapeDescriptionSystem(NULL)
    {
    }

    void PeTableContactsCheckerSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        tableContactsPrepareCheckerSystem = simulationContext->get<PeTableContactsPrepareCheckerSystem>();
        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();

        const char *names[PeConstantsUtils::PeComputeTypeContact_CountAll];

        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            names[i] = PeConstantsUtils::getShaderNameTypeContact(PeConstantsUtils::PeCompute_CheckGeometriesContact,
                                                                  (PeConstantsUtils::PeComputeTypeContact)i);
            if (names[i] == NULL)
            {
                shaders.checkGeometriesContactP[i] = vk::ComputePipeline();
                shaders.checkGeometriesContactL[i] = VK_NULL_HANDLE;
                shaders.checkGeometriesContactS[i] = NULL;
            }
            else
            {
                shaders.checkGeometriesContactP[i] = shaderProvider->getPipeline(names[i]);
                shaders.checkGeometriesContactL[i] = shaderProvider->getPipelineLayout(names[i]);
                shaders.checkGeometriesContactS[i] = shaderProvider->createShaderParameters(names[i], 1);
            }
        }

        buffers.activeTableContacts = PH_NEW(PeGpuTableContactsBuffer)("ActiveTableContacts");
        buffers.activeTableContacts->init(gpuData);
        buffers.activeTableContacts->allocateData(256, 1, VK_FORMAT_R32_SINT);
    }

    void PeTableContactsCheckerSystem::release()
    {
        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            if (shaders.checkGeometriesContactL[i] != VK_NULL_HANDLE)
                shaderProvider->destroyShaderAutoParameters(shaders.checkGeometriesContactS[i]);
        }
        PH_RELEASE(buffers.activeTableContacts);
    }

    void PeTableContactsCheckerSystem::prepareData(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData)
    {
        memoryBarrierController->initUnknowMemory(buffers.activeTableContacts);

        gpuData->computeCommands->debugMarkerBegin("TableContactsCheckerPrepareData", PeDebugMarkersUtils::passCallColor());

        PeGpuTableContactsBuffer *currentTable = _simulationData->tableContacts.tableContactsOBBsBuffers[_updateData.nextIndexTable];
        uint32_t countIndexes = currentTable->getCountIndexes();
        reallocateDatasSystem->ensureTableContactsBuffer(countIndexes,
                                                         buffers.activeTableContacts,
                                                         &buffers.activeTableContacts);

        PeGpuMemoryImage *img = buffers.activeTableContacts;
        PeTypeAccessData imgA = PeTypeAccess_Write;
        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, 1, &img, &imgA);
        VkClearColorValue value{0};
        gpuData->computeCommands->clearImage(*img, &value);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeTableContactsCheckerSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("TableContactsCheckerSimulation", PeDebugMarkersUtils::passCallColor());

        struct
        {
            uint32_t maxCountPairs;
            uint32_t indexTypeContact;
        } pConstants;

        VkDescriptorSet sets[PeGpuPairGeometriesContact_Count];
        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            vk::ShaderAutoParameters *parameters = shaders.checkGeometriesContactS[i];
            if (parameters == NULL)
                continue;

            parameters->beginUpdate();
            parameters->setParameter<vk::ShaderParameterStorageImage>(0, 0, *tableContactsPrepareCheckerSystem->getContactPairsCounter());
            parameters->setParameter<vk::ShaderParameterStorageImage>(0, 1, *buffers.activeTableContacts);

            PeGpuBuffer *pairs = tableContactsPrepareCheckerSystem->getCheckPairContacts((PeGpuPairGeometriesContact)i);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *pairs);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *_simulationData->geometryOBBs.geometryOrientationObbsBuffer);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->geometryOBBs.geometryObbsBuffer);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *shapeDescriptionSystem->getGeometriesBuffer());

            parameters->endUpdate();

            sets[i] = parameters->getCurrentSet(0);
        }

        PeGpuBuffer *indirectBuffer = tableContactsPrepareCheckerSystem->getIndirectDispatchsBuffer();

        {
            const uint32_t countImgs = 2;
            PeGpuMemoryImage *imgs[countImgs] = {tableContactsPrepareCheckerSystem->getContactPairsCounter(), buffers.activeTableContacts};
            PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Read, PeTypeAccess_Write};
            const uint32_t countBuffs = PeGpuPairGeometriesContact_Count;
            PeGpuMemoryBuffer *buffs[countBuffs];
            PeTypeAccessData buffsA[countBuffs];
            for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
            {
                PeGpuBuffer *pairs = tableContactsPrepareCheckerSystem->getCheckPairContacts((PeGpuPairGeometriesContact)i);
                buffs[i] = pairs;
                buffsA[i] = PeTypeAccess_Read;
            }
            memoryBarrierController->setupBarrier(PeTypeStage_Compute, countBuffs, buffs, buffsA, countImgs, imgs, imgsA);

            PeGpuMemoryBuffer *indirectB = indirectBuffer;
            PeTypeAccessData indirectBA = PeTypeAccess_IndirectCallRead;
            memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, &indirectB, &indirectBA, 0, NULL, NULL);
        }

        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            gpuData->computeCommands->debugMarkerBegin("CheckGeometryContacts", PeDebugMarkersUtils::subPassColor());
            if (shaders.checkGeometriesContactL[i] != VK_NULL_HANDLE)
            {
                pConstants.maxCountPairs = tableContactsPrepareCheckerSystem->getMaxCountPairs((PeGpuPairGeometriesContact)i);
                pConstants.indexTypeContact = i;

                gpuData->computeCommands->bindPipeline(shaders.checkGeometriesContactP[i]);
                gpuData->computeCommands->bindDescriptorSet(shaders.checkGeometriesContactL[i], sets[i], 0);
                gpuData->computeCommands->pushConstant(shaders.checkGeometriesContactL[i], 0,
                                                       sizeof(pConstants), &pConstants);

                gpuData->computeCommands->dispatchIndirect(*indirectBuffer, sizeof(PeGpuDataDispatchIndirect) * i);
            }

            gpuData->computeCommands->debugMarkerEnd();
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeTableContactsCheckerSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        return;
        gpuData->computeCommands->debugMarkerBegin("TableContactsCheckerReceive", PeDebugMarkersUtils::passCallColor());

        PeGpuMemoryImage *img = buffers.activeTableContacts;
        PeTypeAccessData imgA = PeTypeAccess_Read;
        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, 1, &img, &imgA);

        gpuData->computeCommands->debugMarkerEnd();
    }
}