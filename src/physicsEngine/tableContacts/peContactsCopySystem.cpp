#include "physicsEngine/tableContacts/peContactsCopySystem.h"
#include "physicsEngine/common/peConstantsUtils.h"

namespace phys
{
    PeContactsCopySystem::PeContactsCopySystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL),
          contactsContainerExchangerSystem(NULL),
          contactsContainerDistributorSystem(NULL)
    {
    }

    void PeContactsCopySystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        contactsContainerExchangerSystem = simulationContext->get<PeContactsContainerExchangerSystem>();
        contactsContainerDistributorSystem = simulationContext->get<PeContactsContainerDistributorSystem>();

        const char *nameGenerateDispatchUpdateContacts = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_GenerateDispatchUpdateContacts);
        shaders.generateDispatchUpdateContactsP = shaderProvider->getPipeline(nameGenerateDispatchUpdateContacts);
        shaders.generateDispatchUpdateContactsL = shaderProvider->getPipelineLayout(nameGenerateDispatchUpdateContacts);
        shaders.generateDispatchUpdateContactsS = shaderProvider->createShaderParameters(nameGenerateDispatchUpdateContacts, 1);

        const char *nameCopyContacts = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_CopyContacts);
        shaders.copyContactsP = shaderProvider->getPipeline(nameCopyContacts);
        shaders.copyContactsL = shaderProvider->getPipelineLayout(nameCopyContacts);
        shaders.copyContactsS = shaderProvider->createShaderParameters(nameCopyContacts, 1);

        uint32_t sizeDispatchs = sizeof(PeGpuDataDispatchIndirect);
        buffers.copyContainersGroups = PH_NEW(PeGpuBuffer)("CopyContainersGroups", gpuData, vk::MemoryUseType_Long,
                                                           vk::VulkanBufferType_UniformStorageBufferStatic, sizeDispatchs);

        buffers.copyContainersGroupsIDB = PH_NEW(PeGpuBuffer)("CopyContainersGroupsIDB", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_IndirectBufferStatic, sizeDispatchs);

        buffers.containersData = PH_NEW(PeGpuBuffer)("ContainersData", gpuData, vk::MemoryUseType_Long,
                                                     vk::VulkanBufferType_UniformStorageBufferStatic,
                                                     sizeof(PeGpuDataContainersType) * PeGpuPairGeometriesContact_Count);

        buffers.updateContainersGroups = PH_NEW(PeGpuBuffer)("UpdateContainersGroups", gpuData, vk::MemoryUseType_Long,
                                                             vk::VulkanBufferType_UniformStorageBufferStatic,
                                                             sizeof(PeGpuDataDispatchIndirect) * PeGpuPairGeometriesContact_Count);

        buffers.updateContainersGroupsIDB = PH_NEW(PeGpuBuffer)("UpdateContainersGroupsIDB", gpuData, vk::MemoryUseType_Long,
                                                                vk::VulkanBufferType_IndirectBufferStatic,
                                                                sizeof(PeGpuDataDispatchIndirect) * PeGpuPairGeometriesContact_Count);

        pReadContainersData = staticReader->createReadMemoryPoint(buffers.containersData->getSize());
    }

    void PeContactsCopySystem::release()
    {
        staticReader->destroyReadMemoryPoint(pReadContainersData);

        shaderProvider->destroyShaderAutoParameters(shaders.copyContactsS);
        shaderProvider->destroyShaderAutoParameters(shaders.generateDispatchUpdateContactsS);

        PH_RELEASE(buffers.updateContainersGroupsIDB);
        PH_RELEASE(buffers.updateContainersGroups);
        PH_RELEASE(buffers.containersData);
        PH_RELEASE(buffers.copyContainersGroupsIDB);
        PH_RELEASE(buffers.copyContainersGroups);
    }

    void PeContactsCopySystem::prepareData(PeSceneSimulationData *_simulationData, PeTableContactsUpdateData _updateData)
    {
        memoryBarrierController->initUnknowMemory(buffers.updateContainersGroupsIDB);
        memoryBarrierController->initUnknowMemory(buffers.updateContainersGroups);
        memoryBarrierController->initUnknowMemory(buffers.containersData);
        memoryBarrierController->initUnknowMemory(buffers.copyContainersGroupsIDB);
        memoryBarrierController->initUnknowMemory(buffers.copyContainersGroups);

        /*if (pReadContainersData->IsValid())
        {
            PeGpuDataContainersType *containersData = (PeGpuDataContainersType *)pReadContainersData->getMemory();
            for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
            {
                PeGpuDataContainersType data = containersData[i];
                printf("firstContainer: %i, countContainers: %i, firstContact: %i, countContacts: %i\n",
                       data.firstContainer, data.countContainers,
                       data.firstContact, data.countContacts);
            }
        }*/
    }

    void PeContactsCopySystem::buildPipeline(PeSceneSimulationData *_simulationData,
                                             PePotentialContactsSystem::ResultStage _previosStage,
                                             PeTableContactsUpdateData _updateData)
    {
        gpuData->computeCommands->debugMarkerBegin("ContactsCopySimulation", PeDebugMarkersUtils::passCallColor());

        PeGpuCounterImageUint *copyContainersCounter = contactsContainerDistributorSystem->getCopyContainersCounter();
        PeGpuCounterImageUint *typesContactsCounter = contactsContainerDistributorSystem->getTypesContactsCounter();

        {
            gpuData->computeCommands->debugMarkerBegin("GenerateDispatchUpdateContacts", PeDebugMarkersUtils::subPassColor());

            shaders.generateDispatchUpdateContactsS->beginUpdate();
            shaders.generateDispatchUpdateContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *copyContainersCounter);
            shaders.generateDispatchUpdateContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *typesContactsCounter);
            shaders.generateDispatchUpdateContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.copyContainersGroups);
            shaders.generateDispatchUpdateContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.containersData);
            shaders.generateDispatchUpdateContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.updateContainersGroups);
            shaders.generateDispatchUpdateContactsS->endUpdate();

            struct
            {
                uint32_t contactsTypesCount;
                uint32_t countContactsInType[PeGpuPairGeometriesContact_Count];
            } pConstants;
            pConstants.contactsTypesCount = PeGpuPairGeometriesContact_Count;
            for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
                pConstants.countContactsInType[i] = PeConstantsUtils::getCountContactsInContainers()[i];

            {
                memoryBarrierController->setImage(0, copyContainersCounter, PeTypeAccess_Read);
                memoryBarrierController->setImage(1, typesContactsCounter, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(0, buffers.copyContainersGroups, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(1, buffers.containersData, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(2, buffers.updateContainersGroups, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 3, 2);
            }

            gpuData->computeCommands->bindPipeline(shaders.generateDispatchUpdateContactsP);
            gpuData->computeCommands->bindDescriptorSet(shaders.generateDispatchUpdateContactsL, shaders.generateDispatchUpdateContactsS->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.generateDispatchUpdateContactsL, 0,
                                                   sizeof(pConstants), &pConstants);
            gpuData->computeCommands->dispatch(1, 1, 1);

            {
                memoryBarrierController->setBuffer(0, buffers.copyContainersGroups, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, buffers.copyContainersGroupsIDB, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(2, buffers.updateContainersGroups, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, buffers.updateContainersGroupsIDB, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 4, 0);
            }

            gpuData->computeCommands->copyBuffer(*buffers.copyContainersGroups, 0,
                                                 *buffers.copyContainersGroupsIDB, 0,
                                                 buffers.copyContainersGroups->getSize());
            gpuData->computeCommands->copyBuffer(*buffers.updateContainersGroups, 0,
                                                 *buffers.updateContainersGroupsIDB, 0,
                                                 buffers.updateContainersGroups->getSize());

            gpuData->computeCommands->debugMarkerEnd();
        }

        // copy contacts
        {
            gpuData->computeCommands->debugMarkerBegin("CopyContacts", PeDebugMarkersUtils::subPassColor());

            PeGpuBuffer *copyContainersBuffer = contactsContainerDistributorSystem->getCopyContainersBuffer();
            PeGpuObjectsBuffer *lastContainersContacts = _simulationData->tableContacts.contactsContainersBuffers[_updateData.lastIndexTable];
            PeGpuObjectsBuffer *currentContainersContacts = _simulationData->tableContacts.contactsContainersBuffers[_updateData.nextIndexTable];
            PeGpuObjectsBuffer *lastContacts = _simulationData->tableContacts.contactsBuffers[_updateData.lastIndexTable];
            PeGpuObjectsBuffer *currentContacts = _simulationData->tableContacts.contactsBuffers[_updateData.nextIndexTable];

            shaders.copyContactsS->beginUpdate();
            shaders.copyContactsS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *copyContainersCounter);
            shaders.copyContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *copyContainersBuffer);
            shaders.copyContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *lastContainersContacts);
            shaders.copyContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *currentContainersContacts);
            shaders.copyContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *lastContacts);
            shaders.copyContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *currentContacts);
            shaders.copyContactsS->endUpdate();

            {
                memoryBarrierController->setImage(0, copyContainersCounter, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(0, copyContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, lastContainersContacts, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, currentContainersContacts, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, lastContacts, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(4, currentContacts, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 5, 1);
            }
            {
                memoryBarrierController->setBuffer(0, buffers.copyContainersGroupsIDB, PeTypeAccess_IndirectCallRead);
                memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
            }

            gpuData->computeCommands->bindPipeline(shaders.copyContactsP);
            gpuData->computeCommands->bindDescriptorSet(shaders.copyContactsL, shaders.copyContactsS->getCurrentSet(0), 0);
            gpuData->computeCommands->dispatchIndirect(*buffers.copyContainersGroupsIDB, 0);

            gpuData->computeCommands->debugMarkerEnd();
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsCopySystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        return;
        staticReader->addRequest(*buffers.containersData, 0, pReadContainersData, 0, buffers.containersData->getSize());
    }
}