#include "physicsEngine/peFetchSceneData.h"

#include "physics/internal/inActor.h"
#include "physics/internal/inRigidDynamic.h"

namespace phys
{
    PeFetchSceneData::PeFetchSceneData() 
        : PeSystem(),
        gpuData(NULL)
    {
    }

    void PeFetchSceneData::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
    }

    void PeFetchSceneData::release()
    {
    }

    void PeFetchSceneData::fetchSceneData(PeSceneSimulationData *_scene)
    {
        PeGpuObjectsBuffer *orientation = _scene->actorOrientationsBuffer;
        PeGpuDataActorOrientation *pOrientations = (PeGpuDataActorOrientation *)gpuData->computeCommands->beginWrite(*orientation, 0, orientation->getSize());

        uint maxIndex = _scene->actorManager.getMaxIndexObject();
        for(uint i = 0; i < maxIndex; i++)
        {
            if(!_scene->actorManager.isValidId(i))
                continue;

            InActor *actor = _scene->actorManager.getObjectPerId(i);
            if(!actor->isDynamic())
                continue;
            
            InRigidDynamic *dynamic = dynamic_cast<InRigidDynamic *>(actor);
            if(dynamic->getFlags() & OrientationFeedback)
            {
                PeGpuDataActorOrientation o = pOrientations[i];
                dynamic->position = o.position;
                dynamic->rotation = o.rotation;
            }
        }

        gpuData->computeCommands->endWrite(*orientation);
    }
}