#include "physicsEngine/potentialContacts/peUpdateOrientationGeometryOBBsSystem.h"

#include "physics/internal/inOutputDebugData.h"

namespace phys
{
    PeUpdateOrientationGeometryOBBsSystem::PeUpdateOrientationGeometryOBBsSystem()
        : PeSystem(),
          updateGeometrySystem(NULL),
          gpuData(NULL),
          shapeDescriptionSystem(NULL),
          shaderProvider(NULL),
          pipeline(),
          layout(),
          parameters(NULL),
          readedOBBofActorsData(),
          readOrientationOBBs(NULL),
          wasReaded(0)
    {
    }

    void PeUpdateOrientationGeometryOBBsSystem::onInit()
    {
        updateGeometrySystem = simulationContext->get<PeUpdateGeometryOBBsSystem>();
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();

        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();

        const char *nameProgram = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_UpdateGeometryOBBs);
        pipeline = shaderProvider->getPipeline(nameProgram);
        layout = shaderProvider->getPipelineLayout(nameProgram);
        parameters = shaderProvider->createShaderParameters(nameProgram, 1);
    }

    void PeUpdateOrientationGeometryOBBsSystem::release()
    {
        readedOBBofActorsData.release();
        if (readOrientationOBBs)
            staticReader->destroyReadMemoryPoint(readOrientationOBBs);

        shaderProvider->destroyShaderAutoParameters(parameters);
    }

    void PeUpdateOrientationGeometryOBBsSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        uint32_t countUpdateOBBs = updateGeometrySystem->getSizeDataOBBsToUpdate();

        if (!countUpdateOBBs)
            return;

        gpuData->computeCommands->debugMarkerBegin("UpdateOrientationGeometryOBBsSimulation", PeDebugMarkersUtils::passCallColor());

        PeGpuObjectsBuffer *actorOrientationsBuf = _simulationData->actorOrientationsBuffer;
        PeGpuObjectsBuffer *geometriesBuf = shapeDescriptionSystem->getGeometriesBuffer();
        PeGpuObjectsBuffer *obbsBuf = _simulationData->geometryOBBs.geometryObbsBuffer;
        PeGpuObjectsBuffer *obbOrientationsBuf = _simulationData->geometryOBBs.geometryOrientationObbsBuffer;

        parameters->beginUpdate();
        parameters->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 0, *updateGeometrySystem->getIndexesToUpdateBuffer());
        parameters->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 1, *actorOrientationsBuf);
        parameters->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 2, *geometriesBuf);
        parameters->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 3, *obbsBuf);
        parameters->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 4, *obbOrientationsBuf);
        parameters->endUpdate();

        gpuData->computeCommands->bindPipeline(pipeline);
        gpuData->computeCommands->bindDescriptorSet(layout, parameters->getCurrentSet(0), 0);
        gpuData->computeCommands->pushConstant(layout, 0, sizeof(uint32_t), &countUpdateOBBs);

        const uint32_t sizeGroupX = 16;
        uint32_t countGroups = countUpdateOBBs / sizeGroupX;
        if (countUpdateOBBs % sizeGroupX != 0)
            countGroups++;

        PeGpuMemoryBuffer *buffersControl = obbOrientationsBuf;
        PeTypeAccessData bufferAccess = PeTypeAccess_Write;
        memoryBarrierController->setupBarrier(PeTypeStage_Compute, 1, &buffersControl, &bufferAccess, 0, NULL, NULL);

        gpuData->computeCommands->dispatch(countGroups, 1, 1);
        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeUpdateOrientationGeometryOBBsSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        if (!_simulationData->settingsFeedbackDebugData.enable)
            return;

        uint32_t alignmentSize = _simulationData->geometryOBBs.geometryOrientationObbType.getAlignmentSize();
        if (readOrientationOBBs != NULL && readOrientationOBBs->IsValid() && wasReaded != 0)
        {
            InOutputDebugData *output = &_simulationData->outputDebugData;
            const uint8_t *orientaions = readOrientationOBBs->getMemory();
            for (uint32_t i = 0; i < wasReaded; i++)
            {
                PeGpuDataOrientationGeometryOBB readedOrientaion{};
                std::memcpy((uint8_t *)(&readedOrientaion), orientaions + alignmentSize * i, sizeof(readedOrientaion));

                PhOutputDebugData::OBBofActor actorData = *readedOBBofActorsData.getMemory(i);

                mth::vec3 size = mth::vec3(readedOrientaion.size.x, readedOrientaion.size.y, readedOrientaion.size.z);
                mth::vec3 pos = mth::vec3(readedOrientaion.position.x, readedOrientaion.position.y, readedOrientaion.position.z);

                output->addObbOrientation(readedOrientaion.rotation, size, pos, 
                                            actorData.indexOBBsOfActor, actorData.indexActor, actorData.indexDynamicActor);
            }
        }
        wasReaded = updateGeometrySystem->getSizeDataOBBsToTestContacts();

        if (!wasReaded)
            return;

        gpuData->computeCommands->debugMarkerBegin("UpdateOrientationGeometryOBBsReceive", PeDebugMarkersUtils::passCallColor());

        uint32_t needSize = alignmentSize * wasReaded;

        if (readOrientationOBBs != NULL && readOrientationOBBs->getSize() < needSize)
        {
            staticReader->destroyReadMemoryPoint(readOrientationOBBs);
            readOrientationOBBs = staticReader->createReadMemoryPoint(needSize);
        }
        if (readOrientationOBBs == NULL)
        {
            readOrientationOBBs = staticReader->createReadMemoryPoint(needSize);
        }

        staticReader->addRequest(*_simulationData->geometryOBBs.geometryOrientationObbsBuffer,
                                 0, readOrientationOBBs, 0, needSize);

        readedOBBofActorsData.clearMemory();
        const PhOutputDebugData::OBBofActor *obbOfActors = updateGeometrySystem->getFilledOBBsActorsData();
        for (uint32_t i = 0; i < wasReaded; i++)
            readedOBBofActorsData.assignmentMemoryAndCopy(&(obbOfActors[i]));

        gpuData->computeCommands->debugMarkerEnd();
    }
}