#include "physicsEngine/potentialContacts/peListCheckContactsBuilderSystem.h"

#include "physicsEngine/potentialContacts/pePotentialContactsStructs.h"

#include <queue>

namespace phys
{
    PeListCheckContactsBuilderSystem::PeListCheckContactsBuilderSystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          kdTreeBuilder(NULL)
    {
        buffers.headPointsCounterImage = NULL;
        buffers.segmentsCounterImage = NULL;
        buffers.indexesCounterImage = NULL;
        buffers.headPointsImage = NULL;
        buffers.outputNodeOBBsIndexesBuffer = NULL;
        buffers.outputSegmentsOBBsBuffer = NULL;
        buffers.outputIndexesBuffer = NULL;

        buffers.maxCountIndexes = 0;
        buffers.sizeNodesImage = mth::Vector2Int(0, 0);

        readCountSegments = NULL;
        readSegments = NULL;
        readIndexes = NULL;
    }

    void PeListCheckContactsBuilderSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();

        kdTreeBuilder = simulationContext->get<PeKdTreeBuilderSystem>();

        const char *nameLinkedListBuilder = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_LinkedListBuilder);
        const char *nameSegmentsBuilder = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_SegmentsBuilder);

        shaders.linkedListBuilderP = shaderProvider->getPipeline(nameLinkedListBuilder);
        shaders.linkedListBuilderL = shaderProvider->getPipelineLayout(nameLinkedListBuilder);
        shaders.linkedListBuilderS = shaderProvider->createShaderParameters(nameLinkedListBuilder, 1);

        shaders.segmentsBuilderP = shaderProvider->getPipeline(nameSegmentsBuilder);
        shaders.segmentsBuilderL = shaderProvider->getPipelineLayout(nameSegmentsBuilder);
        shaders.segmentsBuilderS = shaderProvider->createShaderParameters(nameSegmentsBuilder, 1);

        buffers.headPointsCounterImage = PH_NEW(PeGpuImage)("HeadPointsCounterImage");
        buffers.headPointsCounterImage->init(gpuData);
        buffers.headPointsCounterImage->allocateData(VK_IMAGE_TYPE_1D, VK_FORMAT_R32_UINT, 1, 1, 1, false);

        buffers.segmentsCounterImage = PH_NEW(PeGpuImage)("SegmentsCounterImage");
        buffers.segmentsCounterImage->init(gpuData);
        buffers.segmentsCounterImage->allocateData(VK_IMAGE_TYPE_1D, VK_FORMAT_R32_UINT, 1, 1, 1, false);

        buffers.indexesCounterImage = PH_NEW(PeGpuImage)("IndexesCounterImage");
        buffers.indexesCounterImage->init(gpuData);
        buffers.indexesCounterImage->allocateData(VK_IMAGE_TYPE_1D, VK_FORMAT_R32_UINT, 1, 1, 1, false);

        readCountSegments = staticReader->createReadImageMemoryPoint(*buffers.segmentsCounterImage);
        readSegments = staticReader->createReadMemoryPoint(sizeof(PeGpuDataSegmentOBBs) * 1);
        readIndexes = staticReader->createReadMemoryPoint(sizeof(uint32_t) * 1);
    }

    void PeListCheckContactsBuilderSystem::release()
    {
        staticReader->destroyReadImageMemoryPoint(readCountSegments);
        staticReader->destroyReadMemoryPoint(readIndexes);
        staticReader->destroyReadMemoryPoint(readSegments);

        PH_RELEASE(buffers.indexesCounterImage);
        PH_RELEASE(buffers.segmentsCounterImage);
        PH_RELEASE(buffers.headPointsCounterImage);

        shaderProvider->destroyShaderAutoParameters(shaders.segmentsBuilderS);
        shaderProvider->destroyShaderAutoParameters(shaders.linkedListBuilderS);

        if (buffers.outputIndexesBuffer != NULL)
        {
            PH_RELEASE(buffers.outputIndexesBuffer);
            PH_RELEASE(buffers.outputNodeOBBsIndexesBuffer);
            PH_RELEASE(buffers.outputSegmentsOBBsBuffer);
        }

        if (buffers.headPointsImage != NULL)
            PH_RELEASE(buffers.headPointsImage);
    }

    void PeListCheckContactsBuilderSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        memoryBarrierController->initUnknowMemory(buffers.indexesCounterImage);
        memoryBarrierController->initUnknowMemory(buffers.segmentsCounterImage);
        memoryBarrierController->initUnknowMemory(buffers.headPointsCounterImage);

        if (buffers.outputIndexesBuffer != NULL)
        {
            memoryBarrierController->initUnknowMemory(buffers.outputIndexesBuffer);
            memoryBarrierController->initUnknowMemory(buffers.outputNodeOBBsIndexesBuffer);
            memoryBarrierController->initUnknowMemory(buffers.outputSegmentsOBBsBuffer);
        }

        if (buffers.headPointsImage != NULL)
            memoryBarrierController->initUnknowMemory(buffers.headPointsImage);

        gpuData->computeCommands->debugMarkerBegin("ListCheckContactsPrepareData", PeDebugMarkersUtils::passCallColor());

        ensureMemory(_simulationData);

        const uint32_t countImgs = 4;
        PeGpuMemoryImage *imgs[countImgs] = {buffers.headPointsImage, buffers.headPointsCounterImage,
                                             buffers.segmentsCounterImage, buffers.indexesCounterImage};
        PeTypeAccessData imgsA[countImgs] = {PeTypeAccess_Write, PeTypeAccess_Write, PeTypeAccess_Write, PeTypeAccess_Write};
        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, countImgs, imgs, imgsA);

        VkClearColorValue value{};
        gpuData->computeCommands->clearImage(*buffers.headPointsCounterImage, &value);
        gpuData->computeCommands->clearImage(*buffers.segmentsCounterImage, &value);
        gpuData->computeCommands->clearImage(*buffers.indexesCounterImage, &value);
        value.int32[0] = value.int32[1] = value.int32[2] = value.int32[3] = -1;
        gpuData->computeCommands->clearImage(*buffers.headPointsImage, &value);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeListCheckContactsBuilderSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("ListCheckContactsSimulation", PeDebugMarkersUtils::passCallColor());

        uint32_t countSteps = kdTreeBuilder->getCountUseSteps();
        uint32_t lastStep = countSteps - 1;

        shaders.linkedListBuilderS->beginUpdate();

        vk::VulkanImageView viewPreviosNCO;
        vk::VulkanImageView viewCurrentNCO;

        PeGpuMemoryImage *imagePreviosNCO;
        PeGpuMemoryImage *imageCurrentNCO;
        if (lastStep == 0)
        {
            PeGpuImage *img = kdTreeBuilder->getOutputNodeCountOBBsImage(0);
            viewPreviosNCO = viewCurrentNCO = *img;
            imagePreviosNCO = imageCurrentNCO = img;
        }
        else
        {
            PeGpuImage *previosNCO = kdTreeBuilder->getOutputNodeCountOBBsImage(lastStep - 1);
            PeGpuImage *currentNCO = kdTreeBuilder->getOutputNodeCountOBBsImage(lastStep);

            viewPreviosNCO = *previosNCO;
            viewCurrentNCO = *currentNCO;

            imagePreviosNCO = previosNCO;
            imageCurrentNCO = currentNCO;
        }
        PeGpuMemoryBuffer *inputIndexBuffer = kdTreeBuilder->getOutputIndexBuffer(lastStep);
        shaders.linkedListBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 0, viewPreviosNCO);
        shaders.linkedListBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 1, viewCurrentNCO);
        shaders.linkedListBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 2, *buffers.headPointsImage);
        shaders.linkedListBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 3, *buffers.headPointsCounterImage);
        shaders.linkedListBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 4, *kdTreeBuilder->getCounterHeadPointsImage());

        shaders.linkedListBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *inputIndexBuffer);
        shaders.linkedListBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.outputNodeOBBsIndexesBuffer);

        shaders.linkedListBuilderS->endUpdate();

        //
        shaders.segmentsBuilderS->beginUpdate();
        shaders.segmentsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.headPointsImage);
        shaders.segmentsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *buffers.segmentsCounterImage);
        shaders.segmentsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 2, *buffers.indexesCounterImage);

        shaders.segmentsBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.outputNodeOBBsIndexesBuffer);
        shaders.segmentsBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.outputSegmentsOBBsBuffer);
        shaders.segmentsBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.outputIndexesBuffer);
        shaders.segmentsBuilderS->endUpdate();

        gpuData->computeCommands->debugMarkerBegin("LinkedListBuilder", PeDebugMarkersUtils::subPassColor());
        {
            memoryBarrierController->setImage(0, imagePreviosNCO, PeTypeAccess_Read);
            memoryBarrierController->setImage(1, imageCurrentNCO, PeTypeAccess_Read);
            memoryBarrierController->setImage(2, buffers.headPointsImage, PeTypeAccess_Write);
            memoryBarrierController->setImage(3, buffers.headPointsCounterImage, PeTypeAccess_Write);
            memoryBarrierController->setImage(4, kdTreeBuilder->getCounterHeadPointsImage(), PeTypeAccess_Read);
            memoryBarrierController->setBuffer(0, inputIndexBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(1, buffers.outputNodeOBBsIndexesBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 2, 5);

            struct
            {
                uint32_t indexStep;
                uint32_t countOutputNodes;
            } linkedListBuilderConst;

            linkedListBuilderConst.indexStep = lastStep;
            linkedListBuilderConst.countOutputNodes = buffers.maxCountIndexes;

            gpuData->computeCommands->bindPipeline(shaders.linkedListBuilderP);
            gpuData->computeCommands->bindDescriptorSet(shaders.linkedListBuilderL, shaders.linkedListBuilderS->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.linkedListBuilderL, 0, sizeof(linkedListBuilderConst), &linkedListBuilderConst);

            const uint32_t sizeGroupX = 128;
            uint32_t countGroupX = buffers.maxCountIndexes / sizeGroupX;
            if ((buffers.maxCountIndexes % sizeGroupX) != 0)
                countGroupX++;
            gpuData->computeCommands->dispatch(countGroupX, 1, 1);
        }
        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerBegin("SegmentBuilder", PeDebugMarkersUtils::subPassColor());
        {
            memoryBarrierController->setImage(0, buffers.headPointsImage, PeTypeAccess_Read);
            memoryBarrierController->setImage(1, buffers.segmentsCounterImage, PeTypeAccess_Write);
            memoryBarrierController->setImage(2, buffers.indexesCounterImage, PeTypeAccess_Write);
            memoryBarrierController->setBuffer(0, buffers.outputNodeOBBsIndexesBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(1, buffers.outputSegmentsOBBsBuffer, PeTypeAccess_Write);
            memoryBarrierController->setBuffer(2, buffers.outputIndexesBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 3, 3);

            gpuData->computeCommands->bindPipeline(shaders.segmentsBuilderP);
            gpuData->computeCommands->bindDescriptorSet(shaders.segmentsBuilderL, shaders.segmentsBuilderS->getCurrentSet(0), 0);

            mth::Vector2Int size = kdTreeBuilder->getOutputImageSize(lastStep);
            gpuData->computeCommands->dispatch(size.x / 2, size.y, 1);
        }
        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeListCheckContactsBuilderSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        return;
        
        gpuData->computeCommands->debugMarkerBegin("ListCheckContactsReceive", PeDebugMarkersUtils::passCallColor());

        /*if (readCountSegments->IsValid())
        {
            uint32_t count = *((uint32_t *)readCountSegments->getMemory());

            printf("count segments: %i\n", count);
            uint32_t sumIndexes = 0;
            uint32_t maxSegment = 0;
            PeGpuDataSegmentOBBs *segments = (PeGpuDataSegmentOBBs *)readSegments->getMemory();

            for (uint32_t i = 0; i < count; i++)
            {
                PeGpuDataSegmentOBBs segment = segments[i];
                sumIndexes += segment.count;
                maxSegment = std::max(segment.count, maxSegment);
            }
            printf("average count indexes in segment: %f\n", (float)sumIndexes / (float)count);
            printf("max count indexes in segment: %i\n", maxSegment);
            printf("count indexes in segments: %i\n", sumIndexes);
        }*/

        if (buffers.outputSegmentsOBBsBuffer->getSize() > readSegments->getSize())
        {
            staticReader->destroyReadMemoryPoint(readSegments);
            readSegments = staticReader->createReadMemoryPoint(buffers.outputSegmentsOBBsBuffer->getSize());
        }
        if (buffers.outputIndexesBuffer->getSize() > readIndexes->getSize())
        {
            staticReader->destroyReadMemoryPoint(readIndexes);
            readIndexes = staticReader->createReadMemoryPoint(buffers.outputIndexesBuffer->getSize());
        }

        PeGpuMemoryImage *img = buffers.segmentsCounterImage;
        PeTypeAccessData imgA = PeTypeAccess_Read;
        memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, 1, &img, &imgA);

        staticReader->addRequest(*buffers.segmentsCounterImage, readCountSegments, 0);
        staticReader->addRequest(*buffers.outputSegmentsOBBsBuffer, 0, readSegments, 0, readSegments->getSize());
        staticReader->addRequest(*buffers.outputIndexesBuffer, 0, readIndexes, 0, readIndexes->getSize());

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeListCheckContactsBuilderSystem::ensureMemory(PeSceneSimulationData *_simulationData)
    {
        uint32_t useSteps = kdTreeBuilder->getCountUseSteps();
        uint32_t lastStep = useSteps - 1;

        uint32_t countInputIndexes = kdTreeBuilder->getCountOutputIndexOBBsOnStep(lastStep);
        mth::Vector2Int sizeOutputImage = kdTreeBuilder->getOutputImageSize(lastStep);

        ensureImages(sizeOutputImage);
        ensureBuffers(countInputIndexes, sizeOutputImage);
    }

    void PeListCheckContactsBuilderSystem::ensureImages(mth::Vector2Int _sizeStep)
    {
        if (buffers.sizeNodesImage.x < _sizeStep.x || buffers.sizeNodesImage.y < _sizeStep.y)
        {
            if (buffers.headPointsImage != NULL)
            {
                PH_RELEASE(buffers.headPointsImage);
            }

            buffers.headPointsImage = PH_NEW(PeGpuImage)("HeadPointsImage");
            buffers.headPointsImage->init(gpuData);
            buffers.headPointsImage->allocateData(VK_IMAGE_TYPE_2D, VK_FORMAT_R32_SINT, _sizeStep.x, _sizeStep.y, 1, false);

            buffers.sizeNodesImage = _sizeStep;
        }
    }

    void PeListCheckContactsBuilderSystem::ensureBuffers(uint32_t _countInputIndexes, mth::Vector2Int _sizeImage)
    {
        if (buffers.outputIndexesBuffer != NULL && _countInputIndexes > buffers.maxCountIndexes)
        {
            PH_RELEASE(buffers.outputIndexesBuffer);
            PH_RELEASE(buffers.outputNodeOBBsIndexesBuffer);
        }

        if (_countInputIndexes > buffers.maxCountIndexes)
        {
            buffers.outputIndexesBuffer = PH_NEW(PeGpuBuffer)("IndexesBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_UniformStorageBufferStatic,
                                                              sizeof(uint32_t) * _countInputIndexes);

            buffers.outputNodeOBBsIndexesBuffer = PH_NEW(PeGpuBuffer)("NodeOBBsIndexesBuffer", gpuData, vk::MemoryUseType_Long,
                                                                      vk::VulkanBufferType_UniformStorageBufferStatic,
                                                                      sizeof(PeGpuDataNodeOBB) * _countInputIndexes);

            buffers.maxCountIndexes = _countInputIndexes;
        }

        uint32_t sizeBufferSegments = sizeof(PeGpuDataSegmentOBBs) * _sizeImage.x * _sizeImage.y;
        if (buffers.outputSegmentsOBBsBuffer != NULL && buffers.outputSegmentsOBBsBuffer->getSize() < sizeBufferSegments)
        {
            PH_RELEASE(buffers.outputSegmentsOBBsBuffer);
            buffers.outputSegmentsOBBsBuffer = NULL;
        }

        if (buffers.outputSegmentsOBBsBuffer == NULL)
        {
            buffers.outputSegmentsOBBsBuffer = PH_NEW(PeGpuBuffer)("SegmentsOBBsBuffer", gpuData, vk::MemoryUseType_Long,
                                                                   vk::VulkanBufferType_UniformStorageBufferStatic, sizeBufferSegments);
        }
    }
}