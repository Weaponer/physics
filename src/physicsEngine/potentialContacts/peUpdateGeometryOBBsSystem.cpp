#include "physicsEngine/potentialContacts/peUpdateGeometryOBBsSystem.h"
#include "physicsEngine/peGpuStructures.h"

#include "physics/internal/inActor.h"

namespace phys
{
    PeUpdateGeometryOBBsSystem::PeUpdateGeometryOBBsSystem()
        : PeSystem(),
          maxIndexOBB(0),
          indexPlaneGeomtery(-1),
          bufferOBBsToUpdate(),
          bufferOBBsToTestContacts(),
          bufferOBBsToTestOnlySizableContacts(),
          obbOfActorsData(),
          filledOfActorsData()
    {
    }

    void PeUpdateGeometryOBBsSystem::onInit()
    {

        gpuData = simulationContext->get<PeGpuData>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();

        buffers.indexesToUpdateBuffer = PH_NEW(PeGpuBuffer)("IndexesToUpdateBuffer", gpuData, vk::MemoryUseType_Long,
                                                            vk::VulkanBufferType_UniformStorageBufferDynamic, sizeof(uint32_t));
        buffers.indexesToTestBuffer = PH_NEW(PeGpuBuffer)("IndexesToTestBuffer", gpuData, vk::MemoryUseType_Long,
                                                          vk::VulkanBufferType_UniformStorageBufferDynamic, sizeof(uint32_t));
    }

    void PeUpdateGeometryOBBsSystem::release()
    {
        PH_RELEASE(buffers.indexesToUpdateBuffer);
        PH_RELEASE(buffers.indexesToTestBuffer);

        bufferOBBsToUpdate.release();
        bufferOBBsToTestContacts.release();
        bufferOBBsToTestOnlySizableContacts.release();
        obbOfActorsData.release();
        filledOfActorsData.release();
    }

    void PeUpdateGeometryOBBsSystem::prepareListUpdateGeometries(PeSceneSimulationData *_simulationData)
    {
        maxIndexOBB = 0;
        indexPlaneGeomtery = -1;
        bufferOBBsToUpdate.clearMemory();
        bufferOBBsToTestContacts.clearMemory();
        bufferOBBsToTestOnlySizableContacts.clearMemory();
        obbOfActorsData.clearMemory();
        filledOfActorsData.clearMemory();

        InActorManager *manager = _simulationData->actorMangerCache.getManager();
        uint32_t countEvenets = _simulationData->actorMangerCache.getCountEvents();
        InActorManager::ManagerEvent *events = _simulationData->actorMangerCache.getEvents();
        const uint32_t *flags = _simulationData->geometryOBBs.obbsFlags.getData();

        for (uint32_t i = 0; i < countEvenets; i++)
        {
            InActorManager::ManagerEvent event = events[i];
            if (!manager->isValidId(event.index))
                continue;

            InActor *actor = manager->getObjectPerId(event.index);
            if (actor->isDynamic())
                continue;

            PeGpuDataListGeometryOBBs obbs = _simulationData->geometryOBBs.actorAllocatedSegments.getElement(event.index);
            for (uint32_t i2 = 0; i2 < obbs.count; i2++)
            {
                uint32_t index = obbs.firstIndex + i2;
                bufferOBBsToUpdate.assignmentMemoryAndCopy(&index);
            }
        }

        uint32_t maxIndex = manager->getMaxIndexObject();
        for (uint32_t i = 0; i < maxIndex; i++)
        {
            if (!manager->isValidId(i))
                continue;

            InActor *actor = manager->getObjectPerId(i);
            bool needUpdateOBBs = actor->isDynamic();

            PeGpuDataListGeometryOBBs obbs = _simulationData->geometryOBBs.actorAllocatedSegments.getElement(i);
            for (uint32_t i2 = 0; i2 < obbs.count; i2++)
            {
                uint32_t index = obbs.firstIndex + i2;

                maxIndexOBB = std::max(maxIndexOBB, index); // write max index

                SupportIndexOBB obbOfActor;
                obbOfActor.data.indexDynamicActor = -1;
                obbOfActor.data.indexActor = i;
                obbOfActor.data.indexOBBsOfActor = i2;
                obbOfActor.indexOBB = index;

                if (needUpdateOBBs)
                    obbOfActor.data.indexDynamicActor = manager->getDataPerId(i)->indexAdditionalData;
                obbOfActorsData.assignmentMemoryAndCopy(&obbOfActor);

                bool sizable = isSizable(flags[index]);
                if (sizable)
                    bufferOBBsToTestOnlySizableContacts.assignmentMemoryAndCopy(&index);

                if (!sizable)
                {
                    if (!needUpdateOBBs)
                        indexPlaneGeomtery = index; // non dynamic only.
                    else
                        continue;
                }

                bufferOBBsToTestContacts.assignmentMemoryAndCopy(&index);

                if (needUpdateOBBs)
                    bufferOBBsToUpdate.assignmentMemoryAndCopy(&index);
            }
        }

        uint32_t capacityOBBs = maxIndexOBB + 1;
        if (filledOfActorsData.getCapacity() < capacityOBBs)
            filledOfActorsData.setCapacity(capacityOBBs);

        for (uint32_t i = 0; i < capacityOBBs; i++)
        {
            PhOutputDebugData::OBBofActor deff;
            deff.indexDynamicActor = -1;
            deff.indexActor = -1;
            deff.indexOBBsOfActor = -1;
            filledOfActorsData.assignmentMemoryAndCopy(&deff);
        }
        for (uint32_t i = 0; i < obbOfActorsData.getPos(); i++)
        {
            SupportIndexOBB obbOfActor = *obbOfActorsData.getMemory(i);
            *filledOfActorsData.getMemory(obbOfActor.indexOBB) = obbOfActor.data;
        }

        // update buffers
        memoryBarrierController->initUnknowMemory(buffers.indexesToUpdateBuffer);
        memoryBarrierController->initUnknowMemory(buffers.indexesToTestBuffer);

        reallocateDatasSystem->ensureBuffer(sizeof(uint32_t) * bufferOBBsToUpdate.getPos(),
                                            vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferDynamic,
                                            buffers.indexesToUpdateBuffer, &buffers.indexesToUpdateBuffer);
        reallocateDatasSystem->ensureBuffer(sizeof(uint32_t) * bufferOBBsToTestContacts.getPos(),
                                            vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferDynamic,
                                            buffers.indexesToTestBuffer, &buffers.indexesToTestBuffer);

        if(bufferOBBsToUpdate.getPos() == 0)
            return;

        gpuData->computeCommands->writeToBuffer(*buffers.indexesToUpdateBuffer, 0, sizeof(uint32_t) * bufferOBBsToUpdate.getPos(),
                                                bufferOBBsToUpdate.getMemory(0));
        
        if(bufferOBBsToTestContacts.getPos() == 0)
            return;

        gpuData->computeCommands->writeToBuffer(*buffers.indexesToTestBuffer, 0, sizeof(uint32_t) * bufferOBBsToTestContacts.getPos(),
                                                bufferOBBsToTestContacts.getMemory(0));
    }

    bool PeUpdateGeometryOBBsSystem::isSizable(uint32_t _flags)
    {
        return (PeGpuGeometry_Unsizable & _flags) == 0;
    }
}