#include <vulkan/vulkan.h>
#include <MTH/glGen_matrix.h>

#include "physicsEngine/potentialContacts/peGlobalBoundOBBsBuilderSystem.h"
#include "physicsEngine/potentialContacts/pePotentialContactsStructs.h"

namespace phys
{
    PeGlobalBoundOBBsBuilderSystem::PeGlobalBoundOBBsBuilderSystem()
        : PeSystem(),
          updateGeometrySystem(NULL),
          gpuData(NULL),
          shapeDescriptionSystem(NULL),
          shaderProvider(NULL),
          readMemory(NULL)
    {
    }

    void PeGlobalBoundOBBsBuilderSystem::onInit()
    {
        updateGeometrySystem = simulationContext->get<PeUpdateGeometryOBBsSystem>();
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();

        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();

        const char *nameCalculateAverage = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_CalculateAverageOBBs);
        const char *nameCalculateDispersionOBBs = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_CalculateDispersionOBBs);
        const char *nameBuildBoundOBBs = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_BuildBoundOBBs);
        const char *nameCheckContactsBoundOBBs = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_CheckContactsBoundOBBs);
        const char *nameRebalanceBoundOBBs = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_RebalanceBoundOBBs);

        shaders.calculateAverageP = shaderProvider->getPipeline(nameCalculateAverage);
        shaders.calculateAverageL = shaderProvider->getPipelineLayout(nameCalculateAverage);
        shaders.calculateAverageS = shaderProvider->createShaderParameters(nameCalculateAverage, 1);

        shaders.calculateDispersionBoundP = shaderProvider->getPipeline(nameCalculateDispersionOBBs);
        shaders.calculateDispersionBoundL = shaderProvider->getPipelineLayout(nameCalculateDispersionOBBs);
        shaders.calculateDispersionBoundS = shaderProvider->createShaderParameters(nameCalculateDispersionOBBs, 1);

        shaders.buildBoundP = shaderProvider->getPipeline(nameBuildBoundOBBs);
        shaders.buildBoundL = shaderProvider->getPipelineLayout(nameBuildBoundOBBs);
        shaders.buildBoundS = shaderProvider->createShaderParameters(nameBuildBoundOBBs, 1);

        shaders.checkContactsBoundP = shaderProvider->getPipeline(nameCheckContactsBoundOBBs);
        shaders.checkContactsBoundL = shaderProvider->getPipelineLayout(nameCheckContactsBoundOBBs);
        shaders.checkContactsBoundS = shaderProvider->createShaderParameters(nameCheckContactsBoundOBBs, 1);

        shaders.rebalanceGlobalBoundP = shaderProvider->getPipeline(nameRebalanceBoundOBBs);
        shaders.rebalanceGlobalBoundL = shaderProvider->getPipelineLayout(nameRebalanceBoundOBBs);
        shaders.rebalanceGlobalBoundS = shaderProvider->createShaderParameters(nameRebalanceBoundOBBs, 1);

        buffers.oAveragePos = PH_NEW(PeGpuBuffer)("AveragePosBuffer", gpuData, vk::MemoryUseType_Long,
                                                  vk::VulkanBufferType_UniformStorageBufferStatic, sizeof(PeGpuDataOutAverage));
        buffers.oDispersionSize = PH_NEW(PeGpuBuffer)("DispersionSizeBuffer", gpuData, vk::MemoryUseType_Long,
                                                      vk::VulkanBufferType_UniformStorageBufferStatic, sizeof(PeGpuDataOutDisperstion));
        buffers.oMainPlanes = PH_NEW(PeGpuBuffer)("MainPlanesBuffer", gpuData, vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferStatic,
                                                  sizeof(PeGpuDataMainPlanes));
        buffers.iPlaneCountContact = PH_NEW(PeGpuBuffer)("PlaneCountContactBuffer", gpuData, vk::MemoryUseType_Long,
                                                         vk::VulkanBufferType_UniformStorageBufferStatic, sizeof(PeGpuDataCountContacts));
        buffers.iGlobalBound = PH_NEW(PeGpuBuffer)("GlobalBoundBuffer", gpuData, vk::MemoryUseType_Long,
                                                   vk::VulkanBufferType_UniformStorageBufferStatic, sizeof(PeGpuDataGlobalBound));

        shaders.calculateAverageS->beginUpdate();
        shaders.calculateAverageS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 0, *buffers.oAveragePos);
        shaders.calculateAverageS->endUpdate();

        shaders.calculateDispersionBoundS->beginUpdate();
        shaders.calculateDispersionBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 0, *buffers.oAveragePos);
        shaders.calculateDispersionBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 1, *buffers.oDispersionSize);
        shaders.calculateDispersionBoundS->endUpdate();

        shaders.buildBoundS->beginUpdate();
        shaders.buildBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 0, *buffers.oAveragePos);
        shaders.buildBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 1, *buffers.oDispersionSize);
        shaders.buildBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 2, *buffers.oMainPlanes);
        shaders.buildBoundS->endUpdate();

        shaders.checkContactsBoundS->beginUpdate();
        shaders.checkContactsBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 0, *buffers.oMainPlanes);
        shaders.checkContactsBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 1, *buffers.iPlaneCountContact);
        shaders.checkContactsBoundS->endUpdate();

        shaders.rebalanceGlobalBoundS->beginUpdate();
        shaders.rebalanceGlobalBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 0, *buffers.oMainPlanes);
        shaders.rebalanceGlobalBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 1, *buffers.iPlaneCountContact);
        shaders.rebalanceGlobalBoundS->setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 2, *buffers.iGlobalBound);
        shaders.rebalanceGlobalBoundS->endUpdate();

        readMemory = staticReader->createReadMemoryPoint(sizeof(mth::mat4));
    }

    void PeGlobalBoundOBBsBuilderSystem::release()
    {
        staticReader->destroyReadMemoryPoint(readMemory);

        PH_RELEASE(buffers.oAveragePos);
        PH_RELEASE(buffers.oDispersionSize);
        PH_RELEASE(buffers.oMainPlanes);
        PH_RELEASE(buffers.iPlaneCountContact);
        PH_RELEASE(buffers.iGlobalBound);

        shaderProvider->destroyShaderAutoParameters(shaders.rebalanceGlobalBoundS);
        shaderProvider->destroyShaderAutoParameters(shaders.checkContactsBoundS);
        shaderProvider->destroyShaderAutoParameters(shaders.buildBoundS);
        shaderProvider->destroyShaderAutoParameters(shaders.calculateDispersionBoundS);
        shaderProvider->destroyShaderAutoParameters(shaders.calculateAverageS);
    }

    void PeGlobalBoundOBBsBuilderSystem::prepareData(PeSceneSimulationData *_simulationData)
    {

        memoryBarrierController->initUnknowMemory(buffers.oAveragePos);
        memoryBarrierController->initUnknowMemory(buffers.oDispersionSize);
        memoryBarrierController->initUnknowMemory(buffers.oMainPlanes);
        memoryBarrierController->initUnknowMemory(buffers.iPlaneCountContact);
        memoryBarrierController->initUnknowMemory(buffers.iGlobalBound);

        gpuData->computeCommands->debugMarkerBegin("GlobalBoundOBBsPrepareData", PeDebugMarkersUtils::passCallColor());

        gpuData->computeCommands->fillBuffer(*buffers.oAveragePos, 0, buffers.oAveragePos->getSize(), 0);
        gpuData->computeCommands->fillBuffer(*buffers.oDispersionSize, 0, buffers.oDispersionSize->getSize(), 0);
        gpuData->computeCommands->fillBuffer(*buffers.iGlobalBound, 0, buffers.iGlobalBound->getSize(), 0);
        gpuData->computeCommands->fillBuffer(*buffers.iPlaneCountContact, 0, buffers.iPlaneCountContact->getSize(), 0);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeGlobalBoundOBBsBuilderSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("GlobalBoundOBBsSimulation", PeDebugMarkersUtils::passCallColor());
        {
            buildGlobalBound(_simulationData);
        }
        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeGlobalBoundOBBsBuilderSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("GlobalBoundOBBsBuilderReceive", PeDebugMarkersUtils::passCallColor());
        if (readMemory->IsValid())
        {
            mth::mat4 ort;
            std::memcpy((uint8_t *)(&ort), readMemory->getMemory(), sizeof(ort));
            _simulationData->outputDebugData.setOrientationGlobalBoundOBBs(ort);
            /*std::cout << ort.m[0][0] << " " << ort.m[0][1] << " " << ort.m[0][2] << " " << ort.m[0][3] << std::endl;
            std::cout << ort.m[1][0] << " " << ort.m[1][1] << " " << ort.m[1][2] << " " << ort.m[1][3] << std::endl;
            std::cout << ort.m[2][0] << " " << ort.m[2][1] << " " << ort.m[2][2] << " " << ort.m[2][3] << std::endl;
            std::cout << ort.m[3][0] << " " << ort.m[3][1] << " " << ort.m[3][2] << " " << ort.m[3][3] << std::endl;*/
        }

        staticReader->addRequest(*buffers.iGlobalBound, 0, readMemory, 0, sizeof(mth::mat4));
        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeGlobalBoundOBBsBuilderSystem::buildGlobalBound(PeSceneSimulationData *_simulationData)
    {
        uint32_t countTestOBBs = updateGeometrySystem->getSizeDataOBBsToTestContacts();

        if (!countTestOBBs)
            return;

        PeGpuBuffer *indexesToTest = updateGeometrySystem->getIndexesToTestBuffer();

        PeGpuMemoryBuffer *obbOrientationsBuf = _simulationData->geometryOBBs.geometryOrientationObbsBuffer;
        PeGpuMemoryBuffer *sceneSettings = _simulationData->sceneSettingsBuffer;

        shaders.calculateAverageS->beginUpdate();
        shaders.calculateAverageS->setParameter<vk::ShaderParameterStorageBuffer>(1, 0, *indexesToTest);
        shaders.calculateAverageS->setParameter<vk::ShaderParameterStorageBuffer>(1, 1, *obbOrientationsBuf);
        shaders.calculateAverageS->setParameter<vk::ShaderParameterUniformBuffer>(1, 0, *sceneSettings);
        shaders.calculateAverageS->endUpdate();

        shaders.calculateDispersionBoundS->beginUpdate();
        shaders.calculateDispersionBoundS->setParameter<vk::ShaderParameterStorageBuffer>(1, 0, *indexesToTest);
        shaders.calculateDispersionBoundS->setParameter<vk::ShaderParameterStorageBuffer>(1, 1, *obbOrientationsBuf);
        shaders.calculateDispersionBoundS->setParameter<vk::ShaderParameterUniformBuffer>(1, 0, *sceneSettings);
        shaders.calculateDispersionBoundS->endUpdate();

        shaders.buildBoundS->beginUpdate();
        shaders.buildBoundS->setParameter<vk::ShaderParameterUniformBuffer>(1, 0, *sceneSettings);
        shaders.buildBoundS->endUpdate();

        shaders.checkContactsBoundS->beginUpdate();
        shaders.checkContactsBoundS->setParameter<vk::ShaderParameterStorageBuffer>(1, 0, *indexesToTest);
        shaders.checkContactsBoundS->setParameter<vk::ShaderParameterStorageBuffer>(1, 1, *obbOrientationsBuf);
        shaders.checkContactsBoundS->endUpdate();

        const uint32_t sizeGroupX = 16;
        uint32_t countGroups = countTestOBBs / sizeGroupX;
        if (countTestOBBs % sizeGroupX != 0)
            countGroups++;

        struct
        {
            uint32_t countOBBs;
        } pushConstants;

        pushConstants.countOBBs = countTestOBBs;

        gpuData->computeCommands->debugMarkerBegin("CalculateAveragePosition", PeDebugMarkersUtils::subPassColor());
        {
            gpuData->computeCommands->bindPipeline(shaders.calculateAverageP);
            gpuData->computeCommands->bindDescriptorSet(shaders.calculateAverageL, shaders.calculateAverageS->getCurrentSet(0), 0);
            gpuData->computeCommands->bindDescriptorSet(shaders.calculateAverageL, shaders.calculateAverageS->getCurrentSet(1), 1);
            gpuData->computeCommands->pushConstant(shaders.calculateAverageL, 0, sizeof(pushConstants), &pushConstants);

            PeGpuMemoryBuffer *buffs[] = {
                buffers.oAveragePos,
                indexesToTest,
                obbOrientationsBuf,
                sceneSettings};

            PeTypeAccessData buffsA[] = {
                PeTypeAccess_Write,
                PeTypeAccess_Read,
                PeTypeAccess_Read,
                PeTypeAccess_Read};

            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 4, buffs, buffsA, 0, NULL, NULL);

            gpuData->computeCommands->dispatch(countGroups, 1, 1);
        }
        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerBegin("CalculateDispersionBoundPosition", PeDebugMarkersUtils::subPassColor());
        {
            gpuData->computeCommands->bindPipeline(shaders.calculateDispersionBoundP);
            gpuData->computeCommands->bindDescriptorSet(shaders.calculateDispersionBoundL, shaders.calculateDispersionBoundS->getCurrentSet(0), 0);
            gpuData->computeCommands->bindDescriptorSet(shaders.calculateDispersionBoundL, shaders.calculateDispersionBoundS->getCurrentSet(1), 1);
            gpuData->computeCommands->pushConstant(shaders.calculateDispersionBoundL, 0, sizeof(pushConstants), &pushConstants);

            PeGpuMemoryBuffer *buffs[] = {
                buffers.oAveragePos,
                buffers.oDispersionSize,
                indexesToTest,
                obbOrientationsBuf,
                sceneSettings};

            PeTypeAccessData buffsA[] = {
                PeTypeAccess_Read,
                PeTypeAccess_Write,
                PeTypeAccess_Read,
                PeTypeAccess_Read,
                PeTypeAccess_Read};

            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 5, buffs, buffsA, 0, NULL, NULL);

            gpuData->computeCommands->dispatch(countGroups, 1, 1);
        }
        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerBegin("BuildBound", PeDebugMarkersUtils::subPassColor());
        {
            gpuData->computeCommands->bindPipeline(shaders.buildBoundP);
            gpuData->computeCommands->bindDescriptorSet(shaders.buildBoundL, shaders.buildBoundS->getCurrentSet(0), 0);
            gpuData->computeCommands->bindDescriptorSet(shaders.buildBoundL, shaders.buildBoundS->getCurrentSet(1), 1);

            PeGpuMemoryBuffer *buffs[] = {
                buffers.oAveragePos,
                buffers.oDispersionSize,
                buffers.oMainPlanes,
                sceneSettings};

            PeTypeAccessData buffsA[] = {
                PeTypeAccess_Read,
                PeTypeAccess_Read,
                PeTypeAccess_Write,
                PeTypeAccess_Read};

            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 4, buffs, buffsA, 0, NULL, NULL);

            gpuData->computeCommands->dispatch(1, 1, 1);
        }
        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerBegin("CheckContactsWithPlanesXYZ", PeDebugMarkersUtils::subPassColor());
        {
            gpuData->computeCommands->bindPipeline(shaders.checkContactsBoundP);
            gpuData->computeCommands->bindDescriptorSet(shaders.checkContactsBoundL, shaders.checkContactsBoundS->getCurrentSet(0), 0);
            gpuData->computeCommands->bindDescriptorSet(shaders.checkContactsBoundL, shaders.checkContactsBoundS->getCurrentSet(1), 1);
            gpuData->computeCommands->pushConstant(shaders.checkContactsBoundL, 0, sizeof(pushConstants), &pushConstants);

            PeGpuMemoryBuffer *buffs[] = {
                buffers.oMainPlanes,
                buffers.iPlaneCountContact,
                indexesToTest,
                obbOrientationsBuf};

            PeTypeAccessData buffsA[] = {
                PeTypeAccess_Read,
                PeTypeAccess_Write,
                PeTypeAccess_Read,
                PeTypeAccess_Read};

            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 4, buffs, buffsA, 0, NULL, NULL);

            gpuData->computeCommands->dispatch(countGroups, 1, 1);
        }
        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerBegin("RebalanceGlobalBound", PeDebugMarkersUtils::subPassColor());
        {
            gpuData->computeCommands->bindPipeline(shaders.rebalanceGlobalBoundP);
            gpuData->computeCommands->bindDescriptorSet(shaders.rebalanceGlobalBoundL, shaders.rebalanceGlobalBoundS->getCurrentSet(0), 0);

            PeGpuMemoryBuffer *buffs[] = {
                buffers.oMainPlanes,
                buffers.iPlaneCountContact,
                buffers.iGlobalBound,
            };

            PeTypeAccessData buffsA[] = {
                PeTypeAccess_Read,
                PeTypeAccess_Read,
                PeTypeAccess_Write,
            };

            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 3, buffs, buffsA, 0, NULL, NULL);

            gpuData->computeCommands->dispatch(1, 1, 1);

            gpuData->computeCommands->barrierBufferAccess(*buffers.iGlobalBound,
                                                          VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                                                          VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
        }
        gpuData->computeCommands->debugMarkerEnd();
    }
}