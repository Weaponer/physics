#include <vulkan/vulkan.h>

#include "physicsEngine/potentialContacts/peKdTreeBuilderSystem.h"
#include "physicsEngine/potentialContacts/pePotentialContactsStructs.h"

namespace phys
{
    PeKdTreeBuilderSystem::PeKdTreeBuilderSystem()
        : PeSystem(),
          updateGeometrySystem(NULL),
          globalBoundOBBsBuilderSystem(NULL),
          gpuData(NULL),
          shapeDescriptionSystem(NULL),
          shaderProvider(NULL),

          countWrotedBefore(0),
          readMemory(NULL),
          readCounterHeadPoints(NULL),

          countUseSteps(0),
          countTestOBBs(0)
    {
        for (uint32_t i = 0; i < PE_KD_TREE_BUILDER_MAX_ITERATIONS; i++)
        {
            buffers.oNodeCountOBBs[i] = NULL;
            buffers.oNodeDistanceOBBs[i] = NULL;
            buffers.iPreviosNodeOffsets[i] = NULL;
            buffers.iPreviosActiveNodes[i] = NULL;
        }

        for (uint32_t i = 0; i <= PE_KD_TREE_BUILDER_MAX_ITERATIONS; i++)
        {
            buffers.oIndexesKdBuffer[i] = NULL;
            buffers.countOBBsInIndexesKdBuffer[i] = 0;
        }

        for (uint32_t i = 0; i < PE_KD_TREE_BUILDER_MAX_ITERATIONS; i++)
        {
            buffers.sizeInputImages[i] = getSizeInputKdTreeNodes(i);
            buffers.sizeOutputImages[i] = getSizeOutputKdTreeNodes(i);

            buffers.oNodeCountOBBs[i] = NULL;
            buffers.oNodeDistanceOBBs[i] = NULL;
            buffers.iPreviosNodeOffsets[i] = NULL;
        }

        buffers.maxPreparedStep = 0;
    }

    void PeKdTreeBuilderSystem::onInit()
    {
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();

        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();
        updateGeometrySystem = simulationContext->get<PeUpdateGeometryOBBsSystem>();
        globalBoundOBBsBuilderSystem = simulationContext->get<PeGlobalBoundOBBsBuilderSystem>();
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();

        const char *nameKdTreeNodeBuilder = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_KdTreeNodeBuilder);
        const char *nameKdTreeNodeOffsetsBuilder = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_KdTreeNodeOffsetsBuilder);

        shaders.kdTreeNodeBuilderP = shaderProvider->getPipeline(nameKdTreeNodeBuilder);
        shaders.kdTreeNodeBuilderL = shaderProvider->getPipelineLayout(nameKdTreeNodeBuilder);
        shaders.kdTreeNodeBuilderS = shaderProvider->createShaderParameters(nameKdTreeNodeBuilder, PE_KD_TREE_BUILDER_MAX_ITERATIONS);

        shaders.kdTreeNodeOffsetsBuilderP = shaderProvider->getPipeline(nameKdTreeNodeOffsetsBuilder);
        shaders.kdTreeNodeOffsetsBuilderL = shaderProvider->getPipelineLayout(nameKdTreeNodeOffsetsBuilder);
        shaders.kdTreeNodeOffsetsBuilderS = shaderProvider->createShaderParameters(nameKdTreeNodeOffsetsBuilder, PE_KD_TREE_BUILDER_MAX_ITERATIONS);

        buffers.oCounterHeadPoints = PH_NEW(PeGpuImage)("CounterHeadPointsCounter");
        buffers.oCounterHeadPoints->init(gpuData);
        buffers.oCounterHeadPoints->allocateData(VK_IMAGE_TYPE_1D, VK_FORMAT_R32_UINT, PE_KD_TREE_BUILDER_MAX_ITERATIONS, 1, 1, false);

        buffers.oDispatchDataBuffer = PH_NEW(PeGpuBuffer)("DispatchDataBuffer", gpuData, vk::MemoryUseType_Long,
                                                          vk::VulkanBufferType_UniformStorageBufferStatic, sizeof(PeGpuDataDispatchIndirect));

        buffers.iIndirectDispatchBuffer = PH_NEW(PeGpuBuffer)("IndirectDispatchBuffer", gpuData, vk::MemoryUseType_Long,
                                                              vk::VulkanBufferType_IndirectBufferStatic, sizeof(PeGpuDataDispatchIndirect));

        readCounterHeadPoints = staticReader->createReadImageMemoryPoint(*buffers.oCounterHeadPoints);
    }

    void PeKdTreeBuilderSystem::release()
    {
        staticReader->destroyReadImageMemoryPoint(readCounterHeadPoints);

        if (readMemory)
            staticReader->destroyReadMemoryPoint(readMemory);

        shaderProvider->destroyShaderAutoParameters(shaders.kdTreeNodeOffsetsBuilderS);
        shaderProvider->destroyShaderAutoParameters(shaders.kdTreeNodeBuilderS);

        PH_RELEASE(buffers.oCounterHeadPoints);
        PH_RELEASE(buffers.oDispatchDataBuffer);
        PH_RELEASE(buffers.iIndirectDispatchBuffer);

        for (uint32_t i = 0; i < buffers.maxPreparedStep; i++)
        {
            PH_RELEASE(buffers.oNodeCountOBBs[i]);
            PH_RELEASE(buffers.oNodeDistanceOBBs[i]);
            PH_RELEASE(buffers.iPreviosNodeOffsets[i]);
            PH_RELEASE(buffers.iPreviosActiveNodes[i]);
            PH_RELEASE(buffers.oIndexesKdBuffer[i + 1]);
        }

        if (buffers.oIndexesKdBuffer[0])
            PH_RELEASE(buffers.oIndexesKdBuffer[0]);
    }

    void PeKdTreeBuilderSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        memoryBarrierController->initUnknowMemory(buffers.oCounterHeadPoints);
        memoryBarrierController->initUnknowMemory(buffers.oDispatchDataBuffer);
        memoryBarrierController->initUnknowMemory(buffers.iIndirectDispatchBuffer);
        for (uint32_t i = 0; i < buffers.maxPreparedStep; i++)
        {
            memoryBarrierController->initUnknowMemory(buffers.oNodeCountOBBs[i]);
            memoryBarrierController->initUnknowMemory(buffers.oNodeDistanceOBBs[i]);
            memoryBarrierController->initUnknowMemory(buffers.iPreviosNodeOffsets[i]);
            memoryBarrierController->initUnknowMemory(buffers.iPreviosActiveNodes[i]);
            memoryBarrierController->initUnknowMemory(buffers.oIndexesKdBuffer[i + 1]);
        }

        if (buffers.oIndexesKdBuffer[0])
            memoryBarrierController->initUnknowMemory(buffers.oIndexesKdBuffer[0]);

        gpuData->computeCommands->debugMarkerBegin("KdTreePrepareData", PeDebugMarkersUtils::passCallColor());

        // std::cout << std::endl;
        //   read last data
        if (readCounterHeadPoints->IsValid())
        {
            uint32_t *pRead = (uint32_t *)readCounterHeadPoints->getMemory();
            for (uint32_t i = 0; i < countUseSteps; i++)
            {
                uint32_t countUsed = pRead[i];
                updateStepCapacity(i, countUsed);
                // std::cout << i << " : " << countUsed << std::endl;
                countWrotedBefore = countUsed;
            }
        }
        // std::cout << std::endl;

        if (readMemory)
        {
            if (readMemory->IsValid())
            {
                PeGpuDataIndexKeyOBB *pRead = (PeGpuDataIndexKeyOBB *)readMemory->getMemory();
                uint32_t countRead = std::min(countWrotedBefore, (uint32_t)(readMemory->getSize() / sizeof(PeGpuDataIndexKeyOBB)));
                for (uint i = 0; i < countRead; i++)
                {
                    PeGpuDataIndexKeyOBB key = pRead[i];
                    _simulationData->outputDebugData.addKeyOBB(key.index, key.key);
                }
            }

            staticReader->destroyReadMemoryPoint(readMemory);
            readMemory = NULL;
        }

        // update buffers
        uint32_t countOBBs = updateGeometrySystem->getSizeDataOBBsToTestOnlySizableContacts();
        uint32_t countSteps = getNeedCountSteps(countOBBs);

        countUseSteps = countSteps;
        countTestOBBs = countOBBs;

        prepareMemoryForSteps(countSteps, countOBBs);

        if (countTestOBBs == 0)
        {
            gpuData->computeCommands->debugMarkerEnd();
            return;
        }

        // clear images
        VkClearColorValue value{};
        clearImage(buffers.oCounterHeadPoints, value);

        if (buffers.maxPreparedStep != 0)
            clearImage(buffers.iPreviosNodeOffsets[0], value);

        if (buffers.maxPreparedStep != 0)
        {
            VkClearColorValue activeValueClear{~0};
            clearImage(buffers.iPreviosActiveNodes[0], activeValueClear);
        }
        for (uint32_t i = 0; i < buffers.maxPreparedStep; i++)
        {
            clearImage(buffers.oNodeCountOBBs[i], value);
            clearImage(buffers.oNodeDistanceOBBs[i], value);
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeKdTreeBuilderSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("KdTreeSimulation", PeDebugMarkersUtils::passCallColor());
        {
            buildKdTree(_simulationData);
        }
        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeKdTreeBuilderSystem::buildKdTree(PeSceneSimulationData *_simulationData)
    {
        vk::VulkanBuffer *startIndexesBuffer = *buffers.oIndexesKdBuffer[0];
        PeGpuDataIndexKeyOBB *pWrite = (PeGpuDataIndexKeyOBB *)gpuData->computeCommands->beginWrite(startIndexesBuffer, 0, startIndexesBuffer->getSize());
        const uint32_t *indexOBBs = updateGeometrySystem->getDataOBBsToTestOnlySizableContacts();
        for (uint32_t i = 0; i < countTestOBBs; i++)
        {
            PeGpuDataIndexKeyOBB key{};
            key.index = indexOBBs[i];
            pWrite[i] = key;
        }
        gpuData->computeCommands->endWrite(startIndexesBuffer);

        VkDescriptorSet setsKdTreeBuilder[countUseSteps];
        VkDescriptorSet setsKdTreeOffsetsBuilder[countUseSteps];
        buildShaderParameters(_simulationData, countUseSteps, setsKdTreeBuilder, setsKdTreeOffsetsBuilder);

        //
        {
            const uint32_t countImgs = 1 + countUseSteps * 4;
            PeGpuMemoryImage *imgs[countImgs];
            PeTypeAccessData imgsA[countImgs];
            imgs[0] = buffers.oCounterHeadPoints;
            imgsA[0] = PeTypeAccess_Write;
            uint32_t offset = 1;
            for (uint32_t i = 0; i < countUseSteps; i++)
            {
                imgs[offset + i] = buffers.oNodeCountOBBs[i];
                imgsA[offset + i] = PeTypeAccess_Write;
            }
            offset += countUseSteps;
            for (uint32_t i = 0; i < countUseSteps; i++)
            {
                imgs[offset + i] = buffers.oNodeDistanceOBBs[i];
                imgsA[offset + i] = PeTypeAccess_Write;
            }
            offset += countUseSteps;
            for (uint32_t i = 0; i < countUseSteps; i++)
            {
                imgs[offset + i] = buffers.iPreviosNodeOffsets[i];
                imgsA[offset + i] = PeTypeAccess_Read;
            }
            offset += countUseSteps;
            for (uint32_t i = 0; i < countUseSteps; i++)
            {
                imgs[offset + i] = buffers.iPreviosActiveNodes[i];
                imgsA[offset + i] = PeTypeAccess_Read;
            }

            const uint32_t countBuffs = 2;
            PeGpuMemoryBuffer *buffs[countBuffs] = {
                globalBoundOBBsBuilderSystem->getOutputGlobalBoundBuffer(),
                _simulationData->geometryOBBs.geometryOrientationObbsBuffer,
            };

            PeTypeAccessData buffsA[countBuffs] = {
                PeTypeAccess_Read,
                PeTypeAccess_Read,
            };

            memoryBarrierController->setupBarrier(PeTypeStage_Compute, countBuffs, buffs, buffsA, countImgs, imgs, imgsA);
        }
        struct
        {
            uint32_t indexIteration;
            uint32_t countStartOBBs;
            uint32_t sizeInIndexes;
            uint32_t sizeOutIndexes;
        } kdTreeBuilderConst;

        struct
        {
            uint32_t indexIteration;
            uint32_t maxSizeOutIndexes;
        } kdTreeOffsetsBuilderConst;


        for (uint32_t i = 0; i < countUseSteps; i++)
        {
            kdTreeBuilderConst.indexIteration = i;
            kdTreeBuilderConst.countStartOBBs = countTestOBBs;
            kdTreeBuilderConst.sizeInIndexes = buffers.countOBBsInIndexesKdBuffer[i];
            kdTreeBuilderConst.sizeOutIndexes = buffers.countOBBsInIndexesKdBuffer[i + 1];

            bool last = i == countUseSteps - 1;

            kdTreeOffsetsBuilderConst.indexIteration = i;
            kdTreeOffsetsBuilderConst.maxSizeOutIndexes = kdTreeBuilderConst.sizeOutIndexes;

            gpuData->computeCommands->debugMarkerBegin("BuildKdTreeNode", PeDebugMarkersUtils::subPassColor());
            {
                gpuData->computeCommands->bindPipeline(shaders.kdTreeNodeBuilderP);
                gpuData->computeCommands->bindDescriptorSet(shaders.kdTreeNodeBuilderL, setsKdTreeBuilder[i], 0);
                gpuData->computeCommands->pushConstant(shaders.kdTreeNodeBuilderL, 0, sizeof(kdTreeBuilderConst), &kdTreeBuilderConst);

                buildBarrierKdTreeNodeBuilder(_simulationData, i);

                if (i == 0)
                {
                    uint32_t countGroupX = countTestOBBs / 128;
                    if ((countTestOBBs % 128) != 0)
                        countGroupX++;

                    gpuData->computeCommands->dispatch(countGroupX, 1, 1);
                }
                else
                {
                    {
                        memoryBarrierController->setBuffer(0, buffers.oDispatchDataBuffer, PeTypeAccess_IndirectCallRead);
                        memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
                    }

                    gpuData->computeCommands->dispatchIndirect(*buffers.iIndirectDispatchBuffer, 0);
                }
            }
            gpuData->computeCommands->debugMarkerEnd();

            if (last)
                break;

            gpuData->computeCommands->debugMarkerBegin("SetupBarriersKdTree", PeDebugMarkersUtils::subPassColor());
            {
                gpuData->computeCommands->bindPipeline(shaders.kdTreeNodeOffsetsBuilderP);
                gpuData->computeCommands->bindDescriptorSet(shaders.kdTreeNodeOffsetsBuilderL, setsKdTreeOffsetsBuilder[i], 0);
                gpuData->computeCommands->pushConstant(shaders.kdTreeNodeOffsetsBuilderL, 0,
                                                       sizeof(kdTreeOffsetsBuilderConst), &kdTreeOffsetsBuilderConst);

                buildBarrierKdTreeNodeOffsetsBuilder(_simulationData, i);

                gpuData->computeCommands->dispatch(buffers.sizeOutputImages[i].x / 2, buffers.sizeOutputImages[i].y, 1);

                {
                    memoryBarrierController->setBuffer(0, buffers.oDispatchDataBuffer, PeTypeAccess_Write);
                    memoryBarrierController->setBuffer(1, buffers.iIndirectDispatchBuffer, PeTypeAccess_Read);
                    memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 2, 0);
                }

                gpuData->computeCommands->copyBuffer(*buffers.oDispatchDataBuffer, 0, *buffers.iIndirectDispatchBuffer, 0,
                                                     buffers.oDispatchDataBuffer->getSize());

            }
            gpuData->computeCommands->debugMarkerEnd();
        }
    }

    void PeKdTreeBuilderSystem::buildShaderParameters(PeSceneSimulationData *_simulationData,
                                                      uint32_t _countSteps, VkDescriptorSet *_outSetsTreeBuilder, VkDescriptorSet *_outSetsTreeOffsetsBuilder)
    {
        shaders.kdTreeNodeBuilderS->clearParameters();
        shaders.kdTreeNodeBuilderS->reset();
        shaders.kdTreeNodeOffsetsBuilderS->clearParameters();
        shaders.kdTreeNodeOffsetsBuilderS->reset();

        for (uint32_t i = 0; i < _countSteps; i++)
        {
            shaders.kdTreeNodeBuilderS->beginUpdate();
            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.oNodeCountOBBs[i]);
            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *buffers.oNodeDistanceOBBs[i]);
            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 2, *buffers.iPreviosNodeOffsets[i]);
            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 3, *buffers.iPreviosActiveNodes[i]);
            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 4, *buffers.oCounterHeadPoints);

            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *globalBoundOBBsBuilderSystem->getOutputGlobalBoundBuffer());
            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.oIndexesKdBuffer[i]);
            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2,
                                                                                       *_simulationData->geometryOBBs.geometryOrientationObbsBuffer);
            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *buffers.oIndexesKdBuffer[i + 1]);

            shaders.kdTreeNodeBuilderS->setParameter<vk::ShaderParameterUniformBuffer>(0, 0, *_simulationData->sceneSettingsBuffer);

            shaders.kdTreeNodeBuilderS->endUpdate();
            *(_outSetsTreeBuilder + i) = shaders.kdTreeNodeBuilderS->getCurrentSet(0);

            if (i != _countSteps - 1)
            {
                shaders.kdTreeNodeBuilderS->switchToNextSet(0);
            }
        }

        for (uint32_t i = 0; i < _countSteps - 1; i++)
        {
            shaders.kdTreeNodeOffsetsBuilderS->beginUpdate();
            if (i == 0)
                shaders.kdTreeNodeOffsetsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.oNodeCountOBBs[i]);
            else
                shaders.kdTreeNodeOffsetsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *buffers.oNodeCountOBBs[i - 1]);

            shaders.kdTreeNodeOffsetsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 1, *buffers.oNodeCountOBBs[i]);
            shaders.kdTreeNodeOffsetsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 2, *buffers.oNodeDistanceOBBs[i]);
            shaders.kdTreeNodeOffsetsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 3, *buffers.iPreviosNodeOffsets[i + 1]);
            shaders.kdTreeNodeOffsetsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 4, *buffers.iPreviosActiveNodes[i + 1]);
            shaders.kdTreeNodeOffsetsBuilderS->setParameter<vk::ShaderParameterStorageImage>(0, 5, *buffers.oCounterHeadPoints);

            shaders.kdTreeNodeOffsetsBuilderS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.oDispatchDataBuffer);

            shaders.kdTreeNodeOffsetsBuilderS->setParameter<vk::ShaderParameterUniformBuffer>(0, 0, *_simulationData->sceneSettingsBuffer);
            shaders.kdTreeNodeOffsetsBuilderS->endUpdate();

            *(_outSetsTreeOffsetsBuilder + i) = shaders.kdTreeNodeOffsetsBuilderS->getCurrentSet(0);
            shaders.kdTreeNodeOffsetsBuilderS->switchToNextSet(0);
        }
    }

    // see buildShaderParameters
    void PeKdTreeBuilderSystem::buildBarrierKdTreeNodeBuilder(PeSceneSimulationData *_simulationData, uint32_t _step)
    {
        const uint32_t countImgs = 5;
        PeGpuMemoryImage *imgs[countImgs] = {
            buffers.oNodeCountOBBs[_step],
            buffers.oNodeDistanceOBBs[_step],
            buffers.iPreviosNodeOffsets[_step],
            buffers.iPreviosActiveNodes[_step],
            buffers.oCounterHeadPoints,
        };
        PeTypeAccessData imgsA[countImgs] = {
            PeTypeAccess_Write,
            PeTypeAccess_Write,
            PeTypeAccess_Read,
            PeTypeAccess_Read,
            PeTypeAccess_Write,
        };

        const uint32_t countBuffs = 4;
        PeGpuMemoryBuffer *buffs[countBuffs] = {
            globalBoundOBBsBuilderSystem->getOutputGlobalBoundBuffer(),
            buffers.oIndexesKdBuffer[_step],
            _simulationData->geometryOBBs.geometryOrientationObbsBuffer,
            buffers.oIndexesKdBuffer[_step + 1],
        };
        PeTypeAccessData buffsA[countBuffs] = {
            PeTypeAccess_Read,
            PeTypeAccess_Read,
            PeTypeAccess_Read,
            PeTypeAccess_Write,
        };
        memoryBarrierController->setupBarrier(PeTypeStage_Compute, countBuffs, buffs, buffsA, countImgs, imgs, imgsA);
    }

    void PeKdTreeBuilderSystem::buildBarrierKdTreeNodeOffsetsBuilder(PeSceneSimulationData *_simulationData, uint32_t _step)
    {
        const uint32_t countImgs = 6;

        uint32_t indexNodeCount = _step == 0 ? _step : _step - 1;
        PeGpuMemoryImage *imgs[countImgs] = {
            buffers.oNodeCountOBBs[indexNodeCount],
            buffers.oNodeCountOBBs[_step],
            buffers.oNodeDistanceOBBs[_step],
            buffers.iPreviosNodeOffsets[_step],
            buffers.iPreviosActiveNodes[_step],
            buffers.oCounterHeadPoints,
        };
        PeTypeAccessData imgsA[countImgs] = {
            PeTypeAccess_Read,
            PeTypeAccess_Read,
            PeTypeAccess_Read,
            PeTypeAccess_Write,
            PeTypeAccess_Write,
            PeTypeAccess_Read,
        };

        PeGpuMemoryBuffer *buff = buffers.oDispatchDataBuffer;
        PeTypeAccessData buffA = PeTypeAccess_Write;
        memoryBarrierController->setupBarrier(PeTypeStage_Compute, 1, &buff, &buffA, countImgs, imgs, imgsA);
    }

    void PeKdTreeBuilderSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        if (countTestOBBs)
        {
            gpuData->computeCommands->debugMarkerBegin("KdTreeBuilderReceive", PeDebugMarkersUtils::passCallColor());

            //readMemory = staticReader->createReadMemoryPoint(buffers.oIndexesKdBuffer[countUseSteps]->getSize());
            //staticReader->addRequest(*buffers.oIndexesKdBuffer[countUseSteps], 0, readMemory, 0, readMemory->getSize());

            gpuData->computeCommands->barrierImageAccess(*buffers.oCounterHeadPoints,
                                                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                                         VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                                                         VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT);

            staticReader->addRequest(*buffers.oCounterHeadPoints, readCounterHeadPoints, 0);

            gpuData->computeCommands->debugMarkerEnd();
        }
    }

    PeGpuBuffer *PeKdTreeBuilderSystem::createIndexesKdBuffer(uint32_t _step, uint32_t _countOBBs)
    {
        uint32_t size = sizeof(PeGpuDataIndexKeyOBB) * _countOBBs;
        size = std::min((uint32_t)gpuData->bufferAllocator->getMemoryController()->getSizePage(vk::MemoryUseType_Long), size);
        if (_step == 0)
        {
            return PH_NEW(PeGpuBuffer)("IndexesKdBuffer", gpuData, vk::MemoryUseType_Long,
                                       vk::VulkanBufferType_UniformStorageBufferDynamic, size);
        }
        else
        {
            return PH_NEW(PeGpuBuffer)("IndexesKdBuffer", gpuData, vk::MemoryUseType_Long,
                                       vk::VulkanBufferType_UniformStorageBufferStatic, size);
        }
    }

    void PeKdTreeBuilderSystem::prepareMemoryForSteps(uint32_t _steps, uint32_t _startCountOBBs)
    {
        ensureBufferKdTree(_steps, _startCountOBBs);        
        ensureBufferKdTreeNodes(_steps);

        buffers.maxPreparedStep = std::max(_steps, buffers.maxPreparedStep);
    }

    void PeKdTreeBuilderSystem::ensureBufferKdTree(uint32_t _steps, uint32_t _startCountOBBs)
    {
        uint32_t sizes[PE_KD_TREE_BUILDER_MAX_ITERATIONS + 1];
        for (uint32_t i = 0; i < PE_KD_TREE_BUILDER_MAX_ITERATIONS + 1; i++)
        {
            uint32_t step = i;
            sizes[i] = getNeedCountInSteps(step, _startCountOBBs);
        }

        for (uint32_t i = 0; i <= _steps; i++)
        {
            if (buffers.maxPreparedStep >= i && buffers.countOBBsInIndexesKdBuffer[i] != 0)
            {
                if (buffers.countOBBsInIndexesKdBuffer[i] < sizes[i])
                {
                    PH_RELEASE(buffers.oIndexesKdBuffer[i]);
                    buffers.oIndexesKdBuffer[i] = createIndexesKdBuffer(i, sizes[i]);
                }
            }
            else
            {
                buffers.oIndexesKdBuffer[i] = createIndexesKdBuffer(i, sizes[i]);
            }

            buffers.countOBBsInIndexesKdBuffer[i] = std::max(buffers.countOBBsInIndexesKdBuffer[i], sizes[i]);
        }
    }

    void PeKdTreeBuilderSystem::ensureBufferKdTreeNodes(uint32_t _steps)
    {
        for (uint32_t i = 0; i < _steps; i++)
        {
            if (buffers.maxPreparedStep <= i)
            {
                mth::Vector2Int inSize = buffers.sizeInputImages[i];
                mth::Vector2Int outSize = buffers.sizeOutputImages[i];

                buffers.oNodeCountOBBs[i] = PH_NEW(PeGpuImage)("NodeCountOBBsImage");
                buffers.oNodeCountOBBs[i]->init(gpuData);
                buffers.oNodeCountOBBs[i]->allocateData(VK_IMAGE_TYPE_2D, VK_FORMAT_R32_UINT, outSize.x, outSize.y, 1, false);

                buffers.oNodeDistanceOBBs[i] = PH_NEW(PeGpuImage)("NodeDistanceOBBsImage");
                buffers.oNodeDistanceOBBs[i]->init(gpuData);
                buffers.oNodeDistanceOBBs[i]->allocateData(VK_IMAGE_TYPE_2D, VK_FORMAT_R32_SINT, outSize.x, outSize.y, 1, false);

                buffers.iPreviosNodeOffsets[i] = PH_NEW(PeGpuImage)("PreviosNodeOffsetsImage");
                buffers.iPreviosNodeOffsets[i]->init(gpuData);
                buffers.iPreviosNodeOffsets[i]->allocateData(VK_IMAGE_TYPE_2D, VK_FORMAT_R32_SFLOAT, inSize.x, inSize.y, 1, false);

                buffers.iPreviosActiveNodes[i] = PH_NEW(PeGpuImage)("PreviosActiveNodesImage");
                buffers.iPreviosActiveNodes[i]->init(gpuData);
                buffers.iPreviosActiveNodes[i]->allocateData(VK_IMAGE_TYPE_2D, VK_FORMAT_R8_UINT, inSize.x, inSize.y, 1, false);
            }
        }
    }

    void PeKdTreeBuilderSystem::updateStepCapacity(uint32_t _step, uint32_t _countOBBs)
    {
        uint32_t indexBuff = _step + 1;
        if (buffers.countOBBsInIndexesKdBuffer[indexBuff] < _countOBBs)
        {
            PH_RELEASE(buffers.oIndexesKdBuffer[indexBuff]);

            buffers.oIndexesKdBuffer[indexBuff] = createIndexesKdBuffer(indexBuff, _countOBBs);
            buffers.countOBBsInIndexesKdBuffer[indexBuff] = _countOBBs;
        }
    }

    uint32_t PeKdTreeBuilderSystem::getNeedCountSteps(uint32_t _countOBBs)
    {
        if (_countOBBs == 0)
            return 1;

        return std::min(std::max(1U, (uint32_t)ceil(log2(_countOBBs))), PE_KD_TREE_BUILDER_MAX_ITERATIONS);
    }

    uint32_t PeKdTreeBuilderSystem::getNeedCountInSteps(uint32_t _step, uint32_t _countOBBs)
    {
        float resCount = pow(_step + 1, 1.0f / 2.0f) * _countOBBs;
        return std::max((uint32_t)resCount, 16U);
    }

    mth::Vector2Int PeKdTreeBuilderSystem::getSizeOutputKdTreeNodes(uint32_t _step)
    {
        return getSizeInputKdTreeNodes(_step + 1);
    }

    mth::Vector2Int PeKdTreeBuilderSystem::getSizeInputKdTreeNodes(uint32_t _step)
    {
        if (_step == 0)
            return mth::Vector2Int(1, 1);

        mth::Vector2Int vec = mth::Vector2Int();
        vec.x = (int)pow(2, (uint32_t)ceil(_step / 2.0f));
        vec.y = (int)pow(2, (uint32_t)floor(_step / 2.0f));
        return vec;
    }

    void PeKdTreeBuilderSystem::clearImage(PeGpuImage *_image, VkClearColorValue &_value)
    {
        gpuData->computeCommands->barrierImageAccess(*_image, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                                     VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT);
        gpuData->computeCommands->clearImage(*_image, &_value);
    }
}
