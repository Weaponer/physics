#include "physicsEngine/potentialContacts/pePotentialContactsSystem.h"

namespace phys
{
    PePotentialContactsSystem::PePotentialContactsSystem()
        : PeSystem(),
          potentialContactsContext(),
          updateGeometryOBBs(),
          updateOrientationSystem(),
          globalBoundOBBsBuilderSystem(),
          kdTreeBuilderSystem(),
          listCheckContactsBuilderSystem(),
          gpu(NULL)
    {
    }

    void PePotentialContactsSystem::onInit()
    {
        potentialContactsContext.setParent(simulationContext);
        potentialContactsContext.bind(&updateGeometryOBBs);
        potentialContactsContext.bind(&updateOrientationSystem);
        potentialContactsContext.bind(&globalBoundOBBsBuilderSystem);
        potentialContactsContext.bind(&kdTreeBuilderSystem);
        potentialContactsContext.bind(&listCheckContactsBuilderSystem);

        updateGeometryOBBs.init(&potentialContactsContext);
        updateOrientationSystem.init(&potentialContactsContext);
        globalBoundOBBsBuilderSystem.init(&potentialContactsContext);
        kdTreeBuilderSystem.init(&potentialContactsContext);
        listCheckContactsBuilderSystem.init(&potentialContactsContext);

        gpu = simulationContext->get<PeGpuData>();
    }

    void PePotentialContactsSystem::release()
    {
        listCheckContactsBuilderSystem.release();
        kdTreeBuilderSystem.release();
        globalBoundOBBsBuilderSystem.release();
        updateOrientationSystem.release();
        updateGeometryOBBs.release();
        potentialContactsContext.release();
    }

    void PePotentialContactsSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("PotentialContactsPrepareData", PeDebugMarkersUtils::subGroupColor());

        updateGeometryOBBs.prepareListUpdateGeometries(_simulationData);

        globalBoundOBBsBuilderSystem.prepareData(_simulationData);
        kdTreeBuilderSystem.prepareData(_simulationData);
        listCheckContactsBuilderSystem.prepareData(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    void PePotentialContactsSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("PotentialContactsSimulation", PeDebugMarkersUtils::subGroupColor());

        updateOrientationSystem.buildPipeline(_simulationData);

        gpu->computeCommands->pipelineBarrier(VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT);
        globalBoundOBBsBuilderSystem.buildPipeline(_simulationData);
        gpu->computeCommands->pipelineBarrier(VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT);
        kdTreeBuilderSystem.buildPipeline(_simulationData);
        gpu->computeCommands->pipelineBarrier(VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT);
        listCheckContactsBuilderSystem.buildPipeline(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    void PePotentialContactsSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("PotentialContactsReceive", PeDebugMarkersUtils::subGroupColor());

        updateOrientationSystem.buildReceive(_simulationData);
        globalBoundOBBsBuilderSystem.buildReceive(_simulationData);
        kdTreeBuilderSystem.buildReceive(_simulationData);
        listCheckContactsBuilderSystem.buildReceive(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    PePotentialContactsSystem::ResultStage PePotentialContactsSystem::getResultStage()
    {
        ResultStage resutl;
        resutl.maxIndexOBB = updateGeometryOBBs.getMaxIndexOBB();
        resutl.countTestOBBs = updateGeometryOBBs.getSizeDataOBBsToTestContacts();
        resutl.indexPlaneGeomtery = updateGeometryOBBs.getIndexOfPlaneGeometry();
        resutl.countIndexesToUpdate = updateGeometryOBBs.getSizeDataOBBsToUpdate();
        resutl.countIndexesToTest = updateGeometryOBBs.getSizeDataOBBsToTestContacts();
        resutl.indexesToUpdateBuffer = updateGeometryOBBs.getIndexesToUpdateBuffer();
        resutl.indexesToTestBuffer = updateGeometryOBBs.getIndexesToTestBuffer();

        resutl.maxCountSegments = listCheckContactsBuilderSystem.getMaxCountSegments();
        resutl.maxCountIndexes = listCheckContactsBuilderSystem.getMaxCountSegments();

        resutl.indexesBuffer = listCheckContactsBuilderSystem.getIndexesBuffer();
        resutl.segmentsBuffer = listCheckContactsBuilderSystem.getSegmentsBuffer();

        resutl.indexesCounter = listCheckContactsBuilderSystem.getIndexesCounterImage();
        resutl.segmentsCounter = listCheckContactsBuilderSystem.getSegmentsCounterImage();

        return resutl;
    }
}