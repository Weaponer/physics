#include "physicsEngine/peSceneSettingsSystem.h"

#include "physicsEngine/peGpuStructures.h"

namespace phys
{
    PeSceneSettingsSystem::PeSceneSettingsSystem()
        : PeSystem(),
          gpu(NULL),
          sceneSettings(),
          simulationSettings()
    {
    }

    void PeSceneSettingsSystem::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
    }

    void PeSceneSettingsSystem::release()
    {
    }

    void PeSceneSettingsSystem::prepareData(const PhSceneSettings *_settings, const PhSimulationSettings &_simSettings, 
                                            PeSceneSimulationData *_simulationData, float _deltaTime)
    {
        sceneSettings = *_settings;
        simulationSettings = _simSettings;

        gpu->computeCommands->debugMarkerBegin("Physics.PrerapeSceneSettings", PeDebugMarkersUtils::mainSystemColor());

        if (!_simulationData->sceneSettingsBuffer)
        {
            PeGpuObjectType type = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataSceneSettings>(1U, false, 16U);
            _simulationData->sceneSettingsBuffer = PH_NEW(PeGpuObjectBuffer)("SceneSettingBuffer");
            _simulationData->sceneSettingsBuffer->initType(gpu, &type);
            _simulationData->sceneSettingsBuffer->setHostVisibility(true);
            _simulationData->sceneSettingsBuffer->allocateData();
        }
        PeGpuDataSceneSettings settings{};
        settings.gravitation = _settings->gravitation;
        settings.deltaTime = _deltaTime;
        settings.depthContactMin = _simSettings.depthContactMin;
        settings.depthContactMax = _simSettings.depthContactMax;
        settings.linearDamping = _simSettings.linearDamping;
        settings.angularDamping = _simSettings.angularDamping;
        
        if(_simSettings.solveMode == _simSettings.SolverMode_LCP)
        {
            settings.damping = _simSettings.dampingLCP;
            settings.stiffness = _simSettings.stiffnessLCP;
        }
        else
        {
            settings.damping = 0.0f;
            settings.stiffness = _simSettings.stiffnessPBD;
        }
        settings.maxVelocitySleep = 0.2f;
        settings.maxAngularVelocitySleep = 0.2f;//(1.0f / 180.0f) * PI;
        settings.minSleepTime = 0.8f;

        settings.frictionStaticLimit = 0.01f;
        settings.percision = (uint32_t)_settings->percision;
        settings.updateVelocities = _simSettings.updateVelocities ? UINT32_MAX : 0;
        gpu->computeCommands->writeToBuffer(*_simulationData->sceneSettingsBuffer, 0, sizeof(PeGpuDataSceneSettings), &settings);

        gpu->computeCommands->debugMarkerEnd();
    }
}