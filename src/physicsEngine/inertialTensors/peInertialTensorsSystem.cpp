#include "physicsEngine/inertialTensors/peInertialTensorsSystem.h"

namespace phys
{
    PeInertialTensorsSystem::PeInertialTensorsSystem()
        : PeSystem(),
          inertialTensorsContext(),
          generateInertialTensorsSystem(),
          gpu(NULL)
    {
    }

    void PeInertialTensorsSystem::onInit()
    {
        inertialTensorsContext.setParent(simulationContext);
        inertialTensorsContext.bind(&generateInertialTensorsSystem);

        generateInertialTensorsSystem.init(&inertialTensorsContext);
        gpu = simulationContext->get<PeGpuData>();
    }

    void PeInertialTensorsSystem::release()
    {
        generateInertialTensorsSystem.release();
        inertialTensorsContext.release();
    }

    void PeInertialTensorsSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("InertialTensorsPrepareData", PeDebugMarkersUtils::subGroupColor());
        generateInertialTensorsSystem.prepareData(_simulationData);
        gpu->computeCommands->debugMarkerEnd();
    }

    void PeInertialTensorsSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("InertialTensorsSimulation", PeDebugMarkersUtils::subGroupColor());
        generateInertialTensorsSystem.buildPipeline(_simulationData);
        gpu->computeCommands->debugMarkerEnd();
    }

    void PeInertialTensorsSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("InertialTensorsReceive", PeDebugMarkersUtils::subGroupColor());
        generateInertialTensorsSystem.buildReceive(_simulationData);
        gpu->computeCommands->debugMarkerEnd();
    }
}