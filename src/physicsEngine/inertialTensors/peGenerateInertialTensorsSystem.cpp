#include "physicsEngine/inertialTensors/peGenerateInertialTensorsSystem.h"
#include "physicsEngine/common/peUtilities.h"

#include "physics/internal/inRigidDynamic.h"

namespace phys
{
    PeGenerateInertialTensorsSystem::PeGenerateInertialTensorsSystem()
        : PeSystem(),
          gpuData(NULL),
          staticReader(NULL),
          shaderProvider(NULL),
          memoryBarrierController(NULL),
          reallocateDatasSystem(NULL),
          actorDynamicDescription(NULL),
          shapeDescriptionDescription(NULL),
          countElementsInBuffers(1024),
          countIterations(0),
          countUpdate(0),
          pReadTensors(NULL)
    {
    }

    void PeGenerateInertialTensorsSystem::onInit()
    {
        for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
        {
            geometryListsCaches[i] = PH_NEW(PhEasyStack<PeGpuGeometriesDataForTensors>)();
            indexesGeometriesCaches[i] = PH_NEW(PhEasyStack<PeGpuIndexToGeometry>)();
        }
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        actorDynamicDescription = simulationContext->get<PeActorDynamicDescription>();
        shapeDescriptionDescription = simulationContext->get<PeShapeDescriptionSystem>();

        dataActorsType = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataActorDataForTensors>(1U, false, 16U);
        dataGeometryType = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuGeometriesDataForTensors>(1U, false, 16U);
        indexesGeometryType = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuIndexToGeometry>(1U, false, 16U);

        buffers.inputDataActorsBuffer = PH_NEW(PeGpuObjectsBuffer)("InputDataActorsBuffer", gpuData, &dataActorsType, true);

        for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
        {
            buffers.inputDataGeometryBuffer[i] = PH_NEW(PeGpuObjectsBuffer)("InputDataGeometryBuffer", gpuData, &dataGeometryType, true);
            buffers.inputIndexesGeometryBuffer[i] = PH_NEW(PeGpuObjectsBuffer)("InputIndexesGeometryBuffer", gpuData, &indexesGeometryType, true);
        }

        buffers.resultInertialTensorsBuffer = PH_NEW(PeGpuBuffer)("ResultInertialTensorsBuffer");
        buffers.resultInertialTensorsBuffer->init(gpuData);
        buffers.resultInertialTensorsBuffer->allocateData(vk::MemoryUseType_Long, vk::VulkanBufferType_UniformStorageBufferStatic,
                                                          sizeof(mth::mat4) * PeConstantsUtils::PeComputeConvexGeometry_CountAll * countElementsInBuffers);

        const char *nameTensorSumator = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_TensorSumator);
        shaders.tensorsSumatorP = shaderProvider->getPipeline(nameTensorSumator);
        shaders.tensorsSumatorL = shaderProvider->getPipelineLayout(nameTensorSumator);
        shaders.tensorsSumatorS = shaderProvider->createShaderParameters(nameTensorSumator, 1);

        const char *names[PeConstantsUtils::PeComputeConvexGeometry_CountAll];
        for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
        {
            names[i] = PeConstantsUtils::getShaderNameConvexGeometry(PeConstantsUtils::PeCompute_TensorGenerator,
                                                                     (PeConstantsUtils::PeComputeConvexGeometry)i);
            if (names[i] == NULL)
            {
                shaders.calculateInertialTensorP[i] = vk::ComputePipeline();
                shaders.calculateInertialTensorL[i] = VK_NULL_HANDLE;
                shaders.calculateInertialTensorS[i] = NULL;
            }
            else
            {
                shaders.calculateInertialTensorP[i] = shaderProvider->getPipeline(names[i]);
                shaders.calculateInertialTensorL[i] = shaderProvider->getPipelineLayout(names[i]);
                shaders.calculateInertialTensorS[i] = shaderProvider->createShaderParameters(names[i], 1);
            }
        }

        pReadTensors = staticReader->createReadMemoryPoint(sizeof(PeGpuDataDynamicActor));
    }

    void PeGenerateInertialTensorsSystem::release()
    {
        staticReader->destroyReadMemoryPoint(pReadTensors);

        for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
            shaderProvider->destroyShaderAutoParameters(shaders.calculateInertialTensorS[i]);

        shaderProvider->destroyShaderAutoParameters(shaders.tensorsSumatorS);

        for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
        {
            PH_RELEASE(geometryListsCaches[i])
            PH_RELEASE(indexesGeometriesCaches[i])

            PH_RELEASE(buffers.inputDataGeometryBuffer[i]);
            PH_RELEASE(buffers.inputIndexesGeometryBuffer[i]);
        }

        PH_RELEASE(buffers.inputDataActorsBuffer);
        PH_RELEASE(buffers.resultInertialTensorsBuffer);
    }

    void PeGenerateInertialTensorsSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        /*if (pReadTensors->IsValid())
        {
            PeGpuDataDynamicActor data = ((PeGpuDataDynamicActor *)pReadTensors->getMemory())[0];
            printf("mass: %f\n", data.mass);
            printf("center of mass: %f %f %f\n", data.centerMass.x, data.centerMass.y, data.centerMass.z);
            printf("tensor inertial:\n %f %f %f\n %f %f %f\n %f %f %f\n",
                   data.inertialTensor.m[0][0], data.inertialTensor.m[0][1], data.inertialTensor.m[0][2],
                   data.inertialTensor.m[1][0], data.inertialTensor.m[1][1], data.inertialTensor.m[1][2],
                   data.inertialTensor.m[2][0], data.inertialTensor.m[2][1], data.inertialTensor.m[2][2]);
            printf("inv tensor inertial:\n %f %f %f\n %f %f %f\n %f %f %f\n",
                   data.inverseInertialTensor.m[0][0], data.inverseInertialTensor.m[0][1], data.inverseInertialTensor.m[0][2],
                   data.inverseInertialTensor.m[1][0], data.inverseInertialTensor.m[1][1], data.inverseInertialTensor.m[1][2],
                   data.inverseInertialTensor.m[2][0], data.inverseInertialTensor.m[2][1], data.inverseInertialTensor.m[2][2]);
        }*/

        gpuData->computeCommands->debugMarkerBegin("GenerateInertialTensorsPrepareData", PeDebugMarkersUtils::passCallColor());

        memoryBarrierController->initUnknowMemory(buffers.inputDataActorsBuffer);
        memoryBarrierController->initUnknowMemory(buffers.resultInertialTensorsBuffer);
        for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
        {
            memoryBarrierController->initUnknowMemory(buffers.inputDataGeometryBuffer[i]);
            memoryBarrierController->initUnknowMemory(buffers.inputIndexesGeometryBuffer[i]);
        }

        countIterations = 0;
        
        countUpdate = actorDynamicDescription->getCountUpdateDynamicData();
        const InActorManager::ManagerEvent *events = actorDynamicDescription->getEventsUpdateDynamicData();

        {
            reallocateDatasSystem->ensureObjectsBuffer(countUpdate,
                                                       buffers.inputDataActorsBuffer,
                                                       &buffers.inputDataActorsBuffer);

            for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
            {
                PeGpuObjectsBuffer *buf = buffers.inputDataGeometryBuffer[i];
                reallocateDatasSystem->ensureObjectsBuffer(countUpdate, buf, &buf);
                buffers.inputDataGeometryBuffer[i] = buf;
            }
        }

        for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
        {
            geometryListsCaches[i]->clearMemory();
            indexesGeometriesCaches[i]->clearMemory();
        }

        {
            uint8_t *pWrite = (uint8_t *)gpuData->computeCommands->beginWrite(*buffers.inputDataActorsBuffer, 0,
                                                                              buffers.inputDataActorsBuffer->getSize());

            for (uint32_t i = 0; i < countUpdate; i++)
            {
                InActorManager::ManagerEvent event = events[i];
                InActor *actor = _simulationData->actorManager.getObjectPerId(event.index);
                InRigidDynamic *dynamic = dynamic_cast<InRigidDynamic *>(actor);
                PeGpuDataActor *gpuDataActor = _simulationData->actorManager.getDataPerId(event.index);

                uint32_t countShapes = dynamic->getCountShapes();

                float totalVolume = 0.0f;

                uint32_t startIndexes[PeConstantsUtils::PeComputeConvexGeometry_CountAll];
                uint32_t count[PeConstantsUtils::PeComputeConvexGeometry_CountAll];
                for (uint32_t i2 = 0; i2 < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i2++)
                {
                    startIndexes[i2] = indexesGeometriesCaches[i2]->getPos();
                    count[i2] = 0;
                }

                for (uint32_t i2 = 0; i2 < countShapes; i2++)
                {
                    InShape *shape = dynamic_cast<InShape *>(dynamic->getShape(i2));
                    const PeGpuDataShape *gpuDataShape = shapeDescriptionDescription->getGpuDataShape(shape);

                    uint32_t countGeometries = shape->countGeometries();
                    uint32_t firstIndexShapeGeometry = gpuDataShape->indexFirstGeometry;

                    for (uint32_t i3 = 0; i3 < countGeometries; i3++)
                    {
                        InGeometryBase *base = dynamic_cast<InGeometryBase *>(shape->getGeometry(i3));
                        uint32_t index = getIndexTypeGeometry(base);
                        count[index] += 1;

                        PeGpuIndexToGeometry indexGeometry{};
                        indexGeometry.index = firstIndexShapeGeometry + i3;
                        indexGeometry.volumeFactor = base->getVolume();
                        indexGeometry.free_0 = 0;
                        indexGeometry.free_1 = 0;
                        totalVolume += indexGeometry.volumeFactor;

                        indexesGeometriesCaches[index]->assignmentMemoryAndCopy(&indexGeometry);
                    }
                }

                { // actor data for tensors
                    PeGpuDataActorDataForTensors actorData{};
                    actorData.centerOfMass = dynamic->getCenterOfMass();
                    actorData.mass = dynamic->getMass();
                    actorData.totalVolume = totalVolume;
                    actorData.indexActor = gpuDataActor->indexAdditionalData;
                    actorData.free_0 = 0;
                    actorData.free_1 = 0;

                    uint32_t offset = buffers.inputDataActorsBuffer->getTypeDescription().getAlignmentOffsetToIndex(i);
                    std::memcpy(pWrite + offset, &actorData, sizeof(PeGpuDataActorDataForTensors));
                }

                { // geometries lists for tensors
                    for (uint32_t i2 = 0; i2 < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i2++)
                    {
                        PeGpuGeometriesDataForTensors geometriesList{};
                        geometriesList.countIndexesGeometries = count[i2];
                        geometriesList.startIndexToIndexesGeomtries = startIndexes[i2];
                        geometriesList.free_0 = 0;
                        geometriesList.free_1 = 0;

                        geometryListsCaches[i2]->assignmentMemoryAndCopy(&geometriesList);
                    }
                }
            }
            gpuData->computeCommands->endWrite(*buffers.inputDataActorsBuffer);
        }

        {
            for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
            {
                uint8_t *pWrite = (uint8_t *)gpuData->computeCommands->beginWrite(*buffers.inputDataGeometryBuffer[i], 0,
                                                                                  buffers.inputDataGeometryBuffer[i]->getSize());

                uint32_t countLists = geometryListsCaches[i]->getPos();
                PeGpuGeometriesDataForTensors *lists = geometryListsCaches[i]->getMemory(0);
                for (uint32_t i2 = 0; i2 < countLists; i2++)
                {
                    uint32_t offset = buffers.inputDataGeometryBuffer[i]->getTypeDescription().getAlignmentOffsetToIndex(i2);

                    PeGpuGeometriesDataForTensors geometriesList = lists[i2];
                    std::memcpy(pWrite + offset, &geometriesList, sizeof(PeGpuGeometriesDataForTensors));
                }

                gpuData->computeCommands->endWrite(*buffers.inputDataGeometryBuffer[i]);
            }
        }

        {
            for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
            {
                uint32_t countIndexes = indexesGeometriesCaches[i]->getPos();
                {
                    PeGpuObjectsBuffer *buffer = buffers.inputIndexesGeometryBuffer[i];
                    reallocateDatasSystem->ensureObjectsBuffer(countIndexes, buffer, &buffer);
                    buffers.inputIndexesGeometryBuffer[i] = buffer;
                }

                PeGpuObjectType type = buffers.inputIndexesGeometryBuffer[i]->getTypeDescription();
                uint32_t alignmentSize = type.getAlignmentSize();
                uint8_t *pWrite = (uint8_t *)gpuData->computeCommands->beginWrite(*buffers.inputIndexesGeometryBuffer[i], 0,
                                                                                  buffers.inputIndexesGeometryBuffer[i]->getSize());
                for (uint32_t i2 = 0; i2 < countIndexes; i2++)
                {
                    uint8_t index[alignmentSize];
                    PeGpuIndexToGeometry indexToG = *indexesGeometriesCaches[i]->getMemory(i2);
                    std::memcpy(index, &indexToG, alignmentSize);
                    std::memcpy(pWrite + type.getAlignmentOffsetToIndex(i2), index, alignmentSize);
                }

                gpuData->computeCommands->endWrite(*buffers.inputIndexesGeometryBuffer[i]);
            }
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeGenerateInertialTensorsSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        uint32_t countTypeConvexG = 0;
        for (uint32_t i = 0; i < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i++)
            countTypeConvexG += indexesGeometriesCaches[i]->getPos() != 0 ? 1 : 0;

        if (countUpdate == 0)
            return;

        gpuData->computeCommands->debugMarkerBegin("GenerateInertialTensorsSimulation", PeDebugMarkersUtils::passCallColor());

        bool firstLaunch = true;
        for (uint32_t i = 0; i < countUpdate; i += countElementsInBuffers)
        {
            uint32_t countElemets = std::min(countUpdate - i, countElementsInBuffers);
            uint32_t startIndexElement = i;

            uint32_t indexType = 0;
            for (uint32_t i2 = 0; i2 < PeConstantsUtils::PeComputeConvexGeometry_CountAll; i2++)
            {
                if (indexesGeometriesCaches[i2]->getPos() == 0)
                    continue;

                gpuData->computeCommands->debugMarkerBegin("CalculateInertialTensor", PeDebugMarkersUtils::subPassColor());

                struct
                {
                    uint32_t offsetIndex;
                    uint32_t indexTensor;
                    uint32_t indexCountTensors;
                } pConstants;
                pConstants.offsetIndex = startIndexElement;
                pConstants.indexTensor = indexType;
                pConstants.indexCountTensors = countTypeConvexG;

                vk::ShaderAutoParameters *parameters = shaders.calculateInertialTensorS[i2];

                if(firstLaunch)
                {
                    parameters->beginUpdate();
                    parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *shapeDescriptionDescription->getGeometriesBuffer());
                    parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.inputDataActorsBuffer);
                    parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.inputDataGeometryBuffer[i2]);
                    parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *buffers.inputIndexesGeometryBuffer[i2]);
                    parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *buffers.resultInertialTensorsBuffer);
                    parameters->endUpdate();
                }

                memoryBarrierController->setBuffer(0, shapeDescriptionDescription->getGeometriesBuffer(), PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, buffers.inputDataActorsBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, buffers.inputDataGeometryBuffer[i2], PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, buffers.inputIndexesGeometryBuffer[i2], PeTypeAccess_Read);
                memoryBarrierController->setBuffer(4, buffers.resultInertialTensorsBuffer, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 5, 0);

                gpuData->computeCommands->bindPipeline(shaders.calculateInertialTensorP[i2]);
                gpuData->computeCommands->bindDescriptorSet(shaders.calculateInertialTensorL[i2], parameters->getCurrentSet(0), 0);
                gpuData->computeCommands->pushConstant(shaders.calculateInertialTensorL[i2], 0, sizeof(pConstants), &pConstants);
                gpuData->computeCommands->dispatch(countElemets, 1, 1);

                indexType++;
                gpuData->computeCommands->debugMarkerEnd();
            }

            gpuData->computeCommands->debugMarkerBegin("TensorsSumator", PeDebugMarkersUtils::subPassColor());

            struct
            {
                uint maxCountActors;
                uint offsetIndexActor;
                uint indexCountTensors;
            } pConstants;
            pConstants.maxCountActors = countElemets;
            pConstants.offsetIndexActor = startIndexElement;
            pConstants.indexCountTensors = countTypeConvexG;

            if(firstLaunch)
            {
                shaders.tensorsSumatorS->beginUpdate();
                shaders.tensorsSumatorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.inputDataActorsBuffer);
                shaders.tensorsSumatorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.resultInertialTensorsBuffer);
                shaders.tensorsSumatorS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->actorDynamicsBuffer);
                shaders.tensorsSumatorS->endUpdate();
            }
            memoryBarrierController->setBuffer(0, buffers.inputDataActorsBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(1, buffers.resultInertialTensorsBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(2, _simulationData->actorDynamicsBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 3, 0);

            gpuData->computeCommands->bindPipeline(shaders.tensorsSumatorP);
            gpuData->computeCommands->bindDescriptorSet(shaders.tensorsSumatorL, shaders.tensorsSumatorS->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.tensorsSumatorL, 0, sizeof(pConstants), &pConstants);
            gpuData->computeCommands->dispatch(PeUtilties::calculateGroups(32, countElemets), 1, 1);

            gpuData->computeCommands->debugMarkerEnd();

            firstLaunch = false;
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeGenerateInertialTensorsSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        //staticReader->addRequest(*_simulationData->actorDynamicsBuffer, 0, pReadTensors, 0, pReadTensors->getSize());
    }

    PeConstantsUtils::PeComputeConvexGeometry PeGenerateInertialTensorsSystem::getIndexTypeGeometry(InGeometryBase *_geometry)
    {
        switch (_geometry->getGpuGeometryFlags())
        {
        case PeGpuGeometryFlags::PeGpuGeometry_Box:
            return PeConstantsUtils::PeComputeConvexGeometry_Box;
        case PeGpuGeometryFlags::PeGpuGeometry_Sphere:
            return PeConstantsUtils::PeComputeConvexGeometry_Sphere;
        case PeGpuGeometryFlags::PeGpuGeometry_Capsule:
            return PeConstantsUtils::PeComputeConvexGeometry_Capsule;
        }

        return PeConstantsUtils::PeComputeConvexGeometry_CountAll;
    }
}