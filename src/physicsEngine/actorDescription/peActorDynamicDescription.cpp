#include "physicsEngine/actorDescription/peActorDynamicDescription.h"

#include "physicsEngine/peLimits.h"

#include "physics/phActor.h"
#include "physics/phRigidDynamic.h"

#include "physics/internal/inActor.h"
#include "physics/internal/inRigidDynamic.h"

namespace phys
{
    PeActorDynamicDescription::PeActorDynamicDescription()
        : PeSystem(),
          gpu(NULL),
          grabObjectsSystem(NULL),
          staticLoaderSystem(NULL),

          bufferUpdateDynamicData(),
          bufferUpdateVelocityData()
    {
    }

    void PeActorDynamicDescription::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        staticLoaderSystem = simulationContext->get<PeGpuStaticLoaderSystem>();
    }

    void PeActorDynamicDescription::release()
    {
        bufferUpdateDynamicData.release();
        bufferUpdateVelocityData.release();
    }

    void PeActorDynamicDescription::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("ActorDynamicDescriptionPrepareData", PeDebugMarkersUtils::passCallColor());

        bufferUpdateDynamicData.clearMemory();
        bufferUpdateVelocityData.clearMemory();

        uint32_t countEvents = _simulationData->actorMangerCache.getCountEvents();
        InActorManager::ManagerEvent *events = _simulationData->actorMangerCache.getEvents();

        uint32_t capacity = getCapacityActorDynamicBuffers(_simulationData, events, countEvents);

        ensureCapacityDynamicBuffers(_simulationData, capacity);

        updateDynamicData(_simulationData, bufferUpdateDynamicData.getMemory(0), bufferUpdateDynamicData.getPos());
        updateVelocityData(_simulationData, bufferUpdateVelocityData.getMemory(0), bufferUpdateVelocityData.getPos());

        gpu->computeCommands->debugMarkerEnd();
    }

    uint32_t PeActorDynamicDescription::getCapacityActorDynamicBuffers(PeSceneSimulationData *_simulationData, InActorManager::ManagerEvent *_events, uint32_t _countEvents)
    {
        InActorManager *manager = &_simulationData->actorManager;

        InActorManager::ManagerEvent event;
        for (uint32_t i = 0; i < _countEvents; i++)
        {
            event = *(_events + i);
            PeGpuDataActor *actorData = manager->getDataPerId(event.index);
            if (event.event == InActorManager::ManagerEvent_Created)
            {
                InActor *actor = manager->getObjectPerId(event.index);
                bool isDynamic = actor->isDynamic();
                if (isDynamic)
                {
                    int32_t indexActor = event.index;
                    uint32_t indexDynamicData = 0;
                    _simulationData->tableDynamicDatas.assignmentMemoryAndCopy(&indexActor, indexDynamicData);

                    actorData->indexAdditionalData = indexDynamicData;

                    bufferUpdateDynamicData.assignmentMemoryAndCopy(&event);
                    bufferUpdateVelocityData.assignmentMemoryAndCopy(&event);
                }
            }
            else if (event.event == InActorManager::ManagerEvent_Changed)
            {
                bool wasDynamic = (actorData->flags & PeGpuActorFlags::PeGpuActor_Dynamic) != 0;

                InActor *actor = manager->getObjectPerId(event.index);
                bool isDynamic = actor->isDynamic();
                if (wasDynamic && !isDynamic)
                {
                    _simulationData->tableDynamicDatas.popMemory(event.index);
                    actorData->indexAdditionalData = 0;
                }
                else if (!wasDynamic && isDynamic)
                {
                    int32_t indexActor = event.index;
                    uint32_t indexDynamicData = 0;
                    _simulationData->tableDynamicDatas.assignmentMemoryAndCopy(&indexActor, indexDynamicData);

                    actorData->indexAdditionalData = indexDynamicData;

                    bufferUpdateDynamicData.assignmentMemoryAndCopy(&event);
                    bufferUpdateVelocityData.assignmentMemoryAndCopy(&event);
                }
                else if (isDynamic)
                {
                    if ((event.flagChanges & PhStateActorChangesFlags::ChangeMassCenter) || (event.flagChanges & PhStateActorChangesFlags::ChangeShape))
                    {
                        bufferUpdateDynamicData.assignmentMemoryAndCopy(&event);
                    }
                }
            }
            else if (event.event == InActorManager::ManagerEvent_Destroyed)
            {
                bool wasDynamic = (actorData->flags & PeGpuActorFlags::PeGpuActor_Dynamic) != 0;
                if (wasDynamic)
                {
                    _simulationData->tableDynamicDatas.popMemory(actorData->indexAdditionalData);
                    actorData->indexAdditionalData = 0;
                }
            }
        }

        return _simulationData->tableDynamicDatas.getCountUse();
    }

    PeGpuObjectType PeActorDynamicDescription::getActorDynamicType(uint32_t _capacity)
    {
        return PeGpuObjectType::getObjectType<PeGpuDescriptionShot, PeGpuDataDynamicActor>(_capacity, false, 16);
    }

    PeGpuObjectType PeActorDynamicDescription::getActorVelocityType(uint32_t _capacity)
    {
        return PeGpuObjectType::getObjectType<PeGpuDescriptionShot, PeGpuDataVelocityActor>(_capacity, false, 16);
    }

    PeGpuObjectType PeActorDynamicDescription::getActorSleepingType(uint32_t _capacity)
    {
        return PeGpuObjectType::getObjectType<PeGpuDescriptionShot, PeGpuDataSleepingActor>(_capacity, false);
    }

    PeGpuObjectsBuffer *PeActorDynamicDescription::createActorDynamicsBuffer(PeGpuObjectType _type)
    {
        PeGpuObjectsBuffer *buffer = PH_NEW(PeGpuObjectsBuffer)("ActorDynamicBuffer");
        buffer->initType(gpu, &_type);
        buffer->setShaderWriting(true);
        buffer->allocateData();
        return buffer;
    }

    PeGpuObjectsBuffer *PeActorDynamicDescription::createActorVelocitiesBuffer(PeGpuObjectType _type)
    {
        PeGpuObjectsBuffer *buffer = PH_NEW(PeGpuObjectsBuffer)("ActorVelocitiesBuffer");
        buffer->initType(gpu, &_type);
        buffer->setShaderWriting(true);
        buffer->allocateData();
        return buffer;
    }

    PeGpuObjectsBuffer *PeActorDynamicDescription::createActorSleepingBuffer(PeGpuObjectType _type)
    {
        PeGpuObjectsBuffer *buffer = PH_NEW(PeGpuObjectsBuffer)("ActorSleepingBuffer");
        buffer->initType(gpu, &_type);
        buffer->setShaderWriting(true);
        buffer->allocateData();
        return buffer;
    }

    void PeActorDynamicDescription::ensureCapacityDynamicBuffers(PeSceneSimulationData *_simulationData, uint32_t _capacity)
    {
        if (_simulationData->actorDynamicType.getMaxCountObjects() < _capacity)
        {
            _simulationData->actorDynamicType = getActorDynamicType(_capacity);
            _simulationData->actorVelocityType = getActorVelocityType(_capacity);
            _simulationData->actorSleepingType = getActorSleepingType(_capacity);

            PeGpuObjectsBuffer *actorDynamicBuffer = createActorDynamicsBuffer(_simulationData->actorDynamicType);
            PeGpuObjectsBuffer *actorVelocityBuffer = createActorVelocitiesBuffer(_simulationData->actorVelocityType);
            PeGpuObjectsBuffer *actorSleepingBuffer = createActorSleepingBuffer(_simulationData->actorSleepingType);

            if (_simulationData->actorDynamicsBuffer != NULL)
            {
                gpu->computeCommands->copyBuffer(*_simulationData->actorDynamicsBuffer, 0, *actorDynamicBuffer, 0,
                                                 _simulationData->actorDynamicsBuffer->getSize());

                gpu->computeCommands->copyBuffer(*_simulationData->actorVelocitiesBuffer, 0, *actorVelocityBuffer, 0,
                                                 _simulationData->actorVelocitiesBuffer->getSize());

                gpu->computeCommands->copyBuffer(*_simulationData->actorSleepingBuffer, 0, *actorSleepingBuffer, 0,
                                                 _simulationData->actorSleepingBuffer->getSize());

                grabObjectsSystem->addDataToUnload(_simulationData->actorDynamicsBuffer);
                grabObjectsSystem->addDataToUnload(_simulationData->actorVelocitiesBuffer);
                grabObjectsSystem->addDataToUnload(_simulationData->actorSleepingBuffer);
            }

            _simulationData->actorDynamicsBuffer = actorDynamicBuffer;
            _simulationData->actorVelocitiesBuffer = actorVelocityBuffer;
            _simulationData->actorSleepingBuffer = actorSleepingBuffer;
        }
    }

    void PeActorDynamicDescription::updateDynamicData(PeSceneSimulationData *_simulationData, InActorManager::ManagerEvent *_events, uint32_t _countEvents)
    {
        gpu->computeCommands->debugMarkerBegin("UpdateDynamicData", PeDebugMarkersUtils::subPassColor());

        InActorManager *manager = &_simulationData->actorManager;

        staticLoaderSystem->begin();

        InActorManager::ManagerEvent event;
        for (uint32_t i = 0; i < _countEvents; i++)
        {
            event = *(_events + i);

            PeGpuDataActor *actorData = manager->getDataPerId(event.index);
            InRigidDynamic *dynamic = dynamic_cast<InRigidDynamic *>(manager->getObjectPerId(event.index));

            uint32_t index = actorData->indexAdditionalData;
            PeGpuDataDynamicActor data{};
            data.centerMass = dynamic->getCenterOfMass();
            data.mass = dynamic->getMass();
            data.linearDrag = dynamic->getLinearDrag();
            data.angularDrag = dynamic->getAngularDrag();

            staticLoaderSystem->sendData(&data, sizeof(data), *_simulationData->actorDynamicsBuffer,
                                         _simulationData->actorDynamicType.getAlignmentOffsetToIndex(index));
        }

        staticLoaderSystem->end();

        gpu->computeCommands->debugMarkerEnd();
    }

    void PeActorDynamicDescription::updateVelocityData(PeSceneSimulationData *_simulationData, InActorManager::ManagerEvent *_events, uint32_t _countEvents)
    {
        gpu->computeCommands->debugMarkerBegin("UpdateVelocityData", PeDebugMarkersUtils::subPassColor());

        InActorManager *manager = &_simulationData->actorManager;

        staticLoaderSystem->begin();

        InActorManager::ManagerEvent event;
        for (uint32_t i = 0; i < _countEvents; i++)
        {
            event = *(_events + i);

            PeGpuDataActor *actorData = manager->getDataPerId(event.index);
            uint32_t index = actorData->indexAdditionalData;
            PeGpuDataVelocityActor dataVel{};
            PeGpuDataSleepingActor dataSleep{};

            staticLoaderSystem->sendData(&dataVel, sizeof(dataVel), *_simulationData->actorVelocitiesBuffer,
                                         _simulationData->actorVelocityType.getAlignmentOffsetToIndex(index));
            staticLoaderSystem->sendData(&dataSleep, sizeof(dataSleep), *_simulationData->actorSleepingBuffer,
                                         _simulationData->actorSleepingType.getAlignmentOffsetToIndex(index));
            printf("Send update velocity\n");
        }

        staticLoaderSystem->end();

        gpu->computeCommands->debugMarkerEnd();
    }
}