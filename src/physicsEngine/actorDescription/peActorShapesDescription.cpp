#include "physicsEngine/actorDescription/peActorShapesDescription.h"

#include "physics/phActor.h"
#include "physics/phRigidDynamic.h"

namespace phys
{
    PeActorShapesDescription::PeActorShapesDescription()
        : PeSystem(),
          gpu(NULL),
          grabObjectsSystem(NULL),
          staticLoaderSystem(NULL),
          shapeDescriptionSystem(NULL),
          bufferUpdateSegments()
    {
    }

    void PeActorShapesDescription::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        staticLoaderSystem = simulationContext->get<PeGpuStaticLoaderSystem>();
        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();
    }

    void PeActorShapesDescription::release()
    {
        bufferUpdateSegments.release();
    }

    void PeActorShapesDescription::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("ActorShapesDescriptionPrepareData", PeDebugMarkersUtils::passCallColor());

        bufferUpdateSegments.clearMemory();

        uint32_t countEvents = _simulationData->actorMangerCache.getCountEvents();
        InActorManager::ManagerEvent *events = _simulationData->actorMangerCache.getEvents();

        uint32_t capacityShapesList = updateShapesList(_simulationData, events, countEvents);

        ensureShapeList(_simulationData, capacityShapesList);
        sendUpdateShapeList(_simulationData, bufferUpdateSegments.getMemory(0), bufferUpdateSegments.getPos());

        gpu->computeCommands->debugMarkerEnd();
    }

    PeGpuObjectType PeActorShapesDescription::getShapesListType(uint32_t _capacity)
    {
        return PeGpuObjectType::getObjectType<PeGpuDescriptionShot, uint32_t>(_capacity, false);
    }

    PeGpuObjectsBuffer *PeActorShapesDescription::createShapesListBuffer(PeGpuObjectType _type)
    {
        PeGpuObjectsBuffer *buffer = PH_NEW(PeGpuObjectsBuffer)("ShapesListBuffer");
        buffer->initType(gpu, &_type);
        buffer->allocateData();
        return buffer;
    }

    uint32_t PeActorShapesDescription::updateShapesList(PeSceneSimulationData *_simulationData, InActorManager::ManagerEvent *_events, uint32_t _countEvents)
    {
        uint32_t maxCountUpdateShapes = 0;

        InActorManager *manager = &_simulationData->actorManager;

        for (uint32_t i = 0; i < _countEvents; i++)
        {
            InActorManager::ManagerEvent event = *(_events + i);
            PeGpuDataActor *actorData = manager->getDataPerId(event.index);

            if (event.event == InActorManager::ManagerEvent_Destroyed)
            {
                PeBufferChain::Segment segment;
                segment.count = actorData->countShapes;
                segment.firstIndex = actorData->indexFirstShape;
                _simulationData->shapesChain.freeSegment(segment);
            }
            else if (event.event == InActorManager::ManagerEvent_Changed &&
                     (event.flagChanges & PhStateActorChangesFlags::ChangeShape) != 0)
            {
                PeBufferChain::Segment segment;
                segment.count = actorData->countShapes;
                segment.firstIndex = actorData->indexFirstShape;
                _simulationData->shapesChain.freeSegment(segment);

                InRigidBody *actor = dynamic_cast<InRigidBody *>(manager->getObjectPerId(event.index));
                segment = _simulationData->shapesChain.allocateSegment(actor->shapesListener.countShapes());

                actorData->countShapes = segment.count;
                actorData->indexFirstShape = segment.firstIndex;

                UpdateShapeSegment updateSegment;
                updateSegment.rigidBody = actor;
                updateSegment.firstIndex = segment.firstIndex;
                updateSegment.count = segment.count;

                if (updateSegment.count)
                    bufferUpdateSegments.assignmentMemoryAndCopy(&updateSegment);

                maxCountUpdateShapes = std::max(maxCountUpdateShapes, updateSegment.firstIndex + updateSegment.count);
            }
            else if (event.event == InActorManager::ManagerEvent_Created)
            {
                InRigidBody *actor = dynamic_cast<InRigidBody *>(manager->getObjectPerId(event.index));
                PeBufferChain::Segment segment = _simulationData->shapesChain.allocateSegment(actor->shapesListener.countShapes());

                actorData->countShapes = segment.count;
                actorData->indexFirstShape = segment.firstIndex;

                UpdateShapeSegment updateSegment;
                updateSegment.rigidBody = actor;
                updateSegment.firstIndex = segment.firstIndex;
                updateSegment.count = segment.count;

                if (updateSegment.count)
                    bufferUpdateSegments.assignmentMemoryAndCopy(&updateSegment);

                maxCountUpdateShapes = std::max(maxCountUpdateShapes, updateSegment.firstIndex + updateSegment.count);
            }
        }

        return maxCountUpdateShapes;
    }

    void PeActorShapesDescription::ensureShapeList(PeSceneSimulationData *_simulationData, uint32_t _capacity)
    {
        if (_simulationData->shapeListType.getMaxCountObjects() < _capacity)
        {
            _simulationData->shapeListType = getShapesListType(_capacity);

            PeGpuObjectsBuffer *shapeListBuffer = createShapesListBuffer(_simulationData->shapeListType);

            if (_simulationData->shapeListBuffer != NULL)
            {
                gpu->computeCommands->copyBuffer(*_simulationData->shapeListBuffer, 0, *shapeListBuffer, 0,
                                                 _simulationData->shapeListBuffer->getSize());

                grabObjectsSystem->addDataToUnload(_simulationData->shapeListBuffer);
            }

            _simulationData->shapeListBuffer = shapeListBuffer;
        }
    }

    void PeActorShapesDescription::sendUpdateShapeList(PeSceneSimulationData *_simulationData, UpdateShapeSegment *_segments, uint32_t _countSegments)
    {
        staticLoaderSystem->begin();
        for (uint32_t i = 0; i < _countSegments; i++)
        {
            UpdateShapeSegment segment = *(_segments + i);
            uint32_t shapes[segment.count];
            for (uint32_t i2 = 0; i2 < segment.count; i2++)
            {
                InShape *shape = segment.rigidBody->shapesListener.getShape(i2);
                shapes[i2] = shapeDescriptionSystem->getLocalIndexShape(shape);
            }
            staticLoaderSystem->sendData(shapes, sizeof(shapes), *_simulationData->shapeListBuffer,
                                         _simulationData->shapeListType.getAlignmentOffsetToIndex(segment.firstIndex));

            printf("Send update shape list\n");
        }
        staticLoaderSystem->end();
    }
}