#include "physicsEngine/actorDescription/peSceneActorsDescription.h"

#include "physicsEngine/peLimits.h"

#include "physics/phActor.h"
#include "physics/phRigidDynamic.h"

#include "physics/internal/inActor.h"

namespace phys
{
    PeSceneActorsDescription::PeSceneActorsDescription()
        : PeSystem(),
          gpu(NULL),
          grabObjectsSystem(NULL),
          staticLoaderSystem(NULL),
          shapeDescriptionSystem(NULL),
          actorDynamicDescription(NULL),
          actorShapesDescription(NULL)
    {
    }

    void PeSceneActorsDescription::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        staticLoaderSystem = simulationContext->get<PeGpuStaticLoaderSystem>();
        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();
        actorDynamicDescription = simulationContext->get<PeActorDynamicDescription>();
        actorShapesDescription = simulationContext->get<PeActorShapesDescription>();
    }

    void PeSceneActorsDescription::release()
    {
    }

    void PeSceneActorsDescription::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("ActorsDescriptionPrepareData", PeDebugMarkersUtils::subGroupColor());

        uint32_t capacity = readEvents(_simulationData);
        ensureCapacityBuffers(_simulationData, capacity);

        actorDynamicDescription->prepareData(_simulationData);
        actorShapesDescription->prepareData(_simulationData);

        uint32_t countEvents = _simulationData->actorMangerCache.getCountEvents();
        if (countEvents)
            sendUpdateActors(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    PeGpuObjectType PeSceneActorsDescription::getActorType(uint32_t _capacity)
    {
        return PeGpuObjectType::getObjectType<PeGpuDescriptionShot, PeGpuDataActor>(_capacity, false, 16);
    }

    PeGpuObjectType PeSceneActorsDescription::getActorOrientationType(uint32_t _capacity)
    {
        return PeGpuObjectType::getObjectType<PeGpuDescriptionShot, PeGpuDataActorOrientation>(_capacity, false, 16);
    }

    PeGpuObjectsBuffer *PeSceneActorsDescription::createActorsBuffer(PeGpuObjectType _type)
    {
        PeGpuObjectsBuffer *buffer = PH_NEW(PeGpuObjectsBuffer)("ActorsBuffer");
        buffer->initType(gpu, &_type);
        buffer->allocateData();
        return buffer;
    }

    PeGpuObjectsBuffer *PeSceneActorsDescription::createActorOrientationsBuffer(PeGpuObjectType _type)
    {
        PeGpuObjectsBuffer *buffer = PH_NEW(PeGpuObjectsBuffer)("ActorsOrientationsBuffer");
        buffer->initType(gpu, &_type);
        buffer->setHostVisibility(true);
        buffer->setShaderWriting(true);
        buffer->allocateData();
        return buffer;
    }

    uint32_t PeSceneActorsDescription::readEvents(PeSceneSimulationData *_simulationData)
    {
        InActorManager *manager = &_simulationData->actorManager;
        uint32_t countActors = std::min(PE_MAX_COUNT_ACTORS_IN_SCENE, manager->getCapacityTable());
        return countActors;
    }

    void PeSceneActorsDescription::ensureCapacityBuffers(PeSceneSimulationData *_simulationData, uint32_t _capacity)
    {
        if (_simulationData->actorType.getMaxCountObjects() < _capacity)
        {
            _simulationData->actorType = getActorType(_capacity);
            _simulationData->actorOrientationType = getActorOrientationType(_capacity);

            PeGpuObjectsBuffer *actorBuffer = createActorsBuffer(_simulationData->actorType);
            PeGpuObjectsBuffer *orientationBuffer = createActorOrientationsBuffer(_simulationData->actorOrientationType);

            if (_simulationData->actorsBuffer != NULL)
            {
                gpu->computeCommands->copyBuffer(*_simulationData->actorsBuffer, 0, *actorBuffer, 0,
                                                 _simulationData->actorsBuffer->getSize());

                gpu->computeCommands->copyBuffer(*_simulationData->actorOrientationsBuffer, 0, *orientationBuffer, 0,
                                                 _simulationData->actorOrientationsBuffer->getSize());

                grabObjectsSystem->addDataToUnload(_simulationData->actorsBuffer);
                grabObjectsSystem->addDataToUnload(_simulationData->actorOrientationsBuffer);
            }

            _simulationData->actorsBuffer = actorBuffer;
            _simulationData->actorOrientationsBuffer = orientationBuffer;
        }
    }

    PeGpuActorFlags PeSceneActorsDescription::getActorFlags(InActor *_actor)
    {
        PhActor *phActor = dynamic_cast<PhActor *>(_actor);
        if (phActor->getActorType() == PhActor::PhActor_RigidDynamic)
        {
            return _actor->isDynamic() ? PeGpuActorFlags::PeGpuActor_Dynamic : PeGpuActorFlags::PeGpuActor_Kinematic;
        }
        else
        {
            return PeGpuActorFlags::PeGpuActor_Static;
        }
    }

    void PeSceneActorsDescription::sendUpdateActors(PeSceneSimulationData *_simulationData)
    {
        staticLoaderSystem->begin();
        vk::VulkanBuffer *orientationBuffer = *_simulationData->actorOrientationsBuffer;
        uint8_t *pWriteOrientation = (uint8_t *)gpu->computeCommands->beginWrite(orientationBuffer, 0, orientationBuffer->getSize());

        uint32_t countEvents = _simulationData->actorMangerCache.getCountEvents();
        InActorManager::ManagerEvent *events = _simulationData->actorMangerCache.getEvents();

        InActorManager::ManagerEvent event;
        for (uint32_t i = 0; i < countEvents; i++)
        {
            event = events[i];
            PeGpuDataActor *data = _simulationData->actorManager.getDataPerId(event.index);

            if (event.event != InActorManager::ManagerEvent_Destroyed)
            {
                InActor *actor = _simulationData->actorManager.getObjectPerId(event.index);
                data->flags = getActorFlags(actor);

                staticLoaderSystem->sendData(data, sizeof(PeGpuDataActor), *_simulationData->actorsBuffer,
                                             _simulationData->actorType.getAlignmentOffsetToIndex(event.index));

                printf("send actor\n");
            }

            bool sendTransform = false;
            if (event.event == InActorManager::ManagerEvent_Created)
            {
                sendTransform = true;
            }
            else if (event.event == InActorManager::ManagerEvent_Changed &&
                     (event.flagChanges & PhStateActorChangesFlags::ChangeTransform) != 0)
            {
                sendTransform = true;
            }

            if (sendTransform)
            {
                PeGpuDataActorOrientation orientation;

                InActor *actor = _simulationData->actorManager.getObjectPerId(event.index);
                InRigidActor *rigidActor = dynamic_cast<InRigidActor *>(actor);

                orientation.position = rigidActor->position;
                orientation.rotation = rigidActor->rotation;
                orientation.freeMemory_1 = 0.0f;

                std::memcpy(pWriteOrientation + _simulationData->actorOrientationType.getAlignmentOffsetToIndex(event.index),
                            &orientation, sizeof(orientation));

                printf("send transform: index - %i, pos - %f %f %f\n", event.index, orientation.position.x, orientation.position.y, orientation.position.z);
            }
        }

        gpu->computeCommands->endWrite(orientationBuffer);
        staticLoaderSystem->end();
    }
}