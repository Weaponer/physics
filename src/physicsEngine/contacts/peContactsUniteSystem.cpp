#include "physicsEngine/contacts/peContactsUniteSystem.h"
#include "physicsEngine/peLimits.h"

namespace phys
{
    PeContactsUniteSystem::PeContactsUniteSystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL)
    {
    }

    void PeContactsUniteSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();

        tableContactsSystem = simulationContext->get<PeTableContactsSystem>();
        contactsGenerateSystem = simulationContext->get<PeContactsGenerateSystem>();
        contactsUpdateSystem = simulationContext->get<PeContactsUpdateSystem>();

        const char *nameContainersCounter = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_ContainersCounter);
        shaders.containersCounterP = shaderProvider->getPipeline(nameContainersCounter);
        shaders.containersCounterL = shaderProvider->getPipelineLayout(nameContainersCounter);
        shaders.containersCounterS = shaderProvider->createShaderParameters(nameContainersCounter, 1);

        const char *nameContactsUniter = PeConstantsUtils::getShaderName(PeConstantsUtils::PeCompute_ContactsUniter);
        shaders.uniteContactsP = shaderProvider->getPipeline(nameContactsUniter);
        shaders.uniteContactsL = shaderProvider->getPipelineLayout(nameContactsUniter);
        shaders.uniteContactsS = shaderProvider->createShaderParameters(nameContactsUniter, 1);

        buffers.countContainersBuffer = PH_NEW(PeGpuBuffer)("CountContainersBuffer", gpuData, vk::MemoryUseType_Long,
                                                            vk::VulkanBufferType_UniformStorageBufferStatic, sizeof(uint));
        buffers.uniteContactsGroups = PH_NEW(PeGpuBuffer)("UniteContactsGroups", gpuData, vk::MemoryUseType_Long,
                                                          vk::VulkanBufferType_UniformStorageBufferStatic, sizeof(PeGpuDataDispatchIndirect));
        buffers.uniteContactsGroupIDB = PH_NEW(PeGpuBuffer)("UniteContactsGroups", gpuData, vk::MemoryUseType_Long,
                                                            vk::VulkanBufferType_IndirectBufferStatic, sizeof(PeGpuDataDispatchIndirect));

        PeGpuObjectType type = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataContactsContainer>(256, false, sizeof(PeGpuDataContactsContainer));
        buffers.tempContainersBuffer = PH_NEW(PeGpuObjectsBuffer)("TempContainersBuffer", gpuData, &type);
    }

    void PeContactsUniteSystem::release()
    {

        PH_RELEASE(buffers.tempContainersBuffer);
        PH_RELEASE(buffers.uniteContactsGroupIDB);
        PH_RELEASE(buffers.uniteContactsGroups);
        PH_RELEASE(buffers.countContainersBuffer);

        shaderProvider->destroyShaderAutoParameters(shaders.uniteContactsS);
        shaderProvider->destroyShaderAutoParameters(shaders.containersCounterS);
    }

    void PeContactsUniteSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        memoryBarrierController->initUnknowMemory(buffers.tempContainersBuffer);
        memoryBarrierController->initUnknowMemory(buffers.uniteContactsGroupIDB);
        memoryBarrierController->initUnknowMemory(buffers.uniteContactsGroups);
        memoryBarrierController->initUnknowMemory(buffers.countContainersBuffer);

        gpuData->computeCommands->debugMarkerBegin("ContactsUnitePrepareData", PeDebugMarkersUtils::passCallColor());
        uint countContainers = _simulationData->tableContacts.contactsContainersBuffers[0]->getTypeDescription().getMaxCountObjects();

        reallocateDatasSystem->ensureObjectsBuffer(countContainers, buffers.tempContainersBuffer,
                                                                  &buffers.tempContainersBuffer);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsUniteSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("ContactsUniteSimulation", PeDebugMarkersUtils::passCallColor());

        // generate IDB to launch unite contacts
        gpuData->computeCommands->debugMarkerBegin("ContainersCounter", PeDebugMarkersUtils::subPassColor());
        {
            PeGpuCounterImageUint *allocatedContainers = _simulationData->tableContacts.allocatedContactsContainersCounter;

            shaders.containersCounterS->beginUpdate();
            shaders.containersCounterS->setParameter<vk::ShaderParameterStorageImage>(0, 0, *allocatedContainers);
            shaders.containersCounterS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *buffers.uniteContactsGroups);
            shaders.containersCounterS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.countContainersBuffer);
            shaders.containersCounterS->endUpdate();

            {
                memoryBarrierController->setImage(0, allocatedContainers, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(0, buffers.uniteContactsGroups, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(1, buffers.countContainersBuffer, PeTypeAccess_Write);

                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 2, 1);
            }

            struct
            {
                uint32_t sizeGroup;
            } pConstants;
            pConstants.sizeGroup = 32;

            gpuData->computeCommands->bindPipeline(shaders.containersCounterP);
            gpuData->computeCommands->bindDescriptorSet(shaders.containersCounterL,
                                                        shaders.containersCounterS->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.containersCounterL, 0,
                                                   sizeof(pConstants), &pConstants);

            gpuData->computeCommands->dispatch(1, 1, 1);
            {
                memoryBarrierController->setBuffer(0, buffers.uniteContactsGroups, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(0, buffers.uniteContactsGroupIDB, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 1, 0);
            }

            gpuData->computeCommands->copyBuffer(*buffers.uniteContactsGroups, 0,
                                                 *buffers.uniteContactsGroupIDB, 0,
                                                 buffers.uniteContactsGroups->getSize());
        }
        gpuData->computeCommands->debugMarkerEnd();

        // unite contacts
        gpuData->computeCommands->debugMarkerBegin("UniteContacts", PeDebugMarkersUtils::subPassColor());
        {
            PeGpuObjectsBuffer *contactsContainers = _simulationData->tableContacts.contactsContainersBuffers[_simulationData->tableContacts.indexLastBuffer];
            PeGpuObjectsBuffer *contacts = _simulationData->tableContacts.contactsBuffers[_simulationData->tableContacts.indexLastBuffer];

            {
                memoryBarrierController->setBuffer(0, contactsContainers, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, buffers.tempContainersBuffer, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 2, 0);
            }

            gpuData->computeCommands->copyBuffer(*contactsContainers, 0,
                                                 *buffers.tempContainersBuffer, 0,
                                                 contactsContainers->getSize());

            PeGpuBuffer *containersData = tableContactsSystem->getContactsCopySystem()->getContainersData();

            PeGpuObjectsBuffer *generatedContacts = contactsGenerateSystem->getGeneratedContacts();
            PeGpuObjectsBuffer *indexesTypeContainer = contactsGenerateSystem->getIndexesTypeContainer();

            PeGpuObjectsBuffer *updatedContacts = contactsUpdateSystem->getUpdatedContacts();
            PeGpuObjectsBuffer *isContactActive = contactsUpdateSystem->getIsContactActive();

            {
                memoryBarrierController->setBuffer(0, containersData, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(1, buffers.countContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(2, buffers.tempContainersBuffer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(3, generatedContacts, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(4, indexesTypeContainer, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(5, updatedContacts, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(6, isContactActive, PeTypeAccess_Read);
                memoryBarrierController->setBuffer(7, contactsContainers, PeTypeAccess_Write);
                memoryBarrierController->setBuffer(8, contacts, PeTypeAccess_Write);
                memoryBarrierController->setupBarrier(PeTypeStage_Compute, 9, 0);

                memoryBarrierController->setBuffer(0, buffers.uniteContactsGroupIDB, PeTypeAccess_IndirectCallRead);
                memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
            }

            shaders.uniteContactsS->beginUpdate();
            shaders.uniteContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *containersData);
            shaders.uniteContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *buffers.countContainersBuffer);
            shaders.uniteContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *buffers.tempContainersBuffer);
            shaders.uniteContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *generatedContacts);
            shaders.uniteContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *indexesTypeContainer);
            shaders.uniteContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *updatedContacts);
            shaders.uniteContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *isContactActive);
            shaders.uniteContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 7, *contactsContainers);
            shaders.uniteContactsS->setParameter<vk::ShaderParameterStorageBuffer>(0, 8, *contacts);
            shaders.uniteContactsS->endUpdate();

            gpuData->computeCommands->bindPipeline(shaders.uniteContactsP);
            gpuData->computeCommands->bindDescriptorSet(shaders.uniteContactsL,
                                                        shaders.uniteContactsS->getCurrentSet(0), 0);

            gpuData->computeCommands->dispatchIndirect(*buffers.uniteContactsGroupIDB, 0);
        }
        gpuData->computeCommands->debugMarkerEnd();

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsUniteSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
    }
}