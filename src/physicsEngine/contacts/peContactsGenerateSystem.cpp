#include "physicsEngine/contacts/peContactsGenerateSystem.h"

namespace phys
{
    PeContactsGenerateSystem::PeContactsGenerateSystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL),
          tableContactsSystem(NULL)

    {
    }

    void PeContactsGenerateSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();
        tableContactsSystem = simulationContext->get<PeTableContactsSystem>();

        const char *names[PeConstantsUtils::PeComputeTypeContact_CountAll];

        for (uint32_t i = 0; i < PeConstantsUtils::PeComputeTypeContact_CountAll; i++)
        {
            names[i] = PeConstantsUtils::getShaderNameTypeContact(PeConstantsUtils::PeCompute_ContactsGenerator,
                                                                  (PeConstantsUtils::PeComputeTypeContact)i);
            if (names[i] == NULL)
            {
                shaders.generateContactP[i] = vk::ComputePipeline();
                shaders.generateContactL[i] = VK_NULL_HANDLE;
                shaders.generateContactS[i] = NULL;
            }
            else
            {
                shaders.generateContactP[i] = shaderProvider->getPipeline(names[i]);
                shaders.generateContactL[i] = shaderProvider->getPipelineLayout(names[i]);
                shaders.generateContactS[i] = shaderProvider->createShaderParameters(names[i], 1);
            }
        }

        {
            PeGpuObjectType type = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataContact>(256, false, sizeof(PeGpuDataContact));
            buffers.newContactsBuffer = PH_NEW(PeGpuObjectsBuffer)("NewContactsBuffer", gpuData, &type);
        }
        {
            PeGpuObjectType type = PeGpuObjectType::getObjectType<PeGpuDescription, uint32_t>(256, false, sizeof(uint32_t));
            buffers.indexesTypeContainerBuffer = PH_NEW(PeGpuObjectsBuffer)("IndexesTypeContainerBuffer", gpuData, &type);
        }
    }

    void PeContactsGenerateSystem::release()
    {
        PH_RELEASE(buffers.indexesTypeContainerBuffer);
        PH_RELEASE(buffers.newContactsBuffer);

        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            if (shaders.generateContactS[i] != VK_NULL_HANDLE)
                shaderProvider->destroyShaderAutoParameters(shaders.generateContactS[i]);
        }
    }

    void PeContactsGenerateSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        memoryBarrierController->initUnknowMemory(buffers.indexesTypeContainerBuffer);
        memoryBarrierController->initUnknowMemory(buffers.newContactsBuffer);

        gpuData->computeCommands->debugMarkerBegin("ContactsGeneratePrepareData", PeDebugMarkersUtils::passCallColor());

        uint32_t countContainers = _simulationData->tableContacts.contactsContainersBuffers[0]->getTypeDescription().getMaxCountObjects();

        reallocateDatasSystem->ensureObjectsBuffer(countContainers, buffers.indexesTypeContainerBuffer, &buffers.indexesTypeContainerBuffer);
        reallocateDatasSystem->ensureObjectsBuffer(countContainers, buffers.newContactsBuffer, &buffers.newContactsBuffer);

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsGenerateSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("ContactsGenerateSimulation", PeDebugMarkersUtils::passCallColor());

        PeGpuBuffer *containersData = tableContactsSystem->getContactsCopySystem()->getContainersData();

        // Barrier`s allready set into INDIRECT call
        PeGpuBuffer *updateContainersGroupIDB = tableContactsSystem->getContactsCopySystem()->getUpdateContainersGroupIDB();

        // Barrier`s allready set into shader read call
        PeGpuObjectsBuffer *containers = _simulationData->tableContacts.contactsContainersBuffers[_simulationData->tableContacts.indexLastBuffer];

        {
            memoryBarrierController->setBuffer(0, containersData, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(1, containers, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(2, _simulationData->geometryOBBs.geometryOrientationObbsBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(3, _simulationData->geometryOBBs.geometryObbsBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(4, shapeDescriptionSystem->getGeometriesBuffer(), PeTypeAccess_Read);
            memoryBarrierController->setBuffer(5, buffers.newContactsBuffer, PeTypeAccess_Write);
            memoryBarrierController->setBuffer(6, buffers.indexesTypeContainerBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 7, 0);

            memoryBarrierController->setBuffer(0, updateContainersGroupIDB, PeTypeAccess_IndirectCallRead);
            memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
        }

        struct
        {
            uint32_t indexContactType;
        } pConstants;

        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            if (shaders.generateContactL[i] == VK_NULL_HANDLE)
                continue;

            gpuData->computeCommands->debugMarkerBegin("GenerateContact", PeDebugMarkersUtils::subPassColor());

            vk::ShaderAutoParameters *parameters = shaders.generateContactS[i];

            parameters->beginUpdate();
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *containersData);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *containers);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->geometryOBBs.geometryOrientationObbsBuffer);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->geometryOBBs.geometryObbsBuffer);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *shapeDescriptionSystem->getGeometriesBuffer());
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *buffers.newContactsBuffer);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *buffers.indexesTypeContainerBuffer);
            parameters->endUpdate();

            pConstants.indexContactType = i;

            gpuData->computeCommands->bindPipeline(shaders.generateContactP[i]);
            gpuData->computeCommands->bindDescriptorSet(shaders.generateContactL[i],
                                                        parameters->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.generateContactL[i], 0,
                                                   sizeof(pConstants), &pConstants);

            gpuData->computeCommands->dispatchIndirect(*updateContainersGroupIDB, sizeof(PeGpuDataDispatchIndirect) * i);

            gpuData->computeCommands->debugMarkerEnd();
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsGenerateSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("ContactsGenerateReceive", PeDebugMarkersUtils::passCallColor());

        gpuData->computeCommands->debugMarkerEnd();
    }
}