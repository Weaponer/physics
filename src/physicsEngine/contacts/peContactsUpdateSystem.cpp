#include "physicsEngine/contacts/peContactsUpdateSystem.h"

namespace phys
{
    PeContactsUpdateSystem::PeContactsUpdateSystem()
        : PeSystem(),
          gpuData(NULL),
          shaderProvider(NULL),
          staticReader(NULL),
          grabObjectsSystem(NULL),
          reallocateDatasSystem(NULL),
          tableContactsSystem(NULL)
    {
    }

    void PeContactsUpdateSystem::onInit()
    {
        gpuData = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
        shaderProvider = simulationContext->get<PeShaderProvider>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsSystem = simulationContext->get<PeGrabObjectsDatasSystem>();
        reallocateDatasSystem = simulationContext->get<PeReallocateDatasSystem>();
        shapeDescriptionSystem = simulationContext->get<PeShapeDescriptionSystem>();
        tableContactsSystem = simulationContext->get<PeTableContactsSystem>();

        const char *names[PeConstantsUtils::PeComputeTypeContact_CountAll];
        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            names[i] = PeConstantsUtils::getShaderNameTypeContact(PeConstantsUtils::PeCompute_ContactsUpdate,
                                                                  (PeConstantsUtils::PeComputeTypeContact)i);
            if (names[i] == NULL)
            {
                shaders.updateContactP[i] = vk::ComputePipeline();
                shaders.updateContactL[i] = VK_NULL_HANDLE;
                shaders.updateContactS[i] = NULL;
            }
            else
            {
                shaders.updateContactP[i] = shaderProvider->getPipeline(names[i]);
                shaders.updateContactL[i] = shaderProvider->getPipelineLayout(names[i]);
                shaders.updateContactS[i] = shaderProvider->createShaderParameters(names[i], 1);
            }
        }

        {
            PeGpuObjectType type = PeGpuObjectType::getObjectType<PeGpuDescription, PeGpuDataContact>(256, false, sizeof(PeGpuDataContact));
            buffers.updatedContactsBuffer = PH_NEW(PeGpuObjectsBuffer)("UpdatedContactsBuffer", gpuData, &type);
        }
        {
            PeGpuObjectType type = PeGpuObjectType::getObjectType<PeGpuDescription, uint32_t>(256, false, sizeof(uint32_t));
            buffers.isContactActiveBuffer = PH_NEW(PeGpuObjectsBuffer)("IsContactActiveBuffer", gpuData, &type);
        }
    }

    void PeContactsUpdateSystem::release()
    {
        PH_RELEASE(buffers.isContactActiveBuffer)
        PH_RELEASE(buffers.updatedContactsBuffer)

        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            if (shaders.updateContactS[i] != VK_NULL_HANDLE)
                shaderProvider->destroyShaderAutoParameters(shaders.updateContactS[i]);
        }
    }

    void PeContactsUpdateSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        memoryBarrierController->initUnknowMemory(buffers.updatedContactsBuffer);
        memoryBarrierController->initUnknowMemory(buffers.isContactActiveBuffer);

        gpuData->computeCommands->debugMarkerBegin("ContactsUpdatePrepareData", PeDebugMarkersUtils::passCallColor());

        uint32_t countContacts = _simulationData->tableContacts.contactsBuffers[0]->getTypeDescription().getMaxCountObjects();
        reallocateDatasSystem->ensureObjectsBuffer(countContacts, buffers.updatedContactsBuffer, &buffers.updatedContactsBuffer);
        reallocateDatasSystem->ensureObjectsBuffer(countContacts, buffers.isContactActiveBuffer, &buffers.isContactActiveBuffer);

        {
            vk::VulkanBuffer *buffer = *buffers.isContactActiveBuffer;
            gpuData->computeCommands->fillBuffer(buffer, 0, buffer->getSize(), 0);
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsUpdateSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpuData->computeCommands->debugMarkerBegin("ContactsUpdateSimulation", PeDebugMarkersUtils::passCallColor());

        // Barrier`s allready set into shader read call
        PeGpuBuffer *containersData = tableContactsSystem->getContactsCopySystem()->getContainersData();
        PeGpuBuffer *updateContainersGroupIDB = tableContactsSystem->getContactsCopySystem()->getUpdateContainersGroupIDB();

        PeGpuObjectsBuffer *currentContacts = _simulationData->tableContacts.contactsBuffers[_simulationData->tableContacts.indexLastBuffer];

        PeGpuObjectsBuffer *containers = _simulationData->tableContacts.contactsContainersBuffers[_simulationData->tableContacts.indexLastBuffer];

        struct
        {
            uint32_t indexContactType;
        } pConstants;

        {
            memoryBarrierController->setBuffer(0, containersData, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(1, containers, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(2, _simulationData->geometryOBBs.geometryOrientationObbsBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(3, _simulationData->geometryOBBs.geometryObbsBuffer, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(4, shapeDescriptionSystem->getGeometriesBuffer(), PeTypeAccess_Read);
            memoryBarrierController->setBuffer(5, currentContacts, PeTypeAccess_Read);
            memoryBarrierController->setBuffer(6, buffers.updatedContactsBuffer, PeTypeAccess_Write);
            memoryBarrierController->setBuffer(7, buffers.isContactActiveBuffer, PeTypeAccess_Write);
            memoryBarrierController->setupBarrier(PeTypeStage_Compute, 8, 0);

            memoryBarrierController->setBuffer(0, updateContainersGroupIDB, PeTypeAccess_IndirectCallRead);
            memoryBarrierController->setupBarrier(PeTypeStage_IndirectCallCompute, 1, 0);
        }

        for (uint32_t i = 0; i < PeGpuPairGeometriesContact_Count; i++)
        {
            if (shaders.updateContactL[i] == VK_NULL_HANDLE)
                continue;
            gpuData->computeCommands->debugMarkerBegin("UpdateContact", PeDebugMarkersUtils::subPassColor());

            vk::ShaderAutoParameters *parameters = shaders.updateContactS[i];

            parameters->beginUpdate();
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 0, *containersData);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 1, *containers);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 2, *_simulationData->geometryOBBs.geometryOrientationObbsBuffer);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 3, *_simulationData->geometryOBBs.geometryObbsBuffer);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 4, *shapeDescriptionSystem->getGeometriesBuffer());
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 5, *currentContacts);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 6, *buffers.updatedContactsBuffer);
            parameters->setParameter<vk::ShaderParameterStorageBuffer>(0, 7, *buffers.isContactActiveBuffer);
            parameters->endUpdate();

            pConstants.indexContactType = i;

            gpuData->computeCommands->bindPipeline(shaders.updateContactP[i]);
            gpuData->computeCommands->bindDescriptorSet(shaders.updateContactL[i],
                                                        parameters->getCurrentSet(0), 0);
            gpuData->computeCommands->pushConstant(shaders.updateContactL[i], 0,
                                                   sizeof(pConstants), &pConstants);

            gpuData->computeCommands->dispatchIndirect(*updateContainersGroupIDB, sizeof(PeGpuDataDispatchIndirect) * i);
            gpuData->computeCommands->debugMarkerEnd();
        }

        gpuData->computeCommands->debugMarkerEnd();
    }

    void PeContactsUpdateSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
    }
}