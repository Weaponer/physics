#include "physicsEngine/contacts/peContactsSystem.h"

namespace phys
{
    PeContactsSystem::PeContactsSystem()
        : PeSystem(),
          contactsContext(),
          gpu(NULL),
          contactsGenerateSystem(),
          contactsUpdateSystem(),
          contactsUniteSystem()
    {
    }

    void PeContactsSystem::onInit()
    {
        contactsContext.setParent(simulationContext);
        contactsContext.bind(&contactsGenerateSystem);
        contactsContext.bind(&contactsUpdateSystem);
        contactsContext.bind(&contactsUniteSystem);

        contactsGenerateSystem.init(&contactsContext);
        contactsUpdateSystem.init(&contactsContext);
        contactsUniteSystem.init(&contactsContext);

        gpu = simulationContext->get<PeGpuData>();
    }

    void PeContactsSystem::release()
    {
        contactsUniteSystem.release();
        contactsUpdateSystem.release();
        contactsGenerateSystem.release();
        contactsContext.release();
    }

    void PeContactsSystem::prepareData(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("ContactsPrepareData", PeDebugMarkersUtils::subGroupColor());

        contactsGenerateSystem.prepareData(_simulationData);
        contactsUpdateSystem.prepareData(_simulationData);
        contactsUniteSystem.prepareData(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    void PeContactsSystem::buildPipeline(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("ContactsSimulation", PeDebugMarkersUtils::subGroupColor());

        contactsGenerateSystem.buildPipeline(_simulationData);
        contactsUpdateSystem.buildPipeline(_simulationData);
        contactsUniteSystem.buildPipeline(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    void PeContactsSystem::buildReceive(PeSceneSimulationData *_simulationData)
    {
        gpu->computeCommands->debugMarkerBegin("ContactsReceive", PeDebugMarkersUtils::subGroupColor());

        contactsGenerateSystem.buildReceive(_simulationData);
        contactsUpdateSystem.buildReceive(_simulationData);
        contactsUniteSystem.buildReceive(_simulationData);

        gpu->computeCommands->debugMarkerEnd();
    }

    PeContactsSystem::ResultContainersData PeContactsSystem::getResultContainersData()
    {
        ResultContainersData result;
        result.containersGroupIDB = contactsUniteSystem.getContainersGroupIDB();
        result.countContainersBuffer = contactsUniteSystem.getCountContainersBuffer();
        return result;
    }
}