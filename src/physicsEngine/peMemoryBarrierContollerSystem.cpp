#include "physicsEngine/peMemoryBarrierContollerSystem.h"

namespace phys
{
    PeMemoryBarrierControllerSystem::PeMemoryBarrierControllerSystem()
        : PeSystem(),
          gpu(NULL),
          currentStep(0)
    {
    }

    void PeMemoryBarrierControllerSystem::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
    }

    void PeMemoryBarrierControllerSystem::release()
    {
        PeSystem::release();
    }

    void PeMemoryBarrierControllerSystem::reset()
    {
        currentStep = 0;
    }

    void PeMemoryBarrierControllerSystem::doNextStep()
    {
        currentStep++;
    }

    void PeMemoryBarrierControllerSystem::initUnknowMemory(PeGpuMemory *_memory)
    {
        _memory->setLastStage(PeTypeStage_Unknow);
        _memory->setLastAccess(PeTypeAccess_Unknow);
        _memory->setNumLastStep(0);
    }

#define ASSERT PH_ASSERT(_i < maxInternalMemory, "I is more then max internal memory.")

    void PeMemoryBarrierControllerSystem::setImage(uint32_t _i, PeGpuMemoryImage *_m, PeTypeAccessData _a)
    {
        ASSERT
        images[_i] = _m;
        imagesA[_i] = _a;
    }

    void PeMemoryBarrierControllerSystem::setBuffer(uint32_t _i, PeGpuMemoryBuffer *_m, PeTypeAccessData _a)
    {
        ASSERT
        buffers[_i] = _m;
        buffersA[_i] = _a;
    }

    void PeMemoryBarrierControllerSystem::setupBarrier(PeTypeStageData _stage, uint32_t _buffes, uint32_t _images)
    {
        setupBarrier(_stage, _buffes, buffers, buffersA, _images, images, imagesA);
    }

    void PeMemoryBarrierControllerSystem::setupBarrier(PeTypeStageData _stage,
                                                       uint32_t _countBuffers, PeGpuMemoryBuffer **_memoryBuffers, PeTypeAccessData *_accessBuffers,
                                                       uint32_t _countImages, PeGpuMemoryImage **_memoryImages, PeTypeAccessData *_accessImages)
    {
        PH_ASSERT(_stage != PeTypeStage_Unknow, "PeStageData can`t be unknow.")

        const uint32_t countStages = PeTypeStage_IndirectCallCompute + 1;
        const uint32_t countAccesses = PeTypeAccess_IndirectCallRead + 1;

        const static VkPipelineStageFlags flagsStages[countStages] = {
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
            VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT};

        const static VkPipelineStageFlags flagsAccessB[countStages][countAccesses] = {
            {VK_ACCESS_NONE, VK_ACCESS_NONE, VK_ACCESS_NONE, VK_ACCESS_NONE},
            {VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT, VK_ACCESS_NONE},
            {VK_ACCESS_NONE, VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_ACCESS_SHADER_READ_BIT},
            {VK_ACCESS_NONE, VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_ACCESS_INDIRECT_COMMAND_READ_BIT}};

        const static VkPipelineStageFlags flagsAccessI[countStages][countAccesses] = {
            {VK_ACCESS_NONE, VK_ACCESS_NONE, VK_ACCESS_NONE, VK_ACCESS_NONE},
            {VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT, VK_ACCESS_NONE},
            {VK_ACCESS_NONE, VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_ACCESS_SHADER_READ_BIT},
            {VK_ACCESS_NONE, VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_ACCESS_SHADER_READ_BIT}};

        const static VkImageLayout layouts[countStages][countAccesses]{
            {VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_UNDEFINED},
            {VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL},
            {VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL},
            {VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL},
        };

        bool activeSrcStages[countStages];
        for (uint32_t i = 0; i < countStages; i++)
            activeSrcStages[i] = false;

        for (uint32_t i = 0; i < _countBuffers; i++)
        {
            if (_memoryBuffers[i]->getNumLastStep() != currentStep)
            {
                _memoryBuffers[i]->setLastStage(PeTypeStage_Unknow);
                _memoryBuffers[i]->setLastAccess(PeTypeAccess_Unknow);
                _memoryBuffers[i]->setNumLastStep(currentStep);
            }

            PeTypeStageData lastStage = _memoryBuffers[i]->getLastStage();
            activeSrcStages[(uint32_t)lastStage] = true;
        }

        for (uint32_t i = 0; i < _countImages; i++)
        {
            if (_memoryImages[i]->getNumLastStep() != currentStep)
            {
                _memoryImages[i]->setLastStage(PeTypeStage_Unknow);
                _memoryImages[i]->setLastAccess(PeTypeAccess_Unknow);
                _memoryImages[i]->setNumLastStep(currentStep);
            }

            PeTypeStageData lastStage = _memoryImages[i]->getLastStage();
            activeSrcStages[(uint32_t)lastStage] = true;
        }

        for (uint32_t i = 0; i < countStages; i++)
        {
            if (!activeSrcStages[i])
                continue;

            VkBufferMemoryBarrier bufferBarriers[_countBuffers];
            VkImageMemoryBarrier imageBarriers[_countImages];

            uint32_t writedBuffers = 0;
            uint32_t writedImages = 0;

            for (uint32_t i2 = 0; i2 < _countBuffers; i2++)
            {
                PeGpuMemoryBuffer *buffer = _memoryBuffers[i2];
                PeTypeAccessData needAccess = _accessBuffers[i2];
                PeTypeAccessData wasAccess = buffer->getLastAccess();
                if (wasAccess != PeTypeAccess_Write || buffer->getLastStage() != i)
                    continue;

                VkBufferMemoryBarrier bufferB{};
                bufferB.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
                bufferB.offset = 0;
                bufferB.size = VK_WHOLE_SIZE;
                bufferB.buffer = **(buffer);
                bufferB.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                bufferB.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                bufferB.srcAccessMask = flagsAccessB[i][wasAccess];
                bufferB.dstAccessMask = flagsAccessB[_stage][needAccess];

                bufferBarriers[writedBuffers] = bufferB;
                writedBuffers++;
            }

            for (uint32_t i2 = 0; i2 < _countImages; i2++)
            {
                PeGpuMemoryImage *image = _memoryImages[i2];
                PeTypeAccessData needAccess = _accessImages[i2];
                PeTypeAccessData wasAccess = image->getLastAccess();
                VkImageLayout lastLayout = layouts[i][wasAccess];
                VkImageLayout newLayout = layouts[_stage][needAccess];
                if (image->getLastStage() != i || (wasAccess != PeTypeAccess_Write && lastLayout == newLayout))
                    continue;

                vk::VulkanImage *nImage = *image;

                VkImageMemoryBarrier imageB{};
                imageB.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                imageB.image = *nImage;
                imageB.subresourceRange.aspectMask = nImage->getAspect();
                imageB.subresourceRange.baseArrayLayer = 0;
                imageB.subresourceRange.baseMipLevel = 0;
                imageB.subresourceRange.layerCount = nImage->getLayers();
                imageB.subresourceRange.levelCount = nImage->getCountMipmaps();
                imageB.srcAccessMask = flagsAccessI[i][wasAccess];
                imageB.dstAccessMask = flagsAccessI[_stage][needAccess];
                imageB.oldLayout = lastLayout;
                imageB.newLayout = newLayout;
                imageB.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                imageB.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

                imageBarriers[writedImages] = imageB;
                writedImages++;
            }

            if (writedImages == 0 && writedBuffers == 0)
                continue;

            gpu->computeCommands->pipelineBarrier(flagsStages[i], flagsStages[_stage], 0, 0, NULL,
                                                  writedBuffers, bufferBarriers, writedImages, imageBarriers);
        }

        for (uint32_t i = 0; i < _countBuffers; i++)
        {
            _memoryBuffers[i]->setLastStage(_stage);
            _memoryBuffers[i]->setLastAccess(_accessBuffers[i]);
        }

        for (uint32_t i = 0; i < _countImages; i++)
        {
            _memoryImages[i]->setLastStage(_stage);
            _memoryImages[i]->setLastAccess(_accessImages[i]);
        }
    }
}