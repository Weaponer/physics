#include "physicsEngine/peResultReceiverSystem.h"

namespace phys
{
    PeResultReceiverSystem::PeResultReceiverSystem()
        : PeSystem(), gpu(NULL), staticReader(NULL)
    {
    }

    void PeResultReceiverSystem::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        staticReader = simulationContext->get<PeGpuStaticReaderSystem>();
    }

    void PeResultReceiverSystem::release()
    {
    }

    void PeResultReceiverSystem::receiveData()
    {
        staticReader->receiveMemory();
        staticReader->resetEvents();
    }

    void PeResultReceiverSystem::sendData()
    {
        staticReader->sendEvents();
    }
}