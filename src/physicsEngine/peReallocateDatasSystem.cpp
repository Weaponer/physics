#include <cmath>

#include "physicsEngine/peReallocateDatasSystem.h"
#include "physicsEngine/common/peGpuBarrier.h"

namespace phys
{
    PeReallocateDatasSystem::PeReallocateDatasSystem()
        : PeSystem(),
          gpu(NULL), grabObjectsDatas(NULL)
    {
    }

    void PeReallocateDatasSystem::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        memoryBarrierController = simulationContext->get<PeMemoryBarrierControllerSystem>();
        grabObjectsDatas = simulationContext->get<PeGrabObjectsDatasSystem>();
    }

    void PeReallocateDatasSystem::release()
    {
    }

    bool PeReallocateDatasSystem::ensureObjectsBuffer(uint32_t _minimalCountObjects,
                                                      PeGpuObjectsBuffer *_currentBuffer,
                                                      PeGpuObjectsBuffer **_outputCurrentBuffer,
                                                      ReserveMemoryType _reserveMemoryType)
    {
        PeGpuObjectType type = _currentBuffer->getTypeDescription();
        if (_minimalCountObjects > type.getMaxCountObjects())
        {
            PeGpuObjectType newType = PeGpuObjectType(type, reserveCount(_minimalCountObjects, _reserveMemoryType));

            PeGpuObjectsBuffer *newBuffer = PH_NEW(PeGpuObjectsBuffer)(_currentBuffer->getName());
            newBuffer->initType(gpu, &newType);
            newBuffer->setHostVisibility(_currentBuffer->getHostVisibility());
            newBuffer->setShaderWriting(_currentBuffer->getShaderWriting());
            newBuffer->allocateData();

            PH_RELEASE(_currentBuffer);

            *_outputCurrentBuffer = newBuffer;
            return true;
        }
        return false;
    }

    bool PeReallocateDatasSystem::ensureObjectsBufferWithCopyAndGrap(uint32_t _minimalCountObjects,
                                                                     PeGpuObjectsBuffer *_currentBuffer,
                                                                     PeGpuObjectsBuffer **_outputCurrentBuffer,
                                                                     ReserveMemoryType _reserveMemoryType,
                                                                     uint32_t *_defaultValue)
    {
        PeGpuObjectType type = _currentBuffer->getTypeDescription();
        if (_minimalCountObjects > type.getMaxCountObjects())
        {
            PeGpuObjectType newType = PeGpuObjectType(type, reserveCount(_minimalCountObjects, _reserveMemoryType));
            PeGpuObjectsBuffer *newBuffer = PH_NEW(PeGpuObjectsBuffer)(_currentBuffer->getName());
            newBuffer->initType(gpu, &newType);
            newBuffer->setHostVisibility(_currentBuffer->getHostVisibility());
            newBuffer->setShaderWriting(_currentBuffer->getShaderWriting());
            newBuffer->allocateData();

            PeGpuMemoryBuffer *buff[2] = {_currentBuffer, newBuffer};
            PeTypeAccessData buffA[2] = {PeTypeAccess_Read, PeTypeAccess_Write};

            if (_defaultValue != NULL)
            {
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 1, buff + 1, buffA + 1, 0, NULL, NULL);
                gpu->computeCommands->fillBuffer(*newBuffer, 0, newBuffer->getSize(), *_defaultValue);
            }

            memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 2, buff, buffA, 0, NULL, NULL);
            gpu->computeCommands->copyBuffer(*_currentBuffer, 0,
                                             *newBuffer, 0,
                                             _currentBuffer->getSize());

            grabObjectsDatas->addDataToUnload(_currentBuffer);
            *_outputCurrentBuffer = newBuffer;
            return true;
        }
        return false;
    }

    bool PeReallocateDatasSystem::ensureTableContactsBuffer(uint32_t _minimalCountObjects,
                                                            PeGpuTableContactsBuffer *_currentTableContacts,
                                                            PeGpuTableContactsBuffer **_outputCurrentTableContacts,
                                                            ReserveMemoryType _reserveMemoryType)
    {
        if (_minimalCountObjects > _currentTableContacts->getCountIndexes())
        {
            PeGpuTableContactsBuffer *newTableContacts = PH_NEW(PeGpuTableContactsBuffer)(_currentTableContacts->getName());
            newTableContacts->init(gpu);
            newTableContacts->allocateData(reserveCount(_minimalCountObjects, _reserveMemoryType),
                                           _currentTableContacts->getCountContactsPerIndex(),
                                           _currentTableContacts->getFormat());

            PH_RELEASE(_currentTableContacts);

            *_outputCurrentTableContacts = newTableContacts;
            return true;
        }
        return false;
    }

    bool PeReallocateDatasSystem::ensureTableContactsBufferWithCopyAndGrap(uint32_t _minimalCountObjects,
                                                                           PeGpuTableContactsBuffer *_currentTableContacts,
                                                                           PeGpuTableContactsBuffer **_outputCurrentTableContacts,
                                                                           ReserveMemoryType _reserveMemoryType,
                                                                           VkClearColorValue *_clearColorValue)
    {
        if (_minimalCountObjects > _currentTableContacts->getCountIndexes())
        {
            PeGpuTableContactsBuffer *newTableContacts = PH_NEW(PeGpuTableContactsBuffer)(_currentTableContacts->getName());
            newTableContacts->init(gpu);
            newTableContacts->allocateData(reserveCount(_minimalCountObjects, _reserveMemoryType),
                                           _currentTableContacts->getCountContactsPerIndex(),
                                           _currentTableContacts->getFormat());

            PeGpuMemoryImage *images[2] = {_currentTableContacts, newTableContacts};

            VkClearColorValue value{};
            if (_clearColorValue != NULL)
            {
                value = *_clearColorValue;
            }
            else
            {
                value.int32[0] = value.int32[1] = value.int32[2] = value.int32[3] = -1;
                value.uint32[0] = value.uint32[1] = value.uint32[2] = value.uint32[3] = 0;
            }
            {
                PeGpuMemoryImage *img = images[1];
                PeTypeAccessData imgA = PeTypeAccess_Write;
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, 1, &img, &imgA);
            }
            gpu->computeCommands->clearImage(*images[1], &value, 0, 1, 0, newTableContacts->getCountLayers());

            {
                PeTypeAccessData imgA[2] = {PeTypeAccess_Read, PeTypeAccess_Write};
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, 2, images, imgA);
            }
            gpu->computeCommands->copyImage(*images[0], *images[1], 0, 0, _currentTableContacts->getCountLayers());

            grabObjectsDatas->addDataToUnload(_currentTableContacts);
            *_outputCurrentTableContacts = newTableContacts;
            return true;
        }
        return false;
    }

    bool PeReallocateDatasSystem::ensureBuffer(uint32_t _minimalSize, vk::MemoryUseType _useType, vk::VulkanBufferType _type,
                                               PeGpuBuffer *_currentBuffer,
                                               PeGpuBuffer **_outputCurrentBuffer,
                                               ReserveMemoryType _reserveMemoryType)
    {
        if (_minimalSize > _currentBuffer->getSize())
        {
            uint32_t newSize = reserveCount(_minimalSize, _reserveMemoryType);

            PeGpuBuffer *newBuffer = PH_NEW(PeGpuBuffer)(_currentBuffer->getName());
            newBuffer->init(gpu);
            newBuffer->allocateData(_useType, _type, newSize);

            PH_RELEASE(_currentBuffer)

            *_outputCurrentBuffer = newBuffer;
            return true;
        }
        return false;
    }

    bool PeReallocateDatasSystem::ensureBufferWithCopyAndGrap(uint32_t _minimalSize, vk::MemoryUseType _useType, vk::VulkanBufferType _type,
                                                              PeGpuBuffer *_currentBuffer,
                                                              PeGpuBuffer **_outputCurrentBuffer,
                                                              ReserveMemoryType _reserveMemoryType,
                                                              uint32_t *_defaultValue)
    {
        if (_minimalSize > _currentBuffer->getSize())
        {
            uint32_t newSize = reserveCount(_minimalSize, _reserveMemoryType);

            PeGpuBuffer *newBuffer = PH_NEW(PeGpuBuffer)(_currentBuffer->getName());
            newBuffer->init(gpu);
            newBuffer->allocateData(_useType, _type, newSize);

            PeGpuMemoryBuffer *buff[2] = {_currentBuffer, newBuffer};
            PeTypeAccessData buffA[2] = {PeTypeAccess_Read, PeTypeAccess_Write};

            if (_defaultValue != NULL)
            {
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 1, buff + 1, buffA + 1, 0, NULL, NULL);
                gpu->computeCommands->fillBuffer(*newBuffer, 0, newBuffer->getSize(), *_defaultValue);
            }

            memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 2, buff, buffA, 0, NULL, NULL);
            gpu->computeCommands->copyBuffer(*_currentBuffer, 0,
                                             *newBuffer, 0,
                                             _currentBuffer->getSize());

            grabObjectsDatas->addDataToUnload(_currentBuffer);
            *_outputCurrentBuffer = newBuffer;
            return true;
        }
        return false;
    }

    bool PeReallocateDatasSystem::ensureImage(uint32_t _minimalWidth, uint32_t _minimalHeight,
                                              PeGpuImage *_currentImage,
                                              PeGpuImage **_outputCurrentImage,
                                              ReserveMemoryType _reserveMemoryType)
    {
        bool reallocate = false;
        if (_minimalWidth > (uint32_t)_currentImage->getWidth())
        {
            reallocate = true;
            _minimalWidth = reserveCount(_minimalWidth, _reserveMemoryType);
        }
        if (_minimalHeight > (uint32_t)_currentImage->getHeight())
        {
            reallocate = true;
            _minimalHeight = reserveCount(_minimalHeight, _reserveMemoryType);
        }

        if (reallocate)
        {
            PeGpuImage *newImage = PH_NEW(PeGpuImage)(_currentImage->getName());
            newImage->init(gpu);
            newImage->allocateData(_currentImage->getImageType(), _currentImage->getFormat(),
                                   _minimalWidth, _minimalHeight, _currentImage->getCountLayers(),
                                   _currentImage->getImage()->isHostVisible());

            PH_RELEASE(_currentImage);

            *_outputCurrentImage = newImage;
        }
        return reallocate;
    }

    bool PeReallocateDatasSystem::ensureImageWithCopyAndGrap(uint32_t _minimalWidth, uint32_t _minimalHeight,
                                                             PeGpuImage *_currentImage,
                                                             PeGpuImage **_outputCurrentImage,
                                                             ReserveMemoryType _reserveMemoryType,
                                                             VkClearColorValue *_clearColorValue)
    {
        bool reallocate = false;
        if (_minimalWidth > (uint32_t)_currentImage->getWidth())
        {
            reallocate = true;
            _minimalWidth = reserveCount(_minimalWidth, _reserveMemoryType);
        }
        if (_minimalHeight > (uint32_t)_currentImage->getHeight())
        {
            reallocate = true;
            _minimalHeight = reserveCount(_minimalHeight, _reserveMemoryType);
        }

        if (reallocate)
        {
            PeGpuImage *newImage = PH_NEW(PeGpuImage)(_currentImage->getName());
            newImage->init(gpu);
            newImage->allocateData(_currentImage->getImageType(), _currentImage->getFormat(),
                                   _minimalWidth, _minimalHeight, _currentImage->getCountLayers(),
                                   _currentImage->getImage()->isHostVisible());

            PeGpuMemoryImage *images[2] = {_currentImage, newImage};

            VkClearColorValue value{};
            if (_clearColorValue != NULL)
            {
                value = *_clearColorValue;
            }
            else
            {
                value.int32[0] = value.int32[1] = value.int32[2] = value.int32[3] = -1;
                value.uint32[0] = value.uint32[1] = value.uint32[2] = value.uint32[3] = 0;
            }
            {
                PeGpuMemoryImage *img = images[1];
                PeTypeAccessData imgA = PeTypeAccess_Write;
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, 1, &img, &imgA);
            }
            gpu->computeCommands->clearImage(*images[1], &value, 0, 1, 0, newImage->getCountLayers());

            {
                PeTypeAccessData imgA[2] = {PeTypeAccess_Read, PeTypeAccess_Write};
                memoryBarrierController->setupBarrier(PeTypeStage_Transfer, 0, NULL, NULL, 2, images, imgA);
            }
            gpu->computeCommands->copyImage(*images[0], *images[1], 0, 0, _currentImage->getCountLayers());

            grabObjectsDatas->addDataToUnload(_currentImage);
            
            *_outputCurrentImage = newImage;
        }
        return reallocate;
    }

    uint32_t PeReallocateDatasSystem::reserveCount(uint32_t _count, ReserveMemoryType _reserveMemoryType)
    {
        return (uint32_t)std::ceil(_count * ((float)_reserveMemoryType / 100.0f));
    }
}