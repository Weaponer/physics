#include "physicsEngine/peGpuStaticReaderSystem.h"

#include "common/phMacros.h"

namespace phys
{
    PeGpuStaticReaderSystem::PeGpuStaticReaderSystem()
        : PeSystem(), gpu(NULL), buffers(), events(), imageEvents(),
          indexCurrentBuffer(0), offsetCurrentBuffer(0),
          sizeBuffer(8388608) // 8 mb
    {
    }

    void PeGpuStaticReaderSystem::onInit()
    {
        gpu = simulationContext->get<PeGpuData>();
        vk::VulkanBuffer *mainBuffer = createBuffer();
        buffers.assignmentMemoryAndCopy(&mainBuffer);
    }

    void PeGpuStaticReaderSystem::release()
    {
        uint32_t count = buffers.getPos();
        for (uint32_t i = 0; i < count; i++)
            gpu->bufferAllocator->destroyBuffer(*buffers.getMemory(i));
        buffers.release();
        imageEvents.release();
        events.release();
    }

    PeGpuStaticReaderSystem::ReadMemory *PeGpuStaticReaderSystem::createReadMemoryPoint(uint32_t _size)
    {
        PeGpuStaticReaderSystem::ReadMemory *memory = PH_NEW(PeGpuStaticReaderSystem::ReadMemory)();
        memory->isValid = false;
        memory->size = _size;
        memory->memory = (uint8_t *)phAllocateMemory(_size);
        return memory;
    }

    void PeGpuStaticReaderSystem::destroyReadMemoryPoint(ReadMemory *_memory)
    {
        phDeallocateMemory(_memory->memory);
        PH_RELEASE(_memory);
    }

    PeGpuStaticReaderSystem::ReadImageMemory *PeGpuStaticReaderSystem::createReadImageMemoryPoint(uint32_t _size)
    {
        PeGpuStaticReaderSystem::ReadImageMemory *memory = PH_NEW(PeGpuStaticReaderSystem::ReadImageMemory)();
        memory->isValid = false;
        memory->size = _size;
        memory->stagingBuffer = gpu->bufferAllocator->createBuffer(vk::MemoryUseType_Medium, vk::VulkanBufferType_Staging,
                                                                   _size, 1, &gpu->mainFamilyQueue);
        memory->memory = (uint8_t *)phAllocateMemory(_size);
        return memory;
    }

    PeGpuStaticReaderSystem::ReadImageMemory *PeGpuStaticReaderSystem::createReadImageMemoryPoint(vk::VulkanImage *_image)
    {
        PH_ASSERT(_image->getSize() > 0, "The image hasn't a valid memory.");
        return createReadImageMemoryPoint(_image->getSize());
    }

    void PeGpuStaticReaderSystem::destroyReadImageMemoryPoint(ReadImageMemory *_memory)
    {
        gpu->bufferAllocator->destroyBuffer(_memory->stagingBuffer);
        phDeallocateMemory(_memory->memory);
        PH_RELEASE(_memory);
    }

    void PeGpuStaticReaderSystem::sendEvents()
    {
        uint32_t countEvents = events.getPos();
        for (uint32_t i = 0; i < countEvents; i++)
        {
            ReadEvent event = *events.getMemory(i);
            vk::VulkanBuffer *buffer = *buffers.getMemory(event.indexDstBuffer);
            gpu->computeCommands->copyBuffer(event.srcBuffer, event.offsetSrc, buffer, event.offsetDstBuffer, event.size);
        }

        countEvents = imageEvents.getPos();
        for (uint32_t i = 0; i < countEvents; i++)
        {
            ReadImageEvent event = *imageEvents.getMemory(i);
            VkBufferImageCopy copyInfo{};
            copyInfo.bufferImageHeight = 0;
            copyInfo.bufferRowLength = 0;
            copyInfo.bufferOffset = event.offsetDstMemory;
            copyInfo.imageExtent = event.imageExtent;
            copyInfo.imageOffset = event.imageOffset;
            copyInfo.imageSubresource = event.imageSubresource;
            gpu->computeCommands->copyImageToBuffer(event.srcImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                                                    event.dstMemory->stagingBuffer, &copyInfo);
        }
    }

    void PeGpuStaticReaderSystem::receiveMemory()
    {
        uint32_t countEvents = events.getPos();
        vk::VulkanBuffer *openedBuffer = NULL;
        int32_t openedBufferIndex = -1;
        uint8_t *pWrite = NULL;
        for (uint32_t i = 0; i < countEvents; i++)
        {
            ReadEvent event = *events.getMemory(i);
            if ((int32_t)event.indexDstBuffer != openedBufferIndex)
            {
                if (openedBuffer)
                    gpu->computeCommands->endWrite(openedBuffer);

                openedBuffer = *buffers.getMemory(event.indexDstBuffer);
                pWrite = (uint8_t *)gpu->computeCommands->beginWrite(openedBuffer, 0, sizeBuffer);
                openedBufferIndex = event.indexDstBuffer;
            }

            std::memcpy(event.dstMemory->memory + event.offsetDstMemory, pWrite + event.offsetDstBuffer, event.size);

            event.dstMemory->isValid = true;
        }

        if (openedBuffer)
            gpu->computeCommands->endWrite(openedBuffer);

        countEvents = imageEvents.getPos();
        for (uint32_t i = 0; i < countEvents; i++)
        {
            ReadImageEvent event = *imageEvents.getMemory(i);
            ReadImageMemory *readPoint = event.dstMemory;
            uint8_t *pWrite = (uint8_t *)gpu->computeCommands->beginWrite(readPoint->stagingBuffer, 0, readPoint->size);
            std::memcpy(readPoint->memory, pWrite, readPoint->size);
            readPoint->isValid = true;
            gpu->computeCommands->endWrite(readPoint->stagingBuffer);
        }
    }

    void PeGpuStaticReaderSystem::resetEvents()
    {
        imageEvents.clearMemory();
        events.clearMemory();
        indexCurrentBuffer = 0;
        offsetCurrentBuffer = 0;
    }

    void PeGpuStaticReaderSystem::addRequest(vk::VulkanBuffer *_src, uint32_t _srcOffset, ReadMemory *_dst, uint32_t _dstOffset, uint32_t _size)
    {
        /*if(!((_size + _dstOffset) < _dst->size))
        {
            float debugPoint = 0.0f;
        }*/

        PH_ASSERT((_size + _dstOffset) <= _dst->size, "The offset + size of writing memory more greater than size memory.");

        uint32_t wrote = 0;
        while (wrote < _size)
        {
            uint32_t canWrite = std::min(std::max(sizeBuffer - offsetCurrentBuffer, 0U), _size - wrote);
            if (canWrite)
            {
                ReadEvent event;
                event.size = canWrite;
                event.dstMemory = _dst;
                event.offsetDstMemory = _dstOffset + wrote;
                event.srcBuffer = _src;
                event.offsetSrc = _srcOffset + wrote;

                event.indexDstBuffer = indexCurrentBuffer;
                event.offsetDstBuffer = offsetCurrentBuffer;
                events.assignmentMemoryAndCopy(&event);
            }

            wrote += canWrite;
            offsetCurrentBuffer += canWrite;
            if (offsetCurrentBuffer >= sizeBuffer)
                allocateNextBuffer();
        }

        _dst->isValid = false;
    }

    void PeGpuStaticReaderSystem::addRequest(vk::VulkanImage *_src, ReadImageMemory *_dst, uint32_t _dstOffset)
    {
        VkImageSubresourceLayers subresource;
        subresource.aspectMask = _src->getAspect();
        subresource.baseArrayLayer = 0;
        subresource.layerCount = 1;
        subresource.mipLevel = 0;

        VkOffset3D offset;
        offset.x = 0;
        offset.y = 0;
        offset.z = 0;

        VkExtent3D extent;
        extent.width = _src->getWidth();
        extent.height = _src->getHeight();
        extent.depth = _src->getDepth();

        addRequest(_src, _dst, _dstOffset, subresource, offset, extent);
    }

    void PeGpuStaticReaderSystem::addRequest(vk::VulkanImage *_src, ReadImageMemory *_dst, uint32_t _dstOffset,
                                             VkImageSubresourceLayers _imageSubresource, VkOffset3D _imageOffset, VkExtent3D _imageExtent)
    {
        ReadImageEvent event;
        event.dstMemory = _dst;
        event.srcImage = _src;
        event.offsetDstMemory = _dstOffset;
        event.imageSubresource = _imageSubresource;
        event.imageOffset = _imageOffset;
        event.imageExtent = _imageExtent;

        imageEvents.assignmentMemoryAndCopy(&event);
    }

    vk::VulkanBuffer *PeGpuStaticReaderSystem::createBuffer()
    {
        return gpu->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_Staging, sizeBuffer,
                                                  1, &gpu->mainFamilyQueue);
    }

    void PeGpuStaticReaderSystem::allocateNextBuffer()
    {
        if ((indexCurrentBuffer + 1) >= buffers.getPos())
        {
            vk::VulkanBuffer *buffer = createBuffer();
            buffers.assignmentMemoryAndCopy(&buffer);
        }

        indexCurrentBuffer += 1;
        offsetCurrentBuffer = 0;
    }
}