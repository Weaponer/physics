#include "physicsEngine/peShaderProvider.h"

#include "common/phMacros.h"

namespace phys
{
#define PHYSICS_SHADER_TAG "physics"
#define NOT_FOUND_PHYSICS_SHADER_ERROR "The Physics Shader doesn`t found."

    PeShaderProvider::PeShaderProvider()
        : PhBase(), computePipelines(),
          provider(NULL), device(NULL), shaderManager(NULL), poolManager(NULL)
    {
    }

    void PeShaderProvider::init(PhResourceProvider *_provider, vk::VulkanDevice *_device)
    {
        provider = _provider;
        device = _device;

        shaderManager = new vk::ShaderManager(_device);
        poolManager = new vk::VulkanDescriptorPoolManager(_device);

        if (!provider->getCountShaders(PHYSICS_SHADER_TAG))
        {
            provider->loadShadersPacket("physics.spak", PHYSICS_SHADER_TAG);
        }

        uint32_t count = provider->getCountShaders(PHYSICS_SHADER_TAG);
        vk::ShaderData *const *data = provider->getShadersPerTag(PHYSICS_SHADER_TAG);
        for (uint32_t i = 0; i < count; i++)
        {
            vk::ShaderData *shaderData = data[i];
            shaderData->prepareData(_device);

            vk::ShaderModule module;
            if (!shaderManager->registerShader(shaderData, &module))
                continue;

            const char *name = shaderData->getName();
            uint32_t lenName = std::strlen(name) + 1;
            char *copyName = (char *)phAllocateMemory(sizeof(char) * lenName);
            copyName[lenName - 1] = '\0';
            std::strcpy(copyName, name);

            ComputePipelineData pipelineData;
            pipelineData.name = copyName;
            pipelineData.shaderModule = module;
            pipelineData.pipelineLayout = shaderData->getPipelineLayout();
            vk::ComputePipeline::createComputePipeline(device, module, &pipelineData.computePipeline);

            computePipelines.assignmentMemoryAndCopy(&pipelineData);
        }

        poolManager->preparePools(shaderManager);
    }

    void PeShaderProvider::release()
    {
        if (!provider)
            return;

        {
            uint32_t count = computePipelines.getPos();
            for (uint32_t i = 0; i < count; i++)
            {
                ComputePipelineData data = *computePipelines.getMemory(i);
                phDeallocateMemory(data.name);
                vk::ComputePipeline::destroyComputePipeline(device, &data.computePipeline);
            }
            computePipelines.release();
        }
        delete poolManager;
        delete shaderManager;
    }

    vk::ShaderModule PeShaderProvider::getShaderModul(const char *_name)
    {
        ComputePipelineData data;
        PH_ASSERT(tryToFindData(_name, &data), NOT_FOUND_PHYSICS_SHADER_ERROR);
        return data.shaderModule;
    }

    vk::ComputePipeline PeShaderProvider::getPipeline(const char *_name)
    {
        ComputePipelineData data;
        PH_ASSERT(tryToFindData(_name, &data), NOT_FOUND_PHYSICS_SHADER_ERROR);
        return data.computePipeline;
    }

    VkPipelineLayout PeShaderProvider::getPipelineLayout(const char *_name)
    {
        ComputePipelineData data;
        PH_ASSERT(tryToFindData(_name, &data), NOT_FOUND_PHYSICS_SHADER_ERROR);
        return data.pipelineLayout;
    }

    vk::VulkanDescriptorPoolManager *PeShaderProvider::getVulkanDescriptorPoolManager()
    {
        return poolManager;
    }

    vk::ShaderAutoParameters *PeShaderProvider::createShaderParameters(const char *_shaderName, uint _countSets)
    {
        ComputePipelineData data;
        PH_ASSERT(tryToFindData(_shaderName, &data), NOT_FOUND_PHYSICS_SHADER_ERROR);
        vk::ShaderAutoParameters *params = new vk::ShaderAutoParameters(device, data.shaderModule);
        params->takeMemoryPools(poolManager, _countSets);
        return params;
    }

    void PeShaderProvider::destroyShaderAutoParameters(vk::ShaderAutoParameters *_parameters)
    {
        _parameters->freeMemoryPools(poolManager);
        delete _parameters;
    }

    bool PeShaderProvider::tryToFindData(const char *_name, ComputePipelineData *_outData)
    {
        uint32_t count = computePipelines.getPos();
        for (uint32_t i = 0; i < count; i++)
        {
            ComputePipelineData data = *computePipelines.getMemory(i);
            if (std::strcmp(data.name, _name) == 0)
            {
                *_outData = data;
                return true;
            }
        }
        return false;
    }
}