#include <iostream>

#include "physicsEngine/peSceneSimulator.h"
#include "physicsEngine/peFoundation.h"
#include "physicsEngine/peDebugMarkerColors.h"

namespace phys
{
    PeSceneSimulator::PeSceneSimulator(PeFoundation *_foundation)
        : PhBase(), foundation(_foundation), device(NULL),
          simulationContext(),
          gpuData(),
          shaderProvider(),
          memoryBarrierControllerSystem(),
          staticLoaderSystem(), staticReaderSystem(), grabObjectsDatasSystem(), reallocateDatasSystem(),
          sceneSettingsSystem(),
          resultReceiverSystem(), queueCommandsBuilderSystem(),

          materialDescriptionSystem(),
          shapeDescriptionSystem(),

          sceneActrosDescription(),
          actorDynamicDescription(),
          actorShapesDescription(),

          geometryObbDescription(),
          potentialContactsSystem(),
          tableContactsSystem(),
          contactsSystem(),
          inertialTensorsSystem(),
          physicsSimulationSystem(),

          fetchSceneData(),

          sceneInSimulation(NULL)

    {
        device = foundation->getPhFoundation()->getGpuFoundation()->getDevice();

        GPUFoundation *gpuF = foundation->getPhFoundation()->getGpuFoundation();
        gpuData.device = device;
        gpuData.commandReceiver = gpuF->getCommandReceiver();
        gpuData.queue = gpuData.commandReceiver->getFreeQueue(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT);
        gpuData.bufferAllocator = gpuF->getBufferAllocator();
        gpuData.imageAllocator = gpuF->getImageAllocator();
        gpuData.computeCommands = new vk::VulkanComputeCommands(gpuData.queue);
        gpuData.mainFamilyQueue = gpuData.queue->getFamilyIndex();

        simulationContext.bind(&gpuData);
        simulationContext.bind(&shaderProvider);
        simulationContext.bind(&memoryBarrierControllerSystem);
        simulationContext.bind(&staticLoaderSystem);
        simulationContext.bind(&staticReaderSystem);
        simulationContext.bind(&grabObjectsDatasSystem);
        simulationContext.bind(&reallocateDatasSystem);
        simulationContext.bind(&sceneSettingsSystem);
        simulationContext.bind(&resultReceiverSystem);
        simulationContext.bind(&queueCommandsBuilderSystem);
        simulationContext.bind(&materialDescriptionSystem);
        simulationContext.bind(&shapeDescriptionSystem);

        simulationContext.bind(&sceneActrosDescription);
        simulationContext.bind(&actorDynamicDescription);
        simulationContext.bind(&actorShapesDescription);

        simulationContext.bind(&geometryObbDescription);

        simulationContext.bind(&potentialContactsSystem);
        simulationContext.bind(&tableContactsSystem);
        simulationContext.bind(&contactsSystem);
        simulationContext.bind(&inertialTensorsSystem);
        simulationContext.bind(&physicsSimulationSystem);
        
        simulationContext.bind(&fetchSceneData);

        simulationContext.bind(_foundation->getMaterialManger());
        simulationContext.bind(_foundation->getShapeManager());

        shaderProvider.init(foundation->getPhFoundation()->getResourceProvider(), device);
        memoryBarrierControllerSystem.init(&simulationContext);
        staticLoaderSystem.init(&simulationContext);
        staticReaderSystem.init(&simulationContext);
        grabObjectsDatasSystem.init(&simulationContext);
        reallocateDatasSystem.init(&simulationContext);
        sceneSettingsSystem.init(&simulationContext);
        resultReceiverSystem.init(&simulationContext);
        queueCommandsBuilderSystem.init(&simulationContext);
        materialDescriptionSystem.init(&simulationContext);
        shapeDescriptionSystem.init(&simulationContext);

        sceneActrosDescription.init(&simulationContext);
        actorDynamicDescription.init(&simulationContext);
        actorShapesDescription.init(&simulationContext);

        geometryObbDescription.init(&simulationContext);

        potentialContactsSystem.init(&simulationContext);
        tableContactsSystem.init(&simulationContext);
        contactsSystem.init(&simulationContext);
        inertialTensorsSystem.init(&simulationContext);
        physicsSimulationSystem.init(&simulationContext);

        fetchSceneData.init(&simulationContext);
    }

    void PeSceneSimulator::release()
    {
        if (sceneInSimulation)
            fetchScene(sceneInSimulation, true);

        fetchSceneData.release();

        physicsSimulationSystem.release();
        inertialTensorsSystem.release();
        contactsSystem.release();
        tableContactsSystem.release();
        potentialContactsSystem.release();

        geometryObbDescription.release();

        actorShapesDescription.release();
        actorDynamicDescription.release();
        sceneActrosDescription.release();

        shapeDescriptionSystem.release();
        materialDescriptionSystem.release();
        queueCommandsBuilderSystem.release();
        resultReceiverSystem.release();
        sceneSettingsSystem.release();
        reallocateDatasSystem.release();
        grabObjectsDatasSystem.release();
        staticReaderSystem.release();
        staticLoaderSystem.release();

        memoryBarrierControllerSystem.release();
        shaderProvider.release();

        delete gpuData.computeCommands;
        gpuData.commandReceiver->returnQueue(gpuData.queue);
        simulationContext.release();
        PhBase::release();
    }

    void PeSceneSimulator::addScene(InScene *_scene)
    {
        _scene->data = generateSceneSimulationData();
    }

    void PeSceneSimulator::removeScene(InScene *_scene)
    {
        PH_RELEASE(_scene->data);
    }

    void PeSceneSimulator::simulate(InScene *_scene, float _deltaTime, const PhSimulationSettings &_settings)
    {
        if (sceneInSimulation)
        {
            fetchScene(sceneInSimulation, true);
        }
        sceneInSimulation = _scene;

        lockMainManagers();
        sceneInSimulation->data->actorManager.lock();

        PeSceneSimulationData *data = sceneInSimulation->data;

        data->outputDebugData.clearData();

        //
        resultReceiverSystem.receiveData();
        grabObjectsDatasSystem.unloadData();

        memoryBarrierControllerSystem.reset();
        resetSimulationMemoryData(data);

        // fill gpu commands
        data->finishFence.reset();

        gpuData.computeCommands->resetCommandBuffer();
        gpuData.computeCommands->debugMarkerBegin("Physics", PeDebugMarkersUtils::titleColor());
        printf("---Physics.Begin\n");

        sceneSettingsSystem.prepareData(&_scene->settings, _settings, data, _deltaTime);

        queueCommandsBuilderSystem.buildPrepareStage(_scene->data);
        gpuData.computeCommands->setSignalSemaphoreCmd(data->prepareDataStage.getSemaphore());

        gpuData.computeCommands->updateCommandBuffer();

        memoryBarrierControllerSystem.doNextStep();

        gpuData.computeCommands->addWaitSemaphoreCmd(data->prepareDataStage.getSemaphore(), data->prepareDataStage.getWaitStage());
        queueCommandsBuilderSystem.buildSimulationStage(_scene->data);
        gpuData.computeCommands->setSignalSemaphoreCmd(data->completeSimulationStage.getSemaphore());

        gpuData.computeCommands->updateCommandBuffer();

        memoryBarrierControllerSystem.doNextStep();

        gpuData.computeCommands->addWaitSemaphoreCmd(data->completeSimulationStage.getSemaphore(), data->completeSimulationStage.getWaitStage());
        queueCommandsBuilderSystem.buildReceiveDataStage(_scene->data);

        resultReceiverSystem.sendData();

        printf("---Physics.End\n");
        gpuData.computeCommands->debugMarkerEnd();
        gpuData.commandReceiver->submitCommands(gpuData.computeCommands, data->finishFence);
    }

    bool PeSceneSimulator::fetchScene(InScene *_scene, bool _wait)
    {
        PH_ASSERT(sceneInSimulation, "Anything scene didn`t launch.");

        vk::VulkanFence fence = _scene->data->finishFence;

        if (_wait)
        {
            fence.wait();
        }
        else
        {
            if (!fence.isSignaled())
                return false;
        }
        sceneInSimulation = NULL;

        fetchSceneData.fetchSceneData(_scene->data);
        _scene->data->actorManager.unlock();
        unlockMainManagers();

        return true;
    }

    PeSceneSimulationData *PeSceneSimulator::generateSceneSimulationData()
    {
        PeSceneSimulationData *data = PH_NEW(PeSceneSimulationData)();
        vk::VulkanFence::createFence(device, &data->finishFence);
        data->prepareDataStage = PeSemaphoreStage("PrepareDataStage", device, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
        data->completeSimulationStage = PeSemaphoreStage("CompleteSimulationStage", device, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
        return data;
    }

#define INIT_UNKNOW_MEMORY(x) \
    if (x)                    \
        memoryBarrierControllerSystem.initUnknowMemory(x);

    void PeSceneSimulator::resetSimulationMemoryData(PeSceneSimulationData *_sceneSimulationData)
    {
        INIT_UNKNOW_MEMORY(_sceneSimulationData->actorDynamicsBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->actorOrientationsBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->actorsBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->actorVelocitiesBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->actorSleepingBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->sceneSettingsBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->shapeListBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->geometryOBBs.geometryObbsBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->geometryOBBs.geometryOrientationObbsBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.allocatedContactsContainersCounter)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.allocatedContactsCounter)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.contactsBuffers[0])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.contactsBuffers[1])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.contactsContainersBuffers[0])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.contactsContainersBuffers[1])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.actorLostContainerBuffer)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.statisticUsingImage)
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.swapIndexesBuffers[0])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.swapIndexesBuffers[1])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.tableContactsOBBsBuffers[0])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.tableContactsOBBsBuffers[1])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.tableContainerContactsBuffers[0])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->tableContacts.tableContainerContactsBuffers[1])
        INIT_UNKNOW_MEMORY(_sceneSimulationData->simulationData.maxContainersOnActorCounter)
    }

    void PeSceneSimulator::lockMainManagers()
    {
        foundation->getMaterialManger()->lock();
        foundation->getShapeManager()->lock();
    }

    void PeSceneSimulator::unlockMainManagers()
    {
        foundation->getMaterialManger()->unlock();
        foundation->getShapeManager()->unlock();
    }
}