#include "geometry/internal/inShapeExclusive.h"

namespace phys
{
    uint InShapeExclusive::addShapeRegister(InShapeListenerBase *_register)
    {
        own = _register;
        return 0;
    }

    void InShapeExclusive::removeShapeRegister(uint _index)
    {
        own = NULL;
    }

    void InShapeExclusive::release()
    {
        InShape::release();
    }

    void InShapeExclusive::onChange()
    {
        if (own != NULL)
            own->onShapeChanged(this);
    }
}