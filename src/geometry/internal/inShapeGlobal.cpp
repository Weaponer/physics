#include "geometry/internal/inShapeGlobal.h"

namespace phys
{
    uint InShapeGlobal::addShapeRegister(InShapeListenerBase *_register)
    {
        unsigned int index = 0;
        registers.assignmentMemoryAndCopy(&_register, index);
        return index;
    }

    void InShapeGlobal::removeShapeRegister(uint _index)
    {
        registers.popMemory(_index);
    }

    void InShapeGlobal::release()
    {
        registers.release();
        InShape::release();
    }

    void InShapeGlobal::onChange()
    {
        uint count = registers.getPosBuffer();
        for (uint i = 0; i < count; i++)
        {
            InShapeListenerBase *listener = *registers.getMemory(i);
            if (listener != NULL)
                listener->onShapeChanged(this);
        }
    }
};