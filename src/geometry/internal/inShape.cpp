#include "geometry/internal/inShape.h"
#include "physics/internal/inShapeManager.h"
#include "geometry/internal/inGeometryTemplate.h"

namespace phys
{
    InShape::InShape(InShapeManager *_manager, PhFlags _typeInShape)
        : PhShape(), managerRegistration(), material(NULL), geometryBuffer(),
          typeInShape(_typeInShape)
    {
        managerRegistration.connect(_manager, this);
    }

    void InShape::connectGeometry(PhGeometry *_geometry)
    {
        InGeometryBase *base = dynamic_cast<InGeometryBase *>(_geometry);

        geometryBuffer.pushElement(&base);
        base->setOwnShape(this);
        base->incReference(base);

        onChange();
        managerRegistration.onChange(InShapeFlagBits::ShapeEvent_AddedGeometry);
    }

    void InShape::disconnectGeometry(PhGeometry *_geometry)
    {
        InGeometryBase *base = dynamic_cast<InGeometryBase *>(_geometry);

        int count = geometryBuffer.countElements();
        for (int i = 0; i < count; i++)
            if (geometryBuffer.getElement(i) == base)
            {
                geometryBuffer.popElement(i);
                base->setOwnShape(NULL);
                base->decReference(base);

                onChange();
                managerRegistration.onChange(InShapeFlagBits::ShapeEvent_RemovedGeometry);
                return;
            }
    }

    void InShape::geometryWasChanged(InGeometryBase *_geometry)
    {
        onChange();
        managerRegistration.onChange(InShapeFlagBits::ShapeEvent_ChangedGeometry);
    }

    int InShape::countGeometries()
    {
        return geometryBuffer.countElements();
    }

    PhGeometry *InShape::getGeometry(int _index)
    {
        return dynamic_cast<PhGeometry *>(geometryBuffer.getElement(_index));
    }

    void InShape::setMaterial(PhMaterial *_material)
    {
        if (_material == material)
            return;

        if (material != NULL)
        {
            material->decReference(material);
            material = NULL;
        }

        if (_material != NULL)
        {
            _material->incReference(_material);
        }

        managerRegistration.onChange(InShapeFlagBits::ShapeEvent_ChangedMaterial);
        material = _material;
    }

    PhMaterial *InShape::getMaterial()
    {
        return material;
    }

    void InShape::release()
    {
        if (material != NULL)
        {
            material->decReference(material);
            PH_REF_RELEASE(material);
        }
        while (true)
        {
            int count = countGeometries();
            if (count == 0)
                break;
            int index = count - 1;
            InGeometryBase *base = geometryBuffer.getElement(index);
            geometryBuffer.popElement(index);
            base->setOwnShape(NULL);

            base->decReference(base);
            PH_REF_RELEASE(base);
        }

        managerRegistration.disconnect();
        geometryBuffer.release();
        PhShape::release();
    }
}