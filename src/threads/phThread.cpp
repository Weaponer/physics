#include "threads/phThread.h"

namespace phys
{
    PhThread::PhThread(const char *_name, int _userData)
        : name(_name), userData(_userData), thread()
    {
        runFlag.store(false, std::memory_order_release);
        stopLoopFlag.store(false, std::memory_order_relaxed);
    }

    PhThread::~PhThread()
    {
        quit();
        wait();
    }

    void PhThread::init()
    {
        onInit();
    }

    bool PhThread::run()
    {
        if (runFlag.load(std::memory_order_acquire))
        {
            return false;
        }

        stopLoopFlag.store(false, std::memory_order_relaxed);
        runFlag.store(true, std::memory_order_release);
        thread = std::thread(&PhThread::singleRun, this);
        return true;
    }

    bool PhThread::runLoop()
    {
        if (runFlag.load(std::memory_order_acquire))
        {
            return false;
        }

        stopLoopFlag.store(false, std::memory_order_relaxed);
        runFlag.store(true, std::memory_order_release);
        thread = std::thread(&PhThread::loop, this);
        return true;
    }

    bool PhThread::wait()
    {
        if (thread.joinable())
            thread.join();

        runFlag.store(false, std::memory_order_release);
        return true;
    }

    bool PhThread::quit()
    {
        if (!runFlag.load(std::memory_order_acquire))
        {
            return false;
        }

        stopLoopFlag.store(true, std::memory_order_relaxed);
        return true;
    }

    void PhThread::singleRun()
    {
        onRun();
        runFlag.store(false, std::memory_order_release);
    }

    void PhThread::loop()
    {
        while (!stopLoopFlag.load(std::memory_order_relaxed))
        {
            onRun();
        }
        runFlag.store(false, std::memory_order_release);
    }
}