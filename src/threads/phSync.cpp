#include "threads/phSync.h"

namespace phys
{
    PhSync::PhSync() : mutex(), cv(), countListeners(0), activeSignal(false)
    {
    }

    PhSync::~PhSync()
    {
        signal();
    }

    bool PhSync::wait()
    {
        std::unique_lock<std::mutex> locker(mutex);
        if (!activeSignal.load(std::memory_order_acquire))
            return false;

        countListeners.fetch_add(1, std::memory_order_seq_cst);
        cv.wait(locker);
        countListeners.fetch_sub(1, std::memory_order_release);
        return true;
    }

    void PhSync::enableSignal()
    {
        std::unique_lock<std::mutex> locker(mutex);
        activeSignal.store(true);
    }

    void PhSync::signal()
    {
        std::unique_lock<std::mutex> locker(mutex);
        activeSignal.store(false, std::memory_order_release);
        int count = countListeners.load(std::memory_order_acquire);
        if (!count)
            return;

        if (count > 1)
            cv.notify_all();
        else if (count == 1)
            cv.notify_one();
    }
}