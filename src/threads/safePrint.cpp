#include "threads/safePrint.h"
#include <stdlib.h>
#include <stdio.h>
#include <cstdarg>

namespace phys
{
    std::mutex SafePrint::printLock = std::mutex();

    void SafePrint::print(const char *c_str, ...)
    {
        printLock.lock();
        va_list arg;
        va_start(arg, c_str);
        vprintf(c_str, arg);
        va_end(arg);
        printLock.unlock();
    }
}