#include "threads/phTaskQueue.h"

namespace phys
{
    PhTaskQueue::PhTaskQueue() : mData(), cv(), isStopQueue(false), countListeners(0), tasks()
    {
    }

    PhTaskQueue::~PhTaskQueue()
    {
        if (!isStopQueue.load(std::memory_order_acquire))
            stopQueue();
    }

    void PhTaskQueue::pushTask(PhTask *_task, int _countInstance, int _offsetInstance)
    {
        {
            std::unique_lock<std::mutex> locker(mData);
            TaskData data;
            data.task = _task;
            data.countInstances = _countInstance;
            data.offsetInstance = _offsetInstance;
            tasks.push(data);
        }
        cv.notify_one();
    }

    void PhTaskQueue::stopQueue()
    {
        {
            std::unique_lock<std::mutex> locker(mData);
            isStopQueue.store(true, std::memory_order_release);
        }
        cv.notify_one();
    }

    bool PhTaskQueue::waitToTakeTask(TaskData *_outData)
    {
        if (isStopQueue.load(std::memory_order_acquire))
            return false;

        std::unique_lock<std::mutex> locker(mData);

        countListeners.fetch_add(1, std::memory_order_seq_cst);
        while (tasks.empty() && !isStopQueue.load(std::memory_order_acquire))
            cv.wait(locker);

        countListeners.fetch_sub(1, std::memory_order_release);

        if (isStopQueue.load(std::memory_order_acquire))
            return false;

        *_outData = tasks.front();
        tasks.pop();
        return true;
    }

    bool PhTaskQueue::isEmpty()
    {
        std::unique_lock<std::mutex> locker(mData);
        return tasks.empty();
    }

    bool PhTaskQueue::isSomeoneWaitTask()
    {
        std::unique_lock<std::mutex> locker(mData);
        return countListeners.load(std::memory_order_acquire) != 0 && tasks.empty();
    }
}