#include <vector>
#include <algorithm>
#include <iostream>

#include "threads/phThreadsManager.h"
#include "threads/safePrint.h"

namespace phys
{
    PhThreadsManager::TaskThread::TaskThread()
        : PhThread("", 0), taskQueue(), eventFinish()
    {
    }

    PhThreadsManager::TaskThread::~TaskThread()
    {
    }

    void PhThreadsManager::TaskThread::onInit()
    {
    }

    void PhThreadsManager::TaskThread::onRun()
    {
        PhTaskQueue::TaskData data;

        if (!taskQueue.waitToTakeTask(&data))
            return;

        eventFinish.enableSignal();

        for (int i = 0; i < data.countInstances; i++)
        {
            data.task->complete(data.offsetInstance + i);
        }

        data.task->finishedLaunchers.fetch_add(1, std::memory_order_release);
        eventFinish.signal();
    }

    //

    PhThreadsManager::PhThreadsManager() : taskThreads()
    {
    }

    PhThreadsManager::~PhThreadsManager()
    {
        clear();
    }

    void PhThreadsManager::allocateTaskThreads(int _count)
    {
        if (isTaskSystemEnable())
            waitAllTasks();

        int currentCount = (int)taskThreads.size();
        if (currentCount < _count)
        {
            for (int i = currentCount; i < _count; i++)
            {
                TaskThread *taskThread = new TaskThread();
                taskThreads.push_back(taskThread);
                taskThread->runLoop();
            }
        }
    }

    void PhThreadsManager::clear()
    {
        if (isTaskSystemEnable())
        {
            waitAllTasks();

            int count = (int)taskThreads.size();
            for (int i = 0; i < count; i++)
            {
                taskThreads[i]->taskQueue.stopQueue();
                taskThreads[i]->quit();
                taskThreads[i]->wait();
                delete taskThreads[i];
            }
            taskThreads.clear();
        }
    }

    void PhThreadsManager::addTask(PhTask *_task, int _countInstance)
    {
        if(!isTaskSystemEnable())
        {
            for(int i = 0; i < _countInstance; i++)
                _task->complete(i);
            return;
        }

        if (_task->startedLaunchers.load(std::memory_order_acquire) == 0 ||
            _task->startedLaunchers.load(std::memory_order_acquire) == _task->finishedLaunchers.load(std::memory_order_acquire))
        {
            _task->startedLaunchers.store(0, std::memory_order_release);
            _task->finishedLaunchers.store(0, std::memory_order_release);

            int countThreads = (int)taskThreads.size();
            if (_countInstance <= countThreads)
            {
                for (int i = 0; i < _countInstance; i++)
                {
                    _task->startedLaunchers.fetch_add(1, std::memory_order_release);
                    taskThreads[i]->taskQueue.pushTask(_task, 1, i);
                }
            }
            else
            {
                int instancePerTask = _countInstance / countThreads;
                int additional = _countInstance - (countThreads * instancePerTask);
                int offset = 0;
                for (int i = 0; i < countThreads; i++)
                {
                    int launch = instancePerTask + additional;

                    _task->startedLaunchers.fetch_add(1, std::memory_order_release);
                    taskThreads[i]->taskQueue.pushTask(_task, launch, offset);

                    offset += launch;
                    additional = 0;
                }
            }
        }
    }

    void PhThreadsManager::waitTask(PhTask *_task)
    {
        while (_task->startedLaunchers.load(std::memory_order_acquire) != _task->finishedLaunchers.load(std::memory_order_acquire))
            waitAnySignal();
    }

    void PhThreadsManager::waitAllTasks()
    {
        int countThreads = (int)taskThreads.size();
        for (int i = 0; i < countThreads; i++)
        {
            while (!taskThreads[i]->taskQueue.isSomeoneWaitTask())
            {
                taskThreads[i]->eventFinish.wait();
            }
        }
    }

    void PhThreadsManager::waitAnySignal()
    {
        int countThreads = (int)taskThreads.size();
        for (int i = 0; i < countThreads; i++)
        {
            if (taskThreads[i]->eventFinish.wait())
                return;
        }
    }
}