#include "extension/phWindowImGUI.h"
#include "window/window.h"

namespace phys
{
    PhWindowImGUI::PhWindowImGUI() : windowImGUI(), descriptorPool(VK_NULL_HANDLE),
                                     pipelineCache(VK_NULL_HANDLE), renderPass(VK_NULL_HANDLE), framebuffers(),
                                     commandPool(VK_NULL_HANDLE), cmd(VK_NULL_HANDLE), showDemoWindow(true), otherTestWindow(true)
    {
    }

    PhWindowImGUI::~PhWindowImGUI()
    {
    }

    void PhWindowImGUI::updateEvent(const SDL_Event *_event)
    {
        ImGui_ImplSDL2_ProcessEvent(_event);
    }

    void PhWindowImGUI::beginImGui()
    {
        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplSDL2_NewFrame();
        ImGui::NewFrame();
        //ImGui::ShowDemoWindow(&showDemoWindow);
    }

    void PhWindowImGUI::endImGui()
    {
        ImGui::EndFrame();
        ImGui::Render();
    }

    void PhWindowImGUI::drawFrame(vk::VulkanSemaphore _waitSemaphore, vk::VulkanSemaphore _signalSemaphore, vk::VulkanFence _fence)
    {
        ImDrawData *draw_data = ImGui::GetDrawData();
        device->vkResetCommandBuffer(cmd, 0);
        {
            VkCommandBufferBeginInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
            device->vkBeginCommandBuffer(cmd, &info);
        }
        {
            VkRenderPassBeginInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            info.renderPass = renderPass;
            info.framebuffer = framebuffers[window->getCurrentImageIndex()];
            info.renderArea.extent.width = window->getWidth();
            info.renderArea.extent.height = window->getHeight();
            info.clearValueCount = 0;
            info.pClearValues = NULL;
            device->vkCmdBeginRenderPass(cmd, &info, VK_SUBPASS_CONTENTS_INLINE);
        }

        if (draw_data != NULL)
        {
            ImGui_ImplVulkan_RenderDrawData(draw_data, cmd);
        }
        device->vkCmdEndRenderPass(cmd);
        {
            VkPipelineStageFlags waitStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            VkSemaphore waitSemaphore = _waitSemaphore;
            VkSemaphore signalSemaphore = _signalSemaphore;

            VkSubmitInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            info.waitSemaphoreCount = 1;
            info.pWaitSemaphores = &waitSemaphore;
            info.pWaitDstStageMask = &waitStage;
            info.commandBufferCount = 1;
            info.pCommandBuffers = &cmd;
            info.signalSemaphoreCount = 1;
            info.pSignalSemaphores = &signalSemaphore;

            device->vkEndCommandBuffer(cmd);
            device->vkQueueSubmit(*queue, 1, &info, _fence);
        }
    }

    void PhWindowImGUI::onInit(Window *_window, vk::VulkanDevice *_device, vk::VulkanQueue *_queue)
    {
        VkPipelineCacheCreateInfo info{};
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
        device->vkCreatePipelineCache(*device, &info, NULL, &pipelineCache);

        VkDescriptorPoolSize pool_sizes[] =
            {
                {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1},
            };
        VkDescriptorPoolCreateInfo poolInfo = {};
        poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        poolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
        poolInfo.maxSets = 1;
        poolInfo.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
        poolInfo.pPoolSizes = pool_sizes;
        device->vkCreateDescriptorPool(*device, &poolInfo, NULL, &descriptorPool);

        createRenderPass();

        VkCommandPoolCreateInfo commandPoolInfo{};
        commandPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolInfo.queueFamilyIndex = queue->getFamilyIndex();
        commandPoolInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        device->vkCreateCommandPool(*device, &commandPoolInfo, NULL, &commandPool);

        VkCommandBufferAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.commandPool = commandPool;
        allocInfo.commandBufferCount = 1;
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        device->vkAllocateCommandBuffers(*device, &allocInfo, &cmd);

        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO &io = ImGui::GetIO();
        (void)io;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;
        ImGui::StyleColorsDark();
        ImGui_ImplSDL2_InitForVulkan(*_window);
        ImGui_ImplVulkan_InitInfo initInfo = {};
        initInfo.Instance = *_window->getVulkanInstance();
        initInfo.PhysicalDevice = _device->getPhysicalDevice();
        initInfo.Device = *_device;
        initInfo.QueueFamily = _queue->getFamilyIndex();
        initInfo.Queue = *_queue;
        initInfo.PipelineCache = pipelineCache;
        initInfo.DescriptorPool = descriptorPool;
        initInfo.Subpass = 0;
        initInfo.MinImageCount = _window->getSwapchain()->getCountImage();
        initInfo.ImageCount = initInfo.MinImageCount;
        initInfo.MSAASamples = VK_SAMPLE_COUNT_1_BIT;
        initInfo.Allocator = NULL;
        initInfo.CheckVkResultFn = NULL;
        ImGui_ImplVulkan_Init(&initInfo, renderPass);
    }

    void PhWindowImGUI::onDestory()
    {
        ImGui_ImplVulkan_Shutdown();
        ImGui_ImplSDL2_Shutdown();
        ImGui::DestroyContext();

        for (auto i : framebuffers)
        {
            device->vkDestroyFramebuffer(*device, i, NULL);
        }
        framebuffers.clear();
        device->vkDestroyCommandPool(*device, commandPool, NULL);
        commandPool = VK_NULL_HANDLE;
        device->vkDestroyRenderPass(*device, renderPass, NULL);
        renderPass = VK_NULL_HANDLE;
        device->vkDestroyDescriptorPool(*device, descriptorPool, NULL);
        descriptorPool = VK_NULL_HANDLE;
        device->vkDestroyPipelineCache(*device, pipelineCache, NULL);
        pipelineCache = VK_NULL_HANDLE;
    }

    void PhWindowImGUI::createRenderPass()
    {
        VkAttachmentDescription attachment = {};
        attachment.format = window->getSwapchain()->getImage(0)->getFormat();
        attachment.samples = VK_SAMPLE_COUNT_1_BIT;
        attachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        VkAttachmentReference color_attachment = {};
        color_attachment.attachment = 0;
        color_attachment.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        VkSubpassDescription subpass = {};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &color_attachment;
        VkSubpassDependency dependency = {};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        VkRenderPassCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        info.attachmentCount = 1;
        info.pAttachments = &attachment;
        info.subpassCount = 1;
        info.pSubpasses = &subpass;
        info.dependencyCount = 1;
        info.pDependencies = &dependency;

        device->vkCreateRenderPass(*device, &info, NULL, &renderPass);

        VkFramebufferCreateInfo framebufferInfo{};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.layers = 1;
        framebufferInfo.width = window->getWidth();
        framebufferInfo.height = window->getHeight();
        framebufferInfo.renderPass = renderPass;
        framebufferInfo.attachmentCount = 1;
        vk::VulkanSwapchain *swapchain = window->getSwapchain();
        for (int i = 0; i < swapchain->getCountImage(); i++)
        {
            VkImageView view = swapchain->getImageView(i);
            framebufferInfo.pAttachments = &view;
            VkFramebuffer framebuffer;
            device->vkCreateFramebuffer(*device, &framebufferInfo, NULL, &framebuffer);
            framebuffers.push_back(framebuffer);
        }
    }
}