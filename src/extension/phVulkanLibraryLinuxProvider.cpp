#include <dlfcn.h>

#include "extension/phVulkanLibraryLinuxProvider.h"

namespace phys
{
    void *PhVulkanLibraryLinuxProvider::loadLibrary()
    {
        return dlopen("libvulkan.so.1", RTLD_NOW);
    }

    void PhVulkanLibraryLinuxProvider::closeLibrary(void *_library)
    {
        dlclose(_library);
    }

    void *PhVulkanLibraryLinuxProvider::loadFunctionOfLibrary(void *_library, const char *_name)
    {
        return dlsym(_library, _name); 
    }
}