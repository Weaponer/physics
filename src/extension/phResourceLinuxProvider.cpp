#include <fstream>
#include <vector>

#include "extension/phResourceLinuxProvider.h"

namespace phys
{
    const char *PhResourceLinuxProvider::getPathToShaders()
    {
        return "./shaders/cshaders/";
    }

    const char *PhResourceLinuxProvider::getPathToObjectMeshes()
    {
        return "./models/";
    }

    bool PhResourceLinuxProvider::readFile(const char *_path, char **_data, int *_size)
    {
        std::fstream f;
        f.open(_path, std::fstream::in);

        std::vector<char> fileData;
        fileData = std::vector<char>(std::istreambuf_iterator<char>(f), {});
        f.close();

        int size = (int)fileData.size();
        *_size = size;
        *_data = (char *)phAllocateMemory(sizeof(char) * size);
        std::memcpy(*_data, fileData.data(), sizeof(char) * size);
        return true;
    }
}