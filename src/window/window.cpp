#include <SDL2/SDL_vulkan.h>
#include <iostream>
#include <algorithm>

#include "window/window.h"

namespace phys
{
    Window::Window(const char *_title, int _width, int _height)
        : PhBase(), title(_title), width(_width), height(_height), window(NULL), instance(NULL),
          device(NULL), queue(NULL), surface(VK_NULL_HANDLE), swapchain(),
          waitBeginRenderingSamephore(NULL, VK_NULL_HANDLE), signalRenderingSamephore(NULL, VK_NULL_HANDLE), currentImageIndex(0),
          GUIs(), mousePosDelta()
    {
        window = SDL_CreateWindow(_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _width, _height, SDL_WINDOW_VULKAN | SDL_WINDOW_SHOWN);
    }

    void Window::release()
    {
        if (!GUIs.empty())
        {
            disconnectGUI(*GUIs.begin());
        }
        if (swapchain.isValid())
        {
            swapchain.destroySwapchain();
            vk::VulkanSemaphore::destroySemaphore(&waitBeginRenderingSamephore);
            vk::VulkanSemaphore::destroySemaphore(&signalRenderingSamephore);
        }
        if (surface != VK_NULL_HANDLE)
        {
            instance->vkDestroySurfaceKHR(*instance, surface, NULL);
        }
        SDL_DestroyWindow(window);
    }

    void Window::connectVulkanInstance(vk::VulkanInstance *_instance)
    {
        if (instance != NULL)
            return;

        instance = _instance;
        SDL_Vulkan_CreateSurface(window, *instance, &surface);
    }

    void Window::connectDevice(vk::VulkanDevice *_device, vk::VulkanQueue *_queue)
    {
        if (device != NULL)
            return;

        device = _device;
        queue = _queue;
        swapchain.createSwapchain(device, surface, width, height);
        vk::VulkanSemaphore::createSemaphore(device, &waitBeginRenderingSamephore);
        vk::VulkanSemaphore::createSemaphore(device, &signalRenderingSamephore);
    }

    void Window::connectGUI(WindowGUI *_gui)
    {
        auto found = std::find(GUIs.begin(), GUIs.end(), _gui);
        if (found == GUIs.end())
        {
            _gui->init(this, device, queue);
            GUIs.push_back(_gui);
        }
    }

    void Window::disconnectGUI(WindowGUI *_gui)
    {
        auto found = std::find(GUIs.begin(), GUIs.end(), _gui);
        if (found != GUIs.end())
        {
            _gui->destroy();
            GUIs.erase(found);
        }
    }

    void Window::getExtensions(uint *_count, const char **_names)
    {
        SDL_Vulkan_GetInstanceExtensions(window, _count, _names);
    }

    bool Window::updateEvents()
    {
        SDL_Event event;
        bool running = true;

        mousePosDelta.clear();

        while (SDL_PollEvent(&event))
        {
            for (auto i : GUIs)
                (*i).updateEvent(&event);

            if (event.type == SDL_MOUSEMOTION)
                mousePosDelta = mth::vec2(event.motion.xrel, event.motion.yrel);

            if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.scancode == SDL_SCANCODE_ESCAPE))
                running = false;
        }
        return running;
    }

    bool Window::beginRendering(uint *_imageIndex)
    {
        currentImageIndex = 0;
        VkResult result = device->vkAcquireNextImageKHR(*device, swapchain, UINT64_MAX, waitBeginRenderingSamephore, VK_NULL_HANDLE, &currentImageIndex);

        if (result != VK_SUCCESS)
            return false;

        *_imageIndex = currentImageIndex;
        return true;
    }

    bool Window::endRendering()
    {
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        VkSemaphore semaphoreWait = signalRenderingSamephore;
        presentInfo.pWaitSemaphores = &semaphoreWait;
        presentInfo.swapchainCount = 1;
        VkSwapchainKHR swapchainKHR = swapchain;
        presentInfo.pSwapchains = &swapchainKHR;
        presentInfo.pImageIndices = &currentImageIndex;
        presentInfo.pResults = NULL;

        return device->vkQueuePresentKHR(*queue, &presentInfo) == VK_SUCCESS;
    }

    void Window::showCursor()
    {
        SDL_SetRelativeMouseMode(SDL_FALSE);
    }

    void Window::hideCursor()
    {
        SDL_SetRelativeMouseMode(SDL_TRUE);
    }

    mth::vec2 Window::getDeltaMoveMouse()
    {
        return mousePosDelta;
    }
}