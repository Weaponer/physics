#include "window/windowGUI.h"

namespace phys
{
    WindowGUI::WindowGUI() : window(NULL), device(NULL), queue(NULL)
    {
    }

    WindowGUI::~WindowGUI()
    {
    }

    void WindowGUI::init(Window *_window, vk::VulkanDevice *_device, vk::VulkanQueue *_queue)
    {
        if (window != NULL)
            return;

        window = _window;
        device = _device;
        queue = _queue;

        onInit(window, device, queue);
    }

    void WindowGUI::destroy()
    {
        if (window == NULL)
            return;

        onDestory();

        window = NULL;
        device = NULL;
        queue = NULL;
    }
}