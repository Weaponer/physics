#include "rendering/internal/inSkyBoxRenderer.h"

namespace phys::ge
{
    mth::boundSphere InSkyBoxRenderer::getGlobalSphereBound()
    {
        return mth::boundSphere(mth::vec3(), MAXFLOAT);
    }

    mth::boundOBB InSkyBoxRenderer::getGlobalOBB()
    {
        return mth::boundOBB(mth::vec3(), mth::vec3(MAXFLOAT, MAXFLOAT, MAXFLOAT));
    }
}