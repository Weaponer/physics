#include "rendering/internal/inPivotRenderer.h"

namespace phys::ge
{
    float InPivotRenderer::getSizePivot() const
    {
        return 0.35f;
    }

    GePivotRenderer::MoveScaleAxisData InPivotRenderer::getMoveScaleAxisData() const
    {
        MoveScaleAxisData data;
        data.sizePlaneRect = 0.25f;
        data.offsetPlaneRect = 0.5f - data.sizePlaneRect / 2.0f;
        data.sizeCenterCube = 0.05f;
        data.radiusConus = 0.035f;
        data.heightConus = 0.17f;
        data.sizeEdgeCube = data.radiusConus;
        return data;
    }

    GePivotRenderer::RotateAxisData InPivotRenderer::getRotateAxisData() const
    {
        RotateAxisData data;
        data.radiusMainAxis = 0.8f;
        data.radiusFrontAxis = 0.82f;
        data.radiusOutsideCircle = 1.0f;
        data.cylindersHeight = 0.02f;
        return data;
    }

    float InPivotRenderer::getSizePivot(GeCamera *_camera) const
    {
        mth::vec3 localPos = _camera->getInverseTRS().transform(position);
        float distance = localPos.z;
        return distance * getSizePivot() * _camera->getFovTanget();
    }

    float InPivotRenderer::getSizePivotNoDistance(GeCamera *_camera) const
    {
        return getSizePivot() * _camera->getFovTanget();
    }
}