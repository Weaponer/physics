#include "rendering/internal/inMeshRenderer.h"
#include "rendering/internal/graphicPipeline/inMeshInstanceRegister.h"

namespace phys::ge
{
    void InMeshRenderer::release()
    {
        setMesh(NULL);

        GeMeshRenderer::release();
        InSelectableRenderer::release();
        GeSelectableRenderer::release();
    }

    void InMeshRenderer::setMesh(GeMesh *_mesh)
    {
        if (mesh != NULL)
        {
            meshRendererRegister->unregisterRenderer(mesh, indexMeshRendererInstance);
            mesh = NULL;
        }
        mesh = dynamic_cast<InMesh *>(_mesh);
        if (mesh != NULL)
        {
            indexMeshRendererInstance = meshRendererRegister->registerRenderer(this, mesh);
        }
    }

    mth::boundSphere InMeshRenderer::getGlobalSphereBound()
    {
        if (mesh == NULL)
        {
            return mth::boundSphere(position, 0.0f);
        }
        checkAndRecalculateTRS();
        return globalSphereBound;
    }

    mth::boundOBB InMeshRenderer::getGlobalOBB()
    {
        if (mesh == NULL)
        {
            return mth::boundOBB(position,
                                 mth::mat3(1.0f, 0.0f, 0.0f,
                                           0.0f, 1.0f, 0.0f,
                                           0.0f, 0.0f, 1.0f),
                                 mth::vec3(0.0f, 0.0f, 0.0f));
        }
        checkAndRecalculateTRS();
        return globalOBB;
    }

    void InMeshRenderer::onRecalculateTRS()
    {
        GeMeshRenderer::onRecalculateTRS();

        mth::boundAABB boundMesh = mesh->bound;
        mth::vec3 min = boundMesh.min.componentProduct(size);
        mth::vec3 max = boundMesh.max.componentProduct(size);
        mth::vec3 boundSize = (max - min) / 2.0f;
        boundSize.x = abs(boundSize.x);
        boundSize.y = abs(boundSize.y);
        boundSize.z = abs(boundSize.z);

        float radius = (boundSize).magnitude();
        mth::vec3 position = TRS.transform(boundMesh.origin);
        globalSphereBound = mth::boundSphere(position, radius);

        globalOBB = mth::boundOBB(position, mth::matrix3FromMatrix4(rotation.getMatrix()), boundSize);
    }
}