#include "rendering/internal/inFloorRenderer.h"

namespace phys::ge
{
    void InFloorRenderer::release()
    {
        GeFloorRenderer::release();
        InSelectableRenderer::release();
        GeSelectableRenderer::release();
    }

    mth::boundSphere InFloorRenderer::getGlobalSphereBound()
    {
        return mth::boundSphere(position, MAXFLOAT);
    }

    mth::boundOBB InFloorRenderer::getGlobalOBB()
    {
        mth::boundOBB bound;
        bound.center = position;
        bound.rotate = mth::matrix3FromMatrix4(rotation.getMatrix());
        bound.size = mth::vec3(MAXFLOAT, 0.0f, MAXFLOAT);
        return bound;
    }
}