#include "rendering/internal/inSceneRenderer.h"
#include "inScene.h"

namespace phys::ge
{
    InSceneRenderer::InSceneRenderer() : GeSceneRenderer()
    {
    }

    void InSceneRenderer::release()
    {

        GeSceneRenderer::release();
    }

    void InSceneRenderer::renderScene(GeScene *_scene, uint32_t _imageIndex, vk::VulkanSemaphore _waitBegin, vk::VulkanSemaphore _signalEnd, vk::VulkanFence _fence)
    {
        InScene *scene = dynamic_cast<InScene *>(_scene);
        graphicPipeline->render(scene, _imageIndex, _waitBegin, _signalEnd, _fence);
    }
}