#include "rendering/internal/templates/meshRenderTemplate.h"

namespace phys::ge
{
    MeshRenderTemplate::MeshRenderTemplate()
    {
    }

    void MeshRenderTemplate::release()
    {
    }

    void MeshRenderTemplate::init(const InPass::InitContextData *_context, const InitRenderTemplate *_contextTemplate, vk::VulkanQueue *_graphicQueue)
    {
        const MeshTemplateInitData *additionalData = (MeshTemplateInitData *)_contextTemplate->customData;

        vertexModul = _context->shaderProvider->getShaderModule(_contextTemplate->vertexShader);
        fragmentModul = _context->shaderProvider->getShaderModule(_contextTemplate->fragmentShader);

        vk::ShaderModule modules[2] = {vertexModul, fragmentModul};
        vk::VulkanPipelineLayout::createPipelineLayout(_context->device, 2, modules, &layout);

        graphicPipeline = _context->graphicPipelinesManager->createGraphicPipeline();

        graphicPipeline->setVertexModule(vertexModul);
        graphicPipeline->setFragmentModule(fragmentModul);
        graphicPipeline->setPipelineLayout(layout);
        graphicPipeline->setBlendColorState(_contextTemplate->blendColorState);
        graphicPipeline->setVertexAttributeState(_contextTemplate->vertexAttributeState);
        graphicPipeline->setDepthCompareOp(VK_COMPARE_OP_LESS);
        if (additionalData->isWireframe)
        {
            graphicPipeline->setTopology(VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
            graphicPipeline->setLineWidth(1.5f);
        }
        graphicPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_VIEWPORT, true);
        graphicPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_SCISSOR, true);

        _context->graphicPipelinesManager->updateGraphicPipeline(graphicPipeline, _contextTemplate->renderPass);

        graphicPipeline->setBlendColorState(NULL);
        graphicPipeline->setVertexAttributeState(NULL);

        vertexParameters = new vk::ShaderAutoParameters(_context->device, vertexModul);
        vertexParameters->takeMemoryPools(_context->descriptorPoolManager, 1);

        vertexParameters->beginUpdate();
        vertexParameters->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, _context->cameraData->getCameraData());
        vertexParameters->endUpdate();
    }

    void MeshRenderTemplate::beginRender(InScene *_scene, const InPass::RenderContextData *_context,
                                         const BeginRenderTemplate *_beginRender, vk::VulkanGraphicCommands *graphicCommands)
    {
        vk::VulkanRenderPass renderPass = _beginRender->renderPass;
        vk::VulkanFramebuffer framebuffer = _beginRender->framebuffer;
        graphicCommands->beginRenderPass(renderPass, framebuffer);
        graphicCommands->setDefaultViewport(framebuffer);

        VkPipeline pipeline = graphicPipeline->getPipeline(&renderPass);
        graphicCommands->bindPipeline(pipeline);
        graphicCommands->bindDescriptorSet(layout, vertexParameters->getCurrentSet(0), 0);
    }

    void MeshRenderTemplate::endRender(vk::VulkanGraphicCommands *graphicCommands)
    {
        graphicCommands->endRenderPass();
    }

    void MeshRenderTemplate::cleanup(const InPass::CleanupContextData *_context)
    {
        vertexParameters->freeMemoryPools(_context->descriptorPoolManager);
        delete vertexParameters;
        _context->graphicPipelinesManager->deleteGraphicPipeline(graphicPipeline);
        vk::VulkanPipelineLayout::destroyPipelineLayout(_context->device, &layout);
    }

    void MeshRenderTemplate::draw(DrawRenderTemplate *_settings, vk::VulkanGraphicCommands *graphicCommands)
    {
        graphicCommands->bindVertexBuffer(_settings->vertexBuffer, 0);
        graphicCommands->bindInstanceBuffer(_settings->instanceBuffer, _settings->offsetInstanceBuffer);

        graphicCommands->draw(_settings->countVertex, _settings->countInstance, 0, 0);
    }

    void MeshRenderTemplate::drawIndexed(DrawRenderTemplate *_settings, vk::VulkanGraphicCommands *graphicCommands)
    {
        graphicCommands->bindIndexBuffer(_settings->indexBuffer, 0, _settings->indexType);
        graphicCommands->bindVertexBuffer(_settings->vertexBuffer, 0);
        graphicCommands->bindInstanceBuffer(_settings->instanceBuffer, _settings->offsetInstanceBuffer);

        graphicCommands->drawIndexed(_settings->countIndex, _settings->countInstance, 0, 0, 0);
    }
}