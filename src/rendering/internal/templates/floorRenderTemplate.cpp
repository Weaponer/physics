#include "rendering/internal/templates/floorRenderTemplate.h"

namespace phys::ge
{
    FloorRenderTemplate::FloorRenderTemplate()
    {
    }

    void FloorRenderTemplate::release()
    {
    }

    void FloorRenderTemplate::init(const InPass::InitContextData *_context, const InitRenderTemplate *_contextTemplate, vk::VulkanQueue *_graphicQueue)
    {
        floorDataUBO = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_UniformBlockDynamic,
                                                               sizeof(data), 1, &_context->mainFamilyIndex, "FloorDataUBO");

        vertexModul = _context->shaderProvider->getShaderModule(_contextTemplate->vertexShader);
        fragmentModul = _context->shaderProvider->getShaderModule(_contextTemplate->fragmentShader);

        vk::ShaderModule modules[2] = {vertexModul, fragmentModul};
        vk::VulkanPipelineLayout::createPipelineLayout(_context->device, 2, modules, &layout);

        graphicPipeline = _context->graphicPipelinesManager->createGraphicPipeline();

        graphicPipeline->setVertexModule(vertexModul);
        graphicPipeline->setFragmentModule(fragmentModul);
        graphicPipeline->setPipelineLayout(layout);
        graphicPipeline->setBlendColorState(_contextTemplate->blendColorState);
        graphicPipeline->setVertexAttributeState(_context->fullScreenPassData->getVertexAttributeState());
        graphicPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_VIEWPORT, true);
        graphicPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_SCISSOR, true);
        graphicPipeline->setDepthWrite(true);
        graphicPipeline->setDepthTest(true);

        _context->graphicPipelinesManager->updateGraphicPipeline(graphicPipeline, _contextTemplate->renderPass);

        graphicPipeline->setBlendColorState(NULL);
        graphicPipeline->setVertexAttributeState(NULL);

        vertexParameters = new vk::ShaderAutoParameters(_context->device, vertexModul);
        vertexParameters->takeMemoryPools(_context->descriptorPoolManager, 1);
        fragmentParameters = new vk::ShaderAutoParameters(_context->device, fragmentModul);
        fragmentParameters->takeMemoryPools(_context->descriptorPoolManager, 1);

        vertexParameters->beginUpdate();
        vertexParameters->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, _context->cameraData->getCameraData());
        vertexParameters->endUpdate();

        fragmentParameters->beginUpdate();
        fragmentParameters->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, _context->cameraData->getCameraData());
        fragmentParameters->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 1, floorDataUBO);
        fragmentParameters->endUpdate();
    }

    void FloorRenderTemplate::beginRender(InScene *_scene, const InPass::RenderContextData *_context,
                                          const BeginRenderTemplate *_beginRender, vk::VulkanGraphicCommands *graphicCommands)
    {
        updateData(_scene->floorRenderer, graphicCommands);

        vk::VulkanRenderPass renderPass = _beginRender->renderPass;
        vk::VulkanFramebuffer framebuffer = _beginRender->framebuffer;
        vk::VulkanBuffer *vertBuffer = _context->fullScreenPassData->getVertexBuffer();
        vk::VulkanBuffer *indexBuffer = _context->fullScreenPassData->getIndexBuffer();

        graphicCommands->beginRenderPass(renderPass, framebuffer);
        graphicCommands->bindPipeline(graphicPipeline->getPipeline(&renderPass));
        graphicCommands->bindDescriptorSet(layout, vertexParameters->getCurrentSet(0), 0);
        graphicCommands->bindDescriptorSet(layout, fragmentParameters->getCurrentSet(0), 1);
        graphicCommands->setDefaultViewport(framebuffer);
        graphicCommands->bindVertexBuffer(vertBuffer, 0);
        graphicCommands->bindIndexBuffer(indexBuffer, 0, VkIndexType::VK_INDEX_TYPE_UINT16);
    }

    void FloorRenderTemplate::endRender(vk::VulkanGraphicCommands *graphicCommands)
    {
        graphicCommands->endRenderPass();
    }

    void FloorRenderTemplate::cleanup(const InPass::CleanupContextData *_context)
    {
        fragmentParameters->freeMemoryPools(_context->descriptorPoolManager);
        vertexParameters->freeMemoryPools(_context->descriptorPoolManager);
        delete fragmentParameters;
        delete vertexParameters;
        _context->graphicPipelinesManager->deleteGraphicPipeline(graphicPipeline);
        vk::VulkanPipelineLayout::destroyPipelineLayout(_context->device, &layout);

        _context->bufferAllocator->destroyBuffer(floorDataUBO);
    }

    void FloorRenderTemplate::draw(vk::VulkanGraphicCommands *graphicCommands)
    {
        graphicCommands->drawIndexed(6, 1, 0, 0, 0);
    }

    void FloorRenderTemplate::updateData(InFloorRenderer *_floor, vk::VulkanGraphicCommands *graphicCommands)
    {
        data.floorTRS = _floor->getTRS();
        data.floorInverseTRS = _floor->getInverseTRS();
        mth::col color = _floor->color;
        data.floorColor = mth::vec3(color.r, color.g, color.b);
        graphicCommands->writeToBuffer(floorDataUBO, 0, sizeof(data), &data);
    }
}