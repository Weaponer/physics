#include "rendering/internal/inDebugLineRenderer.h"
#include "common/phMacros.h"

namespace phys::ge
{
    InDebugLineRenderer::InDebugLineRenderer(uint _index) : GeDebugLineRenderer(),
                                                            index(_index)
    {
        currentBuffer = new (phAllocateMemory(sizeof(PhEasyStack<DrawLine>))) PhEasyStack<DrawLine>();
        nextBuffer = new (phAllocateMemory(sizeof(PhEasyStack<DrawLine>))) PhEasyStack<DrawLine>();
    }

    void InDebugLineRenderer::release()
    {
        nextBuffer->release();
        PH_RELEASE(nextBuffer);
        currentBuffer->release();
        PH_RELEASE(currentBuffer);
    }

    void InDebugLineRenderer::drawLine(mth::vec3 _begin, mth::vec3 _end, mth::col _color, float _duration)
    {
        pushData(_begin, _end, _color, _duration);
    }

    void InDebugLineRenderer::drawCube(mth::vec3 _pos, mth::vec3 _size, mth::col _color, float _duration)
    {
        pushCube(_pos, _size,
                 mth::mat4(1.0f, 0.0f, 0.0f, 0.0f,
                           0.0f, 1.0f, 0.0f, 0.0f,
                           0.0f, 0.0f, 1.0f, 0.0f,
                           0.0f, 0.0f, 0.0f, 1.0f),
                 _color, _duration);
    }

    void InDebugLineRenderer::drawCube(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color, float _duration)
    {
        pushCube(_pos, _size, _mat, _color, _duration);
    }

    void InDebugLineRenderer::drawRect(mth::vec3 _pos, mth::vec3 _size, mth::col _color, float _duration)
    {
        pushRect(_pos, _size,
                 mth::mat4(1.0f, 0.0f, 0.0f, 0.0f,
                           0.0f, 1.0f, 0.0f, 0.0f,
                           0.0f, 0.0f, 1.0f, 0.0f,
                           0.0f, 0.0f, 0.0f, 1.0f),
                 _color, _duration);
    }

    void InDebugLineRenderer::drawRect(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color, float _duration)
    {
        pushRect(_pos, _size, _mat, _color, _duration);
    }

    void InDebugLineRenderer::updateData(float _deltaTime)
    {
        uint count = getCountData();
        const DrawLine *datas = getData();
        for (uint i = 0; i < count; i++)
        {
            DrawLine data = datas[i];
            data.duration -= _deltaTime;
            if (data.duration <= 0.0f)
            {
                continue;
            }

            nextBuffer->assignmentMemoryAndCopy(&data);
        }
        std::swap(currentBuffer, nextBuffer);
        nextBuffer->clearMemory();
    }

    void InDebugLineRenderer::pushCube(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color, float _duration)
    {
        pushData(_pos + _mat.transform(mth::vec3(_size.x, -_size.y, -_size.z)),
                 _pos + _mat.transform(mth::vec3(_size.x, -_size.y, _size.z)), _color, _duration);
        pushData(_pos + _mat.transform(mth::vec3(-_size.x, -_size.y, -_size.z)),
                 _pos + _mat.transform(mth::vec3(-_size.x, -_size.y, _size.z)), _color, _duration);

        pushData(_pos + _mat.transform(mth::vec3(_size.x, -_size.y, _size.z)),
                 _pos + _mat.transform(mth::vec3(-_size.x, -_size.y, _size.z)), _color, _duration);
        pushData(_pos + _mat.transform(mth::vec3(_size.x, -_size.y, -_size.z)),
                 _pos + _mat.transform(mth::vec3(-_size.x, -_size.y, -_size.z)), _color, _duration);

        pushData(_pos + _mat.transform(mth::vec3(_size.x, _size.y, -_size.z)),
                 _pos + _mat.transform(mth::vec3(_size.x, _size.y, _size.z)), _color, _duration);
        pushData(_pos + _mat.transform(mth::vec3(-_size.x, _size.y, -_size.z)),
                 _pos + _mat.transform(mth::vec3(-_size.x, _size.y, _size.z)), _color, _duration);

        pushData(_pos + _mat.transform(mth::vec3(_size.x, _size.y, _size.z)),
                 _pos + _mat.transform(mth::vec3(-_size.x, _size.y, _size.z)), _color, _duration);
        pushData(_pos + _mat.transform(mth::vec3(_size.x, _size.y, -_size.z)),
                 _pos + _mat.transform(mth::vec3(-_size.x, _size.y, -_size.z)), _color, _duration);

        pushData(_pos + _mat.transform(mth::vec3(_size.x, _size.y, _size.z)),
                 _pos + _mat.transform(mth::vec3(_size.x, -_size.y, _size.z)), _color, _duration);
        pushData(_pos + _mat.transform(mth::vec3(_size.x, _size.y, -_size.z)),
                 _pos + _mat.transform(mth::vec3(_size.x, -_size.y, -_size.z)), _color, _duration);

        pushData(_pos + _mat.transform(mth::vec3(-_size.x, _size.y, _size.z)),
                 _pos + _mat.transform(mth::vec3(-_size.x, -_size.y, _size.z)), _color, _duration);
        pushData(_pos + _mat.transform(mth::vec3(-_size.x, _size.y, -_size.z)),
                 _pos + _mat.transform(mth::vec3(-_size.x, -_size.y, -_size.z)), _color, _duration);
    }

    void InDebugLineRenderer::pushRect(mth::vec3 _pos, mth::vec3 _size, mth::mat4 _mat, mth::col _color, float _duration)
    {
        pushData(_pos + _mat.transform(mth::vec3(_size.x, 0, _size.y)), _pos + _mat.transform(mth::vec3(-_size.x, 0, _size.y)), _color, _duration);
        pushData(_pos + _mat.transform(mth::vec3(_size.x, 0, -_size.y)), _pos + _mat.transform(mth::vec3(-_size.x, 0, -_size.y)), _color, _duration);

        pushData(_pos + _mat.transform(mth::vec3(_size.x, 0, _size.y)), _pos + _mat.transform(mth::vec3(_size.x, 0, -_size.y)), _color, _duration);
        pushData(_pos + _mat.transform(mth::vec3(-_size.x, 0, _size.y)), _pos + _mat.transform(mth::vec3(-_size.x, 0, -_size.y)), _color, _duration);
    }

    void InDebugLineRenderer::pushData(const mth::vec3 &_start, const mth::vec3 &_end, const mth::col &_color, float _duration)
    {
        DrawLine *line = currentBuffer->assignmentMemory();
        line->start = _start;
        line->end = _end;
        line->color = mth::vec3(_color.r, _color.g, _color.b);
        line->duration = _duration;
    }
}