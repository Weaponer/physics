#include "rendering/internal/inScene.h"

#include "common/phMacros.h"

#include "rendering/geCamera.h"

namespace phys::ge
{
    InScene::InScene(InMeshManager *_meshManager)
        : meshManager(_meshManager), meshInstanceRegister(NULL),
          sceneEntityPicker(NULL), skyBoxRenderer(NULL), floorRenderer(NULL), camera(NULL)
    {
        meshInstanceRegister = new (phAllocateMemory(sizeof(InMeshInstanceRegister))) InMeshInstanceRegister();
        sceneEntityPicker = new (phAllocateMemory(sizeof(InSceneEntityPicker))) InSceneEntityPicker();
        skyBoxRenderer = new (phAllocateMemory(sizeof(InSkyBoxRenderer))) InSkyBoxRenderer();

        tablePivotRenderers = new (phAllocateMemory(sizeof(PhTableContainer<InPivotRenderer *>))) PhTableContainer<InPivotRenderer *>();
        {
            tablePivotRenderers->setUseDefault(true);
            InPivotRenderer *defaultData = NULL;
            tablePivotRenderers->setDefault(&defaultData);
        }

        tableDebugLineRenderers = new (phAllocateMemory(sizeof(PhTableContainer<InDebugLineRenderer *>))) PhTableContainer<InDebugLineRenderer *>();
        {
            tableDebugLineRenderers->setUseDefault(true);
            InDebugLineRenderer *defaultData = NULL;
            tableDebugLineRenderers->setDefault(&defaultData);
        }
    }

    void InScene::release()
    {
        PH_RELEASE(tablePivotRenderers);
        PH_RELEASE(tableDebugLineRenderers);
        PH_RELEASE(skyBoxRenderer);
        PH_RELEASE(sceneEntityPicker);
        PH_RELEASE(meshInstanceRegister);
    }

    GeCamera *InScene::createCamera()
    {
        GeCamera *camera = new (phAllocateMemory(sizeof(GeCamera))) GeCamera();
        return camera;
    }

    void InScene::destroyCamera(GeCamera *_geCamera)
    {
        PH_RELEASE(_geCamera);
    }

    GeMeshRenderer *InScene::createMeshRenderer()
    {
        InMeshRenderer *meshRenderer = new (phAllocateMemory(sizeof(InMeshRenderer))) InMeshRenderer(meshInstanceRegister);
        return meshRenderer;
    }

    void InScene::destroyMeshRenderer(GeMeshRenderer *_meshRenderer)
    {
        InMeshRenderer *meshRenderer = dynamic_cast<InMeshRenderer *>(_meshRenderer);
        PH_RELEASE(meshRenderer)
    }

    GeFloorRenderer *InScene::createFloorRenderer()
    {
        PH_ASSERT(floorRenderer == NULL, "This scene already has a floor renderer.")
        floorRenderer = new (phAllocateMemory(sizeof(InFloorRenderer))) InFloorRenderer();
        return floorRenderer;
    }

    void InScene::destroyFloorRenderer(GeFloorRenderer *_floorRendere)
    {
        InFloorRenderer *inFloorRenderer = dynamic_cast<InFloorRenderer *>(_floorRendere);
        PH_ASSERT(floorRenderer == inFloorRenderer, "This floor renderer does not belong to this scene.")
        PH_RELEASE(floorRenderer)
        floorRenderer = NULL;
    }

    GeFloorRenderer *InScene::getFloorRenderer()
    {
        return floorRenderer;
    }

    GeSceneEntityPicker *InScene::getSceneEntityPicker()
    {
        return sceneEntityPicker;
    }

    GeSkyBoxRenderer *InScene::getSkyBoxRenderer()
    {
        return skyBoxRenderer;
    }

    GeMesh *InScene::createMesh(const PhObjectMesh::Mesh *_mesh)
    {
        return meshManager->registerMesh(_mesh);
    }

    void InScene::destroyMesh(GeMesh *_mesh)
    {
        meshManager->unregisterMesh(_mesh);
    }

    GePivotRenderer *InScene::createPivotRenderer()
    {
        uint index = 0;
        InPivotRenderer **data = tablePivotRenderers->assignmentMemory(index);
        *data = new (phAllocateMemory(sizeof(InPivotRenderer))) InPivotRenderer(index);
        return *data;
    }

    void InScene::destroyPivotRenderer(GePivotRenderer *_pivotRenderer)
    {
        InPivotRenderer *inDebugLineRenderer = dynamic_cast<InPivotRenderer *>(_pivotRenderer);
        tablePivotRenderers->popMemory(inDebugLineRenderer->getIndex());
        PH_RELEASE(inDebugLineRenderer);
    }

    GeDebugLineRenderer *InScene::createDebugLineRenderer()
    {
        uint index = 0;
        InDebugLineRenderer **data = tableDebugLineRenderers->assignmentMemory(index);
        *data = new (phAllocateMemory(sizeof(InDebugLineRenderer))) InDebugLineRenderer(index);
        return *data;
    }

    void InScene::destroyDebugLineRenderer(GeDebugLineRenderer *_debugLineRenderer)
    {
        InDebugLineRenderer *inDebugLineRenderer = dynamic_cast<InDebugLineRenderer *>(_debugLineRenderer);
        tableDebugLineRenderers->popMemory(inDebugLineRenderer->getIndex());
        PH_RELEASE(inDebugLineRenderer);
    }

    void InScene::updateScene(float _deltaTime)
    {
        uint count = tableDebugLineRenderers->getPosBuffer();
        for (uint i = 0; i < count; i++)
        {
            InDebugLineRenderer *item = *tableDebugLineRenderers->getMemory(i);
            if (item != NULL)
                item->updateData(_deltaTime);
        }
    }
}