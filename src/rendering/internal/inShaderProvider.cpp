#include "rendering/internal/inShaderProvider.h"

#include "common/phMacros.h"

#define RENDER_SHADER_TAG "render"

namespace phys::ge
{
    InShaderProvider::InShaderProvider(PhResourceProvider *_provider, vk::VulkanDevice *_device)
        : provider(_provider), device(_device), shaderManager(NULL), poolManager(NULL)
    {
        shaderManager = new vk::ShaderManager(_device);
        poolManager = new vk::VulkanDescriptorPoolManager(_device);

        if (!provider->getCountShaders(RENDER_SHADER_TAG))
            provider->loadShadersPacket("render.spak", RENDER_SHADER_TAG);

        uint32_t count = provider->getCountShaders(RENDER_SHADER_TAG);
        vk::ShaderData *const *data = provider->getShadersPerTag(RENDER_SHADER_TAG);
        for (uint32_t i = 0; i < count; i++)
        {
            data[i]->prepareData(_device);
            vk::ShaderModule module;
            shaderManager->registerShader(data[i], &module);
        }

        poolManager->preparePools(shaderManager);
    }

    void InShaderProvider::release()
    {
        delete poolManager;
        delete shaderManager;
    }

    vk::ShaderModule InShaderProvider::getShaderModule(const char *_name)
    {
        vk::ShaderModule module{};
        PH_ASSERT(shaderManager->getShaderByName(_name, &module), "The Shader doesn`t found.");
        return module;
    }

    vk::VulkanDescriptorPoolManager *InShaderProvider::getVulkanDescriptorPoolManager()
    {
        return poolManager;
    }
}