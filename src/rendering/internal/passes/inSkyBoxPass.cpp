#include "rendering/internal/passes/inSkyBoxPass.h"

#include "rendering/internal/inSkyBoxRenderer.h"

namespace phys::ge
{
    InSkyBoxPass::InSkyBoxPass() : InPass()
    {
    }

    void InSkyBoxPass::release()
    {
    }

    void InSkyBoxPass::render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context)
    {
        graphicCommands->debugMarkerBegin("SkyBoxPass", InDebugMarkersUtils::passCallColor());
        InSkyBoxRenderer *skyBoxRenderer = _scene->skyBoxRenderer;

        VkClearColorValue value;
        value.float32[0] = skyBoxRenderer->color.r;
        value.float32[1] = skyBoxRenderer->color.g;
        value.float32[2] = skyBoxRenderer->color.b;
        value.float32[3] = 1.0f;
        graphicCommands->clearImage(_context->imageBuffer, &value);
        graphicCommands->debugMarkerEnd();
    }
}