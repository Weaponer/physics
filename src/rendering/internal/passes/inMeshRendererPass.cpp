#include "rendering/internal/passes/inMeshRendererPass.h"
#include "rendering/internal/graphicPipeline/inMeshRendererCullingManager.h"

#include "gpuCommon/rendering/vulkanVertexAttributeState.h"

#include "common/phObjectMesh.h"
#include "common/phMacros.h"

namespace phys::ge
{
    InMeshRendererPass::InstancingMainPass::InstancingMainPass() : InInstancingGenerator()
    {
    }

    void InMeshRendererPass::InstancingMainPass::release()
    {
        InInstancingGenerator::release();
    }

    uint32_t InMeshRendererPass::InstancingMainPass::getSizeInstanceData()
    {
        return sizeof(InstancingMainPass::Data);
    }

    bool InMeshRendererPass::InstancingMainPass::pushInstanceData(GeRenderer *_renderer, void *_pData)
    {
        GeMeshRenderer *renderer = dynamic_cast<GeMeshRenderer *>(_renderer);
        InstancingMainPass::Data data;
        data.TRS = renderer->getTRS();
        data.color = renderer->getColor();
        *((InstancingMainPass::Data *)_pData) = data;
        return true;
    }

    InMesh *InMeshRendererPass::InstancingMainPass::getMesh(GeRenderer *_renderer)
    {
        return (dynamic_cast<InMeshRenderer *>(_renderer))->mesh;
    }

    InMeshRendererPass::InMeshRendererPass() : instancingMainPass(NULL), meshRender(NULL), wireframeRender(NULL)
    {
    }

    void InMeshRendererPass::release()
    {
    }

    void InMeshRendererPass::init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        instancingMainPass = new (phAllocateMemory(sizeof(InstancingMainPass))) InstancingMainPass();
        instancingMainPass->initBuffers(_context->bufferAllocator, _context->mainFamilyIndex);

        vk::VulkanBlendColorState colorState = vk::VulkanBlendColorState();
        colorState.setCountColorAttachments(1);

        vk::VulkanVertexAttributeState attributeState = vk::VulkanVertexAttributeState();
        attributeState.setEnableAttributessInstance(true);
        attributeState.setSizeInputVertex(sizeof(PhObjectMesh::Vertex));
        attributeState.addInputVertexAttribute(offsetof(PhObjectMesh::Vertex, pos), VK_FORMAT_R32G32B32_SFLOAT);
        attributeState.addInputVertexAttribute(offsetof(PhObjectMesh::Vertex, uv), VK_FORMAT_R32G32_SFLOAT);
        attributeState.addInputVertexAttribute(offsetof(PhObjectMesh::Vertex, normal), VK_FORMAT_R32G32B32_SFLOAT);
        attributeState.addInputVertexAttribute(offsetof(PhObjectMesh::Vertex, tangent), VK_FORMAT_R32G32B32_SFLOAT);
        attributeState.setSizeInputInstance(sizeof(InstancingMainPass::Data));
        attributeState.addInputInstanceAttributeMat4Float(offsetof(InstancingMainPass::Data, TRS));
        attributeState.addInputInstanceAttribute(offsetof(InstancingMainPass::Data, color), VK_FORMAT_R32G32B32A32_SFLOAT);

        RenderTemplate::InitRenderTemplate initTemplate{};
        MeshRenderTemplate::MeshTemplateInitData meshRenderInit{};

        meshRenderInit.isWireframe = false;
        initTemplate.customData = &meshRenderInit;
        initTemplate.vertexShader = "meshVertex";
        initTemplate.fragmentShader = "meshFragment";
        initTemplate.vertexAttributeState = &attributeState;
        initTemplate.blendColorState = &colorState;
        initTemplate.renderPass = _context->renderPass;

        meshRender = new (phAllocateMemory(sizeof(MeshRenderTemplate))) MeshRenderTemplate();
        meshRender->init(_context, &initTemplate, _graphicQueue);

        // wireframe
        initTemplate.vertexShader = "meshLineVertex";
        initTemplate.fragmentShader = "meshLineFragment";
        meshRenderInit.isWireframe = true;

        attributeState.clearAttributes();
        attributeState.setSizeInputVertex(sizeof(PhObjectMesh::LineVertex));
        attributeState.addInputVertexAttribute(offsetof(PhObjectMesh::LineVertex, pos), VK_FORMAT_R32G32B32_SFLOAT);
        attributeState.setEnableAttributessInstance(true);
        attributeState.setSizeInputInstance(sizeof(InstancingMainPass::Data));
        attributeState.addInputInstanceAttributeMat4Float(offsetof(InstancingMainPass::Data, TRS));

        wireframeRender = new (phAllocateMemory(sizeof(MeshRenderTemplate))) MeshRenderTemplate();
        wireframeRender->init(_context, &initTemplate, _graphicQueue);
    }

    void InMeshRendererPass::render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context)
    {
        graphicCommands->debugMarkerBegin("MeshesPass", InDebugMarkersUtils::passCallColor());

        _context->meshRendererCullingManager->updateInstancingGenerator(instancingMainPass, graphicCommands);

        uint32_t count = instancingMainPass->getCountDrawCalls();
        if (!count)
            return;

        RenderTemplate::BeginRenderTemplate beginRender{};
        beginRender.renderPass = _context->renderPass;
        beginRender.framebuffer = _context->framebuffer;

        wireframeRender->beginRender(_scene, _context, &beginRender, graphicCommands);

        const InInstancingGenerator::DrawCall *drawCalls = instancingMainPass->getDrawCalls();
        RenderTemplate::DrawRenderTemplate drawData{};

        for (uint32_t i = 0; i < count; i++)
        {
            graphicCommands->debugMarkerBegin("Wireframe", InDebugMarkersUtils::subPassColor());

            InInstancingGenerator::DrawCall drawCall = drawCalls[i];

            drawData.vertexBuffer = drawCall.mesh->getLineVertexBuffer();
            drawData.countVertex = drawCall.mesh->getCountLineVertex();
            drawData.instanceBuffer = drawCall.buffer;
            drawData.countInstance = drawCall.countInstance;
            drawData.offsetInstanceBuffer = drawCall.offset;

            wireframeRender->draw(&drawData, graphicCommands);
            graphicCommands->debugMarkerEnd();
        }
        wireframeRender->endRender(graphicCommands);

        meshRender->beginRender(_scene, _context, &beginRender, graphicCommands);
        for (uint32_t i = 0; i < count; i++)
        {
            graphicCommands->debugMarkerBegin("Mesh", InDebugMarkersUtils::subPassColor());
            InInstancingGenerator::DrawCall drawCall = drawCalls[i];

            drawData.vertexBuffer = drawCall.mesh->getVertexBuffer();
            drawData.countVertex = 0;
            drawData.indexBuffer = drawCall.mesh->getIndexBuffer();
            drawData.indexType = drawCall.mesh->getIndexType();
            drawData.countIndex = drawCall.mesh->getCountIndex();
            drawData.instanceBuffer = drawCall.buffer;
            drawData.countInstance = drawCall.countInstance;
            drawData.offsetInstanceBuffer = drawCall.offset;

            meshRender->drawIndexed(&drawData, graphicCommands);
            graphicCommands->debugMarkerEnd();
        }
        meshRender->endRender(graphicCommands);

        graphicCommands->debugMarkerEnd();
    }

    void InMeshRendererPass::cleanup(const CleanupContextData *_context)
    {
        wireframeRender->cleanup(_context);
        meshRender->cleanup(_context);
        PH_RELEASE(wireframeRender);
        PH_RELEASE(meshRender);
        PH_RELEASE(instancingMainPass);
    }
}