#include <vulkan/vulkan.h>

#include "rendering/internal/passes/inScenePickerPass.h"

#include "common/phMacros.h"

namespace phys::ge
{
    InScenePickerPass::InstancingScenePickerPass::InstancingScenePickerPass() : InInstancingGenerator()
    {
    }

    void InScenePickerPass::InstancingScenePickerPass::release()
    {
        InInstancingGenerator::release();
    }

    uint32_t InScenePickerPass::InstancingScenePickerPass::getSizeInstanceData()
    {
        return sizeof(InstancingScenePickerPass::Data);
    }

    bool InScenePickerPass::InstancingScenePickerPass::pushInstanceData(GeRenderer *_renderer, void *_pData)
    {
        GeMeshRenderer *renderer = dynamic_cast<GeMeshRenderer *>(_renderer);
        InstancingScenePickerPass::Data data;
        data.TRS = renderer->getTRS();
        data.indexObject = renderer->getIndexScenePicker();
        if (data.indexObject == 0)
            return false;

        *((InstancingScenePickerPass::Data *)_pData) = data;
        return true;
    }

    InMesh *InScenePickerPass::InstancingScenePickerPass::getMesh(GeRenderer *_renderer)
    {
        return (dynamic_cast<InMeshRenderer *>(_renderer))->mesh;
    }

    InScenePickerPass::InScenePickerPass() : instancingScenePickerPass(NULL),
                                             indexesTarget(NULL), indexesTargetView(), resultBuffer(NULL), renderPass(), framebuffer(),
                                             floorRenderTemplate(NULL), meshRenderTemplate(NULL), firstUpdate(true)
    {
    }

    void InScenePickerPass::release()
    {
    }

    void InScenePickerPass::init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        instancingScenePickerPass = new (phAllocateMemory(sizeof(InstancingScenePickerPass))) InstancingScenePickerPass();
        instancingScenePickerPass->initBuffers(_context->bufferAllocator, _context->mainFamilyIndex);

        indexesTarget = _context->imageAllocator->createImage(vk::MemoryUseType_Long, vk::VulkanImageType_ImageStatic,
                                                              VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                                                              VK_IMAGE_TYPE_2D, VK_FORMAT_R32_UINT, VK_IMAGE_ASPECT_COLOR_BIT,
                                                              _context->swapchain->getWidth(), _context->swapchain->getHeight(),
                                                              1, 1, 1, 1, &_context->mainFamilyIndex, "ScenePickerIndexesBuffer");

        vk::VulkanImageView::createImageView(_context->device, VK_IMAGE_VIEW_TYPE_2D,
                                             indexesTarget, &indexesTargetView, "ScenePickerIndexesBufferView");

        resultBuffer = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_Staging,
                                                               sizeof(uint), 1, &_context->mainFamilyIndex, "ScenePickerResultBuffer");

        _context->renderPassesManager->beginCreateRenderPass();
        _context->renderPassesManager->addColorAttachments(indexesTarget);
        _context->renderPassesManager->setDepthStencil(_context->depthBuffer);
        _context->renderPassesManager->endCreate(&renderPass);
        vk::VulkanFramebuffer::createFramebuffer(_context->device, renderPass, 1, &indexesTarget, &indexesTargetView,
                                                 _context->depthBuffer, _context->depthBufferView, &framebuffer);

        floorRenderTemplate = new (phAllocateMemory(sizeof(FloorRenderTemplate))) FloorRenderTemplate();
        RenderTemplate::InitRenderTemplate initTemplate{};
        initTemplate.renderPass = renderPass;
        initTemplate.vertexShader = "fullScreenVertex";
        initTemplate.fragmentShader = "floorScenePicker";

        vk::VulkanBlendColorState colorState = vk::VulkanBlendColorState();
        colorState.setCountColorAttachments(1);

        initTemplate.blendColorState = &colorState;
        floorRenderTemplate->init(_context, &initTemplate, _graphicQueue);

        vk::VulkanVertexAttributeState attributeState = vk::VulkanVertexAttributeState();
        attributeState.setEnableAttributessInstance(true);
        attributeState.setSizeInputVertex(sizeof(PhObjectMesh::Vertex));
        attributeState.addInputVertexAttribute(offsetof(PhObjectMesh::Vertex, pos), VK_FORMAT_R32G32B32_SFLOAT);
        attributeState.setSizeInputInstance(sizeof(InstancingScenePickerPass::Data));
        attributeState.addInputInstanceAttributeMat4Float(offsetof(InstancingScenePickerPass::Data, TRS));
        attributeState.addInputInstanceAttribute(offsetof(InstancingScenePickerPass::Data, indexObject), VK_FORMAT_R32_UINT);

        MeshRenderTemplate::MeshTemplateInitData meshRenderInit{};

        meshRenderInit.isWireframe = false;
        initTemplate.customData = &meshRenderInit;
        initTemplate.vertexShader = "meshScenePickerVertex";
        initTemplate.fragmentShader = "meshScenePickerFragment";
        initTemplate.vertexAttributeState = &attributeState;
        initTemplate.blendColorState = &colorState;

        meshRenderTemplate = new (phAllocateMemory(sizeof(MeshRenderTemplate))) MeshRenderTemplate();
        meshRenderTemplate->init(_context, &initTemplate, _graphicQueue);
    }

    void InScenePickerPass::render(InScene *_scene, vk::VulkanGraphicCommands *_graphicCommands, const RenderContextData *_context)
    {
        if (!firstUpdate)
        {
            _graphicCommands->readFromBuffer(resultBuffer, 0, sizeof(uint), &(_scene->sceneEntityPicker->lastIndex));
        }
        else
        {
            _scene->sceneEntityPicker->lastIndex = 0;
            firstUpdate = false;
        }

        _graphicCommands->barrierImageAccess(indexesTarget, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                             VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                             VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT);
        VkClearColorValue value{};
        _graphicCommands->clearImage(indexesTarget, &value);
        _graphicCommands->barrierImageAccess(indexesTarget, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                             VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                             VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

        RenderTemplate::BeginRenderTemplate beginRender{};
        beginRender.renderPass = renderPass;
        beginRender.framebuffer = framebuffer;

        if (_scene->floorRenderer != NULL)
        {
            floorRenderTemplate->beginRender(_scene, _context, &beginRender, _graphicCommands);
            uint floorIndex = _scene->floorRenderer->getIndexScenePicker();
            _graphicCommands->pushConstant(floorRenderTemplate->getLayout(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(uint), &floorIndex);
            floorRenderTemplate->draw(_graphicCommands);
            floorRenderTemplate->endRender(_graphicCommands);
        }

        _context->meshRendererCullingManager->updateInstancingGenerator(instancingScenePickerPass, _graphicCommands);
        uint32_t count = instancingScenePickerPass->getCountDrawCalls();
        if (count)
        {
            const InInstancingGenerator::DrawCall *drawCalls = instancingScenePickerPass->getDrawCalls();
            RenderTemplate::DrawRenderTemplate drawData{};

            meshRenderTemplate->beginRender(_scene, _context, &beginRender, _graphicCommands);
            for (uint32_t i = 0; i < count; i++)
            {
                InInstancingGenerator::DrawCall drawCall = drawCalls[i];

                drawData.vertexBuffer = drawCall.mesh->getVertexBuffer();
                drawData.countVertex = 0;
                drawData.indexBuffer = drawCall.mesh->getIndexBuffer();
                drawData.indexType = drawCall.mesh->getIndexType();
                drawData.countIndex = drawCall.mesh->getCountIndex();
                drawData.instanceBuffer = drawCall.buffer;
                drawData.countInstance = drawCall.countInstance;
                drawData.offsetInstanceBuffer = drawCall.offset;

                meshRenderTemplate->drawIndexed(&drawData, _graphicCommands);
            }
            meshRenderTemplate->endRender(_graphicCommands);
        }
        _graphicCommands->barrierImageAccess(indexesTarget, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                             VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                                             VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT);

        {
            uint32_t width = indexesTarget->getWidth() - 1;
            uint32_t height = indexesTarget->getHeight() - 1;
            mth::vec2 pos = _scene->sceneEntityPicker->position;
            VkBufferImageCopy copyInfo{};
            copyInfo.bufferOffset = 0;
            copyInfo.bufferImageHeight = 0;
            copyInfo.bufferRowLength = 0;
            copyInfo.imageExtent.width = 1;
            copyInfo.imageExtent.height = 1;
            copyInfo.imageExtent.depth = 1;
            copyInfo.imageOffset.x = (uint32_t)(pos.x * width);
            copyInfo.imageOffset.y = (uint32_t)(pos.y * height);
            copyInfo.imageOffset.z = 0;
            copyInfo.imageSubresource.aspectMask = indexesTarget->getAspect();
            copyInfo.imageSubresource.baseArrayLayer = 0;
            copyInfo.imageSubresource.layerCount = 1;
            copyInfo.imageSubresource.mipLevel = 0;
            _graphicCommands->copyImageToBuffer(indexesTarget, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, resultBuffer, &copyInfo);
        }
    }

    void InScenePickerPass::cleanup(const CleanupContextData *_context)
    {
        meshRenderTemplate->cleanup(_context);
        PH_RELEASE(meshRenderTemplate);

        floorRenderTemplate->cleanup(_context);
        PH_RELEASE(floorRenderTemplate);

        vk::VulkanFramebuffer::destroyFramebuffer(_context->device, &framebuffer);
        _context->bufferAllocator->destroyBuffer(resultBuffer);
        vk::VulkanImageView::destroyImageView(_context->device, indexesTargetView);
        _context->imageAllocator->destroyImage(indexesTarget);

        PH_RELEASE(instancingScenePickerPass);
    }
}