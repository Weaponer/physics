#include "rendering/internal/passes/inDebugLinesPass.h"

namespace phys::ge
{
    InDebugLinesPass::InDebugLinesPass() : vertexModul(), fragmentModul(), layout(),
                                           graphicOverDepthPipeline(NULL), graphicUnderDepthPipeline(NULL),
                                           vertexParameters(NULL),
                                           vertexBuffer(NULL)
    {
    }

    void InDebugLinesPass::release()
    {
    }

    void InDebugLinesPass::init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        vertexModul = _context->shaderProvider->getShaderModule("debugLineVertex");
        fragmentModul = _context->shaderProvider->getShaderModule("debugLineFragment");

        vk::ShaderModule modules[2] = {vertexModul, fragmentModul};
        vk::VulkanPipelineLayout::createPipelineLayout(_context->device, 2, modules, &layout);

        graphicOverDepthPipeline = _context->graphicPipelinesManager->createGraphicPipeline();
        graphicUnderDepthPipeline = _context->graphicPipelinesManager->createGraphicPipeline();

        vk::VulkanVertexAttributeState vertexState = vk::VulkanVertexAttributeState();
        vertexState.setSizeInputVertex(sizeof(LineVertex));
        vertexState.addInputVertexAttribute(offsetof(LineVertex, point), VK_FORMAT_R32G32B32_SFLOAT);
        vertexState.addInputVertexAttribute(offsetof(LineVertex, color), VK_FORMAT_R32G32B32_SFLOAT);

        vk::VulkanBlendColorState colorState = vk::VulkanBlendColorState();
        colorState.setCountColorAttachments(1);

        const float lineWidth = 1.5f;
        graphicOverDepthPipeline->setVertexModule(vertexModul);
        graphicOverDepthPipeline->setFragmentModule(fragmentModul);
        graphicOverDepthPipeline->setPipelineLayout(layout);
        graphicOverDepthPipeline->setBlendColorState(&colorState);

        graphicOverDepthPipeline->setVertexAttributeState(&vertexState);
        graphicOverDepthPipeline->setTopology(VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
        graphicOverDepthPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_VIEWPORT, true);
        graphicOverDepthPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_SCISSOR, true);
        graphicOverDepthPipeline->setLineWidth(lineWidth);
        graphicOverDepthPipeline->setDepthWrite(true);
        graphicOverDepthPipeline->setDepthTest(true);

        _context->graphicPipelinesManager->updateGraphicPipeline(graphicOverDepthPipeline, _context->renderPass);
        graphicOverDepthPipeline->setBlendColorState(NULL);
        graphicOverDepthPipeline->setVertexAttributeState(NULL);

        colorState.setColorBlendAttachmentState(0, VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT,
                                                true, VK_BLEND_OP_ADD, VK_BLEND_OP_ADD,
                                                VK_BLEND_FACTOR_SRC_ALPHA, VK_BLEND_FACTOR_SRC_ALPHA, VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA, VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA);

        graphicUnderDepthPipeline->setVertexModule(vertexModul);
        graphicUnderDepthPipeline->setFragmentModule(fragmentModul);
        graphicUnderDepthPipeline->setPipelineLayout(layout);
        graphicUnderDepthPipeline->setBlendColorState(&colorState);

        graphicUnderDepthPipeline->setVertexAttributeState(&vertexState);
        graphicUnderDepthPipeline->setTopology(VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
        graphicUnderDepthPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_VIEWPORT, true);
        graphicUnderDepthPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_SCISSOR, true);
        graphicUnderDepthPipeline->setLineWidth(lineWidth);
        graphicUnderDepthPipeline->setDepthWrite(false);
        graphicUnderDepthPipeline->setDepthTest(true);
        graphicUnderDepthPipeline->setDepthCompareOp(VK_COMPARE_OP_GREATER);

        _context->graphicPipelinesManager->updateGraphicPipeline(graphicUnderDepthPipeline, _context->renderPass);

        graphicUnderDepthPipeline->setBlendColorState(NULL);
        graphicUnderDepthPipeline->setVertexAttributeState(NULL);
        vertexParameters = new vk::ShaderAutoParameters(_context->device, vertexModul);
        vertexParameters->takeMemoryPools(_context->descriptorPoolManager, 1);

        vertexParameters->beginUpdate();
        vertexParameters->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, _context->cameraData->getCameraData());
        vertexParameters->endUpdate();
    }

    void InDebugLinesPass::render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context)
    {
        if (!_scene->tableDebugLineRenderers->getCountUse())
            return;

        uint32_t countVertexes = updateData(_scene, graphicCommands, _context);
        if (!countVertexes)
            return;

        graphicCommands->debugMarkerBegin("DebugLinesPass", InDebugMarkersUtils::passCallColor());

        vk::VulkanRenderPass renderPass = _context->renderPass;
        vk::VulkanFramebuffer framebuffer = _context->framebuffer;

        float alpha = 1.0;

        graphicCommands->debugMarkerBegin("FrontDebugLines", InDebugMarkersUtils::subPassColor());
        {
            graphicCommands->beginRenderPass(renderPass, framebuffer);
            graphicCommands->setDefaultViewport(framebuffer);
            graphicCommands->bindPipeline(graphicOverDepthPipeline->getPipeline(&renderPass));
            graphicCommands->bindDescriptorSet(layout, vertexParameters->getCurrentSet(0), 0);
            graphicCommands->pushConstant(layout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(alpha), &alpha);
            graphicCommands->bindVertexBuffer(vertexBuffer, 0);
            graphicCommands->draw(countVertexes, 1, 0, 0);
        }
        graphicCommands->debugMarkerEnd();

        graphicCommands->debugMarkerBegin("BackDebugLines", InDebugMarkersUtils::subPassColor());
        {
            alpha = 0.3f;
            graphicCommands->bindPipeline(graphicUnderDepthPipeline->getPipeline(&renderPass));
            graphicCommands->bindDescriptorSet(layout, vertexParameters->getCurrentSet(0), 0);
            graphicCommands->pushConstant(layout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(alpha), &alpha);
            graphicCommands->bindVertexBuffer(vertexBuffer, 0);
            graphicCommands->draw(countVertexes, 1, 0, 0);
            graphicCommands->endRenderPass();
        }
        graphicCommands->debugMarkerEnd();

        graphicCommands->debugMarkerEnd();
    }

    void InDebugLinesPass::cleanup(const CleanupContextData *_context)
    {
        vertexParameters->freeMemoryPools(_context->descriptorPoolManager);
        delete vertexParameters;
        _context->graphicPipelinesManager->deleteGraphicPipeline(graphicOverDepthPipeline);
        _context->graphicPipelinesManager->deleteGraphicPipeline(graphicUnderDepthPipeline);
        vk::VulkanPipelineLayout::destroyPipelineLayout(_context->device, &layout);

        if (vertexBuffer)
            _context->bufferAllocator->destroyBuffer(vertexBuffer);
    }

    uint32_t InDebugLinesPass::updateData(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context)
    {
        uint32_t countLines = 0;
        uint32_t count = _scene->tableDebugLineRenderers->getPosBuffer();
        InDebugLineRenderer **datas = _scene->tableDebugLineRenderers->getMemory(0);
        for (uint32_t i = 0; i < count; i++)
        {
            if (datas[i] != NULL)
                countLines += datas[i]->getCountData();
        }

        if (!countLines)
            return 0;

        // allocate buffer
        uint32_t needSize = countLines * 2 * sizeof(LineVertex);
        if (vertexBuffer == NULL || (uint32_t)vertexBuffer->getSize() < needSize)
        {
            if (vertexBuffer)
                _context->bufferAllocator->destroyBuffer(vertexBuffer);

            uint32_t pageSize = _context->bufferAllocator->getMemoryController()->getSizePage(vk::MemoryUseType_Long);
            uint32_t size = std::min(pageSize, needSize);
            vertexBuffer = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferDynamic, size,
                                                                   1, &_context->mainFamilyIndex, "DebugLines_vertexes");
        }

        // fill buffer
        LineVertex *dst = (LineVertex *)graphicCommands->beginWrite(vertexBuffer, 0, vertexBuffer->getSize());
        uint32_t maxCountVertex = vertexBuffer->getSize() / sizeof(LineVertex);
        maxCountVertex -= maxCountVertex & 1;

        uint32_t indexVertex = 0;
        for (uint32_t i = 0; i < count; i++)
        {
            if (datas[i] == NULL)
                continue;

            uint32_t lines = datas[i]->getCountData();
            const InDebugLineRenderer::DrawLine *subDatas = datas[i]->getData();
            for (uint32_t i2 = 0; i2 < lines && indexVertex < maxCountVertex; i2++)
            {
                InDebugLineRenderer::DrawLine line = subDatas[i2];

                LineVertex vert;
                vert.point = line.start;
                vert.color = line.color;
                *(dst + indexVertex) = vert;
                indexVertex++;
                vert.point = line.end;
                *(dst + indexVertex) = vert;
                indexVertex++;
            }
        }
        graphicCommands->endWrite(vertexBuffer);

        return indexVertex;
    }
}