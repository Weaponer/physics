#include <MTH/matrix.h>
#include <MTH/glGen_matrix.h>

#include "common/phMacros.h"

#include "rendering/internal/passes/inPivotRendererPass.h"
#include "rendering/internal/graphicPipeline/inStaticBufferLoader.h"

namespace phys::ge
{

    static float meshCubeData[] = {
        -1.0000f, -1.0000f, 1.0000f, -1.0000f, 1.0000f, 1.0000f, -1.0000f, 1.0000f, -1.0000f,    //
        -1.0000f, -1.0000f, -1.0000f, -1.0000f, -1.0000f, -1.0000f, -1.0000f, 1.0000f, -1.0000f, //
        1.0000f, 1.0000f, -1.0000f, 1.0000f, -1.0000f, -1.0000f, 1.0000f, -1.0000f, -1.0000f,    //
        1.0000f, 1.0000f, -1.0000f, 1.0000f, 1.0000f, 1.0000f, 1.0000f, -1.0000f, 1.0000f,       //
        1.0000f, -1.0000f, 1.0000f, 1.0000f, 1.0000f, 1.0000f, -1.0000f, 1.0000f, 1.0000f,       //
        -1.0000f, -1.0000f, 1.0000f, -1.0000f, -1.0000f, -1.0000f, 1.0000f, -1.0000f, -1.0000f,  //
        1.0000f, -1.0000f, 1.0000f, -1.0000f, -1.0000f, 1.0000f, 1.0000f, 1.0000f, -1.0000f,     //
        -1.0000f, 1.0000f, -1.0000f, -1.0000f, 1.0000f, 1.0000f, 1.0000f, 1.0000f, 1.0000f       //
    };
    static uint16_t meshCubeIndexesData[] = {
        0, 1, 2,    //
        0, 2, 3,    //
        4, 5, 6,    //
        4, 6, 7,    //
        8, 9, 10,   //
        8, 10, 11,  //
        12, 13, 14, //
        12, 14, 15, //
        16, 17, 18, //
        16, 18, 19, //
        20, 21, 22, //
        20, 22, 23  //
    };

    static float meshConusData[] = {
        0.0000f, 0.0000f, -1.0000f, 0.0000f, 1.0000f, -0.0000f, 0.2588f, 0.0000f, -0.9659f,   //
        0.2588f, 0.0000f, -0.9659f, 0.0000f, 1.0000f, -0.0000f, 0.5000f, 0.0000f, -0.8660f,   //
        0.5000f, 0.0000f, -0.8660f, 0.0000f, 1.0000f, -0.0000f, 0.7071f, 0.0000f, -0.7071f,   //
        0.7071f, 0.0000f, -0.7071f, 0.0000f, 1.0000f, -0.0000f, 0.8660f, 0.0000f, -0.5000f,   //
        0.8660f, 0.0000f, -0.5000f, 0.0000f, 1.0000f, -0.0000f, 0.9659f, 0.0000f, -0.2588f,   //
        0.9659f, 0.0000f, -0.2588f, 0.0000f, 1.0000f, -0.0000f, 1.0000f, 0.0000f, -0.0000f,   //
        0.9659f, 0.0000f, 0.2588f, 0.9659f, 0.0000f, 0.2588f, 0.0000f, 1.0000f, -0.0000f,     //
        0.8660f, 0.0000f, 0.5000f, 0.8660f, 0.0000f, 0.5000f, 0.0000f, 1.0000f, -0.0000f,     //
        0.7071f, 0.0000f, 0.7071f, 0.7071f, 0.0000f, 0.7071f, 0.0000f, 1.0000f, -0.0000f,     //
        0.5000f, 0.0000f, 0.8660f, 0.5000f, 0.0000f, 0.8660f, 0.0000f, 1.0000f, -0.0000f,     //
        0.2588f, 0.0000f, 0.9659f, 0.2588f, 0.0000f, 0.9659f, 0.0000f, 1.0000f, -0.0000f,     //
        0.0000f, 0.0000f, 1.0000f, 0.0000f, 0.0000f, 1.0000f, 0.0000f, 1.0000f, -0.0000f,     //
        -0.2588f, 0.0000f, 0.9659f, -0.2588f, 0.0000f, 0.9659f, 0.0000f, 1.0000f, -0.0000f,   //
        -0.5000f, 0.0000f, 0.8660f, -0.5000f, 0.0000f, 0.8660f, 0.0000f, 1.0000f, -0.0000f,   //
        -0.7071f, 0.0000f, 0.7071f, -0.7071f, 0.0000f, 0.7071f, 0.0000f, 1.0000f, -0.0000f,   //
        -0.8660f, 0.0000f, 0.5000f, -0.8660f, 0.0000f, 0.5000f, 0.0000f, 1.0000f, -0.0000f,   //
        -0.9659f, 0.0000f, 0.2588f, -0.9659f, 0.0000f, 0.2588f, 0.0000f, 1.0000f, -0.0000f,   //
        -1.0000f, 0.0000f, -0.0000f, -1.0000f, 0.0000f, -0.0000f, 0.0000f, 1.0000f, -0.0000f, //
        -0.9659f, 0.0000f, -0.2588f, -0.9659f, 0.0000f, -0.2588f, 0.0000f, 1.0000f, -0.0000f, //
        -0.8660f, 0.0000f, -0.5000f, -0.8660f, 0.0000f, -0.5000f, 0.0000f, 1.0000f, -0.0000f, //
        -0.7071f, 0.0000f, -0.7071f, -0.7071f, 0.0000f, -0.7071f, 0.0000f, 1.0000f, -0.0000f, //
        -0.5000f, 0.0000f, -0.8660f, 0.0000f, 0.0000f, -1.0000f, 0.2588f, 0.0000f, -0.9659f,  //
        0.5000f, 0.0000f, -0.8660f, 0.0000f, 0.0000f, -1.0000f, 0.5000f, 0.0000f, -0.8660f,   //
        0.7071f, 0.0000f, -0.7071f, 0.0000f, 0.0000f, -1.0000f, 0.7071f, 0.0000f, -0.7071f,   //
        0.8660f, 0.0000f, -0.5000f, 0.0000f, 0.0000f, -1.0000f, 0.8660f, 0.0000f, -0.5000f,   //
        0.9659f, 0.0000f, -0.2588f, 0.0000f, 0.0000f, -1.0000f, 0.9659f, 0.0000f, -0.2588f,   //
        1.0000f, 0.0000f, -0.0000f, 0.0000f, 0.0000f, -1.0000f, 1.0000f, 0.0000f, -0.0000f,   //
        0.9659f, 0.0000f, 0.2588f, 0.0000f, 0.0000f, -1.0000f, 0.9659f, 0.0000f, 0.2588f,     //
        0.8660f, 0.0000f, 0.5000f, 0.0000f, 0.0000f, -1.0000f, 0.8660f, 0.0000f, 0.5000f,     //
        0.7071f, 0.0000f, 0.7071f, 0.0000f, 0.0000f, -1.0000f, 0.7071f, 0.0000f, 0.7071f,     //
        0.5000f, 0.0000f, 0.8660f, 0.0000f, 0.0000f, -1.0000f, 0.5000f, 0.0000f, 0.8660f,     //
        0.2588f, 0.0000f, 0.9659f, 0.0000f, 0.0000f, -1.0000f, 0.2588f, 0.0000f, 0.9659f,     //
        0.0000f, 0.0000f, 1.0000f, 0.0000f, 0.0000f, -1.0000f, 0.0000f, 0.0000f, 1.0000f,     //
        -0.2588f, 0.0000f, 0.9659f, 0.0000f, 0.0000f, -1.0000f, -0.2588f, 0.0000f, 0.9659f,   //
        -0.5000f, 0.0000f, 0.8660f, 0.0000f, 0.0000f, -1.0000f, -0.5000f, 0.0000f, 0.8660f,   //
        -0.7071f, 0.0000f, 0.7071f, 0.0000f, 0.0000f, -1.0000f, -0.7071f, 0.0000f, 0.7071f,   //
        -0.8660f, 0.0000f, 0.5000f, 0.0000f, 0.0000f, -1.0000f, -0.8660f, 0.0000f, 0.5000f,   //
        -0.9659f, 0.0000f, 0.2588f, 0.0000f, 0.0000f, -1.0000f, -0.9659f, 0.0000f, 0.2588f,   //
        -1.0000f, 0.0000f, -0.0000f, 0.0000f, 0.0000f, -1.0000f, -1.0000f, 0.0000f, -0.0000f, //
        -0.9659f, 0.0000f, -0.2588f, 0.0000f, 0.0000f, -1.0000f, -0.9659f, 0.0000f, -0.2588f, //
        -0.8660f, 0.0000f, -0.5000f, 0.0000f, 0.0000f, -1.0000f, -0.8660f, 0.0000f, -0.5000f, //
        -0.7071f, 0.0000f, -0.7071f, 0.0000f, 0.0000f, -1.0000f, -0.7071f, 0.0000f, -0.7071f, //
        -0.5000f, 0.0000f, -0.8660f, 0.0000f, 0.0000f, -1.0000f, -0.5000f, 0.0000f, -0.8660f, //
        -0.2588f, 0.0000f, -0.9659f, -0.5000f, 0.0000f, -0.8660f, 0.0000f, 1.0000f, -0.0000f, //
        -0.2588f, 0.0000f, -0.9659f, -0.2588f, 0.0000f, -0.9659f, 0.0000f, 1.0000f, -0.0000f, //
        0.0000f, 0.0000f, -1.0000f,                                                           //
    };
    static uint16_t meshConusIndexesData[] = {
        0, 1, 2,       //
        3, 4, 5,       //
        6, 7, 8,       //
        9, 10, 11,     //
        12, 13, 14,    //
        15, 16, 17,    //
        17, 16, 18,    //
        19, 20, 21,    //
        22, 23, 24,    //
        25, 26, 27,    //
        28, 29, 30,    //
        31, 32, 33,    //
        34, 35, 36,    //
        37, 38, 39,    //
        40, 41, 42,    //
        43, 44, 45,    //
        46, 47, 48,    //
        49, 50, 51,    //
        52, 53, 54,    //
        55, 56, 57,    //
        58, 59, 60,    //
        61, 62, 63,    //
        64, 65, 66,    //
        67, 68, 69,    //
        70, 71, 72,    //
        73, 74, 75,    //
        76, 77, 78,    //
        79, 80, 81,    //
        82, 83, 84,    //
        85, 86, 87,    //
        88, 89, 90,    //
        91, 92, 93,    //
        94, 95, 96,    //
        97, 98, 99,    //
        100, 101, 102, //
        103, 104, 105, //
        106, 107, 108, //
        109, 110, 111, //
        112, 113, 114, //
        115, 116, 117, //
        118, 119, 120, //
        121, 122, 123, //
        124, 125, 126, //
        127, 128, 129, //
        130, 131, 132, //
        133, 134, 135, //
    };

    static float meshCylinderData[] = {
        0.0000f, -1.0000f, -1.0000f, 0.0000f, 1.0000f, -1.0000f, 0.1951f, 1.0000f, -0.9808f,     //
        0.1951f, -1.0000f, -0.9808f, 0.1951f, -1.0000f, -0.9808f, 0.1951f, 1.0000f, -0.9808f,    //
        0.3827f, 1.0000f, -0.9239f, 0.3827f, -1.0000f, -0.9239f, 0.3827f, -1.0000f, -0.9239f,    //
        0.3827f, 1.0000f, -0.9239f, 0.5556f, 1.0000f, -0.8315f, 0.5556f, -1.0000f, -0.8315f,     //
        0.5556f, -1.0000f, -0.8315f, 0.5556f, 1.0000f, -0.8315f, 0.7071f, 1.0000f, -0.7071f,     //
        0.7071f, -1.0000f, -0.7071f, 0.7071f, -1.0000f, -0.7071f, 0.7071f, 1.0000f, -0.7071f,    //
        0.8315f, 1.0000f, -0.5556f, 0.8315f, -1.0000f, -0.5556f, 0.8315f, -1.0000f, -0.5556f,    //
        0.8315f, 1.0000f, -0.5556f, 0.9239f, 1.0000f, -0.3827f, 0.9239f, -1.0000f, -0.3827f,     //
        0.9239f, -1.0000f, -0.3827f, 0.9239f, 1.0000f, -0.3827f, 0.9808f, 1.0000f, -0.1951f,     //
        0.9808f, -1.0000f, -0.1951f, 0.9808f, -1.0000f, -0.1951f, 0.9808f, 1.0000f, -0.1951f,    //
        1.0000f, 1.0000f, 0.0000f, 1.0000f, -1.0000f, 0.0000f, 1.0000f, -1.0000f, 0.0000f,       //
        1.0000f, 1.0000f, 0.0000f, 0.9808f, 1.0000f, 0.1951f, 0.9808f, -1.0000f, 0.1951f,        //
        0.9808f, -1.0000f, 0.1951f, 0.9808f, 1.0000f, 0.1951f, 0.9239f, 1.0000f, 0.3827f,        //
        0.9239f, -1.0000f, 0.3827f, 0.9239f, -1.0000f, 0.3827f, 0.9239f, 1.0000f, 0.3827f,       //
        0.8315f, 1.0000f, 0.5556f, 0.8315f, -1.0000f, 0.5556f, 0.8315f, -1.0000f, 0.5556f,       //
        0.8315f, 1.0000f, 0.5556f, 0.7071f, 1.0000f, 0.7071f, 0.7071f, -1.0000f, 0.7071f,        //
        0.7071f, -1.0000f, 0.7071f, 0.7071f, 1.0000f, 0.7071f, 0.5556f, 1.0000f, 0.8315f,        //
        0.5556f, -1.0000f, 0.8315f, 0.5556f, -1.0000f, 0.8315f, 0.5556f, 1.0000f, 0.8315f,       //
        0.3827f, 1.0000f, 0.9239f, 0.3827f, -1.0000f, 0.9239f, 0.3827f, -1.0000f, 0.9239f,       //
        0.3827f, 1.0000f, 0.9239f, 0.1951f, 1.0000f, 0.9808f, 0.1951f, -1.0000f, 0.9808f,        //
        0.1951f, -1.0000f, 0.9808f, 0.1951f, 1.0000f, 0.9808f, 0.0000f, 1.0000f, 1.0000f,        //
        0.0000f, -1.0000f, 1.0000f, 0.0000f, -1.0000f, 1.0000f, 0.0000f, 1.0000f, 1.0000f,       //
        -0.1951f, 1.0000f, 0.9808f, -0.1951f, -1.0000f, 0.9808f, -0.1951f, -1.0000f, 0.9808f,    //
        -0.1951f, 1.0000f, 0.9808f, -0.3827f, 1.0000f, 0.9239f, -0.3827f, -1.0000f, 0.9239f,     //
        -0.3827f, -1.0000f, 0.9239f, -0.3827f, 1.0000f, 0.9239f, -0.5556f, 1.0000f, 0.8315f,     //
        -0.5556f, -1.0000f, 0.8315f, -0.5556f, -1.0000f, 0.8315f, -0.5556f, 1.0000f, 0.8315f,    //
        -0.7071f, 1.0000f, 0.7071f, -0.7071f, -1.0000f, 0.7071f, -0.7071f, -1.0000f, 0.7071f,    //
        -0.7071f, 1.0000f, 0.7071f, -0.8315f, 1.0000f, 0.5556f, -0.8315f, -1.0000f, 0.5556f,     //
        -0.8315f, -1.0000f, 0.5556f, -0.8315f, 1.0000f, 0.5556f, -0.9239f, 1.0000f, 0.3827f,     //
        -0.9239f, -1.0000f, 0.3827f, -0.9239f, -1.0000f, 0.3827f, -0.9239f, 1.0000f, 0.3827f,    //
        -0.9808f, 1.0000f, 0.1951f, -0.9808f, -1.0000f, 0.1951f, -0.9808f, -1.0000f, 0.1951f,    //
        -0.9808f, 1.0000f, 0.1951f, -1.0000f, 1.0000f, 0.0000f, -1.0000f, -1.0000f, 0.0000f,     //
        -1.0000f, -1.0000f, 0.0000f, -1.0000f, 1.0000f, 0.0000f, -0.9808f, 1.0000f, -0.1951f,    //
        -0.9808f, -1.0000f, -0.1951f, -0.9808f, -1.0000f, -0.1951f, -0.9808f, 1.0000f, -0.1951f, //
        -0.9239f, 1.0000f, -0.3827f, -0.9239f, -1.0000f, -0.3827f, -0.9239f, -1.0000f, -0.3827f, //
        -0.9239f, 1.0000f, -0.3827f, -0.8315f, 1.0000f, -0.5556f, -0.8315f, -1.0000f, -0.5556f,  //
        -0.8315f, -1.0000f, -0.5556f, -0.8315f, 1.0000f, -0.5556f, -0.7071f, 1.0000f, -0.7071f,  //
        -0.7071f, -1.0000f, -0.7071f, -0.7071f, -1.0000f, -0.7071f, -0.7071f, 1.0000f, -0.7071f, //
        -0.5556f, 1.0000f, -0.8315f, -0.5556f, -1.0000f, -0.8315f, -0.5556f, -1.0000f, -0.8315f, //
        -0.5556f, 1.0000f, -0.8315f, -0.3827f, 1.0000f, -0.9239f, -0.3827f, -1.0000f, -0.9239f,  //
        -0.3827f, -1.0000f, -0.9239f, -0.3827f, 1.0000f, -0.9239f, -0.1951f, 1.0000f, -0.9808f,  //
        -0.1951f, -1.0000f, -0.9808f, -0.1951f, -1.0000f, -0.9808f, -0.1951f, 1.0000f, -0.9808f, //
        0.0000f, 1.0000f, -1.0000f, 0.0000f, -1.0000f, -1.0000f,                                 //
    };

    static uint16_t meshCylinderIndexesData[] = {
        0, 1, 2,       //
        0, 2, 3,       //
        4, 5, 6,       //
        4, 6, 7,       //
        8, 9, 10,      //
        8, 10, 11,     //
        12, 13, 14,    //
        12, 14, 15,    //
        16, 17, 18,    //
        16, 18, 19,    //
        20, 21, 22,    //
        20, 22, 23,    //
        24, 25, 26,    //
        24, 26, 27,    //
        28, 29, 30,    //
        28, 30, 31,    //
        32, 33, 34,    //
        32, 34, 35,    //
        36, 37, 38,    //
        36, 38, 39,    //
        40, 41, 42,    //
        40, 42, 43,    //
        44, 45, 46,    //
        44, 46, 47,    //
        48, 49, 50,    //
        48, 50, 51,    //
        52, 53, 54,    //
        52, 54, 55,    //
        56, 57, 58,    //
        56, 58, 59,    //
        60, 61, 62,    //
        60, 62, 63,    //
        64, 65, 66,    //
        64, 66, 67,    //
        68, 69, 70,    //
        68, 70, 71,    //
        72, 73, 74,    //
        72, 74, 75,    //
        76, 77, 78,    //
        76, 78, 79,    //
        80, 81, 82,    //
        80, 82, 83,    //
        84, 85, 86,    //
        84, 86, 87,    //
        88, 89, 90,    //
        88, 90, 91,    //
        92, 93, 94,    //
        92, 94, 95,    //
        96, 97, 98,    //
        96, 98, 99,    //
        100, 101, 102, //
        100, 102, 103, //
        104, 105, 106, //
        104, 106, 107, //
        108, 109, 110, //
        108, 110, 111, //
        112, 113, 114, //
        112, 114, 115, //
        116, 117, 118, //
        116, 118, 119, //
        120, 121, 122, //
        120, 122, 123, //
        124, 125, 126, //
        124, 126, 127, //
    };

    static mth::vec3 meshRectData[6] = {{0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 1.0f}};
    static mth::vec3 meshLineRectData[5] = {{0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}};
    static mth::vec3 meshLineData[2] = {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f}};
    static float meshLineCircleData[] = {
        0.0000f, 0.0000f, 1.0000f, 0.1951f, 0.0000f, 0.9808f, 0.3827f, 0.0000f, 0.9239f,       //
        0.5556f, 0.0000f, 0.8315f, 0.7071f, 0.0000f, 0.7071f, 0.8315f, 0.0000f, 0.5556f,       //
        0.9239f, 0.0000f, 0.3827f, 0.9808f, 0.0000f, 0.1951f, 1.0000f, 0.0000f, 0.0000f,       //
        0.9808f, 0.0000f, -0.1951f, 0.9239f, 0.0000f, -0.3827f, 0.8315f, 0.0000f, -0.5556f,    //
        0.7071f, 0.0000f, -0.7071f, 0.5556f, 0.0000f, -0.8315f, 0.3827f, 0.0000f, -0.9239f,    //
        0.1951f, 0.0000f, -0.9808f, 0.0000f, 0.0000f, -1.0000f, -0.1951f, 0.0000f, -0.9808f,   //
        -0.3827f, 0.0000f, -0.9239f, -0.5556f, 0.0000f, -0.8315f, -0.7071f, 0.0000f, -0.7071f, //
        -0.8315f, 0.0000f, -0.5556f, -0.9239f, 0.0000f, -0.3827f, -0.9808f, 0.0000f, -0.1951f, //
        -1.0000f, 0.0000f, -0.0000f, -0.9808f, 0.0000f, 0.1951f, -0.9239f, 0.0000f, 0.3827f,   //
        -0.8315f, 0.0000f, 0.5556f, -0.7071f, 0.0000f, 0.7071f, -0.5556f, 0.0000f, 0.8315f,    //
        -0.3827f, 0.0000f, 0.9239f, -0.1951f, 0.0000f, 0.9808f, -0.0000f, 0.0000f, 1.0000f     //
    };

    InPivotRendererPass::InPivotRendererPass()
    {
    }

    void InPivotRendererPass::release()
    {
    }

    void InPivotRendererPass::init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        vertexModul = _context->shaderProvider->getShaderModule("pivotVertex");
        fragmentModul = _context->shaderProvider->getShaderModule("pivotFragment");
        vk::ShaderModule modules[2] = {vertexModul, fragmentModul};
        vk::VulkanPipelineLayout::createPipelineLayout(_context->device, 2, modules, &layout);

        lineGraphicPipeline = _context->graphicPipelinesManager->createGraphicPipeline();
        opaqueGraphicPipeline = _context->graphicPipelinesManager->createGraphicPipeline();
        transparentGraphicPipeline = _context->graphicPipelinesManager->createGraphicPipeline();

        vk::VulkanVertexAttributeState vertexState = vk::VulkanVertexAttributeState();
        vertexState.setEnableAttributessInstance(true);
        vertexState.setSizeInputVertex(sizeof(VertexData));
        vertexState.addInputVertexAttribute(offsetof(VertexData, position), VK_FORMAT_R32G32B32_SFLOAT);
        vertexState.setSizeInputInstance(sizeof(InstanceData));
        vertexState.addInputInstanceAttributeMat4Float(offsetof(InstanceData, TRS));
        vertexState.addInputInstanceAttribute(offsetof(InstanceData, color), VK_FORMAT_R32G32B32A32_SFLOAT);

        vk::VulkanBlendColorState colorState = vk::VulkanBlendColorState();
        colorState.setCountColorAttachments(1);

        vk::VulkanGraphicPipeline *pipelines[3] = {lineGraphicPipeline, opaqueGraphicPipeline, transparentGraphicPipeline};
        for (uint i = 0; i < 3; i++)
        {
            pipelines[i]->setVertexModule(vertexModul);
            pipelines[i]->setFragmentModule(fragmentModul);
            pipelines[i]->setPipelineLayout(layout);
            pipelines[i]->setVertexAttributeState(&vertexState);
            pipelines[i]->setBlendColorState(&colorState);
            pipelines[i]->setEnableDynamicState(VK_DYNAMIC_STATE_VIEWPORT, true);
            pipelines[i]->setEnableDynamicState(VK_DYNAMIC_STATE_SCISSOR, true);
            pipelines[i]->setDepthWrite(true);
            pipelines[i]->setDepthTest(true);
        }

        lineGraphicPipeline->setTopology(VK_PRIMITIVE_TOPOLOGY_LINE_STRIP);
        lineGraphicPipeline->setLineWidth(2.0f);
        _context->graphicPipelinesManager->updateGraphicPipeline(lineGraphicPipeline, _context->renderPass);
        _context->graphicPipelinesManager->updateGraphicPipeline(opaqueGraphicPipeline, _context->renderPass);

        transparentGraphicPipeline->setDepthWrite(false);
        transparentGraphicPipeline->setDepthTest(true);
        transparentGraphicPipeline->setCullMode(VK_CULL_MODE_NONE);
        colorState.setColorBlendAttachmentState(0, VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT,
                                                true, VK_BLEND_OP_ADD, VK_BLEND_OP_ADD,
                                                VK_BLEND_FACTOR_SRC_ALPHA, VK_BLEND_FACTOR_SRC_ALPHA, VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA, VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA);
        _context->graphicPipelinesManager->updateGraphicPipeline(transparentGraphicPipeline, _context->renderPass);

        vertexParams = new vk::ShaderAutoParameters(_context->device, vertexModul);
        vertexParams->takeMemoryPools(_context->descriptorPoolManager, 1);
        vertexParams->beginUpdate();
        vertexParams->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, _context->cameraData->getCameraData());
        vertexParams->endUpdate();

        initBuffers(_context, _graphicQueue);

        instanceBuffers = new (phAllocateMemory(sizeof(InInstanceBuffers))) InInstanceBuffers(sizeof(InstanceData), 10,
                                                                                              _context->bufferAllocator, _context->mainFamilyIndex);
    }

    void InPivotRendererPass::render(InScene *_scene, vk::VulkanGraphicCommands *_graphicCommands, const RenderContextData *_context)
    {
        _graphicCommands->debugMarkerBegin("PivotPass", InDebugMarkersUtils::passCallColor());
        instanceBuffers->resetUsedBuffers();

        uint count = _scene->tablePivotRenderers->getPosBuffer();
        for (uint i = 0; i < count; i++)
        {
            InPivotRenderer *pivot = *_scene->tablePivotRenderers->getMemory(i);
            if (pivot != NULL)
            {
                drawPivot(_scene->camera, pivot, _graphicCommands, _context);
            }
        }
        _graphicCommands->debugMarkerEnd();
    }

    void InPivotRendererPass::cleanup(const CleanupContextData *_context)
    {
        _context->bufferAllocator->destroyBuffer(meshLineCircle);
        _context->bufferAllocator->destroyBuffer(meshLine);
        _context->bufferAllocator->destroyBuffer(meshLineRect);
        _context->bufferAllocator->destroyBuffer(meshRect);
        _context->bufferAllocator->destroyBuffer(meshCylinderIndexes);
        _context->bufferAllocator->destroyBuffer(meshCylinder);
        _context->bufferAllocator->destroyBuffer(meshConusIndexes);
        _context->bufferAllocator->destroyBuffer(meshConus);
        _context->bufferAllocator->destroyBuffer(meshCubeIndexes);
        _context->bufferAllocator->destroyBuffer(meshCube);

        PH_RELEASE(instanceBuffers);
        vertexParams->freeMemoryPools(_context->descriptorPoolManager);
        delete vertexParams;

        _context->graphicPipelinesManager->deleteGraphicPipeline(transparentGraphicPipeline);
        _context->graphicPipelinesManager->deleteGraphicPipeline(opaqueGraphicPipeline);
        _context->graphicPipelinesManager->deleteGraphicPipeline(lineGraphicPipeline);
        vk::VulkanPipelineLayout::destroyPipelineLayout(_context->device, &layout);
    }

    void InPivotRendererPass::initBuffers(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        meshCube = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                           sizeof(meshCubeData), 1, &_context->mainFamilyIndex, "PivotCube_vertexes");
        meshCubeIndexes = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_IndexBufferStatic,
                                                                  sizeof(meshCubeIndexesData), 1, &_context->mainFamilyIndex, "PivotCube_indexes");
        meshConus = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                            sizeof(meshConusData), 1, &_context->mainFamilyIndex, "PivotConus_vertexes");
        meshConusIndexes = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_IndexBufferStatic,
                                                                   sizeof(meshConusIndexesData), 1, &_context->mainFamilyIndex, "PivotConus_indexes");
        meshCylinder = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                               sizeof(meshCylinderData), 1, &_context->mainFamilyIndex, "PivotCylinder_vertexes");
        meshCylinderIndexes = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_IndexBufferStatic,
                                                                      sizeof(meshCylinderIndexesData), 1, &_context->mainFamilyIndex, "PivotCylinder_indexes");
        meshRect = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                           sizeof(meshRectData), 1, &_context->mainFamilyIndex, "PivotRect_vertexes");
        meshLineRect = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                               sizeof(meshLineRectData), 1, &_context->mainFamilyIndex, "PivotRect_indexes");
        meshLine = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                           sizeof(meshLineData), 1, &_context->mainFamilyIndex, "PivotLine_vertexes");
        meshLineCircle = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                                 sizeof(meshLineCircleData), 1, &_context->mainFamilyIndex, "PivotCircle_vertexes");

        {
            InStaticBufferLoader *staticBufferLoader = new (phAllocateMemory(sizeof(InStaticBufferLoader))) InStaticBufferLoader(
                _context->device, _context->bufferAllocator, _context->mainFamilyIndex);

            staticBufferLoader->beginSend();
            staticBufferLoader->addBuffer(meshCube, (char *)meshCubeData, sizeof(meshCubeData));
            staticBufferLoader->addBuffer(meshCubeIndexes, (char *)meshCubeIndexesData, sizeof(meshCubeIndexesData));
            staticBufferLoader->addBuffer(meshConus, (char *)meshConusData, sizeof(meshConusData));
            staticBufferLoader->addBuffer(meshConusIndexes, (char *)meshConusIndexesData, sizeof(meshConusIndexesData));
            staticBufferLoader->addBuffer(meshCylinder, (char *)meshCylinderData, sizeof(meshCylinderData));
            staticBufferLoader->addBuffer(meshCylinderIndexes, (char *)meshCylinderIndexesData, sizeof(meshCylinderIndexesData));
            staticBufferLoader->addBuffer(meshRect, (char *)meshRectData, sizeof(meshRectData));
            staticBufferLoader->addBuffer(meshLineRect, (char *)meshLineRectData, sizeof(meshLineRectData));
            staticBufferLoader->addBuffer(meshLine, (char *)meshLineData, sizeof(meshLineData));
            staticBufferLoader->addBuffer(meshLineCircle, (char *)meshLineCircleData, sizeof(meshLineCircleData));
            staticBufferLoader->endSend(_context->commandReceiver, _graphicQueue);

            PH_RELEASE(staticBufferLoader);
        }
    }

    void InPivotRendererPass::drawPivot(GeCamera *_camera, InPivotRenderer *_pivot, vk::VulkanGraphicCommands *_graphicCommands, const RenderContextData *_context)
    {
        PhFlags state = _pivot->getPivotState();
        if (state == InPivotRenderer::PivotState_None)
            return;

        mth::mat4 worldToLocal = _camera->getInverseTRS();
        mth::vec3 pivotLocalPos = worldToLocal.transform(_pivot->getPosition());
        float near = _camera->getNear();
        if (pivotLocalPos.z <= near)
            return;

        float scale = _pivot->getSizePivot(_camera);

        vk::VulkanBuffer *instanceBuffer = instanceBuffers->allocateBuffer();
        InstanceData *pWrite = (InstanceData *)_graphicCommands->beginWrite(instanceBuffer, 0, instanceBuffer->getSize());

        fillColorData(pWrite, _pivot);

        vk::VulkanRenderPass renderPass = _context->renderPass;
        vk::VulkanFramebuffer framebuffer = _context->framebuffer;

        if (state == InPivotRenderer::PivotState_Move || state == InPivotRenderer::PivotState_Scale)
        {
            if (state == InPivotRenderer::PivotState_Move)
                _graphicCommands->debugMarkerBegin("MovePivot", InDebugMarkersUtils::subPassColor());
            else
                _graphicCommands->debugMarkerBegin("ScalePivot", InDebugMarkersUtils::subPassColor());

            fillMatrixesMoveScaleData(pWrite, _pivot, scale);
            _graphicCommands->endWrite(instanceBuffer);

            _graphicCommands->beginRenderPass(renderPass, framebuffer);
            _graphicCommands->bindPipeline(lineGraphicPipeline->getPipeline(&renderPass));
            _graphicCommands->bindDescriptorSet(layout, vertexParams->getCurrentSet(0), 0);
            _graphicCommands->bindVertexBuffer(meshLine, 0);
            _graphicCommands->bindInstanceBuffer(instanceBuffer, 0);
            _graphicCommands->draw(2, 3, 0, 0);
            _graphicCommands->bindVertexBuffer(meshLineRect, 0);
            _graphicCommands->draw(5, 3, 0, 6);

            _graphicCommands->bindPipeline(opaqueGraphicPipeline->getPipeline(&renderPass));
            _graphicCommands->bindDescriptorSet(layout, vertexParams->getCurrentSet(0), 0);
            _graphicCommands->bindInstanceBuffer(instanceBuffer, 0);
            if (state == InPivotRenderer::PivotState_Move)
            {
                _graphicCommands->bindVertexBuffer(meshConus, 0);
                _graphicCommands->bindIndexBuffer(meshConusIndexes, 0, VK_INDEX_TYPE_UINT16);
                _graphicCommands->drawIndexed(sizeof(meshConusIndexesData) / sizeof(uint16_t), 3, 0, 0, 3); // draw conuses

                _graphicCommands->bindVertexBuffer(meshCube, 0);
                _graphicCommands->bindIndexBuffer(meshCubeIndexes, 0, VK_INDEX_TYPE_UINT16);
                _graphicCommands->drawIndexed(sizeof(meshCubeIndexesData) / sizeof(uint16_t), 1, 0, 0, 9); // draw center cube
            }
            else
            {
                _graphicCommands->bindVertexBuffer(meshCube, 0);
                _graphicCommands->bindIndexBuffer(meshCubeIndexes, 0, VK_INDEX_TYPE_UINT16);
                _graphicCommands->drawIndexed(sizeof(meshCubeIndexesData) / sizeof(uint16_t), 3, 0, 0, 3); // draw edge cubes
                _graphicCommands->drawIndexed(sizeof(meshCubeIndexesData) / sizeof(uint16_t), 1, 0, 0, 9); // draw center cube
            }

            _graphicCommands->bindPipeline(transparentGraphicPipeline->getPipeline(&renderPass));
            _graphicCommands->bindDescriptorSet(layout, vertexParams->getCurrentSet(0), 0);
            _graphicCommands->bindVertexBuffer(meshRect, 0);
            _graphicCommands->bindInstanceBuffer(instanceBuffer, 0);
            _graphicCommands->draw(6, 3, 0, 6);

            _graphicCommands->endRenderPass();

            _graphicCommands->debugMarkerEnd();
        }
        else if (state == InPivotRenderer::PivotState_Rotate)
        {
            fillMatrixesRotateData(pWrite, _camera, _pivot, scale);
            _graphicCommands->endWrite(instanceBuffer);

            _graphicCommands->debugMarkerBegin("RotatePivot", InDebugMarkersUtils::subPassColor());

            _graphicCommands->beginRenderPass(renderPass, framebuffer);
            _graphicCommands->bindPipeline(lineGraphicPipeline->getPipeline(&renderPass));
            _graphicCommands->bindDescriptorSet(layout, vertexParams->getCurrentSet(0), 0);
            _graphicCommands->bindVertexBuffer(meshLine, 0);
            _graphicCommands->bindInstanceBuffer(instanceBuffer, 0);
            _graphicCommands->draw(2, 3, 0, 0); // lines

            _graphicCommands->bindPipeline(opaqueGraphicPipeline->getPipeline(&renderPass));
            _graphicCommands->bindDescriptorSet(layout, vertexParams->getCurrentSet(0), 0);
            _graphicCommands->bindInstanceBuffer(instanceBuffer, 0);
            _graphicCommands->bindVertexBuffer(meshCylinder, 0);
            _graphicCommands->bindIndexBuffer(meshCylinderIndexes, 0, VK_INDEX_TYPE_UINT16);
            _graphicCommands->drawIndexed(sizeof(meshCylinderIndexesData) / sizeof(uint16_t), 3, 0, 0, 3); // draw cylinders

            _graphicCommands->bindPipeline(lineGraphicPipeline->getPipeline(&renderPass));
            _graphicCommands->bindDescriptorSet(layout, vertexParams->getCurrentSet(0), 0);
            _graphicCommands->bindInstanceBuffer(instanceBuffer, 0);
            _graphicCommands->bindVertexBuffer(meshLineCircle, 0);
            _graphicCommands->draw(sizeof(meshLineCircleData) / (sizeof(float) * 3), 2, 0, 6); // draw circle line

            _graphicCommands->endRenderPass();

            _graphicCommands->debugMarkerEnd();
        }
    }

    void InPivotRendererPass::fillColorData(InstanceData *_pWrite, InPivotRenderer *_pivot)
    {
        float selectedValue = 0.80f;
        float noselectedValue = 0.70f;
        mth::col whiteSelected = mth::col(selectedValue, selectedValue, selectedValue);
        mth::col whiteNoselected = mth::col(noselectedValue, noselectedValue, noselectedValue);

        mth::col redSelected = whiteSelected;
        mth::col redNoselected = mth::col(229.0f / 255.0f, 68.0f / 255.0f, 68.0f / 255.0f);
        mth::col greenSelected = whiteSelected;
        mth::col greenNoselected = mth::col(45.0f / 255.0f, 229.0f / 255.0f, 121.0f / 255.0f);
        mth::col blueSelected = whiteSelected;
        mth::col blueNoselected = mth::col(45.0f / 255.0f, 137.0f / 255.0f, 229.0f / 255.0f);

        PhFlags stateSelected = _pivot->getActiveAxises();
        if ((stateSelected & InPivotRenderer::PivotAxis_Z) != 0)
        {
            _pWrite[0].color = blueSelected;
            _pWrite[3].color = blueSelected;
        }
        else
        {
            _pWrite[0].color = blueNoselected;
            _pWrite[3].color = blueNoselected;
        }

        if ((stateSelected & InPivotRenderer::PivotAxis_Y) != 0)
        {
            _pWrite[1].color = greenSelected;
            _pWrite[4].color = greenSelected;
        }
        else
        {
            _pWrite[1].color = greenNoselected;
            _pWrite[4].color = greenNoselected;
        }

        if ((stateSelected & InPivotRenderer::PivotAxis_X) != 0)
        {
            _pWrite[2].color = redSelected;
            _pWrite[5].color = redSelected;
        }
        else
        {
            _pWrite[2].color = redNoselected;
            _pWrite[5].color = redNoselected;
        }

        if ((stateSelected) ==
            (InPivotRenderer::PivotAxis_X | InPivotRenderer::PivotAxis_Y))
        {
            _pWrite[6].color = blueSelected;
        }
        else
        {
            _pWrite[6].color = blueNoselected;
        }

        if ((stateSelected) ==
            (InPivotRenderer::PivotAxis_Z | InPivotRenderer::PivotAxis_Y))
        {
            _pWrite[8].color = redSelected;
        }
        else
        {
            _pWrite[8].color = redNoselected;
        }

        if ((stateSelected) ==
            (InPivotRenderer::PivotAxis_X | InPivotRenderer::PivotAxis_Z))
        {
            _pWrite[7].color = greenSelected;
        }
        else
        {
            _pWrite[7].color = greenNoselected;
        }
        float alphaTransparent = 0.5f;

        _pWrite[6].color.a = alphaTransparent;
        _pWrite[7].color.a = alphaTransparent;
        _pWrite[8].color.a = alphaTransparent;

        if ((stateSelected) ==
            (InPivotRenderer::PivotAxis_X | InPivotRenderer::PivotAxis_Y | InPivotRenderer::PivotAxis_Z))
        {
            _pWrite[9].color = whiteSelected;
        }
        else
        {
            _pWrite[9].color = whiteNoselected;
        }

        if (_pivot->getPivotState() == GePivotRenderer::PivotState_Rotate)
        {
            mth::col defaultInsideColor = mth::col(0.2f, 0.2f, 0.2f);
            PhFlags frontAxis = _pivot->getFrontRotateAxis();
            _pWrite[6].color = defaultInsideColor;

            if (frontAxis == GePivotRenderer::PivotAxis_Z)
                _pWrite[6].color = _pWrite[0].color;
            else if (frontAxis == GePivotRenderer::PivotAxis_Y)
                _pWrite[6].color = _pWrite[1].color;
            else if (frontAxis == GePivotRenderer::PivotAxis_X)
                _pWrite[6].color = _pWrite[2].color;

            _pWrite[7].color = mth::col(0.45f, 0.45f, 0.45f); // color outside circle
        }
    }

    void InPivotRendererPass::fillMatrixesMoveScaleData(InstanceData *_pWrite, InPivotRenderer *_pivot, float _scale)
    {
        PhFlags state = _pivot->getPivotState();
        GePivotRenderer::MoveScaleAxisData moveData = _pivot->getMoveScaleAxisData();

        mth::mat4 pivotTRS = _pivot->getTRS();
        mth::mat4 scaleM = mth::scale(_scale, _scale, _scale);
        mth::mat4 directionY = mth::rotate(-90.0f, 0.0f, 0.0f);
        mth::mat4 directionX = mth::rotate(0.0f, 90.0f, 0.0f);

        mth::mat4 pivotM = pivotTRS * scaleM;
        mth::mat4 pivotMY = pivotTRS * scaleM * directionY;
        mth::mat4 pivotMX = pivotTRS * scaleM * directionX;

        mth::mat4 scaleLine;
        if (state == GePivotRenderer::PivotState_Move)
        {
            float distToConus = 1.0f - moveData.heightConus;
            scaleLine = mth::scale(0.0f, 0.0f, distToConus);
            _pWrite[0].TRS = pivotM * scaleLine;
            _pWrite[1].TRS = pivotMY * scaleLine;
            _pWrite[2].TRS = pivotMX * scaleLine;
        }
        else
        {
            mth::vec3 offset = _pivot->getScaleOffsets();
            scaleLine = mth::scale(0.0f, 0.0f, offset.z - moveData.sizeEdgeCube);
            _pWrite[0].TRS = pivotM * scaleLine;
            scaleLine = mth::scale(0.0f, 0.0f, offset.y - moveData.sizeEdgeCube);
            _pWrite[1].TRS = pivotMY * scaleLine;
            scaleLine = mth::scale(0.0f, 0.0f, offset.x - moveData.sizeEdgeCube);
            _pWrite[2].TRS = pivotMX * scaleLine;
        }

        // conuses
        if (state == GePivotRenderer::PivotState_Move)
        {
            float radiusConus = moveData.radiusConus;
            float heightConus = moveData.heightConus;
            mth::mat4 scaleAndOffsetConus = mth::rotate(90.0f, 0.0f, 0.0f) *
                                            mth::translate(0.0f, 1.0f - heightConus, 0.0f) *
                                            mth::scale(radiusConus, heightConus, radiusConus);

            _pWrite[3].TRS = pivotM * scaleAndOffsetConus;
            _pWrite[4].TRS = pivotMY * scaleAndOffsetConus;
            _pWrite[5].TRS = pivotMX * scaleAndOffsetConus;
        }
        else
        {
            float sizeCube = moveData.sizeEdgeCube;
            mth::vec3 offset = _pivot->getScaleOffsets();
            mth::mat4 scaleCube = mth::scale(sizeCube, sizeCube, sizeCube);

            _pWrite[3].TRS = pivotM * mth::translate(0.0f, 0.0f, offset.z) * scaleCube;
            _pWrite[4].TRS = pivotMY * mth::translate(0.0f, 0.0f, offset.y) * scaleCube;
            _pWrite[5].TRS = pivotMX * mth::translate(0.0f, 0.0f, offset.x) * scaleCube;
        }

        // plane rects
        float rectSize = moveData.sizePlaneRect;
        float rectOffset = moveData.offsetPlaneRect;
        mth::mat4 offsetAndScale = mth::translate(0.0f, rectOffset, rectOffset) * mth::scale(0.0f, rectSize, rectSize);
        _pWrite[6].TRS = pivotMX * offsetAndScale;

        _pWrite[7].TRS = pivotM * mth::rotate(0.0f, 0.0f, -90.0f) * offsetAndScale;

        _pWrite[8].TRS = pivotM * offsetAndScale;

        // white cube
        float sizeCube = moveData.sizeCenterCube;
        _pWrite[9].TRS = pivotM * mth::scale(sizeCube, sizeCube, sizeCube);
    }

    void InPivotRendererPass::fillMatrixesRotateData(InstanceData *_pWrite, GeCamera *_camera, InPivotRenderer *_pivot, float _scale)
    {
        GePivotRenderer::RotateAxisData rotateData = _pivot->getRotateAxisData();

        mth::mat4 pivotTRS = _pivot->getTRS();
        mth::vec3 pivotPos = _pivot->getPosition();
        mth::mat4 scaleM = mth::scale(_scale, _scale, _scale);

        mth::mat4 directionY = mth::rotate(-90.0f, 0.0f, 0.0f);
        mth::mat4 directionX = mth::rotate(0.0f, 90.0f, 0.0f);

        mth::mat4 pivotM = pivotTRS * scaleM;
        mth::mat4 pivotMY = pivotTRS * scaleM * directionY;
        mth::mat4 pivotMX = pivotTRS * scaleM * directionX;

        mth::mat4 scaleLine = mth::scale(0.0f, 0.0f, rotateData.radiusMainAxis);
        mth::mat4 scaleCylinders = mth::scale(rotateData.radiusMainAxis, rotateData.cylindersHeight, rotateData.radiusMainAxis);
        _pWrite[0].TRS = pivotM * scaleLine;
        _pWrite[1].TRS = pivotMY * scaleLine;
        _pWrite[2].TRS = pivotMX * scaleLine;

        PhFlags frontAxis = _pivot->getFrontRotateAxis();

        _pWrite[3].TRS = pivotMY * scaleCylinders;
        _pWrite[4].TRS = pivotM * scaleCylinders;
        _pWrite[5].TRS = pivotM * mth::rotate(0.0f, 0.0f, 90.0f) * scaleCylinders;

        if (frontAxis == GePivotRenderer::PivotAxis_Z)
            _pWrite[3].TRS = {};
        if (frontAxis == GePivotRenderer::PivotAxis_Y)
            _pWrite[4].TRS = {};
        if (frontAxis == GePivotRenderer::PivotAxis_X)
            _pWrite[5].TRS = {};

        mth::vec3 dirToPivot = pivotPos - _camera->getPosition();
        dirToPivot.normalise();
        mth::vec3 up;
        if (abs(dirToPivot * mth::vec3(0.0f, 1.0f, 0.0f)) < 0.1f)
            up = mth::vec3(1.0f, 0.0f, 0.0f);
        else
            up = mth::vec3(0.0f, 1.0f, 0.0f);

        mth::vec3 right = (dirToPivot % up);
        right.normalise();
        up = right % dirToPivot;
        up.normalise();

        mth::mat3 rot3x3 = mth::mat3();
        rot3x3.setOrientation(right, dirToPivot, up);

        mth::mat4 circleM = mth::translate(pivotPos.x, pivotPos.y, pivotPos.z) *
                            mth::mat4(rot3x3) *
                            scaleM;

        _pWrite[6].TRS = circleM * mth::scale(rotateData.radiusFrontAxis, 0.0f, rotateData.radiusFrontAxis);
        _pWrite[7].TRS = circleM * mth::scale(rotateData.radiusOutsideCircle, 0.0f, rotateData.radiusOutsideCircle);
    }
}