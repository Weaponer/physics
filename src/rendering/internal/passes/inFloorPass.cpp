#include "rendering/internal/passes/inFloorPass.h"

#include "common/phMacros.h"

namespace phys::ge
{
    InFloorPass::InFloorPass() : floorRender(NULL)
    {
    }

    void InFloorPass::release()
    {
    }

    void InFloorPass::init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        floorRender = new (phAllocateMemory(sizeof(FloorRenderTemplate))) FloorRenderTemplate();
        RenderTemplate::InitRenderTemplate initTemplate{};
        initTemplate.renderPass = _context->renderPass;
        initTemplate.vertexShader = "fullScreenVertex";
        initTemplate.fragmentShader = "floorFragment";

        vk::VulkanBlendColorState colorState = vk::VulkanBlendColorState();
        colorState.setCountColorAttachments(1);

        initTemplate.blendColorState = &colorState;
        floorRender->init(_context, &initTemplate, _graphicQueue);
    }

    void InFloorPass::render(InScene *_scene, vk::VulkanGraphicCommands *_graphicCommands, const RenderContextData *_context)
    {
        if (!_scene->floorRenderer)
            return;

        _graphicCommands->debugMarkerBegin("FloorPass", InDebugMarkersUtils::passCallColor());

        RenderTemplate::BeginRenderTemplate beginRender{};
        beginRender.renderPass = _context->renderPass;
        beginRender.framebuffer = _context->framebuffer;
        floorRender->beginRender(_scene, _context, &beginRender, _graphicCommands);
        floorRender->draw(_graphicCommands);
        floorRender->endRender(_graphicCommands);

        _graphicCommands->debugMarkerEnd();
    }

    void InFloorPass::cleanup(const CleanupContextData *_context)
    {
        floorRender->cleanup(_context);
        PH_RELEASE(floorRender);
    }
}