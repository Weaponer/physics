#include "rendering/internal/passes/inSelectedEntityPass.h"

#include "common/phMacros.h"

namespace phys::ge
{

    InSelectedEntityPass::InstancingSelectedEntityPass::InstancingSelectedEntityPass() : InInstancingGenerator()
    {
    }

    void InSelectedEntityPass::InstancingSelectedEntityPass::release()
    {
        InInstancingGenerator::release();
    }

    uint32_t InSelectedEntityPass::InstancingSelectedEntityPass::getSizeInstanceData()
    {
        return sizeof(InstancingSelectedEntityPass::Data);
    }

    bool InSelectedEntityPass::InstancingSelectedEntityPass::pushInstanceData(GeRenderer *_renderer, void *_pData)
    {
        GeMeshRenderer *renderer = dynamic_cast<GeMeshRenderer *>(_renderer);
        if (!renderer->getSelected())
            return false;

        InstancingSelectedEntityPass::Data data;
        data.TRS = renderer->getTRS();
        mth::col c = renderer->getColorHighlightLine();
        data.col = mth::vec3(c.r, c.g, c.b);

        *((InstancingSelectedEntityPass::Data *)_pData) = data;
        return true;
    }

    InMesh *InSelectedEntityPass::InstancingSelectedEntityPass::getMesh(GeRenderer *_renderer)
    {
        return (dynamic_cast<InMeshRenderer *>(_renderer))->mesh;
    }

    InSelectedEntityPass::InSelectedEntityPass() : uboData({}),
                                                   instancingSelectedEntityPass(NULL),
                                                   renderPass(),
                                                   floorRenderTemplate(NULL),
                                                   meshRenderTemplate(NULL),
                                                   ubo(NULL),
                                                   sampler(NULL),
                                                   vertexModul(),
                                                   fragmentModul(),
                                                   layout(),
                                                   blurPipeline(NULL),
                                                   blurFinalPipeline(NULL),
                                                   fragmentParameters(NULL)
    {
    }

    void InSelectedEntityPass::release()
    {
    }

    void InSelectedEntityPass::init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        instancingSelectedEntityPass = new (phAllocateMemory(sizeof(InstancingSelectedEntityPass))) InstancingSelectedEntityPass();
        instancingSelectedEntityPass->initBuffers(_context->bufferAllocator, _context->mainFamilyIndex);

        initTargets(_context, _graphicQueue);
        initTemplates(_context, _graphicQueue);
        initBlurPass(_context, _graphicQueue);
    }

    void InSelectedEntityPass::render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context)
    {
        graphicCommands->debugMarkerBegin("SelectedEntityPass", InDebugMarkersUtils::passCallColor());

        graphicCommands->debugMarkerBegin("ClearTargets", InDebugMarkersUtils::subPassColor());
        {
            clearImage(0, graphicCommands);
            clearImage(1, graphicCommands);
        }
        graphicCommands->debugMarkerEnd();

        graphicCommands->debugMarkerBegin("DrawPass", InDebugMarkersUtils::passCallColor());
        bool sceneDrawn = drawScene(_scene, graphicCommands, _context);
        graphicCommands->debugMarkerEnd();

        if (sceneDrawn)
        {
            graphicCommands->debugMarkerBegin("GenerateBlur", InDebugMarkersUtils::subPassColor());
            generateBlur(graphicCommands, _context);
            graphicCommands->debugMarkerEnd();

            graphicCommands->debugMarkerBegin("FinalCombine", InDebugMarkersUtils::subPassColor());
            finalCombine(graphicCommands, _context);
            graphicCommands->debugMarkerEnd();
        }
        graphicCommands->debugMarkerEnd();
    }

    void InSelectedEntityPass::cleanup(const CleanupContextData *_context)
    {
        fragmentParameters->freeMemoryPools(_context->descriptorPoolManager);
        delete fragmentParameters;

        _context->graphicPipelinesManager->deleteGraphicPipeline(blurPipeline);
        _context->graphicPipelinesManager->deleteGraphicPipeline(blurFinalPipeline);
        vk::VulkanPipelineLayout::destroyPipelineLayout(_context->device, &layout);

        delete sampler;
        _context->bufferAllocator->destroyBuffer(ubo);

        meshRenderTemplate->cleanup(_context);
        PH_RELEASE(meshRenderTemplate);

        floorRenderTemplate->cleanup(_context);
        PH_RELEASE(floorRenderTemplate);

        for (int i = 0; i < 2; i++)
        {
            vk::VulkanFramebuffer::destroyFramebuffer(_context->device, &blurFramebuffer[i]);
            vk::VulkanImageView::destroyImageView(_context->device, blurTargetViews[i]);
            _context->imageAllocator->destroyImage(blurTargets[i]);
        }
        PH_RELEASE(instancingSelectedEntityPass);
    }

    void InSelectedEntityPass::initTargets(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        for (int i = 0; i < 2; i++)
        {
            blurTargets[i] = _context->imageAllocator->createImage(vk::MemoryUseType_Long, vk::VulkanImageType_ImageStatic,
                                                                   VK_IMAGE_USAGE_TRANSFER_DST_BIT |
                                                                       VK_IMAGE_USAGE_TRANSFER_SRC_BIT |
                                                                       VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
                                                                       VK_IMAGE_USAGE_SAMPLED_BIT,
                                                                   VK_IMAGE_TYPE_2D, VK_FORMAT_B8G8R8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT,
                                                                   _context->swapchain->getWidth(), _context->swapchain->getHeight(),
                                                                   1, 1, 1, 1, &_context->mainFamilyIndex, "BlurTarget");

            vk::VulkanImageView::createImageView(_context->device, VK_IMAGE_VIEW_TYPE_2D,
                                                 blurTargets[i], &(blurTargetViews[i]), "BlurTargetView");
        }

        _context->renderPassesManager->beginCreateRenderPass();
        _context->renderPassesManager->addColorAttachments(blurTargets[0]);
        _context->renderPassesManager->setDepthStencil(_context->depthBuffer);
        _context->renderPassesManager->endCreate(&renderPass);

        for (int i = 0; i < 2; i++)
        {
            vk::VulkanFramebuffer::createFramebuffer(_context->device, renderPass, 1, &blurTargets[i], &blurTargetViews[i],
                                                     _context->depthBuffer, _context->depthBufferView, &blurFramebuffer[i]);
        }
    }

    void InSelectedEntityPass::initTemplates(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        floorRenderTemplate = new (phAllocateMemory(sizeof(FloorRenderTemplate))) FloorRenderTemplate();
        RenderTemplate::InitRenderTemplate initTemplate{};
        initTemplate.renderPass = renderPass;
        initTemplate.vertexShader = "fullScreenVertex";
        initTemplate.fragmentShader = "floorSelected";

        vk::VulkanBlendColorState colorState = vk::VulkanBlendColorState();
        colorState.setCountColorAttachments(1);

        initTemplate.blendColorState = &colorState;
        floorRenderTemplate->init(_context, &initTemplate, _graphicQueue);

        vk::VulkanVertexAttributeState attributeState = vk::VulkanVertexAttributeState();
        attributeState.setEnableAttributessInstance(true);
        attributeState.setSizeInputVertex(sizeof(PhObjectMesh::Vertex));
        attributeState.addInputVertexAttribute(offsetof(PhObjectMesh::Vertex, pos), VK_FORMAT_R32G32B32_SFLOAT);
        attributeState.setSizeInputInstance(sizeof(InstancingSelectedEntityPass::Data));
        attributeState.addInputInstanceAttributeMat4Float(offsetof(InstancingSelectedEntityPass::Data, TRS));
        attributeState.addInputInstanceAttribute(offsetof(InstancingSelectedEntityPass::Data, col), VK_FORMAT_R32G32B32_SFLOAT);

        MeshRenderTemplate::MeshTemplateInitData meshRenderInit{};

        meshRenderInit.isWireframe = false;
        initTemplate.customData = &meshRenderInit;
        initTemplate.vertexShader = "meshSelectedVertex";
        initTemplate.fragmentShader = "meshSelectedFragment";
        initTemplate.vertexAttributeState = &attributeState;
        initTemplate.blendColorState = &colorState;

        meshRenderTemplate = new (phAllocateMemory(sizeof(MeshRenderTemplate))) MeshRenderTemplate();
        meshRenderTemplate->init(_context, &initTemplate, _graphicQueue);
    }

    void InSelectedEntityPass::initBlurPass(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        ubo = _context->bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_UniformBlockDynamic,
                                                      sizeof(uboData), 1, &_context->mainFamilyIndex);
        sampler = new vk::VulkanSampler(_context->device);
        sampler->update();

        uboData.width = _context->swapchain->getWidth();
        uboData.height = _context->swapchain->getHeight();
        uboData.iterations = 5;
        {
            vk::VulkanTransferCommands transferCommands = vk::VulkanTransferCommands(_graphicQueue, false);
            transferCommands.writeToBuffer(ubo, 0, sizeof(uboData), &uboData);
        }

        vertexModul = _context->shaderProvider->getShaderModule("fullScreenVertex");
        fragmentModul = _context->shaderProvider->getShaderModule("blurPass");
        vk::ShaderModule modules[2] = {vertexModul, fragmentModul};
        vk::VulkanPipelineLayout::createPipelineLayout(_context->device, 2, modules, &layout);

        vk::VulkanBlendColorState colorState = vk::VulkanBlendColorState();
        colorState.setCountColorAttachments(1);

        vk::VulkanGraphicPipeline *pipelines[2];
        for (int i = 0; i < 2; i++)
        {
            pipelines[i] = _context->graphicPipelinesManager->createGraphicPipeline();
            pipelines[i]->setVertexModule(vertexModul);
            pipelines[i]->setFragmentModule(fragmentModul);
            pipelines[i]->setPipelineLayout(layout);
            pipelines[i]->setVertexAttributeState(_context->fullScreenPassData->getVertexAttributeState());
            VkRect2D scissor{};
            scissor.extent.width = blurFramebuffer[i].getWidth();
            scissor.extent.height = blurFramebuffer[i].getHeight();
            pipelines[i]->setScissor(scissor);
            VkViewport viewport{};
            viewport.minDepth = 0.0f;
            viewport.maxDepth = 1.0f;
            viewport.width = scissor.extent.width;
            viewport.height = scissor.extent.height;
            pipelines[i]->setViewport(viewport);
            pipelines[i]->setDepthWrite(false);
            pipelines[i]->setDepthTest(false);
        }

        blurPipeline = pipelines[0];
        blurPipeline->setBlendColorState(&colorState);
        _context->graphicPipelinesManager->updateGraphicPipeline(blurPipeline, renderPass);
        blurFinalPipeline = pipelines[1];
        colorState.setColorBlendAttachmentState(0, VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT,
                                                true, VK_BLEND_OP_ADD, VK_BLEND_OP_ADD,
                                                VK_BLEND_FACTOR_SRC_ALPHA, VK_BLEND_FACTOR_SRC_ALPHA, VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA, VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA);

        blurFinalPipeline->setBlendColorState(&colorState);
        _context->graphicPipelinesManager->updateGraphicPipeline(blurFinalPipeline, renderPass);

        fragmentParameters = new vk::ShaderAutoParameters(_context->device, fragmentModul);
        fragmentParameters->takeMemoryPools(_context->descriptorPoolManager, 2);

        for (int i = 0; i < 2; i++)
        {
            fragmentParameters->beginUpdate();
            fragmentParameters->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, ubo);
            fragmentParameters->setParameter<vk::ShaderAutoParameters::SampledImage>(0, 0, blurTargetViews[i], sampler);
            fragmentParameters->endUpdate();

            fragmentSets[i] = fragmentParameters->getCurrentSet(0);

            if (i == 0)
                fragmentParameters->switchToNextSet(0);
        }
    }

    bool InSelectedEntityPass::drawScene(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context)
    {
        bool wasRendering = false;
        RenderTemplate::BeginRenderTemplate beginRender{};
        beginRender.renderPass = renderPass;
        beginRender.framebuffer = blurFramebuffer[0];

        if (_scene->floorRenderer != NULL && _scene->floorRenderer->getSelected())
        {
            graphicCommands->debugMarkerBegin("Floor", InDebugMarkersUtils::subPassColor());

            wasRendering = true;
            floorRenderTemplate->beginRender(_scene, _context, &beginRender, graphicCommands);
            mth::col col = _scene->floorRenderer->getColorHighlightLine();
            graphicCommands->pushConstant(floorRenderTemplate->getLayout(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(mth::col), &col);
            floorRenderTemplate->draw(graphicCommands);
            floorRenderTemplate->endRender(graphicCommands);

            graphicCommands->debugMarkerEnd();
        }

        _context->meshRendererCullingManager->updateInstancingGenerator(instancingSelectedEntityPass, graphicCommands);
        uint32_t count = instancingSelectedEntityPass->getCountDrawCalls();
        if (count)
        {
            wasRendering = true;
            const InInstancingGenerator::DrawCall *drawCalls = instancingSelectedEntityPass->getDrawCalls();
            RenderTemplate::DrawRenderTemplate drawData{};

            meshRenderTemplate->beginRender(_scene, _context, &beginRender, graphicCommands);
            for (uint32_t i = 0; i < count; i++)
            {
                graphicCommands->debugMarkerBegin("Mesh", InDebugMarkersUtils::subPassColor());
                InInstancingGenerator::DrawCall drawCall = drawCalls[i];

                drawData.vertexBuffer = drawCall.mesh->getVertexBuffer();
                drawData.countVertex = 0;
                drawData.indexBuffer = drawCall.mesh->getIndexBuffer();
                drawData.indexType = drawCall.mesh->getIndexType();
                drawData.countIndex = drawCall.mesh->getCountIndex();
                drawData.instanceBuffer = drawCall.buffer;
                drawData.countInstance = drawCall.countInstance;
                drawData.offsetInstanceBuffer = drawCall.offset;

                meshRenderTemplate->drawIndexed(&drawData, graphicCommands);
                graphicCommands->debugMarkerEnd();
            }
            meshRenderTemplate->endRender(graphicCommands);
        }

        return wasRendering;
    }

    void InSelectedEntityPass::generateBlur(vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context)
    {
        prepareImageToSampled(0, graphicCommands);
        int currentRead = 0;
        VkPipeline pipeline = blurPipeline->getPipeline(&renderPass);
        vk::VulkanBuffer *vertBuffer = _context->fullScreenPassData->getVertexBuffer();
        vk::VulkanBuffer *indexBuffer = _context->fullScreenPassData->getIndexBuffer();
        VkDescriptorSet vertSet = _context->fullScreenPassData->getVertexDescriptorSet();
        PushConstants constants{};
        for (int i = 0; i < uboData.iterations; i++)
        {
            graphicCommands->beginRenderPass(renderPass, blurFramebuffer[1 - currentRead]);
            graphicCommands->bindPipeline(pipeline);
            graphicCommands->bindDescriptorSet(layout, vertSet, 0);
            graphicCommands->bindDescriptorSet(layout, fragmentSets[currentRead], 1);

            constants.iteration = i;
            graphicCommands->pushConstant(layout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(constants), &constants);

            graphicCommands->bindVertexBuffer(vertBuffer, 0);
            graphicCommands->bindIndexBuffer(indexBuffer, 0, VkIndexType::VK_INDEX_TYPE_UINT16);
            graphicCommands->drawIndexed(6, 1, 0, 0, 0);
            graphicCommands->endRenderPass();

            if (currentRead == 0)
                currentRead = 1;
            else
                currentRead = 0;

            prepareImageToSampled(currentRead, graphicCommands);
            prepareImageToDraw(1 - currentRead, graphicCommands);
        }
    }

    void InSelectedEntityPass::finalCombine(vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context)
    {
        int indexRead = 0;
        if ((uboData.iterations & 1) != 0)
        {
            indexRead = 1;
        }

        vk::VulkanBuffer *vertBuffer = _context->fullScreenPassData->getVertexBuffer();
        vk::VulkanBuffer *indexBuffer = _context->fullScreenPassData->getIndexBuffer();

        vk::VulkanRenderPass cRenderPass = _context->renderPass;
        vk::VulkanFramebuffer cFramebuffer = _context->framebuffer;
        PushConstants constants{};
        constants.iteration = 0;
        constants.finalPass = true;
        graphicCommands->beginRenderPass(cRenderPass, cFramebuffer);
        graphicCommands->bindPipeline(blurFinalPipeline->getPipeline(&cRenderPass));
        graphicCommands->bindDescriptorSet(layout, _context->fullScreenPassData->getVertexDescriptorSet(), 0);
        graphicCommands->bindDescriptorSet(layout, fragmentSets[indexRead], 1);

        graphicCommands->pushConstant(layout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(constants), &constants);

        graphicCommands->bindVertexBuffer(vertBuffer, 0);
        graphicCommands->bindIndexBuffer(indexBuffer, 0, VkIndexType::VK_INDEX_TYPE_UINT16);
        graphicCommands->drawIndexed(6, 1, 0, 0, 0);
        graphicCommands->endRenderPass();
    }

    void InSelectedEntityPass::clearImage(int _index, vk::VulkanGraphicCommands *graphicCommands)
    {
        graphicCommands->barrierImageAccess(blurTargets[_index], VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                            VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT);

        VkClearColorValue clear{};
        graphicCommands->clearImage(blurTargets[_index], &clear);

        graphicCommands->barrierImageAccess(blurTargets[_index], VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                            VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);
    }

    void InSelectedEntityPass::prepareImageToDraw(int _index, vk::VulkanGraphicCommands *graphicCommands)
    {
        graphicCommands->barrierImageAccess(blurTargets[_index], VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                            VK_ACCESS_SHADER_READ_BIT, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);
    }

    void InSelectedEntityPass::prepareImageToSampled(int _index, vk::VulkanGraphicCommands *graphicCommands)
    {
        graphicCommands->barrierImageAccess(blurTargets[_index], VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                                            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                            VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
    }
}