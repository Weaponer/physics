#include "rendering/internal/passes/inTestProjectionPass.h"

namespace phys::ge
{
    InTestProjectionPass::InTestProjectionPass()
    {
    }

    void InTestProjectionPass::release()
    {
    }

    void InTestProjectionPass::init(const InitContextData *_context, vk::VulkanQueue *_graphicQueue)
    {
        tempImage = _context->imageAllocator->createImage(vk::MemoryUseType_Long, vk::VulkanImageType_ImageStatic,
                                                          VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                                                          VK_IMAGE_TYPE_2D, _context->swapchain->getImage(0)->getFormat(), VK_IMAGE_ASPECT_COLOR_BIT,
                                                          _context->swapchain->getWidth(), _context->swapchain->getHeight(), 1, 1, 1, 1, &_context->mainFamilyIndex);

        vk::VulkanImageView::createImageView(_context->device, VK_IMAGE_VIEW_TYPE_2D, tempImage, &tempImageView);
        sampler = new vk::VulkanSampler(_context->device);
        sampler->update();

        vertexModul = _context->shaderProvider->getShaderModule("fullScreenVertex");
        fragmentModul = _context->shaderProvider->getShaderModule("testProjectionFragment");

        vk::ShaderModule modules[2] = {vertexModul, fragmentModul};
        vk::VulkanPipelineLayout::createPipelineLayout(_context->device, 2, modules, &layout);

        graphicPipeline = _context->graphicPipelinesManager->createGraphicPipeline();

        vk::VulkanBlendColorState colorState = vk::VulkanBlendColorState();
        colorState.setCountColorAttachments(1);

        graphicPipeline->setVertexModule(vertexModul);
        graphicPipeline->setFragmentModule(fragmentModul);
        graphicPipeline->setPipelineLayout(layout);
        graphicPipeline->setBlendColorState(&colorState);
        graphicPipeline->setVertexAttributeState(_context->fullScreenPassData->getVertexAttributeState());
        graphicPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_VIEWPORT, true);
        graphicPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_SCISSOR, true);
        graphicPipeline->setDepthWrite(false);
        graphicPipeline->setDepthTest(false);

        _context->graphicPipelinesManager->updateGraphicPipeline(graphicPipeline, _context->renderPass);

        graphicPipeline->setBlendColorState(NULL);
        graphicPipeline->setVertexAttributeState(NULL);

        vertexParameters = new vk::ShaderAutoParameters(_context->device, vertexModul);
        vertexParameters->takeMemoryPools(_context->descriptorPoolManager, 1);
        fragmentParameters = new vk::ShaderAutoParameters(_context->device, fragmentModul);
        fragmentParameters->takeMemoryPools(_context->descriptorPoolManager, 1);

        vertexParameters->beginUpdate();
        vertexParameters->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, _context->cameraData->getCameraData());
        vertexParameters->endUpdate();

        fragmentParameters->beginUpdate();
        //fragmentParameters->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, _context->cameraData->getCameraData());
        fragmentParameters->setParameter<vk::ShaderAutoParameters::SampledImage>(0, 0, tempImageView, sampler);
        fragmentParameters->endUpdate();
    }

    void InTestProjectionPass::render(InScene *_scene, vk::VulkanGraphicCommands *graphicCommands, const RenderContextData *_context)
    {
        graphicCommands->barrierImageAccess(_context->imageBuffer, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                                            VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT);
        graphicCommands->barrierImageAccess(tempImage, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                            VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT);

        graphicCommands->copyImage(_context->imageBuffer, tempImage);

        graphicCommands->barrierImageAccess(_context->imageBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                            VK_ACCESS_TRANSFER_READ_BIT, VK_ACCESS_TRANSFER_WRITE_BIT);

        VkClearColorValue val{};
        graphicCommands->clearImage(_context->imageBuffer, &val);

        graphicCommands->barrierImageAccess(_context->imageBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                            VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

        graphicCommands->barrierImageAccess(tempImage, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                                            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                            VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);

        vk::VulkanRenderPass renderPass = _context->renderPass;
        vk::VulkanFramebuffer framebuffer = _context->framebuffer;
        vk::VulkanBuffer *vertBuffer = _context->fullScreenPassData->getVertexBuffer();
        vk::VulkanBuffer *indexBuffer = _context->fullScreenPassData->getIndexBuffer();

        graphicCommands->beginRenderPass(renderPass, framebuffer);
        graphicCommands->bindPipeline(graphicPipeline->getPipeline(&renderPass));
        graphicCommands->bindDescriptorSet(layout, vertexParameters->getCurrentSet(0), 0);
        graphicCommands->bindDescriptorSet(layout, fragmentParameters->getCurrentSet(0), 1);
        graphicCommands->setDefaultViewport(framebuffer);
        graphicCommands->bindVertexBuffer(vertBuffer, 0);
        graphicCommands->bindIndexBuffer(indexBuffer, 0, VkIndexType::VK_INDEX_TYPE_UINT16);
        graphicCommands->drawIndexed(6, 1, 0, 0, 0);
        graphicCommands->endRenderPass();
    }

    void InTestProjectionPass::cleanup(const CleanupContextData *_context)
    {
        fragmentParameters->freeMemoryPools(_context->descriptorPoolManager);
        vertexParameters->freeMemoryPools(_context->descriptorPoolManager);
        delete fragmentParameters;
        delete vertexParameters;
        _context->graphicPipelinesManager->deleteGraphicPipeline(graphicPipeline);
        vk::VulkanPipelineLayout::destroyPipelineLayout(_context->device, &layout);

        delete sampler;
        vk::VulkanImageView::destroyImageView(_context->device, tempImageView);
        _context->imageAllocator->destroyImage(tempImage);
    }
}