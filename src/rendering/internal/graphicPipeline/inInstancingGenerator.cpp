#include "rendering/internal/graphicPipeline/inInstancingGenerator.h"

#include "common/phMacros.h"

namespace phys::ge
{
#define MAX_SIZE_INSTANCE_GROUP 500

    InInstancingGenerator::InInstancingGenerator() : PhBase(), bufferAllocator(NULL), mainFamilyIndex(0),
                                                     instanceBuffers(NULL), drawCalls(), sizeInstanceData(0),
                                                     currentBufferWrite(NULL), pCurrentWrite(NULL), currentIndexInstance(0)
    {
    }

    void InInstancingGenerator::release()
    {
        if (instanceBuffers)
            PH_RELEASE(instanceBuffers);
        drawCalls.release();
    }

    void InInstancingGenerator::initBuffers(vk::VulkanBufferAllocator *_bufferAllocator, uint32_t _mainFamilyIndex)
    {
        sizeInstanceData = getSizeInstanceData();
        instanceBuffers = new (phAllocateMemory(sizeof(InInstanceBuffers))) InInstanceBuffers(sizeInstanceData, MAX_SIZE_INSTANCE_GROUP,
                                                                                              _bufferAllocator, _mainFamilyIndex);
        bufferAllocator = _bufferAllocator;
        mainFamilyIndex = _mainFamilyIndex;
    }

    void InInstancingGenerator::beginInstances(vk::VulkanTransferCommands *_transferCommands)
    {
        PH_ASSERT(!currentBufferWrite, "Instancing generator already started.");

        cleanLastDrawCalls();

        currentBufferWrite = instanceBuffers->allocateBuffer();
        pCurrentWrite = _transferCommands->beginWrite(currentBufferWrite, 0, currentBufferWrite->getSize());
        currentIndexInstance = 0;
    }

    void InInstancingGenerator::addRenderers(uint _count, GeRenderer **_renderers, vk::VulkanTransferCommands *_transferCommands)
    {
        PH_ASSERT(currentBufferWrite, "Instancing generator didn`t start.");

        uint32_t indexStartBuffer = currentIndexInstance;

        for (uint32_t i2 = 0; i2 < _count; i2++)
        {
            GeRenderer *renderer = _renderers[i2];

            if (pushInstanceData(renderer, (uint8_t *)pCurrentWrite + currentIndexInstance * sizeInstanceData))
                currentIndexInstance++;

            if (currentIndexInstance == MAX_SIZE_INSTANCE_GROUP)
            {
                if (indexStartBuffer != currentIndexInstance)
                    pushDrawCall(currentBufferWrite, getMesh(renderer), indexStartBuffer, currentIndexInstance - indexStartBuffer);

                _transferCommands->endWrite(currentBufferWrite);
                currentIndexInstance = indexStartBuffer = 0;
                currentBufferWrite = instanceBuffers->allocateBuffer();
                pCurrentWrite = _transferCommands->beginWrite(currentBufferWrite, 0, currentBufferWrite->getSize());
            }
        }

        if (currentIndexInstance != indexStartBuffer)
            pushDrawCall(currentBufferWrite, getMesh(_renderers[_count - 1]), indexStartBuffer, currentIndexInstance - indexStartBuffer);

        if (currentIndexInstance == MAX_SIZE_INSTANCE_GROUP)
        {
            _transferCommands->endWrite(currentBufferWrite);
            currentIndexInstance = 0;
            currentBufferWrite = instanceBuffers->allocateBuffer();
            pCurrentWrite = _transferCommands->beginWrite(currentBufferWrite, 0, currentBufferWrite->getSize());
        }
    }

    void InInstancingGenerator::endInstances(vk::VulkanTransferCommands *_transferCommands)
    {
        PH_ASSERT(currentBufferWrite, "Instancing generator didn`t start.");

        _transferCommands->endWrite(currentBufferWrite);
        currentBufferWrite = NULL;
        pCurrentWrite = NULL;
        currentIndexInstance = 0;
    }

    void InInstancingGenerator::cleanLastDrawCalls()
    {
        drawCalls.clearMemory();
        instanceBuffers->resetUsedBuffers();
    }

    void InInstancingGenerator::pushDrawCall(vk::VulkanBuffer *_buffer, InMesh *_mesh, uint32_t _offset, uint32_t _countInstance)
    {
        DrawCall drawCall;
        drawCall.buffer = _buffer;
        drawCall.mesh = _mesh;
        drawCall.offset = _offset * sizeInstanceData;
        drawCall.countInstance = _countInstance;
        drawCalls.assignmentMemoryAndCopy(&drawCall);
    }
}