#include <MTH/vectors.h>

#include "rendering/internal/graphicPipeline/inFullScreenPassData.h"

#include "gpuCommon/vulkanFence.h"

namespace phys::ge
{
    InFullScreenPassData::InFullScreenPassData() : attributeState(), bufferAllocator(NULL),
                                                   vertexBuffer(NULL), indexBuffer(NULL), descriptorPoolManager(NULL), vertexParams(NULL)
    {
    }

    void InFullScreenPassData::release()
    {
        if (!bufferAllocator)
            return;

        vertexParams->freeMemoryPools(descriptorPoolManager);
        delete vertexParams;

        bufferAllocator->destroyBuffer(vertexBuffer);
        bufferAllocator->destroyBuffer(indexBuffer);
    }

    void InFullScreenPassData::initData(InCamereData *_cameraData, vk::VulkanBufferAllocator *_bufferAllocator, GeShaderProvider *_shaderProvider,
                                        vk::VulkanCommandReceiver *_receiver, vk::VulkanQueue *_graphicQueue)
    {
        bufferAllocator = _bufferAllocator;

        struct vert
        {
            mth::vec2 position;
            mth::vec2 uv;
        } vertexes[4];

        vertexes[0] = {mth::vec2(-1.0f, -1.0f), mth::vec2(0.0f, 0.0f)};
        vertexes[1] = {mth::vec2(1.0f, -1.0f), mth::vec2(1.0f, 0.0f)};
        vertexes[2] = {mth::vec2(-1.0f, 1.0f), mth::vec2(0.0f, 1.0f)};
        vertexes[3] = {mth::vec2(1.0f, 1.0f), mth::vec2(1.0f, 1.0f)};

        uint16_t indexes[6] = {0, 1, 2,
                               3, 2, 1};

        attributeState.clearAttributes();
        attributeState.setSizeInputVertex(sizeof(vert));
        attributeState.addInputVertexAttribute(offsetof(vert, position), VK_FORMAT_R32G32_SFLOAT);
        attributeState.addInputVertexAttribute(offsetof(vert, uv), VK_FORMAT_R32G32_SFLOAT);

        int totalSize = sizeof(vertexes) + sizeof(indexes);

        {
            vk::VulkanTransferCommands transferCommands = vk::VulkanTransferCommands(_graphicQueue, false);

            uint32_t familyIndex = _graphicQueue->getFamilyIndex();
            vk::VulkanBuffer *stagingBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Temporary, vk::VulkanBufferType_Staging,
                                                                            totalSize, 1, &familyIndex);

            transferCommands.writeToBuffer(stagingBuffer, 0, sizeof(vertexes), vertexes);
            transferCommands.writeToBuffer(stagingBuffer, sizeof(vertexes), sizeof(indexes), indexes);

            vertexBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                         sizeof(vertexes), 1, &familyIndex, "FullScreen_vertexes");
            indexBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_IndexBufferStatic,
                                                        sizeof(indexes), 1, &familyIndex, "FullScreen_indexes");

            transferCommands.copyBuffer(stagingBuffer, 0, vertexBuffer, 0, sizeof(vertexes));
            transferCommands.copyBuffer(stagingBuffer, sizeof(vertexes), indexBuffer, 0, sizeof(indexes));

            vk::VulkanFence fence{};
            vk::VulkanFence::createFence(_receiver->getDevice(), &fence, false);
            _receiver->submitCommands(&transferCommands, fence);
            fence.wait();
            vk::VulkanFence::destroyFence(&fence);

            bufferAllocator->destroyBuffer(stagingBuffer);
        }

        descriptorPoolManager = _shaderProvider->getVulkanDescriptorPoolManager();
        vk::ShaderModule vert = _shaderProvider->getShaderModule("fullScreenVertex");
        vertexParams = new vk::ShaderAutoParameters(_receiver->getDevice(), vert);
        descriptorPoolManager = _shaderProvider->getVulkanDescriptorPoolManager();
        vertexParams->takeMemoryPools(descriptorPoolManager, 1);
        vertexParams->beginUpdate();
        vertexParams->setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, _cameraData->getCameraData());
        vertexParams->endUpdate();

        vertexSet = vertexParams->getCurrentSet(0);
    }
}