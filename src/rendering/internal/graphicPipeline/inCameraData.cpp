#include "rendering/internal/graphicPipeline/inCameraData.h"

namespace phys::ge
{
    InCamereData::InCamereData() : data({}), bufferAllocator(NULL), dataBuffer(NULL)
    {
    }

    void InCamereData::release()
    {
        bufferAllocator->destroyBuffer(dataBuffer);
    }

    void InCamereData::initBuffers(vk::VulkanBufferAllocator *_bufferAllocator, uint32_t _mainFamilyIndex)
    {
        bufferAllocator = _bufferAllocator;
        dataBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_UniformBlockDynamic,
                                                   sizeof(CameraData), 1, &_mainFamilyIndex, "CamereDataUBO");
    }

    void InCamereData::updateData(GeCamera *_camera, vk::VulkanTransferCommands *_transferCommands)
    {
        data.cameraV = _camera->getInverseTRS();
        data.cameraInverseV = _camera->getTRS();
        data.cameraP = _camera->getProjectionMatrix();
        data.cameraInverseP = _camera->getInverseProjectionMatrix();
        data.cameraVP = data.cameraP * data.cameraV;
        data.cameraPosition = _camera->getPosition();
        data.far = _camera->getFar();
        data.near = _camera->getNear();

        _transferCommands->writeToBuffer(dataBuffer, 0, sizeof(CameraData), &data);
    }

    vk::VulkanBuffer *InCamereData::getCameraData()
    {
        return dataBuffer;
    }
}