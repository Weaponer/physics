#include <MTH/bounds.h>

#include "rendering/internal/graphicPipeline/inMeshManager.h"

#include "foundation/phMemoryAllocator.h"

#include "common/phMacros.h"

namespace phys::ge
{
    InMeshManager::InMeshManager() : PhBase(), meshes(), eventsWriteData(), bufferAllocator(NULL), mainQueueFamily(0),
                                     stagingBuffer(NULL)

    {
    }

    void InMeshManager::release()
    {
        uint32_t countEvents = eventsWriteData.getPos();
        for (uint32_t i = 0; i < countEvents; i++)
            phDeallocateMemory(eventsWriteData.getMemory(i)->data);

        eventsWriteData.release();
        meshes.release();

        if (bufferAllocator == NULL)
            return;

        bufferAllocator->destroyBuffer(stagingBuffer);
    }

    void InMeshManager::init(vk::VulkanBufferAllocator *_bufferAllocator, uint32_t _mainQueueFamily)
    {
        bufferAllocator = _bufferAllocator;
        stagingBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_Staging,
                                                      4096, 1, &_mainQueueFamily, PH_TANAME(InMeshManager, "_staging"));
    }

    GeMesh *InMeshManager::registerMesh(const PhObjectMesh::Mesh *_mesh)
    {
        PH_ASSERT(bufferAllocator != NULL, "InMeshManager don`t initialized yet.");

        const PhObjectMesh::Mesh *mesh = _mesh;

        VkIndexType indexType = VkIndexType::VK_INDEX_TYPE_UINT16;
        if (mesh->getCountVertexes() > UINT16_MAX)
        {
            indexType = VkIndexType::VK_INDEX_TYPE_UINT32;
        }

        FillMeshDataEvent event{};
        event.countVertex = mesh->getCountVertexes();
        event.countIndex = mesh->getCountIndexes();
        event.countLineVertex = mesh->getCountLineVertexes();
        uint32_t sizeDataVertex = sizeof(PhObjectMesh::Vertex) * event.countVertex;
        event.beginVertex = 0;
        event.beginIndex = sizeDataVertex;
        uint32_t sizeDataIndex = 0;

        if (indexType == VkIndexType::VK_INDEX_TYPE_UINT32)
            sizeDataIndex = sizeof(uint32_t) * event.countIndex;
        else
            sizeDataIndex = sizeof(uint16_t) * event.countIndex;

        event.beginLineVertex = sizeDataVertex + sizeDataIndex;
        uint32_t sizeDataLineVertex = sizeof(PhObjectMesh::LineVertex) * event.countLineVertex;
        uint32_t sizeData = sizeDataVertex + sizeDataIndex + sizeDataLineVertex;

        char *data = (char *)phAllocateMemory(sizeData);
        std::memcpy(data, mesh->getVertexes(), sizeDataVertex);
        if (indexType == VkIndexType::VK_INDEX_TYPE_UINT32)
        {
            std::memcpy(data + event.beginIndex, mesh->getIndexes(), sizeDataIndex);
        }
        else
        {
            uint16_t *dstIndexes = (uint16_t *)(data + event.beginIndex);
            const uint *srcIndexes = mesh->getIndexes();
            for (uint i = 0; i < event.countIndex; i++)
            {
                dstIndexes[i] = (uint16_t)srcIndexes[i];
            }
        }
        std::memcpy(data + event.beginLineVertex, mesh->getLineVertexes(), sizeDataLineVertex);

        event.indexWrited = 0;
        event.data = data;
        event.sizeData = sizeData;

        InMesh *inMesh = new (phAllocateMemory(sizeof(InMesh))) InMesh();

        event.mesh = inMesh;
        event.canceled = false;

        inMesh->indexFillMeshEvent = eventsWriteData.getPos();
        eventsWriteData.assignmentMemoryAndCopy(&event);

        inMesh->bound = calculateAABB(mesh);

        inMesh->indexType = indexType;
        inMesh->countIndex = event.countIndex;
        inMesh->countVertex = event.countVertex;
        inMesh->countLineVertex = event.countLineVertex;

        inMesh->vertexBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                             sizeDataVertex, 1, &mainQueueFamily, PH_CNAME(_mesh->getName(), "_vertexes"));
        inMesh->indexBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_IndexBufferStatic,
                                                            sizeDataIndex, 1, &mainQueueFamily, PH_CNAME(_mesh->getName(), "_indexes"));
        inMesh->lineVertexBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic,
                                                                 sizeDataLineVertex, 1, &mainQueueFamily, PH_CNAME(_mesh->getName(), "_lines"));
        meshes.assignmentMemoryAndCopy(&inMesh, inMesh->indexMesh);
        return inMesh;
    }

    void InMeshManager::unregisterMesh(GeMesh *_mesh)
    {
        PH_ASSERT(bufferAllocator != NULL, "InMeshManager don`t initialized yet.");

        InMesh *inMesh = dynamic_cast<InMesh *>(_mesh);
        uint indexFillMeshEvent = inMesh->indexFillMeshEvent;
        if (indexFillMeshEvent < eventsWriteData.getPos() && eventsWriteData.getMemory(indexFillMeshEvent)->mesh == inMesh)
        {
            FillMeshDataEvent *event = eventsWriteData.getMemory(indexFillMeshEvent);
            event->canceled = true;
            event->mesh = NULL;
        }

        bufferAllocator->destroyBuffer(inMesh->vertexBuffer);
        bufferAllocator->destroyBuffer(inMesh->indexBuffer);
        bufferAllocator->destroyBuffer(inMesh->lineVertexBuffer);

        meshes.popMemory(inMesh->indexMesh);

        PH_RELEASE(inMesh)
    }

    void InMeshManager::sendData(vk::VulkanTransferCommands *_commands, bool *_endSend, bool *_wasWrited, bool *_endFreeSpace)
    {
        int freeSpace = stagingBuffer->getSize();
        int indexStagingBuffer = 0;
        while (true)
        {
            if (eventsWriteData.getPos() == 0)
            {
                *_endSend = true;
                return;
            }

            int indexEvent = eventsWriteData.getPos() - 1;
            FillMeshDataEvent *event = eventsWriteData.getMemory(indexEvent);
            if (event->canceled)
            {
                eventIsFinish(event);
                eventsWriteData.popMemory();
                continue;
            }

            int offsetData = event->indexWrited;
            int sizeData = event->sizeData;
            int countWriteData = sizeData - offsetData;
            int countWrite = std::min(freeSpace, countWriteData);

            _commands->writeToBuffer(stagingBuffer, indexStagingBuffer, countWrite, ((char *)event->data) + offsetData);
            sendCopyCommands(_commands, event, indexStagingBuffer, offsetData, countWrite);
            *_wasWrited = true;

            freeSpace -= countWrite;
            indexStagingBuffer += countWrite;

            event->indexWrited += countWrite;

            if (event->indexWrited == event->sizeData)
            {
                eventIsFinish(event);
                eventsWriteData.popMemory();
            }

            if (freeSpace <= 0)
            {
                *_endFreeSpace = true;
                return;
            }
        }
    }

    void InMeshManager::eventIsFinish(FillMeshDataEvent *_event)
    {
        phDeallocateMemory(_event->data);
        if (_event->mesh != NULL)
        {
            _event->mesh->finishLoad = true;
            _event->mesh->indexFillMeshEvent = 0;
            _event->mesh = NULL;
        }
    }

    void InMeshManager::sendCopyCommands(vk::VulkanTransferCommands *_commands, const FillMeshDataEvent *_event,
                                         int _indexStagingBuffer, int _indexData, int _sizeData)
    {
        struct BufferWrite
        {
            vk::VulkanBuffer *buffer;
            uint beginInData;
        };

        BufferWrite buffersWrite[3];
        buffersWrite[0] = {_event->mesh->vertexBuffer, _event->beginVertex};
        buffersWrite[1] = {_event->mesh->indexBuffer, _event->beginIndex};
        buffersWrite[2] = {_event->mesh->lineVertexBuffer, _event->beginLineVertex};

        for (int i = 0; i < 3; i++)
        {
            BufferWrite buff = buffersWrite[i];
            int size = buff.buffer->getSize();
            int begin = (int)buff.beginInData;

            if ((size + begin) <= _indexData || (_indexData + _sizeData) <= begin)
                continue;

            int offsetSrc = std::max(begin - _indexData, 0);
            int offsetDst = std::max(_indexData - begin, 0);
            int sizeDst = std::min(std::min((_indexData + _sizeData) - begin, size - offsetDst), _sizeData);
            _commands->copyBuffer(stagingBuffer, _indexStagingBuffer + offsetSrc, buff.buffer, offsetDst, sizeDst);
        }
    }

    mth::boundAABB InMeshManager::calculateAABB(const PhObjectMesh::Mesh *_mesh)
    {
        int countVertexes = _mesh->getCountVertexes();
        const PhObjectMesh::Vertex *vertexes = _mesh->getVertexes();
        mth::vec3 *positions = (mth::vec3 *)phAllocateMemory(sizeof(mth::vec3) * countVertexes);

        for (int i = 0; i < countVertexes; i++)
            positions[i] = vertexes[i].pos;

        mth::boundAABB aabb = mth::generate_AABB_from_points(countVertexes, positions);
        phDeallocateMemory(positions);
        return aabb;
    }
}