#include "rendering/internal/graphicPipeline/inStaticBufferLoader.h"
#include "common/phMacros.h"

namespace phys::ge
{
    InStaticBufferLoader::InStaticBufferLoader(vk::VulkanDevice *_device, vk::VulkanBufferAllocator *_bufferAllocator, uint32_t _mainFamilyIndex)
        : PhBase(), bufferAllocator(_bufferAllocator), mainFamilyIndex(_mainFamilyIndex), sendsBuffer(), stagingBuffer(NULL), fence()
    {
        vk::VulkanFence::createFence(_device, &fence);
    }

    void InStaticBufferLoader::release()
    {
        vk::VulkanFence::destroyFence(&fence);
        if (stagingBuffer)
        {
            bufferAllocator->destroyBuffer(stagingBuffer);
            stagingBuffer = NULL;
        }
        sendsBuffer.release();
        PhBase::release();
    }

    void InStaticBufferLoader::beginSend()
    {
        sendsBuffer.clearMemory();
    }

    void InStaticBufferLoader::addBuffer(vk::VulkanBuffer *_staticBuffer, const char *_data, uint32_t _sizeData)
    {
        SendBuffer send{};
        send.buffer = _staticBuffer;
        send.data = _data;
        send.sizeData = _sizeData;
        sendsBuffer.assignmentMemoryAndCopy(&send);
    }

    void InStaticBufferLoader::endSend(vk::VulkanCommandReceiver *_receiver, vk::VulkanQueue *_transferQueue)
    {
        uint count = sendsBuffer.getPos();
        if (count == 0)
            return;

        uint sumSize = 0;
        uint maxSize = getMaxSizeBuffer();

        uint indexStart = 0;
        for (uint i = 0; i < count; i++)
        {
            SendBuffer send = *sendsBuffer.getMemory(i);
            if (send.sizeData + sumSize > maxSize)
            {
                sendRange(indexStart, i - indexStart, _receiver, _transferQueue);
                indexStart = i;
                sumSize = send.sizeData;
            }
            else
            {
                sumSize += send.sizeData;
            }
        }

        sendRange(indexStart, count - indexStart, _receiver, _transferQueue);
    }

    void InStaticBufferLoader::sendRange(uint _start, uint _count, vk::VulkanCommandReceiver *_receiver, vk::VulkanQueue *_transferQueue)
    {
        fence.reset();
        vk::VulkanTransferCommands transfer = vk::VulkanTransferCommands(_transferQueue, false);
        uint maxI = _start + _count;

        uint maxSum = 0;
        for (uint i = _start; i < maxI; i++)
        {
            SendBuffer send = *sendsBuffer.getMemory(i);
            maxSum += send.sizeData;
        }

        ensureStagingBuffer(maxSum);

        uint offset = 0;
        char *pWrite = (char *)transfer.beginWrite(stagingBuffer, 0, maxSum);
        for (uint i = _start; i < maxI; i++)
        {
            SendBuffer send = *sendsBuffer.getMemory(i);
            std::memcpy(pWrite + offset, send.data, send.sizeData);
            transfer.copyBuffer(stagingBuffer, offset, send.buffer, 0, send.sizeData);
            offset += send.sizeData;
        }
        transfer.endWrite(stagingBuffer);

        _receiver->submitCommands(&transfer, fence);
        fence.wait();
    }

    uint32_t InStaticBufferLoader::getMaxSizeBuffer()
    {
        return bufferAllocator->getMemoryController()->getSizePage(vk::MemoryUseType_Long);
    }

    void InStaticBufferLoader::ensureStagingBuffer(uint32_t _size)
    {
        if (stagingBuffer != NULL && stagingBuffer->getSize() < _size)
        {
            bufferAllocator->destroyBuffer(stagingBuffer);
        }
        else if (stagingBuffer != NULL)
        {
            return;
        }

        stagingBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_Staging,
                                                      _size, 1, &mainFamilyIndex, PH_TANAME(InStaticBufferLoader, "_staging"));
    }
}