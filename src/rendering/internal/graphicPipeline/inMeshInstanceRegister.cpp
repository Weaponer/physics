#include "rendering/internal/graphicPipeline/inMeshInstanceRegister.h"

#include "common/phMacros.h"

namespace phys::ge
{
    void InMeshInstanceRegister::release()
    {
        int pos = instanceGroups.getPosBuffer();
        for (int i = 0; i < pos; i++)
        {
            InInstanceGroup<InMeshRenderer> *group = instanceGroups.getMemory(i)->group;
            if (group != NULL)
                group->release();
        }
        instanceGroups.release();
    }

    uint InMeshInstanceRegister::registerRenderer(InMeshRenderer *_renderer, InMesh *_mesh)
    {
        MeshInstanceGroup *group = NULL;
        int pos = instanceGroups.getPosBuffer();
        for (int i = 0; i < pos; i++)
        {
            MeshInstanceGroup *check = instanceGroups.getMemory(i);
            if (_mesh == check->mesh)
            {
                group = check;
                break;
            }
        }

        if (group == NULL)
        {
            uint index = 0;
            group = instanceGroups.assignmentMemory(index);
            group->mesh = _mesh;
            group->group = new (phAllocateMemory(sizeof(InInstanceGroup<InMeshRenderer>))) InInstanceGroup<InMeshRenderer>();
        }

        uint index = 0;
        group->group->instances.assignmentMemoryAndCopy(&_renderer, index);
        return index;
    }

    void InMeshInstanceRegister::unregisterRenderer(InMesh *_mesh, uint _index)
    {
        MeshInstanceGroup *group = NULL;
        uint indexGroup = 0;
        int pos = instanceGroups.getPosBuffer();
        for (int i = 0; i < pos; i++)
        {
            MeshInstanceGroup *check = instanceGroups.getMemory(i);
            if (check->mesh == _mesh)
            {
                group = check;
                indexGroup = i;
                break;
            }
        }
        if (!group)
            return;

        group->group->instances.popMemory(_index);
        if (group->group->instances.getCountUse() == 0)
        {
            PH_RELEASE(group->group);
            group->mesh = NULL;
            group->group = NULL;
            instanceGroups.popMemory(indexGroup);
        }
    }
}