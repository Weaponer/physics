#include "rendering/internal/graphicPipeline/inGraphicPipeline.h"
#include "rendering/internal/graphicPipeline/inDebugMarkerColors.h"

#include "common/phMacros.h"

namespace phys::ge
{
    InGraphicPipeline::InGraphicPipeline()
    {
    }

    void InGraphicPipeline::release()
    {
        InPass::CleanupContextData cleanupContext;
        fillCleanupContext(&cleanupContext);
        pivotRendererPass->cleanup(&cleanupContext);
        selectedEntityPass->cleanup(&cleanupContext);
        debugLinesPass->cleanup(&cleanupContext);
        meshRendererPass->cleanup(&cleanupContext);
        floorPass->cleanup(&cleanupContext);
        skyBoxPass->cleanup(&cleanupContext);
        scenePickerPass->cleanup(&cleanupContext);

        destroyFramebuffers();

        PH_RELEASE(pivotRendererPass);
        PH_RELEASE(selectedEntityPass);
        PH_RELEASE(debugLinesPass);
        PH_RELEASE(meshRendererPass);
        PH_RELEASE(floorPass);
        PH_RELEASE(skyBoxPass);
        PH_RELEASE(scenePickerPass);

        PH_RELEASE(meshRendererCullingManager);
        PH_RELEASE(fullScreenPassData);
        PH_RELEASE(cameraData);
        delete graphicPipelinesManager;

        PH_RELEASE(memoryLoader);
        vk::VulkanSemaphore::destroySemaphore(&signalEndLoading);

        delete renderPassesManager;
        delete computeCommands;
        delete graphicsCommands;
        PhBase::release();
    }

    void InGraphicPipeline::init()
    {
        device = commandReceiver->getDevice();
        graphicsCommands = new vk::VulkanGraphicCommands(graphicQueue);
        computeCommands = new vk::VulkanComputeCommands(computeQueue);
        renderPassesManager = new vk::RenderPassesManager(device);

        vk::VulkanSemaphore::createSemaphore(device, &signalEndLoading, "SignalEndLoading");

        memoryLoader = new (phAllocateMemory(sizeof(InMemoryLoader))) InMemoryLoader();
        memoryLoader->device = device;
        memoryLoader->receiver = commandReceiver;
        memoryLoader->transferCommands = graphicsCommands;
        memoryLoader->init();

        initFramebuffers();

        graphicPipelinesManager = new vk::GraphicPipelinesManager(device);
        cameraData = new (phAllocateMemory(sizeof(InCamereData))) InCamereData();
        fullScreenPassData = new (phAllocateMemory(sizeof(InFullScreenPassData))) InFullScreenPassData();
        meshRendererCullingManager = new (phAllocateMemory(sizeof(InMeshRendererCullingManager))) InMeshRendererCullingManager();

        scenePickerPass = new (phAllocateMemory(sizeof(InScenePickerPass))) InScenePickerPass();
        skyBoxPass = new (phAllocateMemory(sizeof(InSkyBoxPass))) InSkyBoxPass();
        floorPass = new (phAllocateMemory(sizeof(InFloorPass))) InFloorPass();
        meshRendererPass = new (phAllocateMemory(sizeof(InMeshRendererPass))) InMeshRendererPass();
        debugLinesPass = new (phAllocateMemory(sizeof(InDebugLinesPass))) InDebugLinesPass();
        selectedEntityPass = new (phAllocateMemory(sizeof(InSelectedEntityPass))) InSelectedEntityPass();
        pivotRendererPass = new (phAllocateMemory(sizeof(InPivotRendererPass))) InPivotRendererPass();

        InPass::InitContextData initContextData;
        fillCleanupContext(&initContextData);
        fillInitContext(&initContextData);

        cameraData->initBuffers(bufferAllocator, graphicQueue->getFamilyIndex());
        fullScreenPassData->initData(cameraData, bufferAllocator, shaderProvider, commandReceiver, graphicQueue);
        meshRendererCullingManager->init(threadsManager);

        scenePickerPass->init(&initContextData, graphicQueue);
        skyBoxPass->init(&initContextData, graphicQueue);
        floorPass->init(&initContextData, graphicQueue);
        meshRendererPass->init(&initContextData, graphicQueue);
        debugLinesPass->init(&initContextData, graphicQueue);
        selectedEntityPass->init(&initContextData, graphicQueue);
        pivotRendererPass->init(&initContextData, graphicQueue);
    }

    bool InGraphicPipeline::render(InScene *_scene, uint32_t _imageIndex, vk::VulkanSemaphore _waitBegin, vk::VulkanSemaphore _signalEnd, vk::VulkanFence _fence)
    {
        graphicsCommands->resetCommandBuffer();
        graphicsCommands->addWaitSemaphoreCmd(_waitBegin, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);

        bool _needWaitEndLoading = false;
        memoryLoader->loadResourceScene(_scene, signalEndLoading, &_needWaitEndLoading);

        if (_needWaitEndLoading)
        {
            graphicsCommands->updateCommandBuffer();
            graphicsCommands->addWaitSemaphoreCmd(signalEndLoading, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
        }

        graphicsCommands->debugMarkerBegin("Rendering", InDebugMarkersUtils::mainSystemColor());
        clearRendererTarget(_imageIndex);

        InPass::RenderContextData contextData;
        fillCleanupContext(&contextData);
        fillInitContext(&contextData);
        fillRenderContext(_imageIndex, &contextData);

        cameraData->updateData(_scene->getActiveCamera(), graphicsCommands);
        meshRendererCullingManager->updateCulling(_scene->meshInstanceRegister, _scene->getActiveCamera());

        if (_scene->getSettings().isSelectableObjects)
        {
            scenePickerPass->render(_scene, graphicsCommands, &contextData);
            clearDepthBuffer();
        }

        skyBoxPass->render(_scene, graphicsCommands, &contextData);

        graphicsCommands->barrierImageAccess(contextData.imageBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                             VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                             VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

        graphicsCommands->debugMarkerBegin("MainGeometryRendering", InDebugMarkersUtils::subGroupColor());
        {
            floorPass->render(_scene, graphicsCommands, &contextData);
            meshRendererPass->render(_scene, graphicsCommands, &contextData);
        }
        graphicsCommands->debugMarkerEnd();

        graphicsCommands->debugMarkerBegin("EditorFunctionsRendering", InDebugMarkersUtils::subGroupColor());
        {
            debugLinesPass->render(_scene, graphicsCommands, &contextData);
            clearDepthBuffer();
            selectedEntityPass->render(_scene, graphicsCommands, &contextData);
            clearDepthBuffer();
            pivotRendererPass->render(_scene, graphicsCommands, &contextData);
        }
        graphicsCommands->debugMarkerEnd();

        graphicsCommands->barrierImageAccess(contextData.imageBuffer, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                                             VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                                             VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_NONE);
        graphicsCommands->setSignalSemaphoreCmd(_signalEnd);

        graphicsCommands->debugMarkerEnd();

        commandReceiver->submitCommands(graphicsCommands, _fence);
        return true;
    }

    void InGraphicPipeline::initFramebuffers()
    {
        uint32_t countFramebuffers = (uint32_t)swapchain->getCountImage();
        framebuffers.countFramebuffers = countFramebuffers;
        framebuffers.framebuffers = (vk::VulkanFramebuffer *)phAllocateMemory(sizeof(vk::VulkanFramebuffer) * countFramebuffers);

        uint32_t mainFamilyIndex = graphicQueue->getFamilyIndex();

        framebuffers.depthBuffer = imageAllocator->createImage(vk::MemoryUseType_Long, vk::VulkanImageType_ImageStatic,
                                                               VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                                                               VK_IMAGE_TYPE_2D, VK_FORMAT_D32_SFLOAT, VK_IMAGE_ASPECT_DEPTH_BIT,
                                                               swapchain->getWidth(), swapchain->getHeight(), 1, 1, 1, 1, &mainFamilyIndex,
                                                               "DepthBuffer");

        vk::VulkanImageView::createImageView(device, VK_IMAGE_VIEW_TYPE_2D, framebuffers.depthBuffer,
                                             &framebuffers.depthBufferView, "DepthBufferView");

        renderPassesManager->beginCreateRenderPass();
        renderPassesManager->addColorAttachments(swapchain->getImage(0));
        renderPassesManager->setDepthStencil(framebuffers.depthBuffer);
        renderPassesManager->endCreate(&framebuffers.renderPass);

        for (uint32_t i = 0; i < countFramebuffers; i++)
        {
            vk::VulkanFramebuffer framebuffer;
            vk::VulkanImage *colorAttachment = swapchain->getImage(i);
            vk::VulkanImageView viewAttachment = swapchain->getImageView(i);
            vk::VulkanFramebuffer::createFramebuffer(device, framebuffers.renderPass, 1, &colorAttachment, &viewAttachment,
                                                     framebuffers.depthBuffer, framebuffers.depthBufferView, &framebuffer);

            framebuffers.framebuffers[i] = framebuffer;
        }
    }

    void InGraphicPipeline::destroyFramebuffers()
    {
        for (uint32_t i = 0; i < framebuffers.countFramebuffers; i++)
        {
            vk::VulkanFramebuffer::destroyFramebuffer(device, &(framebuffers.framebuffers[i]));
        }
        phDeallocateMemory(framebuffers.framebuffers);
        vk::VulkanImageView::destroyImageView(device, framebuffers.depthBufferView);
        imageAllocator->destroyImage(framebuffers.depthBuffer);
    }

    void InGraphicPipeline::clearRendererTarget(uint32_t _imageIndex)
    {
        graphicsCommands->debugMarkerBegin("ClearRendererTarget", InDebugMarkersUtils::subPassColor());
        vk::VulkanImage *image = swapchain->getImage(_imageIndex);
        graphicsCommands->barrierImageAccess(image, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                             VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                             VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT);

        VkClearColorValue value{};
        graphicsCommands->clearImage(image, &value);
        clearDepthBuffer();
        graphicsCommands->debugMarkerEnd();
    }

    void InGraphicPipeline::clearDepthBuffer()
    {
        graphicsCommands->debugMarkerBegin("ClearDepthBuffer", InDebugMarkersUtils::subPassColor());
        graphicsCommands->barrierImageAccess(framebuffers.depthBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                             VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                             VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT);
        VkClearDepthStencilValue depthValue{};
        depthValue.depth = 1.0f;
        graphicsCommands->clearDepthStencilImage(framebuffers.depthBuffer, &depthValue);
        graphicsCommands->barrierImageAccess(framebuffers.depthBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
                                             VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                                             VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);
        graphicsCommands->debugMarkerEnd();
    }

    void InGraphicPipeline::fillCleanupContext(InPass::CleanupContextData *_context)
    {
        _context->device = device;
        _context->mainFamilyIndex = graphicQueue->getFamilyIndex();
        _context->bufferAllocator = bufferAllocator;
        _context->imageAllocator = imageAllocator;
        _context->commandReceiver = commandReceiver;
        _context->graphicPipelinesManager = graphicPipelinesManager;
        _context->shaderProvider = shaderProvider;
        _context->descriptorPoolManager = shaderProvider->getVulkanDescriptorPoolManager();
    }

    void InGraphicPipeline::fillInitContext(InPass::InitContextData *_context)
    {
        _context->swapchain = swapchain;
        _context->fullScreenPassData = fullScreenPassData;
        _context->cameraData = cameraData;
        _context->renderPassesManager = renderPassesManager;
        _context->meshRendererCullingManager = meshRendererCullingManager;

        _context->renderPass = framebuffers.renderPass;
        _context->depthBuffer = framebuffers.depthBuffer;
        _context->depthBufferView = framebuffers.depthBufferView;
    }

    void InGraphicPipeline::fillRenderContext(uint32_t _imageIndex, InPass::RenderContextData *_context)
    {
        _context->framebuffer = framebuffers.framebuffers[_imageIndex];
        _context->imageBuffer = swapchain->getImage(_imageIndex);
        _context->imageBufferView = swapchain->getImageView(_imageIndex);
    }
}