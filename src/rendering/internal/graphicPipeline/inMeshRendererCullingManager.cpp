#include "rendering/internal/graphicPipeline/inMeshRendererCullingManager.h"

#include "common/phMacros.h"

namespace phys::ge
{
    InMeshRendererCullingManager::InMeshRendererCullingManager() : PhBase(),
                                                                   bufferTempRenderers(), bufferCullingRenderers(),
                                                                   bufferCullingResults(), rendererCulling(NULL)
    {
    }

    void InMeshRendererCullingManager::release()
    {
        if (rendererCulling)
        {
            PH_RELEASE(rendererCulling);
        }

        bufferTempRenderers.release();
        bufferCullingRenderers.release();
        bufferCullingResults.release();
    }

    void InMeshRendererCullingManager::init(PhThreadsManager *_threadsManager)
    {
        rendererCulling = new (phAllocateMemory(sizeof(InRendererCulling))) InRendererCulling();
        rendererCulling->init(_threadsManager);
    }

    void InMeshRendererCullingManager::updateCulling(InMeshInstanceRegister *_meshInstanceRegister, const GeCamera *_camera)
    {
        if (rendererCulling->isLaunchCulling())
            rendererCulling->fetch();

        rendererCulling->clearGroups();

        uint needSize = 0;

        uint sizeTable = _meshInstanceRegister->getSizeTableInstanceGroups();
        const InMeshInstanceRegister::MeshInstanceGroup *tableGroups = _meshInstanceRegister->getTableMeshInstanceGroups();
        for (uint i = 0; i < sizeTable; i++)
        {
            InMeshInstanceRegister::MeshInstanceGroup group = tableGroups[i];
            if (!group.mesh)
                continue;

            needSize += group.group->instances.getCountUse();
        }

        bufferCullingRenderers.setCapacity(needSize);
        bufferCullingResults.setCapacity(needSize);

        RendererCullingData *cullingRenderers = bufferCullingRenderers.getMemory(0);
        bool *cullingResults = bufferCullingResults.getMemory(0);

        uint indexRenderer = 0;
        for (uint i = 0; i < sizeTable; i++)
        {
            InMeshInstanceRegister::MeshInstanceGroup group = tableGroups[i];
            if (!group.mesh)
                continue;

            uint startIndex = indexRenderer;

            uint countRenderers = group.group->instances.getPosBuffer();
            InMeshRenderer **renderers = group.group->instances.getMemory(0);
            for (uint i2 = 0; i2 < countRenderers; i2++)
            {
                InMeshRenderer *renderer = renderers[i2];
                if (!renderer)
                    continue;

                RendererCullingData data;
                data.obb = renderer->getGlobalOBB();
                data.renderer = renderer;
                cullingRenderers[indexRenderer] = data;
                indexRenderer++;
            }

            if (indexRenderer != startIndex)
            {
                CullingGroup cullingGroup;
                cullingGroup.countRenderers = indexRenderer - startIndex;
                cullingGroup.inData = cullingRenderers + startIndex;
                cullingGroup.outResults = cullingResults + startIndex;

                rendererCulling->addCullingGroup(cullingGroup);
            }
        }
        rendererCulling->runCulling(_camera);
    }

    void InMeshRendererCullingManager::updateInstancingGenerator(InInstancingGenerator *_generator, vk::VulkanTransferCommands *_transferCommands)
    {
        if (rendererCulling->isLaunchCulling())
            rendererCulling->fetch();

        _generator->cleanLastDrawCalls();

        uint countGroups = rendererCulling->getCountGroups();
        if (!countGroups)
            return;

        _generator->beginInstances(_transferCommands);
        for (uint i = 0; i < countGroups; i++)
        {
            bufferTempRenderers.clearMemory();

            CullingGroup group = rendererCulling->getCullingGroup(i);
            for (uint i2 = 0; i2 < group.countRenderers; i2++)
            {
                if (!group.outResults[i2])
                    continue;

                GeRenderer *renderer = group.inData[i2].renderer;
                
                bufferTempRenderers.assignmentMemoryAndCopy(&renderer);
            }
            if (bufferTempRenderers.getPos())
                _generator->addRenderers(bufferTempRenderers.getPos(), bufferTempRenderers.getMemory(0), _transferCommands);
        }
        _generator->endInstances(_transferCommands);
    }
}