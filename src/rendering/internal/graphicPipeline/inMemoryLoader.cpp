#include "rendering/internal/graphicPipeline/inMemoryLoader.h"

namespace phys::ge
{
    InMemoryLoader::InMemoryLoader() : device(NULL), transferCommands(NULL), receiver(NULL), fenceLoading()
    {
    }

    void InMemoryLoader::release()
    {
        vk::VulkanFence::destroyFence(&fenceLoading);
        PhBase::release();
    }

    void InMemoryLoader::init()
    {
        vk::VulkanFence::createFence(device, &fenceLoading, false);
    }

    void InMemoryLoader::loadResourceScene(InScene *_scene, vk::VulkanSemaphore _signalEndLoading, bool *_needWaitLoading)
    {
        InMeshManager *meshManager = _scene->meshManager;

        *_needWaitLoading = false;
        bool endSend = false;
        bool wasWrite = false;
        bool endFreeSpace = false;
        while (true)
        {
            meshManager->sendData(transferCommands, &endSend, &wasWrite, &endFreeSpace);
            if (endSend)
            {
                if (wasWrite)
                {
                    transferCommands->setSignalSemaphoreCmd(_signalEndLoading);
                    *_needWaitLoading = true;
                }
                break;
            }

            if (endFreeSpace)
            {
                fenceLoading.reset();
                receiver->submitCommands(transferCommands, fenceLoading);
                fenceLoading.wait();
                transferCommands->resetCommandBuffer();
            }

            endSend = wasWrite = endFreeSpace = false;
        }
    }
}