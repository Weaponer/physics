#include <MTH/boundOBB.h>
#include <MTH/plane.h>
#include <MTH/closestPoint.h>
#include <MTH/bounds.h>

#include "rendering/internal/graphicPipeline/inRendererCulling.h"

#include "common/phMacros.h"

#include "threads/safePrint.h"

namespace phys::ge
{
    // task
    InRendererCulling::CullingTask::CullingTask() : PhTask(), group({}), cameraData({})
    {
    }

    InRendererCulling::CullingTask::~CullingTask()
    {
    }

    void InRendererCulling::CullingTask::setData(const CullingGroup &_group, const CameraCullingData &_cameraData)
    {
        group = _group;
        cameraData = _cameraData;
    }

    void InRendererCulling::CullingTask::complete(int _instance)
    {
        mth::boundOBB obb = group.inData[_instance].obb;
        bool result = false;
        if (mth::test_OBB_OBB(obb, cameraData.obbCamera))
        {
            for (int i = 0; i < 6; i++)
            {
                if (!mth::test_OBB_Plane(obb, cameraData.clipPlanes[i]))
                    goto FINISH;
            }
            result = true;
        }
    FINISH:
        group.outResults[_instance] = result;
    }

    // renderer culling
    InRendererCulling::InRendererCulling() : cullingGroups(), bufferTasks(), countUseTasks(0), isLaunchTasks(false), threadsManager(NULL)
    {
    }

    void InRendererCulling::release()
    {
        cullingGroups.release();
        uint countTasks = bufferTasks.getPos();
        for (uint i = 0; i < countTasks; i++)
            delete *bufferTasks.getMemory(i);
        bufferTasks.release();
    }

    void InRendererCulling::init(PhThreadsManager *_threadsManager)
    {
        threadsManager = _threadsManager;
    }

    void InRendererCulling::clearGroups()
    {
        cullingGroups.clearMemory();
    }

    void InRendererCulling::addCullingGroup(CullingGroup _group)
    {
        PH_ASSERT(!isLaunchTasks, "Culling already launch.");
        cullingGroups.assignmentMemoryAndCopy(&_group);
    }

    void InRendererCulling::runCulling(const GeCamera *_camera)
    {
        PH_ASSERT(!isLaunchTasks, "Culling already launch.");
        CameraCullingData cameraCulling;
        mth::quat rot = _camera->getRotation();
        mth::vec3 pos = _camera->getPosition();
        mth::vec3 forward = rot.rotateVector(mth::vec3(0, 0, 1));
        mth::vec3 nearPoint = pos + forward * _camera->getNear();
        mth::vec3 farPoint = pos + forward * _camera->getFar();
        const mth::plane *localPlanes = _camera->getClippingPlanes();
        cameraCulling.clipPlanes[1] = mth::plane(farPoint, forward);
        forward.invert();
        cameraCulling.clipPlanes[0] = mth::plane(nearPoint, forward);
        cameraCulling.clipPlanes[2] = mth::plane(pos, rot.rotateVector(localPlanes[2].normal));
        cameraCulling.clipPlanes[3] = mth::plane(pos, rot.rotateVector(localPlanes[3].normal));
        cameraCulling.clipPlanes[4] = mth::plane(pos, rot.rotateVector(localPlanes[4].normal));
        cameraCulling.clipPlanes[5] = mth::plane(pos, rot.rotateVector(localPlanes[5].normal));
        cameraCulling.obbCamera = _camera->calculateGlobalOBB();

        uint countGroups = cullingGroups.getPos();

        if (!countGroups)
            return;

        for (uint i = 0; i < countGroups; i++)
        {
            CullingGroup group = *cullingGroups.getMemory(i);
            CullingTask *task = getFreeTask();
            task->setData(group, cameraCulling);
            threadsManager->addTask(task, group.countRenderers);
        }
        isLaunchTasks = true;
    }

    void InRendererCulling::fetch()
    {
        PH_ASSERT(countUseTasks, "Culling don`t launch.");

        for (uint i = 0; i < countUseTasks; i++)
        {
            CullingTask *task = *bufferTasks.getMemory(i);
            threadsManager->waitTask(task);
        }
        countUseTasks = 0;
        isLaunchTasks = false;
    }

    InRendererCulling::CullingTask *InRendererCulling::getFreeTask()
    {
        CullingTask *task = NULL;
        if (countUseTasks >= bufferTasks.getPos())
        {
            task = new CullingTask();
            bufferTasks.assignmentMemoryAndCopy(&task);
        }
        else
        {
            task = *bufferTasks.getMemory(countUseTasks);
        }
        countUseTasks++;
        return task;
    }
}