#include <MTH/matrix.h>
#include <MTH/glGen_matrix.h>

#include "rendering/geEntity.h"

namespace phys::ge
{
    const mth::mat4 &GeEntity::getTRS()
    {
        checkAndRecalculateTRS();
        return TRS;
    }

    const mth::mat4 &GeEntity::getInverseTRS()
    {
        checkAndRecalculateTRS();
        return inverseTRS;
    }

    void GeEntity::checkAndRecalculateTRS()
    {
        if (wasUpdate)
        {
            wasUpdate = false;
            calculateMatrixTRS();
            onRecalculateTRS();
        }
    }

    void GeEntity::calculateMatrixTRS()
    {

        TRS = mth::TRS(position, rotation, size);
        inverseTRS = mth::inverseTRS(TRS);
    }
}