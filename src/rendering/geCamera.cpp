#include <MTH/matrix.h>
#include <MTH/glGen_matrix.h>
#include <MTH/vectors.h>

#include "rendering/geCamera.h"

namespace phys::ge
{
    GeCamera::GeCamera() : GeEntity(GeEntity::GeEntity_Camera, GeEntity::GeEntityNoneResizable)
    {
        setCameraSetting(0.1f, 250, 70.0f, 16.0f / 9.0f);
    }

    void GeCamera::setCameraSetting(float _near, float _far, float _fov, float _aspect)
    {
        fovTangent = tan((0.5f * _fov) * RADIAN);
        float y = 1.0f / fovTangent;
        float x = y / _aspect;
        float A = _far / (_far - _near);
        float B = _near * A;

        projectionMatrix = mth::mat4(x, 0.0f, 0.0f, 0.0f,
                                     0.0f, -y, 0.0f, 0.0f,
                                     0.0f, 0.0f, A, -B,
                                     0.0f, 0.0f, 1.0f, 0.0f);

        inverseProjectionMatrix = mth::mat4(1.0f / x, 0.0f, 0.0f, 0.0f,
                                            0.0f, -1.0f / y, 0.0f, 0.0f,
                                            0.0f, 0.0f, 0.0f, 1.0f,
                                            0.0f, 0.0f, -1.0f / A, 1.0f / B);

        clippingPlanes[0] = mth::plane(-_near, mth::vec3(0, 0, -1)); // rear
        clippingPlanes[1] = mth::plane(_far, mth::vec3(0, 0, 1));    // front

        float ab = _far * tan(_fov * RADIAN / 2);
        mth::vec2 rightP = mth::vec2(ab * _aspect, _far);
        rightP = rightP.perpendecular();
        rightP.normalise();

        mth::vec2 leftP = mth::vec2(-rightP.x, rightP.y);

        clippingPlanes[2] = mth::plane(0, mth::vec3(leftP.x, 0, -leftP.y));   // left
        clippingPlanes[3] = mth::plane(0, mth::vec3(rightP.x, 0, -rightP.y)); // right

        mth::vec2 bottomP = mth::vec2(-ab, _far);
        bottomP = bottomP.perpendecular();
        bottomP.normalise();

        mth::vec2 topP = mth::vec2(-bottomP.x, bottomP.y);

        clippingPlanes[4] = mth::plane(0, mth::vec3(0, topP.x, topP.y));       // top
        clippingPlanes[5] = mth::plane(0, mth::vec3(0, bottomP.x, bottomP.y)); // bottom

        float abN = _near * tan(_fov * RADIAN / 2);

        pointsPiramidClipping[0] = mth::vec3(abN * _aspect, abN, _near);
        pointsPiramidClipping[1] = mth::vec3(-abN * _aspect, abN, _near);
        pointsPiramidClipping[2] = mth::vec3(abN * _aspect, -abN, _near);
        pointsPiramidClipping[3] = mth::vec3(-abN * _aspect, -abN, _near);

        pointsPiramidClipping[4] = mth::vec3(ab * _aspect, ab, _far);
        pointsPiramidClipping[5] = mth::vec3(-ab * _aspect, ab, _far);
        pointsPiramidClipping[6] = mth::vec3(ab * _aspect, -ab, _far);
        pointsPiramidClipping[7] = mth::vec3(-ab * _aspect, -ab, _far);

        mth::vec3 origin = mth::vec3(0.0f, 0.0f, (_far - _near) / 2.0f + _near);
        mth::vec3 sizeBound = mth::vec3(ab * _aspect, ab, (_far - _near) / 2.0f);

        boundCamera = mth::boundAABB(origin, sizeBound);
    }

    mth::boundOBB GeCamera::calculateGlobalOBB() const
    {
        mth::vec3 worldPosition = position + rotation.rotateVector(boundCamera.origin);
        return mth::boundOBB(worldPosition, mth::matrix3FromMatrix4(rotation.getMatrix()), boundCamera.size);
    }

    void GeCamera::release()
    {
        GeEntity::release();
    }
}