#include "rendering/geShaderProvider.h"
#include "rendering/internal/inShaderProvider.h"

#include "common/phMacros.h"

namespace phys::ge
{
    GeShaderProvider *GeShaderProvider::craeteShaderProvider(vk::VulkanDevice *_device, PhResourceProvider *_provider)
    {
        InShaderProvider *shaderProvider = new (phAllocateMemory(sizeof(InShaderProvider))) InShaderProvider(_provider, _device);
        return shaderProvider;
    }

    void GeShaderProvider::destroyShaderProvider(GeShaderProvider *_provider)
    {
        PH_RELEASE(_provider);
    }
}