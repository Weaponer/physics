#include "rendering/geFoundation.h"
#include "common/phMacros.h"
#include "foundation/phMemoryAllocator.h"

#include "rendering/internal/graphicPipeline/inMeshManager.h"
#include "rendering/internal/inScene.h"

namespace phys::ge
{
    void GeFoundation::release()
    {
        PH_RELEASE(inSceneRenderer);
        PH_RELEASE(graphicPipeline);
    }

    GeFoundation *GeFoundation::createGeFoundation(PhThreadsManager *_threadsManager, GPUFoundation *_gpuFoundation,
                                                   GeShaderProvider *_shaderProvider, Window *_window, vk::VulkanQueue *_renderingComputeQueue)
    {
        GeFoundation *geFoundation = new (phAllocateMemory(sizeof(GeFoundation))) GeFoundation();
        geFoundation->threadsManager = _threadsManager;
        geFoundation->gpuFoundation = _gpuFoundation;
        geFoundation->shaderProvider = _shaderProvider;
        geFoundation->window = _window;
        geFoundation->renderingComputeQueue = _renderingComputeQueue;

        geFoundation->graphicPipeline = new (phAllocateMemory(sizeof(InGraphicPipeline))) InGraphicPipeline();
        geFoundation->graphicPipeline->threadsManager = _threadsManager;
        geFoundation->graphicPipeline->swapchain = geFoundation->window->getSwapchain();
        geFoundation->graphicPipeline->graphicQueue = _window->getVulkanQueue();
        geFoundation->graphicPipeline->computeQueue = _renderingComputeQueue;
        geFoundation->graphicPipeline->bufferAllocator = _gpuFoundation->getBufferAllocator();
        geFoundation->graphicPipeline->imageAllocator = _gpuFoundation->getImageAllocator();
        geFoundation->graphicPipeline->commandReceiver = _gpuFoundation->getCommandReceiver();
        geFoundation->graphicPipeline->shaderProvider = _shaderProvider;
        geFoundation->graphicPipeline->init();

        geFoundation->inSceneRenderer = new (phAllocateMemory(sizeof(InSceneRenderer))) InSceneRenderer();
        geFoundation->inSceneRenderer->graphicPipeline = geFoundation->graphicPipeline;
        return geFoundation;
    }

    void GeFoundation::destroyGeFoundation(GeFoundation *_geFoundation)
    {
        PH_RELEASE(_geFoundation);
    }

    GeScene *GeFoundation::createScene()
    {
        InMeshManager *meshManager = new (phAllocateMemory(sizeof(InMeshManager))) InMeshManager();
        meshManager->init(gpuFoundation->getBufferAllocator(), window->getVulkanQueue()->getFamilyIndex());
        InScene *inScene = new (phAllocateMemory(sizeof(InScene))) InScene(meshManager);
        return inScene;
    }

    void GeFoundation::destroy(GeScene *_scene)
    {
        InScene *inScene = dynamic_cast<InScene *>(_scene);
        InMeshManager *meshManager = inScene->meshManager;
        PH_RELEASE(inScene);
        PH_RELEASE(meshManager);
    }
}