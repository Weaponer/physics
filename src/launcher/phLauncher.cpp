#include <time.h>
#include <unistd.h>
#include <chrono>
#include <iostream>
#include <bitset>

#include <MTH/matrix.h>
#include <MTH/glGen_matrix.h>
#include <MTH/bounds.h>

#include "launcher/phLauncher.h"
#include "common/phMacros.h"

#include "rendering/internal/graphicPipeline/inRendererCulling.h"

#include "launcher/editor/phEditorPhysicsSimulationSettings.h"

namespace phys
{
    PhLauncher::PhLauncher() : PhBase(), foundation(NULL), physics(NULL), physicsScene(NULL),
                               resourceProvider(NULL),
                               window(NULL), imGUI(NULL), editor(NULL),
                               renderingComputeQueue(NULL), endRenderSceneSignal(), fence(),
                               geFoundation(NULL), geScene(NULL), debugLines(NULL)
    {
    }

    PhLauncher *PhLauncher::phCreateLauncher()
    {
        return new (phAllocateMemory(sizeof(PhLauncher))) PhLauncher();
    }

    void PhLauncher::phDestroyLauncher(PhLauncher *_launcher)
    {
        PH_RELEASE(_launcher)
    }

    void PhLauncher::init(PhFoundation *_foundation, PhPhysics *_physics, PhResourceProvider *_resourceProvider)
    {
        PH_ASSERT(foundation == NULL, "Launcher already initialized.");

        foundation = _foundation;
        physics = _physics;
        resourceProvider = _resourceProvider;
    }

    void PhLauncher::initRendering(GPUFoundation *_foundation, Window *_window, vk::VulkanQueue *_renderingComputeQueue)
    {
        PH_ASSERT(window == NULL, "Launcher rendering already initialized.");
        gpuFoundation = _foundation;
        window = _window;
        imGUI = new PhWindowImGUI();
        window->connectGUI(imGUI);
        renderingComputeQueue = renderingComputeQueue;

        vk::VulkanSemaphore::createSemaphore(gpuFoundation->getDevice(), &endRenderSceneSignal);
        vk::VulkanFence::createFence(gpuFoundation->getDevice(), &fence, false);

        geShaderProvider = ge::GeShaderProvider::craeteShaderProvider(gpuFoundation->getDevice(), resourceProvider);
        geFoundation = ge::GeFoundation::createGeFoundation(foundation->getThreadsManager(), gpuFoundation, geShaderProvider, window, renderingComputeQueue);
    }

    void PhLauncher::launch()
    {
        const float lockFPS = 144.0f;
        float deltaTime = 0.0f;

        createScene();

        float midleDeltaTime = 1.0f / lockFPS;
        int countSamples = 1;
        std::chrono::_V2::system_clock::time_point start, end;
        float lock = 1.0f / lockFPS;
        while (true)
        {
            fence.reset();
            start = std::chrono::high_resolution_clock::now();
            if (!window->updateEvents())
                break;

            uint indexImage = 0;
            if (!window->beginRendering(&indexImage))
                break;

            geScene->updateScene(deltaTime);

            imGUI->beginImGui();
            editor->update(deltaTime);
            imGUI->endImGui();

            PhEditorPhysicsSimulationSettings physicsSimulation = editor->getPhysicsSimulationSettings();
            bool simulatePhys = physicsSimulation.doOneStep | physicsSimulation.simulate;
            float physTime = physicsSimulation.useDeltaTime ? deltaTime : physicsSimulation.timeStep;

            drawOutDebugPhysics();

            if (simulatePhys)
                physicsScene->simulation(physTime, physicsSimulation.simulationSettings);

            debugLines->drawLine(mth::vec3(0, 4.0f, 0.0f), mth::vec3(1, 4.0f, 0.0f), mth::col(1.0f, 0.0f, 0.0f));
            debugLines->drawLine(mth::vec3(0, 4.0f, 0.0f), mth::vec3(0, 5.0f, 0.0f), mth::col(0.0f, 1.0f, 0.0f));
            debugLines->drawLine(mth::vec3(0, 4.0f, 0.0f), mth::vec3(0, 4.0f, 1.0f), mth::col(0.0f, 0.0f, 1.0f));
            // debugLines->drawCube(mth::vec3(0, 1.0f, 10.0f), mth::vec3(2.0f, 2.0f, 2.0f));

            geFoundation->getSceneRenderer()->renderScene(geScene, indexImage,
                                                          window->getWaitBeginRenderingSemaphore(), endRenderSceneSignal);

            imGUI->drawFrame(endRenderSceneSignal, window->getSignalEndRenderingSemaphore(), fence);
            if (!window->endRendering())
                break;
            
            fence.wait();

            if (simulatePhys)
            {
                // std::chrono::_V2::system_clock::time_point pstart, pend;
                // pstart = std::chrono::high_resolution_clock::now();
                physicsScene->fetch();
                // pend = std::chrono::high_resolution_clock::now();
                // float pdeltaTime = std::chrono::duration_cast<std::chrono::microseconds>(pend - pstart).count() / 1000000.0f;
                // std::cout << "Physics fetch delta time: " << pdeltaTime << std::endl;
            }

            end = std::chrono::high_resolution_clock::now();
            deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000000.0f;

            midleDeltaTime += deltaTime;
            countSamples++;

            // std::cout << deltaTime << " | " << (midleDeltaTime / countSamples) << std::endl;

            if (lock > deltaTime)
            {
                usleep((lock - deltaTime) * 1000);
                deltaTime += (lock - deltaTime);
            }
        }
    }

    void PhLauncher::release()
    {
        if (editor)
            PH_RELEASE(editor);

        if (physicsScene)
            physics->destroyScene(physicsScene);

        if (debugLines)
            geScene->destroyDebugLineRenderer(debugLines);

        if (geScene)
            geFoundation->destroy(geScene);

        if (geFoundation)
            ge::GeFoundation::destroyGeFoundation(geFoundation);

        if (geShaderProvider)
            ge::GeShaderProvider::destroyShaderProvider(geShaderProvider);

        if (endRenderSceneSignal.isValid())
            vk::VulkanSemaphore::destroySemaphore(&endRenderSceneSignal);

        if (fence.isValid())
            vk::VulkanFence::destroyFence(&fence);

        if (imGUI)
        {
            window->disconnectGUI(imGUI);
            delete imGUI;
        }
    }

    void PhLauncher::createScene()
    {
        geScene = geFoundation->createScene();
        ge::GeSceneSettings settingsScene;
        settingsScene.isSelectableObjects = true;
        geScene->setSettings(settingsScene);
        geScene->getSkyBoxRenderer()->setSkyColor(mth::col(0.11f, 0.11f, 0.11f));

        debugLines = geScene->createDebugLineRenderer();

        physicsScene = physics->createScene();

        editor = new (phAllocateMemory(sizeof(PhEditor))) PhEditor();
        PhEditorInitContext contextEditor;
        contextEditor.scene = geScene;
        contextEditor.window = window;
        contextEditor.resourceProvider = resourceProvider;
        contextEditor.physics = physics;
        contextEditor.physicsScene = physicsScene;

        editor->init(contextEditor);
    }

    void PhLauncher::drawOutDebugPhysics()
    {
    }
}