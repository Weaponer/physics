#include "launcher/editor/phEditorObjectManagerSystem.h"

#include "common/phMacros.h"

#include "launcher/editor/phEditorScenePickerSystem.h"
#include "launcher/editor/phEditorSignals.h"

namespace phys
{
    PhEditorObjectManagerSystem::PhEditorObjectManagerSystem()
        : PhEditorSystem(), objects(), sphereMesh(NULL), cubeMesh(NULL), capsuleMesh(NULL), physicsManager(NULL)
    {
        PhEditorObject *def = NULL;
        objects.setUseDefault(true);
        objects.setDefault(&def);
    }

    void PhEditorObjectManagerSystem::release()
    {
        objects.release();
    }

    PhEditorObject *PhEditorObjectManagerSystem::createEditorObject(PhFlags _type)
    {
        ge::GeSelectableRenderer *renderer = NULL;
        const char *name = "Default";
        switch (_type)
        {
        case PhEditorObject_Cube:
        {
            ge::GeMeshRenderer *meshR = scene->createMeshRenderer();
            meshR->setMesh(cubeMesh);
            renderer = meshR;
            name = "Cube";
            break;
        }
        case PhEditorObject_Sphere:
        {
            ge::GeMeshRenderer *meshR = scene->createMeshRenderer();
            meshR->setMesh(sphereMesh);
            renderer = meshR;
            name = "Sphere";
            break;
        }
        case PhEditorObject_Capsule:
        {
            ge::GeMeshRenderer *meshR = scene->createMeshRenderer();
            meshR->setMesh(capsuleMesh);
            renderer = meshR;
            name = "Capsule";
            break;
        }
        case PhEditorObject_Floor:
        {
            renderer = scene->createFloorRenderer();
            name = "Floor";
            break;
        }
        }

        uint index = 0;
        PhEditorObject **memPos = objects.assignmentMemory(index);
        PhEditorObject *editorObject = new (phAllocateMemory(sizeof(PhEditorObject))) PhEditorObject(renderer, index);
        *memPos = editorObject;

        editorObject->setName(name);
        context->get<PhCreateObjectSig>()->signal(editorObject);
        
        physicsManager->setRigidDynamic(editorObject);
        physicsManager->addShape(editorObject);

        if (_type == PhEditorObject_Cube)
            physicsManager->addBox(editorObject, 0);
        else if (_type == PhEditorObject_Capsule)
            physicsManager->addCapsule(editorObject, 0);
        else if (_type == PhEditorObject_Floor)
            physicsManager->addPlane(editorObject, 0);
        else
            physicsManager->addSphere(editorObject, 0);

        return editorObject;
    }

    void PhEditorObjectManagerSystem::destroyEditorObject(PhEditorObject *_object)
    {
        context->get<PhDestroyObjectSig>()->signal(_object);

        objects.popMemory(_object->getIndex());
        destroyObject(_object);
    }

    void PhEditorObjectManagerSystem::onInit()
    {
        scene = context->get<ge::GeScene>();
        resourceProvider = context->get<PhResourceProvider>();
        scenePickerSystem = context->get<PhEditorScenePickerSystem>();

        PhObjectMesh *cubeObj = resourceProvider->loadObjectMesh("cube.obj");
        cubeMesh = scene->createMesh(cubeObj->getMesh(0));
        PhObjectMesh *sphereObj = resourceProvider->loadObjectMesh("sphere.obj");
        sphereMesh = scene->createMesh(sphereObj->getMesh(0));
        PhObjectMesh *capsuleObj = resourceProvider->loadObjectMesh("capsule.obj");
        capsuleMesh = scene->createMesh(capsuleObj->getMesh(0));

        physicsManager = context->get<PhEditorPhysicsManagerSystem>();
    }

    void PhEditorObjectManagerSystem::onUpdate(float _deltaTime)
    {
    }

    void PhEditorObjectManagerSystem::onCleanup()
    {
        uint count = objects.getPosBuffer();
        for (uint i = 0; i < count; i++)
        {
            PhEditorObject *object = *objects.getMemory(i);
            if (object != NULL)
                destroyObject(object);
        }

        scene->destroyMesh(capsuleMesh);
        scene->destroyMesh(sphereMesh);
        scene->destroyMesh(cubeMesh);
    }

    void PhEditorObjectManagerSystem::destroyObject(PhEditorObject *_object)
    {
        physicsManager->destroyRigidBody(_object);

        PhFlags type = _object->getRenderer()->getTypeEntity();
        switch (type)
        {
        case ge::GeEntity::GeEntity_MeshRenderer:
            scene->destroyMeshRenderer(dynamic_cast<ge::GeMeshRenderer *>(_object->getRenderer()));
            break;
        case ge::GeEntity::GeEntity_FloorRenderer:
            ge::GeFloorRenderer *floor = dynamic_cast<ge::GeFloorRenderer *>(_object->getRenderer());
            scene->destroyFloorRenderer(floor);
            break;
        }

        PH_RELEASE(_object);
    }
}