#include "launcher/editor/phEditorObject.h"

namespace phys
{
    PhEditorObject::PhEditorObject(ge::GeSelectableRenderer *_renderer, uint _indexRegister)
        : PhBase(),
          name(NULL), renderer(_renderer), indexRegister(_indexRegister), rigidBody(NULL)
    {
        name = new std::string();
    }

    void PhEditorObject::release()
    {
        delete name;
        name = NULL;
    }

    void PhEditorObject::syncWithRigidbody()
    {
        renderer->setPosition(rigidBody->getPosition());
        renderer->setRotation(rigidBody->getRotation());
    }

    const char *PhEditorObject::getName() const
    {
        return name->c_str();
    }

    void PhEditorObject::setName(const char *_name)
    {
        *name = std::string(_name);
    }

    void PhEditorObject::setRigidBody(PhRigidBody *_actor)
    {
        rigidBody = _actor;
        if (rigidBody)
        {
            rigidBody->setPosition(getPosition());
            rigidBody->setRotation(getRotation());
        }
    }

    void PhEditorObject::setPosition(mth::vec3 _position)
    {
        renderer->setPosition(_position);
        if (rigidBody)
            rigidBody->setPosition(_position);
    }

    void PhEditorObject::setRotation(mth::quat _rotation)
    {
        renderer->setRotation(_rotation);
        if (rigidBody)
            rigidBody->setRotation(_rotation);
    }
}