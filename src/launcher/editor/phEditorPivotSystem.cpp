#include "launcher/editor/phEditorPivotSystem.h"

#include "plugins/imgui/imgui.h"

namespace phys
{
    PhEditorPivotSystem::PhEditorPivotSystem()
        : PhEditorSystem(),
          startState({}),
          selectConnection(), unselectConnection(), controlEditorObject(NULL), pivotMode(ge::GePivotRenderer::PivotState_None),
          pivotMouseControl(), pivotRenderer(NULL)
    {
    }

    void PhEditorPivotSystem::release()
    {
        pivotMouseControl.release();
    }

    void PhEditorPivotSystem::onInit()
    {
        window = context->get<Window>();
        scene = context->get<ge::GeScene>();
        mouseState = context->get<PhEditorMouseState>();

        pivotRenderer = scene->createPivotRenderer();
        pivotRenderer->setScaleOffsets(mth::vec3(1.0f, 1.0f, 1.0f));
        pivotRenderer->setPosition(mth::vec3(1, 1, 1));
        mth::quat rot = mth::quat();
        rot.setEuler(0.0f, 0.0f, 0.0f);
        pivotRenderer->setRotation(rot);

        pivotMouseControl.initPivot(pivotRenderer);

        selectConnection = context->get<PhSelectObjectSig>()->bind(this, &PhEditorPivotSystem::onSelectObject);
        unselectConnection = context->get<PhUnselectObjectSig>()->bind(this, &PhEditorPivotSystem::onUnselectObject);
        pivotModeConnection = context->get<PhChangePivotModeSig>()->bind(this, &PhEditorPivotSystem::onChangePivotMode);
    }

    void PhEditorPivotSystem::onSelectObject(PhEditorObject *_object)
    {
        controlEditorObject = _object;
        pivotRenderer->setPivotState(pivotMode);
        startState.startPosition = _object->getPosition();
        startState.startRotate = _object->getRotation();
        startState.startScale = _object->getSize();
        pivotRenderer->setPosition(startState.startPosition);
        pivotRenderer->setRotation(startState.startRotate);
    }

    void PhEditorPivotSystem::onUnselectObject()
    {
        controlEditorObject = NULL;
        pivotRenderer->setPivotState(ge::GePivotRenderer::PivotState_None);
    }

    void PhEditorPivotSystem::onChangePivotMode(PhFlags _mode)
    {
        pivotMode = _mode;
        if (controlEditorObject)
            pivotRenderer->setPivotState(pivotMode);
    }

    void PhEditorPivotSystem::onUpdate(float _deltaTime)
    {
        if (controlEditorObject == NULL || pivotRenderer->getPivotState() == ge::GePivotRenderer::PivotState_None)
            return;

        ImVec2 mousePos = ImGui::GetMousePos();
        mth::vec2 normalizedPos = mth::vec2(mousePos.x / window->getWidth(), mousePos.y / window->getHeight());
        bool mouseOutside = false;
        if (normalizedPos.x < 0.0 || normalizedPos.x > 1.0f || normalizedPos.y < 0.0 || normalizedPos.y > 1.0f)
        {
            mouseOutside = true;
            normalizedPos.x = std::max(0.0f, std::min(1.0f, normalizedPos.x));
            normalizedPos.y = std::max(0.0f, std::min(1.0f, normalizedPos.y));
        }

        // in the begining
        if (!pivotMouseControl.getIsLock())
        {
            pivotRenderer->setPosition(controlEditorObject->getPosition());
            pivotRenderer->setRotation(controlEditorObject->getRotation());
        }

        if (ImGui::IsMouseReleased(ImGuiMouseButton_Left) && pivotMouseControl.getIsLock())
        {
            unlockPivot();
        }

        // update
        bool mouseStateLocked = mouseState->tryLock(PhEditorMouseState::PhEditorMouseState_ObjectPivot, PhEditorMouseState::PhEditorMouseState_ObjectPivot);

        if (mouseStateLocked)
        {
            if (mouseOutside && !pivotMouseControl.getIsLock())
            {
                pivotMouseControl.updatePivotBackground(scene->getActiveCamera());
            }
            else
            {
                pivotMouseControl.updatePivot(scene->getActiveCamera(), normalizedPos);
            }

            if (ImGui::IsMouseDown(ImGuiMouseButton_Left) && !pivotMouseControl.getIsLock())
                tryLockPivot();
        }
        else
        {
            pivotMouseControl.updatePivotBackground(scene->getActiveCamera());
        }

        if (mouseStateLocked && !pivotMouseControl.getIsLock())
            mouseState->tryLock(PhEditorMouseState::PhEditorMouseState_ObjectPivot, PhEditorMouseState::PhEditorMouseState_None);

        // in the end
        if (pivotMouseControl.getIsLock())
        {
            controlEditorObject->setPosition(pivotRenderer->getPosition());
            controlEditorObject->setRotation(pivotRenderer->getRotation());
            controlEditorObject->setSize(startState.startScale.componentProduct(pivotRenderer->getScaleOffsets()));
        }
    }

    void PhEditorPivotSystem::onCleanup()
    {
        context->get<PhSelectObjectSig>()->unbind(selectConnection);
        context->get<PhUnselectObjectSig>()->unbind(unselectConnection);
        context->get<PhChangePivotModeSig>()->unbind(pivotModeConnection);
        scene->destroyPivotRenderer(pivotRenderer);
    }

    bool PhEditorPivotSystem::tryLockPivot()
    {
        // pivot was locked
        startState.startPosition = controlEditorObject->getPosition();
        startState.startRotate = controlEditorObject->getRotation();
        startState.startScale = controlEditorObject->getSize();

        return pivotMouseControl.tryLockPivot();
    }

    void PhEditorPivotSystem::unlockPivot()
    {
        pivotMouseControl.unlockPivot();
        pivotMouseControl.resetPivot();
    }

}