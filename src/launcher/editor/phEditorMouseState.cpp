#include "launcher/editor/phEditorMouseState.h"

namespace phys
{
    bool PhEditorMouseState::tryLock(PhFlags srcState, PhFlags dstState)
    {
        if (lockStage == PhEditorMouseState_None || lockStage == srcState)
        {
            lockStage = dstState;
            return true;
        }
        else
        {
            return false;
        }
    }
}