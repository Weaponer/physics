#include <MTH/vectors.h>

#include "rendering/geMeshRenderer.h"
#include "rendering/geFloorRenderer.h"

#include "launcher/editor/gui/phEditorInspectorWindow.h"
#include "launcher/editor/phEditorSignals.h"

#include "plugins/imgui/imgui.h"

namespace phys
{
    PhEditorInspectorWindow::PhEditorInspectorWindow() : PhEditorGuiWindow(),
                                                         selectConnection(), unselectConnection(),
                                                         physics(NULL), physicsManager(NULL),
                                                         object(NULL), safeEulerAngles(),
                                                         safeGeometryEulers()
    {
    }

    void PhEditorInspectorWindow::release()
    {
        context->get<PhSelectObjectSig>()->unbind(selectConnection);
        context->get<PhUnselectObjectSig>()->unbind(unselectConnection);

        safeGeometryEulers.release();
        PhEditorGuiWindow::release();
    }

    void PhEditorInspectorWindow::onInit()
    {
        selectConnection = context->get<PhSelectObjectSig>()->bind(this, &PhEditorInspectorWindow::onSelectObject);
        selectConnection = context->get<PhUnselectObjectSig>()->bind(this, &PhEditorInspectorWindow::onUnselectObject);

        physics = context->get<PhPhysics>();
        physicsManager = context->get<PhEditorPhysicsManagerSystem>();
    }

    void PhEditorInspectorWindow::onSelectObject(PhEditorObject *_object)
    {
        object = _object;
    }

    void PhEditorInspectorWindow::onUnselectObject()
    {
        object = NULL;
    }

    static bool isNumbersEqual(float _a, float _b, int _offset)
    {
        int a = (int)(_a * _offset);
        int b = (int)(_b * _offset);
        return a == b;
    }

    static bool drawGetFloat(const char *_name, const char *_labelName, float _mainOffset, float _offset, float _val, float *_out)
    {
        ImGui::NewLine();
        ImGui::SameLine(_mainOffset);
        ImGui::TextUnformatted(_name);
        ImGui::SameLine(_offset);
        if (ImGui::DragFloat(_labelName, _out, 0.01f))
        {
            return true;
        }
        return false;
    }

    static bool drawGetVector3(const char *_name, const char *_labelName, float _mainOffset, float _offset, const mth::vec3 &_vec, mth::vec3 *_out)
    {
        ImGui::NewLine();
        ImGui::SameLine(_mainOffset);
        float vector[3] = {_vec.x, _vec.y, _vec.z};
        ImGui::TextUnformatted(_name);
        ImGui::SameLine(_offset);
        if (ImGui::DragFloat3(_labelName, vector, 0.01f))
        {
            _out->x = vector[0];
            _out->y = vector[1];
            _out->z = vector[2];
            return true;
        }
        return false;
    }

    static bool drawGetQuaternionAsEuler(const char *_name, const char *_labelName, float _mainOffset, float _offset, mth::vec3 *_safeEuler, const mth::quat &_rot, mth::quat *_out)
    {
        ImGui::NewLine();
        ImGui::SameLine(_mainOffset);
        mth::quat testQuat;
        testQuat.setEuler(_safeEuler->x, _safeEuler->y, _safeEuler->z);
        int floatOffset = 10000;
        if (!(isNumbersEqual(_rot.x, testQuat.x, floatOffset) &&
              isNumbersEqual(_rot.y, testQuat.y, floatOffset) &&
              isNumbersEqual(_rot.z, testQuat.z, floatOffset) &&
              isNumbersEqual(_rot.w, testQuat.w, floatOffset)))
        {
            *_safeEuler = _rot.getEuler();
        }

        float rotate[3] = {_safeEuler->x, _safeEuler->y, _safeEuler->z};
        ImGui::TextUnformatted(_name);
        ImGui::SameLine(_offset);
        if (ImGui::DragFloat3(_labelName, rotate))
        {
            _safeEuler->x = rotate[0];
            _safeEuler->y = rotate[1];
            _safeEuler->z = rotate[2];

            _out->setEuler(rotate[0], rotate[1], rotate[2]);
            return true;
        }
        return false;
    }

    void PhEditorInspectorWindow::onDraw()
    {
        if (!object)
            return;

        float nameOffset = ImGui::CalcTextSize("Position").x * 1.8f;
        drawName(nameOffset);
        drawTransform(nameOffset);
        drawRenderer(nameOffset);

        if (!object->getRigidBody())
        {
            drawAddRigidBody(nameOffset);
        }
        else
        {
            PhRigidBody *rigidBody = object->getRigidBody();
            if (rigidBody->getActorType() == rigidBody->PhActor_RigidStatic)
            {
                drawRigidStatic(nameOffset);
            }
            else
            {
                drawRigidDynamic(nameOffset);
            }
        }
    }

    void PhEditorInspectorWindow::drawName(float _mainNameOffset)
    {
        char resultName[256] = {0};
        const char *currentName = object->getName();
        uint32_t len = std::strlen(currentName);
        std::memcpy(resultName, currentName, std::min((int)len, 255));

        ImGui::Text("Name");
        ImGui::SameLine(_mainNameOffset);
        std::string test = "test";
        if (ImGui::InputText("###label name input", resultName, IM_ARRAYSIZE(resultName)))
        {
            object->setName(resultName);
        }
    }

    void PhEditorInspectorWindow::drawTransform(float _mainNameOffset)
    {
        ImGui::SeparatorText("Transform");
        /// transform
        { // position
            mth::vec3 pos = object->getPosition();
            if (drawGetVector3("Position", "##label position", 0, _mainNameOffset, pos, &pos))
                object->setPosition(pos);
        }
        { // rotate
            mth::quat quatRot = object->getRotation();
            if (drawGetQuaternionAsEuler("Rotate", "##label rotate", 0, _mainNameOffset, &safeEulerAngles, quatRot, &quatRot))
                object->setRotation(quatRot);
        }
        { // size
            mth::vec3 size = object->getSize();
            if (drawGetVector3("Scale", "##label scale", 0, _mainNameOffset, size, &size))
            {
                size.x = std::max(0.0f, size.x);
                size.y = std::max(0.0f, size.y);
                size.z = std::max(0.0f, size.z);
                object->setSize(size);
            }
        }
    }

    void PhEditorInspectorWindow::drawRenderer(float _mainNameOffset)
    {
        ImGui::SeparatorText("Renderer");
        { // main color
            ImGui::Text("Color");
            ImGui::SameLine(_mainNameOffset);
            mth::col color = getColor(object->getRenderer());
            float colRes[3] = {color.r, color.g, color.b};
            if (ImGui::ColorEdit3("###label color", colRes))
                setColor(object->getRenderer(), mth::col(colRes[0], colRes[1], colRes[2]));
        }
        { // highlight color
            ImGui::Text("Highlight");
            ImGui::SameLine(_mainNameOffset);
            mth::col color = object->getRenderer()->getColorHighlightLine();
            float colRes[3] = {color.r, color.g, color.b};
            if (ImGui::ColorEdit3("###label highlight", colRes))
                object->getRenderer()->setColorHighlightLine(mth::col(colRes[0], colRes[1], colRes[2]));
        }
    }

    void PhEditorInspectorWindow::drawAddRigidBody(float _mainNameOffset)
    {
        ImGui::SeparatorText("Add rigid body");
        if (ImGui::Button("Add dynamic"))
        {
            physicsManager->setRigidDynamic(object);
        }
        ImGui::SameLine();
        if (ImGui::Button("Add static"))
        {
            physicsManager->setRigidStatic(object);
        }
    }

    void PhEditorInspectorWindow::drawRigidDynamic(float _mainNameOffset)
    {
        ImGui::SeparatorText("Rigid Dynamic");
        if (ImGui::Button("Remove"))
        {
            physicsManager->destroyRigidBody(object);
            return;
        }

        PhRigidDynamic *dynamic = dynamic_cast<PhRigidDynamic *>(object->getRigidBody());

        { // is kinematic
            ImGui::Text("Is Kinematic");
            ImGui::SameLine(_mainNameOffset);
            bool flag = (dynamic->getFlags() & PhRigidBodyDynamicFlags::RigidBodyKinematic) != 0;
            if (ImGui::Checkbox("###label isKineamtic", &flag))
            {
                dynamic->setFlags(PhRigidBodyDynamicFlags::RigidBodyKinematic, flag);
            }
        }

        drawShapes(_mainNameOffset);
    }

    void PhEditorInspectorWindow::drawRigidStatic(float _mainNameOffset)
    {
        ImGui::SeparatorText("Rigid Static");
        if (ImGui::Button("Remove"))
        {
            physicsManager->destroyRigidBody(object);
            return;
        }

        drawShapes(_mainNameOffset);
    }

    void PhEditorInspectorWindow::drawShapes(float _mainNameOffset)
    {
        uint32_t indexGeometry = 0;

        ImGui::SeparatorText("Shapes");

        PhRigidBody *rigidBody = object->getRigidBody();
        uint32_t countShapes = rigidBody->getCountShapes();

        int32_t indexDestroyShape = -1;
        for (uint32_t i = 0; i < countShapes; i++)
        {
            ImGui::PushID(i);

            ImGui::Text("Shape");
            ImGui::SameLine(ImGui::GetWindowWidth() - 30);
            if (ImGui::Button("-"))
                indexDestroyShape = i;

            ImGui::Text("Material null");

            float geometryOffsets = 20;

            int indexDestroyGeometry = -1;
            PhShape *shape = rigidBody->getShape(i);
            uint32_t countGeometries = shape->countGeometries();
            for (uint32_t i2 = 0; i2 < countGeometries; i2++)
            {
                ImGui::PushID(indexGeometry);

                PhGeometry *geometry = shape->getGeometry(i2);
                bool needDestroy = false;
                drawGeometry(geometry, indexGeometry, geometryOffsets, _mainNameOffset, &needDestroy);

                if (needDestroy)
                    indexDestroyGeometry = i2;

                ImGui::PopID();

                indexGeometry++;
            }

            if (indexDestroyGeometry >= 0)
            {
                physicsManager->removeGeometry(object, i, indexDestroyGeometry);
            }

            ImGui::NewLine();
            ImGui::SameLine(geometryOffsets);

            if (ImGui::Button("Add Sphere"))
                physicsManager->addSphere(object, i);
            ImGui::SameLine();
            if (ImGui::Button("Add Box"))
                physicsManager->addBox(object, i);
            ImGui::SameLine();
            if (ImGui::Button("Add Capsule"))
                physicsManager->addCapsule(object, i);
            ImGui::SameLine();
            if (ImGui::Button("Add Plane"))
                physicsManager->addPlane(object, i);

            ImGui::PopID();
        }

        if (indexDestroyShape != -1)
        {
            physicsManager->removeShape(object, indexDestroyShape);
        }

        if (ImGui::Button("Add shape"))
        {
            physicsManager->addShape(object);
        }
    }

    void PhEditorInspectorWindow::drawGeometry(PhGeometry *_geometry, uint32_t _index, float _offset, float _mainNameOffset, bool *_outNeedDestroy)
    {
        PhFlags type = _geometry->getGeometryType();
        const char *name;
        if (type == PhGeometry::PhGeometry_Sphere)
            name = "Sphere";
        else if (type == PhGeometry::PhGeometry_Box)
            name = "Box";
        else if (type == PhGeometry::PhGeometry_Plane)
            name = "Plane";
        else
            name = "Capsule";

        ImGui::NewLine();
        ImGui::SameLine(_offset);
        ImGui::TextUnformatted(name);
        ImGui::SameLine(ImGui::GetWindowWidth() - 30);
        if (ImGui::Button("-"))
            *_outNeedDestroy = true;

        mth::vec3 *safeEuler;
        if (safeGeometryEulers.getPos() <= _index)
        {
            safeEuler = safeGeometryEulers.assignmentMemory();
            *safeEuler = mth::vec3();
        }
        else
        {
            safeEuler = safeGeometryEulers.getMemory(_index);
        }

        _offset += 5;
        if (type == PhGeometry::PhGeometry_Sphere)
        {
            PhSphereGeometry *sphere = dynamic_cast<PhSphereGeometry *>(_geometry);
            mth::vec3 offsetGeom = sphere->getOffset();
            if (drawGetVector3("Offset", "###label offset", _offset, _mainNameOffset, offsetGeom, &offsetGeom))
                sphere->setOffset(offsetGeom);

            float radius = sphere->getRadius();
            if (drawGetFloat("Radius", "###label radius", _offset, _mainNameOffset, radius, &radius))
                sphere->setRadius(radius);
        }
        else if (type == PhGeometry::PhGeometry_Box)
        {
            PhBoxGeometry *box = dynamic_cast<PhBoxGeometry *>(_geometry);
            mth::vec3 offsetGeom = box->getOffset();
            if (drawGetVector3("Offset", "###label offset", _offset, _mainNameOffset, offsetGeom, &offsetGeom))
                box->setOffset(offsetGeom);

            mth::quat rotate = box->getRotate();
            if (drawGetQuaternionAsEuler("Rotate", "###label rotate", _offset, _mainNameOffset, safeEuler, rotate, &rotate))
                box->setRotate(rotate);

            mth::vec3 size = box->getSize();
            if (drawGetVector3("Size", "###label size", _offset, _mainNameOffset, size, &size))
                box->setSize(size);
        }
        else if (type == PhGeometry::PhGeometry_Plane)
        {
            PhPlaneGeometry *plane = dynamic_cast<PhPlaneGeometry *>(_geometry);
            float offsetGeom = plane->getOffset();
            if (drawGetFloat("Offset", "###label offset", _offset, _mainNameOffset, offsetGeom, &offsetGeom))
                plane->setOffset(offsetGeom);
        }
        else
        {
            PhCapsuleGeometry *capsule = dynamic_cast<PhCapsuleGeometry *>(_geometry);
            mth::vec3 offsetGeom = capsule->getOffset();
            if (drawGetVector3("Offset", "###label offset", _offset, _mainNameOffset, offsetGeom, &offsetGeom))
                capsule->setOffset(offsetGeom);

            mth::quat rotate = capsule->getRotate();
            if (drawGetQuaternionAsEuler("Rotate", "###label rotate", _offset, _mainNameOffset, safeEuler, rotate, &rotate))
                capsule->setRotate(rotate);

            float radius = capsule->getRadius();
            if (drawGetFloat("Radius", "###label radius", _offset, _mainNameOffset, radius, &radius))
                capsule->setRadius(radius);

            float height = capsule->getHeight();
            if (drawGetFloat("Height", "###label height", _offset, _mainNameOffset, height, &height))
                capsule->setHeight(height);
        }
    }

    mth::col PhEditorInspectorWindow::getColor(ge::GeSelectableRenderer *_object)
    {
        if (_object->getTypeEntity() == ge::GeEntity::GeEntity_MeshRenderer)
            return dynamic_cast<ge::GeMeshRenderer *>(_object)->getColor();
        else if (_object->getTypeEntity() == ge::GeEntity::GeEntity_FloorRenderer)
            return dynamic_cast<ge::GeFloorRenderer *>(_object)->getColor();
        return mth::col();
    }

    void PhEditorInspectorWindow::setColor(ge::GeSelectableRenderer *_object, const mth::col &_color)
    {
        if (_object->getTypeEntity() == ge::GeEntity::GeEntity_MeshRenderer)
            dynamic_cast<ge::GeMeshRenderer *>(_object)->setColor(_color);
        else if (_object->getTypeEntity() == ge::GeEntity::GeEntity_FloorRenderer)
            dynamic_cast<ge::GeFloorRenderer *>(_object)->setColor(_color);
    }

    const char *PhEditorInspectorWindow::onGetName()
    {
        return "Inspector";
    }
}