#include "launcher/editor/gui/phEditorObjectsViewWindow.h"
#include "launcher/editor/phEditorScenePickerSystem.h"

namespace phys
{

    PhEditorObjectsViewWindow::PhEditorObjectsViewWindow() : PhEditorGuiWindow(),
                                                             selectConnection(), unselectConnection(),
                                                             objectManagerSystem(NULL),
                                                             lastSelectedIndex(-1)
    {
    }

    void PhEditorObjectsViewWindow::release()
    {
        context->get<PhSelectObjectSig>()->unbind(selectConnection);
        context->get<PhUnselectObjectSig>()->unbind(unselectConnection);
        PhEditorGuiWindow::release();
    }

    void PhEditorObjectsViewWindow::onSelectObject(PhEditorObject *_object)
    {
        lastSelectedIndex = _object->getIndex();
    }

    void PhEditorObjectsViewWindow::onUnselectObject()
    {
        lastSelectedIndex = -1;
    }

    void PhEditorObjectsViewWindow::onInit()
    {
        objectManagerSystem = context->get<PhEditorObjectManagerSystem>();
        selectConnection = context->get<PhSelectObjectSig>()->bind(this, &PhEditorObjectsViewWindow::onSelectObject);
        selectConnection = context->get<PhUnselectObjectSig>()->bind(this, &PhEditorObjectsViewWindow::onUnselectObject);
    }

    void PhEditorObjectsViewWindow::onDraw()
    {
        int indexSelectedObject = lastSelectedIndex;

        PhEditorObject *const *table = objectManagerSystem->getTableEditorObjects();
        int size = (int)objectManagerSystem->getTableEditorObjectsSize();
        ImVec2 min = ImGui::GetWindowContentRegionMin();
        ImVec2 max = ImGui::GetWindowContentRegionMax();
        if (ImGui::BeginListBox("###label list objects", {max.x - min.x, max.y - min.y}))
        {
            for (int i = 0; i < size; i++)
            {
                PhEditorObject *object = *(table + i);
                if (object == NULL)
                    continue;

                bool selected = indexSelectedObject == i;
                ImGui::PushID(i);
                ImGui::Selectable(object->getName(), &selected);

                ImGui::PopID();
                if (selected)
                    indexSelectedObject = i;
            }
            ImGui::EndListBox();
        }

        if (lastSelectedIndex != indexSelectedObject && indexSelectedObject != -1)
        {
            context->get<PhEditorScenePickerSystem>()->selectEditorObject(*(table + indexSelectedObject));
        }
        lastSelectedIndex = indexSelectedObject;
    }

    const char *PhEditorObjectsViewWindow::onGetName()
    {
        return "Objects View";
    }
}