#include "launcher/editor/gui/phEditorGuiWindow.h"

#include "plugins/imgui/imgui.h"

namespace phys
{
    PhEditorGuiWindow::PhEditorGuiWindow() : PhBase()
    {
    }

    void PhEditorGuiWindow::release()
    {
        PhBase::release();
    }

    void PhEditorGuiWindow::init(const PhContext *_context)
    {
        context = _context;
        onInit();
    }

    void PhEditorGuiWindow::draw()
    {
        if (ImGui::Begin(onGetName(), NULL, getWindowFlags()))
            onDraw();
        ImGui::End();
    }
}