#include "launcher/editor/gui/phEditorPhysicsWindow.h"

#include "physics/phScene.h"
#include "physics/phOutputDebugData.h"

namespace phys
{
    PhEditorPhysicsWindow::PhEditorPhysicsWindow() : PhEditorGuiWindow(),
                                                     objectManager(NULL), scenePicker(NULL),
                                                     physicsManager(NULL), physicsSimulationSettings(NULL),
                                                     geScene(NULL), debugLineRenderer(NULL),
                                                     autoUpdateVelocities(false),
                                                     drawVelocities(false), drawSleeping(false),
                                                     drawContacts(false), drawEdges(false), indexGraph(-1)
    {
    }

    void PhEditorPhysicsWindow::release()
    {
        geScene->destroyDebugLineRenderer(debugLineRenderer);
    }

    void PhEditorPhysicsWindow::onInit()
    {
        objectManager = context->get<PhEditorObjectManagerSystem>();
        scenePicker = context->get<PhEditorScenePickerSystem>();
        physicsManager = context->get<PhEditorPhysicsManagerSystem>();
        physicsSimulationSettings = context->get<PhEditorPhysicsSimulationSettings>();
        geScene = context->get<ge::GeScene>();
        debugLineRenderer = geScene->createDebugLineRenderer();
    }

    static void drawMat4(const mth::mat4 &_m, const char *_name, uint32_t _id)
    {
        const uint32_t offsetX = 30;
        uint32_t xPos = ImGui::GetCursorPos().x;
        ImGui::SetCursorPosX(xPos + offsetX);
        ImGui::PushID(_id);
        if (ImGui::CollapsingHeader(_name))
        {
            ImGui::SetCursorPosX(xPos + offsetX);
            ImGui::LabelText("", "%f %f %f %f \n%f %f %f %f \n %f %f %f %f \n %f %f %f %f ",
                             _m.m[0][0], _m.m[0][1], _m.m[0][2], _m.m[0][3],
                             _m.m[1][0], _m.m[1][1], _m.m[1][2], _m.m[1][3],
                             _m.m[2][0], _m.m[2][1], _m.m[2][2], _m.m[2][3],
                             _m.m[3][0], _m.m[3][1], _m.m[3][2], _m.m[3][3]);
            ImGui::SetCursorPosX(xPos);
        }
        ImGui::PopID();
        ImGui::SetCursorPosX(xPos);
    }

    void PhEditorPhysicsWindow::onDraw()
    {
        physicsSimulationSettings->doOneStep = false;
        physicsSimulationSettings->simulationSettings.updateVelocities = false;
        ImGui::Checkbox("Simulate physics", &physicsSimulationSettings->simulate);
        ImGui::Checkbox("Auto update velocities", &autoUpdateVelocities);
        ImGui::Checkbox("Use delta time", &physicsSimulationSettings->useDeltaTime);
        if (!physicsSimulationSettings->useDeltaTime)
        {
            ImGui::SliderFloat("Delta time ", &physicsSimulationSettings->timeStep, 0.001f, 0.034f);
        }
        ImGui::SliderInt("Count iteration", &physicsSimulationSettings->simulationSettings.countIterations, 1, 32);
        ImGui::SliderInt("Count sub graphs", &physicsSimulationSettings->simulationSettings.countSubGraphs, 1, 16);

        if (!physicsSimulationSettings->simulate)
        {
            physicsSimulationSettings->doOneStep = ImGui::Button("Do step");
        }

        if (!autoUpdateVelocities)
            physicsSimulationSettings->simulationSettings.updateVelocities = ImGui::Button("Update velocities");
        else
            physicsSimulationSettings->simulationSettings.updateVelocities = autoUpdateVelocities;

        ImGui::Separator();
        // solver settings
        ImGui::SliderFloat("Depth contact min ", &physicsSimulationSettings->simulationSettings.depthContactMin, 0.000f, 0.1f);
        ImGui::SliderFloat("Depth contact max ", &physicsSimulationSettings->simulationSettings.depthContactMax, 0.000f, 0.1f);
        ImGui::SliderFloat("Linear damping ", &physicsSimulationSettings->simulationSettings.linearDamping, 0.0f, 10.0f);
        ImGui::SliderFloat("Angular damping ", &physicsSimulationSettings->simulationSettings.angularDamping, 0.0f, 10.0f);

        {
            const char *variants[] = {"PBD", "LCP (BLCP)"};
            uint currentSolver = (uint)physicsSimulationSettings->simulationSettings.solveMode;
            if (ImGui::BeginCombo("Solver type", variants[currentSolver], ImGuiComboFlags_NoArrowButton))
            {
                for (uint i = 0; i < PhSimulationSettings::SolverMode::CountSolvers; i++)
                {
                    bool isSelected = (variants[currentSolver] == variants[i]);
                    if (ImGui::Selectable(variants[i], isSelected))
                        physicsSimulationSettings->simulationSettings.solveMode = (PhSimulationSettings::SolverMode)i;
                    if (isSelected)
                        ImGui::SetItemDefaultFocus();
                }
                ImGui::EndCombo();
            }
        }

        if (physicsSimulationSettings->simulationSettings.solveMode == physicsSimulationSettings->simulationSettings.SolverMode_PBD)
        {
            ImGui::SliderFloat("Stiffness", &physicsSimulationSettings->simulationSettings.stiffnessPBD, 0.0f, 100.0f);
        }
        else
        {
            ImGui::SliderFloat("Stiffness", &physicsSimulationSettings->simulationSettings.stiffnessLCP, 0.0f, 10000000.0f);
            ImGui::SliderFloat("Damping", &physicsSimulationSettings->simulationSettings.dampingLCP, 0.0f, 1000000.0f);
        }

        ImGui::Separator();

        PhScene *scene = physicsManager->getPhysicsScene();
        const PhOutputDebugData *debugData = scene->getOutputDebugData();
        PhSettingsFeedbackDebugData *feedbackSettings = scene->getSettingsFeedbackDebug();

        // contact draw type
        ImGui::Checkbox("Enable debug", &feedbackSettings->enable);
        if (!feedbackSettings->enable)
            return;

        ImGui::Checkbox("Draw velocity", &drawVelocities);
        ImGui::Checkbox("Draw actor is sleeping", &drawSleeping);

        feedbackSettings->actorsOrientation = drawVelocities | drawSleeping; 
        feedbackSettings->sleepingActors = drawSleeping;

        ImGui::Checkbox("Draw edge containers", &drawEdges);
        ImGui::Checkbox("Draw contacts", &drawContacts);
        feedbackSettings->containers = drawEdges || drawContacts;
        feedbackSettings->contacts = drawContacts;

        ImGui::Separator();
        if (drawEdges)
            ImGui::SliderInt("Edge index", &indexGraph, -1, 16);

        if (drawVelocities)
            drawVelocitiesF();
        if (drawSleeping)
            drawActorSleepingF();
        if (drawEdges)
            drawEdgeContainersF();
        if (drawContacts)
            drawContactsF();
    }

    const char *PhEditorPhysicsWindow::onGetName()
    {
        return "Physics Simulation";
    }

    ImGuiWindowFlags PhEditorPhysicsWindow::getWindowFlags()
    {
        return 0;
    }

    void PhEditorPhysicsWindow::drawVelocitiesF()
    {
        PhScene *scene = physicsManager->getPhysicsScene();
        const PhOutputDebugData *debugData = scene->getOutputDebugData();

        uint32_t countOBBs = debugData->getCountObbs();
        const InOutputDebugData::Orientation *obbs = debugData->getObbsBuffer();

        uint32_t countActors = debugData->getCountActorOrientations();
        const InOutputDebugData::ActorOrientation *actors = debugData->getActorOrientations();
        const InOutputDebugData::ActorVelocity *velocities = debugData->getActorVelocity();
        for (uint32_t i = 0; i < countOBBs; i++)
        {
            InOutputDebugData::Orientation obb = obbs[i];
            if (obb.isDynamic == false || obb.indexOBBsOfActor != 0)
                continue;

            InOutputDebugData::ActorOrientation actor = actors[obb.indexActor];
            InOutputDebugData::ActorVelocity velocity = velocities[obb.indexDynamicActor];

            debugLineRenderer->drawLine(actor.position, actor.position + velocity.linearVelocity, mth::col(1, 1, 0));
            debugLineRenderer->drawLine(actor.position, actor.position + velocity.angularVelocity, mth::col(1, 0, 1));
        }
    }

    void PhEditorPhysicsWindow::drawActorSleepingF()
    {
        PhScene *scene = physicsManager->getPhysicsScene();
        const PhOutputDebugData *debugData = scene->getOutputDebugData();

        uint32_t countOBBs = debugData->getCountObbs();
        const InOutputDebugData::Orientation *obbs = debugData->getObbsBuffer();

        uint32_t countActors = debugData->getCountActorOrientations();
        const InOutputDebugData::ActorVelocity *velocities = debugData->getActorVelocity();
        for (uint32_t i = 0; i < countActors; i++)
        {
            InOutputDebugData::Orientation obb = obbs[i];
            if (obb.isDynamic == false)
                continue;

            InOutputDebugData::ActorVelocity velocity = velocities[obb.indexDynamicActor];
            mth::col color = mth::col(1, 0, 0);
            if (velocity.sleeping)
                color = mth::col(0, 1, 0);
            debugLineRenderer->drawCube(obb.position, obb.size, obb.rotation.getMatrix(), color);
        }
    }

    void PhEditorPhysicsWindow::drawEdgeContainersF()
    {
        PhScene *scene = physicsManager->getPhysicsScene();
        const PhOutputDebugData *debugData = scene->getOutputDebugData();

        uint32_t countOBBs = debugData->getCountObbs();
        const InOutputDebugData::Orientation *obbs = debugData->getObbsBuffer();

        uint32_t contactsContainers = debugData->getCountContainersContacts();
        const InOutputDebugData::ContainerContacts *containers = debugData->getContainersContacts();
        for (uint32_t i = 0; i < contactsContainers; i++)
        {

            InOutputDebugData::ContainerContacts container = containers[i];
            if (container.edgeIndex != indexGraph)
                continue;

            InOutputDebugData::Orientation obbA = obbs[container.obbA];
            InOutputDebugData::Orientation obbB = obbs[container.obbB];

            debugLineRenderer->drawLine(obbA.position, obbB.position, mth::col(1, 0, 1));
        }
    }

    void PhEditorPhysicsWindow::drawContactsF()
    {
        PhScene *scene = physicsManager->getPhysicsScene();
        const PhOutputDebugData *debugData = scene->getOutputDebugData();

        uint32_t countContacts = debugData->getCountContact();
        const InOutputDebugData::Contact *contacts = debugData->getContactBuffer();
        for (uint32_t i = 0; i < countContacts; i++)
        {
            InOutputDebugData::Contact contact = contacts[i];
            debugLineRenderer->drawLine(contact.firstOrigin, contact.firstPosition, mth::col(1, 0, 0));
            debugLineRenderer->drawLine(contact.secondOrigin, contact.secondPosition, mth::col(0, 1, 0));
        }
    }
}