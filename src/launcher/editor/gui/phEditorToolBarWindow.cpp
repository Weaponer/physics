#include "launcher/editor/gui/phEditorToolBarWindow.h"
#include "launcher/editor/phEditorSignals.h"
#include "launcher/editor/phEditorSpawnObjectSystem.h"

#include "rendering/gePivotRenderer.h"

namespace phys
{
    PhEditorToolBarWindow::PhEditorToolBarWindow() : PhEditorGuiWindow(),
                                                     selectConnection(), unselectConnection(), selectedObject(NULL),
                                                     currentState(0)
    {
    }

    void PhEditorToolBarWindow::release()
    {
        context->get<PhSelectObjectSig>()->unbind(selectConnection);
        context->get<PhUnselectObjectSig>()->unbind(unselectConnection);
        PhEditorGuiWindow::release();
    }

    void PhEditorToolBarWindow ::onSelectObject(PhEditorObject *_object)
    {
        selectedObject = _object;
    }

    void PhEditorToolBarWindow ::onUnselectObject()
    {
        selectedObject = NULL;
    }

    void PhEditorToolBarWindow::onInit()
    {
        selectConnection = context->get<PhSelectObjectSig>()->bind(this, &PhEditorToolBarWindow::onSelectObject);
        selectConnection = context->get<PhUnselectObjectSig>()->bind(this, &PhEditorToolBarWindow::onUnselectObject);
    }

    static void addVariant(PhFlags _variant, const char *_text, PhFlags *_currentState)
    {
        static const size_t TabWidth = 55;
        static const size_t TabHeight = 18;

        ImGui::PushID(_variant);
        ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 1);

        ImGui::SameLine(0, 1.0f);

        if (*_currentState == _variant)
            ImGui::PushStyleColor(ImGuiCol_Button, ImGui::GetStyle().Colors[ImGuiCol_ButtonActive]);
        else
            ImGui::PushStyleColor(ImGuiCol_Button, ImGui::GetStyle().Colors[ImGuiCol_Button]);

        if (ImGui::Button(_text, ImVec2(TabWidth, TabHeight))) // If tab clicked
            *_currentState = _variant;

        ImGui::PopStyleVar();
        ImGui::PopStyleColor(1);
        ImGui::PopID();
    }

    static void addVerticalBorder(float _offset)
    {
        ImGui::SameLine(0, _offset / 2.0f);

        auto DrawList = ImGui::GetWindowDrawList();
        ImVec2 cursorPos = ImGui::GetCursorPos();
        ImVec2 windowPos = ImGui::GetWindowPos();
        cursorPos.x += windowPos.x;
        cursorPos.y += windowPos.y;

        ImVec4 color = ImGui::GetStyle().Colors[ImGuiCol_Border];
        DrawList->AddLine({cursorPos.x, windowPos.y}, {cursorPos.x, windowPos.y + ImGui::GetWindowSize().y},
                          IM_COL32((int)(color.x * 255), (int)(color.y * 255), (int)(color.z * 255), 255), 1);

        ImGui::SameLine(0, _offset);
    }

    void PhEditorToolBarWindow::onDraw()
    {
        bool isDisable = selectedObject == NULL;

        PhFlags oldValue = currentState;

        if (isDisable)
            ImGui::BeginDisabled();
        addVariant(ge::GePivotRenderer::PivotState_None, "Hand", &currentState);
        addVariant(ge::GePivotRenderer::PivotState_Move, "Move", &currentState);
        addVariant(ge::GePivotRenderer::PivotState_Rotate, "Rotate", &currentState);
        addVariant(ge::GePivotRenderer::PivotState_Scale, "Scale", &currentState);
        if (isDisable)
            ImGui::EndDisabled();

        if (currentState != oldValue)
            context->get<PhChangePivotModeSig>()->signal(currentState);

        addVerticalBorder(20);

        if (ImGui::BeginPopupContextWindow("Spawn objects popup"))
        {
            if (ImGui::Button("Cube"))
            {
                context->get<PhEditorSpawnObjectSystem>()->spawCube();
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::Button("Sphere"))
            {
                context->get<PhEditorSpawnObjectSystem>()->spawnSphere();
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::Button("Capsule"))
            {
                context->get<PhEditorSpawnObjectSystem>()->spawnCapsule();
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::Button("Floor"))
            {
                context->get<PhEditorSpawnObjectSystem>()->spawnFloor();
                ImGui::CloseCurrentPopup();
            }
            ImGui::EndPopup();
        }

        if (ImGui::Button("Spawn"))
        {
            ImGui::OpenPopup("Spawn objects popup");
        }

        addVerticalBorder(20);

        if (isDisable)
            ImGui::BeginDisabled();

        if (ImGui::Button("Destroy") && selectedObject)
        {
            context->get<PhEditorObjectManagerSystem>()->destroyEditorObject(selectedObject);
        }

        if (isDisable)
            ImGui::EndDisabled();
    }

    const char *PhEditorToolBarWindow::onGetName()
    {
        return "Tool Bar";
    }

    ImGuiWindowFlags PhEditorToolBarWindow::getWindowFlags()
    {
        return ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize;
    }
}