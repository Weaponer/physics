#include "launcher/editor/phEditorGuiSystem.h"

#include "plugins/imgui/imgui.h"

#include "launcher/editor/gui/phEditorInspectorWindow.h"
#include "launcher/editor/gui/phEditorToolBarWindow.h"
#include "launcher/editor/gui/phEditorObjectsViewWindow.h"
#include "launcher/editor/gui/phEditorPhysicsWindow.h"

namespace phys
{
    void PhEditorGuiSystem::release()
    {
        guiContext.release();
    }

    void PhEditorGuiSystem::onInit()
    {
        mouseState = context->get<PhEditorMouseState>();
        guiContext.setParent(context);
        guiContext.bindDefault<PhEditorInspectorWindow>()->init(&guiContext);
        guiContext.bindDefault<PhEditorToolBarWindow>()->init(&guiContext);
        guiContext.bindDefault<PhEditorObjectsViewWindow>()->init(&guiContext);
        guiContext.bindDefault<PhEditorPhysicsWindow>()->init(&guiContext);
    }

    void PhEditorGuiSystem::onUpdate(float _deltaTime)
    {
        if (ImGui::GetIO().WantCaptureMouse)
            mouseState->tryLock(PhEditorMouseState::PhEditorMouseState_ImGUI, PhEditorMouseState::PhEditorMouseState_ImGUI);
        else
            mouseState->tryLock(PhEditorMouseState::PhEditorMouseState_ImGUI, PhEditorMouseState::PhEditorMouseState_None);
        guiContext.get<PhEditorInspectorWindow>()->draw();
        guiContext.get<PhEditorToolBarWindow>()->draw();
        guiContext.get<PhEditorObjectsViewWindow>()->draw();
        guiContext.get<PhEditorPhysicsWindow>()->draw();
    }

    void PhEditorGuiSystem::onCleanup()
    {
        guiContext.releaseBind<PhEditorInspectorWindow>();
        guiContext.releaseBind<PhEditorToolBarWindow>();
        guiContext.releaseBind<PhEditorObjectsViewWindow>();
        guiContext.releaseBind<PhEditorPhysicsWindow>();
    }
}