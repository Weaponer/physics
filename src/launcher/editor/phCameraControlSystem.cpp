#include <MTH/vectors.h>
#include <MTH/matrix.h>
#include <MTH/quaternion.h>

#include "launcher/editor/phCameraControlSystem.h"

#include "plugins/imgui/imgui.h"

namespace phys
{
    PhCameraControlSystem::PhCameraControlSystem() : camera(NULL), isControlCamera(false)
    {
    }

    void PhCameraControlSystem::release()
    {
    }

    void PhCameraControlSystem::onInit()
    {
        window = context->get<Window>();
        scene = context->get<ge::GeScene>();
        mouseState = context->get<PhEditorMouseState>();

        camera = scene->createCamera();
        scene->setActiveCamera(camera);

        speedMove = 0.0f;
        newRotate = mth::vec3();
    }

    void PhCameraControlSystem::onUpdate(float _deltaTime)
    {
        if (isControlCamera && ImGui::IsMouseReleased(ImGuiMouseButton_Right))
        {
            isControlCamera = false;
            window->showCursor();
            mouseState->tryLock(PhEditorMouseState::PhEditorMouseState_CameraControl, PhEditorMouseState::PhEditorMouseState_None);
        }
        else
        {
            if (!ImGui::GetIO().WantTextInput && ImGui::IsMouseClicked(ImGuiMouseButton_Right) &&
                mouseState->tryLock(PhEditorMouseState::PhEditorMouseState_CameraControl, PhEditorMouseState::PhEditorMouseState_CameraControl))
            {
                isControlCamera = true;
                window->hideCursor();
            }
        }

        if (!isControlCamera)
            return;

        const float maxSpeedMove = 20;
        const float acceleration = 8;
        const float powBreak = 5;
        const float speedRotate = 50;

        mth::vec2 deltaMove = window->getDeltaMoveMouse();
        mth::vec2 deltaRotate = deltaMove * speedRotate * _deltaTime;
        newRotate = newRotate + mth::vec3(deltaRotate.y, deltaRotate.x, 0.0f);

        mth::quat newRot = mth::quat();
        mth::quat newRotMove = mth::quat();
        newRot.setEuler(newRotate.x, newRotate.y, newRotate.z);
        newRotMove.setEuler(0, newRotate.y, newRotate.z);

        mth::mat4 mat4RotPos = newRotMove.getMatrix();

        mth::vec3 move = mth::vec3();
        if (ImGui::IsKeyDown(ImGuiKey_W))
            move.z = 1.0f;
        if (ImGui::IsKeyDown(ImGuiKey_S))
            move.z = -1.0f;
        if (ImGui::IsKeyDown(ImGuiKey_A))
            move.x = -1.0f;
        if (ImGui::IsKeyDown(ImGuiKey_D))
            move.x = 1.0f;
        if (ImGui::IsKeyDown(ImGuiKey_LeftShift))
            move.y = -1.0f;
        if (ImGui::IsKeyDown(ImGuiKey_Space))
            move.y = 1.0f;

        if (move.squareMagnitude() == 0)
        {
            float pB = speedMove - speedMove * powBreak * _deltaTime;
            speedMove = std::min(pB, speedMove);
        }
        else
        {
            speedMove = std::min(speedMove + acceleration * _deltaTime, maxSpeedMove);
        }

        mth::vec3 pos = camera->getPosition() + mat4RotPos.transform(move * speedMove * _deltaTime);

        camera->setPosition(pos);
        camera->setRotation(newRot);
    }

    void PhCameraControlSystem::onCleanup()
    {
        scene->destroyCamera(camera);
    }
}