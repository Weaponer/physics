#include "launcher/editor/phEditorSpawnObjectSystem.h"

namespace phys
{
    PhEditorSpawnObjectSystem::PhEditorSpawnObjectSystem() : PhEditorSystem(),
                                                             objectManager(NULL), scenePicker(NULL), scene(NULL)
    {
    }

    void PhEditorSpawnObjectSystem::release()
    {
    }

    void PhEditorSpawnObjectSystem::spawCube()
    {
        PhEditorObject *object = objectManager->createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
        scenePicker->selectEditorObject(object);
        object->setPosition(getSpawnPosition());
    }

    void PhEditorSpawnObjectSystem::spawnSphere()
    {
        PhEditorObject *object = objectManager->createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Sphere);
        scenePicker->selectEditorObject(object);
        object->setPosition(getSpawnPosition());
    }
    void PhEditorSpawnObjectSystem::spawnCapsule()
    {
        PhEditorObject *object = objectManager->createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Capsule);
        scenePicker->selectEditorObject(object);
        object->setPosition(getSpawnPosition());
    }

    void PhEditorSpawnObjectSystem::spawnFloor()
    {
        if (scene->getFloorRenderer() == NULL)
        {
            PhEditorObject *object = objectManager->createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Floor);
            scenePicker->selectEditorObject(object);
        }
    }

    void PhEditorSpawnObjectSystem::onInit()
    {
        objectManager = context->get<PhEditorObjectManagerSystem>();
        scenePicker = context->get<PhEditorScenePickerSystem>();
        scene = context->get<ge::GeScene>();
    }

    void PhEditorSpawnObjectSystem::onUpdate(float _deltaTime)
    {
    }

    void PhEditorSpawnObjectSystem::onCleanup()
    {
    }

    mth::vec3 PhEditorSpawnObjectSystem::getSpawnPosition()
    {
        const float distSpawn = 15.0f;
        ge::GeCamera *camera = scene->getActiveCamera();
        if (camera == NULL)
            return mth::vec3();

        return camera->getPosition() + camera->getRotation().rotateVector(mth::vec3(0, 0, 1.0f)) * distSpawn;
    }
}