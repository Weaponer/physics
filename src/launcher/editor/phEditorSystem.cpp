#include "launcher/editor/phEditorSystem.h"

namespace phys
{
    void PhEditorSystem::init(const PhContext *_context)
    {
        context = _context;
        onInit();
    }

    void PhEditorSystem::update(float _deltaTime)
    {
        onUpdate(_deltaTime);
    }

    void PhEditorSystem::cleanup()
    {
        onCleanup();
    }
}