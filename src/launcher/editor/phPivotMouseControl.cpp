#include <MTH/vectors.h>
#include <MTH/quaternion.h>
#include <MTH/closestPoint.h>
#include <MTH/intersections.h>
#include <MTH/bounds.h>
#include <MTH/glGen_matrix.h>

#include "launcher/editor/phPivotMouseControl.h"

namespace phys
{
    static bool intersectionRayPlane(const mth::vec3 &_origin, const mth::vec3 &_dir, const mth::vec3 &_normal, mth::vec3 &_p)
    {
        float t = _origin * _normal;
        float cosDir = _normal * _dir;
        if ((t < 0.0f && cosDir < 0.0f) || abs(cosDir) <= __FLT_EPSILON__)
            return false;
        if (t >= 0.0f && cosDir >= 0.0f)
            return false;

        _p = _origin + _dir * (-t / cosDir);
        return true;
    }

    static bool intersectionRayDistPlane(const mth::vec3 &_origin, const mth::vec3 &_dir, const mth::plane &_plane, mth::vec3 &_p)
    {
        float denom = _plane.normal * _dir;
        if (abs(denom) > std::numeric_limits<float>::epsilon())
        {
            float t = ((_plane.normal * _plane.dist - _origin) * _plane.normal) / denom;
            if (t >= 0.0f)
            {
                _p = _origin + _dir * t;
                return true;
            }
        }
        return false;
    }

    void PhPivotMouseControl::initPivot(ge::GePivotRenderer *_pivot)
    {
        lockContactData = {};
        hasLockContact = false;

        pivot = _pivot;
        isLock = false;
    }

    void PhPivotMouseControl::resetPivot()
    {
        pivot->setActiveAxises(ge::GePivotRenderer::PivotAxis_None);
        pivot->setScaleOffsets(mth::vec3(1.0f, 1.0f, 1.0f));
        isLock = false;
    }

    void PhPivotMouseControl::updatePivot(ge::GeCamera *_camera, mth::vec2 _mousePosition)
    {
        PhFlags state = pivot->getPivotState();
        if (state == ge::GePivotRenderer::PivotState_None)
        {
            pivot->setActiveAxises(ge::GePivotRenderer::PivotAxis_None);
            return;
        }

        UpdateData updateData = getUpdateData(_camera, pivot, _mousePosition);

        if (state == ge::GePivotRenderer::PivotState_Rotate)
        {
            PhFlags frontAxis = getFrontRotateAxis(updateData, _camera, pivot);
            pivot->setFrontRotateAxis(frontAxis);
        }

        PhFlags acitveAxises = ge::GePivotRenderer::PivotAxis_None;
        bool hasContact = false;
        ContactData newContact;

        newContact.scale = updateData.pivotScale;
        newContact.mousePosition = updateData.mousePosition;

        if (!isLock)
        {
            if (state == ge::GePivotRenderer::PivotState_Rotate)
            {
                hasContact = getActiveRotateAxis(updateData, _camera, pivot, acitveAxises, newContact);
            }
            else
            {
                hasContact = getActiveMoveScaleAxis(updateData, _camera, pivot, acitveAxises, newContact);
            }

            hasLockContact = hasContact;
            lockContactData = newContact;
            pivot->setActiveAxises(acitveAxises);
        }
        else
        {
            if (state == ge::GePivotRenderer::PivotState_Move)
            {
                movePivot(lockContactData, updateData, _camera, pivot);
            }
            else if (state == ge::GePivotRenderer::PivotState_Scale)
            {
                scalePivot(lockContactData, updateData, _camera, pivot);
            }
            else if (state == ge::GePivotRenderer::PivotState_Rotate)
            {
                rotatePivot(lockContactData, updateData, _camera, pivot);
            }
        }
    }

    void PhPivotMouseControl::updatePivotBackground(ge::GeCamera *_camera)
    {
        resetPivot();
        if (pivot->getPivotState() == ge::GePivotRenderer::PivotState_Rotate)
        {
            UpdateData updateData = getUpdateData(_camera, pivot, mth::vec2(0.0f, 0.0f));
            PhFlags frontAxis = getFrontRotateAxis(updateData, _camera, pivot);
            pivot->setFrontRotateAxis(frontAxis);
        }
    }

    bool PhPivotMouseControl::tryLockPivot()
    {
        if (!isLock && hasLockContact)
        {
            isLock = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    void PhPivotMouseControl::unlockPivot()
    {
        if (isLock)
            resetPivot();

        isLock = false;
    }

    PhPivotMouseControl::UpdateData PhPivotMouseControl::getUpdateData(ge::GeCamera *_camera, ge::GePivotRenderer *_pivot,
                                                                       mth::vec2 _mousePosition)
    {
        PhPivotMouseControl::UpdateData data;
        // ray
        mth::vec3 rayOrigin = _camera->getPosition();
        mth::vec3 rayDirection;
        mth::vec3 rayEndPoint;
        mth::vec3 localRayOrigin;
        mth::vec3 localRayDirection;
        mth::vec3 localRayEnd;
        {
            mth::vec4 mouseProj = mth::vec4(_mousePosition.x * 2.0f - 1.0f, _mousePosition.y * 2.0f - 1.0f, 1.0f, 1.0f);
            mouseProj = _camera->getInverseProjectionMatrix().transform(mouseProj);
            mouseProj = mouseProj / mouseProj.w;

            mth::vec4 viewPos = mth::vec4(mouseProj.x, mouseProj.y, mouseProj.z, 0.0f);
            viewPos = _camera->getTRS().transform(viewPos);

            rayDirection = mth::vec3(viewPos.x, viewPos.y, viewPos.z);
            rayDirection.normalise();

            float farCamera = _camera->getFar();
            rayEndPoint = rayOrigin + rayDirection * farCamera;

            mth::mat4 inverseTRS = _pivot->getInverseTRS();

            localRayOrigin = inverseTRS.transform(rayOrigin);
            localRayDirection = inverseTRS.transformNoMove(rayDirection);
            localRayEnd = localRayOrigin + localRayDirection * farCamera;
        }

        data.pivotPosition = _pivot->getPosition();
        data.cameraPosition = _camera->getPosition();

        mth::vec3 dir = data.pivotPosition - data.cameraPosition;
        dir.normalise();
        data.cameraForward = _camera->getRotation().getMatrix3().getVector(2);
        data.directionToPivot = dir;
        data.localDirectionToPivot = _pivot->getRotation().getMatrix3().transformTranspose(dir);

        data.rayOrigin = rayOrigin;
        data.rayDirection = rayDirection;
        data.rayEndPoint = rayEndPoint;
        data.localRayOrigin = localRayOrigin;
        data.localRayDirection = localRayDirection;
        data.localRayEnd = localRayEnd;

        data.pivotScale = _pivot->getSizePivot(_camera);

        mth::mat3 pivotRotM = _pivot->getRotation().getMatrix3();
        data.pivotRight = pivotRotM.getVector(0);
        data.pivotUp = pivotRotM.getVector(1);
        data.pivotForward = pivotRotM.getVector(2);

        data.mousePosition = _mousePosition;

        return data;
    }

    PhFlags PhPivotMouseControl::getFrontRotateAxis(UpdateData &_data, ge::GeCamera *_camera, ge::GePivotRenderer *_pivot)
    {
        mth::vec3 dir = _pivot->getPosition() - _camera->getPosition();
        dir.normalise();
        const float activeFrontAngle = 0.90f;
        if (abs(dir * _data.pivotForward) >= activeFrontAngle)
            return ge::GePivotRenderer::PivotAxis_Z;
        else if (abs(dir * _data.pivotUp) >= activeFrontAngle)
            return ge::GePivotRenderer::PivotAxis_Y;
        else if (abs(dir * _data.pivotRight) >= activeFrontAngle)
            return ge::GePivotRenderer::PivotAxis_X;

        return ge::GePivotRenderer::PivotAxis_None;
    }

    bool PhPivotMouseControl::getActiveMoveScaleAxis(const UpdateData &_data, const ge::GeCamera *_camera, const ge::GePivotRenderer *_pivot,
                                                     PhFlags &_activeAxies, ContactData &_contactData)
    {
        ge::GePivotRenderer::MoveScaleAxisData moveAxisData = _pivot->getMoveScaleAxisData();

        // center cube
        {
            float sizeCube = moveAxisData.sizeCenterCube * _data.pivotScale;
            mth::boundAABB aabb = mth::boundAABB(mth::vec3(0, 0, 0), mth::vec3(sizeCube, sizeCube, sizeCube));
            if (mth::test_Segment_AABB(_data.localRayOrigin, _data.localRayEnd, aabb))
            {
                _contactData.contactType = PhPivotContact_Plane;
                _contactData.originData = _data.pivotPosition;
                _contactData.directionData = _data.cameraForward;
                float t;
                mth::intersectSegmentPlane(_data.rayOrigin, _data.rayEndPoint, mth::plane(_contactData.originData, _contactData.directionData),
                                           t, _contactData.contactData);
                _contactData.activeScaleAxises = ge::GePivotRenderer::PivotAxis_X | ge::GePivotRenderer::PivotAxis_Y | ge::GePivotRenderer::PivotAxis_Z;
                _activeAxies = _contactData.activeScaleAxises;
                return true;
            }
        }

        // axises
        {
            mth::vec3 startAxisSegment = _data.pivotPosition;
            mth::vec3 endAxisSegmentX = _data.pivotPosition + _data.pivotRight * _data.pivotScale;
            mth::vec3 endAxisSegmentY = _data.pivotPosition + _data.pivotUp * _data.pivotScale;
            mth::vec3 endAxisSegmentZ = _data.pivotPosition + _data.pivotForward * _data.pivotScale;

            float detectAxisDist = moveAxisData.sizeEdgeCube * _data.pivotScale;
            detectAxisDist *= detectAxisDist; // s quare

            _contactData.contactType = PhPivotContact_Segment;

            float s = 0.0f;
            float t = 0.0f;
            mth::vec3 c1 = mth::vec3();
            mth::vec3 c2 = mth::vec3();
            float distToX = mth::closestPointSegmentSegment(_data.rayOrigin, _data.rayEndPoint, startAxisSegment, endAxisSegmentX,
                                                            s, t, c1, c2);
            if (distToX <= detectAxisDist)
            {
                _contactData.originData = _data.pivotPosition;
                _contactData.directionData = _data.pivotRight;
                _contactData.contactData = c2;
                _contactData.activeScaleAxises = ge::GePivotRenderer::PivotAxis_X;
                _activeAxies = _contactData.activeScaleAxises;
                return true;
            }

            float distToY = mth::closestPointSegmentSegment(_data.rayOrigin, _data.rayEndPoint, startAxisSegment, endAxisSegmentY,
                                                            s, t, c1, c2);
            if (distToY <= detectAxisDist)
            {
                _contactData.originData = _data.pivotPosition;
                _contactData.directionData = _data.pivotUp;
                _contactData.contactData = c2;
                _contactData.activeScaleAxises = ge::GePivotRenderer::PivotAxis_Y;
                _activeAxies = _contactData.activeScaleAxises;
                return true;
            }

            float distToZ = mth::closestPointSegmentSegment(_data.rayOrigin, _data.rayEndPoint, startAxisSegment, endAxisSegmentZ,
                                                            s, t, c1, c2);
            if (distToZ <= detectAxisDist)
            {
                _contactData.originData = _data.pivotPosition;
                _contactData.directionData = _data.pivotForward;
                _contactData.contactData = c2;
                _contactData.activeScaleAxises = ge::GePivotRenderer::PivotAxis_Z;
                _activeAxies = _contactData.activeScaleAxises;
                return true;
            }
        }

        // planes
        {
            float offsetRect = moveAxisData.offsetPlaneRect * _data.pivotScale;
            float sizeRect = moveAxisData.sizePlaneRect * _data.pivotScale;

            int resIndex = -1;
            mth::vec3 directions[3] = {_data.pivotRight, _data.pivotUp, _data.pivotForward};
            for (int i = 0; i < 3; i++)
            {
                mth::vec3 n = mth::vec3();
                n[i] = 1.0f;
                mth::vec3 offset = mth::vec3(offsetRect, offsetRect, offsetRect);
                offset[i] = 0.0f;

                mth::vec3 res;
                if (intersectionRayPlane(_data.localRayOrigin, _data.localRayDirection, n, res))
                {
                    res = (res - offset) / sizeRect;
                    res[i] = 0.5f;
                    if (res.x >= 0.0f && res.x <= 1.0f && res.y >= 0.0f && res.y <= 1.0f && res.z >= 0.0f && res.z <= 1.0f)
                    {
                        resIndex = i;
                        break;
                    }
                }
            }

            if (resIndex != -1)
            {
                _contactData.contactType = PhPivotContact_Plane;
                _contactData.originData = _data.pivotPosition;
                _contactData.directionData = directions[resIndex];
                float t;
                mth::intersectSegmentPlane(_data.rayOrigin, _data.rayEndPoint, mth::plane(_data.pivotPosition, directions[resIndex]),
                                           t, _contactData.contactData);

                if (resIndex == 0)
                    _contactData.activeScaleAxises = ge::GePivotRenderer::PivotAxis_Y | ge::GePivotRenderer::PivotAxis_Z;
                else if (resIndex == 1)
                    _contactData.activeScaleAxises = ge::GePivotRenderer::PivotAxis_X | ge::GePivotRenderer::PivotAxis_Z;
                else if (resIndex == 2)
                    _contactData.activeScaleAxises = ge::GePivotRenderer::PivotAxis_X | ge::GePivotRenderer::PivotAxis_Y;

                _activeAxies = _contactData.activeScaleAxises;
                return true;
            }
        }

        _activeAxies = ge::GePivotRenderer::PivotAxis_None;
        return false;
    }

    bool PhPivotMouseControl::getActiveRotateAxis(const UpdateData &_data, const ge::GeCamera *_camera, const ge::GePivotRenderer *_pivot,
                                                  PhFlags &_activeAxies, ContactData &_contactData)
    {
        _contactData.rotate = _pivot->getRotation().getMatrix3();

        ge::GePivotRenderer::RotateAxisData rotateAxisData = _pivot->getRotateAxisData();
        ge::GePivotRenderer::MoveScaleAxisData moveAxisData = _pivot->getMoveScaleAxisData();

        PhFlags axises[3] = {ge::GePivotRenderer::PivotAxis_X, ge::GePivotRenderer::PivotAxis_Y, ge::GePivotRenderer::PivotAxis_Z};
        PhFlags rotateAxises[3] = {PhPivotRotate_X, PhPivotRotate_Y, PhPivotRotate_Z};

        mth::vec3 axisesDirections[3] = {_data.pivotRight, _data.pivotUp, _data.pivotForward};

        mth::vec3 startAxisSegment = _data.pivotPosition;
        mth::vec3 axisesEnd[3] = {_data.pivotPosition + _data.pivotRight * _data.pivotScale,
                                  _data.pivotPosition + _data.pivotUp * _data.pivotScale,
                                  _data.pivotPosition + _data.pivotForward * _data.pivotScale};
        float detectAxisDist = moveAxisData.sizeEdgeCube * _data.pivotScale;
        detectAxisDist *= detectAxisDist;

        PhFlags frontAxis = _pivot->getFrontRotateAxis();

        float radiusMainAxis = rotateAxisData.radiusMainAxis * _data.pivotScale;
        float heightCylinder = rotateAxisData.cylindersHeight * 2 * _data.pivotScale;

        // test cylinder axises
        for (int i = 0; i < 3; i++)
        {
            {
                if (frontAxis == axises[i])
                    continue;

                mth::vec3 cylinderDirection = mth::vec3();
                cylinderDirection[i] = 1.0f;

                float d = cylinderDirection * _data.localRayOrigin;
                mth::vec3 projOrigin = _data.localRayOrigin - cylinderDirection * d;

                mth::vec3 projDirection = _data.localRayDirection;
                projDirection[i] = 0.0f;
                projDirection.normalise();

                float t = projDirection * (-projOrigin);

                if (t < 0.0f)
                    continue;

                float h = (projDirection * t + projOrigin).magnitude();

                if (h > radiusMainAxis)
                    continue;

                float f = sqrt(radiusMainAxis * radiusMainAxis - h * h);
                float c = projDirection * _data.localRayDirection;

                mth::vec3 nearPoint = _data.localRayOrigin + _data.localRayDirection * ((t - f) / c);
                if (abs(nearPoint[i]) <= heightCylinder)
                {
                    _activeAxies = axises[i];

                    _contactData.contactType = PhPivotContact_Plane;
                    _contactData.originData = _data.pivotPosition;
                    _contactData.directionData = axisesDirections[i];

                    mth::intersectSegmentPlane(_data.rayOrigin, _data.rayEndPoint, mth::plane(_data.pivotPosition, axisesDirections[i]),
                                               t, _contactData.contactData);
                    _contactData.rotateAxis = rotateAxises[i];
                    return true;
                }
            }
        }

        // test line axises
        for (int i = 0; i < 3; i++)
        {
            {
                float s = 0.0f;
                float t = 0.0f;
                mth::vec3 c1 = mth::vec3();
                mth::vec3 c2 = mth::vec3();
                float distToAxis = mth::closestPointSegmentSegment(_data.rayOrigin, _data.rayEndPoint, startAxisSegment, axisesEnd[i],
                                                                   s, t, c1, c2);
                if (distToAxis <= detectAxisDist)
                {
                    _activeAxies = axises[i];
                    _contactData.contactType = PhPivotContact_Segment;
                    _contactData.originData = _data.pivotPosition;
                    _contactData.directionData = axisesDirections[i];
                    _contactData.contactData = c2;
                    _contactData.rotateAxis = rotateAxises[i];
                    return true;
                }
            }
        }

        // test front axises;
        mth::vec3 dirToCenter = _pivot->getPosition() - _data.rayOrigin;
        float distToCenter = dirToCenter.magnitude();
        dirToCenter = dirToCenter / distToCenter;

        float distToCenterProj = 0.0f;
        mth::vec3 pointContactWithFrontPlane;
        {
            float c = dirToCenter * _data.rayDirection;
            float f = distToCenter / c;
            distToCenterProj = sqrt(f * f - distToCenter * distToCenter);

            float t;
            mth::intersectSegmentPlane(_data.rayOrigin, _data.rayEndPoint, mth::plane(_data.pivotPosition, dirToCenter),
                                       t, pointContactWithFrontPlane);
        }

        _activeAxies = ge::GePivotRenderer::PivotAxis_None;
        _contactData.contactType = PhPivotContact_Plane;
        _contactData.originData = _data.pivotPosition;

        if (distToCenterProj <= rotateAxisData.radiusOutsideCircle * _data.pivotScale)
        {
            if (frontAxis != ge::GePivotRenderer::PivotAxis_None)
            {
                float const contactDist = 0.04f;
                if (abs(distToCenterProj - rotateAxisData.radiusFrontAxis * _data.pivotScale) <= contactDist * _data.pivotScale)
                {
                    _activeAxies = frontAxis;

                    for (int i = 0; i < 3; i++)
                    {
                        if (frontAxis != axises[i])
                            continue;

                        _contactData.rotateAxis = rotateAxises[i];
                        _contactData.directionData = axisesDirections[i];
                        float t;
                        mth::intersectSegmentPlane(_data.rayOrigin, _data.rayEndPoint, mth::plane(_data.pivotPosition, axisesDirections[i]),
                                                   t, _contactData.contactData);
                        break;
                    }

                    return true;
                }
            }

            _contactData.directionData = dirToCenter;
            _contactData.contactData = pointContactWithFrontPlane;

            if (distToCenterProj <= rotateAxisData.radiusFrontAxis * _data.pivotScale)
            {
                _contactData.rotateAxis = PhPivotRotate_Global;
                return true;
            }

            _contactData.rotateAxis = PhPivotRotate_Camera;
            return true;
        }

        return false;
    }

    static bool getNearPointRayToRay(const mth::vec3 &_origin1, const mth::vec3 &_ray1,
                                     const mth::vec3 &_origin2, const mth::vec3 &_ray2,
                                     mth::vec3 &_outPoint)
    {
        if ((1.0f - abs(_ray1 * _ray2)) <= std::numeric_limits<float>::epsilon())
            return false;

        mth::vec3 dirToOrigin = _origin1 - _origin2;
        dirToOrigin.normalise();

        mth::vec3 otherNormal = _ray1 % dirToOrigin;
        otherNormal = otherNormal % _ray1;
        otherNormal.normalise();

        mth::plane testPlane = mth::plane(_origin1, otherNormal);
        mth::vec3 contact;
        intersectionRayDistPlane(_origin2, _ray2, testPlane, contact);
        float t = _ray1 * (contact - _origin1);
        _outPoint = _origin1 + (_ray1 * t);
        return true;
    }

    void PhPivotMouseControl::movePivot(const ContactData &_firstContactData, const UpdateData &_data, ge::GeCamera *_camera, ge::GePivotRenderer *_pivot)
    {
        mth::mat4 inverseTRS = _camera->getInverseTRS();
        mth::vec3 localOriginData = inverseTRS.transform(_firstContactData.originData);
        mth::vec3 localContactData = inverseTRS.transform(_firstContactData.contactData);
        mth::vec3 localDirectionData = inverseTRS.transformNoMove(_firstContactData.directionData);
        localDirectionData.normalise();

        mth::vec3 localRayOrigin = inverseTRS.transform(_data.rayOrigin);
        mth::vec3 localRayDirection = inverseTRS.transformNoMove(_data.rayDirection);
        localRayDirection.normalise();

        mth::vec3 localNewPoint;
        mth::vec3 localMoveDirection;
        if (_firstContactData.contactType == PhPivotContact_Segment)
        {
            if (!getNearPointRayToRay(localOriginData, localDirectionData, localRayOrigin, localRayDirection, localNewPoint))
                return;

            localMoveDirection = localDirectionData;
        }
        else
        {
            if (abs(localDirectionData.z) <= std::numeric_limits<float>::epsilon())
                return;

            mth::plane testPlane = mth::plane(localOriginData, localDirectionData);
            mth::vec3 contact;
            intersectionRayDistPlane(localRayOrigin, localRayDirection, testPlane, contact);
            localNewPoint = contact;

            localMoveDirection = localContactData - localOriginData;
            localMoveDirection.normalise();
        }

        mth::vec3 localStartPosition = mth::vec3();

        float c = localMoveDirection.z;
        if (abs(c) <= std::numeric_limits<float>::epsilon())
        {
            localStartPosition = localNewPoint;
        }
        else
        {
            float t = localNewPoint.z;
            float cosDir = c;
            localStartPosition = localNewPoint + localMoveDirection * (-t / cosDir);
        }

        float lastScale = _pivot->getSizePivotNoDistance(_camera) * ((_firstContactData.contactData - _firstContactData.originData).magnitude() / _firstContactData.scale);

        float distToNewPoint = (localNewPoint - localStartPosition).magnitude();
        mth::vec3 locDirToNewPoint = localNewPoint - localStartPosition;
        locDirToNewPoint.normalise();

        float t;
        if (locDirToNewPoint * localMoveDirection >= 0.0f)
            t = distToNewPoint / (1.0f + abs(c) * lastScale);
        else
            t = distToNewPoint / (1.0f - abs(c) * lastScale);

        mth::vec3 locFinalPosition = localStartPosition + locDirToNewPoint * t;
        mth::mat4 TRS = _camera->getTRS();
        _pivot->setPosition(TRS.transform(locFinalPosition));
    }

    void PhPivotMouseControl::scalePivot(const ContactData &_firstContactData, const UpdateData &_data, ge::GeCamera *_camera, ge::GePivotRenderer *_pivot)
    {
        mth::vec3 scaleOffset = _pivot->getScaleOffsets();
        if (_firstContactData.contactType == PhPivotContact_Segment)
        {
            mth::vec3 contactPoint;
            if (!getNearPointRayToRay(_firstContactData.originData, _firstContactData.directionData,
                                      _data.rayOrigin, _data.rayDirection, contactPoint))
                return;

            float segmentScale;
            if ((contactPoint - _firstContactData.originData) * _firstContactData.directionData < 0.0f)
                segmentScale = 0;
            else
                segmentScale = (contactPoint - _firstContactData.originData).magnitude() / _firstContactData.scale;

            if (_firstContactData.activeScaleAxises == ge::GePivotRenderer::PivotAxis_X)
                scaleOffset.x = segmentScale;
            else if (_firstContactData.activeScaleAxises == ge::GePivotRenderer::PivotAxis_Y)
                scaleOffset.y = segmentScale;
            else if (_firstContactData.activeScaleAxises == ge::GePivotRenderer::PivotAxis_Z)
                scaleOffset.z = segmentScale;
        }
        else
        {
            if (abs(_firstContactData.directionData * _data.rayDirection) <= std::numeric_limits<float>::epsilon())
                return;

            mth::plane testPlane = mth::plane(_firstContactData.originData, _firstContactData.directionData);
            mth::vec3 contact;
            intersectionRayDistPlane(_data.rayOrigin, _data.rayDirection, testPlane, contact);
            mth::vec3 contactPoint = contact;

            bool doubleSide = true;
            if (_firstContactData.activeScaleAxises == (ge::GePivotRenderer::PivotAxis_X | ge::GePivotRenderer::PivotAxis_Y | ge::GePivotRenderer::PivotAxis_Z))
                doubleSide = false;

            if (doubleSide)
            {
                mth::mat4 inverseTRS = _pivot->getInverseTRS();
                mth::vec3 localContactPoint = inverseTRS.transform(contactPoint);
                mth::vec3 localFirstContact = inverseTRS.transform(_firstContactData.contactData);

                mth::vec3 mask;
                if (_firstContactData.activeScaleAxises == (ge::GePivotRenderer::PivotAxis_X | ge::GePivotRenderer::PivotAxis_Y))
                    mask = mth::vec3(1.0f, 1.0f, 0.0f);
                else if (_firstContactData.activeScaleAxises == (ge::GePivotRenderer::PivotAxis_X | ge::GePivotRenderer::PivotAxis_Z))
                    mask = mth::vec3(1.0f, 0.0f, 1.0f);
                else
                    mask = mth::vec3(0.0f, 1.0f, 1.0f);

                localContactPoint.x = std::max(0.0f, localContactPoint.x);
                localContactPoint.y = std::max(0.0f, localContactPoint.y);
                localContactPoint.z = std::max(0.0f, localContactPoint.z);

                if (mask.x != 0.0f)
                    localContactPoint.x = localContactPoint.x / abs(localFirstContact.x);
                if (mask.y != 0.0f)
                    localContactPoint.y = localContactPoint.y / abs(localFirstContact.y);
                if (mask.z != 0.0f)
                    localContactPoint.z = localContactPoint.z / abs(localFirstContact.z);

                localContactPoint = localContactPoint / mth::vec3(1.0f, 1.0f, 0.0f).magnitude();

                mth::vec3 inverseMask = mth::vec3(1.0f, 1.0f, 1.0f) - mask;
                scaleOffset = inverseMask + mask * localContactPoint.magnitude();
            }
            else
            {
                mth::mat4 inverseTRS = _camera->getInverseTRS();
                mth::vec3 localContactPoint = inverseTRS.transform(contactPoint);
                mth::vec3 localFirstContact = inverseTRS.transform(_firstContactData.contactData);

                mth::vec2 dir = mth::vec2(localContactPoint.x - localFirstContact.x, localContactPoint.y - localFirstContact.y);
                mth::vec2 axisScale = mth::vec2(1.0f, 1.0f);
                axisScale.normalise();

                float scale = 1.0f + (dir * axisScale) / _firstContactData.scale * _camera->getFovTanget();
                scale = std::max(0.0f, scale);

                scaleOffset = mth::vec3(scale, scale, scale);
            }
        }

        _pivot->setScaleOffsets(scaleOffset);
    }

    void PhPivotMouseControl::rotatePivot(ContactData &_firstContactData, const UpdateData &_data, ge::GeCamera *_camera, ge::GePivotRenderer *_pivot)
    {
        if (_firstContactData.contactType == PhPivotContact_Segment)
        {
            float rotate = (_data.mousePosition.x - _firstContactData.mousePosition.x) * 360.0f;

            mth::mat4 finalRot = mth::rotate(-rotate, _firstContactData.directionData) * mth::mat4(_firstContactData.rotate);
            mth::quat resultQuat = mth::quat();
            resultQuat.setFromMatrix3(mth::matrix3FromMatrix4(finalRot));
            _pivot->setRotation(resultQuat);
        }
        else if (_firstContactData.contactType == PhPivotContact_Plane)
        {
            mth::vec3 dirToPivot = _firstContactData.originData - _data.cameraPosition;
            dirToPivot.normalise();
            if (_firstContactData.rotateAxis == PhPivotRotate_Global)
            {
                mth::vec2 delta = _data.mousePosition - _firstContactData.mousePosition;
                delta.y *= -1;

                mth::vec3 dirRotate = mth::vec3(delta.x, delta.y, 0.0f);
                dirRotate.normalise();
                mth::vec3 dir = _camera->getTRS().transformNoMove(dirRotate);

                mth::vec3 axisRotate = dir % dirToPivot;
                axisRotate.normalise();
                mth::mat4 finalRot = mth::rotate(delta.magnitude() * 360.0f, axisRotate) * mth::mat4(_firstContactData.rotate);
                mth::quat resultQuat = mth::quat();
                resultQuat.setFromMatrix3(mth::matrix3FromMatrix4(finalRot));
                _pivot->setRotation(resultQuat);
            }
            else
            {
                const float angleLimit = 0.15f; // cos limit rotate angle for planes

                if (abs(_firstContactData.directionData * dirToPivot) <= angleLimit)
                {
                    mth::vec3 up = dirToPivot % _firstContactData.directionData;
                    mth::vec3 localUp = _camera->getInverseTRS().transformNoMove(up);

                    localUp.z = 0.0f;
                    localUp.normalise();

                    float t = (_data.mousePosition - _firstContactData.mousePosition) * mth::vec2(localUp.x, localUp.y) * 360.0f;
                    mth::mat4 finalRot = mth::rotate(-t, _firstContactData.directionData) * mth::mat4(_firstContactData.rotate);
                    mth::quat resultQuat = mth::quat();
                    resultQuat.setFromMatrix3(mth::matrix3FromMatrix4(finalRot));
                    _pivot->setRotation(resultQuat);
                }
                else
                {
                    float t;
                    mth::vec3 newContact;
                    mth::intersectSegmentPlane(_data.rayOrigin, _data.rayEndPoint,
                                               mth::plane(_firstContactData.originData, _firstContactData.directionData),
                                               t, newContact);

                    mth::vec3 dirMainContact = _firstContactData.contactData - _firstContactData.originData;
                    dirMainContact.normalise();

                    mth::vec3 dirNewContact = newContact - _firstContactData.originData;
                    dirNewContact.normalise();

                    mth::vec3 otherSide = _firstContactData.directionData % dirMainContact;
                    otherSide.normalise();

                    float angle = 0.0f;
                    if (1.0f - abs(dirMainContact * dirNewContact) > std::numeric_limits<float>::epsilon())
                        angle = acos(dirMainContact * dirNewContact) * DEGREE;

                    if (otherSide * dirNewContact < 0.0f)
                        angle *= -1;

                    mth::mat4 finalRot = mth::rotate(angle, _firstContactData.directionData) * mth::mat4(_firstContactData.rotate);
                    mth::quat resultQuat = mth::quat();
                    resultQuat.setFromMatrix3(mth::matrix3FromMatrix4(finalRot));
                    _pivot->setRotation(resultQuat);

                    _firstContactData.contactData = newContact;
                    _firstContactData.rotate = mth::matrix3FromMatrix4(finalRot);
                }
            }
        }
    }
}