#include "launcher/editor/phEditorPhysicsManagerSystem.h"
#include "launcher/editor/phEditorObjectManagerSystem.h"

namespace phys
{
    PhEditorPhysicsManagerSystem::PhEditorPhysicsManagerSystem()
        : PhEditorSystem(), physics(NULL), scene(NULL), objectManager(NULL)
    {
    }

    void PhEditorPhysicsManagerSystem::release()
    {
    }

    void PhEditorPhysicsManagerSystem::destroyRigidBody(PhEditorObject *_object)
    {
        PhRigidBody *rigidBody = _object->getRigidBody();
        if (rigidBody)
        {
            scene->removeActor(rigidBody);
            physics->destroyActor(rigidBody);
            _object->setRigidBody(NULL);
        }
    }

    void PhEditorPhysicsManagerSystem::setRigidDynamic(PhEditorObject *_object)
    {
        destroyRigidBody(_object);
        PhRigidBody *rigidBody = physics->createRigidBodyDynamic();
        scene->addActor(rigidBody);
        _object->setRigidBody(rigidBody);
    }

    void PhEditorPhysicsManagerSystem::setRigidStatic(PhEditorObject *_object)
    {
        destroyRigidBody(_object);
        PhRigidBody *rigidBody = physics->createRigidBodyStatic();
        scene->addActor(rigidBody);
        _object->setRigidBody(rigidBody);
    }

    void PhEditorPhysicsManagerSystem::addShape(PhEditorObject *_object)
    {
        PhShape *shape = physics->createShape();
        _object->getRigidBody()->addShape(shape);
    }

    void PhEditorPhysicsManagerSystem::removeShape(PhEditorObject *_object, uint32_t _index)
    {
        PhShape *shape = _object->getRigidBody()->getShape(_index);
        _object->getRigidBody()->removeShape(shape);
        physics->destroyShape(shape);
    }

    void PhEditorPhysicsManagerSystem::addSphere(PhEditorObject *_object, uint32_t _index)
    {
        PhGeometry *geom = physics->createSphereGeometry();
        _object->getRigidBody()->getShape(_index)->connectGeometry(geom);
    }

    void PhEditorPhysicsManagerSystem::addBox(PhEditorObject *_object, uint32_t _index)
    {
        PhGeometry *geom = physics->createBoxGeometry();
        _object->getRigidBody()->getShape(_index)->connectGeometry(geom);
    }

    void PhEditorPhysicsManagerSystem::addCapsule(PhEditorObject *_object, uint32_t _index)
    {
        PhGeometry *geom = physics->createCapsuleGeometry();
        _object->getRigidBody()->getShape(_index)->connectGeometry(geom);
    }

    void PhEditorPhysicsManagerSystem::addPlane(PhEditorObject *_object, uint32_t _index)
    {
        PhGeometry *geom = physics->createPlaneGeometry();
        _object->getRigidBody()->getShape(_index)->connectGeometry(geom);

        if (_object->getRigidBody()->getActorType() == PhActor::PhActor_RigidDynamic)
            dynamic_cast<PhRigidDynamic *>(_object->getRigidBody())->setFlags(RigidBodyKinematic, true);
    }

    void PhEditorPhysicsManagerSystem::removeGeometry(PhEditorObject *_object, uint32_t _indexShape, uint32_t _indexGeometry)
    {
        PhShape *shape = _object->getRigidBody()->getShape(_indexShape);
        PhGeometry *geom = shape->getGeometry(_indexGeometry);
        shape->disconnectGeometry(geom);
        physics->destroyGeometry(geom);
    }

    void PhEditorPhysicsManagerSystem::onInit()
    {
        physics = context->get<PhPhysics>();
        scene = context->get<PhScene>();
        objectManager = context->get<PhEditorObjectManagerSystem>();
    }

    void PhEditorPhysicsManagerSystem::onUpdate(float _deltaTime)
    {
        PhEditorObject *const *objects = objectManager->getTableEditorObjects();
        uint size = objectManager->getTableEditorObjectsSize();
        for (uint i = 0; i < size; i++)
        {
            if (objects[i] == NULL)
                continue;

            PhEditorObject *object = objects[i];
            PhRigidBody *rigidbody = object->getRigidBody();
            if (rigidbody != NULL && rigidbody->getActorType() == PhActor::PhActor_RigidDynamic)
            {
                PhRigidDynamic *rigidDynamic = dynamic_cast<PhRigidDynamic *>(rigidbody);
                PhRigidBodyDynamicFlags flags = rigidDynamic->getFlags();
                if ((flags & RigidBodyKinematic) == 0 && (flags & OrientationFeedback) != 0)
                    object->syncWithRigidbody();
            }
        }
    }
}