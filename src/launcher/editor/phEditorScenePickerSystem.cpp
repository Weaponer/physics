#include "launcher/editor/phEditorScenePickerSystem.h"

#include "common/phMacros.h"

#include "plugins/imgui/imgui.h"

#include "rendering/geSceneEntityPicker.h"

namespace phys
{
    PhEditorScenePickerSystem::PhEditorScenePickerSystem() : PhEditorSystem(),
                                                             createConnection(), destroyConnection(),
                                                             indexes(), selectObject(NULL), isBeginPress(false)
    {
        PhEditorObject *nullItem = NULL;
        uint index = 0;
        indexes.assignmentMemoryAndCopy(&nullItem, index);
        indexes.setUseDefault(true);
        indexes.setDefault(&nullItem);
    }

    void PhEditorScenePickerSystem::release()
    {
        indexes.release();
    }

    void PhEditorScenePickerSystem::selectEditorObject(PhEditorObject *_object)
    {
        if (selectObject != NULL)
        {
            selectObject->getRenderer()->setSelected(false);
            selectObject = NULL;
            context->get<PhUnselectObjectSig>()->signal();
        }

        selectObject = _object;
        if (selectObject != NULL)
        {
            selectObject->getRenderer()->setSelected(true);
            context->get<PhSelectObjectSig>()->signal(selectObject);
        }
    }

    void PhEditorScenePickerSystem::registerRenderer(PhEditorObject *_object)
    {
        uint index = 0;
        ge::GeSelectableRenderer *r = _object->getRenderer();
        indexes.assignmentMemoryAndCopy(&_object, index);
        r->setIndexScenePicker(index);
    }

    void PhEditorScenePickerSystem::unregisterRenderer(PhEditorObject *_object)
    {
        if (_object == selectObject)
        {
            selectObject = NULL;
            context->get<PhUnselectObjectSig>()->signal();
        }

        ge::GeSelectableRenderer *r = _object->getRenderer();
        uint index = r->getIndexScenePicker();

        PH_ASSERT(index != 0, "Renderer waasn`t register into scene picker.");
        indexes.popMemory(index);
        r->setIndexScenePicker(0);
    }

    void PhEditorScenePickerSystem::onInit()
    {
        window = context->get<Window>();
        scene = context->get<ge::GeScene>();
        mouseState = context->get<PhEditorMouseState>();

        createConnection = context->get<PhCreateObjectSig>()->bind(this, &PhEditorScenePickerSystem::registerRenderer);
        destroyConnection = context->get<PhDestroyObjectSig>()->bind(this, &PhEditorScenePickerSystem::unregisterRenderer);
    }

    void PhEditorScenePickerSystem::onUpdate(float _deltaTime)
    {
        ge::GeSceneEntityPicker *picker = scene->getSceneEntityPicker();

        ImVec2 mousePos = ImGui::GetMousePos();

        mth::vec2 normalizedPos = mth::vec2(mousePos.x / window->getWidth(), mousePos.y / window->getHeight());
        normalizedPos.x = std::min(1.0f, std::max(0.0f, normalizedPos.x));
        normalizedPos.y = std::min(1.0f, std::max(0.0f, normalizedPos.y));

        picker->setCheckCoordinates(normalizedPos);

        if (ImGui::IsMouseDown(ImGuiMouseButton_Left) &&
            mouseState->tryLock(PhEditorMouseState::PhEditorMouseState_ObjectSelect, PhEditorMouseState::PhEditorMouseState_ObjectSelect) &&
            !isBeginPress)
        {
            isBeginPress = true;
        }

        if (ImGui::IsMouseReleased(ImGuiMouseButton_Left) &&
            isBeginPress)
        {
            isBeginPress = false;

            uint index = picker->getLastSelectedObject();
            if (selectObject != NULL)
            {
                selectObject->getRenderer()->setSelected(false);
                selectObject = NULL;
                context->get<PhUnselectObjectSig>()->signal();
            }

            mouseState->tryLock(PhEditorMouseState::PhEditorMouseState_ObjectSelect, PhEditorMouseState::PhEditorMouseState_None);

            selectObject = *indexes.getMemory(index);
            if (selectObject != NULL)
            {
                selectObject->getRenderer()->setSelected(true);
                context->get<PhSelectObjectSig>()->signal(selectObject);
            }
        }
    }

    void PhEditorScenePickerSystem::onCleanup()
    {
        context->get<PhCreateObjectSig>()->unbind(createConnection);
        context->get<PhDestroyObjectSig>()->unbind(destroyConnection);

        if (selectObject != NULL)
            selectObject->getRenderer()->setSelected(false);

        selectObject = NULL;
    }
}