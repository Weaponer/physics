#include "launcher/editor/phEditor.h"

#include "launcher/editor/phEditorSignals.h"

namespace phys
{
    PhEditor::PhEditor() : editorContext(), mouseState(), physicsSimulationSettings(),
                           guiSystem(), cameraControl(), pickerSystem(), objectManagerSystem(), physicsManager()
    {
    }

    void PhEditor::release()
    {
        physicsManager.cleanup();
        physicsManager.release();
        spawnSystem.cleanup();
        spawnSystem.release();
        pivotSystem.cleanup();
        pivotSystem.release();
        pickerSystem.cleanup();
        pickerSystem.release();
        objectManagerSystem.cleanup();
        objectManagerSystem.release();
        cameraControl.cleanup();
        cameraControl.release();
        guiSystem.cleanup();
        guiSystem.release();

        editorContext.releaseBind<PhDestroyObjectSig>();
        editorContext.releaseBind<PhCreateObjectSig>();
        editorContext.releaseBind<PhUnselectObjectSig>();
        editorContext.releaseBind<PhSelectObjectSig>();
        editorContext.releaseBind<PhChangePivotModeSig>();

        editorContext.release();
    }

    void PhEditor::init(const PhEditorInitContext &_context)
    {
        editorContext.bind<Window>(_context.window);
        editorContext.bind<ge::GeScene>(_context.scene);
        editorContext.bind<PhResourceProvider>(_context.resourceProvider);
        editorContext.bind<PhPhysics>(_context.physics);
        editorContext.bind<PhScene>(_context.physicsScene);

        editorContext.bind<PhEditorMouseState>(&mouseState);
        editorContext.bind<PhEditorPhysicsSimulationSettings>(&physicsSimulationSettings);

        editorContext.bind<PhEditorGuiSystem>(&guiSystem);
        editorContext.bind<PhCameraControlSystem>(&cameraControl);
        editorContext.bind<PhEditorObjectManagerSystem>(&objectManagerSystem);
        editorContext.bind<PhEditorScenePickerSystem>(&pickerSystem);
        editorContext.bind<PhEditorPivotSystem>(&pivotSystem);
        editorContext.bind<PhEditorSpawnObjectSystem>(&spawnSystem);
        editorContext.bind<PhEditorPhysicsManagerSystem>(&physicsManager);

        editorContext.bindDefault<PhSelectObjectSig>();
        editorContext.bindDefault<PhUnselectObjectSig>();
        editorContext.bindDefault<PhCreateObjectSig>();
        editorContext.bindDefault<PhDestroyObjectSig>();
        editorContext.bindDefault<PhChangePivotModeSig>();

        guiSystem.init(&editorContext);
        cameraControl.init(&editorContext);
        objectManagerSystem.init(&editorContext);
        pickerSystem.init(&editorContext);
        pivotSystem.init(&editorContext);
        spawnSystem.init(&editorContext);
        physicsManager.init(&editorContext);


        //physics settings
        physicsSimulationSettings.simulate = false;
        physicsSimulationSettings.doOneStep = false;
        physicsSimulationSettings.useDeltaTime = false;
        physicsSimulationSettings.timeStep = 0.017f;
        physicsSimulationSettings.simulationSettings.countIterations = 8;
        physicsSimulationSettings.simulationSettings.countSubGraphs = 8;


        //

        objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Floor);

        srand((unsigned)time(0));

        // ground
        /*{
            PhEditorObject *obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
            obj->setName("Ground");
            obj->setPosition(mth::vec3(0, 0, 0));
            mth::quat r = mth::quat();
            //r.setEuler(1, 0, 0);
            obj->setRotation(r);
            obj->setSize(mth::vec3(1000, 1, 1000));
            PhRigidBody *rigidbody = obj->getRigidBody();
            PhRigidDynamic *dyn = dynamic_cast<PhRigidDynamic *>(rigidbody);
            dyn->setFlags(RigidBodyKinematic, true);
            dynamic_cast<PhBoxGeometry *>(dyn->getShape(0)->getGeometry(0))->setSize(obj->getSize());
        }
       const float offsetWalls = 12;
        {
            PhEditorObject *obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
            obj->setPosition(mth::vec3(6, 4, -2));
            mth::quat r = mth::quat();
            //r.setEuler(1, 0, 0);
            obj->setRotation(r);
            obj->setSize(mth::vec3(8, 10, 0.5));
            PhRigidBody *rigidbody = obj->getRigidBody();
            PhRigidDynamic *dyn = dynamic_cast<PhRigidDynamic *>(rigidbody);
            dyn->setFlags(RigidBodyKinematic, true);
            dynamic_cast<PhBoxGeometry *>(dyn->getShape(0)->getGeometry(0))->setSize(obj->getSize());
        }
        {
            PhEditorObject *obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
            obj->setPosition(mth::vec3(6, 4, 1 + offsetWalls));
            mth::quat r = mth::quat();
            //r.setEuler(1, 0, 0);
            obj->setRotation(r);
            obj->setSize(mth::vec3(8, 10, 0.5));
            PhRigidBody *rigidbody = obj->getRigidBody();
            PhRigidDynamic *dyn = dynamic_cast<PhRigidDynamic *>(rigidbody);
            dyn->setFlags(RigidBodyKinematic, true);
            dynamic_cast<PhBoxGeometry *>(dyn->getShape(0)->getGeometry(0))->setSize(obj->getSize());
        }
        {
            PhEditorObject *obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
            obj->setPosition(mth::vec3(1 + offsetWalls, 4, 6));
            mth::quat r = mth::quat();
            //r.setEuler(1, 0, 0);
            obj->setRotation(r);
            obj->setSize(mth::vec3(0.5, 10, 8));
            PhRigidBody *rigidbody = obj->getRigidBody();
            PhRigidDynamic *dyn = dynamic_cast<PhRigidDynamic *>(rigidbody);
            dyn->setFlags(RigidBodyKinematic, true);
            dynamic_cast<PhBoxGeometry *>(dyn->getShape(0)->getGeometry(0))->setSize(obj->getSize());
        }
        {
            PhEditorObject *obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
            obj->setPosition(mth::vec3(-2, 4, 6));
            mth::quat r = mth::quat();
            //r.setEuler(1, 0, 0);
            obj->setRotation(r);
            obj->setSize(mth::vec3(0.5, 10, 8));
            PhRigidBody *rigidbody = obj->getRigidBody();
            PhRigidDynamic *dyn = dynamic_cast<PhRigidDynamic *>(rigidbody);
            dyn->setFlags(RigidBodyKinematic, true);
            dynamic_cast<PhBoxGeometry *>(dyn->getShape(0)->getGeometry(0))->setSize(obj->getSize());
        }*/
        
        uint32_t index = 0;
        for (int x = 0; x < 5; x++)
        {
            for (int y = 0; y < 5; y++)
            {
                for (int i2 = 0; i2 < 5; i2++)
                {
                    PhEditorObject *obj;
                    obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
                    //PhRigidBody *rigidbody = obj->getRigidBody();
                    //PhRigidDynamic *dyn = dynamic_cast<PhRigidDynamic *>(rigidbody);
                    //dyn->setCenterOfMass(mth::vec3(0.5, 0.0, 0.0));
                    obj->setPosition(mth::vec3(x * 3, 1.99f * i2 + 0.99f, y * 3));
                    //obj->setSize(mth::vec3(1 + (5 - i2), 1, 1 + (5 - i2))); 

                    //PhRigidBody *rigidbody = obj->getRigidBody();
                    //PhRigidDynamic *dyn = dynamic_cast<PhRigidDynamic *>(rigidbody);
                    //dynamic_cast<PhBoxGeometry *>(dyn->getShape(0)->getGeometry(0))->setSize(obj->getSize());

                    obj->setName((std::string("Cube ") + std::to_string(index)).c_str());
                    index++;
                }
            }
        }
            /*for (int i2 = 0; i2 < 1; i2++)
            {
                for (int i3 = 0; i3 < 1; i3++)
                {
                    mth::vec3 coords = mth::vec3((float)(rand() % 10000) / 150.0f,
                                                 (float)(rand() % 10000) / 150.0f,
                                                 (float)(rand() % 10000) / 150.0f);

                    PhEditorObject *obj;
                    {
                        obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
                        obj->setPosition(mth::vec3(0, 1.9f * i * 2 + 2.8f, 0));
                        obj->setSize(mth::vec3(5, 1, 5));
                        PhRigidBody *rigidbody = obj->getRigidBody();
                        PhRigidDynamic *dyn = dynamic_cast<PhRigidDynamic *>(rigidbody);
                        dynamic_cast<PhBoxGeometry *>(dyn->getShape(0)->getGeometry(0))->setSize(obj->getSize());
                    }
                    {
                        obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
                        obj->setPosition(mth::vec3(0, 1.9f * i * 2 + 0.99f, 0) - mth::vec3(3, 0, 3));
                        obj->setSize(mth::vec3(1, 1, 1));
                        obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
                        obj->setPosition(mth::vec3(0, 1.9f * i * 2 + 0.99f, 0) + mth::vec3(3, 0, 3));
                        obj->setSize(mth::vec3(1, 1, 1));
                        obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
                        obj->setPosition(mth::vec3(0, 1.9f * i * 2 + 0.99f, 0) - mth::vec3(3, 0, -3));
                        obj->setSize(mth::vec3(1, 1, 1));
                        obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Cube);
                        obj->setPosition(mth::vec3(0, 1.9f * i * 2 + 0.99f, 0) + mth::vec3(3, 0, -3));
                        obj->setSize(mth::vec3(1, 1, 1));
                    }
                    // coords = mth::vec3(2.1f * i2, 1.9 * i3 + 1.9f, i * 2.1f);
                    // obj->setPosition(coords);
                    // obj->setName((std::string("Cube ") + std::to_string(index)).c_str());
                    index++;
                }
            }
        }*/
        /*{
            PhEditorObject *obj = objectManagerSystem.createEditorObject(PhEditorObjectManagerSystem::PhEditorObject_Capsule);
            obj->setPosition(mth::vec3(0, 2, 0));
            obj->setName((std::string("Capsule")).c_str());
        }*/
    }

    void PhEditor::update(float _deltaTime)
    {
        guiSystem.update(_deltaTime);
        cameraControl.update(_deltaTime);
        objectManagerSystem.update(_deltaTime);
        pivotSystem.update(_deltaTime);
        pickerSystem.update(_deltaTime);
        spawnSystem.update(_deltaTime);
        physicsManager.update(_deltaTime);
    }
}