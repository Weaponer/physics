#include "common/phMacros.h"

#include "foundation/phMemoryAllocator.h"

#include "physics/phPhysics.h"
#include "physics/internal/inFactory.h"

namespace phys
{
    PhPhysics::PhPhysics(PhFoundation *_foundation, PeFoundation *_peFoundation) : foundation(_foundation),
                                                                                   peFoundation(_peFoundation),
                                                                                   factory(NULL)

    {
        factory = new (phAllocateMemory(sizeof(InFactory))) InFactory(peFoundation);
    }

    PhPhysics *PhPhysics::createPhysics(PhFoundation *_foundation)
    {
        PeFoundation *peFoundation = PeFoundation::createPeFoundation(_foundation);
        return new (phAllocateMemory(sizeof(PhPhysics))) PhPhysics(_foundation, peFoundation);
    }

    void PhPhysics::destroyPhysics(PhPhysics *_physics)
    {
        PeFoundation::destroyPeFoundation(_physics->peFoundation);
        _physics->peFoundation = NULL;
        PH_RELEASE(_physics)
    }

    void PhPhysics::release()
    {
        PH_RELEASE(factory)
        PhBase::release();
    }
}