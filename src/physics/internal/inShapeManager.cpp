#include "physics/internal/inShapeManager.h"

namespace phys
{
    InShapeManager::InShapeManager() : InManager<InShape, PeGpuDataShape>()
    {
    }

    void InShapeManager::release()
    {
        InManager<InShape, PeGpuDataShape>::release();
    }
}