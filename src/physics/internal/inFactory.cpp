#include "physics/internal/inFactory.h"
#include "foundation/phMemoryAllocator.h"

#include "physics/internal/inRigidDynamic.h"
#include "physics/internal/inRigidStatic.h"
#include "physics/internal/inScene.h"
#include "physics/internal/inMaterial.h"
#include "geometry/internal/inShape.h"
#include "geometry/internal/inShapeGlobal.h"
#include "geometry/internal/inPlaneGeometry.h"
#include "geometry/internal/inBoxGeometry.h"
#include "geometry/internal/inSphereGeometry.h"
#include "geometry/internal/inCapsuleGeometry.h"

namespace phys
{
    InFactory::InFactory(PeFoundation *_peFoundation) : peFoundation(_peFoundation),
                                                        materialManager(_peFoundation->getMaterialManger()),
                                                        shapeManager(_peFoundation->getShapeManager())
    {
    }

    phys::PhRigidDynamic *InFactory::CREATE_RIGID_BODY_DYNAMIC()
    {
        return PH_NEW(InRigidDynamic)();
    }

    phys::PhRigidDynamic *InFactory::CREATE_RIGID_BODY_KINEMATIC()
    {
        InRigidDynamic *rigid = PH_NEW(InRigidDynamic)();
        rigid->setFlags(PhRigidBodyDynamicFlags::RigidBodyKinematic, true);
        return rigid;
    }

    phys::PhRigidStatic *InFactory::CREATE_RIGID_BODY_STATIC()
    {
        return PH_NEW(InRigidStatic)();
    }

    phys::PhShape *InFactory::CREATE_SHAPE()
    {
        return PH_NEW(InShapeGlobal)(shapeManager);
    }

    phys::PhPlaneGeometry *InFactory::CREATE_PLANE_GEOMETRY()
    {
        return PH_NEW(InPlaneGeometry)();
    }

    phys::PhBoxGeometry *InFactory::CREATE_BOX_GEOMETRY()
    {
        return PH_NEW(InBoxGeometry)();
    }

    phys::PhSphereGeometry *InFactory::CREATE_SPHERE_GEOMETRY()
    {
        return PH_NEW(InSphereGeometry)();
    }

    phys::PhCapsuleGeometry *InFactory::CREATE_CAPSULE_GEOMETRY()
    {
        return PH_NEW(InCapsuleGeometry)();
    }

    phys::PhMaterial *InFactory::CREATE_MATERIAL()
    {
        return PH_NEW(InMaterial)(materialManager);
    }

    phys::PhScene *InFactory::CREATE_SCENE()
    {
        PeSceneSimulator *simulator = peFoundation->getSceneSimulator();
        InScene *scene = PH_NEW(InScene)(simulator);
        simulator->addScene(scene);
        return scene;
    }

    void InFactory::DESTROY_ACTOR(phys::PhActor *_in)
    {
        InActor *actor = dynamic_cast<InActor *>(_in);
        PH_RELEASE(actor);
    }

    void InFactory::DESTROY_RIGID_BODY_DYNAMIC(phys::PhRigidDynamic *_in)
    {
        InRigidDynamic *rigidDynamic = dynamic_cast<InRigidDynamic *>(_in);
        PH_RELEASE(rigidDynamic);
    }

    void InFactory::DESTROY_RIGID_BODY_STATIC(phys::PhRigidStatic *_in)
    {
        InRigidStatic *rigidStatic = dynamic_cast<InRigidStatic *>(_in);
        PH_RELEASE(rigidStatic);
    }

    void InFactory::DESTROY_SHAPE(phys::PhShape *_in)
    {
        InShape *shape = dynamic_cast<InShape *>(_in);
        PH_RELEASE(shape);
    }

    void InFactory::DESTROY_GEOMETRY(phys::PhGeometry *_in)
    {
        InGeometryBase *base = dynamic_cast<InGeometryBase *>(_in);
        PH_RELEASE(base);
    }

    void InFactory::DESTROY_MATERIAL(phys::PhMaterial *_in)
    {
        PH_RELEASE(_in);
    }

    void InFactory::DESTROY_SCENE(phys::PhScene *_in)
    {
        PeSceneSimulator *simulator = peFoundation->getSceneSimulator();
        InScene *scene = dynamic_cast<InScene *>(_in);
        simulator->removeScene(scene);
        PH_RELEASE(scene);
    }

    void InFactory::release()
    {
        PhFactory::release();
    }
}