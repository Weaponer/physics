#include "inMaterialManager.h"
#include "physics/internal/inMaterial.h"
#include "common/phMacros.h"

namespace phys
{
    InMaterial::InMaterial(InMaterialManager *_manager)
        : PhMaterial(), managerRegistration(),
          staticFriction(1.0f), dynamicFriction(1.0f), bounciness(1.0f)
    {
        managerRegistration.connect(_manager, this);
    }

    void InMaterial::setStaticFriction(float _friction)
    {
        staticFriction = _friction;
        managerRegistration.onChange();
    }

    void InMaterial::setDynamicFriction(float _friction)
    {
        dynamicFriction = _friction;
        managerRegistration.onChange();
    }

    void InMaterial::setBounciness(float _bounciness)
    {
        bounciness = _bounciness;
        managerRegistration.onChange();
    }

    void InMaterial::release()
    {
        managerRegistration.disconnect();
        PhMaterial::release();
    }

}