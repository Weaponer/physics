#include "physics/internal/inOutputDebugData.h"
#include <algorithm>

namespace phys
{
    InOutputDebugData::InOutputDebugData()
        : PhOutputDebugData(), obbOrientaions(), keyOBBs(), contacts(), containers(), actorOrientations(), actorVelocity(),
          orientationGlobalBoundOBBs()
    {
    }

    void InOutputDebugData::release()
    {
        obbOrientaions.release();
        keyOBBs.release();
        contacts.release();
        containers.release();
        actorOrientations.release();
        actorVelocity.release();

        PhOutputDebugData::release();
    }

    void InOutputDebugData::clearData()
    {
        contacts.clearMemory();
        keyOBBs.clearMemory();
        obbOrientaions.clearMemory();
        containers.clearMemory();
        actorOrientations.clearMemory();
        actorVelocity.clearMemory();
    }

    uint32_t InOutputDebugData::getCountObbs() const
    {
        return obbOrientaions.getPos();
    }

    const InOutputDebugData::Orientation *InOutputDebugData::getObbsBuffer() const
    {
        return obbOrientaions.getMemory(0);
    }

    uint32_t InOutputDebugData::getCountKeyOBBs() const
    {
        return keyOBBs.getPos();
    }

    const InOutputDebugData::KeyOBB *InOutputDebugData::getKeyOBBsBuffer() const
    {
        return keyOBBs.getMemory(0);
    }

    const mth::mat4 &InOutputDebugData::getOrientationGlobalBoundOBBs() const
    {
        return orientationGlobalBoundOBBs;
    }

    uint32_t InOutputDebugData::getCountContact() const
    {
        return contacts.getPos();
    }

    const InOutputDebugData::Contact *InOutputDebugData::getContactBuffer() const
    {
        return contacts.getMemory(0);
    }

    const InOutputDebugData::ContainerContacts *InOutputDebugData::getContainersContacts() const
    {
        return containers.getMemory(0);
    }

    uint32_t InOutputDebugData::getCountContainersContacts() const
    {
        return containers.getPos();
    }

    uint32_t InOutputDebugData::getCountActorOrientations() const
    {
        return actorOrientations.getPos();
    }

    const InOutputDebugData::ActorOrientation *InOutputDebugData::getActorOrientations() const
    {
        return actorOrientations.getMemory(0);
    }

    uint32_t InOutputDebugData::getCountActorVelocity() const
    {
        return actorVelocity.getPos();
    }

    const InOutputDebugData::ActorVelocity *InOutputDebugData::getActorVelocity() const
    {
        return actorVelocity.getMemory(0);
    }

    void InOutputDebugData::addContact(mth::vec3 _firstPosition, mth::vec3 _secondPosition, mth::vec3 _firstOrigin, mth::vec3 _secondOrigin)
    {
        Contact contact;
        contact.firstPosition = _firstPosition;
        contact.secondPosition = _secondPosition;
        contact.firstOrigin = _firstOrigin;
        contact.secondOrigin = _secondOrigin;

        contacts.assignmentMemoryAndCopy(&contact);
    }

    void InOutputDebugData::addObbOrientation(const mth::quat &_rot, const mth::vec3 &_size, const mth::vec3 &_pos,
                                              int _indexOBBsOfActor, int _indexActor, int _indexDynamicActor)
    {
        Orientation orientation;
        orientation.rotation = _rot;
        orientation.size = _size;
        orientation.position = _pos;
        orientation.indexOBBsOfActor = _indexOBBsOfActor;
        orientation.indexActor = _indexActor;
        orientation.indexDynamicActor = _indexDynamicActor;
        orientation.isDynamic = _indexDynamicActor >= 0;

        obbOrientaions.assignmentMemoryAndCopy(&orientation);
    }

    void InOutputDebugData::addKeyOBB(uint32_t _index, uint32_t _key)
    {
        KeyOBB key;
        key.indexOBBs = _index;
        key.key = _key;
        keyOBBs.assignmentMemoryAndCopy(&key);
    }

    void InOutputDebugData::setOrientationGlobalBoundOBBs(const mth::mat4 &_mat)
    {
        orientationGlobalBoundOBBs = _mat;
    }

    void InOutputDebugData::addContactsContainer(const InOutputDebugData::ContainerContacts &_container)
    {
        containers.assignmentMemoryAndCopy(&_container);
    }

    void InOutputDebugData::setIndexGraphGeneralContainer(uint32_t _index, int32_t _indexGraph)
    {
        InOutputDebugData::ContainerContacts *container = containers.getMemory(_index);
        container->edgeIndex = _indexGraph;
    }

    void InOutputDebugData::addActorOrientation(const mth::quat &_rot, const mth::vec3 &_pos)
    {
        ActorOrientation ort{};
        ort.position = _pos;
        ort.rotation = _rot;
        actorOrientations.assignmentMemoryAndCopy(&ort);
    }

    void InOutputDebugData::addActorVelocity(const mth::vec3 &_linearVelocity, const mth::vec3 &_angularVelocity,
                                             bool _sleeping, float _timeSleeping)
    {
        ActorVelocity vel{};
        vel.linearVelocity = _linearVelocity;
        vel.angularVelocity = _angularVelocity;
        vel.sleeping = _sleeping;
        vel.timeSleeping = _timeSleeping;
        actorVelocity.assignmentMemoryAndCopy(&vel);
    }
}