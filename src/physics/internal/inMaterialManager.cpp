#include "physics/internal/inMaterialManager.h"

namespace phys
{
    InMaterialManager::InMaterialManager() : InManager<InMaterial, uint8_t>()
    {
    }

    void InMaterialManager::release()
    {
        InManager<InMaterial, uint8_t>::release();
    }
}