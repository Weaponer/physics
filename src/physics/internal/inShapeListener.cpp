#include "physics/internal/inShapeListener.h"
#include "geometry/internal/inShape.h"
#include "common/phMacros.h"

namespace phys
{
    void InShapeListenerBase::registerShape(InShape *_shape)
    {
        int count = countShapes();
        for (int i = 0; i < count; i++)
        {
            PH_ASSERT(shapes.getElement(i).shape != _shape, PH_THIS_SHAPE_ALLREADY_CONNECT)
        }

        RegistredShape dataRegistration;
        dataRegistration.shape = _shape;
        dataRegistration.indexRegister = _shape->addShapeRegister(this);
        shapes.pushElement(&dataRegistration);
        _shape->incReference(_shape);
    }

    void InShapeListenerBase::unregisterShape(InShape *_shape)
    {
        int count = countShapes();
        for (int i = 0; i < count; i++)
        {
            if (shapes.getElement(i).shape == _shape)
            {
                unregisterShape(i);
                return;
            }
        }
    }

    void InShapeListenerBase::unregisterShape(int _index)
    {
        RegistredShape dataRegistration = shapes.getElement(_index);
        dataRegistration.shape->removeShapeRegister(dataRegistration.indexRegister);
        shapes.popElement(_index);
        dataRegistration.shape->decReference(dataRegistration.shape);
    }

    InShape *InShapeListenerBase::getShape(int _index) const
    {
        return shapes.getElement(_index).shape;
    }

    int InShapeListenerBase::countShapes() const
    {
        return shapes.countElements();
    }

    void InShapeListenerBase::release()
    {
        int count = countShapes();
        for (int i = 0; i < count; i++)
        {
            RegistredShape dataRegistration = shapes.getElement(i);
            dataRegistration.shape->removeShapeRegister(dataRegistration.indexRegister);

            dataRegistration.shape->decReference(dataRegistration.shape);
            PH_REF_RELEASE(dataRegistration.shape);
        }

        shapes.release();
    }
}