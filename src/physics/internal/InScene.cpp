#include "physics/phActor.h"

#include "physics/internal/inScene.h"
#include "physics/internal/inActor.h"

#include "physicsEngine/peSceneSimulator.h"
#include "physicsEngine/peLimits.h"

namespace phys
{
    InScene::InScene(PeSceneSimulator *_simulator) : settings(), simulator(_simulator), data(NULL), wasLaunched(false)
    {
    }

    void InScene::addActor(PhActor *_actor)
    {
        InActor *actor = dynamic_cast<InActor *>(_actor);
        PH_ASSERT(!actor->scene, "This actor has already connected to scene.");
        PH_ASSERT(data->actorManager.getCountObjects() < PE_MAX_COUNT_ACTORS_IN_SCENE, "There are maximum of actors.")

        actor->scene = this;
        actor->managerRegistration.connect(&data->actorManager, actor);
    }

    void InScene::removeActor(PhActor *_actor)
    {
        InActor *actor = dynamic_cast<InActor *>(_actor);
        PH_ASSERT(actor->scene == this, "This actor hasn`t connected to this scene.");

        actor->managerRegistration.disconnect();
        actor->scene = NULL;
    }

    void InScene::simulation(float _deltaTime, const PhSimulationSettings &_settings)
    {
        simulator->simulate(this, _deltaTime, _settings);
        wasLaunched = true;
    }

    void InScene::fetch(bool _waitFinish)
    {
        wasLaunched = false;
        simulator->fetchScene(this, _waitFinish);
    }

    PhSceneSettings *InScene::getSceneSettings()
    {
        return &settings;
    }

    const PhSceneSettings &InScene::getSceneSettings() const
    {
        return settings;
    }

    void InScene::setSceneSettings(const PhSceneSettings &_settings)
    {
        settings = _settings;
    }

    PhSettingsFeedbackDebugData *InScene::getSettingsFeedbackDebug()
    {
        return &data->settingsFeedbackDebugData;
    }

    const PhSettingsFeedbackDebugData &InScene::getSettingsFeedbackDebug() const
    {
        return data->settingsFeedbackDebugData;
    }

    void InScene::setSettingsFeedbackDebug(const PhSettingsFeedbackDebugData &_settings)
    {
        data->settingsFeedbackDebugData = _settings;
    }

    const PhOutputDebugData *InScene::getOutputDebugData() const
    {
        return &data->outputDebugData;
    }

    void InScene::release()
    {
        PhScene::release();
    }
}