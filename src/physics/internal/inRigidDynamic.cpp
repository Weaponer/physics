#include "physics/internal/inRigidDynamic.h"
#include "physics/enums.h"

namespace phys
{
    void InRigidDynamic::setFlags(PhRigidBodyDynamicFlags _flag, bool _enable)
    {
        PhRigidBodyDynamicFlags oldFlags = dynamicFlags;
        if (_enable)
            dynamicFlags = (PhRigidBodyDynamicFlags)(dynamicFlags | _flag);
        else
            dynamicFlags = (PhRigidBodyDynamicFlags)(dynamicFlags & (~_flag));

        if (oldFlags != dynamicFlags)
            managerRegistration.onChange(PhStateActorChangesFlags::ChangeKinematicFlag);
    }

    void InRigidDynamic::setMass(float _mass)
    {
        PH_ASSERT(_mass > 0.001f, "The mass is zero.");
        mass = _mass;
        managerRegistration.onChange(PhStateActorChangesFlags::ChangeMassCenter);
    }

    void InRigidDynamic::setCenterOfMass(mth::vec3 _localP)
    {
        localCenterOfMass = _localP;
        managerRegistration.onChange(PhStateActorChangesFlags::ChangeMassCenter);
    }
}