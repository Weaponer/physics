#include <vulkan/vulkan.h>
#include <vector>
#include <iostream>

#include "gpuCommon/gpuFoundation.h"
#include "deviceSelector.h"
#include "gpuCommon/memory/memoryTypeDescriptions.h"

#include "tests/gpuTester.h"

namespace phys
{
    GPUFoundation::GPUFoundation(PhVulkanLibraryProvider *_vulkanLibProvider, const GPUFoundationSettings *_setting, Window *_window)
        : vulkanLibProvider(_vulkanLibProvider), window(_window),
          library(NULL), mainFunctions(NULL), instance(NULL), logicalDevice(NULL), memoryController(NULL)
    {

        library = new vk::VulkanExportedLibrary(vulkanLibProvider);
        mainFunctions = new vk::VulkanMainFunctions(library);

        const char *needLayers[1] = {"VK_LAYER_KHRONOS_validation"};
        std::vector<const char *> layers = mainFunctions->getSupportedLayers(1, needLayers);
        uint countExtensions = 0;
        if (window != NULL)
        {
            window->getExtensions(&countExtensions, NULL);
        }
        int countNeedExtensions = countExtensions + 1;
        const char *needExtensions[countNeedExtensions];
        needExtensions[0] = VK_EXT_DEBUG_REPORT_EXTENSION_NAME;

        if (window != NULL)
        {
            window->getExtensions(&countExtensions, &(needExtensions[countNeedExtensions - countExtensions]));
        }
        std::vector<const char *> extensions = mainFunctions->getSupportedExtensions(countNeedExtensions, needExtensions);

        VkInstanceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.enabledLayerCount = layers.size();
        createInfo.ppEnabledLayerNames = layers.data();
        createInfo.enabledExtensionCount = extensions.size();
        createInfo.ppEnabledExtensionNames = extensions.data();

        VkApplicationInfo applicationInfo;
        applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationInfo.pApplicationName = "Physics-Vulkan";
        applicationInfo.pEngineName = "Physics-Vulkan";
        applicationInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        applicationInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        applicationInfo.pNext = NULL;
        applicationInfo.apiVersion = VK_API_VERSION_1_3;
        createInfo.pApplicationInfo = &applicationInfo;
        instance = new vk::VulkanInstance(createInfo, library, mainFunctions);
        if (window != NULL)
        {
            window->connectVulkanInstance(instance);
        }

        enableDebugMessage();
        const char *needDeviceExtensions[4] = {
            VK_EXT_DEBUG_MARKER_EXTENSION_NAME,
            VK_KHR_VULKAN_MEMORY_MODEL_EXTENSION_NAME,
            VK_EXT_SHADER_ATOMIC_FLOAT_EXTENSION_NAME,
            VK_KHR_SWAPCHAIN_EXTENSION_NAME};

        VkPhysicalDeviceVulkanMemoryModelFeatures memoryFeatures{};
        memoryFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES;
        memoryFeatures.vulkanMemoryModel = true;
        memoryFeatures.vulkanMemoryModelDeviceScope = true;
        memoryFeatures.vulkanMemoryModelAvailabilityVisibilityChains = false;

        VkPhysicalDeviceShaderAtomicFloatFeaturesEXT atomicFloatFeatures{};
        atomicFloatFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT;
        atomicFloatFeatures.shaderSharedFloat32AtomicAdd = true;
        atomicFloatFeatures.shaderSharedFloat32Atomics = true;

        memoryFeatures.pNext = &atomicFloatFeatures;

        if (window != NULL)
            createDevice(_setting, 1, needLayers, 4, needDeviceExtensions, &memoryFeatures);
        else
            createDevice(_setting, 1, needLayers, 2, needDeviceExtensions);

        std::cout << "Device: " << std::string(logicalDevice->getProperties().deviceName) << std::endl;

        createMemoryController();
        createCommandReceiver(_setting);

        if (window != NULL)
        {
            window->connectDevice(logicalDevice, commandReceiver->getFreeQueue(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT));
        }
    }

    GPUFoundation::~GPUFoundation()
    {
        delete commandReceiver;
        delete imageAllocator;
        delete bufferAllocator;
        delete memoryController;
        delete logicalDevice;
        
        if (instance->vkDestroyDebugUtilsMessengerEXT != NULL)
            instance->vkDestroyDebugUtilsMessengerEXT(*instance, debugMessenger, NULL);
        else if (instance->vkDestroyDebugReportCallbackEXT != NULL)
            instance->vkDestroyDebugReportCallbackEXT(*instance, debugReport, NULL);

        delete instance;
        instance = NULL;
        delete mainFunctions;
        mainFunctions = NULL;
        delete library;
        library = NULL;
    }

    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                                                        VkDebugUtilsMessageTypeFlagsEXT messageType,
                                                        const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
                                                        void *pUserData)
    {
        std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;
        if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
        {
            exit(1);
        }
        return VK_FALSE;
    }

    static VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objectType,
                                                              uint64_t object, size_t location, int32_t messageCode, const char *pLayerPrefix, const char *pMessage, void *pUserData)
    {
        std::cout << "[VK_DEBUG_REPORT] ";
        if (messageCode & VK_DEBUG_REPORT_ERROR_BIT_EXT)
            std::cout << "ERROR";
        else if (messageCode & VK_DEBUG_REPORT_WARNING_BIT_EXT)
            std::cout << "WARNING";
        else if (messageCode & VK_DEBUG_REPORT_INFORMATION_BIT_EXT)
            std::cout << "INFORMATION";
        else if (messageCode & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT)
            std::cout << "PERFORMANCE";
        else if (messageCode & VK_DEBUG_REPORT_DEBUG_BIT_EXT)
            std::cout << "DEBUG";

        std::cout << ": [" << pLayerPrefix << "] Code" << messageCode << ":" << pMessage << std::endl;
        return VK_TRUE;
    }

    void GPUFoundation::enableDebugMessage()
    {
        if (instance->vkCreateDebugUtilsMessengerEXT != NULL)
        {
            VkDebugUtilsMessengerCreateInfoEXT createInfo{};
            createInfo.sType =
                VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
            createInfo.messageSeverity =
                VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
            createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                                     VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
            createInfo.pfnUserCallback = debugCallback;
            createInfo.pUserData = NULL;

            if (instance->vkCreateDebugUtilsMessengerEXT(*instance, &createInfo, NULL, &debugMessenger) != VK_SUCCESS)
            {
                std::cout << "Can`t create debug utils messenger" << std::endl;
            }
        }
        else if (instance->vkCreateDebugReportCallbackEXT != NULL)
        {
            VkDebugReportCallbackCreateInfoEXT createInfo{};
            createInfo.pNext = NULL;
            createInfo.pUserData = NULL;
            createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
            createInfo.flags = VK_DEBUG_REPORT_INFORMATION_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT |
                               VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT | VK_DEBUG_REPORT_ERROR_BIT_EXT |
                               VK_DEBUG_REPORT_DEBUG_BIT_EXT;
            createInfo.pfnCallback = debugReportCallback;
            if (instance->vkCreateDebugReportCallbackEXT(*instance, &createInfo, NULL, &debugReport) != VK_SUCCESS)
            {
                std::cout << "Can`t create debug report callback" << std::endl;
            }
        }
    }

    void GPUFoundation::createDevice(const GPUFoundationSettings *_setting, int _countLayers, const char **_layerNames, int _countExtensions, const char **_extensionNames, void *_pExtensionData)
    {
        vk::DeviceSelector deviceSelector;
        deviceSelector.addRequireQueueType((VkQueueFlagBits)(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT), _setting->minCountComputeQueues);
        if (window != NULL)
        {
            deviceSelector.addRequireQueueType((VkQueueFlagBits)(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT), _setting->minCountGraphicQueues,
                                               true, window->getSurface());
        }

        vk::VulkanMemoryDescriptor memoryDescriptor;
        deviceSelector.addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_Staging));
        deviceSelector.addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformBlockDynamic));
        deviceSelector.addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformBlockStatic));
        deviceSelector.addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformStorageBufferStatic));
        deviceSelector.addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformStorageBufferDynamic));
        deviceSelector.addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanImageType_Staging));
        deviceSelector.addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanImageType_ImageDynamic));
        deviceSelector.addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanImageType_ImageStatic));

        VkPhysicalDevice physDevice = deviceSelector.getDevice(instance);
        if (physDevice == VK_NULL_HANDLE)
        {
            std::cerr << "Imposible choose device." << std::endl;
        }

        for (int i = 0; i < _countLayers; i++)
            deviceSelector.addRequireLayer(_layerNames[i]);

        for (int i = 0; i < _countExtensions; i++)
            deviceSelector.addRequireExtension(_extensionNames[i]);

        vk::VulkanDevice *device = deviceSelector.createLogicalDevice(physDevice, instance, _pExtensionData);
        if (device == NULL)
        {
            std::cerr << "Can`t create device." << std::endl;
            return;
        }

        logicalDevice = device;
    }

    void GPUFoundation::createMemoryController()
    {
        vk::VulkanMemoryDescriptor memoryDescriptor;
        memoryController = new vk::GpuMemoryController(logicalDevice);
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_Staging));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformBlockDynamic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformBlockStatic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformStorageBufferStatic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformStorageBufferDynamic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_VertexBufferStatic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_VertexBufferDynamic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_IndexBufferStatic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_IndexBufferDynamic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_IndirectBufferDynamic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_IndirectBufferStatic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanImageType::VulkanImageType_Staging));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanImageType::VulkanImageType_ImageStatic));
        memoryController->addRequireMemoryType(memoryDescriptor.getRequireMemoryType(vk::VulkanImageType::VulkanImageType_ImageDynamic));

        memoryController->prepareMemory();

        bufferAllocator = new vk::VulkanBufferAllocator(logicalDevice, memoryController);
        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType::VulkanBufferType_Staging,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_Staging));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType::VulkanBufferType_Staging,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType::VulkanBufferType_UniformBlockDynamic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformBlockDynamic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType::VulkanBufferType_UniformBlockDynamic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType::VulkanBufferType_UniformBlockStatic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType::VulkanBufferType_UniformBlockStatic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType::VulkanBufferType_UniformBlockStatic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType_UniformStorageBufferStatic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType_UniformStorageBufferStatic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType_UniformStorageBufferStatic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType_UniformStorageBufferDynamic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType_UniformStorageBufferDynamic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType_UniformStorageBufferDynamic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType_VertexBufferStatic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType_VertexBufferStatic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType_VertexBufferStatic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType_VertexBufferDynamic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType_VertexBufferDynamic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType_VertexBufferDynamic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType_IndexBufferStatic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType_IndexBufferStatic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType_IndexBufferStatic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType_IndexBufferDynamic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType_IndexBufferDynamic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType_IndexBufferDynamic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType_IndirectBufferStatic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType_IndirectBufferStatic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType_IndirectBufferStatic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT);

        bufferAllocator->setTypeMemoryForTypeBuffer(vk::VulkanBufferType_IndirectBufferDynamic,
                                                    memoryDescriptor.getRequireMemoryType(vk::VulkanBufferType_IndirectBufferDynamic));
        bufferAllocator->setUsageForTypeBuffer(vk::VulkanBufferType_IndirectBufferDynamic,
                                               VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT);

        imageAllocator = new vk::VulkanImageAllocator(logicalDevice, memoryController);

        imageAllocator->setSettingForTypeImage(vk::VulkanImageType_Staging, VK_IMAGE_TILING_LINEAR,
                                               VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
                                               memoryDescriptor.getRequireMemoryType(vk::VulkanImageType_Staging));

        imageAllocator->setSettingForTypeImage(vk::VulkanImageType_ImageStatic, VK_IMAGE_TILING_OPTIMAL,
                                               VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT,
                                               memoryDescriptor.getRequireMemoryType(vk::VulkanImageType_ImageStatic));

        imageAllocator->setSettingForTypeImage(vk::VulkanImageType_ImageDynamic, VK_IMAGE_TILING_LINEAR,
                                               VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT,
                                               memoryDescriptor.getRequireMemoryType(vk::VulkanImageType_ImageDynamic));
    }

    void GPUFoundation::createCommandReceiver(const GPUFoundationSettings *_setting)
    {
        commandReceiver = new vk::VulkanCommandReceiver(logicalDevice);
        if (window != NULL)
        {
            for (int i = 0; i < _setting->minCountGraphicQueues; i++)
            {
                vk::VulkanQueue *graphicQueue = logicalDevice->getFreeQueue(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT);
                commandReceiver->addQueue(graphicQueue);
            }
        }
        for (int i = 0; i < _setting->minCountComputeQueues; i++)
        {
            commandReceiver->addQueue(logicalDevice->getFreeQueue(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT));
        }
    }
}