#include "gpuCommon/specialCommands/vulkanStateContainers.h"

namespace phys::vk
{
    VulkanBufferDescriptor BufferStateContainer::addBuffer(VulkanBuffer *buffer, VkAccessFlags _accessFlags,
                                                           int _queueFamilyIndex, SpaceUsing _spaceUsing)
    {
        BufferState *state = new BufferState();
        state->accessFlags = _accessFlags;
        state->queueFamilyIndex = _queueFamilyIndex;
        state->spaceUsing = _spaceUsing;
        states.push_back(state);
        VulkanBufferDescriptor descriptor;

        descriptor.state = states[states.size() - 1];
        descriptor.vulkanBuffer = buffer;
        return descriptor;
    }

    void BufferStateContainer::clear()
    {
        for (auto i : states)
        {
            delete i;
        }
        states.clear();
    }
}