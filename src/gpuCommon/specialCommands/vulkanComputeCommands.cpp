#include "gpuCommon/specialCommands/vulkanComputeCommands.h"

namespace phys::vk
{
    VulkanComputeCommands::VulkanComputeCommands()
        : VulkanTransferCommands(false)
    {
    }

    VulkanComputeCommands::VulkanComputeCommands(VulkanQueue *_queue)
        : VulkanTransferCommands(_queue, false)
    {
    }

    VulkanComputeCommands::VulkanComputeCommands(VulkanQueue *_queue, VulkanCommandPool *_commandPool)
        : VulkanTransferCommands(_queue, _commandPool, false)
    {
    }

    void VulkanComputeCommands::bindPipeline(VkPipeline _pipeline)
    {
        device->vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, _pipeline);
    }

    void VulkanComputeCommands::bindDescriptorSet(VkPipelineLayout _layout, VkDescriptorSet _set, int _indexBinding)
    {
        device->vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, _layout, _indexBinding, 1, &_set, 0, NULL);
    }

    void VulkanComputeCommands::dispatch(uint32_t _groupCountX, uint32_t _groupCountY, uint32_t _groupCountZ)
    {
        device->vkCmdDispatch(cmd, _groupCountX, _groupCountY, _groupCountZ);
    }

    void VulkanComputeCommands::dispatchIndirect(VulkanBuffer *_buffer, VkDeviceSize _offset)
    {
        device->vkCmdDispatchIndirect(cmd, *_buffer, _offset);
    }

    void VulkanComputeCommands::pushConstant(VkPipelineLayout _layout, uint32_t _offset, uint32_t _size, const void *_pValues)
    {
        device->vkCmdPushConstants(cmd, _layout, VK_SHADER_STAGE_COMPUTE_BIT, _offset, _size, _pValues);
    }
}