#include <cstring>
#include <iostream>

#include "gpuCommon/specialCommands/vulkanTransferCommands.h"

namespace phys::vk
{
    VulkanTransferCommands::VulkanTransferCommands(bool _useRenderCommands)
        : VulkanSpecialCommands(_useRenderCommands)
    {
    }

    VulkanTransferCommands::VulkanTransferCommands(VulkanQueue *_queue, bool _useRenderCommands)
        : VulkanSpecialCommands(_queue, _useRenderCommands)
    {
    }

    VulkanTransferCommands::VulkanTransferCommands(VulkanQueue *_queue, VulkanCommandPool *_commandPool, bool _useRenderCommands)
        : VulkanSpecialCommands(_queue, _commandPool, _useRenderCommands)
    {
    }

    VulkanTransferCommands::~VulkanTransferCommands()
    {
    }

    void VulkanTransferCommands::writeToBuffer(VulkanBuffer *_buffer, int _offset, int _size, const void *_data)
    {
        void *writeData = NULL;
        DeviceMemory *deviceMemory = _buffer->getDeviceMemory();
        device->vkMapMemory(*device, *deviceMemory, _offset + deviceMemory->getOffsetAlignment(), _size, 0, &writeData);
        std::memcpy(writeData, _data, _size);
        device->vkUnmapMemory(*device, *deviceMemory);
    }

    void *VulkanTransferCommands::beginWrite(VulkanBuffer *_buffer, int _offset, int _size)
    {
        void *writeData = NULL;
        DeviceMemory *deviceMemory = _buffer->getDeviceMemory();
        device->vkMapMemory(*device, *deviceMemory, _offset + deviceMemory->getOffsetAlignment(), _size, 0, &writeData);
        return writeData;
    }

    void VulkanTransferCommands::endWrite(VulkanBuffer *_buffer)
    {
        device->vkUnmapMemory(*device, *_buffer->getDeviceMemory());
    }

    void VulkanTransferCommands::readFromBuffer(VulkanBuffer *_buffer, int _offset, int _size, void *_data)
    {
        void *readData = NULL;
        DeviceMemory *deviceMemory = _buffer->getDeviceMemory();
        device->vkMapMemory(*device, *deviceMemory, _offset + deviceMemory->getOffsetAlignment(), _size, 0, &readData);
        std::memcpy(_data, readData, _size);
        device->vkUnmapMemory(*device, *deviceMemory);
    }

    void VulkanTransferCommands::copyBuffer(VulkanBuffer *_srcBuffer, int _srcOffset, VulkanBuffer *_dstBuffer,
                                            int _dstOffset, int _size)
    {
        VkBufferCopy bufferCopy{};
        bufferCopy.size = _size;
        bufferCopy.srcOffset = _srcOffset;
        bufferCopy.dstOffset = _dstOffset;
        device->vkCmdCopyBuffer(cmd, *_srcBuffer, *_dstBuffer, 1, &bufferCopy);
    }

    void VulkanTransferCommands::writeToImage(VulkanImage *_image, int _offset, int _size, const void *_data)
    {
        void *writeData = NULL;
        DeviceMemory *deviceMemory = _image->getDeviceMemory();
        device->vkMapMemory(*device, *deviceMemory, _offset + deviceMemory->getOffsetAlignment(), _size, 0, &writeData);
        std::memcpy(writeData, _data, _size);
        device->vkUnmapMemory(*device, *deviceMemory);
    }

    void VulkanTransferCommands::readFromImage(VulkanImage *_image, int _offset, int _size, void *_data)
    {
        void *readData = NULL;
        DeviceMemory *deviceMemory = _image->getDeviceMemory();
        device->vkMapMemory(*device, *deviceMemory, _offset + deviceMemory->getOffsetAlignment(), _size, 0, &readData);
        std::memcpy(_data, readData, _size);
        device->vkUnmapMemory(*device, *deviceMemory);
    }

    void VulkanTransferCommands::fillBuffer(VulkanBuffer *_buffer, int _offset, int _size, uint32_t _data)
    {
        device->vkCmdFillBuffer(cmd, *_buffer, _offset, _size, _data); 
    }

    void VulkanTransferCommands::copyImage(VulkanImage *_srcImage, VulkanImage *_dstImage, VkImageLayout _srcLayout, VkImageLayout _dstLayout, VkImageCopy *_copyInfo)
    {
        device->vkCmdCopyImage(cmd, *_srcImage, _srcLayout, *_dstImage, _dstLayout, 1, _copyInfo);
    }

    void VulkanTransferCommands::copyImage(VulkanImage *_srcImage, VulkanImage *_dstImage, int _mipLevel, int _baseArrayLayer,
                                           int _layerCount, VkOffset3D _srcOffset, VkOffset3D _dstOffset)
    {
        VkImageCopy imageCopy{};
        imageCopy.extent.width = _srcImage->getWidth();
        imageCopy.extent.height = _srcImage->getHeight();
        imageCopy.extent.depth = _srcImage->getDepth();
        imageCopy.srcOffset = _srcOffset;
        imageCopy.dstOffset = _dstOffset;
        imageCopy.srcSubresource.aspectMask = _srcImage->getAspect();
        imageCopy.srcSubresource.baseArrayLayer = _baseArrayLayer;
        imageCopy.srcSubresource.layerCount = _layerCount;
        imageCopy.srcSubresource.mipLevel = _mipLevel;
        imageCopy.dstSubresource.aspectMask = _dstImage->getAspect();
        imageCopy.dstSubresource.baseArrayLayer = _baseArrayLayer;
        imageCopy.dstSubresource.layerCount = _layerCount;
        imageCopy.dstSubresource.mipLevel = _mipLevel;

        copyImage(_srcImage, _dstImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &imageCopy);
    }

    void VulkanTransferCommands::copyImageToBuffer(VulkanImage *_image, VkImageLayout _srcLayout, VulkanBuffer *_buffer, VkBufferImageCopy *_copyInfo)
    {
        device->vkCmdCopyImageToBuffer(cmd, *_image, _srcLayout, *_buffer, 1, _copyInfo);
    }

    void VulkanTransferCommands::copyImageToBuffer(VulkanImage *_image, VulkanBuffer *_buffer, int _bufferOffset, int _mipLevel, int _baseArrayLayer,
                                                   int _layerCount, VkOffset3D _srcOffset)
    {
        VkBufferImageCopy copyInfo{};
        copyInfo.bufferRowLength = 0;
        copyInfo.bufferImageHeight = 0;
        copyInfo.bufferOffset = _bufferOffset;
        copyInfo.imageSubresource.aspectMask = _image->getAspect();
        copyInfo.imageSubresource.baseArrayLayer = _baseArrayLayer;
        copyInfo.imageSubresource.layerCount = _layerCount;
        copyInfo.imageSubresource.mipLevel = _mipLevel;
        copyInfo.imageExtent.width = _image->getWidth();
        copyInfo.imageExtent.height = _image->getHeight();
        copyInfo.imageExtent.depth = _image->getDepth();
        copyInfo.imageOffset = _srcOffset;

        copyImageToBuffer(_image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, _buffer, &copyInfo);
    }

    void VulkanTransferCommands::copyBufferToImage(VulkanBuffer *_buffer, VulkanImage *_image, VkImageLayout _srcLayout, VkBufferImageCopy *_copyInfo)
    {
        device->vkCmdCopyBufferToImage(cmd, *_buffer, *_image, _srcLayout, 1, _copyInfo);
    }

    void VulkanTransferCommands::copyBufferToImage(VulkanBuffer *_buffer, VulkanImage *_image, int _bufferOffset, int _mipLevel, int _baseArrayLayer,
                                                   int _layerCount, VkOffset3D _dstOffset)
    {
        VkBufferImageCopy copyInfo{};
        copyInfo.bufferRowLength = 0;
        copyInfo.bufferImageHeight = 0;
        copyInfo.bufferOffset = _bufferOffset;
        copyInfo.imageSubresource.aspectMask = _image->getAspect();
        copyInfo.imageSubresource.baseArrayLayer = _baseArrayLayer;
        copyInfo.imageSubresource.layerCount = _layerCount;
        copyInfo.imageSubresource.mipLevel = _mipLevel;
        copyInfo.imageExtent.width = _image->getWidth();
        copyInfo.imageExtent.height = _image->getHeight();
        copyInfo.imageExtent.depth = _image->getDepth();
        copyInfo.imageOffset = _dstOffset;

        copyBufferToImage(_buffer, _image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &copyInfo);
    }

    void VulkanTransferCommands::clearImage(VulkanImage *_image, VkImageLayout _layout, const VkClearColorValue *_color, const VkImageSubresourceRange *_range)
    {
        device->vkCmdClearColorImage(cmd, *_image, _layout, _color, 1, _range);
    }

    void VulkanTransferCommands::clearImage(VulkanImage *_image, const VkClearColorValue *_color, int _baseMipLevel, int _mipLevelCount, int _baseArrayLayer,
                                            int _layerCount)
    {
        VkImageSubresourceRange range{};
        range.aspectMask = _image->getAspect();
        range.baseMipLevel = _baseMipLevel;
        range.levelCount = _mipLevelCount;
        range.baseArrayLayer = _baseArrayLayer;
        range.layerCount = _layerCount;

        clearImage(_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, _color, &range);
    }

    void VulkanTransferCommands::clearDepthStencilImage(VulkanImage *_image, VkImageLayout _layout, const VkClearDepthStencilValue *_depthStencil,
                                                        const VkImageSubresourceRange *_range)
    {
        device->vkCmdClearDepthStencilImage(cmd, *_image, _layout, _depthStencil, 1, _range);
    }

    void VulkanTransferCommands::clearDepthStencilImage(VulkanImage *_image, const VkClearDepthStencilValue *_depthStencil, int _baseMipLevel, int _mipLevelCount, int _baseArrayLayer,
                                                        int _layerCount)
    {
        VkImageSubresourceRange range{};
        range.aspectMask = _image->getAspect();
        range.baseMipLevel = _baseMipLevel;
        range.levelCount = _mipLevelCount;
        range.baseArrayLayer = _baseArrayLayer;
        range.layerCount = _layerCount;

        clearDepthStencilImage(_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, _depthStencil, &range);
    }
}