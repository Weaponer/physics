#include <cstring>

#include "gpuCommon/specialCommands/vulkanCommandReceiver.h"
#include "gpuCommon/vulkanCommandPool.h"
#include "gpuCommon/specialCommands/vulkanSpecialCommands.h"

namespace phys::vk
{
    VulkanCommandReceiver::VulkanCommandReceiver(VulkanDevice *_device)
        : device(_device)
    {
    }

    VulkanCommandReceiver::~VulkanCommandReceiver()
    {
    }

    void VulkanCommandReceiver::addQueue(VulkanQueue *_queue)
    {
        for (auto i : bufferQueues)
        {
            if (i.queue == _queue)
                return;
        }
        VulkanQueueDescriptor descriptor;
        descriptor.queue = _queue;
        descriptor.used = false;

        bufferQueues.push_back(descriptor);
    }

    VulkanQueue *VulkanCommandReceiver::getFreeQueue(VkQueueFlags _functions)
    {
        int foundIndex = -1;
        for (int i = 0; i < (int)bufferQueues.size(); i++)
        {
            VulkanQueueDescriptor descriptor = bufferQueues[i];
            if (descriptor.used)
                continue;

            VkQueueFlags flags = descriptor.queue->getQueueFlag();
            if ((flags & _functions) == _functions)
            {
                foundIndex = i;
            }

            if (((~_functions) & flags) == 0)
            {
                break;
            }
        }

        if (foundIndex == -1)
            return NULL;

        bufferQueues[foundIndex].used = true;
        return bufferQueues[foundIndex].queue;
    }

    void VulkanCommandReceiver::returnQueue(VulkanQueue *_queue)
    {
        for (int i = 0; i < (int)bufferQueues.size(); i++)
        {
            VulkanQueueDescriptor descriptor = bufferQueues[i];
            if (descriptor.queue == _queue)
            {
                bufferQueues[i].used = false;
                break;
            }
        }
    }

    void VulkanCommandReceiver::submitCommands(VulkanSpecialCommands *_commands, VulkanFence _fence)
    {
        _commands->endCommandBuffer();

        VulkanCommandPool *pool = _commands->getCommandPool();
        int countSubmitInfos = pool->getCountSubmitParts();
        VkSubmitInfo submitInfos[countSubmitInfos];
        int countCommandBuffers = pool->getCountSubmitParts();
        VkCommandBuffer commandBuffers[countCommandBuffers];

        int countWaiteSemaphores = pool->getCountWaitSemaphores();
        VkSemaphore waitSemaphores[countWaiteSemaphores];
        VkPipelineStageFlags waitStages[countWaiteSemaphores];
        int countSignalSemaphores = pool->getCountSignalSemaphores();
        VkSemaphore signalSemaphores[countSignalSemaphores];

        int writedSubmitInfos = pool->fillQueueSubmitInfo(countSubmitInfos, submitInfos, countCommandBuffers, commandBuffers, countWaiteSemaphores,
                                                          waitSemaphores, waitStages, countSignalSemaphores, signalSemaphores);

        if (countSubmitInfos != 0)
        {
            device->vkQueueSubmit(*_commands->getQueue(), writedSubmitInfos, submitInfos, _fence);
        }
    }
}