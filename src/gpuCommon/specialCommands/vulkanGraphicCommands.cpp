#include "gpuCommon/specialCommands/vulkanGraphicCommands.h"

namespace phys::vk
{
    VulkanGraphicCommands::VulkanGraphicCommands()
        : VulkanTransferCommands(true)
    {
    }

    VulkanGraphicCommands::VulkanGraphicCommands(VulkanQueue *_queue)
        : VulkanTransferCommands(_queue, true)
    {
    }

    VulkanGraphicCommands::VulkanGraphicCommands(VulkanQueue *_queue, VulkanCommandPool *_commandPool)
        : VulkanTransferCommands(_queue, _commandPool, true)
    {
    }

    void VulkanGraphicCommands::beginRenderPass(VulkanRenderPass &_renderPass, VulkanFramebuffer &_framebuffer)
    {
        VkRenderPassBeginInfo info{};
        info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        info.renderPass = _renderPass;
        info.framebuffer = _framebuffer;
        info.renderArea.offset = {0, 0};
        info.renderArea.extent.width = _framebuffer.getWidth();
        info.renderArea.extent.height = _framebuffer.getHeight();
        device->vkCmdBeginRenderPass(cmd, &info, VK_SUBPASS_CONTENTS_INLINE);
    }

    void VulkanGraphicCommands::beginRenderPass(VulkanRenderPass &_renderPass, VulkanFramebuffer &_framebuffer,
                                                int _countClearValues, const VkClearValue *_values)
    {
        VkRenderPassBeginInfo info{};
        info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        info.renderPass = _renderPass;
        info.framebuffer = _framebuffer;
        info.renderArea.offset = {0, 0};
        info.renderArea.extent.width = _framebuffer.getWidth();
        info.renderArea.extent.height = _framebuffer.getHeight();
        info.clearValueCount = _countClearValues;
        info.pClearValues = _values;
        device->vkCmdBeginRenderPass(cmd, &info, VK_SUBPASS_CONTENTS_INLINE);
    }

    void VulkanGraphicCommands::endRenderPass()
    {
        device->vkCmdEndRenderPass(cmd);
    }

    void VulkanGraphicCommands::setViewport(uint32_t _firstViewport, uint32_t _coutViewports, const VkViewport *_viewports)
    {
        device->vkCmdSetViewport(cmd, _firstViewport, _coutViewports, _viewports);
    }

    void VulkanGraphicCommands::setScissor(uint32_t _firstScissor, uint32_t _coutScissors, const VkRect2D *_scissors)
    {
        device->vkCmdSetScissor(cmd, _firstScissor, _coutScissors, _scissors);
    }

    void VulkanGraphicCommands::setDefaultViewport(const VulkanFramebuffer &_framebuffer)
    {
        VkRect2D scissor{};
        scissor.extent.width = _framebuffer.getWidth();
        scissor.extent.height = _framebuffer.getHeight();
        setScissor(0, 1, &scissor);
        VkViewport viewport{};
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        viewport.width = scissor.extent.width;
        viewport.height = scissor.extent.height;
        setViewport(0, 1, &viewport);
    }

    void VulkanGraphicCommands::draw(uint32_t _countVertex, uint32_t _countInstance, uint32_t _firstVertex, uint32_t _firstInstance)
    {
        device->vkCmdDraw(cmd, _countVertex, _countInstance, _firstVertex, _firstInstance);
    }

    void VulkanGraphicCommands::drawIndirect(VulkanBuffer *_buffer, VkDeviceSize _offset, uint32_t _drawCount, uint32_t _stride)
    {
        device->vkCmdDrawIndirect(cmd, *_buffer, _offset, _drawCount, _stride);
    }

    void VulkanGraphicCommands::drawIndexed(uint32_t _countIndexes, uint32_t _countInstance, uint32_t _firstIndex,
                                            uint32_t _vertexOffset, uint32_t _firstInstance)
    {
        device->vkCmdDrawIndexed(cmd, _countIndexes, _countInstance, _firstIndex, _vertexOffset, _firstInstance);
    }

    void VulkanGraphicCommands::drawIndexedIndirect(VulkanBuffer *_buffer, VkDeviceSize _offset, uint32_t _drawCount, uint32_t _stride)
    {
        device->vkCmdDrawIndexedIndirect(cmd, *_buffer, _offset, _drawCount, _stride);
    }

    void VulkanGraphicCommands::bindPipeline(VkPipeline _pipeline)
    {
        device->vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, _pipeline);
    }

    void VulkanGraphicCommands::bindDescriptorSet(VkPipelineLayout _layout, VkDescriptorSet _set, int _indexBinding)
    {
        device->vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, _layout, _indexBinding, 1, &_set, 0, NULL);
    }

    void VulkanGraphicCommands::pushConstant(VkPipelineLayout _layout, VkShaderStageFlags _stageFlags, uint32_t _offset, uint32_t _size, const void *_pValues)
    {
        device->vkCmdPushConstants(cmd, _layout, _stageFlags, _offset, _size, _pValues);
    }

    void VulkanGraphicCommands::bindVertexBuffer(VulkanBuffer *_buffer, VkDeviceSize _offset)
    {
        VkBuffer buffer = *_buffer;
        device->vkCmdBindVertexBuffers(cmd, 0, 1, &buffer, &_offset);
    }

    void VulkanGraphicCommands::bindInstanceBuffer(VulkanBuffer *_buffer, VkDeviceSize _offset)
    {
        VkBuffer buffer = *_buffer;
        device->vkCmdBindVertexBuffers(cmd, 1, 1, &buffer, &_offset);
    }

    void VulkanGraphicCommands::bindIndexBuffer(VulkanBuffer *_buffer, VkDeviceSize _offset, VkIndexType _type)
    {
        device->vkCmdBindIndexBuffer(cmd, *_buffer, _offset, _type);
    }
}