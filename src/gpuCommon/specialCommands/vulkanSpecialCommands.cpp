#include "gpuCommon/specialCommands/vulkanSpecialCommands.h"

namespace phys::vk
{

    VulkanSpecialCommands::VulkanSpecialCommands(bool _useRenderCommands)
        : vulkanQueue(NULL), device(NULL), vulkanCommandPool(NULL), cmd(VK_NULL_HANDLE, NULL),
          countWaitSemaphores(0), signalSemaphore(NULL, VK_NULL_HANDLE), useRenderCommands(_useRenderCommands)
    {
    }

    VulkanSpecialCommands::VulkanSpecialCommands(VulkanQueue *_queue, bool _useRenderCommands)
        : vulkanQueue(_queue), device(NULL), vulkanCommandPool(NULL), cmd(VK_NULL_HANDLE, NULL),
          countWaitSemaphores(0), signalSemaphore(NULL, VK_NULL_HANDLE), useRenderCommands(_useRenderCommands)
    {
        if (_queue != NULL)
        {
            device = vulkanQueue->getDevice();
            vulkanCommandPool = vulkanQueue->getCommandPool();
            updateCommandBuffer();
        }
    }

    VulkanSpecialCommands::VulkanSpecialCommands(VulkanQueue *_queue, VulkanCommandPool *_commandPool, bool _useRenderCommands)
        : vulkanQueue(_queue), device(vulkanQueue->getDevice()), vulkanCommandPool(_commandPool), cmd(VK_NULL_HANDLE, NULL),
          countWaitSemaphores(0), signalSemaphore(NULL, VK_NULL_HANDLE), useRenderCommands(_useRenderCommands)
    {
        updateCommandBuffer();
    }

    VulkanSpecialCommands::~VulkanSpecialCommands()
    {
        if (isValid())
        {
            vulkanQueue->returnCommandPool(vulkanCommandPool);
        }
    }

    void VulkanSpecialCommands::resetCommandBuffer()
    {
        vulkanCommandPool->reset();
        countWaitSemaphores = 0;
        signalSemaphore = VulkanSemaphore(NULL, VK_NULL_HANDLE);
        cmd = vulkanCommandPool->beginCommandBuffer(useRenderCommands);
    }

    void VulkanSpecialCommands::updateCommandBuffer()
    {
        if (cmd.isValid())
        {
            vulkanCommandPool->endCommandBuffer(cmd, countWaitSemaphores, bufferWaitSemaphores.data(), bufferStageSemaphores.data(), signalSemaphore);
            countWaitSemaphores = 0;
            signalSemaphore = VulkanSemaphore(NULL, VK_NULL_HANDLE);
        }

        cmd = vulkanCommandPool->beginCommandBuffer(useRenderCommands);
    }

    void VulkanSpecialCommands::endCommandBuffer()
    {
        if (cmd.isValid())
        {
            vulkanCommandPool->endCommandBuffer(cmd, countWaitSemaphores, bufferWaitSemaphores.data(), bufferStageSemaphores.data(), signalSemaphore);
            countWaitSemaphores = 0;
            signalSemaphore = VulkanSemaphore(NULL, VK_NULL_HANDLE);
            cmd = VulkanCommandBuffer(VK_NULL_HANDLE, NULL);
        }
    }

    void VulkanSpecialCommands::addWaitSemaphoreCmd(VulkanSemaphore _waitSemaphore, VkPipelineStageFlags _waitStage)
    {
        if (countWaitSemaphores == MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER)
            return;
        bufferWaitSemaphores[countWaitSemaphores] = _waitSemaphore;
        bufferStageSemaphores[countWaitSemaphores] = _waitStage;
        countWaitSemaphores++;
    }

    void VulkanSpecialCommands::moveBufferInQueueFamily(VulkanBuffer *_buffer, int _srcQueueFamily, int _dstQueueFamily,
                                                        VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                                        VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask)
    {
        VkBufferMemoryBarrier memoryBarrier{};
        memoryBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
        memoryBarrier.offset = 0;
        memoryBarrier.size = VK_WHOLE_SIZE;
        memoryBarrier.buffer = *_buffer;
        memoryBarrier.srcQueueFamilyIndex = _srcQueueFamily;
        memoryBarrier.dstQueueFamilyIndex = _dstQueueFamily;
        memoryBarrier.srcAccessMask = _srcAccessMask;
        memoryBarrier.dstAccessMask = _dstAccessMask;
        device->vkCmdPipelineBarrier(cmd, _srcStageMask, _dstStageMask, 0, 0, NULL, 1, &memoryBarrier, 0, NULL);
    }

    void VulkanSpecialCommands::barrierBufferAccess(VulkanBuffer *_buffer, VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                                    VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask)
    {
        VkBufferMemoryBarrier memoryBarrier{};
        memoryBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
        memoryBarrier.offset = 0;
        memoryBarrier.size = VK_WHOLE_SIZE;
        memoryBarrier.buffer = *_buffer;
        memoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        memoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        memoryBarrier.srcAccessMask = _srcAccessMask;
        memoryBarrier.dstAccessMask = _dstAccessMask;
        device->vkCmdPipelineBarrier(cmd, _srcStageMask, _dstStageMask, 0, 0, NULL, 1, &memoryBarrier, 0, NULL);
    }

    void VulkanSpecialCommands::moveImageInQueueFamily(VulkanImage *_image, int _srcQueueFamily, int _dstQueueFamily,
                                                       VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                                       VkImageLayout _srcLayout, VkImageLayout _dstLayout,
                                                       VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask)
    {
        VkImageMemoryBarrier memoryBarrier{};
        memoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        memoryBarrier.image = *_image;
        memoryBarrier.subresourceRange.aspectMask = _image->getAspect();
        memoryBarrier.subresourceRange.baseArrayLayer = 0;
        memoryBarrier.subresourceRange.baseMipLevel = 0;
        memoryBarrier.subresourceRange.layerCount = _image->getLayers();
        memoryBarrier.subresourceRange.levelCount = _image->getCountMipmaps();
        memoryBarrier.srcAccessMask = _srcAccessMask;
        memoryBarrier.dstAccessMask = _dstAccessMask;
        memoryBarrier.oldLayout = _srcLayout;
        memoryBarrier.newLayout = _dstLayout;
        memoryBarrier.srcQueueFamilyIndex = _srcQueueFamily;
        memoryBarrier.dstQueueFamilyIndex = _dstQueueFamily;

        device->vkCmdPipelineBarrier(cmd, _srcStageMask, _dstStageMask, 0, 0, NULL, 0, NULL, 1, &memoryBarrier);
    }

    void VulkanSpecialCommands::barrierImageAccess(VulkanImage *_image, VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                                   VkImageLayout _srcLayout, VkImageLayout _dstLayout, VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask)
    {
        VkImageMemoryBarrier memoryBarrier{};
        memoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        memoryBarrier.image = *_image;
        memoryBarrier.subresourceRange.aspectMask = _image->getAspect();
        memoryBarrier.subresourceRange.baseArrayLayer = 0;
        memoryBarrier.subresourceRange.baseMipLevel = 0;
        memoryBarrier.subresourceRange.layerCount = _image->getLayers();
        memoryBarrier.subresourceRange.levelCount = _image->getCountMipmaps();
        memoryBarrier.srcAccessMask = _srcAccessMask;
        memoryBarrier.dstAccessMask = _dstAccessMask;
        memoryBarrier.oldLayout = _srcLayout;
        memoryBarrier.newLayout = _dstLayout;
        memoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        memoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

        device->vkCmdPipelineBarrier(cmd, _srcStageMask, _dstStageMask, 0, 0, NULL, 0, NULL, 1, &memoryBarrier);
    }

    void VulkanSpecialCommands::barrierImagesAccess(uint32_t _countImages, VulkanImage **_images, VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask,
                                                    VkImageLayout _srcLayout, VkImageLayout _dstLayout, VkAccessFlags _srcAccessMask, VkAccessFlags _dstAccessMask)
    {
        VkImageMemoryBarrier memoryBarriers[_countImages];
        for (uint32_t i = 0; i < _countImages; i++)
        {
            VulkanImage *image = _images[i];
            memoryBarriers[i] = {};
            memoryBarriers[i].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            memoryBarriers[i].image = *image;
            memoryBarriers[i].subresourceRange.aspectMask = image->getAspect();
            memoryBarriers[i].subresourceRange.baseArrayLayer = 0;
            memoryBarriers[i].subresourceRange.baseMipLevel = 0;
            memoryBarriers[i].subresourceRange.layerCount = image->getLayers();
            memoryBarriers[i].subresourceRange.levelCount = image->getCountMipmaps();
            memoryBarriers[i].srcAccessMask = _srcAccessMask;
            memoryBarriers[i].dstAccessMask = _dstAccessMask;
            memoryBarriers[i].oldLayout = _srcLayout;
            memoryBarriers[i].newLayout = _dstLayout;
            memoryBarriers[i].srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            memoryBarriers[i].dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        }
        device->vkCmdPipelineBarrier(cmd, _srcStageMask, _dstStageMask, 0, 0, NULL, 0, NULL, _countImages, memoryBarriers);
    }

    void VulkanSpecialCommands::pipelineBarrier(VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask, VkDependencyFlags _dependencyFlags,
                                                int _memoryBarrierCount, const VkMemoryBarrier *_memoryBarriers, int _bufferMemoryBarrierCount,
                                                const VkBufferMemoryBarrier *_bufferMemoryBarriers, int _imageMemoryBarrierCount, const VkImageMemoryBarrier *_imageMemoryBarriers)
    {
        device->vkCmdPipelineBarrier(cmd, _srcStageMask, _dstStageMask, _dependencyFlags,
                                     _memoryBarrierCount, _memoryBarriers,
                                     _bufferMemoryBarrierCount, _bufferMemoryBarriers,
                                     _imageMemoryBarrierCount, _imageMemoryBarriers);
    }

    void VulkanSpecialCommands::pipelineBarrier(VkPipelineStageFlags _srcStageMask, VkPipelineStageFlags _dstStageMask)
    {
        device->vkCmdPipelineBarrier(cmd, _srcStageMask, _dstStageMask, 0, 0, NULL, 0, NULL, 0, NULL);
    }

    void VulkanSpecialCommands::debugMarkerBegin(const char *_name, float *_col)
    {
        VkDebugMarkerMarkerInfoEXT name{};
        name.sType = VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT;
        name.pMarkerName = _name;
        name.color[0] = _col[0];
        name.color[1] = _col[1];
        name.color[2] = _col[2];
        name.color[3] = 1.0f;

        if (device->vkCmdDebugMarkerBeginEXT)
            device->vkCmdDebugMarkerBeginEXT(cmd, &name);
    }

    void VulkanSpecialCommands::debugMarkerInsert(const char *_name, float *_col)
    {
        VkDebugMarkerMarkerInfoEXT name{};
        name.sType = VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT;
        name.pMarkerName = _name;
        name.color[0] = _col[0];
        name.color[1] = _col[1];
        name.color[2] = _col[2];
        name.color[3] = 1.0f;

        if (device->vkCmdDebugMarkerInsertEXT)
            device->vkCmdDebugMarkerInsertEXT(cmd, &name);
    }

    void VulkanSpecialCommands::debugMarkerEnd()
    {
        if (device->vkCmdDebugMarkerEndEXT)
            device->vkCmdDebugMarkerEndEXT(cmd);
    }
}