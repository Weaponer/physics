#include <cstring>

#include "gpuCommon/specialCommands/shaderAutoParameters.h"

namespace phys::vk
{

    ShaderAutoParameters::ShaderAutoParameters()
        : ShaderParameters(), autoSets(), isBeginUpdate(false)
    {
    }

    ShaderAutoParameters::ShaderAutoParameters(VulkanDevice *_device, ShaderModule _shader)
        : ShaderParameters(_device, _shader), autoSets(), isBeginUpdate(false)
    {
        ShaderData *data = _shader.getData();

        int countSets = data->getCountSets();
        autoSets.resize(data->getCountSets(), SetAutoParameters());

        for (int i = 0; i < countSets; i++)
        {
            ShaderData::ShaderSetData setData = data->getSetData(i);

            SetAutoParameters &set = autoSets[i];
            set.currentSet = VK_NULL_HANDLE;
            set.previousSet = VK_NULL_HANDLE;
            set.countCopyInfos = setData.countUniformBuffers + setData.countStorageBuffers +
                                 setData.countMainSamplers + setData.countMainStorageImages;
            set.copyInfos = new VkCopyDescriptorSet[set.countCopyInfos];
            for (int i2 = 0; i2 < set.countCopyInfos; i2++)
            {
                VkCopyDescriptorSet copyInfo{};
                copyInfo.sType = VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET;
                copyInfo.descriptorCount = 1;
                copyInfo.dstArrayElement = 0;
                copyInfo.srcArrayElement = 0;
                set.copyInfos[i2] = copyInfo;
            }
            int indexCopyInfo = 0;

            UniformBuffer *uniformBufferParams = NULL;
            if (setData.countUniformBuffers != 0)
            {
                uniformBufferParams = new UniformBuffer[setData.countUniformBuffers];
                for (int i2 = 0; i2 < setData.countUniformBuffers; i2++)
                {
                    uniformBufferParams[i2].datas = std::vector<UniformBuffer::ParameterType>(1, UniformBuffer::ParameterType());
                    uniformBufferParams[i2].wasUpdate = false;
                    uniformBufferParams[i2].binding = data->getUniformBlockInSet(i, setData.indexBlocksUniformBuffers[i2]).binding;
                    set.copyInfos[indexCopyInfo].dstBinding = uniformBufferParams[i2].binding;
                    set.copyInfos[indexCopyInfo].srcBinding = uniformBufferParams[i2].binding;
                    set.copyInfos[indexCopyInfo].descriptorCount = 1;
                    indexCopyInfo++;
                }
            }
            set.countMainParameters[Parameter_UniformBuffer] = setData.countUniformBuffers;
            set.countFullDescriptors[Parameter_UniformBuffer] = setData.countUniformBuffers;
            set.parameters[Parameter_UniformBuffer] = uniformBufferParams;

            StorageBuffer *storageBufferParams = NULL;
            if (setData.countStorageBuffers != 0)
            {
                storageBufferParams = new StorageBuffer[setData.countStorageBuffers];
                for (int i2 = 0; i2 < setData.countStorageBuffers; i2++)
                {
                    storageBufferParams[i2].datas = std::vector<StorageBuffer::ParameterType>(1, StorageBuffer::ParameterType());
                    storageBufferParams[i2].wasUpdate = false;
                    storageBufferParams[i2].binding = data->getUniformBlockInSet(i, setData.indexBlocksStorageBuffers[i2]).binding;

                    set.copyInfos[indexCopyInfo].dstBinding = storageBufferParams[i2].binding;
                    set.copyInfos[indexCopyInfo].srcBinding = storageBufferParams[i2].binding;
                    set.copyInfos[indexCopyInfo].descriptorCount = 1;
                    indexCopyInfo++;
                }
            }
            set.countMainParameters[Parameter_StorageBuffer] = setData.countStorageBuffers;
            set.countFullDescriptors[Parameter_StorageBuffer] = setData.countStorageBuffers;
            set.parameters[Parameter_StorageBuffer] = storageBufferParams;

            StorageImage *storageImageParams = NULL;
            if (setData.countMainStorageImages != 0)
            {
                storageImageParams = new StorageImage[setData.countMainStorageImages];
                for (int i2 = 0; i2 < setData.countMainStorageImages; i2++)
                {
                    const ShaderData::UniformSampler &sampler = data->getUniformSamplerInSet(i, setData.indexMainStorageImages[i2]);
                    storageImageParams[i2].datas = std::vector<StorageImage::ParameterType>(sampler.implicitArraySize, StorageImage::ParameterType());
                    storageImageParams[i2].wasUpdate = false;
                    storageImageParams[i2].binding = sampler.binding;

                    set.copyInfos[indexCopyInfo].dstBinding = storageImageParams[i2].binding;
                    set.copyInfos[indexCopyInfo].srcBinding = storageImageParams[i2].binding;
                    set.copyInfos[indexCopyInfo].descriptorCount = sampler.implicitArraySize;
                    indexCopyInfo++;
                }
            }
            set.countMainParameters[Parameter_StorageImage] = setData.countMainStorageImages;
            set.countFullDescriptors[Parameter_StorageImage] = setData.countAllStorageImages;
            set.parameters[Parameter_StorageImage] = storageImageParams;

            SampledImage *sampledImageParams = NULL;
            if (setData.countMainSamplers != 0)
            {
                sampledImageParams = new SampledImage[setData.countMainSamplers];
                for (int i2 = 0; i2 < setData.countMainSamplers; i2++)
                {
                    const ShaderData::UniformSampler &sampler = data->getUniformSamplerInSet(i, setData.indexMainSamplers[i2]);
                    sampledImageParams[i2].datas = std::vector<SampledImage::ParameterType>(sampler.implicitArraySize, SampledImage::ParameterType());
                    sampledImageParams[i2].datas_2 = std::vector<SampledImage::ParameterType_2>(sampler.implicitArraySize, SampledImage::ParameterType_2());

                    sampledImageParams[i2].wasUpdate = false;
                    sampledImageParams[i2].binding = sampler.binding;

                    set.copyInfos[indexCopyInfo].dstBinding = sampledImageParams[i2].binding;
                    set.copyInfos[indexCopyInfo].srcBinding = sampledImageParams[i2].binding;
                    set.copyInfos[indexCopyInfo].descriptorCount = sampler.implicitArraySize;
                    indexCopyInfo++;
                }
            }
            set.countMainParameters[Parameter_SampledImage] = setData.countMainSamplers;
            set.countFullDescriptors[Parameter_SampledImage] = setData.countAllSamplers;
            set.parameters[Parameter_SampledImage] = sampledImageParams;
        }
    }

    ShaderAutoParameters::~ShaderAutoParameters()
    {
        int countSets = (int)autoSets.size();
        for (int i = 0; i < countSets; i++)
        {
            SetAutoParameters &set = autoSets[i];

            if (set.countMainParameters[Parameter_UniformBuffer] != 0)
                delete[] reinterpret_cast<UniformBuffer *>(set.parameters[Parameter_UniformBuffer]);
            if (set.countMainParameters[Parameter_StorageBuffer] != 0)
                delete[] reinterpret_cast<StorageBuffer *>(set.parameters[Parameter_StorageBuffer]);
            if (set.countMainParameters[Parameter_StorageImage] != 0)
                delete[] reinterpret_cast<StorageImage *>(set.parameters[Parameter_StorageImage]);
            if (set.countMainParameters[Parameter_SampledImage] != 0)
                delete[] reinterpret_cast<SampledImage *>(set.parameters[Parameter_SampledImage]);

            delete[] set.copyInfos;
        }
    }

    void ShaderAutoParameters::beginUpdate()
    {
        isBeginUpdate = true;
    }

    void ShaderAutoParameters::endUpdate()
    {
        if (!isBeginUpdate)
            return;

        int countSets = (int)autoSets.size();
        for (int i = 0; i < countSets; i++)
            updateSet(i, true);

        isBeginUpdate = false;
    }

    void ShaderAutoParameters::updateSet(int _indexSet)
    {
        updateSet(_indexSet, false);
    }

    void ShaderAutoParameters::updateSet()
    {
        int countSets = (int)autoSets.size();
        for (int i = 0; i < countSets; i++)
            updateSet(i);
    }

    void ShaderAutoParameters::clearParameters(int _indexSet)
    {
        SetAutoParameters &set = autoSets[_indexSet];
        if (set.countMainParameters[Parameter_UniformBuffer] != 0)
        {
            UniformBuffer *uniformBuffers = reinterpret_cast<UniformBuffer *>(set.parameters[Parameter_UniformBuffer]);
            int count = set.countMainParameters[Parameter_UniformBuffer];
            for (int i = 0; i < count; i++)
            {
                uniformBuffers[i].datas[0] = NULL;
                uniformBuffers[i].wasUpdate = false;
            }
        }
        if (set.countMainParameters[Parameter_StorageBuffer] != 0)
        {
            UniformBuffer *storageBuffers = reinterpret_cast<UniformBuffer *>(set.parameters[Parameter_StorageBuffer]);
            int count = set.countMainParameters[Parameter_StorageBuffer];
            for (int i = 0; i < count; i++)
            {
                storageBuffers[i].datas[0] = NULL;
                storageBuffers[i].wasUpdate = false;
            }
        }
        if (set.countMainParameters[Parameter_StorageImage] != 0)
        {
            StorageImage *storageImages = reinterpret_cast<StorageImage *>(set.parameters[Parameter_StorageImage]);
            int count = set.countMainParameters[Parameter_StorageImage];
            for (int i = 0; i < count; i++)
            {
                int size = (int)storageImages[i].datas.size();
                for (int i2 = 0; i2 < size; i2++)
                    storageImages[i].datas[i2] = StorageImage::ParameterType();
                storageImages[i].wasUpdate = false;
            }
        }
        if (set.countMainParameters[Parameter_SampledImage] != 0)
        {
            SampledImage *sampledImages = reinterpret_cast<SampledImage *>(set.parameters[Parameter_SampledImage]);
            int count = set.countMainParameters[Parameter_SampledImage];
            for (int i = 0; i < count; i++)
            {
                int size = (int)sampledImages[i].datas.size();
                for (int i2 = 0; i2 < size; i2++)
                {
                    sampledImages[i].datas[i2] = SampledImage::ParameterType();
                    sampledImages[i].datas_2[i2] = SampledImage::ParameterType_2();
                }
                sampledImages[i].wasUpdate = false;
            }
        }
    }

    void ShaderAutoParameters::clearParameters()
    {
        int countSets = (int)sets.size();
        for (int i = 0; i < countSets; i++)
        {
            clearParameters(i);
        }
    }

    void ShaderAutoParameters::onSwitchSet(int _indexSet, VkDescriptorSet _previousSet)
    {
        SetAutoParameters &set = autoSets[_indexSet];
        set.previousSet = _previousSet;
        set.currentSet = getCurrentSet(_indexSet);

        if (set.previousSet == VK_NULL_HANDLE)
            return;

        for (int i = 0; i < set.countCopyInfos; i++)
        {
            set.copyInfos[i].srcSet = set.previousSet;
            set.copyInfos[i].dstSet = set.currentSet;
        }

        device->vkUpdateDescriptorSets(*device, 0, NULL, set.countCopyInfos, set.copyInfos);
    }

    void ShaderAutoParameters::onReset()
    {
        int countSets = (int)autoSets.size();
        for (int i = 0; i < countSets; i++)
        {
            autoSets[i].previousSet = VK_NULL_HANDLE;
            autoSets[i].currentSet = VK_NULL_HANDLE;
        }
    }

    void ShaderAutoParameters::updateSet(int _indexSet, bool _onlyChanged)
    {
        SetAutoParameters &set = autoSets[_indexSet];
        VkDescriptorSet currentSet = getCurrentSet(_indexSet);
        uint32_t maxCountWriteDescriptors = set.countMainParameters[Parameter_UniformBuffer] + set.countMainParameters[Parameter_StorageBuffer] +
                                            set.countMainParameters[Parameter_StorageImage] + set.countMainParameters[Parameter_SampledImage];

        VkWriteDescriptorSet writeDescriptorSets[maxCountWriteDescriptors];

        uint32_t countUseWritings = 0;
        VkDescriptorBufferInfo bufferInfos[std::max(1, set.countFullDescriptors[Parameter_UniformBuffer] +
                                                           set.countFullDescriptors[Parameter_StorageBuffer])];
        uint32_t countUseBufferInfos = 0;
        VkDescriptorImageInfo imageInfos[std::max(1, set.countFullDescriptors[Parameter_StorageImage] +
                                                         set.countFullDescriptors[Parameter_SampledImage])];
        uint32_t countUseImageInfos = 0;

        updateSetFillUnfiromBuffers(&set, currentSet, _onlyChanged, writeDescriptorSets, &countUseWritings, bufferInfos, &countUseBufferInfos);
        updateSetFillStorageBuffers(&set, currentSet, _onlyChanged, writeDescriptorSets, &countUseWritings, bufferInfos, &countUseBufferInfos);

        updateSetFillStorageImage(&set, currentSet, _onlyChanged, writeDescriptorSets, &countUseWritings, imageInfos, &countUseImageInfos);
        updateSetFillSampledImage(&set, currentSet, _onlyChanged, writeDescriptorSets, &countUseWritings, imageInfos, &countUseImageInfos);

        if (countUseWritings == 0)
            return;

        device->vkUpdateDescriptorSets(*device, countUseWritings, writeDescriptorSets, 0, NULL);
    }

    void ShaderAutoParameters::updateSetFillUnfiromBuffers(SetAutoParameters *_set, VkDescriptorSet _currentSet, bool _onlyChanged,
                                                           VkWriteDescriptorSet *_writeDescriptorSets, uint32_t *_countUseWritings,
                                                           VkDescriptorBufferInfo *_bufferInfos, uint32_t *_countUseBufferInfos)
    {
        if (_set->countMainParameters[Parameter_UniformBuffer] != 0)
        {
            UniformBuffer *uniformBuffers = reinterpret_cast<UniformBuffer *>(_set->parameters[Parameter_UniformBuffer]);
            int count = _set->countMainParameters[Parameter_UniformBuffer];
            for (int i = 0; i < count; i++)
            {
                if (_onlyChanged && !uniformBuffers[i].wasUpdate)
                    continue;
                uniformBuffers[i].wasUpdate = false;

                VkWriteDescriptorSet writing{};
                writing.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                writing.descriptorCount = 1;
                writing.dstSet = _currentSet;
                writing.dstBinding = uniformBuffers[i].binding;
                writing.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

                VkDescriptorBufferInfo buffInfo{};
                buffInfo.buffer = *uniformBuffers[i].datas[0];
                buffInfo.range = VK_WHOLE_SIZE;
                int countUseBI = *_countUseBufferInfos;
                _bufferInfos[countUseBI] = buffInfo;
                writing.pBufferInfo = &(_bufferInfos[countUseBI]);
                (*_countUseBufferInfos)++;

                _writeDescriptorSets[*_countUseWritings] = writing;
                (*_countUseWritings)++;
            }
        }
    }

    void ShaderAutoParameters::updateSetFillStorageBuffers(SetAutoParameters *_set, VkDescriptorSet _currentSet, bool _onlyChanged,
                                                           VkWriteDescriptorSet *_writeDescriptorSets, uint32_t *_countUseWritings,
                                                           VkDescriptorBufferInfo *_bufferInfos, uint32_t *_countUseBufferInfos)
    {
        if (_set->countMainParameters[Parameter_StorageBuffer] != 0)
        {
            StorageBuffer *storageBuffers = reinterpret_cast<StorageBuffer *>(_set->parameters[Parameter_StorageBuffer]);
            int count = _set->countMainParameters[Parameter_StorageBuffer];
            for (int i = 0; i < count; i++)
            {
                if (_onlyChanged && !storageBuffers[i].wasUpdate)
                    continue;
                storageBuffers[i].wasUpdate = false;

                VkWriteDescriptorSet writing{};
                writing.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                writing.descriptorCount = 1;
                writing.dstSet = _currentSet;
                writing.dstBinding = storageBuffers[i].binding;
                writing.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

                VkDescriptorBufferInfo buffInfo{};
                buffInfo.buffer = *storageBuffers[i].datas[0];
                buffInfo.range = VK_WHOLE_SIZE;
                int countUseBI = *_countUseBufferInfos;
                _bufferInfos[countUseBI] = buffInfo;
                writing.pBufferInfo = &(_bufferInfos[countUseBI]);
                (*_countUseBufferInfos)++;

                _writeDescriptorSets[*_countUseWritings] = writing;
                (*_countUseWritings)++;
            }
        }
    }

    void ShaderAutoParameters::updateSetFillStorageImage(SetAutoParameters *_set, VkDescriptorSet _currentSet, bool _onlyChanged,
                                                         VkWriteDescriptorSet *_writeDescriptorSets, uint32_t *_countUseWritings,
                                                         VkDescriptorImageInfo *_imageInfos, uint32_t *_countUseImageInfos)
    {
        if (_set->countMainParameters[Parameter_StorageImage] != 0)
        {
            StorageImage *storageImages = reinterpret_cast<StorageImage *>(_set->parameters[Parameter_StorageImage]);
            int count = _set->countMainParameters[Parameter_StorageImage];
            for (int i = 0; i < count; i++)
            {
                if (_onlyChanged && !storageImages[i].wasUpdate)
                    continue;
                storageImages[i].wasUpdate = false;

                VkWriteDescriptorSet writing{};
                writing.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                writing.dstSet = _currentSet;
                writing.dstBinding = storageImages[i].binding;
                writing.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
                int size = (int)storageImages[i].datas.size();
                writing.descriptorCount = size;
                writing.pImageInfo = &(_imageInfos[*_countUseImageInfos]);
                for (int i2 = 0; i2 < size; i2++)
                {
                    VkDescriptorImageInfo info;
                    info.sampler = VK_NULL_HANDLE;
                    info.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
                    info.imageView = storageImages[i].datas[i2];
                    _imageInfos[*_countUseImageInfos] = info;
                    (*_countUseImageInfos)++;
                }
                _writeDescriptorSets[*_countUseWritings] = writing;
                (*_countUseWritings)++;
            }
        }
    }

    void ShaderAutoParameters::updateSetFillSampledImage(SetAutoParameters *_set, VkDescriptorSet _currentSet, bool _onlyChanged,
                                                         VkWriteDescriptorSet *_writeDescriptorSets, uint32_t *_countUseWritings,
                                                         VkDescriptorImageInfo *_imageInfos, uint32_t *_countUseImageInfos)
    {
        if (_set->countMainParameters[Parameter_SampledImage] != 0)
        {
            SampledImage *sampledImages = reinterpret_cast<SampledImage *>(_set->parameters[Parameter_SampledImage]);
            uint32_t count = (uint32_t)_set->countMainParameters[Parameter_SampledImage];
            for (uint32_t i = 0; i < count; i = i + 1)
            {
                if (_onlyChanged && !sampledImages[i].wasUpdate)
                    continue;

                sampledImages[i].wasUpdate = false;

                VkWriteDescriptorSet writing{};
                writing.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                writing.dstSet = _currentSet;
                writing.dstBinding = sampledImages[i].binding;
                writing.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                int size = (int)sampledImages[i].datas.size();
                writing.descriptorCount = size;
                writing.pImageInfo = &(_imageInfos[*_countUseImageInfos]);
                for (int i2 = 0; i2 < size; i2++)
                {
                    VkDescriptorImageInfo info;
                    info.sampler = *sampledImages[i].datas_2[i2];
                    info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                    info.imageView = sampledImages[i].datas[i2];
                    _imageInfos[*_countUseImageInfos] = info;
                    (*_countUseImageInfos)++;
                }
                _writeDescriptorSets[*_countUseWritings] = writing;
                (*_countUseWritings)++;
            }
        }
    }
}