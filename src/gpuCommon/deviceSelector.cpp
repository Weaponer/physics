#include "gpuCommon/deviceSelector.h"

namespace phys::vk
{
    void DeviceSelector::addRequireMemoryType(const RequireMemoryType &_type)
    {
        requireMemoryTypes.push_back(_type);
    }

    void DeviceSelector::addRequireQueueType(VkQueueFlagBits _type, int _count, bool _supportSwapchain, VkSurfaceKHR _swapChainSurface)
    {
        RequireQueueType queueType;
        queueType.type = _type;
        queueType.count = _count;
        queueType.supportSwapChain = _supportSwapchain;
        queueType.swapChainSurface = _swapChainSurface;
        requireQueueTypes.push_back(queueType);
    }

    void DeviceSelector::addRequireLayer(const char *_layerName)
    {
        requireLayers.push_back(_layerName);
    }

    void DeviceSelector::addRequireExtension(const char *_extensionName)
    {
        requireExtensions.push_back(_extensionName);
    }

    void DeviceSelector::clear()
    {
        requireMemoryTypes.clear();
        requireQueueTypes.clear();
        requireLayers.clear();
        requireExtensions.clear();
    }

    VkPhysicalDevice DeviceSelector::getDevice(VulkanInstance *_instance) const
    {
        uint countPhysDevice = 0;
        _instance->vkEnumeratePhysicalDevices(*_instance, &countPhysDevice, NULL);
        if (!countPhysDevice)
        {
            return VK_NULL_HANDLE;
        }

        std::vector<VkPhysicalDevice> physDevices = std::vector<VkPhysicalDevice>(countPhysDevice);
        _instance->vkEnumeratePhysicalDevices(*_instance, &countPhysDevice, physDevices.data());

        std::vector<VkPhysicalDevice> suitablesPhysDevices;
        for (uint i = 0; i < countPhysDevice; i++)
        {
            VkPhysicalDevice physDevice = physDevices[i];
            VkPhysicalDeviceMemoryProperties memProperties;
            _instance->vkGetPhysicalDeviceMemoryProperties(physDevice, &memProperties);
            VkPhysicalDeviceProperties properties;
            _instance->vkGetPhysicalDeviceProperties(physDevice, &properties);

            bool discrepancy = false;
            for (int i2 = 0; i2 < (int)requireMemoryTypes.size(); i2++)
            {
                if (!checkMemoryProperties(requireMemoryTypes[i2], memProperties))
                {
                    discrepancy = true;
                    break;
                }
            }
            if (discrepancy)
                continue;

            uint countQueueFamilies = 0;
            _instance->vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &countQueueFamilies, NULL);
            std::vector<VkQueueFamilyProperties> queueFamiliesProperties;
            queueFamiliesProperties.resize(countQueueFamilies);
            _instance->vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &countQueueFamilies, queueFamiliesProperties.data());

            for (int i2 = 0; i2 < (int)requireQueueTypes.size(); i2++)
            {
                RequireQueueType requireProperties = requireQueueTypes[i2];

                if (!checkQueueFamily(_instance, physDevice, requireProperties, queueFamiliesProperties))
                {
                    discrepancy = true;
                    break;
                }
            }
            if (discrepancy)
                continue;

            suitablesPhysDevices.push_back(physDevice);
        }

        if (!suitablesPhysDevices.size())
            return VK_NULL_HANDLE;

        std::vector<std::pair<int, VkPhysicalDevice>> scorePhysDevices;
        int countDevices = (int)suitablesPhysDevices.size();
        for (int i = 0; i < countDevices; i++)
        {
            VkPhysicalDevice physDevice = suitablesPhysDevices[i];
            int score = 0;
            VkPhysicalDeviceFeatures features;
            VkPhysicalDeviceProperties properties;
            _instance->vkGetPhysicalDeviceFeatures(physDevice, &features);
            _instance->vkGetPhysicalDeviceProperties(physDevice, &properties);

            if (!features.geometryShader)
            {
                continue;
            }

            score = properties.limits.maxImageDimension2D;
            scorePhysDevices.push_back(std::make_pair(score, physDevice));
        }

        if (!scorePhysDevices.size())
            return VK_NULL_HANDLE;

        int max = scorePhysDevices[0].first;
        VkPhysicalDevice physDevice = scorePhysDevices[0].second;
        for (int i = 1; i < (int)scorePhysDevices.size(); i++)
        {
            if (max < scorePhysDevices[i].first)
            {
                max = scorePhysDevices[i].first;
                physDevice = scorePhysDevices[i].second;
            }
        }
        return physDevice;
    }

    VulkanDevice *DeviceSelector::createLogicalDevice(VkPhysicalDevice _physicalDevice, VulkanInstance *_instance, void *_pExtensionData) const
    {
        uint countQueueFamilies = 0;
        _instance->vkGetPhysicalDeviceQueueFamilyProperties(_physicalDevice, &countQueueFamilies, NULL);
        std::vector<VkQueueFamilyProperties> queueFamiliesProperties;
        queueFamiliesProperties.resize(countQueueFamilies);
        _instance->vkGetPhysicalDeviceQueueFamilyProperties(_physicalDevice, &countQueueFamilies, queueFamiliesProperties.data());

        VkDeviceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        std::vector<VkDeviceQueueCreateInfo> deviceQueuesInfo;
       
        uint32_t maxCountQueues = 0;
        for (std::size_t i = 0; i < requireQueueTypes.size(); i++)
            maxCountQueues = std::max((uint32_t)requireQueueTypes[i].count, maxCountQueues);

        float priorities[maxCountQueues];
        for(uint32_t i = 0; i < maxCountQueues; i++)
            priorities[i] = 1.0f;
        
        for (std::size_t i = 0; i < requireQueueTypes.size(); i++)
        {
            VkDeviceQueueCreateInfo queueCreateInfo{};
            queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;

            queueCreateInfo.queueCount = requireQueueTypes[i].count;

            queueCreateInfo.pQueuePriorities = priorities;
            int indexFamily = getSuitableFamilyIndex(_instance, _physicalDevice, requireQueueTypes[i], queueFamiliesProperties);
            if (indexFamily == -1)
            {
                return NULL;
            }
            queueCreateInfo.queueFamilyIndex = indexFamily;
            deviceQueuesInfo.push_back(queueCreateInfo);
        }

        createInfo.queueCreateInfoCount = deviceQueuesInfo.size();
        createInfo.pQueueCreateInfos = deviceQueuesInfo.data();

        int sizeBufferLayers = std::max(1, (int)requireLayers.size());
        const char *layers[sizeBufferLayers];
        for (int i = 0; i < (int)requireLayers.size(); i++)
            layers[i] = requireLayers[i].c_str();

        int sizeBufferExtensions = std::max(1, (int)requireExtensions.size());
        const char *extensions[sizeBufferExtensions];
        for (int i = 0; i < (int)requireExtensions.size(); i++)
            extensions[i] = requireExtensions[i].c_str();

        createInfo.enabledLayerCount = (int)requireLayers.size();
        createInfo.enabledExtensionCount = (int)requireExtensions.size();
        createInfo.ppEnabledLayerNames = layers;
        createInfo.ppEnabledExtensionNames = extensions;
        createInfo.pNext = _pExtensionData;

        VkPhysicalDeviceFeatures features{};
        features.wideLines = true;
        features.logicOp = true;
        createInfo.pEnabledFeatures = &features;
        return new VulkanDevice(_physicalDevice, &createInfo, _instance);
    }

    bool DeviceSelector::checkMemoryProperties(RequireMemoryType _requireType, VkPhysicalDeviceMemoryProperties &_properties) const
    {
        int indexNeedMemory = -1;
        for (uint i = 0; i < _properties.memoryTypeCount; i++)
        {
            if ((_requireType.memoryProperties & _properties.memoryTypes[i].propertyFlags) == _requireType.memoryProperties &&
                (_requireType.heapProperties & _properties.memoryHeaps[_properties.memoryTypes[i].heapIndex].flags) == _requireType.heapProperties)
            {
                indexNeedMemory = i;
                continue;
            }
        }
        return indexNeedMemory > -1 ? true : false;
    }

    bool DeviceSelector::checkQueueFamily(VulkanInstance *_instance, VkPhysicalDevice _physDevice, RequireQueueType _requireProperties,
                                          std::vector<VkQueueFamilyProperties> &_queueFamiliesProperties) const
    {
        for (int i = 0; i < (int)_queueFamiliesProperties.size(); i++)
        {
            VkQueueFamilyProperties properties = _queueFamiliesProperties[i];
            if (properties.queueCount < (uint)_requireProperties.count)
                continue;

            if ((properties.queueFlags & _requireProperties.type) != _requireProperties.type)
                continue;

            if (_requireProperties.supportSwapChain)
            {
                VkBool32 isSupport = false;
                _instance->vkGetPhysicalDeviceSurfaceSupportKHR(_physDevice, i, _requireProperties.swapChainSurface, &isSupport);

                if (!isSupport)
                    continue;
            }

            return true;
        }
        return false;
    }

    int DeviceSelector::getSuitableFamilyIndex(VulkanInstance *_instance, VkPhysicalDevice _physDevice, RequireQueueType _requireProperties,
                                               std::vector<VkQueueFamilyProperties> &_queueFamiliesProperties) const
    {
        int suitableIndex = -1;
        for (std::size_t i = 0; i < _queueFamiliesProperties.size(); i++)
        {
            VkQueueFamilyProperties property = _queueFamiliesProperties[i];

            if (_requireProperties.supportSwapChain)
            {
                VkBool32 isSupport = false;
                _instance->vkGetPhysicalDeviceSurfaceSupportKHR(_physDevice, i, _requireProperties.swapChainSurface, &isSupport);

                if (!isSupport)
                    continue;
            }

            if ((property.queueFlags & _requireProperties.type) == _requireProperties.type && property.queueCount >= (uint)_requireProperties.count)
            {
                suitableIndex = i;

                if ((property.queueFlags & (~_requireProperties.type)) == 0)
                    break;
            }
        }
        return suitableIndex;
    }
}