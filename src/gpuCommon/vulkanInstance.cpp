#include <iostream>
#include <cstring>

#include "gpuCommon/vulkanInstance.h"
#include "gpuCommon/vulkanMainFunctions.h"
#include "gpuCommon/vulkanExportedLibrary.h"

namespace phys::vk
{
#define INSTANCE_LEVEL_VULKAN_FUNCTION(name) name = (PFN_##name)loadFunction(#name, NULL, _createInfo);
#define INSTANCE_LEVEL_VULKAN_FUNCTION_EXTENSION(name, extension) name = (PFN_##name)loadFunction(#name, extension, _createInfo);

    VulkanInstance::VulkanInstance(VkInstanceCreateInfo &_createInfo, VulkanExportedLibrary *_vkExportedLibrary, VulkanMainFunctions *_vkMainFunctions)
        : vkExportedLibrary(_vkExportedLibrary), vkMainFunctions(_vkMainFunctions)
    {
        if (vkMainFunctions->vkCreateInstance(&_createInfo, NULL, &instance) != VK_SUCCESS)
        {
            std::cerr << "VkInstance didn`t created!" << std::endl;
            instance = VK_NULL_HANDLE;
            return;
        }

#include "exportedVulkanFunctions.inl"
    }

    VulkanInstance::~VulkanInstance()
    {
        if (instance != VK_NULL_HANDLE)
        {
            this->vkDestroyInstance(instance, NULL);
            instance = VK_NULL_HANDLE;
        }
    }

    PFN_vkVoidFunction VulkanInstance::loadFunction(const char *_name, const char *_extension, VkInstanceCreateInfo &_createInfo)
    {
        if (_extension != NULL)
        {
            bool found = false;
            for (int i = 0; i < (int)_createInfo.enabledExtensionCount; i++)
            {
                if (std::strcmp(_extension, _createInfo.ppEnabledExtensionNames[i]) == 0)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                return NULL;
            }
        }

        PFN_vkVoidFunction func = vkExportedLibrary->vkGetInstanceProcAddr(instance, _name);
        if (!func)
        {
            std::cerr << "Instance function: " << std::string(_name) << " not loaded!" << std::endl;
            return NULL;
        }
        return func;
    }
}