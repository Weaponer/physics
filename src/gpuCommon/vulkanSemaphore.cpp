#include "gpuCommon/vulkanSemaphore.h"
#include "gpuCommon/vulkanDevice.h"

namespace phys::vk
{
    bool VulkanSemaphore::createSemaphore(VulkanDevice *_device, VulkanSemaphore *_outSemaphore, const char *_name)
    {
        VkSemaphoreCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        VkSemaphore internalSemaphore;
        if (_device->vkCreateSemaphore(*_device, &createInfo, NULL, &internalSemaphore) != VK_SUCCESS)
        {
            return false;
        }
        if (_name)
            _device->setNameObject(_name, (uint64_t)internalSemaphore, VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT);

        *_outSemaphore = VulkanSemaphore(_device, internalSemaphore);
        return true;
    }

    void VulkanSemaphore::destroySemaphore(VulkanSemaphore *_semaphore)
    {
        VulkanDevice *device = _semaphore->device;
        device->vkDestroySemaphore(*device, *_semaphore, NULL);
    }
}