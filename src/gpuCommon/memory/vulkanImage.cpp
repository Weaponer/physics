#include "gpuCommon/memory/vulkanImage.h"

namespace phys::vk
{
    bool VulkanImageView::createImageView(VulkanDevice *_device, VkImageViewType _viewType,
                                          VulkanImage *_image, VulkanImageView *_outImageView,
                                          const char *_name,
                                          VkComponentSwizzle _componentR, VkComponentSwizzle _componentG,
                                          VkComponentSwizzle _componentB, VkComponentSwizzle _componentA)
    {
        VkImageViewCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = *_image;
        createInfo.format = _image->getFormat();
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.levelCount = _image->getCountMipmaps();
        createInfo.subresourceRange.layerCount = _image->getLayers();
        createInfo.subresourceRange.aspectMask = _image->getAspect();
        createInfo.viewType = _viewType;

        createInfo.components.r = _componentR;
        createInfo.components.g = _componentG;
        createInfo.components.b = _componentB;
        createInfo.components.a = _componentA;

        VkImageView view;
        if (_device->vkCreateImageView(*_device, &createInfo, NULL, &view) != VK_SUCCESS)
        {
            return false;
        }
        if (_name)
            _device->setNameObject(_name, (uint64_t)view, VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT);

        *_outImageView = VulkanImageView(view);
        return true;
    }

    void VulkanImageView::destroyImageView(VulkanDevice *_device, VulkanImageView _imageView)
    {
        _device->vkDestroyImageView(*_device, _imageView, NULL);
    }

    VulkanSampler::VulkanSampler(VulkanDevice *_device) : device(_device), createInfo(), sampler(VK_NULL_HANDLE)
    {
        createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        createInfo.pNext = NULL;
        createInfo.magFilter = VK_FILTER_LINEAR;
        createInfo.minFilter = VK_FILTER_LINEAR;
        createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        createInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        createInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        createInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        createInfo.mipLodBias = 0.0f;
        createInfo.anisotropyEnable = false;
        createInfo.maxAnisotropy = 1.0f;
        createInfo.compareEnable = false;
        createInfo.compareOp = VK_COMPARE_OP_ALWAYS;
        createInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
        createInfo.unnormalizedCoordinates = false;
    }

    VulkanSampler::~VulkanSampler()
    {
        if (isValid())
            device->vkDestroySampler(*device, sampler, NULL);
    }

    bool VulkanSampler::update()
    {
        if (isValid())
        {
            device->vkDestroySampler(*device, sampler, NULL);
            sampler = VK_NULL_HANDLE;
        }

        return device->vkCreateSampler(*device, &createInfo, NULL, &sampler) == VK_SUCCESS;
    }
}