#include "gpuCommon/memory/vulkanBufferAllocator.h"

namespace phys::vk
{

    VulkanBufferAllocator::VulkanBufferAllocator(VulkanDevice *_device, GpuMemoryController *_memoryController)
        : device(_device), memoryController(_memoryController)
    {
    }

    VulkanBufferAllocator::~VulkanBufferAllocator()
    {
    }

    void VulkanBufferAllocator::setTypeMemoryForTypeBuffer(VulkanBufferType _typeBuffer, RequireMemoryType _requireTypeMemory)
    {
        typesBuffers[_typeBuffer] = _requireTypeMemory;
    }

    void VulkanBufferAllocator::setUsageForTypeBuffer(VulkanBufferType _typeBuffer, VkBufferUsageFlags _usage)
    {
        typesUsageBuffers[_typeBuffer] = _usage;
    }

    VulkanBuffer *VulkanBufferAllocator::createBuffer(MemoryUseType _use, RequireMemoryType _memoryType, VkBufferUsageFlags _usage,
                                                      int _size, int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name)
    {

        VkBuffer buffer = VK_NULL_HANDLE;
        VkBufferCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        createInfo.size = _size;
        createInfo.usage = _usage;

        if (_countQueueFamilies == 1)
            createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        else
            createInfo.sharingMode = VK_SHARING_MODE_CONCURRENT;

        createInfo.queueFamilyIndexCount = _countQueueFamilies;
        createInfo.pQueueFamilyIndices = _queueFamilies;
        if (device->vkCreateBuffer(*device, &createInfo, NULL, &buffer) != VK_SUCCESS)
        {
            return NULL;
        }

        VkMemoryRequirements memRequirements;
        device->vkGetBufferMemoryRequirements(*device, buffer, &memRequirements);

        DeviceMemory *memory = memoryController->getDeviceMemory(_use, memRequirements.size, memRequirements.alignment, _memoryType);
        if (memory == NULL)
        {
            device->vkDestroyBuffer(*device, buffer, NULL);
            return NULL;
        }

        int offset = memory->getOffsetAlignment();
        if (device->vkBindBufferMemory(*device, buffer, *memory, offset) != VK_SUCCESS)
        {
            memoryController->returnDeviceMemory(memory);
            device->vkDestroyBuffer(*device, buffer, NULL);
            return NULL;
        }

        VulkanBuffer *vulkanBuffer = new VulkanBuffer(buffer, memory, _size, _usage, memoryController->isHostVisibleMemory(memory));
        if (_name)
            device->setNameObject(_name, (uint64_t)buffer, VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT);
        return vulkanBuffer;
    }

    VulkanBuffer *VulkanBufferAllocator::createBuffer(MemoryUseType _use, VulkanBufferType _type, int _size,
                                                      int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name)
    {
        return createBuffer(_use, typesBuffers[_type], typesUsageBuffers[_type], _size, _countQueueFamilies, _queueFamilies, _name);
    }

    void VulkanBufferAllocator::destroyBuffer(VulkanBuffer *_buffer)
    {
        device->vkDestroyBuffer(*device, *_buffer, NULL);
        memoryController->returnDeviceMemory(_buffer->getDeviceMemory());
        delete _buffer;
    }
}