#include "gpuCommon/memory/vulkanImageAllocator.h"

namespace phys::vk
{
    VulkanImageAllocator::VulkanImageAllocator(VulkanDevice *_device, GpuMemoryController *_memoryController)
        : device(_device), memoryController(_memoryController)
    {
    }

    VulkanImageAllocator::~VulkanImageAllocator()
    {
    }

    void VulkanImageAllocator::setSettingForTypeImage(VulkanImageType _typeImage, VkImageTiling _imageTiling,
                                                      VkImageUsageFlags _usages, RequireMemoryType _memoryType)
    {
        typesTiling[_typeImage] = _imageTiling;
        typesUsages[_typeImage] = _usages;
        typesMemory[_typeImage] = _memoryType;
    }

    VulkanImage *VulkanImageAllocator::createImage(MemoryUseType _use, VkImageTiling _imageTiling, VkImageUsageFlags _usages, RequireMemoryType _memoryType,
                                                   VkImageType _imageType, VkFormat _format, VkImageAspectFlags _aspect,
                                                   int _width, int _height, int _depth, int _countMipmaps, int _layers,
                                                   int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name)
    {
        VkImageCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;

        if (_countQueueFamilies == 1)
            createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        else
            createInfo.sharingMode = VK_SHARING_MODE_CONCURRENT;

        createInfo.pQueueFamilyIndices = _queueFamilies;

        createInfo.arrayLayers = _layers;
        createInfo.mipLevels = _countMipmaps;
        createInfo.imageType = _imageType;
        createInfo.extent.width = static_cast<uint32_t>(_width);
        createInfo.extent.height = static_cast<uint32_t>(_height);
        createInfo.extent.depth = static_cast<uint32_t>(_depth);
        createInfo.usage = _usages;
        createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        createInfo.format = _format;
        createInfo.tiling = _imageTiling;

        VkImage image;
        if (device->vkCreateImage(*device, &createInfo, NULL, &image) != VK_SUCCESS)
        {
            return NULL;
        }
        if(_name)
            device->setNameObject(_name, (uint64_t)image, VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT);

        VkMemoryRequirements memRequirements;
        device->vkGetImageMemoryRequirements(*device, image, &memRequirements);

        DeviceMemory *memory = memoryController->getDeviceMemory(_use, memRequirements.size, memRequirements.alignment, _memoryType);
        if (memory == NULL)
        {
            device->vkDestroyImage(*device, image, NULL);
            return NULL;
        }

        int offset = memory->getOffsetAlignment();
        if (device->vkBindImageMemory(*device, image, *memory, offset) != VK_SUCCESS)
        {
            memoryController->returnDeviceMemory(memory);
            device->vkDestroyImage(*device, image, NULL);
            return NULL;
        }

        return new VulkanImage(image, memory, memRequirements.size, _usages, memoryController->isHostVisibleMemory(memory), _imageType, _format, _imageTiling, _aspect,
                               _width, _height, _depth, _countMipmaps, _layers);
    }

    VulkanImage *VulkanImageAllocator::createImage(MemoryUseType _use, VulkanImageType _type, VkImageUsageFlags _usages,
                                                   VkImageType _imageType, VkFormat _format, VkImageAspectFlags _aspect,
                                                   int _width, int _height, int _depth, int _countMipmaps, int _layers,
                                                   int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name)
    {
        return createImage(_use, typesTiling[_type], _usages, typesMemory[_type], _imageType, _format, _aspect, _width, _height, _depth,
                           _countMipmaps, _layers, _countQueueFamilies, _queueFamilies, _name);
    }

    VulkanImage *VulkanImageAllocator::createImage(MemoryUseType _use, VulkanImageType _type, VkImageType _imageType, VkFormat _format, VkImageAspectFlags _aspect,
                                                   int _width, int _height, int _depth, int _countMipmaps, int _layers,
                                                   int _countQueueFamilies, const uint32_t *_queueFamilies, const char *_name)
    {
        return createImage(_use, typesTiling[_type], typesUsages[_type], typesMemory[_type], _imageType, _format, _aspect, _width, _height, _depth,
                           _countMipmaps, _layers, _countQueueFamilies, _queueFamilies, _name);
    }

    void VulkanImageAllocator::destroyImage(VulkanImage *_image)
    {
        device->vkDestroyImage(*device, *_image, NULL);
        memoryController->returnDeviceMemory(_image->getDeviceMemory());
        delete _image;
    }
}