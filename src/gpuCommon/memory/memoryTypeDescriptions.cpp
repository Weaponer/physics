#include "gpuCommon/memory/memoryTypeDescriptions.h"

namespace phys::vk
{
    VulkanMemoryDescriptor::VulkanMemoryDescriptor()
    {
        memoryPropertiesBuffers[VulkanBufferType_UniformBlockStatic] = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        heapPropertiesBuffers[VulkanBufferType_UniformBlockStatic] = VK_MEMORY_HEAP_DEVICE_LOCAL_BIT;
        memoryPropertiesBuffers[VulkanBufferType_UniformBlockDynamic] = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        heapPropertiesBuffers[VulkanBufferType_UniformBlockDynamic] = 0;

        memoryPropertiesBuffers[VulkanBufferType_UniformStorageBufferStatic] = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        heapPropertiesBuffers[VulkanBufferType_UniformStorageBufferStatic] = VK_MEMORY_HEAP_DEVICE_LOCAL_BIT;
        memoryPropertiesBuffers[VulkanBufferType_UniformStorageBufferDynamic] = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        heapPropertiesBuffers[VulkanBufferType_UniformStorageBufferDynamic] = 0;

        memoryPropertiesBuffers[VulkanBufferType_VertexBufferStatic] = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        heapPropertiesBuffers[VulkanBufferType_VertexBufferStatic] = VK_MEMORY_HEAP_DEVICE_LOCAL_BIT;
        memoryPropertiesBuffers[VulkanBufferType_VertexBufferDynamic] = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        heapPropertiesBuffers[VulkanBufferType_VertexBufferDynamic] = 0;

        memoryPropertiesBuffers[VulkanBufferType_IndexBufferStatic] = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        heapPropertiesBuffers[VulkanBufferType_IndexBufferStatic] = VK_MEMORY_HEAP_DEVICE_LOCAL_BIT;
        memoryPropertiesBuffers[VulkanBufferType_IndexBufferDynamic] = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        heapPropertiesBuffers[VulkanBufferType_IndexBufferDynamic] = 0;

        memoryPropertiesBuffers[VulkanBufferType_IndirectBufferStatic] = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        heapPropertiesBuffers[VulkanBufferType_IndirectBufferStatic] = VK_MEMORY_HEAP_DEVICE_LOCAL_BIT;
        memoryPropertiesBuffers[VulkanBufferType_IndirectBufferDynamic] = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        heapPropertiesBuffers[VulkanBufferType_IndirectBufferDynamic] = 0;

        memoryPropertiesBuffers[VulkanBufferType_Staging] = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
        heapPropertiesBuffers[VulkanBufferType_Staging] = 0;

        // images
        memoryPropertiesImages[VulkanImageType_ImageStatic] = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        heapPropertiesImages[VulkanImageType_ImageStatic] = VK_MEMORY_HEAP_DEVICE_LOCAL_BIT;
        memoryPropertiesImages[VulkanImageType_ImageDynamic] = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        heapPropertiesImages[VulkanImageType_ImageDynamic] = 0;

        memoryPropertiesImages[VulkanImageType_Staging] = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
        heapPropertiesImages[VulkanImageType_Staging] = 0;
    }

    VulkanMemoryDescriptor::~VulkanMemoryDescriptor()
    {
    }

    RequireMemoryType VulkanMemoryDescriptor::getRequireMemoryType(VulkanBufferType _bufferType) const
    {
        RequireMemoryType type;
        type.memoryProperties = memoryPropertiesBuffers[_bufferType];
        type.heapProperties = heapPropertiesBuffers[_bufferType];
        return type;
    }

    RequireMemoryType VulkanMemoryDescriptor::getRequireMemoryType(VulkanImageType _imageType) const
    {
        RequireMemoryType type;
        type.memoryProperties = memoryPropertiesImages[_imageType];
        type.heapProperties = heapPropertiesImages[_imageType];
        return type;
    }
}