#include <algorithm>
#include <queue>
#include <bitset>
#include <iostream>

#include "gpuCommon/memory/gpuMemoryController.h"

namespace phys::vk
{
    GpuMemoryController::GpuMemoryController(VulkanDevice *_device)
        : device(_device), beginDeviceMemoryNodes(NULL)
    {
        config.sizePrepareDeviceMemoryNodes = 256;
        config.sizeLongMemoryPage = 1024 * 64;
        config.sizeMediumMemoryPage = 1024 * 32;
        config.sizeTemporaryMemoryPage = 1024 * 32;

        config.averageSizeTemporaryAllocation = 1024;
        config.reservedPages = 2;
    }

    GpuMemoryController::~GpuMemoryController()
    {
        for (auto data : allocatedDeviceMemoryNodes)
        {
            delete[] data;
        }

        for (auto data : beginLongMemoryPages)
        {
            clearListPages(data.second);
        }

        for (auto data : beginMediumMemoryPages)
        {
            clearListPages(data.second);
        }

        for (auto data : aviableMemoryTypes)
        {
            delete data.second;
        }
        aviableMemoryTypes.clear();
    }

    bool GpuMemoryController::addRequireMemoryType(RequireMemoryType _requireMemoryType)
    {
        auto found = std::find(requireMemoryTypes.begin(), requireMemoryTypes.end(), _requireMemoryType);
        if (found != requireMemoryTypes.end())
        {
            return false;
        }

        std::vector<std::pair<int, MemoryType>> listAviableMemoryTypes;
        const VkPhysicalDeviceMemoryProperties &memProperties = device->getMemoryProperties();
        int reqMemoryTypeBits = std::bitset<sizeof(VkMemoryPropertyFlags) * 8>(_requireMemoryType.memoryProperties).count();
        int reqMemoryHeapBits = std::bitset<sizeof(VkMemoryHeapFlags) * 8>(_requireMemoryType.heapProperties).count();

        int memoryTypeCount = (int)memProperties.memoryTypeCount;
        for (int i = 0; i < memoryTypeCount; i++)
        {
            VkMemoryType type = memProperties.memoryTypes[i];
            if ((type.propertyFlags & _requireMemoryType.memoryProperties) != _requireMemoryType.memoryProperties)
            {
                continue;
            }

            VkMemoryHeap heap = memProperties.memoryHeaps[type.heapIndex];
            if ((heap.flags & _requireMemoryType.heapProperties) != _requireMemoryType.heapProperties)
            {
                continue;
            }

            int countAnotherBitsMemType = std::bitset<sizeof(VkMemoryPropertyFlags) * 8>(type.propertyFlags).count() - reqMemoryTypeBits;
            int countAnotherBitsMemHeap = std::bitset<sizeof(VkMemoryHeapFlags) * 8>(heap.flags).count() - reqMemoryHeapBits;

            int score = (countAnotherBitsMemHeap * 10 + countAnotherBitsMemType);
            MemoryType memoryType{};
            memoryType.memoryProperties = type.propertyFlags;
            memoryType.heapProperties = heap.flags;
            memoryType.indexMemory = i;
            listAviableMemoryTypes.push_back(std::make_pair(score, memoryType));
            std::sort(listAviableMemoryTypes.begin(), listAviableMemoryTypes.end());
        }

        if (listAviableMemoryTypes.size() != 0)
        {
            requireMemoryTypes.push_back(_requireMemoryType);
            std::vector<MemoryType> *vecData = new std::vector<MemoryType>();
            int size = listAviableMemoryTypes.size();
            for (int i = 0; i < size; i++)
            {
                vecData->push_back(listAviableMemoryTypes[i].second);
            }
            aviableMemoryTypes[_requireMemoryType] = vecData;

            return true;
        }
        else
        {
            return false;
        }
    }

    void GpuMemoryController::prepareMemory()
    {
        allocateAdditionalDeviceMemoryNodes();

        int countRequireMemoryTypes = requireMemoryTypes.size();
        for (int i = 0; i < config.reservedPages; i++)
        {
            for (int i2 = 0; i2 < countRequireMemoryTypes; i2++)
            {
                PrepareMemoryPage *longPage = createPage(config.sizeLongMemoryPage * 1024, requireMemoryTypes[i2]);
                addPageToBuffer(beginLongMemoryPages, requireMemoryTypes[i2], longPage);
                PrepareMemoryPage *mediumPage = createPage(config.sizeMediumMemoryPage * 1024, requireMemoryTypes[i2]);
                addPageToBuffer(beginMediumMemoryPages, requireMemoryTypes[i2], mediumPage);
            }
        }
    }

    int GpuMemoryController::getSizePage(MemoryUseType _use)
    {
        if (_use == MemoryUseType::MemoryUseType_Long)
        {
            return config.sizeLongMemoryPage * 1024;
        }
        else
        {
            return config.sizeMediumMemoryPage * 1024;
        }
    }

    DeviceMemory *GpuMemoryController::getDeviceMemory(MemoryUseType _use, int _size, int _alignment, RequireMemoryType _memoryType)
    {
        DeviceMemory *freeDeviceMemory = NULL;
        if (!tryGetFreeMemoryFromBufferPages(_use, _size, _alignment, _memoryType, &freeDeviceMemory))
        {
            return NULL;
        }

        int offsetAlignment = freeDeviceMemory->offset;
        if (offsetAlignment % _alignment != 0)
        {
            int add = _alignment - (offsetAlignment % _alignment);
            offsetAlignment += add;
            _size += add;
        }

        if (_size == freeDeviceMemory->size)
        {
            freeDeviceMemory->useMemory = true;
            freeDeviceMemory->page->freeSpace -= _size;
            freeDeviceMemory->offsetAlignment = offsetAlignment;
            return freeDeviceMemory;
        }

        DeviceMemory *deviceMemory = getFreeDeviceMemoryNode();
        deviceMemory->back = freeDeviceMemory->back;
        deviceMemory->next = freeDeviceMemory;
        freeDeviceMemory->back = deviceMemory;
        if (deviceMemory->back != NULL)
        {
            deviceMemory->back->next = deviceMemory;
        }
        else
        {
            freeDeviceMemory->page->beginDeviceMemoryIterators = deviceMemory;
        }

        deviceMemory->memory = freeDeviceMemory->memory;
        deviceMemory->useMemory = true;
        deviceMemory->page = freeDeviceMemory->page;
        deviceMemory->size = _size;
        deviceMemory->offset = freeDeviceMemory->offset;
        deviceMemory->offsetAlignment = offsetAlignment;

        freeDeviceMemory->size -= _size;
        freeDeviceMemory->offset += _size;

        deviceMemory->page->freeSpace -= _size;
        return deviceMemory;
    }

    bool GpuMemoryController::isHostVisibleMemory(const DeviceMemory *_memory) const
    {
        return (_memory->page->memoryType.memoryProperties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) != 0;
    }

    void GpuMemoryController::returnDeviceMemory(DeviceMemory *_memory)
    {
        //PrepareMemoryPage *page = _memory->page;

        //std::cout << "start delete:" << std::endl;
        //printErrorPage(page);

        _memory->useMemory = false;
        _memory->offsetAlignment = _memory->offset;
        _memory->page->freeSpace += _memory->size;
        

        DeviceMemory *back = _memory;
        while (true)
        {
            if (back->back == NULL || back->back->useMemory)
            {
                break;
            }
            back = back->back;
        }

        // pooling free memory
        DeviceMemory *next = back;
        while (true)
        {
            if (next->next == NULL || next->next->useMemory)
            {
                break;
            }

            DeviceMemory *current = next;
            next = current->next;

            next->back = current->back;
            if (next->back != NULL)
            {
                next->back->next = next;
            }
            else
            {
                next->page->beginDeviceMemoryIterators = next;
            }
            next->size += current->size;
            next->offset -= current->size;
            returnDeviceMemoryNode(current);
        }
        //printErrorPage(page);

        //std::cout << "end delete." << std::endl;
    }

    bool GpuMemoryController::checkMemoryStable()
    {
        bool result = true;
        for (auto i : beginLongMemoryPages)
        {
            PrepareMemoryPage *next = i.second;
            while (next != NULL)
            {
                result = result & checkMemoryPage(next);
                next = next->nextPage;
            }
        }
        for (auto i : beginMediumMemoryPages)
        {
            PrepareMemoryPage *next = i.second;
            while (next != NULL)
            {
                result = result & checkMemoryPage(next);
                next = next->nextPage;
            }
        }
        return result;
    }

    bool GpuMemoryController::checkMemoryPage(PrepareMemoryPage *_page)
    {
        int countFree = _page->freeSpace;
        int countAll = _page->size;

        int sumAll = 0;
        int sumFree = 0;
        int sumOffset = 0;
        DeviceMemory *nodeNext = _page->beginDeviceMemoryIterators;
        while (nodeNext != NULL)
        {
            if (nodeNext->useMemory == false)
            {
                sumFree += nodeNext->size;
            }
            sumAll += nodeNext->size;
            if (sumOffset != nodeNext->offset)
            {
                return false;
            }

            sumOffset += nodeNext->size;
            if (!nodeNext->useMemory && (nodeNext->next != NULL && !nodeNext->next->useMemory))
            {
                return false;
            }
            if (nodeNext->next != NULL && nodeNext->next->back != nodeNext)
            {
                return false;
            }

            nodeNext = nodeNext->next;
        }

        if (sumAll != countAll)
        {
            return false;
        }

        if (sumFree != countFree)
        {
            return false;
        }

        return true;
    }

    void GpuMemoryController::printErrorPage(PrepareMemoryPage *_page)
    {
        std::cout << "Size all freeSpace: " << _page->freeSpace << std::endl;
        DeviceMemory *nextTest = _page->beginDeviceMemoryIterators;
        while (nextTest != NULL)
        {
            std::cout << "(" << nextTest->useMemory << " : " << nextTest->offset << " : " << nextTest->size << ")"
                      << " -> ";
            nextTest = nextTest->next;
        }
        std::cout << std::endl
                  << std::endl;
    }

    void GpuMemoryController::addPageToBuffer(std::map<RequireMemoryType, PrepareMemoryPage *> &_buffer, RequireMemoryType _type, PrepareMemoryPage *_page)
    {
        PrepareMemoryPage *begin = _buffer[_type];
        if (begin == NULL)
        {
            _page->nextPage = NULL;
            _buffer[_type] = _page;
        }
        else
        {
            _page->nextPage = begin;
            _buffer[_type] = _page;
        }
    }

    PrepareMemoryPage *GpuMemoryController::createPage(int _size, RequireMemoryType _memoryType)
    {
        std::vector<MemoryType> *aviableMemories = aviableMemoryTypes[_memoryType];
        int count = aviableMemories->size();
        for (int i = 0; i < count; i++)
        {
            MemoryType typeMemory = aviableMemories->at(i);

            VkMemoryAllocateInfo info{};
            info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
            info.allocationSize = _size;
            info.memoryTypeIndex = typeMemory.indexMemory;
            VkDeviceMemory allocatedMemory{};
            if (device->vkAllocateMemory(*device, &info, NULL, &allocatedMemory) == VK_SUCCESS)
            {
                PrepareMemoryPage *memoryPage = new PrepareMemoryPage();
                memoryPage->size = _size;
                memoryPage->memory = allocatedMemory;
                memoryPage->memoryType = typeMemory;
                memoryPage->nextPage = NULL;
                memoryPage->freeSpace = memoryPage->size;

                DeviceMemory *node = getFreeDeviceMemoryNode();
                node->size = _size;
                node->offset = 0;
                node->page = memoryPage;
                node->useMemory = false;
                node->memory = allocatedMemory;
                node->next = NULL;
                node->back = NULL;

                memoryPage->beginDeviceMemoryIterators = node;
                return memoryPage;
            }
            else
            {
                continue;
            }
        }

        return NULL;
    }

    bool GpuMemoryController::tryGetFreeMemoryFromBufferPages(MemoryUseType _use, int _size, int _alignment, RequireMemoryType _memoryType,
                                                              DeviceMemory **_outMemoryNode)
    {
        std::map<RequireMemoryType, PrepareMemoryPage *> *bufferPages = NULL;
        int maxSize = getSizePage(_use);
        if (_use == MemoryUseType::MemoryUseType_Long)
        {
            bufferPages = &beginLongMemoryPages;
        }
        else
        {
            bufferPages = &beginMediumMemoryPages;
        }

        if (maxSize < _size)
        {
            return false;
        }

        PrepareMemoryPage *beginPage = bufferPages->at(_memoryType);

    FIND_FREE_DEVICE_MEMORY:
        if (beginPage != NULL)
        {
            PrepareMemoryPage *next = beginPage;
            while (next != NULL)
            {
                PrepareMemoryPage *current = next;
                if (current->freeSpace >= _size)
                {
                    DeviceMemory *memoryNodeNext = current->beginDeviceMemoryIterators;
                    while (memoryNodeNext != NULL)
                    {
                        int nSize = memoryNodeNext->size;
                        int nOffset = memoryNodeNext->offset;
                        if (nSize >= _size && memoryNodeNext->useMemory == false)
                        {
                            if (nOffset % _alignment != 0)
                            {
                                int add = _alignment - (nOffset % _alignment);
                                if (nSize >= _size + add)
                                {
                                    *_outMemoryNode = memoryNodeNext;
                                    return true;
                                }
                            }
                            else
                            {
                                *_outMemoryNode = memoryNodeNext;
                                return true;
                            }
                        }
                        memoryNodeNext = memoryNodeNext->next;
                    }
                }
                next = next->nextPage;
            }
        }

        // creating page
        PrepareMemoryPage *newPage = createPage(maxSize, _memoryType);
        if (newPage == NULL)
        {
            return false;
        }

        addPageToBuffer(*bufferPages, _memoryType, newPage);
        beginPage = newPage;
        goto FIND_FREE_DEVICE_MEMORY;
    }

    DeviceMemory *GpuMemoryController::getFreeDeviceMemoryNode()
    {
        if (beginDeviceMemoryNodes == NULL)
        {
            allocateAdditionalDeviceMemoryNodes();
        }
        DeviceMemory *takeNode = beginDeviceMemoryNodes;
        beginDeviceMemoryNodes = takeNode->next;

        takeNode->next = NULL;
        return takeNode;
    }

    void GpuMemoryController::returnDeviceMemoryNode(DeviceMemory *_node)
    {
        *_node = {};
        _node->next = beginDeviceMemoryNodes;
        beginDeviceMemoryNodes = _node;
    }

    void GpuMemoryController::allocateAdditionalDeviceMemoryNodes()
    {
        int count = config.sizePrepareDeviceMemoryNodes;
        DeviceMemory *buffer = new DeviceMemory[count];
        allocatedDeviceMemoryNodes.push_back(buffer);

        for (int i = 0; i < count; i++)
        {
            DeviceMemory *node = &(buffer[i]);
            *node = {};
            node->next = beginDeviceMemoryNodes;
            beginDeviceMemoryNodes = node;
        }
    }

    void GpuMemoryController::clearListPages(PrepareMemoryPage *_beginList)
    {
        PrepareMemoryPage *next = _beginList;
        while (next != NULL)
        {
            PrepareMemoryPage *current = next;
            next = next->nextPage;

            device->vkFreeMemory(*device, current->memory, NULL);
            delete current;
        }
    }
}