#include <iostream>
#include <cstring>
#include <vulkan/vulkan.h>

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanInstance.h"

namespace phys::vk
{
#define DEVICE_LEVEL_VULKAN_FUNCTION(name) name = (PFN_##name)loadFunction(#name, NULL, _createInfo);
#define DEVICE_LEVEL_VULKAN_FUNCTION_EXTENSION(name, extension) name = (PFN_##name)loadFunction(#name, extension, _createInfo);

    VulkanDevice::VulkanDevice(VkPhysicalDevice _physicalDevice, const VkDeviceCreateInfo *_createInfo, VulkanInstance *_instance)
        : instance(_instance), physicalDevice(_physicalDevice)
    {
        if (instance->vkCreateDevice(_physicalDevice, _createInfo, NULL, &device) != VK_SUCCESS)
        {
            std::cerr << "VkInstance didn`t created!" << std::endl;
            device = VK_NULL_HANDLE;
            return;
        }

#include "exportedVulkanFunctions.inl"

        instance->vkGetPhysicalDeviceFeatures(_physicalDevice, &features);
        instance->vkGetPhysicalDeviceProperties(_physicalDevice, &properties);
        instance->vkGetPhysicalDeviceMemoryProperties(_physicalDevice, &memoryProperties);

        uint countQueueFamilies = 0;
        instance->vkGetPhysicalDeviceQueueFamilyProperties(_physicalDevice, &countQueueFamilies, NULL);
        queueFamiliesProperties.resize(countQueueFamilies);
        instance->vkGetPhysicalDeviceQueueFamilyProperties(_physicalDevice, &countQueueFamilies, queueFamiliesProperties.data());

        printf("Count queue families: %i\n", countQueueFamilies);

        for (uint i = 0; i < countQueueFamilies; i++)
        {
            printf("There is queue: %i :", i);
            if ((queueFamiliesProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT)
                std::cout << "Graphic, ";
            if ((queueFamiliesProperties[i].queueFlags & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT)
                std::cout << "Transfer, ";
            if ((queueFamiliesProperties[i].queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT)
                std::cout << "Compute, ";
            if ((queueFamiliesProperties[i].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) == VK_QUEUE_SPARSE_BINDING_BIT)
                std::cout << "Sparse, ";
            if ((queueFamiliesProperties[i].queueFlags & VK_QUEUE_PROTECTED_BIT) == VK_QUEUE_PROTECTED_BIT)
                std::cout << "Protected, ";
            std::cout << std::endl;
        }

        int queueCreateInfoCount = (int)_createInfo->queueCreateInfoCount;
        for (int i = 0; i < queueCreateInfoCount; i++)
        {
            VkDeviceQueueCreateInfo queueCreateInfo = _createInfo->pQueueCreateInfos[i];
            int familyIndex = (int)queueCreateInfo.queueFamilyIndex;
            queueFamilies.push_back(familyIndex);

            int queueCount = (int)queueCreateInfo.queueCount;
            for (int i2 = 0; i2 < queueCount; i2++)
            {
                VkQueue queue{};
                this->vkGetDeviceQueue(device, queueCreateInfo.queueFamilyIndex, i2, &queue);
                VulkanQueue *vulkanQueue = new VulkanQueue(this, queue, familyIndex, queueFamiliesProperties[familyIndex].queueFlags);
                std::cout << "Added queue: family: " << familyIndex << " types: ";
                if ((queueFamiliesProperties[familyIndex].queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT)
                    std::cout << "Graphic, ";
                if ((queueFamiliesProperties[familyIndex].queueFlags & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT)
                    std::cout << "Transfer, ";
                if ((queueFamiliesProperties[familyIndex].queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT)
                    std::cout << "Compute, ";
                if ((queueFamiliesProperties[familyIndex].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) == VK_QUEUE_SPARSE_BINDING_BIT)
                    std::cout << "Sparse, ";
                if ((queueFamiliesProperties[familyIndex].queueFlags & VK_QUEUE_PROTECTED_BIT) == VK_QUEUE_PROTECTED_BIT)
                    std::cout << "Protected, ";
                std::cout << std::endl;

                queues.push_back(vulkanQueue);
                unusedQueues.push_back(vulkanQueue);
            }
        }
    }

    VulkanDevice::~VulkanDevice()
    {
        for (auto i : queues)
        {
            delete i;
        }
        queues.clear();
        unusedQueues.clear();
        if (device != VK_NULL_HANDLE)
        {
            this->vkDestroyDevice(device, NULL);
            device = VK_NULL_HANDLE;
        }
    }

    VulkanQueue *VulkanDevice::getFreeQueue(VkQueueFlags _queueFlag)
    {
        auto end = unusedQueues.end();
        std::list<VulkanQueue *>::iterator found = end;
        for (auto i = unusedQueues.begin(); i != end; ++i)
        {
            VkQueueFlags flag = (*i)->getQueueFlag();
            if ((flag & _queueFlag) == _queueFlag)
            {
                found = i;
                if ((flag & (~_queueFlag)) == 0)
                {
                    break;
                }
            }
        }

        if (found == end)
        {
            return NULL;
        }

        VulkanQueue *foundQueue = *found;
        unusedQueues.erase(found);
        return foundQueue;
    }

    void VulkanDevice::returnQueue(VulkanQueue *_queue)
    {
        auto end = unusedQueues.end();
        for (auto i = unusedQueues.begin(); i != end; ++i)
        {
            if (*i == _queue)
            {
                return;
            }
        }

        unusedQueues.push_back(_queue);
    }

    void VulkanDevice::setNameObject(const char *_name, uint64_t _object, VkDebugReportObjectTypeEXT _type)
    {
        if (vkDebugMarkerSetObjectNameEXT)
        {
            VkDebugMarkerObjectNameInfoEXT name{};
            name.sType = VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT;
            name.object = _object;
            name.objectType = _type;
            name.pObjectName = _name;
            vkDebugMarkerSetObjectNameEXT(device, &name);
        }
    }

    PFN_vkVoidFunction VulkanDevice::loadFunction(const char *_name, const char *_extension, const VkDeviceCreateInfo *_createInfo)
    {
        if (_extension != NULL)
        {
            bool found = false;
            for (int i = 0; i < (int)_createInfo->enabledExtensionCount; i++)
            {
                if (std::strcmp(_extension, _createInfo->ppEnabledExtensionNames[i]) == 0)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                return NULL;
            }
        }

        PFN_vkVoidFunction func = instance->vkGetDeviceProcAddr(device, _name);
        if (!func)
        {
            std::cerr << "Instance function: " << std::string(_name) << " not loaded!" << std::endl;
        }
        return func;
    }
}