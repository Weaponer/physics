#include "gpuCommon/vulkanFence.h"
#include "gpuCommon/vulkanDevice.h"

namespace phys::vk
{
    void VulkanFence::reset()
    {
        device->vkResetFences(*device, 1, &fence);
    }

    void VulkanFence::wait(uint64_t _timeout)
    {
        device->vkWaitForFences(*device, 1, &fence, true, _timeout);
    }

    bool VulkanFence::isSignaled()
    {
        return device->vkGetFenceStatus(*device, fence) == VK_SUCCESS;
    }

    bool VulkanFence::createFence(VulkanDevice *_device, VulkanFence *_outFence, bool _signalEnable, const char *_name)
    {
        VkFenceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        if (_signalEnable)
        {
            createInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
        }

        VkFence internalFence;
        if (_device->vkCreateFence(*_device, &createInfo, NULL, &internalFence) != VK_SUCCESS)
        {
            return false;
        }
        
        if (_name)
            _device->setNameObject(_name, (uint64_t)internalFence, VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT);

        *_outFence = VulkanFence(_device, internalFence);
        return true;
    }

    void VulkanFence::destroyFence(VulkanFence *_fence)
    {
        VulkanDevice *device = _fence->device;
        device->vkDestroyFence(*device, *_fence, NULL);
    }
}