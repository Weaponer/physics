#include <iostream>

#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanCommandPool.h"

namespace phys::vk
{
    VulkanCommandPool::VulkanCommandPool(VkCommandPool _commandPool, VulkanDevice *_device)
        : commandPool(_commandPool), device(_device), countReservedCommandBuffers(0),
          indexWaitSemaphoresBuffer(0), freeWaitSemaphores(0),
          beginFreeIterators(NULL), beginFreeCommandBuffers(NULL), beginWritedCommandBuffers(NULL), endWritedCommandBuffers(NULL),
          lastCmdIsEndSubmitPart(false),
          countWritedCommandBuffers(0), countSubmitParts(1), countWaitSemaphores(0), countSignalSemaphores(0)
    {
    }

    VulkanCommandPool::~VulkanCommandPool()
    {
        clearListNodes(beginFreeCommandBuffers);
        clearListNodes(beginFreeIterators);
        beginFreeIterators = NULL;
        clearListNodes(beginWritedCommandBuffers);
        beginWritedCommandBuffers = NULL;
        for (int i = 0; i < (int)waitSemaphoresBuffer.size(); i++)
        {
            delete waitSemaphoresBuffer[i];
        }
        waitSemaphoresBuffer.clear();
    }

    void VulkanCommandPool::reset()
    {
        device->vkResetCommandPool(*device, commandPool, 0);
        lastCmdIsEndSubmitPart = false;
        countWritedCommandBuffers = 0;
        countSubmitParts = 1;
        countWaitSemaphores = 0;
        countSignalSemaphores = 0;

        CommandBufferNode *next = beginWritedCommandBuffers;
        while (next != NULL)
        {
            CommandBufferNode *current = next;
            next = next->next;
            returnFreeCommandBuffer(current->commandBuffer);
            returnFreeNode(current);
        }
        beginWritedCommandBuffers = NULL;
        endWritedCommandBuffers = NULL;

        resetWaitSemaphoresBuffer();
    }

    VulkanCommandBuffer VulkanCommandPool::beginCommandBuffer(bool _useRenderings)
    {
        VulkanCommandBuffer buffer = getFreeCommandBuffer();

        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT | (_useRenderings ? VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT : 0);
        beginInfo.pInheritanceInfo = NULL;
        VkResult result = device->vkBeginCommandBuffer(buffer, &beginInfo);
        if (result != VK_SUCCESS)
        {
            std::cout << "can`t begin command buffer!" << std::endl;
        }
        return buffer;
    }

    void VulkanCommandPool::endCommandBuffer(VulkanCommandBuffer _commandBuffer, int _countWaitSemaphore, VulkanSemaphore *_waitSemaphores,
                                             VkPipelineStageFlags *_waitStages, VulkanSemaphore _signalSemaphore)
    {
        device->vkEndCommandBuffer(_commandBuffer);
        CommandBufferNode *node = getFreeNode();
        node->commandBuffer = _commandBuffer;
        node->signalSemaphore = _signalSemaphore;
        if (_signalSemaphore.isValid())
        {
            countSignalSemaphores++;
        }
        if (_countWaitSemaphore != 0)
        {
            std::pair<VulkanSemaphore, VkPipelineStageFlags> *waitSemaphores = getWaitSemaphores(_countWaitSemaphore);
            for (int i = 0; i < _countWaitSemaphore; i++)
            {
                waitSemaphores[i].first = _waitSemaphores[i];
                waitSemaphores[i].second = _waitStages[i];
            }
            node->countWaitSemaphores = _countWaitSemaphore;
            node->waitSemaphores = waitSemaphores;
            countWaitSemaphores += _countWaitSemaphore;
        }
        bool isCurrentBufferEndSubmit = false;
        if (_countWaitSemaphore != 0 || _signalSemaphore.isValid())
        {
            isCurrentBufferEndSubmit = true;
        }
        if (lastCmdIsEndSubmitPart || isCurrentBufferEndSubmit)
        {
            countSubmitParts++;
        }

        if (endWritedCommandBuffers == NULL)
        {
            endWritedCommandBuffers = node;
            node->next = NULL;
            beginWritedCommandBuffers = node;
        }
        else
        {
            endWritedCommandBuffers->next = node;
            node->next = NULL;
            endWritedCommandBuffers = node;
        }
        lastCmdIsEndSubmitPart = isCurrentBufferEndSubmit;
        countWritedCommandBuffers++;
    }

    int VulkanCommandPool::fillQueueSubmitInfo(int _sizeSubmitInfosBuffer, VkSubmitInfo *_submitInfosBuffer,
                                               int _sizeCommanBuffersBuffer, VkCommandBuffer *_commandBuffersBuffer,
                                               int _sizeWaitSemaphoresBuffer, VkSemaphore *_waitSemaphoresBuffer, VkPipelineStageFlags *_waitStagesBuffer,
                                               int _sizeSignalSemaphoresBuffer, VkSemaphore *_signalSemaphoresBuffer)
    {
        if (countWritedCommandBuffers == 0 || _sizeSubmitInfosBuffer < countSubmitParts ||
            _sizeCommanBuffersBuffer < countWritedCommandBuffers ||
            _sizeWaitSemaphoresBuffer < countWaitSemaphores ||
            _sizeSignalSemaphoresBuffer < countSignalSemaphores)
            return 0;

        int indexSubmitInfo = 0;
        int startIndexCommandBuffer = 0;
        int indexCommandBuffer = 0;
        int indexWaitSemaphore = 0;
        int indexSignalSemaphore = 0;

        VkSubmitInfo submitInfoData;
        submitInfoData.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfoData.pNext = NULL;

        CommandBufferNode *next = beginWritedCommandBuffers;
        while (next != NULL)
        {
            CommandBufferNode *current = next;
            next = next->next;

            bool endSubmitInfo = false;

            _commandBuffersBuffer[indexCommandBuffer] = current->commandBuffer;
            indexCommandBuffer++;
            if (current->countWaitSemaphores != 0)
            {
                int countWSemaphores = current->countWaitSemaphores;
                for (int i = 0; i < countWSemaphores; i++)
                {
                    _waitSemaphoresBuffer[indexWaitSemaphore] = current->waitSemaphores[i].first;
                    _waitStagesBuffer[indexWaitSemaphore] = current->waitSemaphores[i].second;
                    indexWaitSemaphore++;
                }

                endSubmitInfo = true;
            }
            if (current->signalSemaphore.isValid())
            {
                _signalSemaphoresBuffer[indexSignalSemaphore] = current->signalSemaphore;
                indexSignalSemaphore++;
                endSubmitInfo = true;
            }
            if (current->next == NULL)
            {
                endSubmitInfo = true;
            }

            if (endSubmitInfo)
            {
                if (current->signalSemaphore.isValid())
                {
                    submitInfoData.signalSemaphoreCount = 1;
                    submitInfoData.pSignalSemaphores = _signalSemaphoresBuffer + indexSignalSemaphore - 1;
                }
                else
                {
                    submitInfoData.signalSemaphoreCount = 0;
                    submitInfoData.pSignalSemaphores = NULL;
                }

                if (current->countWaitSemaphores != 0)
                {
                    submitInfoData.waitSemaphoreCount = current->countWaitSemaphores;
                    int indexStartWaitSemaphore = (indexWaitSemaphore - submitInfoData.waitSemaphoreCount);
                    submitInfoData.pWaitSemaphores = _waitSemaphoresBuffer + indexStartWaitSemaphore;
                    submitInfoData.pWaitDstStageMask = _waitStagesBuffer + indexStartWaitSemaphore;
                }
                else
                {
                    submitInfoData.waitSemaphoreCount = 0;
                    submitInfoData.pWaitSemaphores = NULL;
                    submitInfoData.pWaitDstStageMask = NULL;
                }

                submitInfoData.commandBufferCount = indexCommandBuffer - startIndexCommandBuffer;
                submitInfoData.pCommandBuffers = _commandBuffersBuffer + startIndexCommandBuffer;

                startIndexCommandBuffer = indexCommandBuffer;
                _submitInfosBuffer[indexSubmitInfo] = submitInfoData;
                indexSubmitInfo++;
            }
        }

        return indexSubmitInfo;
    }

    int VulkanCommandPool::getWritedCommandBuffers(int _count, VulkanCommandBuffer **_bufferCommandBuffers)
    {
        int countBuffers = 0;
        CommandBufferNode *next = beginWritedCommandBuffers;
        while (_count-- && next != NULL)
        {
            CommandBufferNode *current = next;
            next = next->next;

            _bufferCommandBuffers[countBuffers] = &current->commandBuffer;
            countBuffers++;
        }
        return countBuffers;
    }

    VulkanCommandPool::CommandBufferNode *VulkanCommandPool::getFreeNode()
    {
        if (!beginFreeIterators)
        {
            CommandBufferNode *node = new CommandBufferNode();
            node->next = NULL;
            node->countWaitSemaphores = 0;
            node->waitSemaphores = NULL;
            node->signalSemaphore = VulkanSemaphore();
            node->commandBuffer = VulkanCommandBuffer(VK_NULL_HANDLE, NULL);
            return node;
        }
        CommandBufferNode *node = beginFreeIterators;
        beginFreeIterators = node->next;
        return node;
    }

    void VulkanCommandPool::returnFreeNode(CommandBufferNode *_node)
    {
        _node->commandBuffer = VulkanCommandBuffer(VK_NULL_HANDLE, NULL);
        *_node = {};
        _node->next = beginFreeIterators;
        beginFreeIterators = _node;
    }

    VulkanCommandBuffer VulkanCommandPool::getFreeCommandBuffer()
    {
        VkCommandBuffer buffer{};
        if (countReservedCommandBuffers != 0)
        {
            countReservedCommandBuffers--;
            buffer = reservedCommandBuffers[countReservedCommandBuffers];
        }
        else if (beginFreeCommandBuffers != NULL)
        {
            buffer = beginFreeCommandBuffers->commandBuffer;
            CommandBufferNode *node = beginFreeCommandBuffers;
            beginFreeCommandBuffers = node->next;
            returnFreeNode(node);
        }
        else
        {
            countReservedCommandBuffers = SIZE_ALLOCATE_COMMAND_BUFFERS;

            VkCommandBufferAllocateInfo bufferAllocatorInfo{};
            bufferAllocatorInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            bufferAllocatorInfo.commandPool = commandPool;
            bufferAllocatorInfo.commandBufferCount = countReservedCommandBuffers;
            bufferAllocatorInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            VkResult result = device->vkAllocateCommandBuffers(*device, &bufferAllocatorInfo, reservedCommandBuffers.data());
            if (result != VK_SUCCESS)
            {
                std::cout << "can`t allocate command buffers!" << std::endl;
            }
            countReservedCommandBuffers--;
            buffer = reservedCommandBuffers[countReservedCommandBuffers];
        }

        return VulkanCommandBuffer(buffer, device);
    }

    void VulkanCommandPool::returnFreeCommandBuffer(VulkanCommandBuffer _cmd)
    {
        CommandBufferNode *node = getFreeNode();
        node->commandBuffer = _cmd;
        node->next = beginFreeCommandBuffers;
        beginFreeCommandBuffers = node;
    }

    void VulkanCommandPool::clearListNodes(CommandBufferNode *_list)
    {
        CommandBufferNode *next = _list;
        while (next != NULL)
        {
            CommandBufferNode *current = next;
            next = next->next;
            delete current;
        }
    }

    std::pair<VulkanSemaphore, VkPipelineStageFlags> *VulkanCommandPool::getWaitSemaphores(int _count)
    {
        if (_count > MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER)
            return NULL;

        if (freeWaitSemaphores < _count)
        {
            std::array<std::pair<VulkanSemaphore, VkPipelineStageFlags>, MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER> *newData =
                new std::array<std::pair<VulkanSemaphore, VkPipelineStageFlags>, MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER>();
            waitSemaphoresBuffer.push_back(newData);
            freeWaitSemaphores = MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER;
            indexWaitSemaphoresBuffer = (int)waitSemaphoresBuffer.size() - 1;
        }

        std::pair<VulkanSemaphore, VkPipelineStageFlags> *bufferWaitSemaphores = waitSemaphoresBuffer[indexWaitSemaphoresBuffer]->data();
        std::pair<VulkanSemaphore, VkPipelineStageFlags> *waitSemaphores = bufferWaitSemaphores + (MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER - freeWaitSemaphores);
        freeWaitSemaphores -= _count;
        return waitSemaphores;
    }

    void VulkanCommandPool::resetWaitSemaphoresBuffer()
    {
        freeWaitSemaphores = waitSemaphoresBuffer.size() != 0 ? MAX_WAIT_SEMAPHORES_PER_COMMAND_BUFFER : 0;
        indexWaitSemaphoresBuffer = 0;
    }
}