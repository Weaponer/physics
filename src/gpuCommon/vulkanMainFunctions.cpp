#include <iostream>
#include <vector>
#include <cstring>

#include "gpuCommon/vulkanMainFunctions.h"

namespace phys::vk
{
#define GLOBAL_LEVEL_VULKAN_FUNCTION(name) name = (PFN_##name)loadFunction(#name);

    VulkanMainFunctions::VulkanMainFunctions(VulkanExportedLibrary *_vkExportedLibrary)
        : vkExportedLibrary(_vkExportedLibrary)
    {
#include "exportedVulkanFunctions.inl"

        uint countSupportLayers = 0;
        std::vector<VkLayerProperties> layers;
        this->vkEnumerateInstanceLayerProperties(&countSupportLayers, NULL);
        layers.resize(countSupportLayers);
        this->vkEnumerateInstanceLayerProperties(&countSupportLayers, layers.data());
        for (int i = 0; i < (int)layers.size(); i++)
        {
            availableLayers.push_back(layers[i].layerName);
        }

        uint countExtensions = 0;
        std::vector<VkExtensionProperties> extensions;
        this->vkEnumerateInstanceExtensionProperties(NULL, &countExtensions, NULL);
        extensions.resize(countExtensions);

        this->vkEnumerateInstanceExtensionProperties(NULL, &countExtensions, extensions.data());
        for (int i = 0; i < (int)extensions.size(); i++)
        {
            availableExtensions.push_back(extensions[i].extensionName);
        }
    }

    std::vector<const char *> VulkanMainFunctions::getSupportedLayers(int _count, const char **_layers)
    {
        std::vector<const char *> result;
        for (int i = 0; i < _count; i++)
            if (layerAvailable(_layers[i]))
                result.push_back(_layers[i]);
        return result;
    }

    bool VulkanMainFunctions::layerAvailable(const char *_layer)
    {
        for (auto i : availableLayers)
            if (std::strcmp(_layer, i.c_str()) == 0)
                return true;
        return false;
    }

    std::vector<const char *> VulkanMainFunctions::getSupportedExtensions(int _count, const char **_extensions)
    {
        std::vector<const char *> result;
        for (int i = 0; i < _count; i++)
            if (extensionsAvailable(_extensions[i]))
                result.push_back(_extensions[i]);
        return result;
    }

    bool VulkanMainFunctions::extensionsAvailable(const char *_extension)
    {
        for (auto i : availableExtensions)
            if (std::strcmp(_extension, i.c_str()) == 0)
                return true;
        return false;
    }

    PFN_vkVoidFunction VulkanMainFunctions::loadFunction(const char *_name)
    {
        PFN_vkVoidFunction func = vkExportedLibrary->vkGetInstanceProcAddr(NULL, _name);
        if (!func)
        {
            std::cerr << "Global function: " << std::string(_name) << " not loaded!" << std::endl;
        }
        return func;
    }
}