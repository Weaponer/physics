#include <dlfcn.h>
#include <iostream>
#include "vulkanExportedLibrary.h"

namespace phys::vk
{
#define EXPORTED_VULKAN_FUNCTION( name ) name = (PFN_##name)loadFunction(#name);

    VulkanExportedLibrary::VulkanExportedLibrary(PhVulkanLibraryProvider *_provider)
        : provider(_provider)
    {
        #include "exportedVulkanFunctions.inl"
    }

    void *VulkanExportedLibrary::loadFunction(const char *_name)
    {
        void *func = provider->loadFunction(_name);
        if(!func)
        {
            std::cerr << "Exported function: " << std::string(_name) << " not loaded!" << std::endl;
        } 
        return func;
    }
}