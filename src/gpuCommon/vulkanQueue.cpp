#include <algorithm>

#include "gpuCommon/vulkanQueue.h"
#include "gpuCommon/vulkanDevice.h"
#include "gpuCommon/vulkanCommandPool.h"

namespace phys::vk
{
    VulkanQueue::VulkanQueue(VulkanDevice *_device, VkQueue _queue, int _familyIndex, VkQueueFlags _queueFlag)
        : device(_device), queue(_queue), familyIndex(_familyIndex), queueFlag(_queueFlag)
    {
    }

    VulkanQueue::~VulkanQueue()
    {
        auto end = commandPools.end();
        for (auto i = commandPools.begin(); i != end; ++i)
        {
            (*i)->reset();
            device->vkDestroyCommandPool(*device, *(*i), NULL);
            delete (*i);
        }
    }

    VulkanCommandPool *VulkanQueue::getCommandPool()
    {
        VkCommandPoolCreateInfo commandPoolCreateInfo{};
        commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolCreateInfo.queueFamilyIndex = familyIndex;
        commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
        VkCommandPool pool{};
        if (device->vkCreateCommandPool(*device, &commandPoolCreateInfo, NULL, &pool) == VK_SUCCESS)
        {
            VulkanCommandPool *commandPool = new VulkanCommandPool(pool, device);
            commandPools.push_back(commandPool);
            return commandPool;
        }
        return NULL;
    }

    void VulkanQueue::returnCommandPool(VulkanCommandPool *_commandPool)
    {
        auto found = std::find(commandPools.begin(), commandPools.end(), _commandPool);
        if (found != commandPools.end())
        {
            _commandPool->reset();
            device->vkDestroyCommandPool(*device, *_commandPool, NULL);
            delete _commandPool;

            commandPools.erase(found);
        }
    }

    void VulkanQueue::submit()
    {
        int countCommandBuffers = 0;

        auto end = commandPools.end();
        for (auto i = commandPools.begin(); i != end; ++i)
        {
            countCommandBuffers += (*i)->getCountWritedCommandBuffers();
        }

        if (countCommandBuffers == 0)
        {
            return;
        }

        VulkanCommandBuffer *pointersbuffers[countCommandBuffers];
        int indexBuffer = 0;
        for (auto i = commandPools.begin(); i != end; ++i)
        {
            int countBuffers = (*i)->getCountWritedCommandBuffers();
            (*i)->getWritedCommandBuffers(countBuffers, &(pointersbuffers[indexBuffer]));
            indexBuffer += countBuffers;
        }

        VkCommandBuffer buffers[countCommandBuffers];
        for (int i = 0; i < countCommandBuffers; i++)
        {
            buffers[i] = *pointersbuffers[i];
        }

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = countCommandBuffers;
        submitInfo.pCommandBuffers = buffers;

        device->vkQueueSubmit(*this, 1, &submitInfo, VK_NULL_HANDLE);
    }

    void VulkanQueue::submit(int _countCommadPools, VulkanCommandPool **_commandPools,
                             int _countWaitSemaphores, VulkanSemaphore *_waitSemaphores, const VkPipelineStageFlags *_waitDstStages,
                             VulkanFence _fence, int _countSignalSemaphores, VulkanSemaphore *_signalsSemaphores)
    {
        int countCommandBuffers = 0;
        for (int i = 0; i < _countCommadPools; i++)
            countCommandBuffers += (_commandPools[i])->getCountWritedCommandBuffers();

        if (countCommandBuffers == 0)
        {
            return;
        }

        VulkanCommandBuffer *pointersbuffers[countCommandBuffers];
        int indexBuffer = 0;
        for (int i = 0; i < _countCommadPools; i++)
        {
            int countBuffers = (_commandPools[i])->getCountWritedCommandBuffers();
            (_commandPools[i])->getWritedCommandBuffers(countBuffers, &(pointersbuffers[indexBuffer]));
            indexBuffer += countBuffers;
        }

        VkCommandBuffer buffers[countCommandBuffers];
        for (int i = 0; i < countCommandBuffers; i++)
            buffers[i] = *pointersbuffers[i];

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = countCommandBuffers;
        submitInfo.pCommandBuffers = buffers;

        submitInfo.signalSemaphoreCount = _countSignalSemaphores;
        VkSemaphore semaphores[std::max(1, _countSignalSemaphores)];
        for (int i = 0; i < _countSignalSemaphores; i++)
        {
            semaphores[i] = (_signalsSemaphores[i]);
        }

        if (_countSignalSemaphores == 0)
        {
            submitInfo.pSignalSemaphores = NULL;
        }
        else
        {
            submitInfo.pSignalSemaphores = semaphores;
        }

        submitInfo.waitSemaphoreCount = _countWaitSemaphores;
        VkSemaphore waitSemaphores[std::max(1, _countWaitSemaphores)];
        for (int i = 0; i < _countWaitSemaphores; i++)
        {
            waitSemaphores[i] = (_waitSemaphores[i]);
        }

        if (_countSignalSemaphores == 0)
        {
            submitInfo.pWaitSemaphores = NULL;
            submitInfo.pWaitDstStageMask = NULL;
        }
        else
        {
            submitInfo.pWaitSemaphores = waitSemaphores;
            submitInfo.pWaitDstStageMask = _waitDstStages;
        }

        device->vkQueueSubmit(*this, 1, &submitInfo, _fence);
    }

    void VulkanQueue::waitQueue()
    {
        device->vkQueueWaitIdle(*this);
    }
}