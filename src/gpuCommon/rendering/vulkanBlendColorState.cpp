#include "gpuCommon/rendering/vulkanBlendColorState.h"

namespace phys::vk
{
    VulkanBlendColorState::VulkanBlendColorState()
    {
        colorBlendingState.pNext = NULL;
        colorBlendingState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlendingState.flags = 0;
        colorBlendingState.logicOpEnable = VK_FALSE;
        colorBlendingState.logicOp = VK_LOGIC_OP_COPY;
        colorBlendingState.attachmentCount = 1;
        colorBlendingState.pAttachments = blends.data();
        colorBlendingState.blendConstants[0] = 0.0f;
        colorBlendingState.blendConstants[1] = 0.0f;
        colorBlendingState.blendConstants[2] = 0.0f;
        colorBlendingState.blendConstants[3] = 0.0f;

        VkPipelineColorBlendAttachmentState state;
        state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        state.blendEnable = VK_FALSE;
        state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
        state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        state.colorBlendOp = VK_BLEND_OP_ADD;
        state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        state.alphaBlendOp = VK_BLEND_OP_ADD;
        for (int i = 0; i < MAX_COLOR_ATTACHMENTS; i++)
            blends[i] = state;
    }

    VulkanBlendColorState::~VulkanBlendColorState()
    {
    }

    void VulkanBlendColorState::setColorBlendAttachmentState(int _index, VkColorComponentFlags _maskWrite, bool _blendEnable,
                                                             VkBlendOp _colorBlendOp, VkBlendOp _alphaBlendOp,
                                                             VkBlendFactor _srcColor, VkBlendFactor _srcAlpha,
                                                             VkBlendFactor _dstColor, VkBlendFactor _dstAlpha)
    {
        VkPipelineColorBlendAttachmentState state;
        state.colorWriteMask = _maskWrite;
        state.blendEnable = _blendEnable;
        state.srcColorBlendFactor = _srcColor;
        state.dstColorBlendFactor = _dstColor;
        state.colorBlendOp = _colorBlendOp;
        state.srcAlphaBlendFactor = _srcAlpha;
        state.dstAlphaBlendFactor = _dstAlpha;
        state.alphaBlendOp = _alphaBlendOp;
        blends[_index] = state;
    }
}