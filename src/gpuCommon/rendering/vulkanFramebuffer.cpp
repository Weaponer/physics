#include <algorithm>

#include "gpuCommon/rendering/vulkanFramebuffer.h"

namespace phys::vk
{
    bool VulkanFramebuffer::createFramebuffer(VulkanDevice *_device, VulkanRenderPass &_renderPass, int _countColorAttachments,
                                              VulkanImage **_imagesColor, VulkanImageView *_viewsColor,
                                              VulkanImage *_imageDepthStencil, VulkanImageView _viewDepthStencil,
                                              VulkanFramebuffer *_outFramebuffer)
    {
        int width = INT32_MAX;
        int height = INT32_MAX;
        bool needDepthStencil = _imageDepthStencil != NULL;

        int countAttachments = _countColorAttachments;
        if (needDepthStencil)
            countAttachments++;

        VkImageView attachments[countAttachments];
        for (int i = 0; i < _countColorAttachments; i++)
        {
            width = std::min(_imagesColor[i]->getWidth(), width);
            height = std::min(_imagesColor[i]->getHeight(), height);

            attachments[i] = _viewsColor[i];
        }
        if (needDepthStencil)
        {
            width = std::min(_imageDepthStencil->getWidth(), width);
            height = std::min(_imageDepthStencil->getHeight(), height);
            attachments[_countColorAttachments] = _viewDepthStencil;
        }

        VkFramebufferCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        createInfo.attachmentCount = countAttachments;
        createInfo.pAttachments = attachments;
        createInfo.renderPass = _renderPass;
        createInfo.width = width;
        createInfo.height = height;
        createInfo.layers = 1;

        VkFramebuffer framebuffer;
        if (_device->vkCreateFramebuffer(*_device, &createInfo, NULL, &framebuffer) != VK_SUCCESS)
            return false;

        *_outFramebuffer = VulkanFramebuffer(framebuffer, width, height);
        return true;
    }

    void VulkanFramebuffer::destroyFramebuffer(VulkanDevice *_device, VulkanFramebuffer *_framebuffer)
    {
        _device->vkDestroyFramebuffer(*_device, *_framebuffer, NULL);
        *_framebuffer = VulkanFramebuffer();
    }
}