#include <algorithm>

#include "gpuCommon/rendering/renderPassesManager.h"

namespace phys::vk
{
    RenderPassesManager::RenderPassesManager(VulkanDevice *_device)
        : device(_device), colorAttachments(), maxColorAttachments(MAX_COLOR_ATTACHMENTS), countColorAttachments(0),
          depthStencilAttachment(), beginCreate(false), renderPasses(NULL), countRenderPasses(0)
    {
        maxColorAttachments = std::min((int)_device->getProperties().limits.maxColorAttachments, maxColorAttachments);
    }

    RenderPassesManager::~RenderPassesManager()
    {
        RenderPassDescription *next = renderPasses;
        while (next != NULL)
        {
            RenderPassDescription *current = next;
            device->vkDestroyRenderPass(*device, current->renderPass, NULL);
            next = next->next;
            delete current;
        }
        renderPasses = NULL;
        countRenderPasses = 0;
    }

    void RenderPassesManager::beginCreateRenderPass()
    {
        countColorAttachments = 0;
        depthStencilAttachment = {VK_FORMAT_UNDEFINED, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_UNDEFINED};
        beginCreate = true;
    }

    bool RenderPassesManager::endCreate(VulkanRenderPass *_outRenderPass)
    {
        if (!beginCreate)
            return false;

        beginCreate = false;

        if (countColorAttachments == 0 && depthStencilAttachment.format == VK_FORMAT_UNDEFINED)
        {
            return false;
        }

        RenderPassesManager::RenderPassDescription descriptor{};
        descriptor.countColorAttachments = countColorAttachments;
        descriptor.colorAttachments = colorAttachments;
        descriptor.depthStencilAttachment = depthStencilAttachment;

        RenderPassesManager::RenderPassDescription *renderPassFound = tryGetCompatibilityRenderPass(&descriptor);
        if (renderPassFound == NULL)
        {
            renderPassFound = createNewRenderPass(&descriptor);
            if (renderPassFound == NULL)
            {
                return false;
            }
        }
        *_outRenderPass = renderPassFound->renderPass;
        return true;
    }

    void RenderPassesManager::setDepthStencil(VulkanImage *_depthStencil, VkImageLayout _initialLayout, VkImageLayout _finalLayout)
    {
        depthStencilAttachment = {_depthStencil->getFormat(), _initialLayout, _finalLayout};
    }

    void RenderPassesManager::addColorAttachments(VulkanImage *_color, VkImageLayout _initialLayout, VkImageLayout _finalLayout)
    {
        if (countColorAttachments >= maxColorAttachments)
            return;

        colorAttachments[countColorAttachments] = {_color->getFormat(), _initialLayout, _finalLayout};
        countColorAttachments++;
    }

    RenderPassesManager::RenderPassDescription *RenderPassesManager::tryGetCompatibilityRenderPass(RenderPassesManager::RenderPassDescription *_descriptor)
    {
        RenderPassDescription *next = renderPasses;
        while (next != NULL)
        {
            RenderPassDescription *current = next;
            next = next->next;
            bool needDepthStencil = _descriptor->depthStencilAttachment.format != VK_FORMAT_UNDEFINED;
            if (current->countColorAttachments < _descriptor->countColorAttachments ||
                (current->depthStencilAttachment.format == VK_FORMAT_UNDEFINED && needDepthStencil) ||
                (needDepthStencil && current->countColorAttachments != _descriptor->countColorAttachments))
                continue;

            if (needDepthStencil && _descriptor->depthStencilAttachment != current->depthStencilAttachment)
                continue;

            int count = _descriptor->countColorAttachments;
            bool none = false;
            for (int i = 0; i < count; i++)
            {
                if (current->colorAttachments[i] != _descriptor->colorAttachments[i])
                {
                    none = true;
                    break;
                }
            }
            if (none)
                continue;

            return current;
        }
        return NULL;
    }

    RenderPassesManager::RenderPassDescription *RenderPassesManager::createNewRenderPass(RenderPassesManager::RenderPassDescription *_descriptor)
    {
        int countColors = _descriptor->countColorAttachments;
        int countDescriptions = countColors;
        bool needDepthStencil = _descriptor->depthStencilAttachment.format != VK_FORMAT_UNDEFINED;
        if (needDepthStencil)
            countDescriptions++;

        VkAttachmentReference colorReferences[std::max(1, countColors)];
        for (int i = 0; i < countColors; i++)
        {
            VkAttachmentReference ref;
            ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            ref.attachment = i;
            colorReferences[i] = ref;
        }
        VkAttachmentReference depthReference;
        depthReference.attachment = countColors;
        depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpass{};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = countColors;
        subpass.pColorAttachments = colorReferences;
        if (needDepthStencil)
        {
            subpass.pDepthStencilAttachment = &depthReference;
        }
        else
        {
            subpass.pDepthStencilAttachment = NULL;
        }

        VkSubpassDependency dependency{};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        dependency.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        VkAttachmentDescription descriptions[countDescriptions];
        for (int i = 0; i < countColors; i++)
        {

            VkAttachmentDescription descriptor{};
            descriptor.format = _descriptor->colorAttachments[i].format;
            descriptor.samples = VK_SAMPLE_COUNT_1_BIT;
            descriptor.finalLayout = _descriptor->colorAttachments[i].finalLayout;
            descriptor.initialLayout = _descriptor->colorAttachments[i].initialLayout;
            descriptor.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
            descriptor.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
            descriptor.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
            descriptor.stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
            descriptions[i] = descriptor;
        }
        if (needDepthStencil)
        {
            VkAttachmentDescription descriptor{};
            descriptor.format = _descriptor->depthStencilAttachment.format;
            descriptor.samples = VK_SAMPLE_COUNT_1_BIT;
            descriptor.finalLayout = _descriptor->depthStencilAttachment.finalLayout;
            descriptor.initialLayout = _descriptor->depthStencilAttachment.initialLayout;
            descriptor.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
            descriptor.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
            descriptor.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
            descriptor.stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
            descriptions[countColors] = descriptor;
        }

        VkRenderPassCreateInfo renderPassInfo{};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassInfo.attachmentCount = (uint32_t)countDescriptions;
        renderPassInfo.pAttachments = descriptions;
        renderPassInfo.subpassCount = 1;
        renderPassInfo.pSubpasses = &subpass;

        renderPassInfo.dependencyCount = 1;
        renderPassInfo.pDependencies = &dependency;

        VkRenderPass renderPass;
        if (device->vkCreateRenderPass(*device, &renderPassInfo, NULL, &renderPass) != VK_SUCCESS)
        {
            return NULL;
        }

        RenderPassesManager::RenderPassDescription *renderPassDescription = new RenderPassesManager::RenderPassDescription();

        renderPassDescription->renderPass = VulkanRenderPass(renderPass, countRenderPasses);
        countRenderPasses++;
        renderPassDescription->next = renderPasses;
        renderPasses = renderPassDescription;
        renderPassDescription->countColorAttachments = _descriptor->countColorAttachments;
        renderPassDescription->depthStencilAttachment = _descriptor->depthStencilAttachment;
        renderPassDescription->colorAttachments = _descriptor->colorAttachments;

        return renderPassDescription;
    }
}