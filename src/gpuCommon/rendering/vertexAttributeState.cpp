#include <MTH/vectors.h>

#include "gpuCommon/rendering/vulkanVertexAttributeState.h"

namespace phys::vk
{
    VulkanVertexAttributeState::VulkanVertexAttributeState()
    {
        vertexInputBindings[0].binding = 0;
        vertexInputBindings[0].stride = 0;
        vertexInputBindings[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        vertexInputBindings[1].binding = 1;
        vertexInputBindings[1].stride = 0;
        vertexInputBindings[1].inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;

        countInputAttributes = 0;
        countInputAttributes = 0;

        vertexInfoState.pNext = NULL;
        vertexInfoState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertexInfoState.flags = 0;
        vertexInfoState.vertexBindingDescriptionCount = 1;
        vertexInfoState.pVertexBindingDescriptions = vertexInputBindings.data();
        vertexInfoState.vertexAttributeDescriptionCount = 0;
        vertexInfoState.pVertexAttributeDescriptions = vertexInputAttributes.data();
    }

    VulkanVertexAttributeState::~VulkanVertexAttributeState()
    {
    }

    bool VulkanVertexAttributeState::addInputVertexAttribute(uint32_t _offset, VkFormat _format)
    {
        if (vertexInfoState.vertexAttributeDescriptionCount >= MAX_INPUT_ATTRIBUTES)
        {
            return false;
        }
        int index = vertexInfoState.vertexAttributeDescriptionCount;
        vertexInfoState.vertexAttributeDescriptionCount++;

        vertexInputAttributes[index].location = countInputAttributes;
        countInputAttributes++;
        vertexInputAttributes[index].binding = 0;
        vertexInputAttributes[index].offset = _offset;
        vertexInputAttributes[index].format = _format;
        return true;
    }

    bool VulkanVertexAttributeState::addInputInstanceAttribute(uint32_t _offset, VkFormat _format)
    {
        if (vertexInfoState.vertexAttributeDescriptionCount >= MAX_INPUT_ATTRIBUTES)
        {
            return false;
        }
        int index = vertexInfoState.vertexAttributeDescriptionCount;
        vertexInfoState.vertexAttributeDescriptionCount++;

        vertexInputAttributes[index].location = countInputAttributes;
        countInputAttributes++;
        vertexInputAttributes[index].binding = 1;
        vertexInputAttributes[index].offset = _offset;
        vertexInputAttributes[index].format = _format;
        return true;
    }

    bool VulkanVertexAttributeState::addInputInstanceAttributeMat4Float(uint32_t _offset)
    {
        return addInputInstanceAttribute(_offset, VK_FORMAT_R32G32B32A32_SFLOAT) &
               addInputInstanceAttribute(_offset + sizeof(mth::vec4), VK_FORMAT_R32G32B32A32_SFLOAT) &
               addInputInstanceAttribute(_offset + sizeof(mth::vec4) * 2, VK_FORMAT_R32G32B32A32_SFLOAT) &
               addInputInstanceAttribute(_offset + sizeof(mth::vec4) * 3, VK_FORMAT_R32G32B32A32_SFLOAT);
    }

    bool VulkanVertexAttributeState::addInputInstanceAttributeMat3Float(uint32_t _offset)
    {
        return addInputInstanceAttribute(_offset, VK_FORMAT_R32G32B32_SFLOAT) &
               addInputInstanceAttribute(_offset + sizeof(mth::vec3), VK_FORMAT_R32G32B32_SFLOAT) &
               addInputInstanceAttribute(_offset + sizeof(mth::vec3) * 2, VK_FORMAT_R32G32B32_SFLOAT);
    }

    void VulkanVertexAttributeState::clearAttributes()
    {
        vertexInfoState.vertexAttributeDescriptionCount = 0;
        countInputAttributes = 0;
    }
}