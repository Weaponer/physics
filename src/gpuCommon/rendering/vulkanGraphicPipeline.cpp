#include <algorithm>

#include "gpuCommon/rendering/vulkanGraphicPipeline.h"

namespace phys::vk
{
    VulkanGraphicPipeline::VulkanGraphicPipeline()
        : pipelines()
    {
        setDefaultState();
    }

    VulkanGraphicPipeline::~VulkanGraphicPipeline()
    {
    }

    VkPipeline VulkanGraphicPipeline::getPipeline(VulkanRenderPass *_renderPass)
    {
        return pipelines[_renderPass->getIndex()].first;
    }

    bool VulkanGraphicPipeline::getEnableDynamicState(VkDynamicState _state) const
    {
        if ((int)_state >= (int)enabledDynamicStates.size())
        {
            return false;
        }

        return enabledDynamicStates[(int)_state];
    }

    void VulkanGraphicPipeline::setEnableDynamicState(VkDynamicState _state, bool _enable)
    {
        if ((int)_state >= (int)enabledDynamicStates.size())
            return;

        bool curValue = enabledDynamicStates[(int)_state];
        if (curValue && !_enable)
        {
            countEnableDynamicStates--;
        }
        else if (!curValue && _enable)
        {
            countEnableDynamicStates++;
        }

        enabledDynamicStates[(int)_state] = _enable;
    }

    VulkanPipelineLayout VulkanGraphicPipeline::getPipelineLayout() const
    {
        return VulkanPipelineLayout(createInfo.layout);
    }

    void VulkanGraphicPipeline::setPipelineLayout(VulkanPipelineLayout _layout)
    {
        createInfo.layout = _layout;
    }

    ShaderModule VulkanGraphicPipeline::getVertexModule() const
    {
        return vertexModule;
    }

    void VulkanGraphicPipeline::setVertexModule(ShaderModule _vertexModule)
    {
        vertexModule = _vertexModule;
        stagesModuls[0].module = vertexModule;
    }

    ShaderModule VulkanGraphicPipeline::getFragmentModule() const
    {
        return fragmentModule;
    }

    void VulkanGraphicPipeline::setFragmentModule(ShaderModule _fragmentModule)
    {
        fragmentModule = _fragmentModule;
        stagesModuls[1].module = fragmentModule;
    }

    void VulkanGraphicPipeline::setDefaultState()
    {
        createInfo.pNext = NULL;
        createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        createInfo.basePipelineHandle = VK_NULL_HANDLE;
        createInfo.basePipelineIndex = -1;
        createInfo.subpass = 0;
        createInfo.flags = 0;
        createInfo.layout = VK_NULL_HANDLE;

        tessellationState.pNext = NULL;
        tessellationState.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
        tessellationState.flags = 0;
        tessellationState.patchControlPoints = 0;

        multisamplingState = {};
        multisamplingState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisamplingState.sampleShadingEnable = VK_FALSE;
        multisamplingState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multisamplingState.minSampleShading = 1.0f;
        multisamplingState.pSampleMask = NULL;
        multisamplingState.alphaToCoverageEnable = VK_FALSE;
        multisamplingState.alphaToOneEnable = VK_FALSE;

        viewport.x = viewport.y = 0;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        viewport.width = viewport.height = 1;
        scissor.offset.x = scissor.offset.y = 0;
        scissor.extent.width = scissor.extent.height = 1;
        viewportState = {};
        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = 1;
        viewportState.scissorCount = 1;
        viewportState.pScissors = &scissor;
        viewportState.pViewports = &viewport;

        inputAssemblyState = {};
        inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssemblyState.primitiveRestartEnable = false;
        inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

        dynamicStates.pNext = NULL;
        dynamicStates.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamicStates.flags = 0;
        dynamicStates.dynamicStateCount = 0;
        dynamicStates.pDynamicStates = NULL;

        vertexModule = ShaderModule(VK_NULL_HANDLE, NULL, 0);
        stagesModuls[0].pNext = NULL;
        stagesModuls[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        stagesModuls[0].pName = "main";
        stagesModuls[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
        stagesModuls[0].pSpecializationInfo = NULL;
        stagesModuls[0].flags = 0;
        stagesModuls[0].module = VK_NULL_HANDLE;

        fragmentModule = ShaderModule(VK_NULL_HANDLE, NULL, 0);
        stagesModuls[1].pNext = NULL;
        stagesModuls[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        stagesModuls[1].pName = "main";
        stagesModuls[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        stagesModuls[1].pSpecializationInfo = NULL;
        stagesModuls[1].flags = 0;
        stagesModuls[1].module = VK_NULL_HANDLE;

        rasterizerInfoState = {};
        rasterizerInfoState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizerInfoState.depthClampEnable = VK_FALSE;
        rasterizerInfoState.rasterizerDiscardEnable = VK_FALSE;
        rasterizerInfoState.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizerInfoState.lineWidth = 1.0f;
        rasterizerInfoState.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizerInfoState.frontFace = VK_FRONT_FACE_CLOCKWISE;
        rasterizerInfoState.depthBiasEnable = VK_FALSE;
        rasterizerInfoState.depthBiasConstantFactor = 0.0f;
        rasterizerInfoState.depthBiasClamp = 0.0f;
        rasterizerInfoState.depthBiasSlopeFactor = 0.0f;

        depthStencilState = {};
        depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencilState.depthTestEnable = VK_TRUE;
        depthStencilState.depthWriteEnable = VK_TRUE;
        depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS;
        
        depthStencilState.minDepthBounds = 0.0f;
        depthStencilState.maxDepthBounds = 1.0f;
        depthStencilState.stencilTestEnable = VK_FALSE;
        depthStencilState.front = {};
        depthStencilState.back = {};

        createInfo.pTessellationState = &tessellationState;
        createInfo.pMultisampleState = &multisamplingState;
        createInfo.pViewportState = &viewportState;
        createInfo.pInputAssemblyState = &inputAssemblyState;
        createInfo.pRasterizationState = &rasterizerInfoState;
        createInfo.pDepthStencilState = &depthStencilState;
        createInfo.pVertexInputState = NULL;
        createInfo.pColorBlendState = NULL;
        vertexAttributeState = NULL;
        blendColorState = NULL;

        createInfo.pDynamicState = &dynamicStates;
        createInfo.stageCount = 2;
        createInfo.pStages = stagesModuls.data();

        countEnableDynamicStates = 0;
        for (int i = 0; i < (int)enabledDynamicStates.size(); i++)
            enabledDynamicStates[i] = false;
    }
}