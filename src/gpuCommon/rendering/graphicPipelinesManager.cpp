#include "gpuCommon/rendering/graphicPipelinesManager.h"

namespace phys::vk
{
    GraphicPipelinesManager::GraphicPipelinesManager(VulkanDevice *_device)
        : device(_device), cache(VK_NULL_HANDLE), freePipelines(NULL), freeNodes(NULL)
    {
        VkPipelineCacheCreateInfo info{};
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
        device->vkCreatePipelineCache(*device, &info, NULL, &cache);
    }

    GraphicPipelinesManager::~GraphicPipelinesManager()
    {
        clearCache();
        device->vkDestroyPipelineCache(*device, cache, NULL);
    }

    VulkanGraphicPipeline *GraphicPipelinesManager::createGraphicPipeline()
    {
        VulkanGraphicPipeline *graphicPipeline = getFreePipeline();
        return graphicPipeline;
    }

    void GraphicPipelinesManager::deleteGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline)
    {
        clearGraphicPipeline(_graphicPipeline);
    }

    void GraphicPipelinesManager::updateGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline, VulkanRenderPass _renderPass)
    {
        int index = _renderPass.getIndex();
        if (_graphicPipeline->pipelines[index].first == VK_NULL_HANDLE)
        {
            int countDynamicStates = _graphicPipeline->countEnableDynamicStates;
            VkDynamicState dynamicState[std::max(countDynamicStates, 1)];
            fillDynamicStates(_graphicPipeline, dynamicState);

            _graphicPipeline->dynamicStates.dynamicStateCount = countDynamicStates;
            _graphicPipeline->dynamicStates.pDynamicStates = dynamicState;
            _graphicPipeline->createInfo.renderPass = _renderPass;

            VkPipeline pipeline;
            device->vkCreateGraphicsPipelines(*device, cache, 1, &(_graphicPipeline->createInfo), NULL, &pipeline);
            _graphicPipeline->pipelines[index] = std::make_pair(pipeline, _renderPass);
        }
    }

    void GraphicPipelinesManager::updateGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline)
    {
        int countRenderPasses = (int)_graphicPipeline->pipelines.size();
        if (countRenderPasses == 0)
        {
            return;
        }
        VulkanRenderPass renderPass[countRenderPasses];
        int index = 0;
        auto end = _graphicPipeline->pipelines.end();
        for (auto i = _graphicPipeline->pipelines.begin(); i != end; ++i)
        {
            renderPass[index] = (*i).second.second;
            index++;
        }

        clearCacheGraphicPipeline(_graphicPipeline);

        int countDynamicStates = _graphicPipeline->countEnableDynamicStates;
        VkDynamicState dynamicState[std::max(countDynamicStates, 1)];
        fillDynamicStates(_graphicPipeline, dynamicState);

        _graphicPipeline->dynamicStates.dynamicStateCount = countDynamicStates;
        _graphicPipeline->dynamicStates.pDynamicStates = dynamicState;
        for (int i = 0; i < countRenderPasses; i++)
        {
            _graphicPipeline->createInfo.renderPass = renderPass[i];
            VkPipeline pipeline;
            device->vkCreateGraphicsPipelines(*device, cache, 1, &_graphicPipeline->createInfo, NULL, &pipeline);
            _graphicPipeline->pipelines[index] = std::make_pair(pipeline, renderPass[i]);
        }
    }

    void GraphicPipelinesManager::clearCacheGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline)
    {
        clearGraphicPipeline(_graphicPipeline);
    }

    void GraphicPipelinesManager::clearCache()
    {
        clearListNode(freePipelines);
        freePipelines = NULL;
        clearListNode(freeNodes);
        freeNodes = NULL;
    }

    void GraphicPipelinesManager::fillDynamicStates(VulkanGraphicPipeline *_graphicPipeline, VkDynamicState *_dynamicState)
    {
        int maxCountStates = (int)_graphicPipeline->enabledDynamicStates.size();
        int index = 0;
        for (int i = 0; i < maxCountStates; i++)
        {
            if (_graphicPipeline->enabledDynamicStates[i] != false)
            {
                _dynamicState[index] = (VkDynamicState)i;
                index++;
            }
        }
    }

    void GraphicPipelinesManager::clearGraphicPipeline(VulkanGraphicPipeline *_graphicPipeline)
    {
        auto end = _graphicPipeline->pipelines.end();
        for (auto i = _graphicPipeline->pipelines.begin(); i != end; ++i)
        {
            device->vkDestroyPipeline(*device, (*i).second.first, NULL);
        }
        _graphicPipeline->pipelines.clear();
    }

    VulkanGraphicPipeline *GraphicPipelinesManager::getFreePipeline()
    {
        VulkanGraphicPipeline *pipeline = NULL;
        if (freePipelines == NULL)
        {
            pipeline = new VulkanGraphicPipeline();
        }
        else
        {
            pipeline = freePipelines->graphicPipeline;
            pipeline->setDefaultState();
            GraphicPipelineNode *node = freePipelines;
            freePipelines = node->next;
            node->graphicPipeline = NULL;
            node->next = NULL;
            returnNode(node);
        }
        return pipeline;
    }

    void GraphicPipelinesManager::returnFreePipeline(VulkanGraphicPipeline *_pipeline)
    {
        GraphicPipelineNode *node = getFreeNode();
        node->graphicPipeline = _pipeline;
        node->next = freePipelines;
        freePipelines = node;
    }

    GraphicPipelinesManager::GraphicPipelineNode *GraphicPipelinesManager::getFreeNode()
    {
        GraphicPipelineNode *node = NULL;
        if (freeNodes == NULL)
        {
            node = new GraphicPipelineNode();
        }
        else
        {
            node = freeNodes;
            freeNodes = node->next;
        }
        node->next = NULL;
        node->graphicPipeline = NULL;
        return node;
    }

    void GraphicPipelinesManager::returnNode(GraphicPipelineNode *_node)
    {
        _node->next = freeNodes;
        freeNodes = _node;
    }

    void GraphicPipelinesManager::clearListNode(GraphicPipelineNode *_begin)
    {
        GraphicPipelineNode *next = _begin;
        while (next != NULL)
        {
            GraphicPipelineNode *current = next;
            next = next->next;
            if (current->graphicPipeline != NULL)
            {
                clearGraphicPipeline(current->graphicPipeline);
                delete current->graphicPipeline;
            }
            delete current;
        }
    }
}