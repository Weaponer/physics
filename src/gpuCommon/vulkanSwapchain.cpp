#include "gpuCommon/vulkanSwapchain.h"

namespace phys::vk
{
    VulkanSwapchain::VulkanSwapchain()
        : device(NULL), queue(NULL), surface(VK_NULL_HANDLE), width(0), height(0), swapchain(VK_NULL_HANDLE), images()
    {
    }

    VulkanSwapchain::~VulkanSwapchain()
    {
        destroySwapchain();
    }

#define CLAMP(x, lo, hi) ((x) < (lo) ? (lo) : (x) > (hi) ? (hi) \
                                                         : (x))

    bool VulkanSwapchain::createSwapchain(VulkanDevice *_device, VkSurfaceKHR _surface, int _width, int _height)
    {
        if (swapchain != VK_NULL_HANDLE)
        {
            return false;
        }
        device = _device;
        surface = _surface;

        VulkanInstance *instance = _device->getInstance();
        VkPhysicalDevice physDevice = _device->getPhysicalDevice();
        VkSurfaceCapabilitiesKHR capabilities;
        instance->vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physDevice, surface, &capabilities);

        uint formatCount = 0;
        instance->vkGetPhysicalDeviceSurfaceFormatsKHR(physDevice, surface, &formatCount, NULL);
        std::vector<VkSurfaceFormatKHR> formats;
        if (formatCount != 0)
        {
            formats.resize(formatCount);
            instance->vkGetPhysicalDeviceSurfaceFormatsKHR(physDevice, surface, &formatCount, formats.data());
        }
        VkSurfaceFormatKHR format = formats[0];
        for (uint i = 0; i < formatCount; i++)
        {
            if (formats[i].format == VK_FORMAT_B8G8R8A8_SRGB)
            {
                format = formats[i];
                break;
            }
        }

        width = (int)CLAMP((uint)_width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
        height = (int)CLAMP((uint)_height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

        uint32_t imageCount = capabilities.minImageCount + 1;
        if (capabilities.maxImageCount > 0 && imageCount > capabilities.maxImageCount)
        {
            imageCount = capabilities.maxImageCount;
        }

        VkSwapchainCreateInfoKHR createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.oldSwapchain = VK_NULL_HANDLE;
        createInfo.surface = surface;
        createInfo.minImageCount = capabilities.minImageCount;
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
        createInfo.imageFormat = format.format;
        createInfo.imageColorSpace = format.colorSpace;
        createInfo.imageExtent.width = width;
        createInfo.imageExtent.height = height;
        createInfo.imageArrayLayers = 1;
        createInfo.surface = _surface;
        createInfo.queueFamilyIndexCount = device->getCountQueueFamilies();
        createInfo.pQueueFamilyIndices = device->getQueueFamiliesIndexes();
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.preTransform = capabilities.currentTransform;
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        createInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
        createInfo.clipped = VK_TRUE;

        device->vkCreateSwapchainKHR(*device, &createInfo, NULL, &swapchain);
        uint32_t countSwapchainImages = 0;
        device->vkGetSwapchainImagesKHR(*device, swapchain, &countSwapchainImages, NULL);
        VkImage swapchainImages[countSwapchainImages];
        device->vkGetSwapchainImagesKHR(*device, swapchain, &countSwapchainImages, swapchainImages);

        for (uint32_t i = 0; i < countSwapchainImages; i++)
        {
            VulkanImage image = VulkanImage(swapchainImages[i], NULL, -1, createInfo.imageUsage, false,
                                            VK_IMAGE_TYPE_2D, createInfo.imageFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_ASPECT_COLOR_BIT,
                                            width, height, 1, 1, 1);
            device->setNameObject("SwapchainColorImage", (uint64_t)swapchainImages[i], VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT);
            VulkanImageView view;
            VulkanImageView::createImageView(device, VK_IMAGE_VIEW_TYPE_2D, &image, &view, "SwapchainColorImageView");
            images.push_back(image);
            imageViews.push_back(view);
        }

        return true;
    }

    void VulkanSwapchain::destroySwapchain()
    {
        if (swapchain == VK_NULL_HANDLE)
            return;
        
        int count = (int)images.size();
        for(int i = 0; i < count; i++)
        {
            vk::VulkanImageView::destroyImageView(device, imageViews[i]);
        }
        imageViews.clear();
        images.clear();


        device->vkDestroySwapchainKHR(*device, swapchain, NULL);
        swapchain = VK_NULL_HANDLE;
    }
}