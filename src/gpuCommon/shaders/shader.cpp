#include <algorithm>
#include <set>
#include <cstring>
#include "gpuCommon/shaders/shader.h"

namespace phys::vk
{
    ShaderData::ShaderData(const std::string &_nameShader, ShaderType _type, const std::vector<unsigned int> &_spirvCode,
                           const std::vector<UniformBlock> &_uniformBlocks, const std::vector<UniformSampler> &_uniformSamplers)
        : device(NULL), nameShader(_nameShader), type(_type), spirvCode(_spirvCode),
          uniformBlocks(_uniformBlocks), uniformSamplers(_uniformSamplers),
          pushConstantData({}),
          countUseSets(0)
    {
    }

    ShaderData::~ShaderData()
    {
        if (device == NULL)
            return;

        for (int i = 0; i < countUseSets; i++)
        {
            device->vkDestroyDescriptorSetLayout(*device, shaderSetDatas[i].setLayout, NULL);

            ShaderSetData data = getSetData(i);
            if (data.indexBlocksStorageBuffers)
                delete[] data.indexBlocksStorageBuffers;
            if (data.indexBlocksUniformBuffers)
                delete[] data.indexBlocksUniformBuffers;
            if (data.indexMainSamplers)
                delete[] data.indexMainSamplers;
            if (data.indexMainStorageImages)
                delete[] data.indexMainStorageImages;
        }

        device->vkDestroyPipelineLayout(*device, vkLayout, NULL);
    }

    static uint32_t getSizeData(ShaderData::DataType _type)
    {
        switch (_type)
        {
        case ShaderData::DataType_Float:
            return sizeof(float);
        case ShaderData::DataType_Double:
            return sizeof(double);
        case ShaderData::DataType_Float16:
            return sizeof(float) / 2;
        case ShaderData::DataType_Int8:
            return sizeof(int8_t);
        case ShaderData::DataType_Uint8:
            return sizeof(uint8_t);
        case ShaderData::DataType_Int16:
            return sizeof(int16_t);
        case ShaderData::DataType_Uint16:
            return sizeof(uint16_t);
        case ShaderData::DataType_Int:
            return sizeof(int32_t);
        case ShaderData::DataType_Uint:
            return sizeof(uint32_t);
        case ShaderData::DataType_Int64:
            return sizeof(int64_t);
        case ShaderData::DataType_Uint64:
            return sizeof(uint64_t);
        case ShaderData::DataType_Bool:
            return sizeof(bool);
        default:
            return 1;
        }
    }

    void ShaderData::prepareData(VulkanDevice *_device)
    {
        if (device != NULL)
            return;

        device = _device;

        pushConstantData = {};

        std::set<int> foundSets;

        int countUniformBlocks = getCountUniformBlocks();
        for (int i = 0; i < countUniformBlocks; i++)
        {
            if (!getUniformBlock(i).isValid())
            {
                continue;
            }

            if (getUniformBlock(i).isPushConstant)
            {
                uint32_t min = UINT32_MAX;
                uint32_t max = 0;
                for (auto v : getUniformBlock(i).variables)
                {
                    min = std::min(min, (uint32_t)v.offset);
                    max = std::max(max, (uint32_t)(v.offset + v.size * v.vectorSize * getSizeData(v.type)));
                }
                if (max == 0)
                    continue;

                pushConstantData.offset = min;
                pushConstantData.size = max - min;
                int additionalSize = pushConstantData.size % 4;
                if (additionalSize != 0)
                {
                    pushConstantData.size += 4 - additionalSize;
                }
            }
            else
            {
                int set = getUniformBlock(i).set;
                foundSets.insert(set);
            }
        }

        int countUniformSamplers = getCountUniformSamplers();
        for (int i = 0; i < countUniformSamplers; i++)
        {
            if (!getUniformSampler(i).isValid())
            {
                continue;
            }
            int set = getUniformSampler(i).set;
            foundSets.insert(set);
        }

        countUseSets = foundSets.size();

        numbersSetsToIndexesSets = std::vector<int>(foundSets.begin(), foundSets.end());
        for (int i = 0; i < countUseSets; i++)
            indexesSetsToNumbersSets[numbersSetsToIndexesSets[i]] = i;

        uniformBlocksInSet.resize(countUseSets, std::vector<int>());
        uniformSamplersInSet.resize(countUseSets, std::vector<int>());

        for (int i = 0; i < countUniformBlocks; i++)
        {
            const UniformBlock &block = getUniformBlock(i);
            if (!block.isValid() || block.isPushConstant)
            {
                continue;
            }
            uniformBlocksInSet[indexesSetsToNumbersSets[block.set]].push_back(i);
        }

        for (int i = 0; i < countUniformSamplers; i++)
        {
            const UniformSampler &sampler = getUniformSampler(i);
            if (!sampler.isValid())
            {
                continue;
            }
            uniformSamplersInSet[indexesSetsToNumbersSets[sampler.set]].push_back(i);
        }

        shaderSetDatas.resize(countUseSets, ShaderSetData());

        for (int i = 0; i < countUseSets; i++)
        {
            ShaderSetData data{};
            std::vector<int> indexBlocksUniformBuffers;
            std::vector<int> indexBlocksStorageBuffers;
            std::vector<int> indexMainSamplers;
            std::vector<int> indexMainStorageImages;
            int countUniformBlocks = getCountUniformBlocksInSet(i);

            for (int i2 = 0; i2 < countUniformBlocks; i2++)
            {
                const UniformBlock &block = getUniformBlockInSet(i, i2);
                if (block.blockType == ShaderData::BlockStorage_UniformBuffer)
                {
                    data.countUniformBuffers++;
                    indexBlocksUniformBuffers.push_back(i2);
                }
                else if (block.blockType == ShaderData::BlockStorage_StorageBuffer)
                {
                    data.countStorageBuffers++;
                    indexBlocksStorageBuffers.push_back(i2);
                }
            }

            int countUniformSamplers = getCountUniformSamplersInSet(i);
            for (int i2 = 0; i2 < countUniformSamplers; i2++)
            {
                const UniformSampler &sampler = getUniformSamplerInSet(i, i2);
                if (sampler.isImage)
                {
                    data.countMainStorageImages++;
                    data.countAllStorageImages += sampler.implicitArraySize;
                    indexMainStorageImages.push_back(i2);
                }
                else
                {
                    data.countMainSamplers++;
                    data.countAllSamplers += sampler.implicitArraySize;
                    indexMainSamplers.push_back(i2);
                }
            }

            data.indexMainStorageImages = NULL;
            data.indexMainSamplers = NULL;
            data.indexBlocksStorageBuffers = NULL;
            data.indexBlocksUniformBuffers = NULL;

            if (indexBlocksUniformBuffers.size() != 0)
            {
                int count = (int)indexBlocksUniformBuffers.size();
                data.indexBlocksUniformBuffers = new int[count];
                std::memcpy(data.indexBlocksUniformBuffers, indexBlocksUniformBuffers.data(), sizeof(int) * count);
            }

            if (indexBlocksStorageBuffers.size() != 0)
            {
                int count = (int)indexBlocksStorageBuffers.size();
                data.indexBlocksStorageBuffers = new int[count];
                std::memcpy(data.indexBlocksStorageBuffers, indexBlocksStorageBuffers.data(), sizeof(int) * count);
            }

            if (indexMainSamplers.size() != 0)
            {
                int count = (int)indexMainSamplers.size();
                data.indexMainSamplers = new int[count];
                std::memcpy(data.indexMainSamplers, indexMainSamplers.data(), sizeof(int) * count);
            }

            if (indexMainStorageImages.size() != 0)
            {
                int count = (int)indexMainStorageImages.size();
                data.indexMainStorageImages = new int[count];
                std::memcpy(data.indexMainStorageImages, indexMainStorageImages.data(), sizeof(int) * count);
            }

            shaderSetDatas[i] = data;
        }

        prepareSetLayouts(_device);
        preparePipelineLayout(_device);
    }

    VkShaderStageFlags ShaderData::getStageFlag()
    {
        switch (type)
        {
        case ShaderType::ShaderType_Compute:
            return VK_SHADER_STAGE_COMPUTE_BIT;
        case ShaderType::ShaderType_Vertex:
            return VK_SHADER_STAGE_VERTEX_BIT;
        case ShaderType::ShaderType_Fragment:
            return VK_SHADER_STAGE_FRAGMENT_BIT;
        default:
            return VK_SHADER_STAGE_ALL;
        }
    }

    void ShaderData::prepareSetLayouts(VulkanDevice *_device)
    {
        for (int i = 0; i < countUseSets; i++)
        {
            VkDescriptorSetLayoutCreateInfo createInfo{};
            createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            createInfo.flags = 0;

            std::vector<int> &uniformBlocksDescriptions = uniformBlocksInSet[i];
            std::vector<int> &uniformSamplersDescriptions = uniformSamplersInSet[i];

            ShaderSetData data = shaderSetDatas[i];

            int countBindings = data.countUniformBuffers + data.countStorageBuffers +
                                data.countMainSamplers + data.countMainStorageImages;
            VkDescriptorSetLayoutBinding bindings[countBindings];
            int indexBinding = 0;

            for (int i2 = 0; i2 < (int)uniformBlocksDescriptions.size(); i2++)
            {
                UniformBlock &block = uniformBlocks[uniformBlocksDescriptions[i2]];

                VkDescriptorSetLayoutBinding binding{};
                if (block.blockType == ShaderData::BlockStorage_UniformBuffer)
                {
                    binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                }
                else if (block.blockType == ShaderData::BlockStorage_StorageBuffer)
                {
                    binding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
                }
                else
                {
                    continue;
                }

                binding.binding = block.binding;
                binding.descriptorCount = 1;
                binding.stageFlags = getStageFlag();
                bindings[indexBinding] = binding;
                indexBinding++;
            }

            for (int i2 = 0; i2 < (int)uniformSamplersDescriptions.size(); i2++)
            {
                UniformSampler &sampler = uniformSamplers[uniformSamplersDescriptions[i2]];
                VkDescriptorSetLayoutBinding binding{};
                binding.binding = sampler.binding;
                binding.descriptorCount = sampler.implicitArraySize;
                binding.stageFlags = getStageFlag();
                if (sampler.isImage)
                {
                    binding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
                }
                else
                {
                    binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                }
                bindings[indexBinding] = binding;
                indexBinding++;
            }

            createInfo.bindingCount = countBindings;
            createInfo.pBindings = bindings;

            VkDescriptorSetLayout setLayout = VK_NULL_HANDLE;
            _device->vkCreateDescriptorSetLayout(*device, &createInfo, NULL, &setLayout);
            shaderSetDatas[i].setLayout = setLayout;
        }
    }

    void ShaderData::preparePipelineLayout(VulkanDevice *_device)
    {
        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = countUseSets;
        VkDescriptorSetLayout setLayouts[countUseSets];
        for (int i = 0; i < countUseSets; i++)
        {
            setLayouts[i] = shaderSetDatas[i].setLayout;
        }
        pipelineLayoutInfo.pSetLayouts = setLayouts;

        VkPushConstantRange pushConstantRanges{};
        pushConstantRanges.stageFlags = getStageFlag();
        pushConstantRanges.offset = pushConstantData.offset;
        pushConstantRanges.size = pushConstantData.size;
        if (isUsePushConstant())
        {
            pipelineLayoutInfo.pushConstantRangeCount = 1;
            pipelineLayoutInfo.pPushConstantRanges = &pushConstantRanges;
        }

        _device->vkCreatePipelineLayout(*_device, &pipelineLayoutInfo, NULL, &vkLayout);
    }
}