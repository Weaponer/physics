#include <cstring>

#include "gpuCommon/shaders/shaderManager.h"

namespace phys::vk
{
    ShaderManager::ShaderManager(VulkanDevice *_device)
        : device(_device)
    {
    }

    ShaderManager::~ShaderManager()
    {
        for (auto shader : shaders)
        {
            device->vkDestroyShaderModule(*device, shader, NULL);
        }
        shaders.clear();
    }

    bool ShaderManager::registerShader(ShaderData *_shader, ShaderModule *_shaderModule)
    {
        if (!getShaderByName(_shader->getName(), NULL))
        {
            VkShaderModuleCreateInfo createInfo{};
            createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            const std::vector<unsigned int> &spirvCode = _shader->getSpirvCode();
            createInfo.codeSize = spirvCode.size() * sizeof(uint32_t);
            createInfo.pCode = &(spirvCode[0]);
            VkShaderModule module = VK_NULL_HANDLE;
            if (device->vkCreateShaderModule(*device, &createInfo, NULL, &module) != VK_SUCCESS)
                return false;

            device->setNameObject(_shader->getName(), (uint64_t)module, VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT);

            ShaderModule shaderModule = ShaderModule(module, _shader, shaders.size());
            if (_shaderModule != NULL)
                *_shaderModule = shaderModule;

            shaders.push_back(shaderModule);
            return true;
        }
        return false;
    }

    bool ShaderManager::getShaderByName(const char *_name, ShaderModule *_shaderModule)
    {
        for (auto shader : shaders)
        {
            if (std::strcmp(shader.getData()->getName(), _name) == 0)
            {
                if (_shaderModule != NULL)
                    *_shaderModule = shader;

                return true;
            }
        }
        return false;
    }
}