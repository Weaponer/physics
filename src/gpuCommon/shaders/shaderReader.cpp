#include <cstring>
#include "gpuCommon/shaders/shaderReader.h"

namespace phys::vk
{
    int ShaderReader::getCountShadersInPacket(const char *_data) const
    {
        SeekerDescriptor seeker;
        seeker.data = _data;
        seeker.position = 0;
        return readInt(seeker);
    }

    void ShaderReader::getShadersFromPacket(int _count, ShaderData **_shaders, const char *_data) const
    {
        SeekerDescriptor seeker;
        seeker.data = _data;
        seeker.position = 0;
        int countShaders = readInt(seeker);
        int countReadShaders = std::min(_count, countShaders);

        for (int i = 0; i < countReadShaders; i++)
        {
            std::string nameShader = readString(seeker);

            ShaderData::ShaderType type = (ShaderData::ShaderType)readInt(seeker);
            std::vector<unsigned int> spirvCode = std::vector<unsigned int>();
            readBufferUInt(seeker, spirvCode);

            std::vector<ShaderData::UniformSampler> uniformSamplers;
            int countSamplers = readInt(seeker);
            for (int i2 = 0; i2 < countSamplers; i2++)
            {
                ShaderData::UniformSampler uniformSampler{};
                uniformSampler.name = readString(seeker);
                uniformSampler.set = readInt(seeker);
                uniformSampler.binding = readInt(seeker);
                uniformSampler.size = readInt(seeker);
                uniformSampler.vectorSize = readInt(seeker);
                uniformSampler.implicitArraySize = readInt(seeker);
                uniformSampler.dataType = (ShaderData::DataType)readInt(seeker);
                uniformSampler.samplerFormat = (ShaderData::SamplerFormat)readInt(seeker);
                uniformSampler.isImage = readBool(seeker);
                uniformSampler.isShadow = readBool(seeker);
                uniformSampler.isArray = readBool(seeker);
                uniformSamplers.push_back(uniformSampler);
            }

            std::vector<ShaderData::UniformBlock> uniformBlocks;
            int countBlocks = readInt(seeker);
            for (int i2 = 0; i2 < countBlocks; i2++)
            {
                ShaderData::UniformBlock block{};
                block.name = readString(seeker);
                block.set = readInt(seeker);
                block.binding = readInt(seeker);
                block.size = readInt(seeker);
                block.blockType = (ShaderData::BlockStorageType)readInt(seeker);
                block.isPushConstant = readBool(seeker);
                int countVariables = readInt(seeker);
                for (int i3 = 0; i3 < countVariables; i3++)
                {
                    ShaderData::UniformVariable variable{};
                    variable.name = readString(seeker);
                    variable.offset = readInt(seeker);
                    variable.size = readInt(seeker);
                    variable.vectorSize = readInt(seeker);
                    variable.implicitArraySize = readInt(seeker);
                    variable.type = (ShaderData::DataType)readInt(seeker);
                    block.variables.push_back(variable);
                }
                uniformBlocks.push_back(block);
            }

            ShaderData *shader = new ShaderData(nameShader, type, spirvCode, uniformBlocks, uniformSamplers);
            _shaders[i] = shader;
        }
    }

    int ShaderReader::readInt(SeekerDescriptor &_seeker) const
    {
        int value = 0;
        std::memcpy(&value, &(_seeker.data[_seeker.position]), sizeof(int));
        _seeker.position += sizeof(int);
        return value;
    }

    float ShaderReader::readFloat(SeekerDescriptor &_seeker) const
    {
        float value = 0;
        std::memcpy(&value, &(_seeker.data[_seeker.position]), sizeof(float));
        _seeker.position += sizeof(float);
        return value;
    }

    std::string ShaderReader::readString(SeekerDescriptor &_seeker) const
    {
        int size = readInt(_seeker);
        if (size == 0)
            return "";

        char buffer[size + 1];
        buffer[size] = '\0';
        std::memcpy(buffer, &(_seeker.data[_seeker.position]), size);
        _seeker.position += size;
        return std::string(buffer);
    }

    bool ShaderReader::readBool(SeekerDescriptor &_seeker) const
    {
        bool value = 0;
        std::memcpy(&value, &(_seeker.data[_seeker.position]), sizeof(bool));
        _seeker.position += sizeof(bool);
        return value;
    }

    bool ShaderReader::readBufferUInt(SeekerDescriptor &_seeker, std::vector<unsigned int> &_buf) const
    {
        int size = readInt(_seeker);
        if (size == 0)
            return false;

        _buf.clear();
        _buf.resize(size, 0);
        std::memcpy(_buf.data(), &(_seeker.data[_seeker.position]), sizeof(unsigned int) * size);
        _seeker.position += sizeof(unsigned int) * size;
        return true;
    }
}