#include "gpuCommon/shaders/computePipeline.h"

namespace phys::vk
{
    bool ComputePipeline::createComputePipeline(VulkanDevice *_device, ShaderModule _module, ComputePipeline *_pipeline)
    {
        VkComputePipelineCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
        createInfo.stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        createInfo.stage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
        createInfo.stage.module = _module;
        createInfo.stage.pName = "main";
        createInfo.layout = _module.getData()->getPipelineLayout();
        VkPipeline pipeline;
        if (_device->vkCreateComputePipelines(*_device, NULL, 1, &createInfo, NULL, &pipeline) == VK_SUCCESS)
        {
            *_pipeline = ComputePipeline(pipeline);
            return true;
        }
        else
        {
            return false;
        }
    }

    void ComputePipeline::destroyComputePipeline(VulkanDevice *_device, ComputePipeline *_pipeline)
    {
        _device->vkDestroyPipeline(*_device, *_pipeline, NULL);
    }
}