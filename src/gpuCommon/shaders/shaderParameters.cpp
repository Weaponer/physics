#include <algorithm>
#include <cstring>

#include "gpuCommon/shaders/shaderParameters.h"

namespace phys::vk
{
    ShaderParameters::ShaderParameters()
    {
        bufferingDescriptorSets = 1;
        countSets = 0;
    }

    ShaderParameters::ShaderParameters(VulkanDevice *_device, ShaderModule _shader)
        : device(_device), shader(_shader)
    {
        bufferingDescriptorSets = 1;
        countSets = _shader.getData()->getCountSets();
        sets.resize(countSets, Set());

        for (int i = 0; i < countSets; i++)
        {
            Set &set = sets[i];
            set.beginPage = NULL;
            set.currentPage = NULL;
            set.endPage = NULL;
            set.countFreeSets = 0;
            set.currentSet = VK_NULL_HANDLE;
            set.freeSets.resize(bufferingDescriptorSets, VK_NULL_HANDLE);
            set.setLayout = _shader.getData()->getSetData(i).setLayout;
            set.setBinding = _shader.getData()->getSetIndexBinding(i);
        }
    }

    ShaderParameters::~ShaderParameters()
    {
    }

    void ShaderParameters::takeMemoryPools(VulkanDescriptorPoolManager *_poolManager, int _countSets)
    {
        int maxSetsPerPool = _poolManager->getMaxDescriptorSetsPerPool();
        int countPools = _countSets / maxSetsPerPool + std::min(1, _countSets % maxSetsPerPool);
        for (int i = 0; i < (int)sets.size(); i++)
        {
            Set &set = sets[i];
            VulkanDescriptorPoolPage *pages = _poolManager->getDescriptorPools(shader, i, countPools);
            if (set.beginPage == NULL)
            {
                set.beginPage = pages;
                set.currentPage = set.beginPage;
                VulkanDescriptorPoolPage *next = set.beginPage;
                while (next != NULL)
                {
                    set.endPage = next;
                    next = next->next;
                }

                allocateDescriptorSets(i);
            }
            else
            {
                set.endPage->next = pages;
                VulkanDescriptorPoolPage *next = pages;
                while (next != NULL)
                {
                    set.endPage = next;
                    next = next->next;
                }
            }
        }
    }

    void ShaderParameters::freeMemoryPools(VulkanDescriptorPoolManager *_poolManager)
    {
        for (int i = 0; i < (int)sets.size(); i++)
        {
            Set &set = sets[i];
            set.endPage = NULL;
            set.currentPage = NULL;
            set.currentSet = VK_NULL_HANDLE;
            set.countFreeSets = 0;
            _poolManager->retrunDescriptorPools(set.beginPage);
            set.beginPage = NULL;
        }
    }

    void ShaderParameters::switchToNextSet(int _indexSet)
    {
        Set &set = sets[_indexSet];
        VkDescriptorSet previousSet = set.currentSet;
        if (set.countFreeSets != 0)
        {
            set.countFreeSets -= 1;
            set.currentSet = set.freeSets[set.countFreeSets];
            onSwitchSet(_indexSet, previousSet);
        }
        else
        {
            if (allocateDescriptorSets(_indexSet))
                onSwitchSet(_indexSet, previousSet);
        }
    }

    void ShaderParameters::switchToNextSets()
    {
        for (int i = 0; i < countSets; i++)
        {
            switchToNextSet(i);
        }
    }

    void ShaderParameters::reset()
    {
        for (int i = 0; i < countSets; i++)
        {
            Set &set = sets[i];
            VulkanDescriptorPoolPage *next = set.beginPage;
            while (next != NULL)
            {
                VulkanDescriptorPoolPage *current = next;
                device->vkResetDescriptorPool(*device, current->pool, 0);
                current->countFreeSets = current->countSets;
                next = next->next;
            }

            set.countFreeSets = 0;
            set.currentSet = VK_NULL_HANDLE;
            set.currentPage = set.beginPage;
            allocateDescriptorSets(i);
        }

        onReset();
    }

    VkDescriptorSet ShaderParameters::getCurrentSet(int _indexSet)
    {
        return sets[_indexSet].currentSet;
    }

    int ShaderParameters::getBindingSet(int _indexSet)
    {
        return sets[_indexSet].setBinding;
    }

    void ShaderParameters::onSwitchSet(int _indexSet, VkDescriptorSet _previousSet)
    {
    }

    void ShaderParameters::onReset()
    {
    }

    bool ShaderParameters::allocateDescriptorSets(int _indexSet)
    {
        Set &set = sets[_indexSet];
        if (set.currentPage == NULL)
        {
            return false;
        }
        else if (set.currentPage->countFreeSets == 0)
        {
            if (set.currentPage->next == NULL)
            {
                return false;
            }

            set.currentPage = set.currentPage->next;
        }

        if (set.freeSets.size() != bufferingDescriptorSets)
            set.freeSets.resize(bufferingDescriptorSets, VK_NULL_HANDLE);
        set.currentSet = VK_NULL_HANDLE;

        int countAllocate = std::min(bufferingDescriptorSets, (uint)set.currentPage->countFreeSets);
        VkDescriptorSetAllocateInfo allocateInfo{};
        allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocateInfo.pNext = NULL;
        allocateInfo.descriptorPool = set.currentPage->pool;
        allocateInfo.descriptorSetCount = countAllocate;

        VkDescriptorSetLayout layouts[countAllocate];
        std::fill_n(layouts, countAllocate, set.setLayout);
        allocateInfo.pSetLayouts = layouts;

        device->vkAllocateDescriptorSets(*device, &allocateInfo, set.freeSets.data());
        set.currentPage->countFreeSets -= countAllocate;

        set.countFreeSets = countAllocate - 1;
        set.currentSet = set.freeSets[set.countFreeSets];
        return true;
    }
}