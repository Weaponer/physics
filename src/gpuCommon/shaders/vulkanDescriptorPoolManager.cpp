#include "gpuCommon/shaders/vulkanDescriptorPoolManager.h"

namespace phys::vk
{
    VulkanDescriptorPoolManager::VulkanDescriptorPoolManager(VulkanDevice *_device)
        : device(_device)
    {
        config.maxDescriptorsPerPool = 16;
        config.stepCountDescriptors = 8;
    }

    VulkanDescriptorPoolManager::VulkanDescriptorPoolManager(VulkanDevice *_device, DescriptorPoolManagerConfig _config)
        : device(_device), config(_config)
    {
    }

    VulkanDescriptorPoolManager::~VulkanDescriptorPoolManager()
    {
        clear();
    }

    void VulkanDescriptorPoolManager::preparePools(ShaderManager *_shaderManager)
    {
        clear();

        int countTypeSets = 0;
        int countShaders = _shaderManager->getCountShaders();

        for (int i = 0; i < countShaders; i++)
        {
            ShaderModule shader = _shaderManager->getShaderByIndex(i);
            int countSets = shader.getData()->getCountSets();
            if ((int)poolSchemeByShaderIndex.size() <= shader.getIndex())
            {
                poolSchemeByShaderIndex.resize((shader.getIndex() + 1) * 2, std::make_pair(-1, -1));
            }
            poolSchemeByShaderIndex[shader.getIndex()] = std::make_pair(countTypeSets, countSets);

            countTypeSets += countSets;
        }

        poolSchemesByShaderSetIndex.resize(countTypeSets, -1);
        std::map<uint64_t, int> hashShaderSetToPoolScheme = std::map<uint64_t, int>();

        for (int i = 0; i < countShaders; i++)
        {
            ShaderModule shader = _shaderManager->getShaderByIndex(i);
            int countSets = shader.getData()->getCountSets();
            for (int i2 = 0; i2 < countSets; i2++)
            {
                const ShaderData::ShaderSetData &_set = shader.getData()->getSetData(i2);
                uint64_t key = getHashKeyPoolSetsScheme(_set);
                if (hashShaderSetToPoolScheme.find(key) == hashShaderSetToPoolScheme.end())
                {
                    VulkanDescriptorPoolScheme *poolScheme = new VulkanDescriptorPoolScheme();
                    poolScheme->poolSizes = std::vector<VkDescriptorPoolSize>();
                    fillPoolSchemeByShaderSetData(_set, poolScheme);

                    int index = poolSchemes.size();
                    poolSchemes.push_back(poolScheme);
                    poolPages.push_back(NULL);
                    hashShaderSetToPoolScheme[key] = index;
                }

                poolSchemesByShaderSetIndex[poolSchemeByShaderIndex[i].first + i2] = hashShaderSetToPoolScheme[key];
            }
        }
    }

#define ALIGNMENT_COUNT_DESCRIPTORS(c) (uint64_t) std::min((u_char)(c / config.stepCountDescriptors + std::min(1, c % config.stepCountDescriptors)), maxDescriptorsPerType)

    uint64_t VulkanDescriptorPoolManager::getHashKeyPoolSetsScheme(const ShaderData::ShaderSetData &_setData)
    {
        const u_char maxDescriptorsPerType = 255;

        uint64_t key = (ALIGNMENT_COUNT_DESCRIPTORS(_setData.countAllSamplers)) |
                       (ALIGNMENT_COUNT_DESCRIPTORS(_setData.countAllStorageImages) << 8) |
                       (ALIGNMENT_COUNT_DESCRIPTORS(_setData.countUniformBuffers) << 16) |
                       (ALIGNMENT_COUNT_DESCRIPTORS(_setData.countStorageBuffers) << 24);

        return key;
    }

#define PUSH_DESCRIPTOR_SIZE(count, descType)                                                                                           \
    if (count != 0)                                                                                                                     \
    {                                                                                                                                   \
        int c = (count / config.stepCountDescriptors + std::min(1, count % config.stepCountDescriptors)) * config.stepCountDescriptors; \
        VkDescriptorPoolSize poolSize;                                                                                                  \
        poolSize.descriptorCount = c * config.maxDescriptorsPerPool;                                                                                                   \
        poolSize.type = descType;                                                                                                       \
        _poolScheme->poolSizes.push_back(poolSize);                                                                                     \
    }

    void VulkanDescriptorPoolManager::fillPoolSchemeByShaderSetData(const ShaderData::ShaderSetData &_setData, VulkanDescriptorPoolScheme *_poolScheme)
    {
        _poolScheme->poolSizes.clear();
        PUSH_DESCRIPTOR_SIZE(_setData.countUniformBuffers, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
        PUSH_DESCRIPTOR_SIZE(_setData.countStorageBuffers, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER)
        PUSH_DESCRIPTOR_SIZE(_setData.countAllSamplers, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER)
        PUSH_DESCRIPTOR_SIZE(_setData.countAllStorageImages, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE)
    }

    VulkanDescriptorPoolPage *VulkanDescriptorPoolManager::getDescrpitorPoolPage(int _indexScheme)
    {
        VulkanDescriptorPoolPage *beginPage = poolPages[_indexScheme];
        if (beginPage == NULL)
        {
            VulkanDescriptorPoolScheme *scheme = poolSchemes[_indexScheme];
            VkDescriptorPoolCreateInfo createInfo{};
            createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
            createInfo.maxSets = config.maxDescriptorsPerPool;
            createInfo.poolSizeCount = scheme->poolSizes.size();
            createInfo.pPoolSizes = scheme->poolSizes.data();
            
            VkDescriptorPool vkPool{};
            device->vkCreateDescriptorPool(*device, &createInfo, NULL, &vkPool);

            VulkanDescriptorPoolPage *page = new VulkanDescriptorPoolPage();
            page->countFreeSets = config.maxDescriptorsPerPool;
            page->countSets = config.maxDescriptorsPerPool;
            page->pool = vkPool;
            page->poolShceme = scheme;
            page->indexScheme = _indexScheme;
            page->next = NULL;
            return page;
        }
        else
        {
            poolPages[_indexScheme] = beginPage->next;
            beginPage->next = NULL;
            return beginPage;
        }
    }

    void VulkanDescriptorPoolManager::returnDescrpitorPoolPage(VulkanDescriptorPoolPage *_page)
    {
        int indexScheme = _page->indexScheme;
        VulkanDescriptorPoolPage *beginPage = poolPages[indexScheme];
        _page->next = beginPage;
        if (_page->countFreeSets != config.maxDescriptorsPerPool)
        {
            device->vkResetDescriptorPool(*device, _page->pool, 0);
            _page->countFreeSets = config.maxDescriptorsPerPool;
        }
        poolPages[indexScheme] = _page;
    }

    VulkanDescriptorPoolPage *VulkanDescriptorPoolManager::getDescriptorPools(ShaderModule _shader, int _indexSet, int _count)
    {
        int index = _shader.getIndex();
        int indexScheme = poolSchemesByShaderSetIndex[poolSchemeByShaderIndex[index].first + _indexSet];

        VulkanDescriptorPoolPage *pools = NULL;
        for (int i = 0; i < _count; i++)
        {
            VulkanDescriptorPoolPage *pool = getDescrpitorPoolPage(indexScheme);
            pool->next = pools;
            pools = pool;
        }
        return pools;
    }

    void VulkanDescriptorPoolManager::retrunDescriptorPools(VulkanDescriptorPoolPage *_descriptorPools)
    {
        VulkanDescriptorPoolPage *next = _descriptorPools;
        while (next != NULL)
        {
            VulkanDescriptorPoolPage *current = next;
            next = next->next;
            returnDescrpitorPoolPage(current);
        }
    }

    void VulkanDescriptorPoolManager::clear()
    {
        poolSchemesByShaderSetIndex.clear();
        poolSchemeByShaderIndex.clear();
        for (auto i : poolSchemes)
        {
            delete i;
        }
        poolSchemes.clear();

        for (auto i : poolPages)
        {
            VulkanDescriptorPoolPage *next = i;
            while (next != NULL)
            {
                VulkanDescriptorPoolPage *current = next;
                next = next->next;
                device->vkDestroyDescriptorPool(*device, current->pool, NULL);
                delete current;
            }
        }

        poolPages.clear();
    }
}