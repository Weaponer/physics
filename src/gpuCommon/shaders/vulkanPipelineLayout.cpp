#include <iostream>
#include "gpuCommon/shaders/vulkanPipelineLayout.h"

namespace phys::vk
{
    bool VulkanPipelineLayout::createPipelineLayout(VulkanDevice *_device, int _countShaders, ShaderModule *_modules, VulkanPipelineLayout *_outPipelineLayout)
    {
        int countSets = 0;
        int countPushConstants = 0;
        for (int i = 0; i < _countShaders; i++)
        {
            ShaderData *data = _modules[i].getData();
            countSets += data->getCountSets();
            countPushConstants += data->isUsePushConstant() ? 1 : 0;
        }

        VkDescriptorSetLayout setLayouts[countSets];
        int setIndex = 0;
        for (int i = 0; i < _countShaders; i++)
        {
            ShaderData *data = _modules[i].getData();
            int sets = data->getCountSets();
            for (int i2 = 0; i2 < sets; i2++)
            {
                const ShaderData::ShaderSetData &setData = data->getSetData(i2);
                setLayouts[setIndex] = setData.setLayout;
                setIndex++;
            }
        }

        VkPushConstantRange pushConstants[countPushConstants];
        int pushConstantIndex = 0;
        for (int i = 0; i < _countShaders; i++)
        {
            ShaderData *data = _modules[i].getData();
            if (!data->isUsePushConstant())
                continue;

            VkPushConstantRange info;
            info.stageFlags = data->getStageFlag();
            info.size = data->getPushConstantData().size;
            info.offset = data->getPushConstantData().offset;
            pushConstants[pushConstantIndex] = info;
            pushConstantIndex++;
        }

        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = countSets;
        pipelineLayoutInfo.pSetLayouts = setLayouts;
        pipelineLayoutInfo.pushConstantRangeCount = countPushConstants;
        pipelineLayoutInfo.pPushConstantRanges = pushConstants;

        VkPipelineLayout layout;
        if (_device->vkCreatePipelineLayout(*_device, &pipelineLayoutInfo, NULL, &layout) == VK_SUCCESS)
        {
            *_outPipelineLayout = VulkanPipelineLayout(layout);
            return true;
        }
        else
        {
            return false;
        }
    }

    void VulkanPipelineLayout::destroyPipelineLayout(VulkanDevice *_device, VulkanPipelineLayout *_pipelineLayout)
    {
        _device->vkDestroyPipelineLayout(*_device, _pipelineLayout->layout, NULL);
        _pipelineLayout->layout = VK_NULL_HANDLE;
    }
}