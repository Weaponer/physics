#include <iostream>
#include <cstring>
#include <fstream>
#include <sstream>
#include <SDL2/SDL.h>

#include "tests/gpuTester.h"
#include "gpuCommon/specialCommands/vulkanComputeCommands.h"
#include "gpuCommon/vulkanFence.h"

#include "gpuCommon/shaders/shader.h"
#include "gpuCommon/shaders/shaderReader.h"
#include "gpuCommon/shaders/shaderManager.h"
#include "gpuCommon/shaders/vulkanDescriptorPoolManager.h"
#include "gpuCommon/shaders/computePipeline.h"
#include "gpuCommon/specialCommands/shaderAutoParameters.h"
#include "gpuCommon/shaders/shaderParameters.h"
#include "extension/phWindowImGUI.h"

namespace phys
{
    GpuTester::GpuTester(vk::VulkanCommandReceiver *_receiver, vk::VulkanBufferAllocator *_bufferAllocator,
                         vk::VulkanImageAllocator *_imageAllocator, Window *_window)
        : device(_receiver->getDevice()), receiver(_receiver), bufferAllocator(_bufferAllocator),
          imageAllocator(_imageAllocator), window(_window),
          shaderDatas(), shaderManager(device),
          renderPassesManager(device), graphicPipelineManager(device), graphicCommands()
    {
        readPacketShader();
    }

    GpuTester::~GpuTester()
    {
        for (auto i : shaderDatas)
        {
            delete i;
        }
    }

    void GpuTester::testMemory()
    {
        // srand(time(NULL));

        int countBuffers = 10000;
        int sizeBuffers = 1012;
        uint32_t queueIndex = 0;
        vk::VulkanBuffer *buffers[countBuffers];
        for (int i = 0; i < countBuffers; i++)
        {
            buffers[i] = bufferAllocator->createBuffer(vk::MemoryUseType_Medium, vk::VulkanBufferType_Staging, sizeBuffers, 1, &queueIndex);
        }

        std::cout << "Stable: " << bufferAllocator->getMemoryController()->checkMemoryStable() << std::endl;

        int randomDeleteBuffers = 1000;
        for (int i = 0; i < randomDeleteBuffers; i++)
        {
            int index = rand() % countBuffers;
            if (buffers[index] != NULL)
            {
                bufferAllocator->destroyBuffer(buffers[index]);
                buffers[index] = NULL;
                // std::cout << "Delete: " << index << std::endl;
                // std::cout << "Stable: " << bufferAllocator->getMemoryController()->checkMemoryStable() << std::endl;
            }
        }

        std::cout << "Stable: " << bufferAllocator->getMemoryController()->checkMemoryStable() << std::endl;

        for (int i = 0; i < countBuffers; i++)
        {
            if (buffers[i] != NULL)
            {
                bufferAllocator->destroyBuffer(buffers[i]);
                buffers[i] = NULL;
            }
        }
    }

    void GpuTester::test()
    {
        vk::ShaderModule fillImageProgram = vk::ShaderModule();
        vk::ShaderModule fillStorageProgram = vk::ShaderModule();
        shaderManager.getShaderByName("fillImage", &fillImageProgram);
        shaderManager.getShaderByName("fillStorage", &fillStorageProgram);

        vk::ComputePipeline cpFillImage = vk::ComputePipeline();
        vk::ComputePipeline::createComputePipeline(device, fillImageProgram, &cpFillImage);
        vk::ComputePipeline cpFillStorage = vk::ComputePipeline();
        vk::ComputePipeline::createComputePipeline(device, fillStorageProgram, &cpFillStorage);

        vk::VulkanDescriptorPoolManager descriptorPoolManager = vk::VulkanDescriptorPoolManager(device);
        descriptorPoolManager.preparePools(&shaderManager);

        vk::ShaderAutoParameters spFillImage = vk::ShaderAutoParameters(device, fillImageProgram);
        spFillImage.takeMemoryPools(&descriptorPoolManager, 4);

        vk::ShaderAutoParameters spFillStorage = vk::ShaderAutoParameters(device, fillStorageProgram);
        spFillStorage.takeMemoryPools(&descriptorPoolManager, 4);

        vk::VulkanFence fence{};
        vk::VulkanFence::createFence(device, &fence, false);

        vk::VulkanQueue *queue = receiver->getFreeQueue(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT);
        vk::VulkanComputeCommands computeCommands = vk::VulkanComputeCommands(queue);

        int countFamilyIndexes = 1;
        uint32_t familyIndex = queue->getFamilyIndex();

        //
        int stagingSize = sizeof(int) * 16;
        vk::VulkanBuffer *staginBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Medium, vk::VulkanBufferType_Staging, stagingSize,
                                                                       countFamilyIndexes, &familyIndex);
        int value = 32;
        int uniformBufferSize = sizeof(value);
        vk::VulkanBuffer *uniformBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Medium, vk::VulkanBufferType_UniformBlockStatic, uniformBufferSize,
                                                                        countFamilyIndexes, &familyIndex);

        struct
        {
            int countCalls;
            int firstValue;
        } outStoreData;

        int outStoreDataSize = sizeof(outStoreData);

        vk::VulkanBuffer *outStoreDataBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Medium, vk::VulkanBufferType_UniformStorageBufferStatic, outStoreDataSize,
                                                                             countFamilyIndexes, &familyIndex);

        vk::VulkanImage *atomicCounter = imageAllocator->createImage(vk::MemoryUseType_Medium, vk::VulkanImageType_ImageStatic,
                                                                     VK_IMAGE_TYPE_1D, VK_FORMAT_R32_SINT, VK_IMAGE_ASPECT_COLOR_BIT,
                                                                     1, 1, 1, 1, 1,
                                                                     countFamilyIndexes, &familyIndex);
        vk::VulkanImageView atomicCounterView;
        vk::VulkanImageView::createImageView(device, VK_IMAGE_VIEW_TYPE_1D, atomicCounter, &atomicCounterView);

        int widthImage = 256;
        int heightImage = 256;
        vk::VulkanImage *imageValues = imageAllocator->createImage(vk::MemoryUseType_Medium, vk::VulkanImageType_ImageStatic,
                                                                   VK_IMAGE_TYPE_2D, VK_FORMAT_R32_SINT, VK_IMAGE_ASPECT_COLOR_BIT,
                                                                   widthImage, heightImage, 1, 1, 1,
                                                                   countFamilyIndexes, &familyIndex);
        vk::VulkanImageView imageValuesView;
        vk::VulkanImageView::createImageView(device, VK_IMAGE_VIEW_TYPE_2D, imageValues, &imageValuesView);

        computeCommands.writeToBuffer(staginBuffer, 0, uniformBufferSize, &value);
        computeCommands.copyBuffer(staginBuffer, 0, uniformBuffer, 0, uniformBufferSize);
        computeCommands.barrierBufferAccess(uniformBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                                            VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_UNIFORM_READ_BIT);
        computeCommands.barrierImageAccess(imageValues, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                                           VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, VK_ACCESS_NONE, VK_ACCESS_SHADER_WRITE_BIT);

        computeCommands.barrierImageAccess(atomicCounter, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                           VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT);

        VkClearColorValue clearCol;
        clearCol.int32[0] = 10000;
        clearCol.int32[1] = 0;
        clearCol.int32[2] = 0;
        clearCol.int32[3] = 0;
        computeCommands.clearImage(atomicCounter, &clearCol);

        computeCommands.barrierImageAccess(atomicCounter, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL,
                                           VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_WRITE_BIT);

        spFillImage.beginUpdate();
        spFillImage.setParameter<vk::ShaderAutoParameters::UniformBuffer>(0, 0, uniformBuffer);
        spFillImage.setParameter<vk::ShaderAutoParameters::StorageImage>(0, 0, imageValuesView);
        spFillImage.setParameter<vk::ShaderAutoParameters::StorageImage>(0, 1, atomicCounterView);
        spFillImage.endUpdate();

        computeCommands.bindPipeline(cpFillImage);
        computeCommands.bindDescriptorSet(fillImageProgram.getData()->getPipelineLayout(), spFillImage.getCurrentSet(0), 0);
        computeCommands.dispatch(32, 32, 1);

        computeCommands.barrierImageAccess(atomicCounter, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                                           VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
        computeCommands.barrierImageAccess(imageValues, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                                           VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);

        spFillStorage.beginUpdate();
        spFillStorage.setParameter<vk::ShaderAutoParameters::StorageBuffer>(0, 0, outStoreDataBuffer);
        spFillStorage.setParameter<vk::ShaderAutoParameters::StorageImage>(0, 0, imageValuesView);
        spFillStorage.setParameter<vk::ShaderAutoParameters::StorageImage>(0, 1, atomicCounterView);
        spFillStorage.endUpdate();

        computeCommands.bindPipeline(cpFillStorage);
        computeCommands.bindDescriptorSet(fillStorageProgram.getData()->getPipelineLayout(), spFillStorage.getCurrentSet(0), 0);
        computeCommands.dispatch(1, 1, 1);

        computeCommands.barrierBufferAccess(outStoreDataBuffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT);

        computeCommands.copyBuffer(outStoreDataBuffer, 0, staginBuffer, 0, outStoreDataSize);

        fence.reset();
        receiver->submitCommands(&computeCommands, fence);
        fence.wait();

        computeCommands.readFromBuffer(staginBuffer, 0, outStoreDataSize, &outStoreData);

        std::cout << outStoreData.countCalls << std::endl;
        std::cout << outStoreData.firstValue << std::endl;

        vk::VulkanImageView::destroyImageView(device, imageValuesView);
        imageAllocator->destroyImage(imageValues);
        vk::VulkanImageView::destroyImageView(device, atomicCounterView);
        imageAllocator->destroyImage(atomicCounter);
        bufferAllocator->destroyBuffer(outStoreDataBuffer);
        bufferAllocator->destroyBuffer(uniformBuffer);
        bufferAllocator->destroyBuffer(staginBuffer);
        //
        vk::VulkanFence::destroyFence(&fence);
        fence = vk::VulkanFence();

        spFillImage.freeMemoryPools(&descriptorPoolManager);
        spFillStorage.freeMemoryPools(&descriptorPoolManager);

        vk::ComputePipeline::destroyComputePipeline(device, &cpFillImage);
        vk::ComputePipeline::destroyComputePipeline(device, &cpFillStorage);
    }

    void GpuTester::testWindowRender()
    {
        initWindowRendering();
        PhWindowImGUI *imGUI = new PhWindowImGUI();
        window->connectGUI(imGUI);
        bool running = true;
        while (running)
        {
            running &= window->updateEvents();
            uint imageIndex = 0;
            running &= window->beginRendering(&imageIndex);
            drawFrame(imageIndex);
            imGUI->beginImGui();
            imGUI->endImGui();
            imGUI->drawFrame(signalBeginRenderGUI, window->getSignalEndRenderingSemaphore(), renderingFence);
            running &= window->endRendering();
            renderingFence.wait();
        }
        window->disconnectGUI(imGUI);
        delete imGUI;
        endWindowRendering();
    }

    void GpuTester::readPacketShader()
    {
        std::fstream f;
        f.open("shaders/cshaders/shaders.spak", std::fstream::in);

        std::vector<char> fileData;
        fileData = std::vector<char>(std::istreambuf_iterator<char>(f), {});
        f.close();

        vk::ShaderReader reader = vk::ShaderReader();
        int countShaders = reader.getCountShadersInPacket(fileData.data());
        vk::ShaderData *shaderDatasPacket[countShaders];
        reader.getShadersFromPacket(countShaders, shaderDatasPacket, fileData.data());
        for (int i = 0; i < countShaders; i++)
        {
            shaderDatasPacket[i]->prepareData(device);
            if (!shaderManager.registerShader(shaderDatasPacket[i], NULL))
            {
                delete shaderDatasPacket[i];
            }
            else
            {
                shaderDatas.push_back(shaderDatasPacket[i]);
            }
        }
    }

    void GpuTester::initWindowRendering()
    {
        vk::VulkanSwapchain *swapchain = window->getSwapchain();
        renderPassesManager.beginCreateRenderPass();
        renderPassesManager.addColorAttachments(swapchain->getImage(0));
        renderPassesManager.endCreate(&renderPass);

        int countImages = swapchain->getCountImage();
        for (int i = 0; i < countImages; i++)
        {
            vk::VulkanImage *image = swapchain->getImage(i);
            vk::VulkanImageView view = swapchain->getImageView(i);
            vk::VulkanFramebuffer framebuffer = vk::VulkanFramebuffer();
            vk::VulkanFramebuffer::createFramebuffer(device, renderPass, 1, &image, &view, NULL, NULL, &framebuffer);
            framebuffers.push_back(framebuffer);
        }

        shaderManager.getShaderByName("basicRenderVert", &vertexShader);
        shaderManager.getShaderByName("basicRenderFrag", &fragmentShader);
        vk::ShaderModule modules[2] = {vertexShader, fragmentShader};
        vk::VulkanPipelineLayout::createPipelineLayout(device, 2, modules, &graphicPipelineLayout);

        blendColorState.setCountColorAttachments(1);
        vertices[0] = {mth::vec3(0.0f, -0.5f, 0.0f), mth::vec3(1.0f, 0.0f, 0.0f), mth::vec2(0.0f, 0.0f)};
        vertices[1] = {mth::vec3(0.5f, 0.5f, 0.0f), mth::vec3(0.0f, 1.0f, 0.0f), mth::vec2(0.0f, 1.0f)};
        vertices[2] = {mth::vec3(-0.5f, 0.5f, 0.0f), mth::vec3(0.0f, 0.0f, 1.0f), mth::vec2(1.0f, 1.0f)};
        vertexAttrubuteState.setSizeInputVertex(sizeof(Vertex));
        vertexAttrubuteState.addInputVertexAttribute(offsetof(Vertex, position), VK_FORMAT_R32G32B32_SFLOAT);
        vertexAttrubuteState.addInputVertexAttribute(offsetof(Vertex, color), VK_FORMAT_R32G32B32_SFLOAT);
        vertexAttrubuteState.addInputVertexAttribute(offsetof(Vertex, uv), VK_FORMAT_R32G32_SFLOAT);

        graphicPipeline = graphicPipelineManager.createGraphicPipeline();
        graphicPipeline->setVertexModule(vertexShader);
        graphicPipeline->setFragmentModule(fragmentShader);
        graphicPipeline->setPipelineLayout(graphicPipelineLayout);
        graphicPipeline->setBlendColorState(&blendColorState);
        graphicPipeline->setVertexAttributeState(&vertexAttrubuteState);
        graphicPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_VIEWPORT, true);
        graphicPipeline->setEnableDynamicState(VK_DYNAMIC_STATE_SCISSOR, true);
        graphicPipelineManager.updateGraphicPipeline(graphicPipeline, renderPass);

        vk::VulkanSemaphore::createSemaphore(device, &signalBeginRenderGUI);
        vk::VulkanFence::createFence(device, &renderingFence, false);

        graphicQueue = window->getVulkanQueue();
        graphicCommands = new vk::VulkanGraphicCommands(graphicQueue);

        uint32_t indexQueueFamily = graphicQueue->getFamilyIndex();
        vertexBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Long, vk::VulkanBufferType_VertexBufferStatic, sizeof(vertices), 1, &indexQueueFamily);
        vk::VulkanBuffer *stagingBuffer = bufferAllocator->createBuffer(vk::MemoryUseType_Temporary, vk::VulkanBufferType_Staging, sizeof(vertices), 1, &indexQueueFamily);
        graphicCommands->writeToBuffer(stagingBuffer, 0, sizeof(vertices), vertices.data());
        graphicCommands->copyBuffer(stagingBuffer, 0, vertexBuffer, 0, sizeof(vertices));
        receiver->submitCommands(graphicCommands, renderingFence);
        renderingFence.wait();
        bufferAllocator->destroyBuffer(stagingBuffer);
    }

    bool GpuTester::drawFrame(uint _indexImage)
    {
        std::cout << "Frame update" << std::endl;
        graphicCommands->resetCommandBuffer();

        renderingFence.reset();
        vk::VulkanSwapchain *swapchain = window->getSwapchain();

        vk::VulkanImage *currentImage = swapchain->getImage(_indexImage);
        vk::VulkanFramebuffer framebuffer = framebuffers[_indexImage];

        graphicCommands->addWaitSemaphoreCmd(window->getWaitBeginRenderingSemaphore(), VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
        graphicCommands->barrierImageAccess(currentImage, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                            VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT);

        VkClearColorValue clearColor{};
        clearColor.float32[0] = 0.2f;
        clearColor.float32[1] = 0.2f;
        clearColor.float32[2] = 0.2f;
        graphicCommands->clearImage(currentImage, &clearColor);
        graphicCommands->barrierImageAccess(currentImage, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                            VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

        graphicCommands->beginRenderPass(renderPass, framebuffer);
        graphicCommands->bindPipeline(graphicPipeline->getPipeline(&renderPass));
        VkRect2D scissor{};
        scissor.extent.width = framebuffer.getWidth();
        scissor.extent.height = framebuffer.getHeight();
        graphicCommands->setScissor(0, 1, &scissor);
        VkViewport viewport{};
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        viewport.width = framebuffer.getWidth();
        viewport.height = framebuffer.getHeight();
        graphicCommands->setViewport(0, 1, &viewport);
        graphicCommands->bindVertexBuffer(vertexBuffer, 0);
        graphicCommands->draw(3, 1, 0, 0);
        graphicCommands->endRenderPass();

        graphicCommands->barrierImageAccess(currentImage, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                                            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                                            VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_NONE);
        graphicCommands->setSignalSemaphoreCmd(signalBeginRenderGUI);
        receiver->submitCommands(graphicCommands);

        return true;
    }

    void GpuTester::endWindowRendering()
    {
        bufferAllocator->destroyBuffer(vertexBuffer);

        delete graphicCommands;
        receiver->returnQueue(graphicQueue);

        vk::VulkanFence::destroyFence(&renderingFence);
        vk::VulkanSemaphore::destroySemaphore(&signalBeginRenderGUI);

        graphicPipelineManager.deleteGraphicPipeline(graphicPipeline);

        vk::VulkanPipelineLayout::destroyPipelineLayout(device, &graphicPipelineLayout);

        int countImages = window->getSwapchain()->getCountImage();
        for (int i = 0; i < countImages; i++)
        {
            vk::VulkanFramebuffer framebuffer = framebuffers[i];
            vk::VulkanFramebuffer::destroyFramebuffer(device, &framebuffer);
        }
    }
}