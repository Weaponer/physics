#include <mutex>
#include <time.h>
#include <unistd.h>
#include <chrono>

#include "tests/threadsTester.h"
#include "threads/safePrint.h"

namespace phys
{
    ThreadsTester::ThreadsTester() : manager(NULL)
    {
        manager = new PhThreadsManager();
        manager->allocateTaskThreads(4);
    }

    ThreadsTester::~ThreadsTester()
    {
        delete manager;
    }

    void ThreadsTester::test()
    {
        TestTask *task1 = new TestTask("Task_1");
        TestTask *task2 = new TestTask("Task_2");
        TestTask *task3 = new TestTask("Task_3");
        TestTask *task4 = new TestTask("Task_4");
        TestTask *task5 = new TestTask("Task_5");
        std::chrono::_V2::system_clock::time_point start, end;
        int countSums = 0;
        float sum = 0.0f;
        for (int i = 0; i < 1; i++)
        {
            start = std::chrono::high_resolution_clock::now();
            manager->addTask(task1, 1);
            manager->addTask(task2, 1);
            manager->addTask(task3, 1);
            manager->addTask(task4, 1);
            manager->addTask(task5, 1);

            manager->waitAllTasks();
            end = std::chrono::high_resolution_clock::now();
            sum += std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000000.0f;
            countSums++;
        }

        std::cout << sum << std::endl;

        std::cout << "Task_1 total: " << task1->counter << std::endl;
        std::cout << "Task_2 total: " << task2->counter << std::endl;
        std::cout << "Task_3 total: " << task3->counter << std::endl;
        std::cout << "Task_4 total: " << task4->counter << std::endl;
        std::cout << "Task_5 total: " << task5->counter << std::endl;

        delete task5;
        delete task4;
        delete task3;
        delete task2;
        delete task1;
    }
}