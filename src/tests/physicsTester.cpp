#include "tests/physicsTester.h"

namespace phys
{
    PhysicsTester::PhysicsTester(PhFoundation *_foundation) : foundation(_foundation)
    {
        physics = PhPhysics::createPhysics(_foundation);
        scene = physics->createScene();
    }

    PhysicsTester::~PhysicsTester()
    {
        physics->destroyScene(scene);
        PhPhysics::destroyPhysics(physics);
    }

    void PhysicsTester::test()
    {
        srand((unsigned)time(0));

        PhMaterial *materials[1];
        for (int i = 0; i < 1; i++)
        {
            materials[i] = physics->createMaterial();
            materials[i]->setBounciness((float)((rand() % 10000) / 5000.0f));
        }
        // PhMaterial *material = physics->createMaterial();

        PhShape *shape = physics->createShape();
        PhBoxGeometry *box = physics->createBoxGeometry();
        shape->setMaterial(materials[0]);

        PhRigidDynamic *dynamic = physics->createRigidBodyDynamic();
        dynamic->addShape(shape);

        dynamic->setPosition(mth::vec3(0.0f, 10.0f, 0.0f));

        shape->connectGeometry(box);

        scene->addActor(dynamic);
        box->setOffset(mth::vec3(1.0f, 0.0f, 0.0f));

        PhSimulationSettings simulationSettings = PhSimulationSettings();

        scene->simulation(0.0f, simulationSettings);
        scene->fetch();

        printOBBsData();

        scene->simulation(0.0f, simulationSettings);
        scene->fetch();

        printOBBsData();

        scene->removeActor(dynamic);

        scene->simulation(0.0f, simulationSettings);
        scene->fetch();

        printOBBsData();

        scene->simulation(0.0f, simulationSettings);
        scene->fetch();

        printOBBsData();

        dynamic->removeShape(shape);
        shape->disconnectGeometry(box);
        shape->setMaterial(NULL);

        physics->destroyRigidBodyDynamic(dynamic);
        physics->destroyGeometry(box);
        physics->destroyShape(shape);

        for (int i = 0; i < 1; i++)
        {
            physics->destroyMaterial(materials[i]);
        }
    }

    void PhysicsTester::printOBBsData()
    {
        printf("OBBs:\n");
        const PhOutputDebugData *data = scene->getOutputDebugData();
        uint32_t countOBBs = data->getCountObbs();
        const PhOutputDebugData::Orientation *obbsOrt = data->getObbsBuffer();
        for (uint32_t i = 0; i < countOBBs; i++)
        {
            PhOutputDebugData::Orientation ort = obbsOrt[i];
            printf("obb: rot - %f %f %f %f, size - %f %f %f, pos %f %f %f\n", ort.rotation.x, ort.rotation.y, ort.rotation.z, ort.rotation.w,
                   ort.size.x, ort.size.y, ort.size.z, ort.position.x, ort.position.y, ort.position.z);
        }
    }
}