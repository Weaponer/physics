#include "common/phVulkanLibraryProvider.h"

namespace phys
{
    bool PhVulkanLibraryProvider::load()
    {
        void *loadedLibrary = loadLibrary();
        if(loadedLibrary)
        {
            vulkanLibrary = loadedLibrary;
            return true;
        }
        else
        {
            return false;
        }
    }

    void PhVulkanLibraryProvider::close()
    {
        closeLibrary(vulkanLibrary);
    }

    void *PhVulkanLibraryProvider::loadFunction(const char *_name)
    {
        return loadFunctionOfLibrary(vulkanLibrary, _name);
    }
}