#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

#include "common/phResourceProvider.h"
#include "gpuCommon/shaders/shaderReader.h"
#include "common/phMacros.h"

namespace phys
{
    static mth::vec3 getTanget(mth::vec3 pos1, mth::vec3 pos2, mth::vec3 pos3, mth::vec2 uv1, mth::vec2 uv2, mth::vec2 uv3)
    {
        mth::vec3 edge1 = pos2 - pos1;
        mth::vec3 edge2 = pos3 - pos1;
        mth::vec2 deltaUV1 = uv2 - uv1;
        mth::vec2 deltaUV2 = uv3 - uv1;

        float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

        mth::vec3 tangent;
        tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

        return tangent;
    }

    static int loadDataFromFile(std::ifstream &file, std::vector<std::string> &names, std::list<std::vector<uint>> &index_pos,
                                std::list<std::vector<uint>> &index_uv, std::list<std::vector<uint>> &index_normal, std::list<std::vector<uint>> &index_lines,
                                std::vector<uint> *indexs,
                                std::vector<mth::vec3> *positions, std::vector<mth::vec2> *uvs,
                                std::vector<mth::vec3> *normals)
    {
        float scale = 0.01f;

        std::stringstream ss;
        std::string line = "";
        std::string prefix = "";
        int countMeshes = 0;

        mth::vec3 pos;
        mth::vec2 uv;
        mth::vec3 normal;

        std::vector<uint> *current_indexes_pos;
        std::vector<uint> *current_indexes_uv;
        std::vector<uint> *current_indexes_normal;
        std::vector<uint> *current_indexes_lines;

        while (std::getline(file, line))
        {
            ss.clear();
            ss.str(line);
            prefix.clear();
            ss >> prefix;

            if (prefix == "#")
            {
            }
            else if (prefix == "o")
            {
                std::string name;
                ss >> name;
                names.push_back(name);
                countMeshes++;

                index_pos.push_back(std::vector<uint>());
                current_indexes_pos = &(index_pos.back());

                index_uv.push_back(std::vector<uint>());
                current_indexes_uv = &(index_uv.back());

                index_normal.push_back(std::vector<uint>());
                current_indexes_normal = &(index_normal.back());

                index_lines.push_back(std::vector<uint>());
                current_indexes_lines = &(index_lines.back());
            }
            else if (prefix == "s")
            {
            }
            else if (prefix == "v")
            {
                ss >> pos.x >> pos.y >> pos.z;
                positions->push_back(pos * scale);
            }
            else if (prefix == "vt")
            {
                ss >> uv.x >> uv.y;
                uvs->push_back(uv);
            }
            else if (prefix == "vn")
            {
                ss >> normal.x >> normal.y >> normal.z;
                normals->push_back(normal);
            }
            else if (prefix == "f")
            {
                int temp = 0;
                int count = 0;

                std::vector<uint> ip;
                std::vector<uint> iuv;
                std::vector<uint> in;
                while (ss >> temp)
                {
                    if (count == 0)
                        ip.push_back(temp);
                    if (count == 1)
                        iuv.push_back(temp);
                    if (count == 2)
                        in.push_back(temp);

                    if (ss.peek() == '/')
                    {
                        ++count;
                        ss.ignore(1, '/');
                    }

                    if (ss.peek() == ' ')
                    {
                        ++count;
                        ss.ignore(1, ' ');
                    }

                    if (count > 2)
                    {
                        count = 0;
                    }
                }
                if (ip.size() > 4)
                {
                    current_indexes_pos->push_back(ip[0]);
                    current_indexes_pos->push_back(ip[1]);
                    current_indexes_pos->push_back(ip[2]);
                    current_indexes_uv->push_back(iuv[0]);
                    current_indexes_uv->push_back(iuv[1]);
                    current_indexes_uv->push_back(iuv[2]);
                    current_indexes_normal->push_back(in[0]);
                    current_indexes_normal->push_back(in[1]);
                    current_indexes_normal->push_back(in[2]);

                    current_indexes_lines->push_back(ip[0]);
                    current_indexes_lines->push_back(ip[1]);

                    current_indexes_lines->push_back(ip[1]);
                    current_indexes_lines->push_back(ip[2]);

                    current_indexes_lines->push_back(ip[2]);
                    current_indexes_lines->push_back(ip[0]);

                    for (int i = 3; i < (int)ip.size(); i++)
                    {
                        int lastOneIp = current_indexes_pos->at(current_indexes_pos->size() - 3);
                        int lastTwoIp = current_indexes_pos->at(current_indexes_pos->size() - 1);
                        int lastOneUv = current_indexes_uv->at(current_indexes_pos->size() - 3);
                        int lastTwoUv = current_indexes_uv->at(current_indexes_pos->size() - 1);
                        int lastOneN = current_indexes_normal->at(current_indexes_pos->size() - 3);
                        int lastTwoN = current_indexes_normal->at(current_indexes_pos->size() - 1);

                        current_indexes_pos->push_back(lastOneIp);
                        current_indexes_pos->push_back(lastTwoIp);
                        current_indexes_pos->push_back(ip[i]);
                        current_indexes_uv->push_back(lastOneUv);
                        current_indexes_uv->push_back(lastTwoUv);
                        current_indexes_uv->push_back(iuv[i]);
                        current_indexes_normal->push_back(lastOneN);
                        current_indexes_normal->push_back(lastTwoN);
                        current_indexes_normal->push_back(in[i]);

                        current_indexes_lines->push_back(ip[i]);
                        current_indexes_lines->push_back(lastOneIp);

                        current_indexes_lines->push_back(lastOneIp);
                        current_indexes_lines->push_back(lastTwoIp);

                        current_indexes_lines->push_back(lastTwoIp);
                        current_indexes_lines->push_back(ip[i]);
                    }
                }
                else if (ip.size() > 3)
                {
                    current_indexes_pos->push_back(ip[0]);
                    current_indexes_pos->push_back(ip[1]);
                    current_indexes_pos->push_back(ip[2]);
                    current_indexes_uv->push_back(iuv[0]);
                    current_indexes_uv->push_back(iuv[1]);
                    current_indexes_uv->push_back(iuv[2]);
                    current_indexes_normal->push_back(in[0]);
                    current_indexes_normal->push_back(in[1]);
                    current_indexes_normal->push_back(in[2]);

                    current_indexes_pos->push_back(ip[0]);
                    current_indexes_pos->push_back(ip[2]);
                    current_indexes_pos->push_back(ip[3]);
                    current_indexes_uv->push_back(iuv[0]);
                    current_indexes_uv->push_back(iuv[2]);
                    current_indexes_uv->push_back(iuv[3]);
                    current_indexes_normal->push_back(in[0]);
                    current_indexes_normal->push_back(in[2]);
                    current_indexes_normal->push_back(in[3]);

                    current_indexes_lines->push_back(ip[0]);
                    current_indexes_lines->push_back(ip[1]);

                    current_indexes_lines->push_back(ip[1]);
                    current_indexes_lines->push_back(ip[2]);

                    current_indexes_lines->push_back(ip[2]);
                    current_indexes_lines->push_back(ip[3]);

                    current_indexes_lines->push_back(ip[3]);
                    current_indexes_lines->push_back(ip[0]);
                }
                else
                {
                    current_indexes_pos->push_back(ip[0]);
                    current_indexes_pos->push_back(ip[1]);
                    current_indexes_pos->push_back(ip[2]);
                    current_indexes_uv->push_back(iuv[0]);
                    current_indexes_uv->push_back(iuv[1]);
                    current_indexes_uv->push_back(iuv[2]);
                    current_indexes_normal->push_back(in[0]);
                    current_indexes_normal->push_back(in[1]);
                    current_indexes_normal->push_back(in[2]);

                    current_indexes_lines->push_back(ip[0]);
                    current_indexes_lines->push_back(ip[1]);

                    current_indexes_lines->push_back(ip[1]);
                    current_indexes_lines->push_back(ip[2]);

                    current_indexes_lines->push_back(ip[2]);
                    current_indexes_lines->push_back(ip[0]);
                }
            }
            else
            {
            }
        }
        return countMeshes;
    }

    static void fillBuffersForMesh(PhObjectMesh::Vertex *verts, int count, std::vector<uint> *indexs, std::vector<mth::vec3> *positions,
                                   std::vector<mth::vec2> *uvs, std::vector<mth::vec3> *tangent, std::vector<mth::vec3> *normals)
    {
        bool t = false;
        int num = 0;

        for (int i2 = 0; i2 < count; i2++)
        {
            t = false;
            uint ct = positions->size();

            for (uint i3 = 0; i3 < ct; i3++)
            {
                if (verts[i2].normal == (*normals)[i3] && verts[i2].tangent == (*tangent)[i3] && verts[i2].uv == (*uvs)[i3] &&
                    verts[i2].pos == (*positions)[i3])
                {
                    num = i3;
                    t = true;
                    break;
                }
            }

            if (t)
            {
                indexs->push_back(num);
            }
            else
            {
                indexs->push_back(positions->size());
                positions->push_back(verts[i2].pos);
                uvs->push_back(verts[i2].uv);
                normals->push_back(verts[i2].normal);
                tangent->push_back(verts[i2].tangent);
            }
        }
    }

    PhResourceProvider::~PhResourceProvider()
    {
        release();
    }

    uint32_t PhResourceProvider::getCountShaders(const char *_tag) const
    {
        Packet packet;
        if (getFindPacket(_tag, &packet))
        {
            return packet.shaders->countElements();
        }
        return 0;
    }

    vk::ShaderData *const *PhResourceProvider::getShadersPerTag(const char *_tag)
    {
        Packet packet;
        if (getFindPacket(_tag, &packet))
        {
            return packet.shaders->getData();
        }
        return NULL;
    }

    vk::ShaderData *PhResourceProvider::getShaderData(const char *_name, const char *_tag)
    {
        Packet packet;
        if (getFindPacket(_tag, &packet))
        {
            int count = packet.shaders->countElements();
            for (int i = 0; i < count; i++)
            {
                vk::ShaderData *shader = packet.shaders->getElement(i);
                if (std::strcmp(shader->getName(), _name) == 0)
                    return shader;
            }
        }

        return NULL;
    }

    void PhResourceProvider::loadShadersPacket(const char *_path, const char *_tag)
    {
        std::string path = std::string(getPathToShaders()) + std::string(_path);
        const char *cstrPath = path.c_str();
        int size = 0;
        char *data = NULL;
        if (!readFile(cstrPath, &data, &size))
            return;

        vk::ShaderReader reader = vk::ShaderReader();
        int count = reader.getCountShadersInPacket(data);
        vk::ShaderData *shaders[count];

        reader.getShadersFromPacket(count, shaders, data);

        Packet packet{};
        if (getFindPacket(_tag, &packet))
        {
            for (int i = 0; i < count; i++)
                packet.shaders->pushElement(&(shaders[i]));
        }
        else
        {
            packet.shaders = new (phAllocateMemory(sizeof(PhBuffer<vk::ShaderData *>))) PhBuffer<vk::ShaderData *>();
            int lenTag = std::strlen(_tag) + 1;
            packet.tag = (char *)phAllocateMemory(sizeof(char) * lenTag);
            std::memcpy(packet.tag, _tag, sizeof(char) * lenTag);

            for (int i = 0; i < count; i++)
                packet.shaders->pushElement(&(shaders[i]));

            bufferShadersData.pushElement(&packet);
        }

        phDeallocateMemory(data);
    }

    PhObjectMesh *PhResourceProvider::loadObjectMesh(const char *_path)
    {
        std::string path = std::string(getPathToObjectMeshes()) + std::string(_path);

        std::ifstream file(path);
        if (file.is_open())
        {
            std::vector<uint> *indexs = new std::vector<uint>();
            std::vector<mth::vec3> *positions = new std::vector<mth::vec3>();
            std::vector<mth::vec2> *uvs = new std::vector<mth::vec2>();
            std::vector<mth::vec3> *tangent = new std::vector<mth::vec3>();
            std::vector<mth::vec3> *normals = new std::vector<mth::vec3>();

            int countMeshes = 0;
            std::vector<std::string> names;

            std::vector<PhObjectMesh::Vertex *> bufferVertexes;
            std::vector<PhObjectMesh::LineVertex *> bufferLines;
            std::vector<uint> countIndexesInBuffers;
            std::vector<uint> countLineVertexes;

            std::list<std::vector<uint>> index_pos;
            std::list<std::vector<uint>> index_uv;
            std::list<std::vector<uint>> index_normal;
            std::list<std::vector<uint>> index_line;

            mth::vec3 pos;
            mth::vec2 uv;
            mth::vec3 normal;

            countMeshes = loadDataFromFile(file, names, index_pos, index_uv, index_normal, index_line, indexs, positions, uvs, normals);
            file.close();

            for (int i = 0; i < countMeshes; i++)
            {
                auto c_indexes_pos = index_pos.begin();
                std::advance(c_indexes_pos, i);

                auto c_indexes_uv = index_uv.begin();
                std::advance(c_indexes_uv, i);

                auto c_indexes_normal = index_normal.begin();
                std::advance(c_indexes_normal, i);

                auto c_indexes_line = index_line.begin();
                std::advance(c_indexes_line, i);

                uint count = (*c_indexes_pos).size();
                PhObjectMesh::Vertex *verts = new PhObjectMesh::Vertex[count];

                uint countLines = (*c_indexes_line).size();
                PhObjectMesh::LineVertex *lines = new PhObjectMesh::LineVertex[countLines];

                bufferVertexes.push_back(verts);
                countIndexesInBuffers.push_back(count);
                bufferLines.push_back(lines);
                countLineVertexes.push_back(countLines);

                for (uint i2 = 0; i2 < count; i2 = i2 + 3)
                {
                    for (uint i3 = 0; i3 < 3; i3++)
                    {
                        verts[i2 + i3].pos = (*positions)[(*c_indexes_pos)[i2 + i3] - 1];
                        verts[i2 + i3].uv = (*uvs)[(*c_indexes_uv)[i2 + i3] - 1];
                        verts[i2 + i3].normal = (*normals)[(*c_indexes_normal)[i2 + i3] - 1];
                    }

                    mth::vec3 tang = getTanget(verts[i2].pos, verts[i2 + 1].pos, verts[i2 + 2].pos, verts[i2].uv, verts[i2 + 1].uv, verts[i2 + 2].uv);

                    verts[i2].tangent = tang;
                    verts[i2 + 1].tangent = tang;
                    verts[i2 + 2].tangent = tang;
                }

                for (uint i2 = 0; i2 < countLines; i2++)
                {
                    lines[i2].pos = (*positions)[(*c_indexes_line)[i2] - 1];
                }
            }

            // its generate graphic buffer for each mesh
            PhObjectMesh::Mesh **meshes = (PhObjectMesh::Mesh **)phAllocateMemory(sizeof(PhObjectMesh::Mesh *) * countMeshes);
            for (int i = 0; i < countMeshes; i++)
            {
                indexs->clear();
                positions->clear();
                uvs->clear();
                normals->clear();
                tangent->clear();

                fillBuffersForMesh(bufferVertexes[i], countIndexesInBuffers[i], indexs, positions, uvs, tangent, normals);

                int countVertexes = (int)positions->size();
                PhObjectMesh::Vertex *vertexes = (PhObjectMesh::Vertex *)phAllocateMemory(sizeof(PhObjectMesh::Vertex) * countVertexes);
                for (int i2 = 0; i2 < countVertexes; i2++)
                {
                    PhObjectMesh::Vertex vertex;
                    vertex.pos = (*positions)[i2];
                    vertex.normal = (*normals)[i2];
                    vertex.tangent = (*tangent)[i2];
                    vertex.uv = (*uvs)[i2];
                    vertexes[i2] = vertex;
                }

                int countIndexes = indexs->size();
                uint *indexes = (uint *)phAllocateMemory(sizeof(uint) * countIndexes);
                std::memcpy(indexes, indexs->data(), sizeof(uint) * countIndexes);

                int countLine = countLineVertexes[i];
                PhObjectMesh::LineVertex *srcLineVertexes = bufferLines[i];
                PhObjectMesh::LineVertex *lineVertexes = (PhObjectMesh::LineVertex *)phAllocateMemory(sizeof(PhObjectMesh::LineVertex) * countLine);
                std::memcpy((void *)lineVertexes, (void *)srcLineVertexes, sizeof(PhObjectMesh::LineVertex) * countLine);

                PhObjectMesh::Mesh *mesh = new (phAllocateMemory(sizeof(PhObjectMesh::Mesh)))
                    PhObjectMesh::Mesh(names[i].capacity(), names[i].data(),
                                       countVertexes, vertexes,
                                       countIndexes, indexes,
                                       countLine, lineVertexes);

                meshes[i] = mesh;
            }

            std::for_each(bufferVertexes.begin(), bufferVertexes.end(), [](PhObjectMesh::Vertex *vert)
                          { delete[] vert; });
            std::for_each(bufferLines.begin(), bufferLines.end(), [](PhObjectMesh::LineVertex *vert)
                          { delete[] vert; });
            delete indexs;
            delete positions;
            delete uvs;
            delete normals;
            delete tangent;

            PhObjectMesh *model = new (phAllocateMemory(sizeof(PhObjectMesh))) PhObjectMesh(countMeshes, meshes);
            bufferMeshes.pushElement(&model);
            return model;
        }
        else
        {
            return NULL;
        }
    }

    void PhResourceProvider::release()
    {
        {
            int count = bufferShadersData.countElements();
            for (int i = 0; i < count; i++)
            {
                Packet packet = bufferShadersData.getElement(i);
                phDeallocateMemory(packet.tag);

                int countShaders = packet.shaders->countElements();
                for (int i2 = 0; i2 < countShaders; i2++)
                {
                    delete packet.shaders->getElement(i2);
                }

                packet.shaders->release();
                phDeallocateMemory(packet.shaders);
            }
            bufferShadersData.release();
        }
        {
            int count = bufferMeshes.countElements();
            for (int i = 0; i < count; i++)
            {
                PH_RELEASE(bufferMeshes.getElement(i))
            }
            bufferMeshes.release();
        }
    }

    bool PhResourceProvider::getFindPacket(const char *_tag, Packet *_outPacket) const
    {
        *_outPacket = {};
        int count = bufferShadersData.countElements();
        for (int i = 0; i < count; i++)
        {
            Packet packet = bufferShadersData.getElement(i);
            if (std::strcmp(packet.tag, _tag) == 0)
            {
                *_outPacket = packet;
                return true;
            }
        }
        return false;
    }
}