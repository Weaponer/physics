#include <cstring>
#include "common/phObjectMesh.h"
#include "foundation/phMemoryAllocator.h"

namespace phys
{
    PhObjectMesh::Mesh::Mesh(int _sizeName, const char *_name,
                             int _countVertexes, PhObjectMesh::Vertex *_vertexes,
                             int _countIndexes, uint *_indexes,
                             int _countLineVertexes, LineVertex *_lineVertex)
        : sizeName(_sizeName), name(NULL),
          countVertexes(_countVertexes), vertexes(_vertexes),
          countIndexes(_countIndexes), indexes(_indexes),
          countLineVertexes(_countLineVertexes), lineVertexes(_lineVertex)
    {
        name = (char *)phAllocateMemory(sizeof(char) * sizeName);
        std::memcpy(name, _name, sizeof(char) * sizeName);
    }

    void PhObjectMesh::Mesh::release()
    {
        phDeallocateMemory(name);
        phDeallocateMemory(vertexes);
        phDeallocateMemory(indexes);
        phDeallocateMemory(lineVertexes);
    }

    PhObjectMesh::PhObjectMesh(int _countMeshes, Mesh **_meshes) : countMeshes(_countMeshes), meshes(_meshes)
    {
    }

    void PhObjectMesh::release()
    {
        for (int i = 0; i < countMeshes; i++)
        {
            meshes[i]->release();
            phDeallocateMemory(meshes[i]);
        }
        phDeallocateMemory(meshes);
    }
}