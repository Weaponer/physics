
# Physics

The physics engine with the simple scene editor. The physics totally calculates on GPU with help of Vulkan API.

Possibilities:
-
- Pivot system to move/rotate/scale objects.
- Wireframe rendering.
- Object selection system.
- Inspector to edit objects.
- Basic colliders (box, sphere, capsule, plane)
- Dynamic, kinematic, static geometry.
- Support gpu debug markers.


## Dependencies
- Vulkan API
- SDL2
- [MathLib](https://gitlab.com/Weaponer/mathlib-linux.git)
- [VulkanShaderCompiler](https://gitlab.com/Weaponer/vulkanshadercompiler.git)
## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:Weaponer/physics.git
```

Go to the project directory

```bash
  cd physics
```
Make project

```bash
  make
```
Launch program

```bash
  ./output/main
```
## Demo
![](https://gitlab.com/Weaponer/physics/-/raw/main/docs/screenshot_1.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/physics/-/raw/main/docs/screenshot_2.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/physics/-/raw/main/docs/screenshot_3.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/physics/-/raw/main/docs/screenshot_4.jpg?ref_type=heads)